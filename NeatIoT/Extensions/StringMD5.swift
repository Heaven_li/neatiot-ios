//
//  StringMD5.swift
//  NeatIoT
//
//  Created by neat on 2019/10/23.
//  Copyright © 2019 Neat. All rights reserved.
//

import Foundation

//首先添加一个Int扩展
extension Int
{
    //添加一个扩展方法，用来将整数类型，转换成十六进制的字符串。
    func hexedString() -> String
    {
        //通过设置十六进制的格式，
        //将自身转换成十六进制的字符串
        return NSString(format:"%02x", self) as String
    }
}

//添加一个数据类型的扩展
extension NSData
{
    //添加一个扩展方法，用来将数据类型，转换成十六进制的字符串。
    func hexedString() -> String
    {
        //初始化一个字符串对象
        var string = String()
        //将不安全原始指针格式的字节，
        //转换成不安全指针的格式
        let unsafePointer = bytes.assumingMemoryBound(to: UInt8.self)
        //添加一个循环语句
        for i in UnsafeBufferPointer<UInt8>(start:unsafePointer, count: length)
        {
            //通过整形对象的扩展方法，将二进制数据转换成十六进制的字符串。
            string += Int(i).hexedString()
        }
        //返回十六进制的字符串。
        return string
    }

    //添加一个扩展方法，实现对数据的MD5加密功能
    func MD5() -> NSData
    {
        //首先创建一个16位长度的可变数据对象。
        let result = NSMutableData(length: Int(CC_MD5_DIGEST_LENGTH))!
        //将不安全原始指针格式的字节，转换成不安全指针的格式。
        let unsafePointer = result.mutableBytes.assumingMemoryBound(to: UInt8.self)
        //通过调用加密方法，对数据进行加密，并将加密后的数据，存储在可变数据对象中。
        CC_MD5(bytes, CC_LONG(length), UnsafeMutablePointer<UInt8>(unsafePointer))
        //最后将结果转换成数据格式对象并返回。
        return NSData(data: result as Data)
    }
}

//添加一个字符串扩展。
extension String
{
    //添加一个扩展方法，实现加密的功能。
    func MD5() -> String
    {
        //首先将字符串对象，转换成指定编码的数据对象
        let data = (self as NSString).data(using: String.Encoding.utf8.rawValue)! as NSData
        //调用数据对象的扩展方法，进行加密操作
        //并返回十六进制的结果。
        return data.MD5().hexedString()
    }
    
    func getDeviceCode(nativeStr:String,insertIndex:Int) -> String {
        
        var code = String()
        var indexCount = 0;
        for index in nativeStr.indices {
            
            code+=String(nativeStr[index])
            
            if indexCount%insertIndex == insertIndex - 1 {
                if index != nativeStr.startIndex && index != nativeStr.index(before: nativeStr.endIndex) {
                        code+="."
                    }
            }
            
            indexCount+=1
            
        }
        
        return code
    }
}

extension UIImage {
    // 修复图片旋转
    func fixOrientation() -> UIImage {
        if self.imageOrientation == .up {
            return self
        }
         
        var transform = CGAffineTransform.identity
         
        switch self.imageOrientation {
        case .down, .downMirrored:
            transform = transform.translatedBy(x: self.size.width, y: self.size.height)
            transform = transform.rotated(by: .pi)
            break
             
        case .left, .leftMirrored:
            transform = transform.translatedBy(x: self.size.width, y: 0)
            transform = transform.rotated(by: .pi / 2)
            break
             
        case .right, .rightMirrored:
            transform = transform.translatedBy(x: 0, y: self.size.height)
            transform = transform.rotated(by: -.pi / 2)
            break
             
        default:
            break
        }
         
        switch self.imageOrientation {
        case .upMirrored, .downMirrored:
            transform = transform.translatedBy(x: self.size.width, y: 0)
            transform = transform.scaledBy(x: -1, y: 1)
            break
             
        case .leftMirrored, .rightMirrored:
            transform = transform.translatedBy(x: self.size.height, y: 0);
            transform = transform.scaledBy(x: -1, y: 1)
            break
             
        default:
            break
        }
         
        let ctx = CGContext(data: nil, width: Int(self.size.width), height: Int(self.size.height), bitsPerComponent: self.cgImage!.bitsPerComponent, bytesPerRow: 0, space: self.cgImage!.colorSpace!, bitmapInfo: self.cgImage!.bitmapInfo.rawValue)
        ctx?.concatenate(transform)
         
        switch self.imageOrientation {
        case .left, .leftMirrored, .right, .rightMirrored:
            ctx?.draw(self.cgImage!, in: CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(size.height), height: CGFloat(size.width)))
            break
             
        default:
            ctx?.draw(self.cgImage!, in: CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(size.width), height: CGFloat(size.height)))
            break
        }
         
        let cgimg: CGImage = (ctx?.makeImage())!
        let img = UIImage(cgImage: cgimg)
         
        return img
    }
}
