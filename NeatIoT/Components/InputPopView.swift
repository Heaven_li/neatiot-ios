//
//  InputPopView.swift
//  NeatIoT
//
//  Created by neat on 2019/11/13.
//  Copyright © 2019 Neat. All rights reserved.
//

import UIKit


protocol InputPopViewDelegate {
    
    /// 输入数据回调
    /// - Parameter text: 输入文字
    /// - Parameter isSelect: 是否选中上报（如果有复选框）
    func didInputText(text:String,isSelect:Bool)
    
}


class InputPopView: UIView {
    
    private var farterView:UIView?
    
    private var editItemModel:EditItemModel?
    
    var delegate:InputPopViewDelegate?
    
    let fbGenerator = UIImpactFeedbackGenerator.init(style: .heavy)
    
    
    /// 输入项信息
    var currentEditItemModel: EditItemModel {
        get {
            return EditItemModel()
        }
        set {
        
            editItemModel = newValue
            self.inputTextField.keyboardType = newValue.keyboardType
            self.inputTextField.placeholder = newValue.holderText
            if !newValue.inforText.isEmpty {
                self.inputTextField.text = newValue.inforText
            }else{
                self.inputTextField.text = ""
            }
            self.titleLable.text = newValue.titleText
            self.checkBox.selectState = newValue.uploadIsWarning
            
        }
    }
    
    var isShowCheck: Bool {
        get {
            !self.checkBox.isHidden
        }
        set {
            self.checkBox.isHidden = !newValue
        }
    }
    
    /// 背景view
    private lazy var bgImageView: UIImageView = {
        let imageView = UIImageView.init(image: UIImage.init(named: "input_bg_image"))
        imageView.isUserInteractionEnabled = true
        return imageView
    }()
    
    /// 标题
    lazy var titleLable: UILabel = {
        let titleLable = UILabel.init()
        titleLable.font = UIFont.systemFont(ofSize: 17)
        titleLable.textColor = #colorLiteral(red: 0.1879271269, green: 0.1920092106, blue: 0.1882436872, alpha: 1)
        return titleLable
    }()
    
    /// 输入栏
    private lazy var inputTextField: UITextField = {
        let textField = UITextField.init()
        textField.tintColor = .blue
      
        let placeHolderAttribute = NSMutableAttributedString.init(string: "请输入")
        placeHolderAttribute.addAttribute(.foregroundColor, value: #colorLiteral(red: 0.4432226503, green: 0.4432226503, blue: 0.4432226503, alpha: 1), range:NSRange.init(location: 0, length: 3))
        textField.attributedPlaceholder = placeHolderAttribute
        textField.delegate = self
        textField.textColor = #colorLiteral(red: 0.2586170137, green: 0.2463852763, blue: 0.2367299497, alpha: 1)
        textField.returnKeyType = .done
        return textField
    }()
    
    /// 取消按钮
    private lazy var cancelBtn: UIButton = {
        let btn = UIButton.init(type: .custom)
        btn.setTitle("取消", for: .normal)
        btn.setTitleColor(#colorLiteral(red: 0.1879271269, green: 0.1920092106, blue: 0.1882436872, alpha: 1), for: .normal)
        btn.titleLabel?.font = UIFont.systemFont(ofSize: 15)
        btn.addTarget(self, action: #selector(cancelBtnClick), for: .touchUpInside)
        return btn
    }()
    
    /// 复选框
    private lazy var checkBox: LYCheckBox = {
        let checkbox = LYCheckBox.init()
        checkbox.text = "是否报警"
        checkbox.highlight = UIImage.init(named: "check_select")!
        checkbox.iconNormal = UIImage.init(named: "check_normal")!
        return checkbox
    }()
    
    /// 确定按钮
    private lazy var confirmBtn: UIButton = {
        let btn = UIButton.init(type: .custom)
        btn.setTitle("确定", for: .normal)
        btn.setTitleColor(#colorLiteral(red: 0.1879271269, green: 0.1920092106, blue: 0.1882436872, alpha: 1), for: .normal)
        btn.titleLabel?.font = UIFont.systemFont(ofSize: 15)
        btn.addTarget(self, action: #selector(confirmBtnClick), for: .touchUpInside)
        return btn
    }()
    
    /// 分割线
    private lazy var sepLine: UIView = {
        let line = UIView.init()
        line.backgroundColor = #colorLiteral(red: 0.8822753429, green: 0.8824025989, blue: 0.8822476268, alpha: 1)
        return line
    }()
    
    lazy var bgView: UIView = {
        let bgView = UIView.init()
        bgView.backgroundColor = NormalColor.opaBgColor
        return bgView
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        fbGenerator.prepare()
        
        self.addSubview(self.bgView)
        
        self.bgView.snp.makeConstraints { (make) in
            make.edges.equalTo(self)
        }
        self.bgView.alpha = 0
        self.bgView.addSubview(self.bgImageView)
        
        
        self.bgImageView.addSubview(self.titleLable)
        self.bgImageView.addSubview(self.sepLine)
        self.bgImageView.addSubview(self.cancelBtn)
        self.bgImageView.addSubview(self.checkBox)
        self.checkBox.isHidden = true
        self.bgImageView.addSubview(self.confirmBtn)
        self.bgImageView.addSubview(self.inputTextField)
        
        self.titleLable.snp.makeConstraints { (make) in
            make.top.equalTo(20)
            make.leading.equalTo(10)
            make.trailing.equalTo(-10)
            make.height.equalTo(25)
        }
        
        self.bgImageView.snp.makeConstraints { (make) in
            make.bottom.equalTo(self)
            make.leading.trailing.equalTo(self)
            make.height.equalTo(self.snp.height).multipliedBy(0.23)
        }
        
        self.cancelBtn.snp.makeConstraints { (make) in
            make.left.equalTo(self.bgImageView.snp.left).offset(16)
            make.bottom.equalTo(self.bgImageView.snp.bottom).offset(-11)
            make.height.equalTo(25)
            make.width.equalTo(40)
        }
        
        self.confirmBtn.snp.makeConstraints { (make) in
            make.right.equalTo(self.bgImageView.snp.right).offset(-16)
            make.bottom.equalTo(self.bgImageView.snp.bottom).offset(-11)
            make.height.width.equalTo(self.cancelBtn)
        }
        
        self.checkBox.snp.makeConstraints { (make) in
            make.leading.equalTo(self.cancelBtn.snp.trailing).offset(15)
            make.centerY.equalTo(self.cancelBtn.snp.centerY)
            make.trailing.equalTo(self.confirmBtn.snp.leading)
        }
        
        self.sepLine.snp.makeConstraints { (make) in
            make.leading.trailing.equalTo(self.bgImageView)
            make.bottom.equalTo(self.cancelBtn.snp.top).offset(-11)
            make.height.equalTo(1)
        }
        
        self.inputTextField.snp.makeConstraints { (make) in
            make.top.equalTo(self.titleLable.snp.bottom).offset(5)
            make.leading.equalTo(10)
            make.trailing.equalTo(-10)
            make.bottom.equalTo(self.sepLine.snp.top).offset(-5)
        }
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    func showInputTo(view:UIView) {
        self.farterView = view
        
    }
    
    
    /// 取消按钮
    @objc func cancelBtnClick() {
        debugPrint("取消",self.checkBox.selectState)
        self.inputTextField.resignFirstResponder()
    }
    
    /// 确定按钮
    @objc func confirmBtnClick() {
        
        self.checkInputValue()
        
    }
    
    
    func checkInputValue() {
        
        if editItemModel?.inforType == DeviceInforType.name {
            ///设备名称汉字
            let result = DataVerify.checkDeviceName(deviceName: self.inputTextField.text ?? "")
            if result.correct ?? false {
                self.inputTextField.resignFirstResponder()
                self.delegate?.didInputText(text: self.inputTextField.text ?? "",isSelect: self.checkBox.selectState)
            }else{
                NeatHud.showMessage(message: result.errMessage ?? "", view: self)
                fbGenerator.impactOccurred()
            }
            
        }else if editItemModel?.inforType == DeviceInforType.address{
            ///安装位置
            let result = DataVerify.checkDeviceAddress(address: self.inputTextField.text ?? "")
            if result.correct ?? false {
                self.inputTextField.resignFirstResponder()
                self.delegate?.didInputText(text: self.inputTextField.text ?? "",isSelect: self.checkBox.selectState)
            }else{
                NeatHud.showMessage(message: result.errMessage ?? "", view: self)
                fbGenerator.impactOccurred()
            }
            
        }else if editItemModel?.inforType == DeviceInforType.code ||
            editItemModel?.inforType == DeviceInforType.imei ||
            editItemModel?.inforType == DeviceInforType.imsi{
            ///数字
            let result = DataVerify.checkDeviceCode(code:self.inputTextField.text ?? "")
            if result.correct ?? false {
                self.inputTextField.resignFirstResponder()
                self.delegate?.didInputText(text: self.inputTextField.text ?? "",isSelect: self.checkBox.selectState)
            }else{
                NeatHud.showMessage(message: result.errMessage ?? "", view: self)
                fbGenerator.impactOccurred()
            }
            
        }else{
            self.inputTextField.resignFirstResponder()
            self.delegate?.didInputText(text: self.inputTextField.text ?? "",isSelect: self.checkBox.selectState)
        }
    }
    
    var firstY:CGFloat = 0
    @objc func keyboardWillShow(notify:Notification) {
        
        self.setNeedsLayout()
        //初始化控件值
        self.frame = self.farterView?.bounds ?? UIScreen.main.bounds
        
        let beginFrame = notify.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as! CGRect
        
        let endFrame = notify.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as! CGRect
        
        let time = notify.userInfo?[UIResponder.keyboardAnimationDurationUserInfoKey] as! TimeInterval
        
        let animation = notify.userInfo?[UIResponder.keyboardAnimationCurveUserInfoKey] as! Int
        
        var y = beginFrame.origin.y - endFrame.origin.y
        
        if !self.farterView!.subviews.contains(self) {
            self.farterView!.addSubview(self)
            firstY = y
            
        }else{
            
            y = firstY + y
            firstY = y
        }
        
        UIView.animate(withDuration: time, animations: {
            
            self.bgImageView.transform = CGAffineTransform.init(translationX: 0, y: -y)
            
            UIView.setAnimationCurve(UIView.AnimationCurve.init(rawValue: animation)!)
            self.bgImageView.alpha = 1
            self.bgView.alpha = 1
            
        }) { (finish) in
            self.inputTextField.becomeFirstResponder()
            

        }
        
        
    }
    
    @objc func keyboardWillHiden(notify:Notification) {
        
        let time = notify.userInfo?[UIResponder.keyboardAnimationDurationUserInfoKey] as! TimeInterval
        
        let animation = notify.userInfo?[UIResponder.keyboardAnimationCurveUserInfoKey] as! Int

        
        UIView.animate(withDuration: time, animations: {
            
            self.bgImageView.transform = .identity
            self.bgImageView.alpha = 0
            self.bgView.alpha = 0
            UIView.setAnimationCurve(UIView.AnimationCurve.init(rawValue: animation)!)
            
        }) { (finish) in
            self.removeFromSuperview()
            
        }
    }

}

extension InputPopView:UITextFieldDelegate{
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        self.checkInputValue()
            
        return true
    }
    
    
}
