//
//  DeviceDataInforTableViewCell.swift
//  NeatIoT
//
//  Created by Yang Blus on 2020/5/27.
//  Copyright © 2020 Neat. All rights reserved.
//

import UIKit

protocol DeviceDataInforTableViewCellDelegate {
    func didClickDataItem(model:DeviceDataInforModel)
}

class DeviceDataInforTableViewCell: UITableViewCell {
    
    let cellIdent = "DeviceDataCollectionViewCell"
    
    var dataSource:Array<DeviceDataInforModel> = []
    
    var delegate:DeviceDataInforTableViewCellDelegate?

    @IBOutlet weak var containCollectionView: UICollectionView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
            
        self.selectionStyle = .none
        
        self.containCollectionView.delegate = self
        self.containCollectionView.dataSource = self
        
        self.containCollectionView.register(UINib.init(nibName: "DeviceDataCollectionViewCell", bundle: Bundle.main), forCellWithReuseIdentifier: cellIdent)

    }
    
    func configerCell(models:Array<DeviceDataInforModel>) {
        
        dataSource.removeAll()
        dataSource.append(contentsOf: models)
        
        self.containCollectionView.reloadData()
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}

extension DeviceDataInforTableViewCell:UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {

        let itemCount = dataSource.count
        var itemSize:CGSize = CGSize.zero

        if itemCount >= 4 {
            
            let width = CGFloat(NormalConstant.ScreenWidth/4)

            itemSize = CGSize.init(width: width, height: NormalConstant.DeviceDataViewHeight)
        }else if itemCount > 0 && itemCount < 4{
            let width = CGFloat(NormalConstant.ScreenWidth/CGFloat(itemCount))

            itemSize = CGSize.init(width: width, height: NormalConstant.DeviceDataViewHeight)
        }
        
        return itemSize

    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
}

extension DeviceDataInforTableViewCell:UICollectionViewDelegate{

    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
}

extension DeviceDataInforTableViewCell:UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return dataSource.count
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellIdent, for: indexPath) as! DeviceDataCollectionViewCell
        
        cell.configerCell(model: dataSource[indexPath.item])
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.delegate?.didClickDataItem(model: dataSource[indexPath.item])
    }
    
    
    
    
}
