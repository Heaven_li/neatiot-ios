//
//  DeviceDataInforModel.swift
//  NeatIoT
//
//  Created by Yang Blus on 2020/5/27.
//  Copyright © 2020 Neat. All rights reserved.
//

import UIKit

class DeviceDataInforModel: NSObject {
    
    /// 是否可点击 true 可点击 false 不可点击
    var isUserClickEnable:Bool = false
    /// 内容文字
    var inforText:String = ""
    /// 标题文字
    var titleText:String = ""
    /// 内容文字颜色
    var inforTextColor:UIColor = #colorLiteral(red: 0.2063312555, green: 0.2269184493, blue: 0.2418275634, alpha: 1)
    /// 内容图片名字
    var inforImageName:String = ""
    
}
