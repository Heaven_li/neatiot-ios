//
//  DeviceDataTableViewCell.swift
//  NeatIoT
//
//  Created by Yang Blus on 2020/5/22.
//  Copyright © 2020 Neat. All rights reserved.
//

import UIKit
/// 设备数据项cell 包含信号 在线离线等
class DeviceDataTableViewCell: UITableViewCell {
    
    
    @IBOutlet weak var oneTitleLable: UILabel!
    @IBOutlet weak var oneInforLable: UILabel!
    
    @IBOutlet weak var twoTitleLable: UILabel!
    @IBOutlet weak var twoInforLable: UILabel!
    
    @IBOutlet weak var midSepImageView: UIImageView!
    
    @IBOutlet weak var thirdTitleLable: UILabel!
    @IBOutlet weak var thirdInforLable: UILabel!
    
    @IBOutlet weak var rightSepImageView: UIImageView!
  
    @IBOutlet weak var fourInforLable: UILabel!
    @IBOutlet weak var fourTitleLable: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configerCell(dataArr:Array<EleItemDataModel>) {
        
        
        if dataArr.count == 1 {
            
            let model = dataArr[0]
            oneTitleLable.text = model.itemName
            oneInforLable.text = model.itemValue! + model.itemUnit!
            
            oneTitleLable.isHidden = false
            oneInforLable.isHidden = false
            
            twoTitleLable.isHidden = true
            twoInforLable.isHidden = true
            
            thirdInforLable.isHidden = true
            thirdTitleLable.isHidden = true
            
            fourInforLable.isHidden = true
            fourTitleLable.isHidden = true
            
            midSepImageView.isHidden = true
            
            rightSepImageView.isHidden = true
            
            
        }else if dataArr.count == 2 {
            
            let model = dataArr[0]
            oneTitleLable.text = model.itemName
            oneInforLable.text = model.itemValue! + model.itemUnit!
            
            oneTitleLable.isHidden = false
            oneInforLable.isHidden = false
            
            let model_2 = dataArr[1]
            twoTitleLable.text = model_2.itemName
            twoInforLable.text = model_2.itemValue! + model_2.itemUnit!
            
            twoTitleLable.isHidden = false
            twoInforLable.isHidden = false
            
            thirdInforLable.isHidden = true
            thirdTitleLable.isHidden = true
            
            fourInforLable.isHidden = true
            fourTitleLable.isHidden = true
            
            midSepImageView.isHidden = false
            
            rightSepImageView.isHidden = true
            
            
            
        }else if dataArr.count == 3 {
            
            let model = dataArr[0]
            oneTitleLable.text = model.itemName
            oneInforLable.text = model.itemValue! + model.itemUnit!
            
            oneTitleLable.isHidden = false
            oneInforLable.isHidden = false
            
            let model_2 = dataArr[1]
            twoTitleLable.text = model_2.itemName
            twoInforLable.text = model_2.itemValue! + model_2.itemUnit!
            
            twoTitleLable.isHidden = false
            twoInforLable.isHidden = false
            
            let model_3 = dataArr[2]
            thirdTitleLable.text = model_3.itemName
            thirdInforLable.text = model_3.itemValue! + model_3.itemUnit!
            
            thirdInforLable.isHidden = false
            thirdTitleLable.isHidden = false
            
            fourInforLable.isHidden = true
            fourTitleLable.isHidden = true
            
            midSepImageView.isHidden = false
            
            rightSepImageView.isHidden = false
            
        }else if dataArr.count == 4 {
            
            let model = dataArr[0]
            oneTitleLable.text = model.itemName
            oneInforLable.text = model.itemValue! + model.itemUnit!
            
            oneTitleLable.isHidden = false
            oneInforLable.isHidden = false
            
            let model_2 = dataArr[1]
            twoTitleLable.text = model_2.itemName
            twoInforLable.text = model_2.itemValue! + model_2.itemUnit!
            
            twoTitleLable.isHidden = false
            twoInforLable.isHidden = false
            
            let model_3 = dataArr[2]
            thirdTitleLable.text = model_3.itemName
            thirdInforLable.text = model_3.itemValue! + model_3.itemUnit!
            
            thirdInforLable.isHidden = false
            thirdTitleLable.isHidden = false
            
            let model_4 = dataArr[3]
            fourTitleLable.text = model_4.itemName
            fourInforLable.text = model_4.itemValue! + model_4.itemUnit!
            
            fourInforLable.isHidden = false
            fourTitleLable.isHidden = false
            
            
            midSepImageView.isHidden = false
            rightSepImageView.isHidden = false
            
        }
        
        
    }
    
}
