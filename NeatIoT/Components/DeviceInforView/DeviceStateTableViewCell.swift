//
//  DeviceStateTableViewCell.swift
//  NeatIoT
//
//  Created by Yang Blus on 2020/5/22.
//  Copyright © 2020 Neat. All rights reserved.
//

import UIKit

@objc protocol DeviceStateTableViewCellDelegate :NSObjectProtocol{
    @objc func refreshClick()
    @objc optional func functionClick(sender:LoadingButton)
}

/// 设备状态cell
class DeviceStateTableViewCell: UITableViewCell {
    
    @IBOutlet weak var deviceIconImageView: UIImageView!
    
    @IBOutlet weak var deviceNameLable: UILabel!
    
    @IBOutlet weak var deviceStateBgView: UIView!
    
    @IBOutlet weak var deviceStateLable: UILabel!
    
    @IBOutlet weak var currentValueLable: UILabel!
    
    @IBOutlet weak var refreshImageView: UIImageView!
    
    @IBOutlet weak var functionBtn: UIButton!
    
    var delegate:DeviceStateTableViewCellDelegate?
    
    private var isRefreshing:Bool = false
    
    /// 顶部背景view
    lazy var topBgView: UIView = {
        
        let view = UIView.init(frame: CGRect.init(x: 0, y: 0, width: NormalConstant.ScreenWidth, height: NormalConstant.DeviceBasicViewHeight))
        
        let gradientLayer = CAGradientLayer.init()
        gradientLayer.colors = [#colorLiteral(red: 0.01176470588, green: 0.4, blue: 0.5921568627, alpha: 1).cgColor,#colorLiteral(red: 0.007843137255, green: 0.337254902, blue: 0.5568627451, alpha: 1).cgColor]
        gradientLayer.frame = view.bounds
        gradientLayer.startPoint = CGPoint.init(x: 0, y: 0.5)
        gradientLayer.endPoint = CGPoint.init(x: 1, y: 0.5)
        
        view.layer.addSublayer(gradientLayer)
        return view
    }()
    
    lazy var roateAnimation: CABasicAnimation = {
        let animation = CABasicAnimation.init(keyPath: "transform.rotation.z")
        animation.toValue = Double.pi * 2 * 30
        animation.duration = 30
        animation.repeatCount = 0
        return animation
    }()
    
    lazy var functionButton: LoadingButton = {
        let loadButton = LoadingButton.init()
        loadButton.delegate = self
        return loadButton
    }()

    override func awakeFromNib() {
        super.awakeFromNib()
    
        self.selectionStyle = .none
       
        self.contentView.addSubview(self.topBgView)
        self.contentView.sendSubviewToBack(self.topBgView)
        
        self.contentView.addSubview(self.functionButton)
        
        self.functionButton.snp.makeConstraints { (make) in
            make.edges.equalTo(self.functionBtn)
        }
        
        self.deviceStateBgView.layer.cornerRadius = 2
        self.deviceStateBgView.clipsToBounds = true
        
        self.currentValueLable.isHidden = true
        
        let tap = UITapGestureRecognizer.init(target: self, action: #selector(refreshBtnClick))
        self.refreshImageView.isUserInteractionEnabled = true
        self.refreshImageView.addGestureRecognizer(tap)
        
    }
    
    func configerCell(model:DeviceBasicInforModel) {
        
        isRefreshing = false
        
        let userInforModel = DataArchiver.unArchiver(key:ArchireKey.UserInfor) as! LoginResultModel
        
        let url = URL.init(string: model.iconUrl+"&token="+userInforModel.user_token)
        
        self.deviceIconImageView.kf.setImage(with: url, placeholder: UIImage.init(named: "icon_image_ph"))
        
        self.deviceNameLable.text = model.deviceName
        
        if model.deviceState == AlarmStateType.Alarm {
           //报警
            self.deviceStateBgView.backgroundColor = NormalColor.Alarm
            self.deviceStateLable.text = "报警"
        }else if model.deviceState == AlarmStateType.Fault{
           //故障
            self.deviceStateBgView.backgroundColor = NormalColor.Fault
            self.deviceStateLable.text = "故障"
        }else if model.deviceState == AlarmStateType.Fire{
           //火警
            self.deviceStateBgView.backgroundColor = NormalColor.Fire
            self.deviceStateLable.text = "火警"
        }else if model.deviceState == AlarmStateType.Normal{
           //正常
            self.deviceStateBgView.backgroundColor = NormalColor.Normal
            self.deviceStateLable.text = "正常"
        }else if model.deviceState == AlarmStateType.NotFound{
           //未知
            self.deviceStateBgView.backgroundColor = NormalColor.NotFound
            self.deviceStateLable.text = "未知"
        }else if model.deviceState == AlarmStateType.Offline{
           //离线
            self.deviceStateBgView.backgroundColor = NormalColor.Offline
            self.deviceStateLable.text = "离线"
        }
        
        if !model.currentValue.isEmpty {
            self.currentValueLable.isHidden = false
            
            if let realValue = Float(model.currentValue) {
                self.currentValueLable.text = "当前值:" + String.init(format: "%@", realValue.description)
            }else{
                self.currentValueLable.text = "当前值:" + model.currentValue
            }

            
        }else{
            self.currentValueLable.isHidden = true
        }
        
        self.functionButton.titleText = model.functionBtnText
        
        if !model.functionBtnAbleImageName.isEmpty && !model.functionBtnEnAbleImageName.isEmpty {
            
            self.functionButton.enableBgImageName = model.functionBtnAbleImageName
            self.functionButton.disableBgImageName = model.functionBtnEnAbleImageName
            
    
            self.functionButton.setBtnEnable = model.functionBtnAble
            
            self.functionButton.isHidden = false
            
            
//            self.functionBtn.setImage(UIImage.init(named: model.functionBtnAbleImageName), for: .normal)
//            self.functionBtn.setImage(UIImage.init(named: model.functionBtnEnAbleImageName), for: .disabled)
//
//            self.functionBtn.isEnabled = model.functionBtnAble
//
//            self.functionBtn.isHidden = false
            
        }else{
            
//            self.functionBtn.isHidden = true
            
            self.functionButton.isHidden = true
            
        }
        
        
    }
    
    
    @IBAction func functionBtnClick(_ sender: Any) {
        
//        self.delegate?.functionClick?()
        
    }
    
    @objc func refreshBtnClick(){
        
        if isRefreshing {
            NeatHud.showMessage(message: "正在刷新数据,请勿重复点击", view: self)
            return
        }
        self.refreshImageView.layer.add(self.roateAnimation, forKey: "roate")
        self.delegate?.refreshClick()
        isRefreshing = true
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}

extension DeviceStateTableViewCell:LoadingButtonDelegate{
    
    func didClickButton() {
        self.delegate?.functionClick?(sender: self.functionButton)
    }
    
}
