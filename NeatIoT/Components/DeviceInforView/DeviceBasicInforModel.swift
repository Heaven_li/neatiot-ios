//
//  DeviceBasicInforModel.swift
//  NeatIoT
//
//  Created by Yang Blus on 2020/5/27.
//  Copyright © 2020 Neat. All rights reserved.
//

import UIKit

class DeviceBasicInforModel: NSObject {
    
    /// 设备图片
    var iconUrl:String = ""
    
    /// 设备名称
    var deviceName:String = ""
    
    /// 设备状态
    var deviceState:Int = 0
    
    /// 当前值
    var currentValue:String = "0"
    
    /// 功能按钮是否可点击 true 可用 false 不可用
    var functionBtnAble:Bool = false

    /// 功能按钮不可用图片名称
    var functionBtnEnAbleImageName:String = ""
    
    /// 功能按钮可用图片名称
    var functionBtnAbleImageName:String = ""
    
    /// 按钮显示文字
    var functionBtnText:String = ""
    
    
    
    

}
