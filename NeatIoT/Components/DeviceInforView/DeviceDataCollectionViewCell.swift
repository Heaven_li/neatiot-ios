//
//  DeviceDataCollectionViewCell.swift
//  NeatIoT
//
//  Created by Yang Blus on 2020/5/27.
//  Copyright © 2020 Neat. All rights reserved.
//

import UIKit

class DeviceDataCollectionViewCell: UICollectionViewCell {
    
    
    @IBOutlet weak var titleLable: UILabel!
    
    @IBOutlet weak var inforLable: UILabel!
    
    @IBOutlet weak var inforImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func configerCell(model:DeviceDataInforModel) {
        
        
        if model.inforImageName.isEmpty {
            /// 展示文字
            self.titleLable.text = model.titleText
            self.inforLable.text = model.inforText
            self.inforLable.textColor = model.inforTextColor
            
            self.inforImageView.isHidden = true
            self.inforLable.isHidden = false
            
        }else if model.inforText.isEmpty {
            /// 展示图片
            self.titleLable.text = model.titleText
            self.inforImageView.image = UIImage.init(named: model.inforImageName)
            
            self.inforImageView.isHidden = false
            self.inforLable.isHidden = true
            
            
        }else{
            /// 都有展示文字
            self.titleLable.text = model.titleText
            self.inforLable.text = model.inforText
            self.inforLable.textColor = model.inforTextColor
            
            self.inforImageView.isHidden = true
            self.inforLable.isHidden = false
            
        }
        
        
    }

}
