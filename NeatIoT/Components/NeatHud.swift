//
//  NeatHud.swift
//  NeatIoT
//
//  Created by neat on 2019/10/21.
//  Copyright © 2019 Neat. All rights reserved.
//

import UIKit

class NeatHud: NSObject {
    
    static let hudBgColor = #colorLiteral(red: 0.1568627451, green: 0.1294117647, blue: 0.1137254902, alpha: 1)
    
    class func showMessage(message:String,view:UIView,finish:@escaping ()->Void) -> Void {
        
        dissmissHud(view: view)
        
        let hud = MBProgressHUD.showAdded(to: view, animated: true)
        hud.mode = MBProgressHUDMode.text
        hud.bezelView.style = .solidColor
        hud.bezelView.color = #colorLiteral(red: 0.06335258909, green: 0.05255680231, blue: 0.04625737199, alpha: 1)
        hud.label.numberOfLines = 0
        hud.label.text = message
        hud.completionBlock = finish
        hud.label.textColor = .white
        hud.hide(animated: true, afterDelay: 3)
            
        
 
    }
    
    class func showMessage(message:String,view:UIView) -> Void {
        
        dissmissHud(view: view)
        
        let hud = MBProgressHUD.showAdded(to: view, animated: true)
        hud.mode = MBProgressHUDMode.text
        hud.bezelView.style = .solidColor
        hud.bezelView.color = #colorLiteral(red: 0.06335258909, green: 0.05255680231, blue: 0.04625737199, alpha: 1)
        hud.label.numberOfLines = 0
        hud.label.text = message
        hud.label.textColor = .white
        hud.hide(animated: true, afterDelay: 3)
        
        
    }

    class func showLoading(view:UIView,message:String) -> Void {
        
        dissmissHud(view: view)
        
        let hud = MBProgressHUD.showAdded(to: view, animated: true)
        UIActivityIndicatorView.appearance(whenContainedInInstancesOf: [MBProgressHUD.self]).color = .white
        hud.mode = MBProgressHUDMode.indeterminate
        hud.bezelView.style = .solidColor
        hud.bezelView.color = #colorLiteral(red: 0.06335258909, green: 0.05255680231, blue: 0.04625737199, alpha: 1)
        hud.label.text = message
        hud.label.textColor = .white
        
    }
    
    class func dissmissHud(view:UIView) -> Void {
        
        let subViews = view.subviews
        
        for view in subViews {
            if view.isKind(of: MBProgressHUD.self) {
                let targetHud:MBProgressHUD = view as! MBProgressHUD
                targetHud.hide(animated: true)
            }
        }
            
        
        
    }

}
