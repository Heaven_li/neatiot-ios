//
//  FilterViewModel.swift
//  NeatIoT
//
//  Created by neat on 2019/11/12.
//  Copyright © 2019 Neat. All rights reserved.
//

import UIKit

class FilterViewModel: NSObject {
    
    class func initPartFilterData() -> Array<FilterItemModel> {
        
        let loginModel = DataArchiver.unArchiver(key: ArchireKey.UserInfor) as! LoginResultModel
        
        let domainItem = FilterItemModel.init()
        domainItem.filterTitle = "中心名称"
        domainItem.filterInfor = "全部"
        domainItem.filterInforId = ""
        domainItem.filterType = .DomainId
        domainItem.filterIndex = 0
        
        let entItem = FilterItemModel.init()
        entItem.filterTitle = "企业名称"
        entItem.filterInfor = "全部"
        entItem.filterInforId = ""
        entItem.filterType = .EntId
        entItem.filterIndex = 1
        
        let buildingItem = FilterItemModel.init()
        buildingItem.filterTitle = "建筑名称"
        buildingItem.filterInfor = "全部"
        buildingItem.filterInforId = ""
        buildingItem.filterType = .BuildingId
        buildingItem.filterIndex = 2
        
        let keypartItem = FilterItemModel.init()
        keypartItem.filterTitle = "部位名称"
        keypartItem.filterInfor = "全部"
        keypartItem.filterInforId = ""
        keypartItem.filterType = .KeypartId
        keypartItem.filterIndex = 3
        
        if !loginModel.user_enterprise_name.isEmpty {
            //中心企业唯一
            //中心唯一 不可选择
            domainItem.filterInfor = loginModel.user_domain_name
            domainItem.filterInforId = loginModel.user_domain_id
            domainItem.isEnable = false
            
            //企业唯一 不可选择
            entItem.filterInfor = loginModel.user_enterprise_name
            entItem.filterInforId = loginModel.user_enterprise_id
            entItem.isEnable = false
            entItem.parentId = loginModel.user_domain_id
            //设置建筑类别的父id
            buildingItem.parentId = loginModel.user_enterprise_id
    
        }else if loginModel.user_childDomain_name.isEmpty {
            //中心唯一 不可选择
            domainItem.filterInfor = loginModel.user_domain_name
            domainItem.filterInforId = loginModel.user_domain_id
            domainItem.isEnable = false
            //设置企业类别的父id
            entItem.parentId = loginModel.user_domain_id
            
        }
        
        
        return [domainItem,entItem,buildingItem,keypartItem]
        
    }
    
    class func initLogFilterData() -> Array<FilterItemModel> {
        
        let loginModel = DataArchiver.unArchiver(key: ArchireKey.UserInfor) as! LoginResultModel
        
        let domainItem = FilterItemModel.init()
        domainItem.filterTitle = "中心名称"
        domainItem.filterInfor = "全部"
        domainItem.filterInforId = ""
        domainItem.filterType = .DomainId
        domainItem.filterIndex = 0
        
        let entItem = FilterItemModel.init()
        entItem.filterTitle = "企业名称"
        entItem.filterInfor = "全部"
        entItem.filterInforId = ""
        entItem.filterType = .EntId
        entItem.filterIndex = 1
        
        let eventItem = FilterItemModel.init()
        eventItem.filterTitle = "事件类型"
        eventItem.filterInfor = "全部"
        eventItem.filterInforId = ""
        eventItem.filterType = .EventType
        eventItem.filterIndex = 2
        
        let timeItem = FilterItemModel.init()
        timeItem.filterTitle = "发生时间"
        timeItem.filterInfor = "全部"
        timeItem.filterInforId = ""
        timeItem.filterType = .AppendTime
        timeItem.filterIndex = 3
        
        if !loginModel.user_enterprise_name.isEmpty {
            //中心企业唯一
            //中心唯一 不可选择
            domainItem.filterInfor = loginModel.user_domain_name
            domainItem.filterInforId = loginModel.user_domain_id
            domainItem.isEnable = false
            
            //企业唯一 不可选择
            entItem.filterInfor = loginModel.user_enterprise_name
            entItem.filterInforId = loginModel.user_enterprise_id
            entItem.isEnable = false
            entItem.parentId = loginModel.user_domain_id
    
        }else if loginModel.user_childDomain_name.isEmpty {
            //中心唯一 不可选择
            domainItem.filterInfor = loginModel.user_domain_name
            domainItem.filterInforId = loginModel.user_domain_id
            domainItem.isEnable = false
            //设置企业类别的父id
            entItem.parentId = loginModel.user_domain_id
            
        }
        
        return [domainItem,entItem,eventItem,timeItem]
        
    }
    
    class func initHistoryFilterData() -> Array<FilterItemModel> {
        
        let frequencyItem = FilterItemModel.init()
        frequencyItem.filterTitle = "巡检频率"
        frequencyItem.filterInfor = "全部"
        frequencyItem.filterInforId = ""
        frequencyItem.filterType = .Frequency
        frequencyItem.filterIndex = 0
        
        let finishItem = FilterItemModel.init()
        finishItem.filterTitle = "完成情况"
        finishItem.filterInfor = "全部"
        finishItem.filterInforId = ""
        finishItem.filterType = .FinishState
        finishItem.filterIndex = 1
        
        return [frequencyItem,finishItem]
        
    }
    
    
    
    

}
