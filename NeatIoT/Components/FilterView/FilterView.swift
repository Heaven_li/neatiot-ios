//
//  FilterView.swift
//  NeatIoT
//
//  Created by neat on 2019/11/1.
//  Copyright © 2019 Neat. All rights reserved.
//

import UIKit



protocol FilterViewDelegate {
    
    /// 选中筛选项
    /// - Parameter model: 选中项目
    func selectItem(model:FilterItemModel)
    /// 确定
    /// - Parameter model: 筛选条件
    func confirmFilter(modelArr:Array<FilterItemModel>)
    /// 取消
    func cancelFilter()
    
}

/// 筛选控件
class FilterView: UIView {
    
    
    let FilterItemHeight:Int = 45
    let FilterControlItemHeight:Int = 50
    let AnimationDur = 0.25
    
    let FilterItemIdent = "FilterItemIdent"
    let FilterControlIdent = "FilterControlIdent"
    
    var tableViewHeight = 0
    
    /// 是否显示筛选
    var isShow:Bool = false
    
    var dataSourceArr: Array<FilterItemModel> = []
    
    var delegate: FilterViewDelegate?
    
    var dataSource: Array<FilterItemModel> {
        
        get {
            return self.dataSourceArr
        }
        set {
        
            tableViewHeight = newValue.count * FilterItemHeight + FilterControlItemHeight
            
            self.dataSourceArr.removeAll()
            self.dataSourceArr.append(contentsOf: newValue)
            //配置数据源后更新UI
            self.updateUI()
            
        }
        
    }
    
    lazy var containTableView: UITableView = {
        
        let containView = UITableView.init(frame: CGRect.zero, style: UITableView.Style.plain)
        containView.frame = CGRect.init(x: 0, y: 0, width: Int(NormalConstant.ScreenWidth), height: 0)
        containView.delegate = self
        containView.dataSource = self
        containView.isScrollEnabled = false
        containView.separatorStyle = .none
        containView.backgroundColor = .white
        containView.register(UINib.init(nibName: "FilterItemTableViewCell", bundle: Bundle.main), forCellReuseIdentifier: self.FilterItemIdent)
        containView.register(UINib.init(nibName: "FilterControlTableViewCell", bundle: Bundle.main), forCellReuseIdentifier: self.FilterControlIdent)
        return containView
        
    }()
    
    override init(frame: CGRect) {
        
        super.init(frame: frame)
        
        self.backgroundColor = NormalColor.opaBgColor
        
        self.addSubview(self.containTableView)
        
    }
    
    func updateUI() {
    
        self.containTableView.reloadData()
        
    }
    
    func showFilterView(view:UIView) {
        
        if !isShow {
            
            view.endEditing(true)
            view.addSubview(self)
            self.frame = view.bounds
            self.containTableView.frame = CGRect.init(x: 0, y: -tableViewHeight, width: Int(NormalConstant.ScreenWidth), height: tableViewHeight)
            
            UIView.animate(withDuration: AnimationDur, delay: 0, options: .curveEaseInOut, animations: {
                self.containTableView.transform = CGAffineTransform.init(translationX: 0, y: CGFloat(self.tableViewHeight))
            }) { (finish) in
                self.containTableView.frame = CGRect.init(x: 0, y: 0, width: Int(NormalConstant.ScreenWidth), height: self.tableViewHeight)
            }
            
            isShow = true
            
        }
        
    }
    func dissmisFilterView() {
        
        
        if isShow {
            UIView.animate(withDuration: AnimationDur, delay: 0, options: .curveEaseInOut, animations: {
                self.containTableView.transform = .identity
            }) { (finish) in

                self.removeFromSuperview()

            }
            isShow = false
        }
        
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        self.delegate?.cancelFilter()
        
    }

}

extension FilterView:UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if indexPath.row == dataSourceArr.count {
            return CGFloat(FilterControlItemHeight)
        }else{
            return CGFloat(FilterItemHeight)
        }
        
    }
    
}

extension FilterView:UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSourceArr.count+1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == dataSourceArr.count {
            let cell = tableView.dequeueReusableCell(withIdentifier: FilterControlIdent, for: indexPath) as! FilterControlTableViewCell
            cell.selectionStyle = .none
            cell.delegate = self
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: FilterItemIdent, for: indexPath) as! FilterItemTableViewCell
            cell.selectionStyle = .none
            cell.delegate = self
            cell.configerCell(model: dataSourceArr[indexPath.row])
            return cell
        }
        
    }
    
}


extension FilterView:FilterControlTableViewCellDelegate{
    
    func cancleClick() {
        
        self.delegate?.cancelFilter()
        
    }
    
    func confirmeClick() {
        
        self.delegate?.confirmFilter(modelArr: dataSourceArr)
        
    }
    
}

extension FilterView:FilterItemTableViewCellDelegate{
    
    func selectItem(model: FilterItemModel) {
        self.delegate?.selectItem(model: model)
    }
    
}
