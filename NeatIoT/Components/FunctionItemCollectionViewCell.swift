//
//  InspectionFunctionItemCollectionViewCell.swift
//  NeatIoT
//
//  Created by neat on 2019/12/13.
//  Copyright © 2019 Neat. All rights reserved.
//

import UIKit

class FunctionItemCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var iconImageView: UIImageView!
    
    @IBOutlet weak var notifyNum: UILabel!
    @IBOutlet weak var mainTitleLable: UILabel!
    
    @IBOutlet weak var notifyNumBg: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func configerItem(menuModel:MenuModel) -> Void {
        
    
        notifyNumBg.layer.cornerRadius = 10
        notifyNumBg.clipsToBounds = true
        notifyNum.text = "99"
        
        if menuModel.notifyNum == 0 {
            self.notifyNum.isHidden = true
            self.notifyNumBg.isHidden = true
        }else{
            self.notifyNum.isHidden = false
            self.notifyNumBg.isHidden = false
            if menuModel.notifyNum > 99{
                notifyNum.text = "99+"
            }else{
                notifyNum.text = NSString.init(format: "%d", menuModel.notifyNum) as String
            }
        }
        
        
        
        iconImageView.image = UIImage.init(named: menuModel.style_id)
        mainTitleLable.text = menuModel.name
        
    }

}
