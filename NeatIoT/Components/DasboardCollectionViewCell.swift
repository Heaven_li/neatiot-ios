//
//  DasboardCollectionViewCell.swift
//  NeatIoT
//
//  Created by neat on 2019/12/13.
//  Copyright © 2019 Neat. All rights reserved.
//

import UIKit

class DasboardCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var leftInforLable: UILabel!
    @IBOutlet weak var leftTitleLable: UILabel!
    
    
    @IBOutlet weak var midInforLable: UILabel!
    @IBOutlet weak var midTitleLable: UILabel!
    
 
    @IBOutlet weak var rightInforLable: UILabel!
    @IBOutlet weak var rightTitleLable: UILabel!
    
    @IBOutlet weak var bgImageView: UIImageView!
    
    var finishCount:CGFloat = 2
    var notFinishCount:CGFloat = 0
    var totalCount:CGFloat = 9
    let dotAngle = CGFloat(Double.pi/180*0.1)
    
    var finishRateValue:Float?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        debugPrint(self.bounds,self.center)
        
        
    }
    
    override func layoutSubviews() {
        
        debugPrint("layoutSubviews",self.bounds,self.center)
        
        drawCircle()
    }
    
    func configerCell(dashboardModel:DasboardCollectionModel) {
        
        finishRateValue = dashboardModel.midInfor
        
        let rate = (dashboardModel.midInfor ?? 0)*Float(100)
        
        let newRateStr = String.init(format: "%.1f", rate)
        
        let text = newRateStr + "%"
        
        let rateRange = NSRange.init(location: 0, length: newRateStr.count)
        
        let unitRange = NSRange.init(location: newRateStr.count, length: 1)
        
        let attriStr = NSMutableAttributedString.init(string: text)
        attriStr.addAttribute(.font, value: UIFont.systemFont(ofSize: 24), range: rateRange)
        attriStr.addAttribute(.font, value: UIFont.systemFont(ofSize: 15), range: unitRange)
        
        self.leftTitleLable.text = dashboardModel.leftTitel
        self.leftInforLable.text = String.init(format: "%.f", dashboardModel.leftInfor ?? 0)
        
        self.midTitleLable.text = dashboardModel.midTitel
        self.midInforLable.attributedText = attriStr
        
        self.rightTitleLable.text = dashboardModel.rightTitel
        self.rightInforLable.text = String.init(format: "%.f", dashboardModel.rightInfor ?? 0)
        
    }
    
    func drawCircle() {
        
        
        
        //完成度 开始 结束角度
        let startAngle = CGFloat(Double.pi/2*3)
        var endAngle = CGFloat()
        
        let lineWidth = 0.025*self.bounds.size.width
        
        let finishRate = CGFloat(finishRateValue ?? 0)

        endAngle = CGFloat(Double.pi*2*Double(finishRate) + Double(startAngle))
            
        ///背景圆环贝塞尔曲线
        let radius = CGFloat(self.bounds.size.height * 0.65/2)
        
        let bezierPath = UIBezierPath.init(arcCenter: self.center, radius: radius, startAngle: 0, endAngle: CGFloat(2*Double.pi), clockwise: true)
        
        ///背景圆环
        let circleLayer = CAShapeLayer.init()
        circleLayer.lineWidth = lineWidth
        circleLayer.strokeColor = UIColor.white.cgColor
        circleLayer.fillColor = UIColor.clear.cgColor
        circleLayer.path = bezierPath.cgPath
        circleLayer.contentsScale = UIScreen.main.scale
        
        self.bgImageView.layer.addSublayer(circleLayer)
        
        ///完成率圆环曲线
        let progressPath = UIBezierPath.init(arcCenter: self.center, radius: radius, startAngle: startAngle, endAngle: endAngle, clockwise: true)
    
        ///完成率圆环
        let progressLayer = CAShapeLayer.init()
        progressLayer.path = progressPath.cgPath
        progressLayer.strokeColor = #colorLiteral(red: 0.1504904032, green: 0.5799235106, blue: 0.8673527837, alpha: 1).cgColor
        progressLayer.lineWidth = lineWidth * 1.2
        progressLayer.fillColor = UIColor.clear.cgColor
        progressLayer.lineCap = .round
        progressLayer.contentsScale = UIScreen.main.scale
    
        self.layer.addSublayer(progressLayer)
        
        ///开始圆点
        let beginDotStartAngle = startAngle + dotAngle
        let beginDotEndAngle = startAngle + 2*dotAngle
        let beginDotPath = UIBezierPath.init(arcCenter: self.center, radius: radius, startAngle: beginDotStartAngle, endAngle: beginDotEndAngle, clockwise: true)

        let beginLayer = CAShapeLayer.init()
        beginLayer.path = beginDotPath.cgPath
        beginLayer.strokeColor = UIColor.white.cgColor
        beginLayer.lineWidth = lineWidth*0.6
        beginLayer.fillColor = UIColor.clear.cgColor
        beginLayer.lineCap = .round
        self.layer.addSublayer(beginLayer)

        ///结束圆点
        let endDotStartAngle = endAngle - 2*dotAngle
        let endDotEndAngle = endAngle - dotAngle
        let endDotPath = UIBezierPath.init(arcCenter: self.center, radius: radius, startAngle: endDotStartAngle, endAngle: endDotEndAngle, clockwise: true)

        let endLayer = CAShapeLayer.init()
        endLayer.path = endDotPath.cgPath
        endLayer.strokeColor = UIColor.white.cgColor
        endLayer.lineWidth = lineWidth*0.6
        endLayer.fillColor = UIColor.clear.cgColor
        endLayer.lineCap = .round
        self.layer.addSublayer(endLayer)
        
    }
    
    

}
