//
//  NeatSwitchButton.swift
//  NeatIoT
//
//  Created by Yang Blus on 2020/11/5.
//  Copyright © 2020 Neat. All rights reserved.
//

import UIKit


protocol NeatSwitchButtonClickDelegate {
    func switchBtnClick(isCheck:Bool)
}

class NeatSwitchButton: UIView {
    
    /// 中间圆于背景边距
    private let ovalRREdge:CGFloat = 2
    
    /// 中间圆点线条宽度
    private let ovalInsidePathWidth:CGFloat = 2
    
    /// 中间竖线条宽度
    private let ovalInsideLineWidth:CGFloat = 3
    
    /// 中间圆点颜色
    private let ovalColor:UIColor = .white
    
    /// switch button 未选中颜色
    private let unCheckColor:UIColor = #colorLiteral(red: 0.5842992823, green: 0.5960871385, blue: 0.6054529374, alpha: 1)
    
    /// switch button 选中颜色
    private let checkColor:UIColor = #colorLiteral(red: 0.02352941222, green: 0.2901960909, blue: 0.6392157078, alpha: 1)
    
    /// 控件距4个方向的边距
    private var padLeft:CGFloat = 0
    private var padRight:CGFloat = 0
    private var padTop:CGFloat = 0
    private var padBottom:CGFloat = 0
    
    /// 默认控件的宽 高
    private let buttonDefaultWidth:CGFloat = 58
    private let buttonDefaultHeight:CGFloat = 30
    
    private var isCheckOn:Bool = true
    
    var delegate:NeatSwitchButtonClickDelegate?
    
    var checkOn: Bool {
        get {
            isCheckOn
        }
        set {
            isCheckOn = newValue
            setNeedsDisplay()
        }
    }

    
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        
        for clayer in self.layer.sublayers ?? [] {
            clayer.removeFromSuperlayer()
        }
        //画文字的宽度
        var fontWidth:CGFloat = 0
        //画文字的rect
        var textRect:CGRect = .zero
        //要画的文字
        var textStr:String = ""
        //Switch圆点
        var ovalRect:CGRect = .zero
        //Switch背景颜色
        var bgColor:UIColor = unCheckColor
        //是否画圆环
        var isDrawCircle:Bool = false
        
        //计算必要的坐标值
        
        let viewWith = rect.size.width
        let viewHeight = rect.size.height
        
        if viewWith > buttonDefaultWidth && viewHeight > buttonDefaultHeight {
            
            padLeft = (viewWith - buttonDefaultWidth)/2
            padRight = padLeft
           
            padTop = (viewHeight - buttonDefaultHeight)/2
            padBottom = padTop
            
        }else if viewWith > buttonDefaultWidth {
            
            padLeft = (viewWith - buttonDefaultWidth)/2
            padRight = padLeft
            
        }else if viewHeight > buttonDefaultHeight {
            
            padTop = (viewHeight - buttonDefaultHeight)/2
            padBottom = padTop
            
        }
        
        // 背景rect
        let bgRect = CGRect.init(x: padLeft, y: padTop, width: buttonDefaultWidth, height: buttonDefaultHeight)
        // 背景rect radius
        let bgRadius = buttonDefaultHeight/2
        
        // 圆点rect 关
        let ovalWidth = buttonDefaultHeight - ovalRREdge*2
        let ovalHeight = ovalWidth
    
        if isCheckOn {
            //如果是选中
            
            //画文字
            textStr = "开"
            fontWidth = buttonDefaultWidth - (ovalRREdge*2 + ovalWidth)
            textRect = CGRect.init(x: padLeft + ovalRREdge, y: padTop, width: fontWidth, height: buttonDefaultHeight)
            
            //圆点
            let ovalXOn = padLeft + (buttonDefaultWidth - ovalWidth - ovalRREdge)
            let ovalYOn = padTop + ovalRREdge
            ovalRect = CGRect.init(x: ovalXOn, y: ovalYOn, width: ovalWidth, height: ovalHeight)
        
            //画背景
            bgColor = checkColor
            
            //画中间曲线
            isDrawCircle = false
            
        }else{
            //未选中
            
            //画文字
            textStr = "关"
            fontWidth = buttonDefaultWidth - (ovalRREdge*2 + ovalWidth)
            textRect = CGRect.init(x: ovalRREdge + ovalWidth + padLeft, y: padTop, width: fontWidth, height: buttonDefaultHeight)
            
            //圆点
            let ovalX = padLeft + ovalRREdge
            let ovalY = padTop + ovalRREdge
            ovalRect = CGRect.init(x: ovalX, y: ovalY, width: ovalWidth, height: ovalHeight)
            
            //画背景
            bgColor = unCheckColor
            
            //画中间曲线
            isDrawCircle = true
 
        }
        
        //画背景
        let bgRRectPath = UIBezierPath.init(roundedRect: bgRect, cornerRadius: bgRadius)
        
        let bgRRectLayer = CAShapeLayer.init()
        bgRRectLayer.path = bgRRectPath.cgPath
        bgRRectLayer.fillColor = bgColor.cgColor
        bgRRectLayer.contentsScale = UIScreen.main.scale
        
        self.layer.addSublayer(bgRRectLayer)
        
        //画圆点
        let roundOvalPath = UIBezierPath.init(ovalIn: ovalRect)
        
        let roundOvalLayer = CAShapeLayer.init()
        roundOvalLayer.path = roundOvalPath.cgPath
        roundOvalLayer.fillColor = ovalColor.cgColor
        roundOvalLayer.contentsScale = UIScreen.main.scale
        
        self.layer.addSublayer(roundOvalLayer)
        
        //画文字
        let textLayer = ECATextLayer.init()
        textLayer.string = textStr
        textLayer.font = UIFont.systemFont(ofSize: 5)
        textLayer.fontSize = 13
        textLayer.frame = textRect
        textLayer.alignmentMode = .center
        textLayer.contentsScale = UIScreen.main.scale
        
        self.layer.addSublayer(textLayer)
        
        if isDrawCircle {
            //画圆环
            let circleWidthOut = ovalWidth * 0.5
            let circleHeightOut = circleWidthOut
            let circleXOut = ovalRect.minX + circleWidthOut/2
            let circleYOut = ovalRect.minY + circleHeightOut/2
            let circleOutRect = CGRect.init(x: circleXOut, y: circleYOut, width: circleWidthOut, height: circleHeightOut)
            let outCirclePath = UIBezierPath.init(ovalIn: circleOutRect)

            let outCircleLayer = CAShapeLayer.init()
            outCircleLayer.path = outCirclePath.cgPath
            outCircleLayer.strokeColor = unCheckColor.cgColor
            outCircleLayer.fillColor = ovalColor.cgColor
            outCircleLayer.lineWidth = ovalInsidePathWidth
            outCircleLayer.contentsScale = UIScreen.main.scale

            self.layer.addSublayer(outCircleLayer)
            
        }else{
            //画竖线
            let startX = ovalRect.midX
            let startY = ovalRect.midY - ovalRect.height/4
            let endY = ovalRect.midY + ovalRect.height/4

            let line = UIBezierPath.init()
            line.move(to: CGPoint.init(x: startX, y: startY))
            line.addLine(to: CGPoint.init(x: startX, y: endY))


            let lineLayer = CAShapeLayer.init()
            lineLayer.path = line.cgPath
            lineLayer.strokeColor = checkColor.cgColor
            lineLayer.lineWidth = ovalInsideLineWidth
            lineLayer.lineCap = .round

            self.layer.addSublayer(lineLayer)
            
        }
        
        
    }
    

    
    override func layoutSubviews() {
        
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        isCheckOn = !isCheckOn
        
        self.delegate?.switchBtnClick(isCheck: isCheckOn)
        
//        setNeedsDisplay()
        
    }
    

}
