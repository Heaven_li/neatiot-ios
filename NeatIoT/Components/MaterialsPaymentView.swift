//
//  MaterialsPaymentView.swift
//  NeatIoT
//
//  Created by Yang Blus on 2020/5/15.
//  Copyright © 2020 Neat. All rights reserved.
//

import UIKit

protocol MaterialsPaymentViewDelegate {
    func didClickFinish()
}

class MaterialsPaymentView: UIView {
    
    let bgViewWidth:CGFloat = NormalConstant.ScreenWidth - CGFloat(40)
    let bgViewHeight:CGFloat = 46
    var delegate:MaterialsPaymentViewDelegate?
    
    lazy var bgView: UIView = {
        
        let bgView = UIView.init()
        bgView.backgroundColor = UIColor.white
        
        let maskPath = UIBezierPath.init(roundedRect: CGRect.init(x: 0, y: 0, width: self.bgViewWidth, height: self.bgViewHeight), cornerRadius: 23)
        let maskLayer = CAShapeLayer.init()
        maskLayer.path = maskPath.cgPath
        
        bgView.layer.mask = maskLayer
        
        return bgView
    }()
    
    lazy var finishBtn: UIButton = {
        let btn = UIButton.init(type: .custom)
        btn.setTitleColor(UIColor.white, for: .normal)
        btn.setTitle("完成", for: .normal)
        btn.titleLabel?.font = UIFont.systemFont(ofSize: 15)
        btn.backgroundColor = #colorLiteral(red: 0.05882352963, green: 0.3098039329, blue: 0.5137255192, alpha: 1)
        btn.addTarget(self, action: #selector(finishPayment), for: .touchUpInside)

        return btn
    }()
    
    lazy var numView: UIView = {
        let view = UIView.init()
        view.backgroundColor = #colorLiteral(red: 0.9628253579, green: 0.123109512, blue: 0.2307957411, alpha: 1)
        view.layer.cornerRadius = 10
        view.clipsToBounds = true
        return view
    }()
    
    lazy var numLable: UILabel = {
        let label = UILabel.init()
        label.font = UIFont.systemFont(ofSize: 12)
        label.textColor = .white
        label.textAlignment = .center
        return label
    }()
    
    lazy var packageImageView: UIImageView = {
        
        let imageView = UIImageView.init(image: UIImage.init(named: "consumables_package"))
        imageView.contentMode = .center

        return imageView
    }()
    
    lazy var turePayMentLable: UILabel = {
        let lable = UILabel.init()
        lable.textColor = #colorLiteral(red: 0.05882352963, green: 0.3098039329, blue: 0.5137255192, alpha: 1)
        lable.font = UIFont.boldSystemFont(ofSize: 17)

        return lable
    }()
    
    lazy var nativePayMentLable: UILabel = {
        
        let lable = UILabel.init()
        lable.textColor = #colorLiteral(red: 0.2562195659, green: 0.2683901787, blue: 0.2730521262, alpha: 1)
        lable.font = UIFont.systemFont(ofSize: 14)
       
        return lable
    }()

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.addSubview(self.bgView)
        
        self.bgView.addSubview(self.finishBtn)
        self.bgView.addSubview(self.packageImageView)
        self.packageImageView.addSubview(self.numView)
        self.numView.addSubview(self.numLable)
        self.bgView.addSubview(self.turePayMentLable)
        self.bgView.addSubview(self.nativePayMentLable)
        
        
        self.bgView.snp.makeConstraints { (make) in
            make.leading.equalTo(self).offset(10)
            make.trailing.equalTo(self).offset(-10)
            make.top.equalTo(self).offset(5)
            make.bottom.equalTo(self).offset(-5)
        }
        
        self.finishBtn.snp.makeConstraints { (make) in
            make.trailing.equalTo(self.bgView.snp.trailing)
            make.top.bottom.equalTo(self.bgView)
            make.width.equalTo(self.bgView.snp.width).multipliedBy(0.2)
        }
        
        self.packageImageView.snp.makeConstraints { (make) in
            make.leading.equalTo(self.bgView.snp.leading).offset(23)
            make.top.equalTo(self.bgView).offset(4)
            make.bottom.equalTo(self.bgView).offset(-4)
        }
        
        self.numView.snp.makeConstraints { (make) in
            
            make.top.equalTo(self.packageImageView).offset(-2)
            make.trailing.equalTo(self.packageImageView).offset(5)
            make.height.equalTo(20)
            make.width.greaterThanOrEqualTo(20)
            
        }
        
        self.numLable.snp.makeConstraints { (make) in
            make.top.equalTo(self.numView).offset(2)
            make.leading.equalTo(self.numView).offset(4)
            make.trailing.equalTo(self.numView).offset(-4)
            make.bottom.equalTo(self.numView).offset(-2)
        }
        
        self.turePayMentLable.snp.makeConstraints { (make) in
            make.leading.equalTo(self.packageImageView.snp.trailing).offset(20)
            make.centerY.equalTo(self.packageImageView)
        }
        
        self.nativePayMentLable.snp.makeConstraints { (make) in
            make.leading.equalTo(self.turePayMentLable.snp.trailing).offset(6)
            make.bottom.equalTo(self.turePayMentLable.snp.bottom)
            
        }
        
    }
    
    func reloadDataWith(materCount:Int,turePayMent:Float,nativePayMent:Float) {
        
        self.numLable.text = String(materCount)
        self.turePayMentLable.text = "¥"+String(turePayMent)
        if turePayMent == nativePayMent {
            //不限时 原价
            self.nativePayMentLable.isHidden = true
        }else{
            self.nativePayMentLable.isHidden = false
            
            let text = "¥"+String(nativePayMent)
            let range = NSRange.init(location: 0, length: text.count)
            let attributedText = NSMutableAttributedString.init(string: text)
            
            attributedText.addAttribute(.foregroundColor, value: #colorLiteral(red: 0.2562195659, green: 0.2683901787, blue: 0.2730521262, alpha: 1), range: range)
            attributedText.addAttribute(.strikethroughColor, value: #colorLiteral(red: 0.2562195659, green: 0.2683901787, blue: 0.2730521262, alpha: 1), range: range)
            attributedText.addAttribute(.strikethroughStyle, value: NSNumber(value: 1), range: range)
            
            
        
            self.nativePayMentLable.attributedText = attributedText
            
        }
        
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    @objc func finishPayment() {
        self.delegate?.didClickFinish()
    }
    
    

}
