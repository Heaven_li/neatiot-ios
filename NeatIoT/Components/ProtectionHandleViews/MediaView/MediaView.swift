//
//  MediaView.swift
//  NeatIoT
//
//  Created by Yang Blus on 2020/5/12.
//  Copyright © 2020 Neat. All rights reserved.
//

import UIKit

protocol MediaViewDelegate {
    
    func didSelectItem(model: AnnexModel)
    
    func didLongSelectItem(model:AnnexModel,indexPath: IndexPath)
    
}

class MediaView: UIView {
    
    let ReportMediaTableViewCellIdent:String = "ReportMediaTableViewCell"
    
    /// 数据源数组
    var mDataSourceArr:Array<ReportItemModel> = []
    
    var delegate:MediaViewDelegate?

    
    lazy var containTableView: UITableView = {
        
        let tableView = UITableView.init(frame: CGRect.zero, style: .plain)
        tableView.backgroundColor = UIColor.clear
        tableView.delegate = self
        tableView.dataSource = self
        tableView.separatorInset = UIEdgeInsets.init(top: 0, left: 8, bottom: 0, right: 8)
        tableView.bounces = false
        tableView.register(ReportMediaTableViewCell.self, forCellReuseIdentifier: ReportMediaTableViewCellIdent)
        
        return tableView
    }()

    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.backgroundColor = .white
        
        self.addSubview(self.containTableView)
        
        self.containTableView.snp.makeConstraints { (make) in
            
            make.top.equalTo(self).offset(0)
            make.leading.equalTo(self).offset(0)
            make.trailing.equalTo(self).offset(0)
            make.bottom.equalTo(self).offset(0)
            
        }
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func configerView(model:ReportItemModel) {
        
        self.mDataSourceArr.append(model)
        self.containTableView.reloadData()
        
    }
    
    func updataUI(model:ReportItemModel) {
        
        self.mDataSourceArr.removeAll()
        self.mDataSourceArr.append(model)
        self.containTableView.reloadData()
    }
    
    
    

}

extension MediaView:UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        let model = mDataSourceArr[indexPath.row]
        let itemWidth = (NormalConstant.ScreenWidth - 6*(4 - 1 + 2))/4
        if model.annexArr.count>=1 && model.annexArr.count <= 4 {
            return itemWidth + 6 + 36
        }else{
            return (itemWidth + 6)*2 + 36
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        0.01
    }
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView.init()
    }
    
    
}
extension MediaView:UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return mDataSourceArr.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let model = mDataSourceArr[indexPath.row]
        
        var cell = tableView.dequeueReusableCell(withIdentifier: ReportMediaTableViewCellIdent, for: indexPath) as! ReportMediaTableViewCell
        if cell == nil{
            cell = ReportMediaTableViewCell.init(style: .default, reuseIdentifier: ReportMediaTableViewCellIdent)
        }
        cell.delegate = self
        cell.configerCell(model: model)
        cell.selectionStyle = .none
        
        return cell
        
    }
}

extension MediaView:ReportMediaTableViewCellDelegate{
    
    func didSelectItem(model: AnnexModel) {
        
        self.delegate?.didSelectItem(model: model)

    }
    
    
    
    func didLongSelectItem(model:AnnexModel,indexPath: IndexPath) {
        
        self.delegate?.didLongSelectItem(model: model, indexPath: indexPath)

    }
    
}

