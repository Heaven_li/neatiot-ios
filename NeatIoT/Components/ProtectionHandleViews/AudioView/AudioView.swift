//
//  AudioView.swift
//  NeatIoT
//
//  Created by Yang Blus on 2020/5/12.
//  Copyright © 2020 Neat. All rights reserved.
//

import UIKit


protocol AudioViewDelegate {
    
    func startRecVoice()
    func endRecVoice()
    func clickRecVoice(model: AnnexModel, index: IndexPath, cell: VoiceItemTableViewCell)
    func deleteRecVoice(model: AnnexModel, index: IndexPath)
}

class AudioView: UIView {

    let VoiceTableViewCellIdent:String = "VoiceTableViewCell"
    
    /// 数据源数组
    var mDataSourceArr:Array<ReportItemModel> = []
    
    var delegate:AudioViewDelegate?
    
    lazy var containTableView: UITableView = {
        
        let tableView = UITableView.init(frame: CGRect.zero, style: UITableView.Style.plain)
        tableView.backgroundColor = UIColor.clear
        tableView.delegate = self
        tableView.dataSource = self
        tableView.separatorInset = UIEdgeInsets.init(top: 0, left: 8, bottom: 0, right: 8)
        tableView.bounces = false
        tableView.register(UINib.init(nibName: "VoiceTableViewCell", bundle: Bundle.main), forCellReuseIdentifier: VoiceTableViewCellIdent)
        
        return tableView
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.backgroundColor = .white
        
        self.addSubview(self.containTableView)
        
        self.containTableView.snp.makeConstraints { (make) in
            
            make.top.equalTo(self).offset(0)
            make.leading.equalTo(self).offset(0)
            make.trailing.equalTo(self).offset(0)
            make.bottom.equalTo(self).offset(0)
            
        }
    
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func configerView(model:ReportItemModel) {
        
        self.mDataSourceArr.append(model)
        self.containTableView.reloadData()
        
    }
    
    func updateView(model:ReportItemModel) {
        
        self.mDataSourceArr.removeAll()
        self.mDataSourceArr.append(model)
        self.containTableView.reloadData()
        
    }
    

}

extension AudioView:UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return mDataSourceArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let model = mDataSourceArr[indexPath.row]
        
        let cell = tableView.dequeueReusableCell(withIdentifier: VoiceTableViewCellIdent, for: indexPath) as! VoiceTableViewCell
        cell.delegate = self
        cell.configerCell(mode: model)
        cell.selectionStyle = .none
        return cell
        
    }
    
    
    
}
extension AudioView:UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        let model = mDataSourceArr[indexPath.row]
        
        return CGFloat(48) * CGFloat(model.annexArr.count) + 36
        
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        0.01
    }
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView.init()
    }
    
}

extension AudioView:VoiceTableViewCellDelegate{
    
    func startRecVoice() {
        self.delegate?.startRecVoice()
    }
    
    func endRecVoice() {
        self.delegate?.endRecVoice()
    }
    
    func clickRecVoice(model: AnnexModel, index: IndexPath, cell: VoiceItemTableViewCell) {
        self.delegate?.clickRecVoice(model: model, index: index, cell: cell)
    }
    
    func deleteRecVoice(model: AnnexModel, index: IndexPath) {
        self.delegate?.deleteRecVoice(model: model, index: index)
    }
    
    
    
    
}
