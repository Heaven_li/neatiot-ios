//
//  MaterialsItemTableViewCell.swift
//  NeatIoT
//
//  Created by Yang Blus on 2020/5/12.
//  Copyright © 2020 Neat. All rights reserved.
//

import UIKit

class MaterialsItemTableViewCell: UITableViewCell {
    
    
    @IBOutlet weak var firstLable: UILabel!
    
    @IBOutlet weak var secondLable: UILabel!
    
    @IBOutlet weak var thirdLable: UILabel!
    
    @IBOutlet weak var fourLable: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configerCell(model:MaterialsItemModel) {
        
        self.firstLable.text = model.firstLableText
        self.secondLable.text = model.secondLableText
        self.thirdLable.text = String.init(format: "%.1f", Float(model.thirdLableText ?? "0") ?? 0)
        self.fourLable.text = String.init(format: "%.1f", Float(model.fourLableText ?? "0") ?? 0)
        
    }
    
}
