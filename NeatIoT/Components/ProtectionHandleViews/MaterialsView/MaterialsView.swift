//
//  MaterialsView.swift
//  NeatIoT
//
//  Created by Yang Blus on 2020/5/12.
//  Copyright © 2020 Neat. All rights reserved.
//

import UIKit

class MaterialsView: UIView {
    
    let MaterialsItemTableViewCellIdent:String = "MaterialsItemTableViewCellIdent"
    
    var dataSourceArr:Array<MaterialsItemModel> = []
    
    lazy var titleLable: UILabel = {
        
        let titleLable = UILabel.init()
        titleLable.font = UIFont.systemFont(ofSize: 15)
        titleLable.textColor = NormalColor.NormalFontColor
        titleLable.text = "耗材"
        return titleLable
    }()

    
    lazy var containTableView: UITableView = {
        
        let tableView = UITableView.init(frame: CGRect.zero, style: UITableView.Style.plain)
        tableView.delegate = self
        tableView.dataSource = self
        tableView.separatorStyle = .none
        tableView.isUserInteractionEnabled = false
        tableView.register(UINib.init(nibName: "MaterialsItemTableViewCell", bundle: Bundle.main), forCellReuseIdentifier: MaterialsItemTableViewCellIdent)
        
        return tableView
    }()
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.backgroundColor = .white
        self.addSubview(self.titleLable)
        self.addSubview(self.containTableView)
        
        self.titleLable.snp.makeConstraints { (make) in
            
            make.leading.equalTo(self).offset(10)
            make.trailing.equalTo(self).offset(-10)
            make.top.equalTo(self).offset(8)
            
        }
        
        self.containTableView.snp.makeConstraints { (make) in
            
            make.top.equalTo(self.titleLable.snp.bottom).offset(8)
            make.leading.equalTo(self).offset(10)
            make.trailing.equalTo(self).offset(-10)
            make.bottom.equalTo(self).offset(-8)
            
        }
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func configerCell(model:ReportItemModel) {
        self.dataSourceArr.append(contentsOf: model.materialsArr ?? [])
        
        self.containTableView.reloadData()
    }

}

extension MaterialsView:UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 26
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.01
    }
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView.init()
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let bgView = UIView.init(frame: CGRect.init(x: 0, y: 6, width: NormalConstant.ScreenWidth, height: 32))
        bgView.backgroundColor = #colorLiteral(red: 0.95235008, green: 0.9524834752, blue: 0.9523081183, alpha: 1)
        
        let containStackView = UIStackView.init()
        containStackView.axis = .horizontal
        containStackView.distribution = .fillEqually
        containStackView.spacing = 0
        containStackView.alignment = .center
        
        let firstLable = UILabel.init()
        firstLable.font = UIFont.boldSystemFont(ofSize: 14)
        firstLable.textAlignment = .center
        firstLable.backgroundColor = #colorLiteral(red: 0.95235008, green: 0.9524834752, blue: 0.9523081183, alpha: 1)
        firstLable.textColor = #colorLiteral(red: 0.3331107199, green: 0.3331621289, blue: 0.3330945373, alpha: 1)
        firstLable.text = "耗材名称"
        
        let secondLable = UILabel.init()
        secondLable.font = UIFont.boldSystemFont(ofSize: 14)
        secondLable.textAlignment = .center
        secondLable.backgroundColor = #colorLiteral(red: 0.95235008, green: 0.9524834752, blue: 0.9523081183, alpha: 1)
        secondLable.textColor = #colorLiteral(red: 0.3331107199, green: 0.3331621289, blue: 0.3330945373, alpha: 1)
        secondLable.text = "用量"
        
        let thirdLable = UILabel.init()
        thirdLable.font = UIFont.boldSystemFont(ofSize: 14)
        thirdLable.textAlignment = .center
        thirdLable.backgroundColor = #colorLiteral(red: 0.95235008, green: 0.9524834752, blue: 0.9523081183, alpha: 1)
        thirdLable.textColor = #colorLiteral(red: 0.3331107199, green: 0.3331621289, blue: 0.3330945373, alpha: 1)
        thirdLable.text = "应付总额"
        
        let fourLable = UILabel.init()
        fourLable.font = UIFont.boldSystemFont(ofSize: 14)
        fourLable.textAlignment = .center
        fourLable.backgroundColor = #colorLiteral(red: 0.95235008, green: 0.9524834752, blue: 0.9523081183, alpha: 1)
        fourLable.textColor = #colorLiteral(red: 0.3331107199, green: 0.3331621289, blue: 0.3330945373, alpha: 1)
        fourLable.text = "实付总额"
        
        containStackView.addArrangedSubview(firstLable)
        containStackView.addArrangedSubview(secondLable)
        containStackView.addArrangedSubview(thirdLable)
        containStackView.addArrangedSubview(fourLable)
        firstLable.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(4)
            make.bottom.equalToSuperview().offset(-4)
        }
        
        bgView.addSubview(containStackView)
        containStackView.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }
        
        
        return bgView
    }
    
}
extension MaterialsView:UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        self.dataSourceArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: MaterialsItemTableViewCellIdent, for: indexPath) as! MaterialsItemTableViewCell
        cell.configerCell(model: dataSourceArr[indexPath.row])
        return cell
    }
    
    
}
