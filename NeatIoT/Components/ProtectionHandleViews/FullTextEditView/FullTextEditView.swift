//
//  FullTextEditView.swift
//  NeatIoT
//
//  Created by Yang Blus on 2020/5/12.
//  Copyright © 2020 Neat. All rights reserved.
//

import UIKit


protocol FullTextEditViewDelegate {
    
    func didInputInfor(text:String)
    
    func didClickHistoryRec()
}

class FullTextEditView: UIView {
    
    var currentModel:ReportItemModel?
    
    var delegate:FullTextEditViewDelegate?
    
    lazy var titleLable: UILabel = {
        let titleLable = UILabel.init()
        titleLable.font = UIFont.systemFont(ofSize: 15)
        
        return titleLable
    }()
    
    lazy var textView: UITextField = {
        
        let textView = UITextField.init()
        textView.font = UIFont.systemFont(ofSize: 14)
        textView.delegate = self
        textView.returnKeyType = .done
        textView.layer.borderColor = #colorLiteral(red: 0.004236431792, green: 0.3405023813, blue: 0.55645895, alpha: 1)
        textView.layer.borderWidth = 1
        textView.layer.cornerRadius = 5
        
        let leftView = UIView.init()
        leftView.frame = CGRect.init(x: 0, y: 0, width: 10, height: 10)
        textView.leftView = leftView
        textView.leftViewMode = .always
        
        return textView
    }()
    
    lazy var subFunctionBtn: UIButton = {
        let button = UIButton.init(type: .custom)
        button.addTarget(self, action: #selector(historyRec), for: .touchUpInside)
        return button
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.addSubview(self.titleLable)
        self.addSubview(self.textView)
        self.addSubview(self.subFunctionBtn)
        
        self.backgroundColor = .white
        
        self.titleLable.snp.makeConstraints { (make) in
            
            make.leading.equalTo(self).offset(10)
            make.top.equalTo(self).offset(8)
            make.height.equalTo(18)
            make.width.equalTo(NormalConstant.ScreenWidth/2.0)
            
        }
        
        self.subFunctionBtn.snp.makeConstraints { (make) in
            
            make.trailing.equalTo(self).offset(-10)
            make.top.equalTo(self).offset(8)
            make.height.equalTo(18)
            
        }
        
        self.textView.snp.makeConstraints { (make) in
            
            make.leading.equalTo(self).offset(10)
            make.trailing.equalTo(self).offset(-10)
            make.top.equalTo(self.titleLable.snp.bottom).offset(8)
            make.bottom.equalTo(self.snp.bottom).offset(-8)
            
        }
    
    }
    
    func configerView(model:ReportItemModel) {
        
        self.currentModel = model
        
         self.titleLable.text = model.titleText
        
        
        if model.isCanEdit == false {
            self.textView.isUserInteractionEnabled = false
            self.textView.text = model.inforText
        }else{
            if model.inforText?.isEmpty ?? true{
                //如果内容为空
                self.textView.isUserInteractionEnabled = true
                self.textView.placeholder = model.placeHolderStr
            }else{
                //内容不为空
                self.textView.text = model.inforText
                self.textView.isUserInteractionEnabled = false
               
            }
        }

        if nil != model.subTitleText {
            
            self.subFunctionBtn.isHidden = false
            let countStr = model.subTitleText
            let titleStr = "历史记录(" + countStr! + ")"
            let attributeStr = NSMutableAttributedString.init(string: "历史记录(" + countStr! + ")")
            attributeStr.addAttribute(.foregroundColor, value: UIColor.systemRed, range: NSRange.init(location: 5, length: countStr!.count))
            attributeStr.addAttribute(.font, value: UIFont.systemFont(ofSize: 15), range: NSRange.init(location: 0, length: titleStr.count))
            self.subFunctionBtn.setAttributedTitle(attributeStr, for: .normal)
            
        }else{
            
            self.subFunctionBtn.isHidden = true
        }
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    @objc func historyRec() {
        
        self.delegate?.didClickHistoryRec()
        
    }

}

extension FullTextEditView:UITextFieldDelegate {


    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        self.delegate?.didInputInfor(text: textField.text ?? "")
        currentModel?.inforText = textField.text
        self.textView.resignFirstResponder()
        
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        self.delegate?.didInputInfor(text: textField.text ?? "")
        currentModel?.inforText = textField.text
        self.textView.resignFirstResponder()
        
        
    }
    
}
