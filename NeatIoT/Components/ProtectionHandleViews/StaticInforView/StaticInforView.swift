//
//  StaticInforView.swift
//  NeatIoT
//
//  Created by Yang Blus on 2020/5/11.
//  Copyright © 2020 Neat. All rights reserved.
//

import UIKit

class StaticInforView: UIView {
    
    lazy var titleLable: UILabel = {
        
        let title = UILabel.init()
        title.font = UIFont.systemFont(ofSize: 15)
        title.textColor = NormalColor.NormalFontColor
        title.text = "title"
    
        return title
    }()
    
    lazy var inforLable: UILabel = {
        
        let title = UILabel.init()
        title.font = UIFont.systemFont(ofSize: 14)
        title.textColor = NormalColor.NormalFontColor
        title.numberOfLines = 0
        title.textAlignment = .right
        title.text = "infor"
        
        
        return title
    }()
    
    lazy var sepView: UIView = {
        
        let sepView = UIView.init()
        sepView.backgroundColor = NormalColor.SepLineColor
        
        return sepView
    }()
    
    func configerView(model:ReportItemModel) {
        
        self.titleLable.text = model.titleText
        self.inforLable.text = model.inforText
        
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override init(frame: CGRect) {
        
        super.init(frame: frame)
        
        self.backgroundColor = .white
        
        self.addSubview(self.titleLable)
        self.addSubview(self.inforLable)
        self.addSubview(self.sepView)
        
        self.titleLable.snp.makeConstraints { (make) in
            
            make.leading.equalTo(self).offset(10)
            make.top.equalTo(self)
            make.bottom.equalTo(self)
            make.trailing.equalTo(self.inforLable.snp.leading).offset(-10)
            
        }
        
        self.inforLable.snp.makeConstraints { (make) in
            make.trailing.equalTo(self).offset(-10)
            make.top.bottom.equalTo(self.titleLable)
            make.width.lessThanOrEqualTo(NormalConstant.ScreenWidth * 0.7)
        }
        
        self.sepView.snp.makeConstraints { (make) in
            make.bottom.equalTo(self)
            make.leading.trailing.equalTo(self)
            make.height.equalTo(0.5)
        }
        
        
    }

}
