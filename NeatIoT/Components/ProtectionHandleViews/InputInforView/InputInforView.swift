//
//  InputInforView.swift
//  NeatIoT
//
//  Created by Yang Blus on 2020/5/11.
//  Copyright © 2020 Neat. All rights reserved.
//

import UIKit

protocol InputInforViewDelegate {
    func didInputText(text:String)
}

class InputInforView: UIView {
    
    var currentModel:ReportItemModel?
    
    var delegate:InputInforViewDelegate?
    
    lazy var inputTextFiled: UITextField = {
        
        let textField = UITextField.init()
        textField.font = UIFont.systemFont(ofSize: 14)
        textField.textAlignment = .right
        textField.textColor = NormalColor.NormalFontColor
        textField.returnKeyType = .done
        textField.delegate = self
        return textField
    }()
    
    lazy var titleLable: UILabel = {
        
        let titleLable = UILabel.init()
        titleLable.font = UIFont.systemFont(ofSize: 15)
        titleLable.textColor = NormalColor.NormalFontColor
        return titleLable
    }()

    lazy var sepView: UIView = {
        
        let sepView = UIView.init()
        sepView.backgroundColor = NormalColor.SepLineColor
        
        return sepView
    }()
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.addSubview(titleLable)
        self.addSubview(inputTextFiled)
        self.addSubview(sepView)
        
        self.backgroundColor = .white
        
        self.titleLable.snp.makeConstraints { (make) in
            
            make.leading.equalTo(self).offset(10)
            make.top.equalTo(self)
            make.bottom.equalTo(self)
            
        }
        
        self.inputTextFiled.snp.makeConstraints { (make) in
            make.trailing.equalTo(self).offset(-10)
            make.top.bottom.equalTo(self.titleLable)
        }
        
        self.sepView.snp.makeConstraints { (make) in
            make.bottom.equalTo(self)
            make.leading.trailing.equalTo(self)
            make.height.equalTo(0.5)
        }
        
        
    }
    
    func configerView(model:ReportItemModel) {
        
        currentModel = model
        
        self.titleLable.text = model.titleText
        if model.inforText?.isEmpty ?? true {
            self.inputTextFiled.placeholder = model.placeHolderStr
        }else{
            self.inputTextFiled.text = model.inforText
        }
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}

extension InputInforView:UITextFieldDelegate{
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        currentModel!.inforText = textField.text
        self.inputTextFiled.resignFirstResponder()
        
        self.delegate?.didInputText(text:textField.text ?? "")
        
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        currentModel?.inforText = textField.text
        self.inputTextFiled.resignFirstResponder()
        
        self.delegate?.didInputText(text: textField.text ?? "")
    }
    
}
