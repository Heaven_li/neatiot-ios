//
//  FunctionView.swift
//  NeatIoT
//
//  Created by Yang Blus on 2020/5/11.
//  Copyright © 2020 Neat. All rights reserved.
//

import UIKit

protocol FunctionViewDelegate {
    func didClickAddMaterBtn()
}

class FunctionView: UIView {
    
    var delegate:FunctionViewDelegate?

    lazy var functionBtn: UIButton = {
        
        let functionBtn = UIButton.init()
        functionBtn.setTitle("点击添加耗材", for: .normal)
        functionBtn.titleLabel?.font = UIFont.systemFont(ofSize: 14)
        functionBtn.setTitleColor(#colorLiteral(red: 0.004236431792, green: 0.3405023813, blue: 0.55645895, alpha: 1), for: .normal)
        functionBtn.setTitleColor(#colorLiteral(red: 0.3333333433, green: 0.3333333433, blue: 0.3333333433, alpha: 1), for: .highlighted)
        functionBtn.addTarget(self, action: #selector(didClickAddMater), for: .touchUpInside)
        
        return functionBtn
    }()
    
    lazy var titleLable: UILabel = {
        
        let titleLable = UILabel.init()
        titleLable.font = UIFont.systemFont(ofSize: 15)
        titleLable.textColor = NormalColor.NormalFontColor
        titleLable.text = "添加耗材"
        return titleLable
    }()

    lazy var sepView: UIView = {
        
        let sepView = UIView.init()
        sepView.backgroundColor = NormalColor.SepLineColor
        
        return sepView
    }()
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.addSubview(titleLable)
        self.addSubview(functionBtn)
        self.addSubview(sepView)
        
        self.backgroundColor = .white
        
        self.titleLable.snp.makeConstraints { (make) in
            
            make.leading.equalTo(self).offset(10)
            make.top.equalTo(self)
            make.bottom.equalTo(self)
            
        }
        
        self.functionBtn.snp.makeConstraints { (make) in
            make.trailing.equalTo(self).offset(-10)
            make.top.bottom.equalTo(self.titleLable)
        }
        
        self.sepView.snp.makeConstraints { (make) in
            make.bottom.equalTo(self)
            make.leading.trailing.equalTo(self)
            make.height.equalTo(0.5)
        }
        
        
    }
    
    func configerView(model:ReportItemModel) {
        
        self.titleLable.text = model.titleText
        self.functionBtn.setTitle(model.functionTitleText, for: .normal)
    }
    
    @objc func didClickAddMater() {
        
        self.functionBtn.isHighlighted = true
        self.delegate?.didClickAddMaterBtn()
    }
    
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}
