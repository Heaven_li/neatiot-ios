//
//  LZTableView.swift
//  NeatIoT
//
//  Created by neat on 2019/11/8.
//  Copyright © 2019 Neat. All rights reserved.
//

import UIKit

class LZTableView: UITableView {

}

extension UITableView{
    
    func showLoadingView(_ corverImage:UIImage? = nil) {
        hidenHolderView()

        let view = PlaceHolderView.init(frame: UIScreen.main.bounds)
        view.holderStyle = .LoadingStyle
        
        if corverImage != nil {
            view.isShowLoadingCorver = true
            view.loadBgImage = corverImage!
        }
        
        self.addSubview(view)

        self.isScrollEnabled = false
    }
    
    func showEmptyView() {
        hidenHolderView()
        let view = PlaceHolderView.init(frame: self.bounds)
        view.holderStyle = .EmptyStyle
        view.desText = "空空如也 啥都没有"
        self.addSubview(view)

        self.isScrollEnabled = true
    }
    
    func showRequestFaildView(callBack:@escaping ()->Void) {
        hidenHolderView()
        let view = PlaceHolderView.init(frame: self.bounds)
        view.holderStyle = .RequestFailedStyle
        view.desText = "数据返回异常,稍后重试"
        self.addSubview(view)

        self.isScrollEnabled = false
    }
    
    func showNetErrorView(errorMessage:String,callBack:@escaping ()->Void) {
        hidenHolderView()
        let view = PlaceHolderView.init(frame: self.bounds)
        view.holderStyle = .NetErrorStyle
        view.desText = errorMessage
        self.addSubview(view)

        self.isScrollEnabled = false
    }
    
    func showRequestFaildView(errMessage:String,callBack:@escaping ()->Void) {
        hidenHolderView()
        let view = PlaceHolderView.init(frame: self.bounds)
        view.holderStyle = .RequestFailedStyle
        view.desText = errMessage
        view.reSendCallBack = callBack
        self.addSubview(view)

        self.isScrollEnabled = false
    }
    
    func hidenHolderView() {
        for view in self.subviews {
            if view is PlaceHolderView {
                view.removeFromSuperview()
            }
            
        }

        self.isScrollEnabled = true
    }
    
    
}
