//
//  FilterControlTableViewCell.swift
//  NeatIoT
//
//  Created by neat on 2019/11/1.
//  Copyright © 2019 Neat. All rights reserved.
//

import UIKit

protocol FilterControlTableViewCellDelegate {
    
    func cancleClick()
    
    func confirmeClick()
}


class FilterControlTableViewCell: UITableViewCell {
    
    
    @IBOutlet weak var cancelBtn: UIButton!
    
    @IBOutlet weak var confirmBtn: UIButton!
    
    var delegate:FilterControlTableViewCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
       
        self.cancelBtn.setTitleColor(#colorLiteral(red: 0.9764705882, green: 0.9764705882, blue: 0.9764705882, alpha: 1), for: .normal)
       
        self.confirmBtn.setTitleColor(UIColor.white, for: .normal)
        
        
        
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func cancleBtnClick(_ sender: Any) {
        self.delegate?.cancleClick()
    }
    
    @IBAction func confirmBtnClick(_ sender: Any) {
        self.delegate?.confirmeClick()
    }
    
}
