//
//  HostDeviceItemTableViewCell.swift
//  NeatIoT
//
//  Created by neat on 2019/10/31.
//  Copyright © 2019 Neat. All rights reserved.
//

import UIKit

class HostDeviceItemTableViewCell: UITableViewCell {
    
    /// 设备状态
    @IBOutlet weak var stateLable: UILabel!
    /// 设备名称
    @IBOutlet weak var deviceNameLable: UILabel!
    /// 编码
    @IBOutlet weak var codeLable: UILabel!
    /// 地址
    @IBOutlet weak var addressLable: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        stateLable.layer.cornerRadius = 5
        stateLable.clipsToBounds = true
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configerCell(itemModel:DeviceItemModel) {
        
        if itemModel.device_status != -1 {
            self.stateLable.backgroundColor = itemModel.device_status == 1 ? #colorLiteral(red: 0.6, green: 0.6, blue: 0.6, alpha: 1):#colorLiteral(red: 0.3137254902, green: 0.8392156863, blue: 0.3882352941, alpha: 1)
            self.stateLable.text = itemModel.device_status == 1 ? "离线":"在线"
        }else{
            self.stateLable.backgroundColor = #colorLiteral(red: 0.6, green: 0.6, blue: 0.6, alpha: 1)
            self.stateLable.text = "未知"
        }
        
       
        self.deviceNameLable.text = itemModel.device_name
        self.codeLable.text = "设备编码："+itemModel.device_code
        if itemModel.device_category == "device_smart_gateway" {
            self.addressLable.text = "SIM号："+itemModel.device_sim
        }else{
            self.addressLable.text = "安装位置："+itemModel.device_address
        }
        
        
    }
    
}
