//
//  DasboardCollectionModel.swift
//  NeatIoT
//
//  Created by Yang Blus on 2020/5/19.
//  Copyright © 2020 Neat. All rights reserved.
//

import UIKit

class DasboardCollectionModel: NSObject {
    
    /// 左侧标题
    var leftTitel:String?
    /// 左侧内容
    var leftInfor:Float?
    
    /// 中间标题
    var midTitel:String?
    /// 中间内容
    var midInfor:Float?
    
    /// 右侧标题
    var rightTitel:String?
    /// 右侧内容
    var rightInfor:Float?

}
