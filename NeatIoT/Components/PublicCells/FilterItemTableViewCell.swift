//
//  FilterItemTableViewCell.swift
//  NeatIoT
//
//  Created by neat on 2019/11/1.
//  Copyright © 2019 Neat. All rights reserved.
//

import UIKit

protocol FilterItemTableViewCellDelegate {
    func selectItem(model:FilterItemModel)
}

class FilterItemTableViewCell: UITableViewCell {

    @IBOutlet weak var titleLable: UILabel!
    
    @IBOutlet weak var selectInforLable: UILabel!
    
    @IBOutlet weak var arrowImageView: UIImageView!
    
    var delegate:FilterItemTableViewCellDelegate?
    
    var currentModel:FilterItemModel?
    
    
    
    override func awakeFromNib() {
        
        super.awakeFromNib()
        
        let tapGesture = UITapGestureRecognizer.init(target: self, action: #selector(tapAction))
        selectInforLable.addGestureRecognizer(tapGesture)
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }
    
    func configerCell(model:FilterItemModel) {
        
        currentModel = model
        if !model.isEnable {
            self.arrowImageView.isHidden = true
            self.selectInforLable.isUserInteractionEnabled = false
        }else{
            self.arrowImageView.isHidden = false
            self.selectInforLable.isUserInteractionEnabled = true
        }
        self.titleLable.text = model.filterTitle
        self.selectInforLable.text = model.filterInfor
        
    }
    
    @objc func tapAction() {
        self.delegate?.selectItem(model: currentModel!)
    }
    
}
