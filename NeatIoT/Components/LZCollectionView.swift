//
//  LZCollectionView.swift
//  NeatIoT
//
//  Created by Yang Blus on 2020/2/14.
//  Copyright © 2020 Neat. All rights reserved.
//

import UIKit

class LZCollectionView: UICollectionView {

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}

extension UICollectionView{
    
    
    func showLoadingView() {
        hidenHolderView()

        let view = PlaceHolderView.init(frame: UIScreen.main.bounds)
        view.holderStyle = .LoadingStyle
        self.addSubview(view)

        self.isScrollEnabled = false
    }
    
    func showEmptyView() {
        hidenHolderView()
        let view = PlaceHolderView.init(frame: self.bounds)
        view.holderStyle = .EmptyStyle
        view.desText = "空空如也,啥都没有"
        self.addSubview(view)

        self.isScrollEnabled = false
    }
    
    func showRequestFaildView(callBack:@escaping ()->Void) {
        hidenHolderView()
        let view = PlaceHolderView.init(frame: self.bounds)
        view.holderStyle = .RequestFailedStyle
        view.desText = "数据返回异常,稍后重试"
        self.addSubview(view)

        self.isScrollEnabled = false
    }
    
    func showNetErrorView(errorMessage:String,callBack:@escaping ()->Void) {
        hidenHolderView()
        let view = PlaceHolderView.init(frame: self.bounds)
        view.holderStyle = .NetErrorStyle
        view.desText = errorMessage
        view.reSendCallBack = callBack
        self.addSubview(view)

        self.isScrollEnabled = false
    }
    
    func showRequestFaildView(errMessage:String,callBack:@escaping ()->Void) {
        hidenHolderView()
        let view = PlaceHolderView.init(frame: self.bounds)
        view.holderStyle = .RequestFailedStyle
        view.desText = errMessage
        view.reSendCallBack = callBack
        self.addSubview(view)

        self.isScrollEnabled = false
    }
    
    func hidenHolderView() {
        
        for view in self.subviews {
            if view is PlaceHolderView {
                view.removeFromSuperview()
            }
            
        }

        self.isScrollEnabled = true
    }
    
    
}
