//
//  InforSelectConfigerModel.swift
//  NeatIoT
//
//  Created by neat on 2019/11/15.
//  Copyright © 2019 Neat. All rights reserved.
//

import UIKit

class InforSelectConfigerModel: NSObject {

    
    /// 展示信息类型
    var inforType:String = DeviceInforType.domain
    
    /// 设备类型
    var deviceCategory:String = DeviceModel.FireSystem.uitd_NT9009
    
    /// 展示信息title
    var inforTitle:String = ""
    
    /// 接口参数
    var apiParamet:String = ""
    
    /// 是否是筛选框唤出 true为是 要在数据中插入「全部」选项
    var isOccurFromFilter:Bool = false
    
    
    

}
