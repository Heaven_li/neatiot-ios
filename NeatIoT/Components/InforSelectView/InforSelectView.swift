//
//  InforSelectView.swift
//  NeatIoT
//
//  Created by neat on 2019/11/6.
//  Copyright © 2019 Neat. All rights reserved.
//

import UIKit




protocol InforSelectViewDelegate {
    
    /// 选中信息
    /// - Parameter model: 信息model
    func selectInfor(model:KeyValueModel)
    
}


/// 弹出状态枚举
enum ContainerViewState {
    case FullState
    case MidelState
    case DefaultState
}

class InforSelectView: UIView {
    
    let CellIdenti = "CellIdenti"
    
    let animationDur = 0.25
    
    let containViewTopSpace:CGFloat = 10
    let lhpNavBarHeight:CGFloat = 88
    let normalNavBarHeight:CGFloat = 64
    let viewHeight:CGFloat = UIScreen.main.bounds.size.height
    let viewWidth:CGFloat = UIScreen.main.bounds.size.width
    var containViewHeight:CGFloat = 0
    
    var currentConfigerModel:InforSelectConfigerModel = InforSelectConfigerModel()
    
    var containerViewState = ContainerViewState.MidelState
    
    var dataSourceArr:Array<KeyValueModel> = []
    
    var delegate:InforSelectViewDelegate?
    
    var dataSource: Array<KeyValueModel> {
        get {
            return self.dataSourceArr
        }
        set {
            self.dataSourceArr.removeAll()
            self.dataSourceArr.append(contentsOf: newValue)
            updataTableView()
        }
    }

    /// 选择器容器
    lazy var contaninView: UIView = {
        let view = UIView.init()
        view.backgroundColor = UIColor.clear
        return view
    }()
    
    /// 选择器头部view
    lazy var headImageView: UIImageView = {
        
        let imageView = UIImageView.init(image: UIImage.init(named: "select_header"))
        imageView.frame = CGRect.init(x: 0, y: 0, width: NormalConstant.ScreenWidth, height: NormalConstant.ScreenHeight * 0.1)
        return imageView
    }()
    
    /// 标题lable
    lazy var headTitleLable: UILabel = {
        let lable = UILabel.init()
        lable.textColor = #colorLiteral(red: 0.2470588235, green: 0.2588235294, blue: 0.2862745098, alpha: 1)
        lable.font = UIFont.boldSystemFont(ofSize: 18)
        
        return lable
    }()
    
    /// 数据tableview
    lazy var containTableView: LZTableView = {
        
        let tableView = LZTableView.init(frame: CGRect.zero, style: UITableView.Style.plain)
        tableView.delegate = self
        tableView.dataSource = self
        tableView.bounces = false
        tableView.backgroundColor = .white
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: self.CellIdenti)
        return tableView
        
    }()
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.backgroundColor = NormalColor.opaBgColor
        self.addSubview(self.contaninView)
        self.contaninView.addSubview(self.headImageView)
        self.headImageView.addSubview(self.headTitleLable)
        self.contaninView.addSubview(self.containTableView)
        
        self.contaninView.isUserInteractionEnabled = true
        
        let panGesture = UIPanGestureRecognizer.init(target: self, action: #selector(panGesture(gesture:)))
        panGesture.delegate = self
        self.contaninView.addGestureRecognizer(panGesture)
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func updataTableView() {
        
        if dataSourceArr.count == 0 {
            self.containTableView.showEmptyView()
        }else{
            self.containTableView.hidenHolderView()
        }
        self.containTableView.reloadData()
        
    }
    
    func showSelectView(view:UIView,model:InforSelectConfigerModel) {
        
        currentConfigerModel = model
        self.headTitleLable.text = model.inforTitle
        
        self.frame = UIScreen.main.bounds
        view.window!.addSubview(self)
        
        if NormalConstant.ISLHP{
            containViewHeight = viewHeight - lhpNavBarHeight - containViewTopSpace
        }else{
            containViewHeight = viewHeight - normalNavBarHeight - containViewTopSpace
        }
        
        self.contaninView.frame = CGRect.init(x: 0, y: viewHeight, width: viewWidth, height: containViewHeight)
        
        self.headImageView.frame = CGRect.init(x: 0, y: 0, width: viewWidth, height: viewWidth * 0.17)
        
        self.headTitleLable.frame = CGRect.init(x: 10, y: 10, width: self.headImageView.bounds.width - 20, height: self.headImageView.bounds.height - 15)
        
        self.containTableView.frame = CGRect.init(x: 0, y: self.headImageView.bounds.height, width: self.contaninView.bounds.width, height: self.contaninView.bounds.size.height - self.headImageView.bounds.height)

        UIView.animate(withDuration: animationDur, delay: 0, options: .curveEaseInOut, animations: {

            self.contaninView.transform = CGAffineTransform.init(translationX: 0, y: -self.containViewHeight * 0.5)

        }) { (finish) in
            
            self.containerViewState = .MidelState
            
            self.getData()
            
        }
        
    }
    
    func getData() {
        
        self.containTableView.showLoadingView()
        
        InforSelectViewModel.getInforListData(model: currentConfigerModel, success: { (response) in
        
            self.insertAllItem(dataArr: response.item_arr)
            
        }) { (error) in
            
            self.containTableView.showRequestFaildView {
                self.getData()
            }
            
        }
    }
    
    func insertAllItem(dataArr:Array<KeyValueModel>) {
        
        if self.currentConfigerModel.isOccurFromFilter {
            //如果是筛选 唤出 要添加全部选项
            let allItem = KeyValueModel.init()
            allItem.item_name = "全部"
            allItem.item_id = ""
            
            var newArr = Array<KeyValueModel>.init()
            newArr.append(allItem)
            newArr.append(contentsOf: dataArr)
            
            self.dataSource = newArr
            
        }else{
            
            self.dataSource = dataArr
        }
        
        
        
    
    }
    
    func dissmis() {
        
        UIView.animate(withDuration: animationDur, delay: 0, options: .curveEaseInOut, animations: {

            self.contaninView.transform = .identity
            if self.dataSourceArr.count>0{
                self.containTableView.scrollToRow(at: IndexPath.init(row: 0, section: 0), at: .top, animated: false)
            }
            
        }) { (finish) in
            
            self.dataSourceArr.removeAll()
            self.containTableView.reloadData()
            self.removeFromSuperview()
            
        }
        
    }
    
    /// 拖拽手势
    /// transform 偏移量 参数
    var trans_y:CGFloat = 0
    /// 开始拖拽时的 偏移量
    var beforeContainViewTY:CGFloat = 0
    @objc func panGesture(gesture:UIPanGestureRecognizer) {
        
        let offsetPoint = gesture.translation(in: self.contaninView)
        
        
        //根据containTableview展示状态 判断是响应上滑手势 还是下滑手势
        //如果containTableview已全屏 只能下滑
        //如果containTableview半屏 只能上滑
        if gesture.state == .began {
            //取出初始状态时 view 的Y轴变换量
            beforeContainViewTY = CGFloat(fabsf(Float(self.contaninView.transform.ty)))
        }
        
        if gesture.state == .changed {
            
            if offsetPoint.y<0 {
                
                //上滑
                //计算滑动距离
                trans_y = beforeContainViewTY - offsetPoint.y
                //设置滑动范围 超出即为全屏展示
                if trans_y < containViewHeight{
                   
                    self.contaninView.transform = CGAffineTransform.init(translationX: 0, y: -trans_y)
                    
                }else{
                    
                    beforeContainViewTY = CGFloat(fabsf(Float(self.contaninView.transform.ty)))
                    trans_y = beforeContainViewTY
                    self.containerViewState = .FullState
                    self.containTableView.isScrollEnabled = true
                }
                
            }else if offsetPoint.y>0{
                
                //下滑
                trans_y = beforeContainViewTY - offsetPoint.y
                
                self.contaninView.transform = CGAffineTransform.init(translationX: 0, y: -trans_y)
                
                if trans_y > containViewHeight * 0.5 {
                    //尚未到中间停顿位置
                    
                }else if trans_y == containViewHeight * 0.5{
                    //到达中间停顿位置 置状态
                    self.containerViewState = .MidelState
                    beforeContainViewTY = CGFloat(fabsf(Float(self.contaninView.transform.ty)))
                    
                }else if trans_y < containViewHeight * 0.25{
                    //超过中间停顿位置 自动消失
                    self.containerViewState = .DefaultState
                    beforeContainViewTY = 0
                    self.dissmis()
                    return
                }
    
            }

        }
        
        if gesture.state == .ended {

            if offsetPoint.y < 0 {

                if containerViewState == .MidelState{
                    //上滑
                    UIView.animate(withDuration: animationDur, delay: 0, options: .curveEaseInOut, animations: {
                    
                        self.contaninView.transform = CGAffineTransform.init(translationX: 0, y: -self.containViewHeight)

                    }) { (finish) in

                        self.containerViewState = .FullState
                        self.containTableView.isScrollEnabled = true
                    }
                }

            }else if offsetPoint.y > 0{
                //下滑
                if containerViewState == .FullState {

                    UIView.animate(withDuration: animationDur, delay: 0, options: .curveEaseInOut, animations: {
                        self.contaninView.transform = CGAffineTransform.init(translationX: 0, y: -self.containViewHeight*0.5)
                    }) { (finish) in
                        self.containerViewState = .MidelState
                    }

                }else if containerViewState == .MidelState{

                    self.containerViewState = .DefaultState
                    self.dissmis()

                }
            }
            
        }
    }
    
    

}

extension InforSelectView:UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        self.delegate?.selectInfor(model: dataSourceArr[indexPath.row])
        self.dissmis()
        
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.1
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView.init()
    }

}

extension InforSelectView:UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: CellIdenti, for: indexPath)
    
        cell.textLabel?.text = dataSourceArr[indexPath.row].item_name
        cell.textLabel?.font = UIFont.systemFont(ofSize: 16)
        cell.textLabel?.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.6669185773)
        cell.backgroundColor = .white
        cell.selectionStyle = .none
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSourceArr.count
    }
    
}

extension InforSelectView:UIGestureRecognizerDelegate{
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        
        if otherGestureRecognizer.view is UITableView {
            let table = otherGestureRecognizer.view as! UITableView
            let velPoint = table.panGestureRecognizer.velocity(in: table)
            
            if self.containerViewState == .FullState {
                
                if (velPoint.y > 0) {
                    // 下拉 , 只有当contentOffset.y为0时才允许多手势同时触发
                    let point = table.contentOffset;
                    
                    return point.y == 0
                }else{
                    // 上拉
                    
                    return false;
                }
            }else{
                self.containTableView.isScrollEnabled = false
                return true;
            }
            
        
        }
        
        return false
    }
}
