//
//  InforSelectViewModel.swift
//  NeatIoT
//
//  Created by neat on 2019/11/8.
//  Copyright © 2019 Neat. All rights reserved.
//

import UIKit

class InforSelectViewModel: NSObject {
    
    
    /// 获取中心列表
    class private func getDomainList(success:@escaping (_ response:IDNameResponse) -> Void,failed:@escaping (_ failed:BaseErrorResponse) -> Void) {
        NeatRequest.instance.getUserDomainList(success: { (response) in
            success(response)
        }) { (error) in
            failed(error)
        }
    }
    
    /// 获取企业列表
    /// - Parameter domainId: 中心id
    class private func getEntList(domainId:String,success:@escaping (_ response:IDNameResponse) -> Void,failed:@escaping (_ failed:BaseErrorResponse) -> Void) {
        NeatRequest.instance.getUserEntList(domainId: domainId, success: { (response) in
            success(response)
        }) { (error) in
            failed(error)
        }
    }
    
    /// 获取建筑列表
    /// - Parameter entId: 企业id
    class private func getBuildingList(entId:String,success:@escaping (_ response:IDNameResponse) -> Void,failed:@escaping (_ failed:BaseErrorResponse) -> Void) {
        NeatRequest.instance.getUserBuildingList(entId: entId, success: { (response) in
            success(response)
        }) { (error) in
            failed(error)
        }
    }
    
    /// 获取部位列表
    /// - Parameter buildingId: 建筑id
    class private func getKeypartList(buildingId:String,success:@escaping (_ response:IDNameResponse) -> Void,failed:@escaping (_ failed:BaseErrorResponse) -> Void) {
        NeatRequest.instance.getUserKeypartList(buildingId: buildingId, success: { (response) in
            success(response)
        }) { (error) in
            failed(error)
        }
    }
    
    /// 根据信息类别获取数据
    /// - Parameters:
    ///   - category: 信息类型别
    class private func getInforByCategory(category:String,model:String,success:@escaping (_ response:IDNameResponse) -> Void,failed:@escaping (_ failed:BaseErrorResponse) -> Void) {
        
        NeatRequest.instance.getInforListByCategory(category: category, model: model, success: { (response) in
            success(response)
        }) { (error) in
            failed(error)
        }
        
    }
    /// 获取巡检频率数据
    /// - Parameters:
    ///   - category: 信息类型别
    class private func getFrequency(success:@escaping (_ response:IDNameResponse) -> Void,failed:@escaping (_ failed:BaseErrorResponse) -> Void) {
        
        NeatRequest.instance.getTaskFrequency(success: { (response) in
            
            let responseModel = IDNameResponse.init()
            
            for model in response.frequenceArr!{
                
                let keyValueModel = KeyValueModel.init()
                keyValueModel.item_id = model.id!
                keyValueModel.item_name = model.rateName!
                
                responseModel.item_arr.append(keyValueModel)
            }
            
            success(responseModel)
            
        }) { (error) in
            failed(error)
        }
        
    }
    
    /// 巡检项目获取项目子类型
    /// - Parameters:
    ///   - category: 信息类型别
    class private func getInspectionSubProjectList(parames:String, success:@escaping (_ response:IDNameResponse) -> Void,failed:@escaping (_ failed:BaseErrorResponse) -> Void) {
        
        let model = DataArchiver.unArchiver(key:ArchireKey.UserInfor) as! LoginResultModel
        
        NeatRequest.instance.getProjectSubTypeList(domainId: model.user_domain_id, enterpriseId: model.user_enterprise_id, typeId: parames, childTypeName: "", success: { (response) in
            
            let responseModel = IDNameResponse.init()
            
            for model in (response.resultData?.projectList!)!{
                
                let keyValueModel = KeyValueModel.init()
                keyValueModel.item_id = model.id!
                keyValueModel.item_name = model.typeName!
                
                responseModel.item_arr.append(keyValueModel)
            }
            
            success(responseModel)
        }) { (error) in
            failed(error)
        }
        
    }
    
    
    
    
    /// 获取数据方法
    /// - Parameter model: 筛选模型
    /// - Parameter success: 成功
    /// - Parameter failed: 失败
    class func getInforListData(model:InforSelectConfigerModel,success:@escaping (_ response:IDNameResponse) -> Void,failed:@escaping (_ failed:BaseErrorResponse) -> Void) {
        
        if model.inforType == DeviceInforType.code {
            
            if model.deviceCategory == DeviceModel.WaterSystem.NT8160A_device {
                let oneModel = KeyValueModel.init()
                oneModel.item_id = "1"
                oneModel.item_name = "01"
                
                let twoModel = KeyValueModel.init()
                twoModel.item_id = "2"
                twoModel.item_name = "02"
                
                let modelArr = [oneModel,twoModel]
                
                let idNameResponse = IDNameResponse.init()
                idNameResponse.item_arr = modelArr
                
                success(idNameResponse)
            }
            
        }else if model.inforType == DeviceInforType.domain {
            getDomainList(success: success, failed: failed)
        }else if model.inforType == DeviceInforType.enterprise {
            getEntList(domainId: model.apiParamet ,success: success, failed: failed)
        }else if model.inforType == DeviceInforType.building {
            getBuildingList(entId:model.apiParamet,success: success, failed: failed)
        }else if model.inforType == DeviceInforType.keypart {
            getKeypartList(buildingId: model.apiParamet,success: success, failed: failed)
        }else if model.inforType == DeviceInforType.message_protocol {
            
            if model.deviceCategory == DeviceModel.FireSystem.uitd_NT9009{
                getInforByCategory(category: "UITD_MESSAGE_PROTOCOL",model:"", success: success, failed: failed)
            }
            
        }else if model.inforType == DeviceInforType.manufacturer {
            
            if model.deviceCategory == DeviceModel.FireSystem.uitd_NT9009
            || model.deviceCategory == DeviceModel.FireSystem.IntegratedTransmissionHost{
                getInforByCategory(category: "FIRE_SYSTEM_MANUFACTURE",model:"",  success: success, failed: failed)
            }

            
        }else if model.inforType == DeviceInforType.type {
            
            if model.deviceCategory == DeviceModel.FireSystem.uitd_NT9009 {
                getInforByCategory(category: "FIRE_UITD_CODE",model:"",  success: success, failed: failed)
            }else if model.deviceCategory == DeviceModel.FireSystem.IntegratedTransmissionHost{
                getInforByCategory(category: "FIRE_SYSTEM_CODE",model:"",  success: success, failed: failed)
            }else if model.deviceCategory == DeviceModel.WaterSystem.NeatWaterSingal{
                let pressureModel = KeyValueModel.init()
                pressureModel.item_id = "1"
                pressureModel.item_name = "液压"
                
                let liquidLevelModel = KeyValueModel.init()
                liquidLevelModel.item_id = "2"
                liquidLevelModel.item_name = "液位"
                
                let modelArr = [pressureModel,liquidLevelModel]
                
                let idNameResponse = IDNameResponse.init()
                idNameResponse.item_arr = modelArr
                
                success(idNameResponse)
            }
            
        }else if model.inforType == DeviceInforType.connection_type {
            
            if  model.deviceCategory == DeviceModel.WaterSystem.Wgw_NT8161B ||
                model.deviceCategory == DeviceModel.WaterSystem.Wgw_NT8164B{
                //万讯水设备
                getInforByCategory(category: "DEVICE_TRANSMISSION_PROTOCOL",model:"SENEXWater",  success: success, failed: failed)
            
            }else if model.deviceCategory == DeviceModel.SmartElectricitySystem.ElectricGateway_NT8127C {
                //航天长兴
                getInforByCategory(category: "DEVICE_TRANSMISSION_PROTOCOL",model:"HTCX_ELECTRIC",  success: success, failed: failed)
                
            }else if model.deviceCategory == DeviceModel.SmartElectricitySystem.ElectricGateway_NT8127A ||
                model.deviceCategory == DeviceModel.SmartElectricitySystem.ElectricGateway_NT8128A{
                //胜善
                getInforByCategory(category: "DEVICE_TRANSMISSION_PROTOCOL",model:"SUNSHY_ELECTRIC",  success: success, failed: failed)
                
            }else if model.deviceCategory == DeviceModel.GasDetector.HM710NVM{
                //海曼可燃气体
                getInforByCategory(category: "DEVICE_TRANSMISSION_PROTOCOL",model:"HM710NVMNB",  success: success, failed: failed)
                
            }else if model.deviceCategory == DeviceModel.GasDetector.WS2CG {
                //海曼可燃气体
                getInforByCategory(category: "DEVICE_TRANSMISSION_PROTOCOL",model:"WS2CGNB",  success: success, failed: failed)
                
            }else{
                getInforByCategory(category: "DEVICE_TRANSMISSION_PROTOCOL",model:"",  success: success, failed: failed)
            }
            
        }else if model.inforType == DeviceInforType.unit {
            
        }else if model.inforType == DeviceInforType.frequency {
            
            getFrequency(success: success, failed: failed)
            
        }else if model.inforType == DeviceInforType.finishRate {
            
            let finishItem = KeyValueModel.init()
            finishItem.item_id = "2"
            finishItem.item_name = "已完成"
            
            let timeOutItem = KeyValueModel.init()
            timeOutItem.item_id = "1"
            timeOutItem.item_name = "已超时"
            
            let allItem = KeyValueModel.init()
            allItem.item_id = "0"
            allItem.item_name = "全部"
            
            let idNameResponse = IDNameResponse.init()
            idNameResponse.item_arr = [finishItem,timeOutItem]
            
            success(idNameResponse)
            
        }else if model.inforType == DeviceInforType.eventTyep {
            
            let fireItem = KeyValueModel.init()
            fireItem.item_id = "fire"
            fireItem.item_name = "火警"
            
            let faultItem = KeyValueModel.init()
            faultItem.item_id = "fault"
            faultItem.item_name = "故障"
            
            let alarmItem = KeyValueModel.init()
            alarmItem.item_id = "alarm"
            alarmItem.item_name = "报警"
            
            let idNameResponse = IDNameResponse.init()
            idNameResponse.item_arr = [fireItem,faultItem,alarmItem]
            
            success(idNameResponse)
            
        }else if model.inforType == DeviceInforType.appendTime {
            
            let ThreeDayItem = KeyValueModel.init()
            ThreeDayItem.item_id = "1"
            ThreeDayItem.item_name = "三天内"
            
            let SeveDayItem = KeyValueModel.init()
            SeveDayItem.item_id = "2"
            SeveDayItem.item_name = "七天内"
            
            let oneMouthItem = KeyValueModel.init()
            oneMouthItem.item_id = "3"
            oneMouthItem.item_name = "一个月内"
            
            
            
            let idNameResponse = IDNameResponse.init()
            idNameResponse.item_arr = [ThreeDayItem,SeveDayItem,oneMouthItem]
            
            success(idNameResponse)
            
        }else if model.inforType == InspectionInforType.projectType {
            
            let ThreeDayItem = KeyValueModel.init()
            ThreeDayItem.item_id = "f446c4c6-7e4b-4911-bbba-98f480e80f0c"
            ThreeDayItem.item_name = "设备设施类巡检"
            
            let SeveDayItem = KeyValueModel.init()
            SeveDayItem.item_id = "70bd416e-3077-448f-9a8c-8ad5aee0015d"
            SeveDayItem.item_name = "消防巡检"
            
            let idNameResponse = IDNameResponse.init()
            idNameResponse.item_arr = [ThreeDayItem,SeveDayItem]
            
            success(idNameResponse)
            
        }else if model.inforType == InspectionInforType.projectSubType {
            getInspectionSubProjectList(parames: model.apiParamet, success: success, failed: failed)
        }else if model.inforType == InspectionInforType.enterprise {
            getEntList(domainId: model.apiParamet ,success: success, failed: failed)
        }else if model.inforType == InspectionInforType.building {
            getBuildingList(entId: model.apiParamet ,success: success, failed: failed)
        }else if model.inforType == InspectionInforType.keypart {
            getKeypartList(buildingId: model.apiParamet ,success: success, failed: failed)
        }else if model.inforType == DeviceInforType.frequency_query{
            
            let one_item = KeyValueModel.init()
            one_item.item_id = "8"
            one_item.item_name = "最近8小时"
            
            let two_item = KeyValueModel.init()
            two_item.item_id = "16"
            two_item.item_name = "最近16小时"
            
            let third_item = KeyValueModel.init()
            third_item.item_id = "24"
            third_item.item_name = "最近24小时"
            
            let idNameResponse = IDNameResponse.init()
            idNameResponse.item_arr = [one_item,two_item,third_item]
            
            success(idNameResponse)
        }else if model.inforType == DeviceInforType.gas_detector_fre{
            ///获取可燃气体上报周期列表
            getInforByCategory(category: "GAS_DETECTOR_REPORT_FRE", model:"", success: success, failed: failed)
            
        }
        
        
    }

}
