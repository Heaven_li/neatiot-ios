//
//  EmptyView.swift
//  NeatIoT
//
//  Created by neat on 2019/11/8.
//  Copyright © 2019 Neat. All rights reserved.
//

import UIKit

enum HolderViewStyle {
    case EmptyStyle
    case RequestFailedStyle
    case LoadingStyle
    case NetErrorStyle
}

class PlaceHolderView: UIView {
    
    private var showLoadingCorver = true
    
    private var currentStyle:HolderViewStyle = .LoadingStyle
    
    private var callBack:()->Void = {}
    
    /// view样式
    var holderStyle:HolderViewStyle {
        get {
            return currentStyle
        }
        set {
            currentStyle = newValue
            updateUI()
        }
    }
    
    /// 重试事件回调
    var reSendCallBack: ()->Void {
        get {
            return callBack
        }
        set {
            callBack = newValue
        }
    }
    
    /// 提示文字
    var desText:String{
        set{
            self.desLable.text = newValue
        }
        get{
            return self.desLable.text!
        }
    }
    
    /// 设置加载样式的背景图
    var loadBgImage: UIImage {
        get {
            return UIImage.init()
        }
        set {
            
            self.loadBgImageView.image = newValue

        }
    }
    
    var isShowLoadingCorver: Bool {
        get {
            return false
        }
        set {
            showLoadingCorver = newValue
        }
    }
    
    /// 控制按钮
    private lazy var controlBtn: UIButton = {
        let btn = UIButton.init()
        btn.setTitle("重试", for: .normal)
        btn.backgroundColor = #colorLiteral(red: 0.003921568627, green: 0.337254902, blue: 0.5647058824, alpha: 1)
        btn.layer.cornerRadius = 5
        btn.clipsToBounds = true
        btn.addTarget(self, action: #selector(reSendRequest), for: .touchUpInside)
        return btn
    }()
    
    /// 展示图片
    private lazy var iconImageView: UIImageView = {
        let imageView = UIImageView.init()
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    /// 描述文字
    private lazy var desLable: UILabel = {
        let lable = UILabel.init()
        lable.textColor = #colorLiteral(red: 0.7166229284, green: 0.7166229284, blue: 0.7166229284, alpha: 1)
        lable.textAlignment = .center
        lable.font = UIFont.systemFont(ofSize: 15)
        return lable
    }()
    
    /// 加载背景图
    lazy var loadBgImageView: UIImageView = {
        let imageView = UIImageView.init()
        imageView.image = UIImage.init(named: "table_holder_image")
        return imageView
    }()
    
    /// 加载覆盖图
    lazy var loadCorverImageView: UIImageView = {
        let corverImageView = UIImageView.init()
        corverImageView.image = UIImage.init(named: "holer_corver_image")
        corverImageView.contentMode = .scaleAspectFill
        return corverImageView
    }()
    
    override init(frame: CGRect) {
        
        super.init(frame: frame)
        
        self.backgroundColor = .white
        
        self.addSubview(self.loadBgImageView)
        self.loadBgImageView.addSubview(self.loadCorverImageView)
        
        self.addSubview(self.iconImageView)
        self.iconImageView.addSubview(self.desLable)
        self.addSubview(self.controlBtn)
        
        self.loadBgImageView.snp.makeConstraints { (make) in
            make.edges.equalTo(self)
        }
        
        self.loadCorverImageView.snp.makeConstraints { (make) in
            make.top.equalTo(self.loadBgImageView.snp.top)
            make.centerX.equalTo(self)
            make.width.equalTo(self)
            make.height.equalTo(self.loadCorverImageView.snp.width)
        }
        
        self.iconImageView.snp.makeConstraints { (make) in
            make.top.equalTo(self).offset(frame.size.height*0.1)
            make.centerX.equalTo(self)
            make.width.equalTo(self)
        }
        
        self.desLable.snp.makeConstraints { (make) in
            make.bottom.equalTo(self.iconImageView.snp.bottom)
            make.leading.trailing.equalTo(self)
        }
        
//        self.controlBtn.snp.makeConstraints { (make) in
//            make.top.equalTo(self.desLable.snp.bottom).offset(30)
//            make.left.equalTo(self).offset(20)
//            make.right.equalTo(self).offset(-20)
//            make.height.equalTo(self).multipliedBy(0.06)
//            make.centerX.equalTo(self)
//        }
        
    }
    
    func updateUI() {
        switch currentStyle {
        case .LoadingStyle:
            
            self.iconImageView.isHidden = true
            self.desLable.isHidden = true
            self.controlBtn.isHidden = true
            self.loadBgImageView.isHidden = false
            
            if showLoadingCorver {
                self.loadCorverImageView.isHidden = false
                
                let animation = CABasicAnimation.init(keyPath: "transform.translation.y")
                animation.fromValue =  -self.bounds.height
                animation.toValue =  self.bounds.height
                animation.duration = 1.5
                animation.repeatCount = 30
                animation.autoreverses = false
                self.loadCorverImageView.layer.add(animation, forKey: "load")
            }else{
                
                self.loadCorverImageView.isHidden = true
            }
            
            
            break
        case .EmptyStyle:
            self.iconImageView.image = UIImage.init(named: "empty_icon")
            self.controlBtn.isHidden = true
            self.iconImageView.isHidden = false
            self.desLable.isHidden = false
            self.loadBgImageView.isHidden = true
            self.loadCorverImageView.isHidden = true
            break
        case .RequestFailedStyle:
            self.iconImageView.image = UIImage.init(named: "load_data_failed")
            self.controlBtn.isHidden = false
            self.iconImageView.isHidden = false
            self.desLable.isHidden = false
            self.loadBgImageView.isHidden = true
            self.loadCorverImageView.isHidden = true
            break
        case .NetErrorStyle:
            self.iconImageView.image = UIImage.init(named: "net_error")
            self.controlBtn.isHidden = false
            self.iconImageView.isHidden = false
            self.desLable.isHidden = false
            self.loadBgImageView.isHidden = true
            self.loadCorverImageView.isHidden = true
            break
            
        default: break
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        callBack()
    }
    
    @objc func reSendRequest() {
        callBack()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    

}
