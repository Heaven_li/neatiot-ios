//
//  SelectPopView.swift
//  NeatIoT
//
//  Created by neat on 2019/11/14.
//  Copyright © 2019 Neat. All rights reserved.
//

import UIKit

protocol SelectPopViewDelegate {
    func didSelectItem(item:ButtonItem,editModel:EditItemModel)
}

enum ButtonItem {
    case left
    case right
}

class SelectPopView: UIView {
    
    private let animationDur:CGFloat = 0.25
    
    private var currentItemModel:EditItemModel?
    
    var delegate:SelectPopViewDelegate?
    
    var leftBtnTitle: String {
        get {
            self.leftBtn.titleLabel?.text ?? ""
        }
        set {
            self.leftBtn.setTitle(newValue, for: .normal)
        }
    }
    
    var rightBtnTitle: String {
        get {
            self.rightBtn.titleLabel?.text ?? ""
        }
        set {
            self.rightBtn.setTitle(newValue, for: .normal)
        }
    }
    
    var leftBtnImage: UIImage {
        get {
            self.leftBtn.imageView?.image ?? UIImage()
        }
        set {
            self.leftBtn.setImage(newValue, for: .normal)
        }
    }
    
    var rightBtnImage: UIImage {
        get {
            self.rightBtn.imageView?.image ?? UIImage()
        }
        set {
            self.rightBtn.setImage(newValue, for: .normal)
        }
    }
    
    private lazy var bgView: UIView = {
        let view = UIView.init()
        view.backgroundColor = NormalColor.opaBgColor
        return view
    }()
    
    private lazy var bgImageView: UIImageView = {
        let imageView = UIImageView.init()
        imageView.image = UIImage.init(named: "input_bg_image")
        imageView.isUserInteractionEnabled = true
        return imageView
    }()
    
    private lazy var leftBtn: UIButton = {
        let btn = UIButton.init(type: .custom)
        btn.setTitleColor(#colorLiteral(red: 0.1960576177, green: 0.1960916817, blue: 0.1960501969, alpha: 1), for: .normal)
        btn.addTarget(self, action: #selector(leftBtnClick(btn:)), for: .touchUpInside)
        return btn
    }()
    
    private lazy var rightBtn: UIButton = {
        let btn = UIButton.init(type: .custom)
        btn.setTitleColor(#colorLiteral(red: 0.1960576177, green: 0.1960916817, blue: 0.1960501969, alpha: 1), for: .normal)
        btn.addTarget(self, action: #selector(rightBtnClick(btn:)), for: .touchUpInside)
        return btn
    }()
    
    private lazy var lhpBottomView: UIView = {
        let view = UIView.init()
        view.backgroundColor = #colorLiteral(red: 0.9607002139, green: 0.9608381391, blue: 0.9606701732, alpha: 1)
        return view
    }()
    
    override init(frame: CGRect) {
        
        super.init(frame: frame)
        
        self.addSubview(self.bgView)
        
        self.bgView.snp.makeConstraints { (make) in
            make.edges.equalTo(self)
        }
    
        self.addSubview(self.bgImageView)
        
        self.bgImageView.addSubview(self.leftBtn)
        self.bgImageView.addSubview(self.rightBtn)
        
        if NormalConstant.ISLHP {
            self.addSubview(self.lhpBottomView)
            
            self.lhpBottomView.snp.makeConstraints { (make) in
                make.bottom.leading.trailing.equalTo(self)
                make.height.equalTo(NormalConstant.LHPBottom_Space)
            }
            
            self.bgImageView.snp.makeConstraints { (make) in
                make.leading.trailing.equalTo(self)
                make.bottom.equalTo(self.lhpBottomView.snp.top)
                make.height.equalTo(self.snp.height).multipliedBy(0.3)
            }
            self.leftBtn.snp.makeConstraints { (make) in
                make.width.equalTo(self.bgImageView.snp.width).multipliedBy(0.5)
                make.top.bottom.equalTo(self.bgImageView)
                make.leading.equalTo(self.bgImageView)
            }
            self.rightBtn.snp.makeConstraints { (make) in
                make.width.equalTo(self.leftBtn.snp.width)
                make.top.bottom.equalTo(self.bgImageView)
                make.trailing.equalTo(self.bgImageView)
            }
            
        }else{
            
            self.bgImageView.snp.makeConstraints { (make) in
                make.height.equalTo(self.snp.height).multipliedBy(0.3)
                make.bottom.leading.trailing.equalTo(self)
            }
            self.leftBtn.snp.makeConstraints { (make) in
                make.width.equalTo(self.bgImageView.snp.width).multipliedBy(0.5)
                make.top.bottom.equalTo(self.bgImageView)
                make.leading.equalTo(self.bgImageView)
            }
            self.rightBtn.snp.makeConstraints { (make) in
                make.width.equalTo(self.leftBtn.snp.width)
                make.top.bottom.equalTo(self.bgImageView)
                make.trailing.equalTo(self.bgImageView)
            }
        }
        
    }
    
    func show(view:UIView,model:EditItemModel) {
        
        currentItemModel = model
        
        self.frame = view.bounds
        view.addSubview(self)
        
        let viewHeight = NormalConstant.ScreenHeight - NormalConstant.NavBarHeight - NormalConstant.BottomSafeSpace
        let bgImageViewHeight = viewHeight * 0.3
        let fromValue = viewHeight + bgImageViewHeight*0.5
        let toValue = fromValue - bgImageViewHeight
        
        let animation = CABasicAnimation(keyPath: "position.y")
        animation.fromValue = fromValue
        animation.toValue = toValue
        animation.duration = CFTimeInterval(animationDur)
        self.bgImageView.layer.add(animation, forKey: "pop")
        
    }
    
    func dissmiss() {

        let viewHeight = NormalConstant.ScreenHeight - NormalConstant.NavBarHeight - NormalConstant.BottomSafeSpace
        let bgImageViewHeight = viewHeight * 0.3
        let fromValue = viewHeight - bgImageViewHeight*0.5
        let toValue = fromValue + bgImageViewHeight

        let animation = CABasicAnimation(keyPath: "position.y")
        animation.fromValue = fromValue
        animation.toValue = toValue
        animation.duration = CFTimeInterval(animationDur)
        self.bgImageView.layer.add(animation, forKey: "dissmiss")
        self.removeFromSuperview()
        
    }
    
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    @objc private func leftBtnClick(btn:UIButton) {
        
        self.delegate?.didSelectItem(item: .left,editModel: currentItemModel!)
        self.dissmiss()
        
    }
    @objc private func rightBtnClick(btn:UIButton) {
        
        self.delegate?.didSelectItem(item: .right,editModel: currentItemModel!)
        self.dissmiss()
        
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.dissmiss()
    }
    
    
    

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}

