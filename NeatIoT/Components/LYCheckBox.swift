//
//  LYCheckBox.swift
//  NeatIoT
//
//  Created by neat on 2019/11/14.
//  Copyright © 2019 Neat. All rights reserved.
//

import UIKit

class LYCheckBox: UIView {
    
    /// 设置选中状态
    var selectState: Bool {
        get{
            self.iconImageView.isHighlighted
        }
        set{
            self.iconImageView.isHighlighted = newValue
        }
    }

    /// 文字
    var text: String {
        get {
            self.textLable.text ?? ""
        }
        set {
            self.textLable.text = newValue
        }
    }
    
    /// 正常状态图片
    var iconNormal: UIImage{
        get {
            self.iconImageView.image ?? UIImage()
        }
        set {
            self.iconImageView.image = newValue
        }
    }
    
    /// 高亮图片
    var highlight: UIImage {
        get {
            self.iconImageView.highlightedImage ?? UIImage()
        }
        set {
            self.iconImageView.highlightedImage = newValue
        }
    }
    
    
    private lazy var textLable: UILabel = {
        let lable = UILabel.init()
        lable.textColor = #colorLiteral(red: 0.5372549295, green: 0.5372549295, blue: 0.5372549295, alpha: 1)
        lable.font = UIFont.systemFont(ofSize: 14)
        return lable
    }()
    
    private lazy var iconImageView: UIImageView = {
        let imageView = UIImageView.init()
        return imageView
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.isUserInteractionEnabled = true
        
        /// 添加点击手势
        let tapGesture = UITapGestureRecognizer.init(target: self, action: #selector(tapClick(tap:)))
        self.addGestureRecognizer(tapGesture)
        
        self.addSubview(self.iconImageView)
        self.addSubview(self.textLable)
        
        self.iconImageView.snp.makeConstraints { (make) in
            make.leading.equalTo(self)
            make.centerY.equalTo(self)
            make.width.height.equalTo(16)
        }
        
        self.textLable.snp.makeConstraints { (make) in
            make.centerY.equalTo(self.iconImageView.snp.centerY)
            make.leading.equalTo(self.iconImageView.snp.trailing).offset(4)
            make.trailing.equalTo(self)
            make.height.equalTo(self)
        }
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    @objc func tapClick(tap:UITapGestureRecognizer) {
        
        self.iconImageView.isHighlighted = !self.selectState
        
    }
    
    func resetState() {
        self.selectState = false

    }
    
    
    

}
