//
//  NormalFilterIItemModel.swift
//  NeatIoT
//
//  Created by neat on 2019/12/17.
//  Copyright © 2019 Neat. All rights reserved.
//

import UIKit

class NormalFilterIItemModel: NSObject {
    
    /// 筛选项目名称
    var filterName:String?
    
    /// 筛选项目id
    var filterID:String?
    
    /// 筛选项目是否选中
    var isSelect:Bool = false

}
