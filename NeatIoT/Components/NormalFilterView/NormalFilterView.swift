//
//  NormalFilterView.swift
//  NeatIoT
//
//  Created by neat on 2019/12/17.
//  Copyright © 2019 Neat. All rights reserved.
//

import UIKit

protocol NormalFilterViewDelegate {
    func didSelectFilterItme(model:NormalFilterIItemModel)
}

class NormalFilterView: UIView {
    
    let FilterItemHeight:Int = 45
    let AnimationDur = 0.25
    var currentSelectImage:UIImageView?
    var viewHight = 0
    var delegate:NormalFilterViewDelegate?
    
    private var dataSourceArr: Array<NormalFilterIItemModel> = []
    
    var dataSource: Array<NormalFilterIItemModel> {
        get {
            return dataSourceArr
        }
        set {
            dataSourceArr = newValue
        }
    }
    
    
    lazy var containStackView: UIStackView = {
         let containStackView = UIStackView.init()
               containStackView.axis = .vertical
               containStackView.alignment = .fill
               containStackView.distribution = .equalSpacing
        return containStackView
    }()
    
    lazy var bgView: UIView = {
        let bgView = UIView.init()
        bgView.backgroundColor = .white
        return bgView
    }()
   
    
    override init(frame: CGRect) {
        
        super.init(frame: frame)
        
        
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        
        configerUI()
        viewHight = FilterItemHeight * dataSourceArr.count
        
    }
    
    func configerUI() {
        
        for view in self.containStackView.subviews{
            view.removeFromSuperview()
        }
        for view in self.subviews{
            view.removeFromSuperview()
        }
       
        self.addSubview(containStackView)
        containStackView.snp.makeConstraints { (make) in
            make.top.equalTo(self.snp.top)
            make.leading.equalTo(self.snp.leading).offset(10)
            make.trailing.equalTo(self.snp.trailing).offset(-10)
        }
        
        for index in 0..<dataSourceArr.count {
            
            let model = dataSourceArr[index]
            
            let itemContainStackView = UIStackView.init()
            itemContainStackView.axis = .horizontal
            itemContainStackView.alignment = .center
            itemContainStackView.distribution = .equalSpacing
            itemContainStackView.isUserInteractionEnabled = true
            itemContainStackView.tag = 10086+index
            
            let titleLable = UILabel.init()
            titleLable.text = model.filterName
            
            let selectImage = UIImageView.init(image: UIImage.init(named: "filter_select"))
            
            itemContainStackView.addArrangedSubview(titleLable)
            itemContainStackView.addArrangedSubview(selectImage)
            
            selectImage.isHidden = !model.isSelect
            if model.isSelect {
                currentSelectImage = selectImage
            }
            
            selectImage.snp.makeConstraints { (make) in
                make.width.equalTo(18)
                make.height.equalTo(18)
            }
            
            containStackView.addArrangedSubview(itemContainStackView)
            
            itemContainStackView.snp.makeConstraints { (make) in
                make.leading.trailing.equalTo(containStackView)
                make.height.equalTo(46)
            }
            
            let sepLineView = UIView.init()
            sepLineView.backgroundColor = #colorLiteral(red: 0.7725490196, green: 0.7725490196, blue: 0.7725490196, alpha: 1)
            if index != dataSourceArr.count - 1 {
                containStackView.addArrangedSubview(sepLineView)
                sepLineView.snp.makeConstraints { (make) in
                    make.height.equalTo(0.5)
                }
            }
            
            let tapGesture = UITapGestureRecognizer.init(target: self, action: #selector(tapGesture(tapGesture:)))
            itemContainStackView.addGestureRecognizer(tapGesture)
    
        }
        
        
        self.addSubview(bgView)
        self.sendSubviewToBack(bgView)
        
        bgView.snp.makeConstraints { (make) in
            make.top.equalTo(containStackView.snp.top)
            make.leading.equalTo(containStackView.snp.leading).offset(-10)
            make.trailing.equalTo(containStackView.snp.trailing).offset(10)
            make.bottom.equalTo(containStackView.snp.bottom)
        }
        
        let opBgview = UIView.init()
        opBgview.backgroundColor = NormalColor.opaBgColor
        self.addSubview(opBgview)
        self.sendSubviewToBack(opBgview)
        opBgview.snp.makeConstraints { (make) in
            make.edges.equalTo(self)
        }
        let tap = UITapGestureRecognizer.init(target: self, action: #selector(dissmis))
        opBgview.addGestureRecognizer(tap)
        
    }

    func updateSelectState(index:Int) {
        
        currentSelectImage?.isHidden = true
        
        for i in 0..<dataSourceArr.count {

            let model = dataSourceArr[i]
            if i == index-10086 {
                model.isSelect = true
            }else{
                model.isSelect = false
            }
        }
        
        let view = self.viewWithTag(index)
        
        for subView in view!.subviews {
            if subView is UIImageView {
                subView.isHidden = false
                currentSelectImage = subView as! UIImageView
            }
        }
        
        self.delegate?.didSelectFilterItme(model: dataSourceArr[index - 10086])
        
        dissmis()
        
        
    }
    
    @objc func dissmis() {
        self.removeFromSuperview()
        
        for view in self.subviews{
            view.removeFromSuperview()
        }
        
    }
    
    
    @objc func tapGesture(tapGesture:UITapGestureRecognizer) {
        
        let index = tapGesture.view!.tag
        
        updateSelectState(index: index)
        
    }


}

