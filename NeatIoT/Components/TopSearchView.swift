//
//  TopSearchView.swift
//  NeatIoT
//
//  Created by neat on 2019/10/29.
//  Copyright © 2019 Neat. All rights reserved.
//

import UIKit

@objc protocol TopSearchViewDelegate {
    
    /// 搜索
    /// - Parameter keyword: 关键词
    @objc func doSearchWith(keyword:String)
    
    /// 点击扫码
    @objc optional func scanQRClick()
    
}

class TopSearchView: UIView {
    
    let vSpace:CGFloat = 8
    let hSpace:CGFloat = 20
    
    
    var viewHeight:CGFloat?
    var viewWidth:CGFloat?
    var cornerRadius:CGFloat?
    
    var delegate:TopSearchViewDelegate?
    
    private var isHaveScan = false
    
    var hasScan: Bool {
        get {
            isHaveScan
        }
        set {
            isHaveScan = newValue
        }
    }
    
    /// 渐变背景色
    lazy var bgGradientLayer: CAGradientLayer = {
        let gradientLayer = CAGradientLayer.init()
        gradientLayer.colors = [#colorLiteral(red: 0.01176470588, green: 0.4, blue: 0.5921568627, alpha: 1).cgColor,#colorLiteral(red: 0.007843137255, green: 0.337254902, blue: 0.5568627451, alpha: 1).cgColor]
        gradientLayer.frame = self.bounds
        gradientLayer.startPoint = CGPoint.init(x: 0, y: 0.5)
        gradientLayer.endPoint = CGPoint.init(x: 1, y: 0.5)
        
        return gradientLayer
    }()
    
    /// 输入区渐变背景色
    lazy var inputGradientLayer: CAGradientLayer = {
        let gradLayer = CAGradientLayer.init()
        gradLayer.colors = [#colorLiteral(red: 0.01960784314, green: 0.3529411765, blue: 0.5921568627, alpha: 1).cgColor,#colorLiteral(red: 0.003921568627, green: 0.1607843137, blue: 0.3803921569, alpha: 1).cgColor]
        gradLayer.startPoint = CGPoint.init(x: 0, y: 0.5)
        gradLayer.endPoint = CGPoint.init(x: 1, y: 0.5)
        gradLayer.cornerRadius = self.cornerRadius!
//        gradLayer.frame = CGRect.init(x: 0, y: 0, width: viewWidth! - self.vSpace*3 , height: viewHeight! - self.vSpace*2)
        return gradLayer
    }()
    
    /// 输入区背景view
    lazy var radioBgView: UIView = {
        
        let bgview = UIView.init()
        bgview.layer.cornerRadius = cornerRadius!
        bgview.layer.borderColor = #colorLiteral(red: 0.02021185461, green: 0.5290297954, blue: 0.759876523, alpha: 1).cgColor
        bgview.layer.borderWidth = 1
        
        return bgview
    }()
    
    /// 输入框
    lazy var textfield: UITextField = {

        let textField = UITextField.init()
        textField.borderStyle = .none
        textField.attributedPlaceholder = NSAttributedString.init(string: "在此输入搜索条件", attributes: [.foregroundColor:#colorLiteral(red: 0.02021185461, green: 0.5290297954, blue: 0.759876523, alpha: 1),.font:UIFont.systemFont(ofSize: 15)])
        textField.tintColor = UIColor.white
        textField.textColor = UIColor.white
        textField.delegate = self
        textField.returnKeyType = .search
//        textField.addTarget(self, action: #selector(textFieldValueChanged), for: .valueChanged)
        
        let clearBtn = UIButton.init(type:.custom)
        clearBtn.frame = CGRect.init(x: 0, y: 0, width: 40, height: 40)
        clearBtn.setImage(UIImage.init(named: "search_clear"), for: .normal)
        clearBtn.addTarget(self, action: #selector(clearTextField), for: .touchUpInside)
        textField.rightView = clearBtn
        
        return textField
        
    }()
    
    lazy var searchIcon: UIImageView = {
    
        let tapGesture = UITapGestureRecognizer.init()
        tapGesture.addTarget(self, action:#selector(searchAction))
        
        let imageView = UIImageView.init(image: UIImage.init(named: "search_icon"))
        imageView.isUserInteractionEnabled = true
        imageView.contentMode = .center
        imageView.addGestureRecognizer(tapGesture)
        
        return imageView
    }()
    
    lazy var scanIcon: UIImageView = {
        
        let tapGesture = UITapGestureRecognizer.init()
        tapGesture.addTarget(self, action:#selector(scanQRClick))
        
        let imageView = UIImageView.init(image: UIImage.init(named: "scan_qr"), highlightedImage: nil)
        imageView.contentMode = .center
        imageView.isUserInteractionEnabled = true
        imageView.addGestureRecognizer(tapGesture)
        
        return imageView
    }()

    
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
        
    }
    
    
    override init(frame: CGRect) {
        
        super.init(frame: frame)
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        
        viewHeight = self.bounds.size.height
        viewWidth = self.bounds.size.width
        cornerRadius = (self.viewHeight! - 2*vSpace)/2.0
        configerUI()
        
    }
    
    func configerUI() -> Void {
        
        self.layer.addSublayer(self.bgGradientLayer)
        self.radioBgView.layer.addSublayer(self.inputGradientLayer)
        self.addSubview(self.radioBgView)
        
        if isHaveScan {
            
            self.inputGradientLayer.frame = CGRect.init(x: 0, y: 0, width: viewWidth! - self.hSpace*3 - 30, height: viewHeight! - self.vSpace*2)
            
            //有扫描icon
            self.radioBgView.addSubview(self.textfield)
            self.radioBgView.addSubview(self.searchIcon)
            self.addSubview(self.scanIcon)
            
            self.scanIcon.snp.makeConstraints { (make) in
                make.centerY.equalTo(self.snp.centerY)
                make.leading.equalTo(self.snp.leading).offset(hSpace)
                make.top.equalTo(self.radioBgView).offset(5)
                make.bottom.equalTo(self.radioBgView).offset(-5)
                make.width.equalTo(self.scanIcon.snp.height)
            }
            
            self.radioBgView.snp.makeConstraints { (make) in
                make.top.equalTo(self).offset(vSpace)
                make.bottom.equalTo(self).offset(-vSpace)
                make.left.equalTo(self.scanIcon.snp.right).offset(hSpace)
                make.right.equalTo(self).offset(-hSpace)
            }
            
            self.textfield.snp.makeConstraints { (make) in
                make.top.equalTo(self.radioBgView).offset(5)
                make.bottom.equalTo(self.radioBgView).offset(-5)
                make.left.equalTo(self.radioBgView.snp.left).offset(hSpace)
                make.right.equalTo(self.searchIcon.snp.left)
            }
            
            self.searchIcon.snp.makeConstraints { (make) in
                make.centerY.equalTo(self.radioBgView)
                make.right.equalTo(self.radioBgView.snp.right).offset(-8)
                make.height.equalTo(self.radioBgView).multipliedBy(1)
                make.width.equalTo(self.searchIcon.snp.height)
            }
            
        }else{
            
            //没有扫描icon
            self.inputGradientLayer.frame = CGRect.init(x: 0, y: 0, width: viewWidth! - self.hSpace*2, height: viewHeight! - self.vSpace*2)
            
            self.radioBgView.snp.makeConstraints { (make) in
                make.top.equalTo(self).offset(vSpace)
                make.bottom.equalTo(self).offset(-vSpace)
                make.left.equalTo(self).offset(hSpace)
                make.right.equalTo(self).offset(-hSpace)
            }
            
            self.radioBgView.addSubview(self.textfield)
            self.radioBgView.addSubview(self.searchIcon)
            
            self.textfield.snp.makeConstraints { (make) in
                make.top.equalTo(self.radioBgView).offset(5)
                make.bottom.equalTo(self.radioBgView).offset(-5)
                make.left.equalTo(self.radioBgView).offset(hSpace)
                make.right.equalTo(self.searchIcon.snp.left)
            }
            
            self.searchIcon.snp.makeConstraints { (make) in
                make.centerY.equalTo(self.radioBgView)
                make.right.equalTo(self.radioBgView.snp.right).offset(-8)
                make.height.equalTo(self.radioBgView).multipliedBy(1)
                make.width.equalTo(self.searchIcon.snp.height)
            }
            
        }
        
    }
    
    @objc func searchAction() {
    
        if self.textfield.isFirstResponder {
            self.textfield.resignFirstResponder()
        }
    
        self.delegate?.doSearchWith(keyword: self.textfield.text!)
        
    }
    
    @objc func clearTextField() {
        self.textfield.text = ""
        self.textfield.delegate?.textFieldDidEndEditing?(self.textfield)
        self.delegate?.doSearchWith(keyword: self.textfield.text!)
    }
    
    @objc func scanQRClick() {
        self.delegate?.scanQRClick!()
    }
    
    @objc func textFieldValueChanged() {
        
        if !(self.textfield.text?.isEmpty ?? false) {
            self.textfield.rightViewMode = .always
        }else{
            self.textfield.rightViewMode = .never
        }
        
    }

}

extension TopSearchView:UITextFieldDelegate{
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.textfield.resignFirstResponder()

        self.delegate?.doSearchWith(keyword: self.textfield.text!)

        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if !(self.textfield.text?.isEmpty ?? true) {
            self.textfield.rightViewMode = .always
        }else{
            self.textfield.rightViewMode = .never
        }
        
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if !(textfield.text?.isEmpty ?? false) {
            self.textfield.rightViewMode = .always
        }else{
            self.textfield.rightViewMode = .never
        }
    }
    
    
    
}
