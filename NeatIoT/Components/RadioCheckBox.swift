//
//  RadioCheckBox.swift
//  NeatIoT
//
//  Created by neat on 2019/11/20.
//  Copyright © 2019 Neat. All rights reserved.
//

import UIKit


protocol RadioCheckBoxDelegate {
    
    func checkBoxStateChanged(isSelect:Bool,view:RadioCheckBox)
}

class RadioCheckBox: UIView {
    
    var delegate:RadioCheckBoxDelegate?
    
    /// 是否选中状态
    var checkState: Bool{
        get{
            isCheckBoxSelect
        }
        set{
            isCheckBoxSelect = newValue
            self.centerRadio.isHidden = !isCheckBoxSelect
        }
    }
    
    /// 标题文字
    var titleText: String {
        get {
            self.textLable.text!
        }
        set {
            self.textLable.text = newValue
        }
    }
    
    /// 选中标识
    private var isCheckBoxSelect = true
    
    /// 复选框颜色
    private let radioColor = #colorLiteral(red: 0.3098039329, green: 0.3098039329, blue: 0.3098039329, alpha: 1).cgColor
    
    /// 复选框外层board宽度
    private let boardWidth:CGFloat = 2

    /// 复选框中间的原点
    lazy var centerRadio: CAShapeLayer = {
        let centerRadio = CAShapeLayer.init()
        centerRadio.backgroundColor = radioColor
        return centerRadio
    }()
    
    /// 复选框外层board
    private lazy var boradLayer: CAShapeLayer = {
        let boradLayer = CAShapeLayer.init()
        boradLayer.borderWidth = boardWidth
        boradLayer.borderColor = radioColor
        return boradLayer
    }()
    
    /// 复选框标题
    private lazy var textLable: UILabel = {
        let label = UILabel.init()
        label.textColor = #colorLiteral(red: 0.3098039329, green: 0.3098039329, blue: 0.3098039329, alpha: 1)
        label.minimumScaleFactor = 0.5
        label.font = UIFont.systemFont(ofSize: 15)
        return label
    }()

    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    
    /// 根据视图大小 按比例画复选框
    override func draw(_ rect: CGRect) {
        // Drawing code
        
        let centerRadioScale:CGFloat = 0.3
        let boardRadioScale:CGFloat = 0.6
        
        //文字leading trailing 边距
        let icon_text_space:CGFloat = 9
        
        let viewHeight = rect.size.height
        let viewWidth = rect.size.width
        
        /// 计算中心原点高
        let centerRadioHeight = viewHeight*centerRadioScale
        let centerRadioY = (viewHeight - centerRadioHeight)/2.0
        
        self.centerRadio.frame = CGRect.init(x: viewHeight*0.15, y: centerRadioY, width: centerRadioHeight, height: centerRadioHeight)
        self.centerRadio.cornerRadius = centerRadioHeight/2.0
        self.layer.addSublayer(centerRadio)
        
        /// 计算外层圆环高
        let boardRadioHeight = viewHeight*boardRadioScale
        let boardRadioY = (viewHeight - boardRadioHeight)/2.0
        
        boradLayer.frame = CGRect.init(x: 0, y: boardRadioY, width: boardRadioHeight, height: boardRadioHeight)
        boradLayer.cornerRadius = boardRadioHeight/2.0
        self.layer.addSublayer(boradLayer)
        
        /// 添加标题文字
        textLable.frame = CGRect.init(x: boardRadioHeight + icon_text_space, y: 0, width: viewWidth - (boardRadioHeight + icon_text_space), height: viewHeight)
        self.addSubview(self.textLable)
        
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        if isCheckBoxSelect {
            
            
            unSelectAnimation()
            
        }else{
            
            selectAnimation()
            
        }
        
        
    }
    
    func selectAnimation() {
        
        isCheckBoxSelect = true
        
        self.centerRadio.isHidden = false
        
        let scaleAnimation = CASpringAnimation.init(keyPath: "transform.scale")
        
        scaleAnimation.fromValue = 2
        scaleAnimation.toValue = 1
        scaleAnimation.duration = 0.25
        self.centerRadio.add(scaleAnimation, forKey: "unCheckAnimation")
        
        scaleAnimation.fromValue = 1
        scaleAnimation.toValue = 1.2
        scaleAnimation.isCumulative = true
        scaleAnimation.duration = 0.2
        self.boradLayer.add(scaleAnimation, forKey: "unCheckAnimation")
        
        self.delegate?.checkBoxStateChanged(isSelect: true,view: self)
    }
    
    func unSelectAnimation() {
        
        isCheckBoxSelect = false
        
        let scaleAnimation = CASpringAnimation.init(keyPath: "transform.scale")

        let borderAnimation = CASpringAnimation.init(keyPath: "borderWidth")
        
        scaleAnimation.fromValue = 1
        scaleAnimation.toValue = 1.5
        scaleAnimation.duration = 0.25
        self.centerRadio.add(scaleAnimation, forKey: "unCheckAnimation")
        
        borderAnimation.fromValue = 2
        borderAnimation.toValue = 5
        borderAnimation.isCumulative = true
        borderAnimation.duration = 0.25
        self.boradLayer.add(borderAnimation, forKey: "unCheckAnimation")
        
        self.perform(#selector(animationFinsih), with: self, afterDelay: 0.25)
        
        self.delegate?.checkBoxStateChanged(isSelect: false,view: self)
    }
    
    @objc func animationFinsih() {
        self.centerRadio.isHidden = true
    }
    

}


