//
//  IconTextField.swift
//  NeatIoT
//
//  Created by neat on 2019/10/11.
//  Copyright © 2019 Neat. All rights reserved.
//

import UIKit

class IconTextField: UIView {
    
    var SecureTextEntry:Bool{
        get {
            return false
        }
        set {
            textField.isSecureTextEntry = newValue
        }
    }
    
    var iconName: String {
        get {
            return ""
        }
        set {
            iconImageView.image = UIImage.init(named: newValue)
        }
    }
    
    var placeHolder: String {
        get {
            return ""
        }
        set {
            let attMutStr = NSAttributedString.init(string: newValue, attributes: [.foregroundColor:#colorLiteral(red: 0.7019607843, green: 0.8039215686, blue: 0.8666666667, alpha: 1)])
            textField.attributedPlaceholder = attMutStr
        }
    }
    
    var textValue: String {
        get {
            return textField.text!
        }
        set {
            textField.text = newValue
        }
    }
    
    var keyboardType: UIKeyboardType {
        get {
            return .numberPad
        }
        set {
            textField.keyboardType = newValue
        }
    }
    
    lazy var textField: UITextField = {
        let textField = UITextField()
        textField.tintColor = .white
        textField.textColor = .white
        textField.returnKeyType = .done
        textField.delegate = self
        return textField
    }()
    
    lazy var iconImageView: UIImageView = {
        let imageview = UIImageView.init()
        imageview.contentMode = .scaleAspectFit
        return imageview
    }()
    
    lazy var lineView: UIView = {
        let lineView = UIView()
        lineView.backgroundColor = #colorLiteral(red: 0.7019607843, green: 0.8039215686, blue: 0.8666666667, alpha: 1)
        return lineView
    }()
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        configerUI()
    }
    
    func configerUI() -> Void {
        
        self.addSubview(iconImageView)
        
        iconImageView.snp.makeConstraints { (make) in
            make.left.equalTo(self).offset(8)
            make.top.equalTo(8)
            make.bottom.equalTo(-8)
            make.width.equalTo(iconImageView.snp.height)
        }
        
        self.addSubview(textField)
        
        textField.snp.makeConstraints { (make) in
            make.centerY.equalTo(self)
            make.left.equalTo(iconImageView.snp.right).offset(8)
            make.right.equalTo(self).offset(-8)
            make.height.equalTo(self)
            
        }
        
        self.addSubview(lineView)
        
        lineView.snp.makeConstraints { (make) in
            make.left.right.bottom.equalTo(self)
            make.height.equalTo(1)
        }
        
    }

}

extension IconTextField: UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        textField.resignFirstResponder()
        
        return true
    }
    
    
}
