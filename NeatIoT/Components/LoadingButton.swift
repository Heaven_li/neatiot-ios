//
//  LoadingButton.swift
//  NeatIoT
//
//  Created by Yang Blus on 2020/12/2.
//  Copyright © 2020 Neat. All rights reserved.
//

import UIKit

protocol LoadingButtonDelegate {
    func didClickButton();
}


/// 带请求状态的按钮
class LoadingButton: UIView {
    
    var delegate:LoadingButtonDelegate?
    
    ///按钮当前title
    private var currentTitleText = "";
    ///按钮当前状态 是否是加载中 true:加载 false:未加载
    private var currentIsLoading = false;
    ///按钮当前状态 是否可用 true:可用 false:不可用
    private var currentIsEnable = true;
    ///可用背景图片
    private var currentEnableBgName:String = "submit_enable_bg_btn";
    ///不可背景图片
    private var currentDisableBgName:String = "submit_disable_bg_btn";
    
    /// 设置按钮不可用背景色
    var disableBgImageName: String{
        
        get {
            currentDisableBgName
        }
        set {
            currentDisableBgName = newValue
            bgButton.setBackgroundImage(UIImage.init(named: newValue), for: .normal)
        }
        
    }
    
    /// 设置按钮可用背景色
    var enableBgImageName: String{
        
        get {
            currentEnableBgName
        }
        set {
            currentEnableBgName = newValue
            bgButton.setBackgroundImage(UIImage.init(named: newValue), for: .normal)
        }
        
    }
    
    /// 设置按钮当前是否可用
    var setBtnEnable: Bool {
        get {
            currentIsEnable
        }
        set {
            currentIsEnable = newValue
            
        }
    }
    
    /// 设置按钮文字
    var titleText: String {
        get {
            currentTitleText
        }
        set {
            currentTitleText = newValue
            bgButton.setTitle(currentTitleText, for: .normal)
        }
    }
    
    /// 设置加载状态
    var isLoading: Bool {
        get {
            currentIsLoading
        }
        set {
            currentIsLoading = newValue
            
            if currentIsLoading {
                //加载状态 文字置会 按钮不可用
                bgButton.setTitle("", for: .normal)
                bgButton.setTitleColor(.lightGray, for: .normal)
                bgButton.setBackgroundImage(UIImage.init(named: currentDisableBgName), for: .normal)
                self.indicatorView.isHidden = false
                self.indicatorView.startAnimating()
            }else{
                bgButton.setTitle(currentTitleText, for: .normal)
                bgButton.setTitleColor(.white, for: .normal)
                bgButton.setBackgroundImage(UIImage.init(named: currentEnableBgName), for: .normal)
                self.indicatorView.isHidden = true
                self.indicatorView.stopAnimating()
            }
            
        }
    }
    
    //事件按钮
    private lazy var bgButton: UIButton = {
        
        let bgButton = UIButton.init(type: .custom)
        bgButton.titleLabel?.font = UIFont.systemFont(ofSize: 14)
        bgButton.setTitleColor(.white, for: .normal)
        bgButton.setBackgroundImage(UIImage.init(named: currentEnableBgName), for: .normal)
        bgButton.addTarget(self, action: #selector(didClickButton(sender:)), for: .touchUpInside)
        
        return bgButton
    }()
    //加载view
    private lazy var indicatorView: UIActivityIndicatorView = {
        
        let indicatorView = UIActivityIndicatorView.init(style: .white)
        
            
        return indicatorView
    }()

    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.addSubview(self.bgButton);
        self.addSubview(self.indicatorView);
        
        self.indicatorView.isHidden = true
        
        self.bgButton.snp.makeConstraints { (make) in
            make.top.left.right.bottom.equalTo(self)
        }
        
        self.indicatorView.snp.makeConstraints { (make) in
            make.center.equalTo(self)
        }
        
        
    }
    
    @objc func didClickButton(sender:UIButton) {
        
        if currentIsEnable {
            self.delegate?.didClickButton()
        }
        
    }
    
}
