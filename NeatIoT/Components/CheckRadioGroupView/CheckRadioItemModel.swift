//
//  CheckRadioGroupViewModel.swift
//  NeatIoT
//
//  Created by Yang Blus on 2020/3/20.
//  Copyright © 2020 Neat. All rights reserved.
//

import UIKit


/// 单个复选框元素 model
class CheckRadioItemModel: NSObject {
    
    
    /// radio 对应key
    public var radioKey:String = ""
    
    /// radio 对应选中状态 true 选中   false 为选中
    public var isRadioCheck:Bool = false
    
    /// radio 对应标题
    public var radioTitle:String = ""

}
