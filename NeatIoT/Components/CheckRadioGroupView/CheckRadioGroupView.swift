//
//  CheckRadioGroupView.swift
//  NeatIoT
//
//  Created by Yang Blus on 2020/3/20.
//  Copyright © 2020 Neat. All rights reserved.
//

import UIKit



protocol CheckRadioGroupViewDelegate {
    
    func checkRadioGroupValueChanged(checkRadioItems:[CheckRadioItemModel])
    
}

/// 复选框集合视图
class CheckRadioGroupView: UIView {
    
    
    public var delegate:CheckRadioGroupViewDelegate?
    
    /// 是否可多选
    public var isMuitleCheck:Bool = false
    /// 容器stackview
    lazy var containtStackView: UIStackView = {
        let stackView = UIStackView.init()
        stackView.alignment = .center
        stackView.distribution = .equalSpacing
        stackView.axis = .horizontal
        stackView.spacing = 4
        
    
        return stackView
    }()
    
    /// 标题数组
    var radioItemArr: [CheckRadioItemModel] {
        get {
            currentradioItemArr
        }
        set {
            currentradioItemArr.removeAll()
            currentradioItemArr.append(contentsOf: newValue)
            self.configerRadioView()
        }
    }
    
    
    /// 当前标题数组
    private var currentradioItemArr:[CheckRadioItemModel] = []
    /// 当前选中radio数组
    private var checkRadioArr:[CheckRadioItemModel] = []

    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.addSubview(self.containtStackView)
        self.containtStackView.snp.makeConstraints { (make) in
            make.edges.equalTo(self)
        }
        
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func configerRadioView(){
        
        for view in self.containtStackView.arrangedSubviews {
            view.removeFromSuperview()
        }
        
        
        if currentradioItemArr.count != 0 {
            ///暂时支持单行排列 用stackview？
            for (index,itemModel) in currentradioItemArr.enumerated(){
                
                let radioCheckBox = RadioCheckBox.init();
                radioCheckBox.titleText = itemModel.radioTitle
                radioCheckBox.tag = index
                radioCheckBox.delegate = self
                radioCheckBox.checkState = itemModel.isRadioCheck
                ///如果非多选 要限制一选中radio的重复点击
                if !isMuitleCheck {
                    radioCheckBox.isUserInteractionEnabled = !itemModel.isRadioCheck
                }
                
                
                self.containtStackView.addArrangedSubview(radioCheckBox)
                
                radioCheckBox.snp.makeConstraints { (make) in
                    make.width.equalTo(95)
                    make.height.equalTo(30)
                }
                
            }
        }else{
            debugPrint("title数组不能为空！")
        }
        
        
        
        
    }
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
    

}

extension CheckRadioGroupView:RadioCheckBoxDelegate{
    
    func checkBoxStateChanged(isSelect: Bool, view: RadioCheckBox) {
        
        view.isUserInteractionEnabled = false
        
        if isMuitleCheck {
            //如果是多选 统计当前选中元素
            self.checkRadioArr.append(currentradioItemArr[view.tag])
            
        }else{
            
            
            //如果是单选 更新选中状态
            for radioBox in self.containtStackView.arrangedSubviews {
                
                if radioBox.tag != view.tag {
                    let box = radioBox as! RadioCheckBox
                    box.checkState = false
                    box.isUserInteractionEnabled = true
                }
            }
            
            self.checkRadioArr.append(currentradioItemArr[view.tag])
            
        }
        
        /// 回调通知选中radio
        self.delegate?.checkRadioGroupValueChanged(checkRadioItems: self.checkRadioArr)
        self.checkRadioArr.removeAll()
        
        
    }
    
}
