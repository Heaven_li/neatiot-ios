//
//  FileManagerTool.swift
//  NeatIoT
//
//  Created by Yang Blus on 2020/3/4.
//  Copyright © 2020 Neat. All rights reserved.
//

import UIKit

class FileManagerTool: NSObject {

    class func directoryURL() -> URL {
        
        let date = Date.init(timeIntervalSinceNow: 0)

        let dateFormatter = DateFormatter.init()
        dateFormatter.dateFormat = "YYYYMMddhhmmss"
        let currentTimeStr = dateFormatter.string(from: date)
        let path = NSTemporaryDirectory() + currentTimeStr + ".caf"
        
        return URL.init(fileURLWithPath: path)
        
    }
    
}
