//
//  AudioRecorderTool.swift
//  NeatIoT
//
//  Created by Yang Blus on 2020/2/13.
//  Copyright © 2020 Neat. All rights reserved.
//

import UIKit
import AVFoundation

class AudioRecorderTool: NSObject {
    
    var corverUrl:String = ""
    
    /// 音频录制设置参数
    private let recordSetting: [String: Any] = [AVSampleRateKey: NSNumber(value: 11025),//采样率
        AVFormatIDKey: NSNumber(value: kAudioFormatLinearPCM),//音频格式
        AVLinearPCMBitDepthKey: NSNumber(value: 16),//采样位数
        AVNumberOfChannelsKey: NSNumber(value: 2),//通道数
        AVEncoderAudioQualityKey: NSNumber(value: AVAudioQuality.medium.rawValue)//录音质量

    ];
    
    private let audioSession = AVAudioSession.sharedInstance()
    /// 音频录制
    private var recorder:AVAudioRecorder?
    /// 音频播放
    private var audioPlayer:AVAudioPlayer?
    
    private var finishPlayCallBack:()->Void = {}
    
    var isPlay: Bool {
        return audioPlayer?.isPlaying ?? false
    }
    
    static let sharedInstance: AudioRecorderTool = {
        let instance = AudioRecorderTool.init()
           // setup code
           return instance
    }()
    
    func checkPermAudio(granted:@escaping ()->Void,denied:@escaping ()->Void) {
        
        switch audioSession.recordPermission {
            
            case .granted:
                //已授权
                granted()
            break
            case .denied:
                //已拒绝
                denied()
            break
            case .undetermined:
                
                audioSession.requestRecordPermission { (allow) in
                    DispatchQueue.main.async {
                        if allow {
                            //同意
//                            granted()
                        }else{
                            //不同意
                            denied()
                        }
                    }
                }
                
            break
            
        @unknown default:
            denied()
        }
        
    }
    
    func startRec(url:URL,start:@escaping ()->Void,failed:@escaping (_ isPermissionFalied:Bool, _ recFailed:Bool)->Void) {
        
        checkPermAudio(granted: {
            
            DispatchQueue.global().async {
                //已授权
                do {
                    try self.audioSession.setCategory(AVAudioSession.Category.playAndRecord)
                    try self.audioSession.setActive(true)
                    self.recorder = try AVAudioRecorder.init(url: url, settings:self.recordSetting)
                    if self.recorder?.prepareToRecord() ?? false{
                        
                        self.recorder?.record()
                        
                        JKLameTool.init().audioRecoding(toMP3: url.path, isDeleteSourchFile: true, withSuccessBack: { (resousePath) in
                            
                            self.corverUrl = resousePath
                            
                        }) { (errorMessage) in
                            
                        }
                        DispatchQueue.main.async {
                            start()
                        }
                        
                    }
                } catch {
                    
                    DispatchQueue.main.async {
                        failed(false,true)
                    }
                    
                }
            }
            
        }) {
            //拒绝
            DispatchQueue.main.async {
                failed(true,false)
            }
            
        }
        
        
        
    }
    
    func stopRec(finish:@escaping (_ audioModel:AnnexModel)->Void,failed:@escaping (_ errorStr:String)->Void) {
        
        if recorder?.isRecording ?? false {
            
            DispatchQueue.global().async {
                
                self.recorder!.stop()
                
                JKLameTool.init().sendEndRecord()
                
                do {
                    try self.audioSession.setActive(false)
                    
//                    JKLameTool.audio(toMP3: self.recorder!.url.path, isDeleteSourchFile: true, withSuccessBack: { (path) in
//
//                        let audioModel = AnnexModel.init()
//                        audioModel.annexType = 3
//                        audioModel.sourceFile = URL.init(fileURLWithPath: path)
//                        audioModel.voiceSecond = self.getAudioTime(url: URL.init(fileURLWithPath: path))
//                        audioModel.isNetSource = false
//                        audioModel.isAddNewItem = false
//
//                        DispatchQueue.main.async {
//
//                            if audioModel.voiceSecond ?? 0 > 1{
//                                finish(audioModel)
//                            }else{
//                                failed("说活时间太短")
//                            }
//
//                        }
//
//                    }) { (error) in
//                        ///错误处理
//                    }
                    
                    DispatchQueue.main.async {

                        let audioModel = AnnexModel.init()
                        audioModel.annexType = 3
                        audioModel.sourceFile = URL.init(fileURLWithPath: self.corverUrl)
                        audioModel.voiceSecond = self.getAudioTime(url:audioModel.sourceFile ?? URL.init(string: "")!)
                        audioModel.isNetSource = false
                        audioModel.isAddNewItem = false
                        if audioModel.voiceSecond ?? 0 > 1{
                            finish(audioModel)
                        }else{
                            failed("说活时间太短")
                        }


                    }
                    
                } catch {
                    
                    DispatchQueue.main.async {
                        failed("录制失败")
                    }
                    
                }
            }
            
        }
        
        
        
    }
    
    func playRec(model:AnnexModel,finishPlay:@escaping ()->Void,playFailed:@escaping ()->Void) {
        
        finishPlayCallBack = finishPlay
        
        do {
            let url = model.sourceFile!
            try audioSession.setCategory(AVAudioSession.Category.playback)
            try audioSession.setActive(true)
            audioPlayer = try AVAudioPlayer.init(contentsOf: url)
            audioPlayer?.delegate = self
            audioPlayer?.prepareToPlay()
            audioPlayer?.play()
        } catch  {
            
            playFailed()
        }
        
        
    }
    
    func stopPlayRec() {
        
        if audioPlayer?.isPlaying ?? false {
            audioPlayer?.stop()
            do {
                try audioSession.setActive(false)
            } catch  {
                
            }
        }
        
    }
    
    func getAudioTime(url:URL) -> Int {
        
        let dic = [AVURLAssetPreferPreciseDurationAndTimingKey:true]
        
        let audioAsset = AVURLAsset.init(url: url, options: dic)
        
        let audioTime = CMTimeGetSeconds(audioAsset.duration);
        
        return Int(audioTime)
    }
    

}

extension AudioRecorderTool:AVAudioPlayerDelegate{
    
    func audioPlayerDecodeErrorDidOccur(_ player: AVAudioPlayer, error: Error?) {
        debugPrint("解码失败")
    }
    func audioPlayerDidFinishPlaying(_ player: AVAudioPlayer, successfully flag: Bool) {
        debugPrint("播放完毕")
        finishPlayCallBack()
        do {
            try audioSession.setActive(false)
        } catch  {
            
        }
        
        
    }
        
}

