//
//  CoreDataHelper.swift
//  NeatIoT
//
//  Created by Yang Blus on 2020/5/14.
//  Copyright © 2020 Neat. All rights reserved.
//

import UIKit
import CoreData


class CoreDataHelper: NSObject {
    
    lazy var objectModel: NSManagedObjectModel = {
        
        guard let modelUrl = Bundle.main.url(forResource: "NeatIoTData", withExtension: "momd") else {
            fatalError("获取coreData模型地址报错")
        }
        
        guard let model = NSManagedObjectModel.init(contentsOf: modelUrl) else {
            fatalError("创建objectModel实例报错")
        }
        
        return model
        
    }()
    
    lazy var storeCoord: NSPersistentStoreCoordinator = {
        
        let coord = NSPersistentStoreCoordinator.init(managedObjectModel: self.objectModel)
        
        let pathUrl = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).last
        
        let storeUrl = pathUrl?.appendingPathComponent("/NeatIoTData.sqlite")
        
        do {
            
            // 创建并关联SQLite数据库文件，如果已经存在则不会重复创建
            let options = [NSMigratePersistentStoresAutomaticallyOption: true, NSInferMappingModelAutomaticallyOption: true]
            
            try coord.addPersistentStore(ofType: NSSQLiteStoreType, configurationName: nil, at: storeUrl, options: nil)
            
        }catch{
            
        }
        
        
        return coord
    }()
    
    
    
    
    lazy var managerContent: NSManagedObjectContext = {
        
        let manager = NSManagedObjectContext.init(concurrencyType: .mainQueueConcurrencyType)
        
//        let modoUrl = Bundle.main.url(forResource: "NeatIoTData", withExtension: "momd")
//        let objectModel = NSManagedObjectModel.init(contentsOf: modoUrl!)
//
//        let storeCoord = NSPersistentStoreCoordinator.init(managedObjectModel: objectModel!)
//
//        var dataPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).last
//
//        dataPath?.append("/NeatIoTData.sqlite")
        
//        do{
            // 创建并关联SQLite数据库文件，如果已经存在则不会重复创建
//            try storeCoord.addPersistentStore(ofType: NSSQLiteStoreType, configurationName: nil, at: URL.init(fileURLWithPath: dataPath!), options: nil)
            // 上下文对象设置属性为持久化存储器
            manager.persistentStoreCoordinator = self.storeCoord
//            manager.persistentStoreCoordinator = storeCoord;
            
//        }catch{
            
//        }
        
        return manager
    }()
    
    
    /// 插入入场时间
    /// - Parameter model: 入场时间model
    func insertEnterTimeObjec(model:EnterTiemModel) {
        
        let enterRec = NSEntityDescription.insertNewObject(forEntityName: "EnterTimeRec", into: self.managerContent) as! EnterTimeRec
        
        enterRec.acceptId = model.acceptId
        enterRec.enterTime = model.enterTime
        enterRec.enterAddress = model.enterAddress
        
        do {
            try self.managerContent.save()
        } catch  {
            debugPrint("catch")
        }
    
        
    }
    
    /// 根据受理ID 获取入场时间
    /// - Parameter acceptId: 受理ID
    func getEnterInfor(acceptId:String) -> (ishave:Bool,enterTime:EnterTiemModel?){
        
        let request = NSFetchRequest<NSFetchRequestResult>.init(entityName: "EnterTimeRec")
        
        let predicate = NSPredicate.init(format: "SELF .acceptId = %@", acceptId)
        
        request.predicate = predicate
        
        do {
            let arr = try self.managerContent.fetch(request)
            
            if arr.count == 0{
                debugPrint("没找到 123455")
                return (false,nil)
            }
            for data in arr as! [EnterTimeRec] {
                
                let enterTimeModel = EnterTiemModel.init()
                enterTimeModel.acceptId = data.acceptId
                enterTimeModel.enterAddress = data.enterAddress
                enterTimeModel.enterTime = data.enterTime
                
                return (true,enterTimeModel)
            }
        
        } catch  {
            debugPrint("catch")
        }
        
        return (false,nil)
        
    }
    
    func insertMaterialsObjec(model:ProtectionMaterials) {
        
        if self.checkMaterialsExist(materialsId: model.materialsId!) {
            
            return
            
        }
        
        let entity = NSEntityDescription.insertNewObject(forEntityName: "ConsumablesEntity", into: self.managerContent) as! ConsumablesEntity
        
        entity.consumablesId = model.materialsId
        entity.consumablesName = model.materialsName
        entity.consumablesPrice = model.materialsPrice!
        entity.consumablesUnit = model.materialsUnit
        entity.consumablesNum = Int64(model.materialsCount)
        entity.consumablesType = Int16(model.materialsType!)
        entity.addScTime = model.addScTime
        
        do {
            try self.managerContent.save()
        } catch  {
            debugPrint("catch")
        }

    }
    
    func searchMaterialsWithKeyWord(keyWord:String) -> Array<ProtectionMaterials>{
        
        var materialsArr:Array<ProtectionMaterials> = []
        
        let request = NSFetchRequest<NSFetchRequestResult>.init(entityName: "ConsumablesEntity")
        
        let predicate = NSPredicate.init(format: "consumablesName contains %@ ", keyWord)
        
        request.predicate = predicate
        
        do {
            let arr = try self.managerContent.fetch(request)
            
            for data in arr as! [ConsumablesEntity] {
                
                let model = ProtectionMaterials.init()
                model.materialsId = data.consumablesId
                model.materialsName = data.consumablesName
                model.materialsPrice = data.consumablesPrice
                model.materialsUnit = data.consumablesUnit
                model.materialsCount = Int(data.consumablesNum)
                model.addScTime = data.addScTime
                
                materialsArr.append(model)
            }
            
        } catch  {
            
        }
        
        return materialsArr
    }
    
    
    func updateModel(model:ProtectionMaterials,updataTime:Bool) {
        
        let request = NSFetchRequest<NSFetchRequestResult>.init(entityName: "ConsumablesEntity")
        
        let predicate = NSPredicate.init(format: "SELF .consumablesId = %@", model.materialsId!)
        
        request.predicate = predicate
        
        do {
            
            let arr = try self.managerContent.fetch(request)
            for data in arr as! [ConsumablesEntity] {
                
                data.consumablesNum = Int64(model.materialsCount)
                data.tureTotalMoney = model.tureTotalMoney
                
                if updataTime {
                    data.addScTime = model.addScTime
                }
                
            }
            
            try self.managerContent.save()
        } catch  {
            debugPrint("catch")
        }
        
    }
   
    
    
    func checkMaterialsExist(materialsId:String) -> Bool {
        
        let request = NSFetchRequest<NSFetchRequestResult>.init(entityName: "ConsumablesEntity")
        
        let predicate = NSPredicate.init(format: "SELF .consumablesId = %@", materialsId)
        
        request.predicate = predicate
        
        do {
            let arr = try self.managerContent.fetch(request)
            
            if arr.count == 0{
                return false
            }else{
                return true
            }
            
        } catch  {
            
            return false
            
        }
        
    }
    
    func deleteMaterialsObjec() {
        
        let request = NSFetchRequest<NSFetchRequestResult>.init(entityName: "ConsumablesEntity")
        
        do {
            let arr = try self.managerContent.fetch(request)
            
            for data in arr as! [ConsumablesEntity] {
                
                self.managerContent.delete(data)
            }
            
            try self.managerContent.save()
            
        } catch  {
            
        }
        
        
    }
    
    func getMaterialsByAddTime() -> Array<ProtectionMaterials> {
        
        var dataArr:Array<ProtectionMaterials> = []
        
        let request = NSFetchRequest<NSFetchRequestResult>.init(entityName: "ConsumablesEntity")
        
        let predicate = NSPredicate.init(format: "addScTime != null && addScTime != nil")
        let sortDesc = NSSortDescriptor.init(key: "addScTime", ascending: false)
        request.sortDescriptors = [sortDesc]
        request.predicate = predicate
        
        do {
            
            let arr = try self.managerContent.fetch(request)
            for data in arr as! [ConsumablesEntity] {
                
                let model = ProtectionMaterials.init()
                model.materialsId = data.consumablesId
                model.materialsName = data.consumablesName
                model.materialsPrice = data.consumablesPrice
                model.materialsUnit = data.consumablesUnit
                model.materialsCount = Int(data.consumablesNum)
                model.addScTime = data.addScTime
                model.tureTotalMoney = data.tureTotalMoney
                
                dataArr.append(model)
                
            }
            
        } catch  {
           
        }
        
        return dataArr
        
    }
    
    
    func readObjec() -> Array<ProtectionMaterials> {
        
        let request = NSFetchRequest<NSFetchRequestResult>.init(entityName: "ConsumablesEntity")
        
        do {
            
            let arr = try self.managerContent.fetch(request)
            var dataArr:Array<ProtectionMaterials> = []
            
            for data in arr as! [ConsumablesEntity] {
                
                let model = ProtectionMaterials.init()
                model.materialsId = data.consumablesId
                model.materialsName = data.consumablesName
                model.materialsPrice = data.consumablesPrice
                model.materialsUnit = data.consumablesUnit
                model.materialsCount = Int(data.consumablesNum)
                model.addScTime = data.addScTime
                
                dataArr.append(model)
            
            }
        
            return dataArr
            
        } catch  {
            return []
        }
        
    }
    
    
    
    

}


