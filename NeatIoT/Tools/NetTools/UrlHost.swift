//
//  UrlHost.swift
//  NeatIoT
//
//  Created by neat on 2019/12/19.
//  Copyright © 2019 Neat. All rights reserved.
//

import UIKit

class UrlHost {
    
    private static let HOSTKey = "HOSTKey"
        
        /// host
    static var urlHost: String {
        
        get{
            
            UserDefaults.standard.string(forKey: self.HOSTKey) ?? ""
//            return "http://39.104.187.162:9004"
//            return "http://192.168.0.100:5400"
        }
        set{
            UserDefaults.standard.set("http://"+newValue, forKey: self.HOSTKey)
        }
        
    }
    /// 是否使用v2版本接口
    static var isV2Api = true
        
    /// 设备管理相关接口
    /// 用户验证
    class Auth: NSObject {
        ///登录
        static var loginAPI: String {
            return urlHost+"/OpenApi/Auth/LoginGetToken"
        }
        ///退出登录
        static var logoutAPI: String {
            return urlHost+"/OpenApi/Auth/UserLogout"
        }
    }
    
    /// 通用
    class SysAuth: NSObject {
        
        
        static var model: LoginResultModel {
            let model = DataArchiver.unArchiver(key:ArchireKey.UserInfor)
            if model != nil {
                return  model as! LoginResultModel
            } else {
                return LoginResultModel()
            }
        }
        
        ///获取主页菜单
        static var sysMenuAPI: String {
            
            if isV2Api {
                return urlHost+"/OpenApi//SysAuth/GetSysMenuForMobileV2?token="+model.user_token
            }else{
                return urlHost+"/OpenApi/SysAuth/GetSysMenuForMobile?token="+model.user_token
            }
            
            
        }
        ///获取用户权限内的中心列表
        static var sysDomainListAPI: String {
            return urlHost+"/OpenApi/SysAuth/GetDomainIdList?token="+model.user_token
        }
        ///根据中心获取用户权限内的单位列表
        static var sysEntListAPI: String {
            return urlHost+"/OpenApi/SysAuth/GetEntByDomainId?token="+model.user_token
        }
        ///根据单位获取用户权限内的建筑列表
        static var sysBuildingListAPI: String {
            return urlHost+"/OpenApi/SysAuth/GetBuildingByEntId?token="+model.user_token
        }
        ///根据建筑获取用户权限内的部位列表
        static var sysKeypartListAPI: String {
            return urlHost+"/OpenApi/SysAuth/GetKeypartByBuildingId?token="+model.user_token
        }
        ///通过类型获取信息列表
        static var sysGetInforByCategoreAPI: String{
            return urlHost+"/OpenApi/SysAuth/GetSysCodeList?token="+model.user_token
        }
        ///获取项目子类型
        static var sysGetProjectSubTypeAPI: String{
            return urlHost+"/OpenApi/OsiItem/GetOsiSubTypeItem?token="+model.user_token
        }
    }
    
    ///火相关接口
    class FireUITD: NSObject {
        
        static var model: LoginResultModel {
            let model = DataArchiver.unArchiver(key:ArchireKey.UserInfor)
            if model != nil {
                return  model as! LoginResultModel
            } else {
                return LoginResultModel()
            }
        }
        
        ///获取一体化传输设备列表
        static var uitdListAPI: String{
            return urlHost+"/OpenApi/FireUITD/QueryUITDListForMobile?token="+model.user_token
        }
        ///获取一体化传输设备下主机列表
        static var hostListAPI: String{
            return urlHost+"/OpenApi/FireUITD/GetFireSystemsByUitdIdForMobile?token="+model.user_token
        }
        
        /// 获取一体化传输设备详细信息
        static var uiteDetailAPI: String{
            return urlHost+"/OpenApi/FireUITD/GetUitdDetailForMobile?token="+model.user_token
        }
        /// 获取火主机详细信息
        static var hostDetailAPI: String{
            
            return urlHost+"/OpenApi/FireUITD/GetFireSystemsDetailForMobile?token="+model.user_token
        }
        
        ///添加一体化传输设备
        static var addUITDAPI: String{
            
            return urlHost+"/OpenApi/FireUITD/AddUITDForMobile?token="+model.user_token
        }
        ///添加一体化主机设备
        static var addUITDDeviceAPI: String{
            return urlHost+"/OpenApi/FireUITD/AddFireSystemForMobile?token="+model.user_token
        }
        
        ///修改一体化传输设备
        static var editUITDAPI: String{
            return urlHost+"/OpenApi/FireUITD/UpdateUitd?token="+model.user_token
        }
        ///修改一体化主机设备
        static var editUITDDeviceAPI: String{
            return urlHost+"/OpenApi/FireUITD/UpdateFireSystemForMobile?token="+model.user_token
        }
        ///删除一体化传输设备
        static var deleteUITDAPI: String{
            return urlHost+"/OpenApi/FireUITD/DeleteUitd?token="+model.user_token
        }
        ///删除一体化主机设备
        static var deleteUITDDeviceAPI: String{
            return urlHost+"/OpenApi/FireUITD/DeleteFireSystemForMobile?token="+model.user_token
        }
        
        
        
    }
    
    ///水相关接口
    class WaterDevice: NSObject {
        
        static var model: LoginResultModel {
            let model = DataArchiver.unArchiver(key:ArchireKey.UserInfor)
            if model != nil {
                return  model as! LoginResultModel
            } else {
                return LoginResultModel()
            }
        }
        
        ///获取水网关列表
        static var waterGatewayListAPI: String{
            return urlHost+"/OpenApi/WaterDevice/GetMultiplexWGWListForMobile?token="+model.user_token
        }
        
        ///获取水网关下水信号列表
        static var waterSignalListAPI: String{
            return urlHost+"/OpenApi/WaterDevice/GetWaterSignalListForMobile?token="+model.user_token
        }
        
        ///获取一体式水源监测设备列表
        static var integratedWGWListAPI: String {
            
            if isV2Api {
                return urlHost+"/OpenApi/WaterDevice/GetIntegratedWGWListForMobileV2?token="+model.user_token
            }else{
                return urlHost+"/OpenApi/WaterDevice/GetIntegratedWGWListForMobile?token="+model.user_token
            }
            
        }
        
        /// 获取水网关详情
        static var waterGatewayDetailAPI:String{
            return urlHost+"/OpenApi/WaterDevice/GetMultiplexWGWByIdForMobile?token="+model.user_token
        }
        /// 获取水网信号详情
        static var waterSignalDetailAPI:String{
            return urlHost+"/OpenApi/WaterDevice/GetWaterSignalDetailForMobile?token="+model.user_token
        }
        /// 获取一体式水源监测设备详情
        static var integratedDetailAPI:String{
            return urlHost+"/OpenApi/WaterDevice/GetIntegratedWGWByIdForMobile?token="+model.user_token
        }
        
        
        ///添加水网关
        static var addWaterGatewayAPI: String{
            return urlHost+"/OpenApi/WaterDevice/PutMultiplexWGWForMobile?token="+model.user_token
        }
        
        ///添加水信号
        static var addWaterSignalAPI: String{
            return urlHost+"/OpenApi/WaterDevice/PutWaterSignalForMobile?token="+model.user_token
        }
        
        ///添加一体式水源设备
        static var addWaterSourceAPI: String{
            
            if isV2Api{
                return urlHost+"/OpenApi/WaterDevice/PutIntegratedNeatWGWForMobileV2?token="+model.user_token
            }else{
                return urlHost+"/OpenApi/WaterDevice/PutIntegratedNeatWGWForMobile?token="+model.user_token
            }
            
        }
        
        ///编辑水网关
        static var editWaterGatewayAPI: String{
            return urlHost+"/OpenApi/WaterDevice/PostMultiplexWGWForMobile?token="+model.user_token
        }
        
        ///编辑水信号
        static var editWaterSignalAPI: String{
            return urlHost+"/OpenApi/WaterDevice/PostWaterSignalForMobile?token="+model.user_token
        }
        
        ///编辑一体式水源设备
        static var editWaterSourceAPI: String{
            return urlHost+"/OpenApi/WaterDevice/PostIntegratedWGWForMobile?token="+model.user_token
        }
        
        ///删除水网关
        static var deleteWaterGatewayAPI: String{
            return urlHost+"/OpenApi/WaterDevice/DeleteWGWForMobile?token="+model.user_token
        }
        
        ///删除水信号
        static var deleteWaterSignalAPI: String{
            return urlHost+"/OpenApi/WaterDevice/DeleteWaterSignalByIdForMobile?token="+model.user_token
        }
        
        ///删除一体式水源设备
        static var deleteWaterSourceAPI: String{
            return urlHost+"/OpenApi/WaterDevice/DeleteIntegratedWGWForMobile?token="+model.user_token
        }
        
        
    }
    
    /// 智慧用电相关
    class EleDevice:NSObject{
        
        static var model: LoginResultModel {
            let model = DataArchiver.unArchiver(key:ArchireKey.UserInfor)
            if model != nil {
                return  model as! LoginResultModel
            } else {
                return LoginResultModel()
            }
        }
        
        ///获取智慧用电网关列表
        static var electricGatewayListAPI: String {
            return urlHost+"/OpenApi/ElectricGateway/GetGatewayListForMobile?token="+model.user_token
        }
        
        ///获取一体式智慧用电网关列表
        static var integratedElectricGatewayListApi: String {
            
            if isV2Api {
                return urlHost+"/OpenApi/IntegratedElectricGateway/GetListForMobileV2?token="+model.user_token
            }else{
                return urlHost+"/OpenApi/IntegratedElectricGateway/GetListForMobile?token="+model.user_token
            }
        
        }
        ///添加一体式智慧用电网关
        static var addIntegratedElectricGatewayListApi: String {
            
            return urlHost+"/OpenApi/IntegratedElectricGateway/PostForMobile?token="+model.user_token
            
        }
        
        ///获取一体式智慧用电网关详情
        static var getIntegratedElectricGatewayDetailApi: String {
            
            return urlHost+"/OpenApi/IntegratedElectricGateway/GetByIdForMobile?token="+model.user_token
            
        }
        ///删除一体式智慧用电网关
        static var deleteIntegratedElectricGatewayApi: String {
            
            return urlHost+"/OpenApi/IntegratedElectricGateway/DeleteForMobile?token="+model.user_token
            
        }
        ///编辑一体式智慧用电网关
        static var editIntegratedElectricGatewayApi: String {
            
            return urlHost+"/OpenApi/IntegratedElectricGateway/PutForMobile?token="+model.user_token
            
        }
        

        ///获取智慧用电网关详情
        static var electricGatewayDetailAPI: String{
            return urlHost+"/OpenApi/ElectricGateway/GetGatewayByIdForMobile?token="+model.user_token
        }
        ///绑定智慧用电网关
        static var bindElectricGatewayAPI: String {
            return urlHost+"/OpenApi/ElectricGateway/BindGatewayById?token="+model.user_token
        }
        /// 获取未绑定的智慧用电设备
        static var getElectricGatewayByCodeAPI: String{
            return urlHost+"/OpenApi/ElectricGateway/GetNotBindGatewayByCode?token="+model.user_token
        }
        
        /// 编辑智慧用电设备
        static var editElectricGatewayByCodeAPI: String{
            return urlHost+"/OpenApi/ElectricGateway/PostGatewayForMobile?token="+model.user_token
        }
        
        /// 删除智慧用电设备
        static var deleteElectricGatewayAPI: String{
            return urlHost+"/OpenApi/ElectricGateway/ClearExtInfo?token="+model.user_token
        }
        
        /// 获取智慧用电设备运维记录
        static var electricGatewayTaskInforAPI: String{
            return urlHost+"/OpenApi/ElectricGateway/GetTaskInfosForMobile?token="+model.user_token
        }
        
        /// 获取智慧用电设备升级文件列表
        static var electricGatewayUpgradeFileAPI: String{
            return urlHost+"/OpenApi/ElectricUpgradeFile/Get?token="+model.user_token
        }
        
        /// 智慧用电设备版本升级
        static var electricGatewayUpgradeAPI: String{
            return urlHost+"/OpenApi/ElectricGateway/PutTask?token="+model.user_token
        }
        
        /// 智慧用电设备获取通道设置信息
        static var electricGatewayChanleSetInforAPI: String{
            return urlHost+"/OpenApi/ElectricGateway/GetComponetList?token="+model.user_token
        }
        
        /// 智慧用电设备更新通道设置信息
        static var electricGatewayUpdateChanleSetInforAPI: String{
            return urlHost+"/OpenApi/ElectricGateway/PostComponetList?token="+model.user_token
        }
        
        /// 智慧用电设备复位
        static var electricGatewayRestAPI: String{
            return urlHost+"/OpenApi/ElectricGateway/ResetGateway?token="+model.user_token
        }
        
        /// 智慧用电设备获取通道实时值
        static var electricGatewayChanleValueAPI: String{
            return urlHost+"/OpenApi/ElectricDeviceData/GetComponetDataByGatewayId?token="+model.user_token
        }
        
        
        /// 反控
        /// 消音
        static var electricSelenceCommandAPI: String{
            return urlHost+"/OpenApi/ElectricGateway/CMute?token="+model.user_token
        }
        /// 复位
        static var electricResetCommandAPI: String{
            return urlHost+"/OpenApi/ElectricGateway/Reset?token="+model.user_token
        }
        /// 自检
        static var electricSelfCheckCommandAPI: String{
            return urlHost+"/OpenApi/ElectricGateway/SelfCheck?token="+model.user_token
        }
        /// 分闸
        static var electricFloodgateCommandAPI: String{
            return urlHost+"/OpenApi/ElectricGateway/SeparateBreak?token="+model.user_token
        }
        
    }
    
    /// NB设备相关
    class NBDevice:NSObject{
        
        static var model: LoginResultModel {
            let model = DataArchiver.unArchiver(key:ArchireKey.UserInfor)
            if model != nil {
                return  model as! LoginResultModel
            } else {
                return LoginResultModel()
            }
        }
        
        ///获取NB设备列表
        static var nbDeviceListAPI: String {
            return urlHost+"/OpenApi/NBDevice/QueryNBDeviceListForMobile?token="+model.user_token
        }
        
        /// 获取NB设备详情
        static var nbDetailAPI:String{
            return urlHost+"/OpenApi/NBDevice/QueryNBDeviceByIdForMobile?token="+model.user_token
        }
        ///添加NB设备
        static var addNBDeviceAPI: String {
            return urlHost+"/OpenApi/NBDevice/AddNBDeviceForMobile?token="+model.user_token
        }
        ///编辑NB设备
        static var editNBDeviceAPI: String {
            return urlHost+"/OpenApi/NBDevice/UpdateNBDeviceForMobile?token="+model.user_token
        }
        ///删除NB设备
        static var deleteNBDeviceAPI: String {
            return urlHost+"/OpenApi/NBDevice/DeleteNBDeviceForMobile?token="+model.user_token
        }
        ///NB设备 消音
        static var cmuteNBDeviceAPI: String {
            return urlHost+"/OpenApi/NBDevice/CMute?token="+model.user_token
        }
    }
    
    /// 8140
    class HomeGateway:NSObject{
        
        static var model: LoginResultModel {
            let model = DataArchiver.unArchiver(key:ArchireKey.UserInfor)
            if model != nil {
                return  model as! LoginResultModel
            } else {
                return LoginResultModel()
            }
        }
        
        ///获取8140网关列表
        static var homeGatewayListAPI: String {
            return urlHost+"/OpenApi/IoTDevice/GetListForMobile?token="+model.user_token
        }
        
        /// 获取8140网关详情
        static var homeGatewayDetailAPI:String{
            return urlHost+"/OpenApi/IoTDevice/GetDetailForMObile?token="+model.user_token
        }
        ///添加8140网关
        static var addHomeGatewayAPI: String {
            return urlHost+"/OpenApi/IoTDevice/PostForMobile?token="+model.user_token
        }
        ///编辑8140网关
        static var editHomeGatewayAPI: String {
            return urlHost+"/OpenApi/IoTDevice/EditForMobile?token="+model.user_token
        }
        ///删除8140网关
        static var deleteHomeGatewayAPI: String {
            return urlHost+"/OpenApi/IoTDevice/DeleteForMObile?token="+model.user_token
        }
        ///下属器件测试
        static var homeGatewayDeviceTestAPI: String {
            return urlHost+"/OpenApi/IoTDevice/Test?token="+model.user_token
        }
        ///下属器件详情
        static var homeGatewayDeviceDetailAPI: String {
            return urlHost+"/OpenApi/IoTDevice/GetComponentDetailForMobile?token="+model.user_token
        }
        ///下属器件列表
        static var homeGatewayDeviceListAPI: String {
            return urlHost+"/OpenApi/IoTDevice/GetComponentListForMobile?token="+model.user_token
        }
        ///下属器件删除
        static var deleteHomeGatewayDeviceAPI: String {
            return urlHost+"/OpenApi/IoTDevice/DeleteDevice?token="+model.user_token
        }
        ///下属器件编辑
        static var editHomeGatewayDeviceAPI: String {
            return urlHost+"/OpenApi/IOTDevice/PutDevice?token="+model.user_token
        }
    }
    
    class GasDetector: NSObject {
        
        static var model: LoginResultModel {
            let model = DataArchiver.unArchiver(key:ArchireKey.UserInfor)
            if model != nil {
                return  model as! LoginResultModel
            } else {
                return LoginResultModel()
            }
        }
        
        ///获取可燃气体探测器网关列表
        static var gasDetectorListAPI: String {
            return urlHost+"/OpenApi/FlammableGas/QueryHMDeviceListForMobile?token="+model.user_token
        }
        
        /// 获取可燃气体探测器详情
        static var gasDetectorDetailAPI:String{
            return urlHost+"/OpenApi/flammableGas/QueryHMDeviceDetialForMobile?token="+model.user_token
        }
        ///添加可燃气体探测器
        static var addGasDetectorAPI: String {
            return urlHost+"/OpenApi/flammableGas/AddHMDevice?token="+model.user_token
        }
        ///编辑可燃气体探测器
        static var editGasDetectorAPI: String {
            return urlHost+"/OpenApi/flammableGas/UpdateHMDeviceForMobile?token="+model.user_token
        }
        ///删除可燃气体探测器
        static var deleteGasDetectorAPI: String {
            return urlHost+"/OpenApi/flammableGas/DeleteHMDeviceForMobile?token="+model.user_token
        }
        
        ///获取可燃气体探测器设置信息
        static var getGasDetectorSetAPI: String {
            return urlHost+"/OpenApi/FlammableGas/SetUpInfo?token="+model.user_token
        }
        
        ///设置可燃气体探测器设置信息
        static var postGasDetectorSetAPI: String {
            return urlHost+"/OpenApi/flammableGas/HMHandleControlForMobile?token="+model.user_token
        }
        
    }
    
    /// 共用的设备管理相关接口
    class DeviceManager: NSObject {
        
        static var model: LoginResultModel {
            let model = DataArchiver.unArchiver(key:ArchireKey.UserInfor)
            if model != nil {
                return  model as! LoginResultModel
            } else {
                return LoginResultModel()
            }
        }
        
        /// 获取网关列表
        static var getGatewayListAPI: String {
            return urlHost+"/OpenApi/device/GetListForMobile?token="+model.user_token
        }
        /// 获取网关或器件详情
        static var getGatewayOrDeviceDetailAPI:String{
            return urlHost+"/OpenApi/device/GetDetailForMobile?token="+model.user_token
        }
        /// 添加网关或器件
        static var postAddGatewayOrDeviceAPI: String {
            return urlHost+"/OpenApi/device/PostForMobile?token="+model.user_token
        }
        ///编辑网关或器件信息
        static var postEditGatewayOrDeviceAPI: String {
            return urlHost+"/OpenApi/device/PutForMobile?token="+model.user_token
        }
        ///删除网关或器件
        static var postDeleteGatewayOrDeviceAPI: String {
            return urlHost+"/OpenApi/device/ReomoveForMobile?token="+model.user_token
        }
        ///获取器件列表
        static var getDeviceListAPI: String {
            return urlHost+"/OpenApi/device/GetSignalListForMobile?token="+model.user_token
        }
        ///单个命令的设备反控
        static var postSignalCommandAPI: String {
            return urlHost+"/OpenApi/Device/DownCommandForMobile?token="+model.user_token
        }
        
    }
    
    
    ///巡检相关接口
    ///获取频率相关接口
    class InspectionFrequency:NSObject{
        
        static var model: LoginResultModel {
            let model = DataArchiver.unArchiver(key:ArchireKey.UserInfor)
            if model != nil {
                return  model as! LoginResultModel
            } else {
                return LoginResultModel()
            }
        }
        
        ///获取巡检任务频率列表
        static var TaskFrequency: String {
            return urlHost+"/OpenApi/TaskMod/GetFrequency?domainID="+model.user_domain_id+"&enterpriseID="+model.user_enterprise_id
        }
    }
    
    ///巡检任务相关接口
    class InspectionTask:NSObject{
        
        static var model: LoginResultModel {
            let model = DataArchiver.unArchiver(key:ArchireKey.UserInfor)
            if model != nil {
                return  model as! LoginResultModel
            } else {
                return LoginResultModel()
            }
        }
        
        ///获取巡检任务列表
        static var TaskList: String {
            return urlHost+"/OpenApi/ResultTask/GetPatrolTaskForMobile?token="+model.user_token
        }
        ///获取巡检任务下巡检点列表
        static var TaskPointList: String {
            return urlHost+"/OpenApi/Point/QueryTaskPointListByTaskIdForMobile?token="+model.user_token
        }
        ///获取巡检任务完成率
        static var TaskFinishRate: String{
            return urlHost + "/OpenApi/ResultTask/GetFinishRate?token="+model.user_token
        }
        
        ///巡检结果上报
        static var TaskResultUpload: String{
            return urlHost + "/OpenApi/HDanger/TaskResultUploadForMobile?token="+model.user_token
            
        }
        
        ///隐患上报
        static var TaskWarningUpload: String{
            return urlHost + "/OpenApi/HDanger/TroubleUploadForMobile?token="+model.user_token
        }
        
    }
    ///历史巡检相关接口
    class InspectionHistory:NSObject{
        
        static var model: LoginResultModel {
            let model = DataArchiver.unArchiver(key:ArchireKey.UserInfor)
            if model != nil {
                return  model as! LoginResultModel
            } else {
                return LoginResultModel()
            }
        }
        
        ///获取历史巡检列表
        static var TaskList: String {
            return urlHost+"/OpenApi/ResultTask/GetTaskResultRecordForMobile?token="+model.user_token
        }
        
    }
    ///隐患点相关接口
    class InspectionTroublePoint:NSObject{
        
        static var model: LoginResultModel {
            let model = DataArchiver.unArchiver(key:ArchireKey.UserInfor)
            if model != nil {
                return  model as! LoginResultModel
            } else {
                return LoginResultModel()
            }
        }
        
        ///获取隐患点列表
        static var TaskPointList: String {
            return urlHost+"/OpenApi/Point/GetTroublePointList?token="+model.user_token
        }
        
    }
    ///隐患上报相关接口
    class InspectionTroubleUpload:NSObject{
        
        static var model: LoginResultModel {
            let model = DataArchiver.unArchiver(key:ArchireKey.UserInfor)
            if model != nil {
                return  model as! LoginResultModel
            } else {
                return LoginResultModel()
            }
        }
        
        ///根据标签获取巡检点信息
        static var getPointByTagApi: String {
            return urlHost+"/OpenApi/Point/QueryPointDetailByTagCodeForMobile?token="+model.user_token
        }
        
        ///隐患上报接口
        static var troubleUpLoad:String{
            return urlHost+"/OpenApi/HDanger/TroubleUploadNoTaskForMobile?token="+model.user_token
        }
    
        
        
    }
    ///巡检点绑定相关接口
    class InspectionBindPoint:NSObject{
        
        static var model: LoginResultModel {
            let model = DataArchiver.unArchiver(key:ArchireKey.UserInfor)
            if model != nil {
                return  model as! LoginResultModel
            } else {
                return LoginResultModel()
            }
        }
        
        ///获取巡检点列表
        static var PointList: String {
            return urlHost+"/OpenApi/Point/QueryPatrolPointForMobile?token="+model.user_token
        }
        
        ///巡检点绑定
        static var BindPoint: String {
            return urlHost+"/OpenApi/Point/BindTagForMobile?token="+model.user_token
        }
        
    }
    ///消息提醒相关接口
    class InspectionPushMessage:NSObject{
        
        static var model: LoginResultModel {
            let model = DataArchiver.unArchiver(key:ArchireKey.UserInfor)
            if model != nil {
                return  model as! LoginResultModel
            } else {
                return LoginResultModel()
            }
        }
        
        ///获取消息列表
        static var MessageList: String {
            return urlHost+"/OpenApi/PushMessage/GetPushMessageListForMobile?token="+model.user_token
        }
        ///获取未读消息数
        static var getUnReadMessageCount: String {
            return urlHost+"/OpenApi/PushMessage/GetUserUnReadPushMsgCountForMobile?token="+model.user_token
        }
        ///修改未读消息数
        static var editUnReadMessageCount: String {
            return urlHost+"/OpenApi/PushMessage/UpdatePushMsgStatus?token="+model.user_token
        }
    }
    ///隐患确认相关接口
    class InspectionToubleConfirm:NSObject{
        
        static var model: LoginResultModel {
            let model = DataArchiver.unArchiver(key:ArchireKey.UserInfor)
            if model != nil {
                return  model as! LoginResultModel
            } else {
                return LoginResultModel()
            }
        }
        
        ///获取隐患确认列表
        static var getTroubleConfirmListList: String {
            return urlHost+"/OpenApi/HDanger/GetOsiTroubleConfirmListForMobile?token="+model.user_token
        }
        
        ///隐患处理
        static var handleTrouble: String {
            return urlHost+"/OpenApi/HDanger/TroubleConfirmForMobile?token="+model.user_token
        }
        
    }
    ///快速巡检相关接口
    class InspectionQuickly:NSObject{
        
        static var model: LoginResultModel {
                       let model = DataArchiver.unArchiver(key:ArchireKey.UserInfor)
                       if model != nil {
                           return  model as! LoginResultModel
                       } else {
                           return LoginResultModel()
                       }
                   }
        
        static var getProject:String{
            return urlHost+"/OpenApi/Point/QueryTaskPointDetailByTagCodeForMobile?token=" + model.user_token
        }
        
    }

    ///警情日志相关
    class AlarmLog:NSObject{
        
        static var model: LoginResultModel {
           let model = DataArchiver.unArchiver(key:ArchireKey.UserInfor)
           if model != nil {
               return  model as! LoginResultModel
           } else {
               return LoginResultModel()
           }
        }
        /// 获取警情日志
        static var getAlarmList:String{
            return urlHost+"/OpenApi/DeviceMonitor/GetEventListForMobile?token=" + model.user_token
        }
        
        /// 获取设备运行状态
        static var getDeviceStatue:String{
            return urlHost+"/OpenApi/DeviceMonitor/GetDeviceStatusByEventForMobile?token=" + model.user_token
        }
        
        /// 获取警情详情
        static var getAlarmDetail:String{
            return urlHost+"/OpenApi/DeviceMonitor/GetEventDetailForMobile?token=" + model.user_token
        }
        
        /// 警情处理
        static var handleAlarm:String{
            return urlHost+"/OpenApi/DeviceMonitor/EventHandleForMobile?token=" + model.user_token
        }
        
        /// 获取警情日志处理信息
        static var getAlarmHandleDetail:String{
            return urlHost+"/OpenApi/DeviceMonitor/GetEventHandleForMobile?token=" + model.user_token
        }
        
        /// 获取联动视频信息
        static var getVideoPlayDetail:String{
            return urlHost+"/OpenApi/VideoDevice/GetVideoPlayDeatilForMobile?token=" + model.user_token
        }
        
    }

    ///消防维保
    class FireProtection:NSObject{
        
        static var model: LoginResultModel {
           let model = DataArchiver.unArchiver(key:ArchireKey.UserInfor)
           if model != nil {
               return  model as! LoginResultModel
           } else {
               return LoginResultModel()
           }
        }
        
        /// 获取维保统计完成率
        static var getProtectionStatistics:String{
            return urlHost+"/OpenApi/MtaStatistics/GetMobileHomeStatistics?token=" + model.user_token
        }
        /// 获取维保任务
        static var getProtectionTaskList:String{
            return urlHost+"/OpenApi/MaintenancePlan/GetTaskListForMobile?token=" + model.user_token
        }
        /// 执行维保任务
        static var execProtectionTask:String{
            return urlHost+"/OpenApi/MaintenancePlan/ExecPlanForMobile?token=" + model.user_token
        }
        /// 获取维保计划列表
        static var getProtectionPlanList:String{
            return urlHost+"/OpenApi/MaintenancePlan/GetPlanListForMobile?token=" + model.user_token
        }
        /// 获取维保任务详情列表
        static var getProtectionTaskListByPlanId:String{
            return urlHost+"/OpenApi/MaintenancePlan/GetTaskListByPlanIdForMobile?token=" + model.user_token
        }
        /// 获取维保任务执行详情列表
        static var getProtectionTaskDetail:String{
            return urlHost+"/OpenApi/MaintenancePlan/GetTaskDetailForMobile?token=" + model.user_token
        }
        /// 获取维保消息列表
        static var getProtectionMessageList:String{
            return urlHost+"/OpenApi/MtaUserMessageWarm/GetMessageWarmListForMobile?token=" + model.user_token
        }
        /// 获取维保材料列表
        static var getMaterialsList:String{
            return urlHost+"/OpenApi/Consumable/GetMaterialsListForMobile?token=" + model.user_token
        }
        /// 入场打卡
        static var handleEntrance:String{
            return urlHost+"/OpenApi/OrderManagement/HandleEntranceForMobile?token=" + model.user_token
        }
        /// 工单列表
        static var getOrderList:String{
            return urlHost+"/OpenApi/OrderManagement/GetOrderListForMobile?token=" + model.user_token
        }
        /// 工单处理
        static var handleOrder:String{
            return urlHost+"/OpenApi/OrderManagement/HandleOrderForMobile?token=" + model.user_token
        }
        /// 工单处理历史记录
        static var orderHandleHistory:String{
            return urlHost+"/OpenApi/OrderManagement/GetOrderHandlerHistoryForMobile?token=" + model.user_token
        }
        /// 工单处理详情
        static var orderHandleDetail:String{
            return urlHost+"/OpenApi/OrderManagement/GetOrderHandlerDetailForMobile?token=" + model.user_token
        }
    
        
    }
    
    ///消防维保
    class QureyStats:NSObject{
        
        static var model: LoginResultModel {
           let model = DataArchiver.unArchiver(key:ArchireKey.UserInfor)
           if model != nil {
               return  model as! LoginResultModel
           } else {
               return LoginResultModel()
           }
        }
        
        /// 获取用电安全系统网关列表
        static var getQureyStatsGatewayList:String{
            return urlHost+"/OpenApi/ElectricGateWayQuery/GetListForMobile?token=" + model.user_token
        }
        /// 获取用电安全网关GIS地址
        static var getQureyStatsGatewayGis:String{
            return urlHost+"/OpenApi/ElectricGateWayQuery/GetGisLocationForMobile?token=" + model.user_token
        }
        /// 获取网关事件列表
        static var getQureyStatsGatewayEventList:String{
            return urlHost+"/OpenApi/ElectricGateWayQuery/GetEventListForMobile?token=" + model.user_token
        }
        /// 获取网关通道列表
        static var getQureyStatsGatewayChannelList:String{
            return urlHost+"/OpenApi/ElectricGateWayQuery/GetChannelListForMobile?token=" + model.user_token
        }
        /// 查询通道趋势图接口
        static var getQureyStats:String{
            return urlHost+"/OpenApi/ElectricGateWayQuery/GetChannelHistoryDataForMobile?token=" + model.user_token
        }
        
    
        
    }

}
