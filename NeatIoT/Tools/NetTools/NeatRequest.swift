//
//  NeatRequest.swift
//  NeatIoT
//
//  Created by neat on 2019/10/21.
//  Copyright © 2019 Neat. All rights reserved.
//

import UIKit
import Alamofire

class NeatRequest: NSObject {
    
    // 全局变量
    static let instance = NeatRequest.init()
    // 默认参数
    var defaultParameters: [String:String] {
        let model = DataArchiver.unArchiver(key:ArchireKey.UserInfor) as! LoginResultModel
        return ["token":model.user_token]
    }
    
    var sessionManager:Session?
    
    override init() {
        super.init()
        
        let configer = URLSessionConfiguration.default
        configer.timeoutIntervalForRequest = 30
        configer.httpAdditionalHeaders = ["token":"sss"]
        sessionManager = Session.init(configuration: configer, delegate: SessionDelegate(), rootQueue: DispatchQueue(label: "org.alamofire.session.rootQueue"), startRequestsImmediately: true, requestQueue: nil, serializationQueue: nil, interceptor: nil, serverTrustManager: nil, redirectHandler: nil, cachedResponseHandler: nil, eventMonitors: [])
        
    }
    
    /// 格式化接口请求返回错误信息
    /// - Parameter response: 返回数据
    func formateErrorResponse(response:BaseErrorResponse) -> BaseErrorResponse {
        
        //初始化错误返回对象实例
        let errResponse = BaseErrorResponse()
        
        if response.errCode == -1 {
            errResponse.errCode = RequestResponseCode.notImplemented
            errResponse.errMessage = "接口返回数据解析失败"
            return errResponse
        }
    
        switch response.errCode {
        
        case RequestResponseCode.validateError:
                //验证错误
                errResponse.errCode = RequestResponseCode.unDefineError
                if response.errMessage.isEmpty {
                    errResponse.errMessage = "错误码：401"
                }else{
                    errResponse.errMessage = response.errMessage
                }
                
                break
            case RequestResponseCode.tokenValidateError:
                //token验证错误
                errResponse.errCode = RequestResponseCode.unDefineError
                if response.errMessage.isEmpty {
                    errResponse.errMessage = "错误码：402"
                }else{
                    errResponse.errMessage = response.errMessage
                }
                
                break
            case RequestResponseCode.forbidden:
                //拒绝访问
                errResponse.errCode = RequestResponseCode.unDefineError
                if response.errMessage.isEmpty {
                    errResponse.errMessage = "错误码：403"
                }else{
                    errResponse.errMessage = response.errMessage
                }
                
                self.perform(#selector(tokenTimeOut), with: nil, afterDelay: 2)
                
                break
            case RequestResponseCode.notFound:
                //没找到
                errResponse.errCode = RequestResponseCode.unDefineError
                if response.errMessage.isEmpty {
                    errResponse.errMessage = "错误码：404"
                }else{
                    errResponse.errMessage = response.errMessage
                }
                
                break
            case RequestResponseCode.tokenTimeOut:
                //token过期
                errResponse.errCode = RequestResponseCode.unDefineError
                if response.errMessage.isEmpty {
                    errResponse.errMessage = "错误码：405"
                }else{
                    errResponse.errMessage = response.errMessage
                }
                self.perform(#selector(tokenTimeOut), with: nil, afterDelay: 2)
                
                break
            case RequestResponseCode.unDefineError:
                //服务器错误
                errResponse.errCode = RequestResponseCode.unDefineError
                if response.errMessage.isEmpty {
                    errResponse.errMessage = "错误码：500"
                }else{
                    errResponse.errMessage = response.errMessage
                }
                break
            case RequestResponseCode.notImplemented:
                //未实现错误
                errResponse.errCode = RequestResponseCode.unDefineError
                if response.errMessage.isEmpty {
                    errResponse.errMessage = "错误码：501"
                }else{
                    errResponse.errMessage = response.errMessage
                }
                
                break
            default:
                //未知错误
                errResponse.errCode = RequestResponseCode.notImplemented
                errResponse.errMessage = "未知错误"
                break
        }
        return errResponse
    }
    
    
    /// 格式化接口请求失败错误信息
    /// - Parameter faile: 错误信息
    func formateFailedResponse(faile:AFError) -> BaseErrorResponse {
        //初始化错误返回对象实例
        var errorCode:Int = 0
        var errorMessage:String = "未知错误"
        
        switch faile {
            case .sessionTaskFailed(let error):
                
                errorCode = error._code
                
                if error._code == NSURLErrorTimedOut {
                    errorMessage = "请求超时"
                }else if error._code == NSURLErrorNotConnectedToInternet {
                    errorMessage = "无网络连接，请检查网络"
                }else if error._code == NSURLErrorCannotConnectToHost {
                    errorMessage = "无法连接服务器"
                }
            
                break
            case .invalidURL:
                errorCode = 10000
                errorMessage = "无效的网络请求URL"
                break
                
            default:
                break
        }
        
        let errResponse = BaseErrorResponse()
        errResponse.errCode = errorCode
        errResponse.errMessage = errorMessage
        return errResponse
    }
    
    func handleErrorResponse(res:Any) -> BaseErrorResponse {
        let josnStr = res as? [String: Any]
        let errorResponse = BaseErrorResponse.deserialize(from: josnStr) ?? BaseErrorResponse.init()
        return errorResponse
    }
    
    /// 登录接口
    /// - Parameter userName: 用户名
    /// - Parameter password: 密码
    /// - Parameter success: 成功回调方法
    /// - Parameter failed: 失败回调方法
    func login(userName:String,password:String,success:@escaping (_ response:LoginResponse) -> Void,failed:@escaping (_ failed:BaseErrorResponse) -> Void) -> Void {
        
        sessionManager!.request(UrlHost.Auth.loginAPI, method: .post, parameters: ["userId":userName,"uPwdSecret":password.MD5(),"isNoLogin":"1","clientType":"3"], encoder: JSONParameterEncoder.default, headers: nil, interceptor: nil).responseJSON { (response) in
            
            let statusCode = response.response?.statusCode
            
            //初始化错误返回对象实例
            let errResponse = BaseErrorResponse()
                
            switch response.result{
                
                case .success(let res):
                    
                    if statusCode == 200{
                        
                        //初始化数据返回对象实例
                        var respModel:LoginResponse?
                        let resultModel = LoginResultModel.init()
                        let jsonStr = res as? [String: Any]
                        
                        if (jsonStr?.keys.count == 3 && ((jsonStr?.keys.contains("result")) != nil)) {
                            respModel = LoginResponse.init()
                            respModel!.code = jsonStr?["code"] as? Int ?? 200
                            respModel!.message = jsonStr?["message"] as? String ?? ""
                            
                            if let resultDic = jsonStr?["result"] as? [String : Any]  {
                                
                                resultModel.user_id = resultDic["userId"] as? String ?? ""
                                resultModel.user_name = resultDic["name"] as? String ?? ""
                                resultModel.user_token = resultDic["token"] as? String ?? ""
                                resultModel.user_domain_id = resultDic["domainId"] as? String ?? ""
                                resultModel.user_enterprise_id = resultDic["enterpriseId"] as? String ?? ""
                                resultModel.user_domain_name = resultDic["domainName"] as? String ?? ""
                                resultModel.user_enterprise_name = resultDic["entName"] as? String ?? ""
                                resultModel.user_childDomain_name = resultDic["childDomainName"] as? String ?? ""
                                resultModel.user_roles = resultDic["roles"] as? [String] ?? []
                                
                                respModel?.response_data = resultModel
                                
                            }
                        }else{
                            respModel = nil
                        }
                        
//                        let respModel = LoginResponse.deserialize(from: jsonStr)

                        if respModel != nil {
                                if respModel?.code == 200 {
                                    success(respModel!)
                                }else{
                                    let response = BaseErrorResponse.deserialize(from: jsonStr)
                                    failed(self.formateErrorResponse(response: response!))
                                }
                            }else{
                                errResponse.errCode = RequestResponseCode.dataParsingException
                                errResponse.errMessage = "数据解析异常"
                                failed(errResponse)
                            }
                    }else{
                       
                        failed(self.formateErrorResponse(response: self.handleErrorResponse(res: res)))
        
                    }
                break
                
                case .failure(let error):
                    failed(self.formateFailedResponse(faile: error))
                break
            
            }
        }
    }
    
    /// 退出登录
    /// - Parameter success: 成功回调
    /// - Parameter failed: 失败回调
    func logout(success:@escaping (_ response:BaseResponse) -> Void,failed:@escaping (_ failed:BaseErrorResponse) -> Void) -> Void {
        sessionManager?.request(UrlHost.Auth.logoutAPI, method: .get, parameters: defaultParameters, encoder: URLEncodedFormParameterEncoder.default, headers: nil, interceptor: nil).responseJSON(completionHandler: { (response) in
            
            let statusCode = response.response?.statusCode
            //初始化错误返回对象实例
            let errResponse = BaseErrorResponse()
                
            switch response.result{
                
                case .success(let res):
                    
                    if statusCode == 200{
                    
                        //初始化数据返回对象实例
                        let josnStr = res as? [String: Any]
                        let respModel = BaseResponse.deserialize(from: josnStr)
                        
                        if respModel != nil {
                                if respModel?.code == 200 {
                                    success(respModel!)
                                }else{
                                    let response = BaseErrorResponse.deserialize(from: josnStr)
                                    failed(self.formateErrorResponse(response: response!))
                                }
                            }else{
                                errResponse.errCode = RequestResponseCode.dataParsingException
                                errResponse.errMessage = "数据解析异常"
                                failed(errResponse)
                            }
                    }else{
                        
                        failed(self.formateErrorResponse(response: self.handleErrorResponse(res: res)))
                    }
                break
                
                case .failure(let error):
                    failed(self.formateFailedResponse(faile: error))
                break
            
            }
        })
    }
    
    /// 获取主页菜单列表
    /// - Parameter success: 成功
    /// - Parameter failed: 失败
    func getMenuList(success:@escaping (_ response:MainMenuResponse) -> Void,failed:@escaping (_ failed:BaseErrorResponse) -> Void) -> Void {
        
        let headers: HTTPHeaders = [
            "Accept": "application/json"
        ]
        
        sessionManager!.request(UrlHost.SysAuth.sysMenuAPI, method: .get, parameters: ["systemIds":"8","terminalId":"3"], encoder:URLEncodedFormParameterEncoder.default, headers: headers, interceptor: nil).responseJSON { (response) in
        
            
            let statusCode = response.response?.statusCode
            //初始化错误返回对象实例
            let errResponse = BaseErrorResponse()
                
            switch response.result{
                
                case .success(let res):

                    if statusCode == 200{
                    
                        //初始化数据返回对象实例
                        let josnStr = res as? [String: Any]
                        let respModel = MainMenuResponse.deserialize(from: josnStr)
                        
                        if respModel != nil {
                                if respModel?.code == 200 {
                                    success(respModel!)
                                }else{
                                    let response = BaseErrorResponse.deserialize(from: josnStr)
                                    failed(self.formateErrorResponse(response: response!))
                                }
                            }else{
                                errResponse.errCode = RequestResponseCode.dataParsingException
                                errResponse.errMessage = "数据解析异常"
                                failed(errResponse)
                            }
                    }else{
                        
                        failed(self.formateErrorResponse(response: self.handleErrorResponse(res: res)))
                        
                    }
                break
                
                case .failure(let error):
            
                    failed(self.formateFailedResponse(faile: error))
                    
                break
            
            }
        
        }
    
    }
    
    /// 获取设备主页设备列表
    /// - Parameter url: 请求URL
    /// - Parameter model_type: v2版接口使用的设备类型
    /// - Parameter keyword: 搜索用关键字
    /// - Parameter domainId: 筛选用中心ID
    /// - Parameter enterpriseId: 筛选用企业ID
    /// - Parameter buildingId: 筛选用建筑ID
    /// - Parameter keypartId: 筛选用部位ID
    /// - Parameter orderByColumn: 根据条件排序 默认0  None = 0,Status = 1,HeartTime = 2,Name = 3,omainName = 4,
    /// EntName = 5,BuildingName = 6,KeyPartName = 7,Code = 8,
    /// - Parameter isDescOrder: 是否倒序
    /// - Parameter pageIndex: 当前页数
    /// - Parameter success: 成功回调
    /// - Parameter failed: 失败回调
    func getDeviceHostList(url:String,model_type:String?,keyword:String,domainId:String,enterpriseId:String,buildingId:String,keypartId:String,orderByColumn:String,isDescOrder:Bool,pageIndex:Int,success:@escaping (_ response:DeviceHostListResponse) -> Void,failed:@escaping (_ failed:BaseErrorResponse) -> Void) {
        
        var parameters:[String:String]
        
        if nil != model_type {
            
            parameters = ["keyword":keyword,
            "domainId":domainId,
            "enterpriseId":enterpriseId,
            "buildingId":buildingId,
            "keypartId":keypartId,
            "orderByColumn":"9",
            "isDescOrder":String.init(isDescOrder),
            "pageIndex":String.init(pageIndex),
            "pageSize":"10",
            "model":model_type!]
            
            
        }else{
            
            parameters = ["keyword":keyword,
            "domainId":domainId,
            "enterpriseId":enterpriseId,
            "buildingId":buildingId,
            "keypartId":keypartId,
            "orderByColumn":"9",
            "isDescOrder":String.init(isDescOrder),
            "pageIndex":String.init(pageIndex),
            "pageSize":"10"]
            
        }
        
        sessionManager?.request(url, method: .get, parameters: parameters, encoder: URLEncodedFormParameterEncoder.default, headers: nil, interceptor: nil).responseJSON(completionHandler: { (response) in
            
            let statusCode = response.response?.statusCode
            //初始化错误返回对象实例
            let errResponse = BaseErrorResponse()
                
            switch response.result{
                
                case .success(let res):
                    
                    if statusCode == 200{
                    
                        //初始化数据返回对象实例
                        let josnStr = res as? [String: Any]
                        let respModel = DeviceHostListResponse.deserialize(from: josnStr)
                        
                        if respModel != nil {
                                if respModel?.code == 200 {
                                    success(respModel!)
                                }else{
                                    let response = BaseErrorResponse.deserialize(from: josnStr)
                                    failed(self.formateErrorResponse(response: response!))
                                }
                            }else{
                                errResponse.errCode = RequestResponseCode.dataParsingException
                                errResponse.errMessage = "数据解析异常"
                                failed(errResponse)
                            }
                    }else{
                        failed(self.formateErrorResponse(response: self.handleErrorResponse(res: res)))
                    }
                break
                
                case .failure(let error):
                    
                    failed(self.formateFailedResponse(faile: error))
                    
                break
            
            }
            
        })
        
        
    }
    
    /// 获取二级设备列表
    /// - Parameters:
    ///   - url: url
    ///   - orderByColumn: 排序字段 默认name
    ///   - isDescOrder: 是否倒叙
    ///   - pageIndex: 页数
    ///   - success: 成功回调
    ///   - failed: 失败回调
    func getSubDeviceList(url:String,idParameter:String,fatherId:String,isDescOrder:Bool,pageIndex:Int,success:@escaping (_ response:SubDeviceListResponse) -> Void,failed:@escaping (_ failed:BaseErrorResponse) -> Void) {
        
        let parameters = ["orderByColumn":"name",
                          "isDescOrder":String.init(isDescOrder),
                          "pageIndex":String.init(pageIndex),
                          "pageSize":"12",
                          idParameter:fatherId]
        
        sessionManager?.request(url, method: .get, parameters: parameters, encoder: URLEncodedFormParameterEncoder.default, headers: nil, interceptor: nil).responseJSON(completionHandler: { (response) in
            
            let statusCode = response.response?.statusCode
            //初始化错误返回对象实例
            let errResponse = BaseErrorResponse()
                
            switch response.result{
                
                case .success(let res):
                    
                    if statusCode == 200{
                    
                        //初始化数据返回对象实例
                        let josnStr = res as? [String: Any]
                        let respModel = SubDeviceListResponse.deserialize(from: josnStr)
                        
                        if respModel != nil {
                                if respModel?.code == 200 {
                                    success(respModel!)
                                }else{
                                    let response = BaseErrorResponse.deserialize(from: josnStr)
                                    failed(self.formateErrorResponse(response: response!))
                                }
                            }else{
                                errResponse.errCode = RequestResponseCode.dataParsingException
                                errResponse.errMessage = "数据解析异常"
                                failed(errResponse)
                            }
                    }else{
                        failed(self.formateErrorResponse(response: self.handleErrorResponse(res: res)))
                    }
                break
                
                case .failure(let error):
                    
                    failed(self.formateFailedResponse(faile: error))
                    
                break
            
            }
            
        })
        
    }
    
    
    /// 获取登录用户权限内的中心列表
    /// - Parameter success: 成功返回
    /// - Parameter failed: 失败返回
    func getUserDomainList(success:@escaping (_ response:IDNameResponse) -> Void,failed:@escaping (_ failed:BaseErrorResponse) -> Void) {
        
        self.sessionManager?.request(UrlHost.SysAuth.sysDomainListAPI, method: .get, parameters: ["":""], encoder: URLEncodedFormParameterEncoder.default, headers: nil, interceptor: nil).responseJSON(completionHandler: { (response) in
            
            let statusCode = response.response?.statusCode
            //初始化错误返回对象实例
            let errResponse = BaseErrorResponse()
                
            switch response.result{
                
                case .success(let res):
                    
                    if statusCode == 200{
                    
                        //初始化数据返回对象实例
                        let josnStr = res as? [String: Any]
                        let respModel = IDNameResponse.deserialize(from: josnStr)
                        
                        if respModel != nil {
                                if respModel?.code == 200 {
                                    success(respModel!)
                                }else{
                                    let response = BaseErrorResponse.deserialize(from: josnStr)
                                    failed(self.formateErrorResponse(response: response!))
                                }
                            }else{
                                errResponse.errCode = RequestResponseCode.dataParsingException
                                errResponse.errMessage = "数据解析异常"
                                failed(errResponse)
                            }
                    }else{
                        failed(self.formateErrorResponse(response: self.handleErrorResponse(res: res)))
                    }
                break
                
                case .failure(let error):
                    
                    failed(self.formateFailedResponse(faile: error))
                    
                break
            
            }
            
        })
    }
    
    /// 根据中心id获取权限内的企业列表
    /// - Parameter domainId: 中心id
    /// - Parameter success: 成功返回
    /// - Parameter failed: 失败返回
    func getUserEntList(domainId:String,success:@escaping (_ response:IDNameResponse) -> Void,failed:@escaping (_ failed:BaseErrorResponse) -> Void) {
        
        let parameters = ["domainId":domainId]
        
        self.sessionManager?.request(UrlHost.SysAuth.sysEntListAPI, method: .get, parameters: parameters, encoder: URLEncodedFormParameterEncoder.default, headers: nil, interceptor: nil).responseJSON(completionHandler: { (response) in
            
            let statusCode = response.response?.statusCode
            //初始化错误返回对象实例
            let errResponse = BaseErrorResponse()
                
            switch response.result{
                
                case .success(let res):
                    
                    if statusCode == 200{
                    
                        //初始化数据返回对象实例
                        let josnStr = res as? [String: Any]
                        let respModel = IDNameResponse.deserialize(from: josnStr)
                        
                        if respModel != nil {
                                if respModel?.code == 200 {
                                    success(respModel!)
                                }else{
                                    let response = BaseErrorResponse.deserialize(from: josnStr)
                                    failed(self.formateErrorResponse(response: response!))
                                }
                            }else{
                                errResponse.errCode = RequestResponseCode.dataParsingException
                                errResponse.errMessage = "数据解析异常"
                                failed(errResponse)
                            }
                    }else{
                        failed(self.formateErrorResponse(response: self.handleErrorResponse(res: res)))
                    }
                break
                
                case .failure(let error):
                    
                    failed(self.formateFailedResponse(faile: error))
                    
                break
            
            }
            
        })
    }
    
    /// 根据企业id获取权限内的建筑列表
    /// - Parameter entId: 单位id
    /// - Parameter success: 成功返回
    /// - Parameter failed: 失败返回
    func getUserBuildingList(entId:String,success:@escaping (_ response:IDNameResponse) -> Void,failed:@escaping (_ failed:BaseErrorResponse) -> Void) {
        
        let parameters = ["entId":entId]
        
        self.sessionManager?.request(UrlHost.SysAuth.sysBuildingListAPI, method: .get, parameters: parameters, encoder: URLEncodedFormParameterEncoder.default, headers: nil, interceptor: nil).responseJSON(completionHandler: { (response) in
            
            let statusCode = response.response?.statusCode
            //初始化错误返回对象实例
            let errResponse = BaseErrorResponse()
                
            switch response.result{
                
                case .success(let res):
                    
                    if statusCode == 200{
                    
                        //初始化数据返回对象实例
                        let josnStr = res as? [String: Any]
                        let respModel = IDNameResponse.deserialize(from: josnStr)
                        
                        if respModel != nil {
                                if respModel?.code == 200 {
                                    success(respModel!)
                                }else{
                                    let response = BaseErrorResponse.deserialize(from: josnStr)
                                    failed(self.formateErrorResponse(response: response!))
                                }
                            }else{
                                errResponse.errCode = RequestResponseCode.dataParsingException
                                errResponse.errMessage = "数据解析异常"
                                failed(errResponse)
                            }
                    }else{
                        failed(self.formateErrorResponse(response: self.handleErrorResponse(res: res)))
                    }
                break
                
                case .failure(let error):
                    
                    failed(self.formateFailedResponse(faile: error))
                    
                break
            
            }
            
        })
    }
    
    /// 根据建筑id获取权限内的部位列表
    /// - Parameter buildingId: 建筑id
    /// - Parameter success: 成功返回
    /// - Parameter failed: 失败返回
    func getUserKeypartList(buildingId:String,success:@escaping (_ response:IDNameResponse) -> Void,failed:@escaping (_ failed:BaseErrorResponse) -> Void) {
        
        let parameters = ["buildingId":buildingId]
        
        self.sessionManager?.request(UrlHost.SysAuth.sysKeypartListAPI, method: .get, parameters: parameters, encoder: URLEncodedFormParameterEncoder.default, headers: nil, interceptor: nil).responseJSON(completionHandler: { (response) in
            
            let statusCode = response.response?.statusCode
            //初始化错误返回对象实例
            let errResponse = BaseErrorResponse()
                
            switch response.result{
                
                case .success(let res):
                    
                    if statusCode == 200{
                    
                        //初始化数据返回对象实例
                        let josnStr = res as? [String: Any]
                        let respModel = IDNameResponse.deserialize(from: josnStr)
                        
                        if respModel != nil {
                                if respModel?.code == 200 {
                                    success(respModel!)
                                }else{
                                    let response = BaseErrorResponse.deserialize(from: josnStr)
                                    failed(self.formateErrorResponse(response: response!))
                                }
                            }else{
                                errResponse.errCode = RequestResponseCode.dataParsingException
                                errResponse.errMessage = "数据解析异常"
                                failed(errResponse)
                            }
                    }else{
                        failed(self.formateErrorResponse(response: self.handleErrorResponse(res: res)))
                    }
                break
                
                case .failure(let error):
                    
                    failed(self.formateFailedResponse(faile: error))
                    
                break
            
            }
            
        })
    }
    
    /// 根据指定类别 获取数据集
    /// - Parameters:
    ///   - category: 数据类别
    ///   - success: 成功回调
    ///   - failed: 失败回调
    func getInforListByCategory(category:String,model:String,success:@escaping (_ response:IDNameResponse) -> Void,failed:@escaping (_ failed:BaseErrorResponse) -> Void) {
        
        let parameters = ["category":category,"model":model]
        
        self.sessionManager?.request(UrlHost.SysAuth.sysGetInforByCategoreAPI, method: .get, parameters: parameters, encoder: URLEncodedFormParameterEncoder.default, headers: nil, interceptor: nil).responseJSON(completionHandler: { (response) in
            
            let statusCode = response.response?.statusCode
            //初始化错误返回对象实例
            let errResponse = BaseErrorResponse()
                
            switch response.result{
                
                case .success(let res):
                    
                    if statusCode == 200{
                    
                        //初始化数据返回对象实例
                        let josnStr = res as? [String: Any]
                        let respModel = IDNameResponse.deserialize(from: josnStr)
                        
                        if respModel != nil {
                                if respModel?.code == 200 {
                                    success(respModel!)
                                }else{
                                    let response = BaseErrorResponse.deserialize(from: josnStr)
                                    failed(self.formateErrorResponse(response: response!))
                                }
                            }else{
                                errResponse.errCode = RequestResponseCode.dataParsingException
                                errResponse.errMessage = "数据解析异常"
                                failed(errResponse)
                            }
                    }else{
                        failed(self.formateErrorResponse(response: self.handleErrorResponse(res: res)))
                    }
                break
                
                case .failure(let error):
                    
                    failed(self.formateFailedResponse(faile: error))
                    
                break
            
            }
            
        })
        
    }
    
    /// 添加设备
    /// - Parameters:
    ///   - apiPath: 接口API
    ///   - jsonParame: 上传数据
    ///   - success: 成功回调
    ///   - failed: 失败回调
    func addDevice(apiPath:String,jsonParame:AddDeviceBaseModel,success:@escaping (_ response:DeviceAddResponse) -> Void,failed:@escaping (_ failed:BaseErrorResponse) -> Void) {
        
        self.sessionManager?.request(apiPath, method: .post, parameters: jsonParame, encoder: JSONParameterEncoder.default, headers: nil, interceptor: nil).responseJSON { (response) in
        
            
            let statusCode = response.response?.statusCode
            //初始化错误返回对象实例
            let errResponse = BaseErrorResponse()
                
            switch response.result{
                
                case .success(let res):

                    if statusCode == 200{
                    
                        //初始化数据返回对象实例
                        let josnStr = res as? [String: Any]
                        let respModel = DeviceAddResponse.deserialize(from: josnStr)
                        
                        if respModel != nil {
                                if respModel?.code == 200 {
                                    success(respModel!)
                                }else{
                                    let response = BaseErrorResponse.deserialize(from: josnStr)
                                    failed(self.formateErrorResponse(response: response!))
                                }
                            }else{
                                errResponse.errCode = RequestResponseCode.dataParsingException
                                errResponse.errMessage = "数据解析异常"
                                failed(errResponse)
                            }
                    }else{
                        failed(self.formateErrorResponse(response: self.handleErrorResponse(res: res)))
                    }
                break
                
                case .failure(let error):
            
                    failed(self.formateFailedResponse(faile: error))
                    
                break
            
            }
        
        }
        
    }
    /// 编辑设备
    /// - Parameters:
    ///   - apiPath: 接口API
    ///   - jsonParame: 上传数据
    ///   - success: 成功回调
    ///   - failed: 失败回调
    func editDevice(apiPath:String,jsonParame:DeviceInforBaseModel,success:@escaping (_ response:BaseResponse) -> Void,failed:@escaping (_ failed:BaseErrorResponse) -> Void) {
        
        self.sessionManager?.request(apiPath, method: .post, parameters: jsonParame, encoder: JSONParameterEncoder.default, headers: nil, interceptor: nil).responseJSON(completionHandler: { (response) in
    
            let statusCode = response.response?.statusCode
            //初始化错误返回对象实例
            let errResponse = BaseErrorResponse()
                
            switch response.result{
                
                case .success(let res):
                    
                    if statusCode == 200{
                    
                        //初始化数据返回对象实例
                        let josnStr = res as? [String: Any]
                        let respModel = BaseResponse.deserialize(from: josnStr)
                        
                        if respModel != nil {
                            if respModel?.code == 200 {
                                success(respModel!)
                            }else{
                                let response = BaseErrorResponse.deserialize(from: josnStr)
                                failed(self.formateErrorResponse(response: response!))
                            }
                        }else{
                            errResponse.errCode = RequestResponseCode.dataParsingException
                            errResponse.errMessage = "数据解析异常"
                            failed(errResponse)
                        }
                    }else{
                        failed(self.formateErrorResponse(response: self.handleErrorResponse(res: res)))
                    }
                break
                
                case .failure(let error):
                    
                    failed(self.formateFailedResponse(faile: error))
                    
                break
            
            }
        })
        
    }
    
    /// 删除设备
    /// - Parameters:
    ///   - apiPath: 接口API
    ///   - jsonParame: 上传数据
    ///   - success: 成功回调
    ///   - failed: 失败回调
    func deleteDevice(apiPath:String,paraModel:DeletDeviceModel,success:@escaping (_ response:DeleteDeviceResponse) -> Void,failed:@escaping (_ failed:BaseErrorResponse) -> Void) {
        
        if paraModel.uitdId == "" {
            self.sessionManager?.request(apiPath, method: .post, parameters: paraModel.ids, encoder: JSONParameterEncoder.default, headers: nil, interceptor: nil).responseJSON(completionHandler: { (response) in
            
                    let statusCode = response.response?.statusCode
                    //初始化错误返回对象实例
                    let errResponse = BaseErrorResponse()
                        
                    switch response.result{
                        
                        case .success(let res):
                            
                            if statusCode == 200{
                            
                                //初始化数据返回对象实例
                                let josnStr = res as? [String: Any]
                                let respModel = DeleteDeviceResponse.deserialize(from: josnStr)
                                
                                if respModel != nil {
                                    if respModel?.code == 200 {
                                        success(respModel!)
                                    }else{
                                        let response = BaseErrorResponse.deserialize(from: josnStr)
                                        failed(self.formateErrorResponse(response: response!))
                                    }
                                }else{
                                    errResponse.errCode = RequestResponseCode.dataParsingException
                                    errResponse.errMessage = "数据解析异常"
                                    failed(errResponse)
                                }
                            }else{
                                failed(self.formateErrorResponse(response: self.handleErrorResponse(res: res)))
                            }
                        break
                        
                        case .failure(let error):
                            
                            failed(self.formateFailedResponse(faile: error))
                            
                        break
                    
                    }
                })
        }else{
            self.sessionManager?.request(apiPath, method: .post, parameters: paraModel, encoder: JSONParameterEncoder.default, headers: nil, interceptor: nil).responseJSON(completionHandler: { (response) in
            
                    let statusCode = response.response?.statusCode
                    //初始化错误返回对象实例
                    let errResponse = BaseErrorResponse()
                        
                    switch response.result{
                        
                        case .success(let res):
                            
                            if statusCode == 200{
                            
                                //初始化数据返回对象实例
                                let josnStr = res as? [String: Any]
                                let respModel = DeleteDeviceResponse.deserialize(from: josnStr)
                                
                                if respModel != nil {
                                    if respModel?.code == 200 {
                                        success(respModel!)
                                    }else{
                                        let response = BaseErrorResponse.deserialize(from: josnStr)
                                        failed(self.formateErrorResponse(response: response!))
                                    }
                                }else{
                                    errResponse.errCode = RequestResponseCode.dataParsingException
                                    errResponse.errMessage = "数据解析异常"
                                    failed(errResponse)
                                }
                            }else{
                                failed(self.formateErrorResponse(response: self.handleErrorResponse(res: res)))
                            }
                        break
                        
                        case .failure(let error):
                            
                            failed(self.formateFailedResponse(faile: error))
                            
                        break
                    
                    }
                })
        }
        
    }
    /// NB设备消音
    /// - Parameters:
    ///   - apiPath: 接口API
    ///   - jsonParame: 上传数据
    ///   - success: 成功回调
    ///   - failed: 失败回调
    func postNBCmute(gid:String,cid:String,success:@escaping (_ response:BaseResponse) -> Void,failed:@escaping (_ failed:BaseErrorResponse) -> Void) {
        
        let param = [String:String]()
    
        let url = UrlHost.NBDevice.cmuteNBDeviceAPI + "&gid=" + gid + "&cid=" + cid
        
        self.sessionManager?.request(url, method: .post, parameters: param, encoder: JSONParameterEncoder.default, headers: nil, interceptor: nil).responseJSON(completionHandler: { (response) in
    
            let statusCode = response.response?.statusCode
            //初始化错误返回对象实例
            let errResponse = BaseErrorResponse()
                
            switch response.result{
                
                case .success(let res):
                    
                    if statusCode == 200{
                    
                        //初始化数据返回对象实例
                        let josnStr = res as? [String: Any]
                        let respModel = BaseResponse.deserialize(from: josnStr)
                        
                        if respModel != nil {
                            if respModel?.code == 200 {
                                success(respModel!)
                            }else{
                                let response = BaseErrorResponse.deserialize(from: josnStr)
                                failed(self.formateErrorResponse(response: response!))
                            }
                        }else{
                            errResponse.errCode = RequestResponseCode.dataParsingException
                            errResponse.errMessage = "数据解析异常"
                            failed(errResponse)
                        }
                    }else{
                        failed(self.formateErrorResponse(response: self.handleErrorResponse(res: res)))
                    }
                break
                
                case .failure(let error):
                    
                    failed(self.formateFailedResponse(faile: error))
                    
                break
            
            }
        })
        
    }
    /// 8140设备器件测试
    /// - Parameters:
    ///   - id: 器件ID
    ///   - success: 成功回调
    ///   - failed: 失败回调
    func postHomeGatewayDeviceTest(id:String,success:@escaping (_ response:BaseResponse) -> Void,failed:@escaping (_ failed:BaseErrorResponse) -> Void) {
        
        let param = [String:String]()
    
        let url = UrlHost.HomeGateway.homeGatewayDeviceTestAPI + "&id=" + id
        
        self.sessionManager?.request(url, method: .post, parameters: param, encoder: JSONParameterEncoder.default, headers: nil, interceptor: nil).responseJSON(completionHandler: { (response) in
    
            let statusCode = response.response?.statusCode
            //初始化错误返回对象实例
            let errResponse = BaseErrorResponse()
                
            switch response.result{
                
                case .success(let res):
                    
                    if statusCode == 200{
                    
                        //初始化数据返回对象实例
                        let josnStr = res as? [String: Any]
                        let respModel = BaseResponse.deserialize(from: josnStr)
                        
                        if respModel != nil {
                            if respModel?.code == 200 {
                                success(respModel!)
                            }else{
                                let response = BaseErrorResponse.deserialize(from: josnStr)
                                failed(self.formateErrorResponse(response: response!))
                            }
                        }else{
                            errResponse.errCode = RequestResponseCode.dataParsingException
                            errResponse.errMessage = "数据解析异常"
                            failed(errResponse)
                        }
                    }else{
                        failed(self.formateErrorResponse(response: self.handleErrorResponse(res: res)))
                    }
                break
                
                case .failure(let error):
                    
                    failed(self.formateFailedResponse(faile: error))
                    
                break
            
            }
        })
        
    }
    
    /// 获取智慧用电未绑定设备
    /// - Parameters:
    ///   - code: 设备码
    ///   - success: 成功回调
    ///   - failed: 失败回调
    func getNoBindEleGateway(code:String,success:@escaping (_ response:EleSearchResponse) -> Void,failed:@escaping (_ failed:BaseErrorResponse) -> Void){
        
        let parameters = ["code":code]
        
        self.sessionManager?.request(UrlHost.EleDevice.getElectricGatewayByCodeAPI, method: .get, parameters: parameters, encoder: URLEncodedFormParameterEncoder.default, headers: nil, interceptor: nil).responseJSON(completionHandler: { (response) in
            
            let statusCode = response.response?.statusCode
            //初始化错误返回对象实例
            let errResponse = BaseErrorResponse()
                
            switch response.result{
                
                case .success(let res):
                    
                    if statusCode == 200{
                    
                        //初始化数据返回对象实例
                        let josnStr = res as? [String: Any]
                        let respModel = EleSearchResponse.deserialize(from: josnStr)
                        
                        if respModel != nil {
                            if respModel?.code == 200 {
                                success(respModel!)
                            }else{
                                let response = BaseErrorResponse.deserialize(from: josnStr)
                                failed(self.formateErrorResponse(response: response!))
                            }
                        }else{
                            errResponse.errCode = RequestResponseCode.dataParsingException
                            errResponse.errMessage = "数据解析异常"
                            failed(errResponse)
                        }
                    }else{
                        failed(self.formateErrorResponse(response: self.handleErrorResponse(res: res)))
                    }
                break
                
                case .failure(let error):
                    
                    failed(self.formateFailedResponse(faile: error))
                    
                break
            
            }
            
        })
        
        
    }
    
    /// 获取设备详情信息
    /// - Parameters:
    ///   - url: 接口URL
    ///   - id: 设备id
    ///   - success: 成功回调
    ///   - failed: 失败回调
    func getDeviceDetailInfor(url:String,id:String,success:@escaping (_ response:DeviceDetailResponse) -> Void,failed:@escaping (_ failed:BaseErrorResponse) -> Void){
        
        let parameters = ["id":id]
        
        self.sessionManager?.request(url, method: .get, parameters: parameters, encoder: URLEncodedFormParameterEncoder.default, headers: nil, interceptor: nil).responseJSON(completionHandler: { (response) in
            
            let statusCode = response.response?.statusCode
            //初始化错误返回对象实例
            let errResponse = BaseErrorResponse()
                
            switch response.result{
                
                case .success(let res):
                    
                    if statusCode == 200{
                    
                        //初始化数据返回对象实例
                        let josnStr = res as? [String: Any]
                        let respModel = DeviceDetailResponse.deserialize(from: josnStr)
                        
                        if respModel != nil {
                            if respModel?.code == 200 {
                                success(respModel!)
                            }else{
                                let response = BaseErrorResponse.deserialize(from: josnStr)
                                failed(self.formateErrorResponse(response: response!))
                            }
                        }else{
                            errResponse.errCode = RequestResponseCode.dataParsingException
                            errResponse.errMessage = "数据解析异常"
                            failed(errResponse)
                        }
                    }else{
                        failed(self.formateErrorResponse(response: self.handleErrorResponse(res: res)))
                    }
                break
                
                case .failure(let error):
                    
                    failed(self.formateFailedResponse(faile: error))
                    
                break
            
            }
            
        })
        
        
    }

    /// 获取可燃气体探测器设置详情信息
    /// - Parameters:
    ///   - url: 接口URL
    ///   - id: 设备id
    ///   - success: 成功回调
    ///   - failed: 失败回调
    func getGasDetectorSetDetailInfor(url:String,id:String,success:@escaping (_ response:GasDetectorSetDetailResponse) -> Void,failed:@escaping (_ failed:BaseErrorResponse) -> Void){
        
        let parameters = ["id":id]
        
        self.sessionManager?.request(url, method: .get, parameters: parameters, encoder: URLEncodedFormParameterEncoder.default, headers: nil, interceptor: nil).responseJSON(completionHandler: { (response) in
            
            let statusCode = response.response?.statusCode
            //初始化错误返回对象实例
            let errResponse = BaseErrorResponse()
                
            switch response.result{
                
                case .success(let res):
                    
                    if statusCode == 200{
                    
                        //初始化数据返回对象实例
                        let josnStr = res as? [String: Any]
                        let respModel = GasDetectorSetDetailResponse.deserialize(from: josnStr)
                        
                        if respModel != nil {
                            if respModel?.code == 200 {
                                success(respModel!)
                            }else{
                                let response = BaseErrorResponse.deserialize(from: josnStr)
                                failed(self.formateErrorResponse(response: response!))
                            }
                        }else{
                            errResponse.errCode = RequestResponseCode.dataParsingException
                            errResponse.errMessage = "数据解析异常"
                            failed(errResponse)
                        }
                    }else{
                        failed(self.formateErrorResponse(response: self.handleErrorResponse(res: res)))
                    }
                break
                
                case .failure(let error):
                    
                    failed(self.formateFailedResponse(faile: error))
                    
                break
            
            }
            
        })
        
        
    }
    
    /// 可燃气体探测器 设置
    /// - Parameters:
    ///   - id: 器件ID
    ///   - success: 成功回调
    ///   - failed: 失败回调
    func postGasDetetorSet(setModel:GasDetectorChangeSetModel,success:@escaping (_ response:BaseResponse) -> Void,failed:@escaping (_ failed:BaseErrorResponse) -> Void) {
    
        let url = UrlHost.GasDetector.postGasDetectorSetAPI
        
        self.sessionManager?.request(url, method: .post, parameters: setModel, encoder: JSONParameterEncoder.default, headers: nil, interceptor: nil).responseJSON(completionHandler: { (response) in
    
            let statusCode = response.response?.statusCode
            //初始化错误返回对象实例
            let errResponse = BaseErrorResponse()
                
            switch response.result{
                
                case .success(let res):
                    
                    if statusCode == 200{
                    
                        //初始化数据返回对象实例
                        let josnStr = res as? [String: Any]
                        let respModel = BaseResponse.deserialize(from: josnStr)
                        
                        if respModel != nil {
                            if respModel?.code == 200 {
                                success(respModel!)
                            }else{
                                let response = BaseErrorResponse.deserialize(from: josnStr)
                                failed(self.formateErrorResponse(response: response!))
                            }
                        }else{
                            errResponse.errCode = RequestResponseCode.dataParsingException
                            errResponse.errMessage = "数据解析异常"
                            failed(errResponse)
                        }
                    }else{
                        failed(self.formateErrorResponse(response: self.handleErrorResponse(res: res)))
                    }
                break
                
                case .failure(let error):
                    
                    failed(self.formateFailedResponse(faile: error))
                    
                break
            
            }
        })
        
    }
    
    
    
    /// 获取智慧用电设备运维记录
    /// - Parameters:
    ///   - id: 设备id
    ///   - success: 成功回调
    ///   - failed: 失败回调
    func getSmartGatewayRec(id:String,success:@escaping (_ response:SmartGatewayTaskInforResponse) -> Void,failed:@escaping (_ failed:BaseErrorResponse) -> Void){
        
        let parameters = ["id":id]
        
        self.sessionManager?.request(UrlHost.EleDevice.electricGatewayTaskInforAPI, method: .get, parameters: parameters, encoder: URLEncodedFormParameterEncoder.default, headers: nil, interceptor: nil).responseJSON(completionHandler: { (response) in
            
            let statusCode = response.response?.statusCode
            //初始化错误返回对象实例
            let errResponse = BaseErrorResponse()
                
            switch response.result{
                
                case .success(let res):
                    
                    if statusCode == 200{
                    
                        //初始化数据返回对象实例
                        let josnStr = res as? [String: Any]
                        let respModel = SmartGatewayTaskInforResponse.deserialize(from: josnStr)
                        
                        if respModel != nil {
                            if respModel?.code == 200 {
                                success(respModel!)
                            }else{
                                let response = BaseErrorResponse.deserialize(from: josnStr)
                                failed(self.formateErrorResponse(response: response!))
                            }
                        }else{
                            errResponse.errCode = RequestResponseCode.dataParsingException
                            errResponse.errMessage = "数据解析异常"
                            failed(errResponse)
                        }
                    }else{
                        failed(self.formateErrorResponse(response: self.handleErrorResponse(res: res)))
                    }
                break
                
                case .failure(let error):
                    
                    failed(self.formateFailedResponse(faile: error))
                    
                break
            
            }
            
        })
        
        
    }
    /// 获取智慧用电设备运升级文件列表
    /// - Parameters:
    ///   - success: 成功回调
    ///   - failed: 失败回调
    func getSmartGatewayUpgradeFile(success:@escaping (_ response:SmartGatewayVersionResponse) -> Void,failed:@escaping (_ failed:BaseErrorResponse) -> Void){

        self.sessionManager?.request(UrlHost.EleDevice.electricGatewayUpgradeFileAPI, method: .get, parameters:defaultParameters, encoder: URLEncodedFormParameterEncoder.default, headers: nil, interceptor: nil).responseJSON(completionHandler: { (response) in
            
            let statusCode = response.response?.statusCode
            //初始化错误返回对象实例
            let errResponse = BaseErrorResponse()
                
            switch response.result{
                
                case .success(let res):
                    
                    if statusCode == 200{
                    
                        //初始化数据返回对象实例
                        let josnStr = res as? [String: Any]
                        let respModel = SmartGatewayVersionResponse.deserialize(from: josnStr)
                        
                        if respModel != nil {
                            if respModel?.code == 200 {
                                success(respModel!)
                            }else{
                                let response = BaseErrorResponse.deserialize(from: josnStr)
                                failed(self.formateErrorResponse(response: response!))
                            }
                        }else{
                            errResponse.errCode = RequestResponseCode.dataParsingException
                            errResponse.errMessage = "数据解析异常"
                            failed(errResponse)
                        }
                    }else{
                        failed(self.formateErrorResponse(response: self.handleErrorResponse(res: res)))
                    }
                break
                
                case .failure(let error):
                    
                    failed(self.formateFailedResponse(faile: error))
                    
                break
            
            }
            
        })
        
        
    }
    
    /// 智慧用电设备升级
    /// - Parameters:
    /// - gatewayId: 网关id
    /// - fileId: 文件id
    /// - success: 成功回调
    /// - failed: 失败回调
    func postSmartGatewayUpgrade(gatewayId:String, fileId:String, success:@escaping (_ response:BaseResponse) -> Void,failed:@escaping (_ failed:BaseErrorResponse) -> Void){

//        let parameters = ["gatewayId":gatewayId,"fileId":fileId]
        let url = UrlHost.EleDevice.electricGatewayUpgradeAPI+"&gatewayId="+gatewayId+"&fileId="+fileId
        self.sessionManager?.request(url, method: .post, parameters: defaultParameters, encoder: URLEncodedFormParameterEncoder.default, headers: nil, interceptor: nil).responseJSON(completionHandler: { (response) in
            
            let statusCode = response.response?.statusCode
            //初始化错误返回对象实例
            let errResponse = BaseErrorResponse()
                
            switch response.result{
                
                case .success(let res):
                    
                    if statusCode == 200{
                    
                        //初始化数据返回对象实例
                        let josnStr = res as? [String: Any]
                        let respModel = BaseResponse.deserialize(from: josnStr)
                        
                        if respModel != nil {
                            if respModel?.code == 200 {
                                success(respModel!)
                            }else{
                                let response = BaseErrorResponse.deserialize(from: josnStr)
                                failed(self.formateErrorResponse(response: response!))
                            }
                        }else{
                            errResponse.errCode = RequestResponseCode.dataParsingException
                            errResponse.errMessage = "数据解析异常"
                            failed(errResponse)
                        }
                    }else{
                        failed(self.formateErrorResponse(response: self.handleErrorResponse(res: res)))
                    }
                break
                
                case .failure(let error):
                    
                    failed(self.formateFailedResponse(faile: error))
                    
                break
            
            }
            
        })
    }
    
    /// 获取智慧用电设备 通道设置信息
    /// - Parameters:
    ///   - success: 成功回调
    ///   - failed: 失败回调
    func getSmartGatewayChanleSetInfor(gatewayId:String, success:@escaping (_ response:SmartGatewayChanleSetResponse) -> Void,failed:@escaping (_ failed:BaseErrorResponse) -> Void){
        
        let parameters = ["gatewayId":gatewayId]

        self.sessionManager?.request(UrlHost.EleDevice.electricGatewayChanleSetInforAPI, method: .get, parameters:parameters, encoder: URLEncodedFormParameterEncoder.default, headers: nil, interceptor: nil).responseJSON(completionHandler: { (response) in
            
            let statusCode = response.response?.statusCode
            //初始化错误返回对象实例
            let errResponse = BaseErrorResponse()
                
            switch response.result{
                
                case .success(let res):
                    
                    if statusCode == 200{
                    
                        //初始化数据返回对象实例
                        let josnStr = res as? [String: Any]
                        let respModel = SmartGatewayChanleSetResponse.deserialize(from: josnStr)
                        
                        if respModel != nil {
                            if respModel?.code == 200 {
                                success(respModel!)
                            }else{
                                let response = BaseErrorResponse.deserialize(from: josnStr)
                                failed(self.formateErrorResponse(response: response!))
                            }
                        }else{
                            errResponse.errCode = RequestResponseCode.dataParsingException
                            errResponse.errMessage = "数据解析异常"
                            failed(errResponse)
                        }
                    }else{
                        failed(self.formateErrorResponse(response: self.handleErrorResponse(res: res)))
                    }
                break
                
                case .failure(let error):
                    
                    failed(self.formateFailedResponse(faile: error))
                    
                break
            
            }
            
        })
        
        
    }
    
    /// 智慧用电设备更新通道设置
    /// - Parameters:
    /// - model: 更新数据model
    /// - success: 成功回调
    /// - failed: 失败回调
    func postSmartGatewayUpdateChanleInfor(model:UpdateChanleSet, success:@escaping (_ response:BaseResponse) -> Void,failed:@escaping (_ failed:BaseErrorResponse) -> Void){
        
        self.sessionManager?.request(UrlHost.EleDevice.electricGatewayUpdateChanleSetInforAPI, method: .post, parameters: model, encoder: JSONParameterEncoder.default, headers: nil, interceptor: nil).responseJSON(completionHandler: { (response) in
            
            let statusCode = response.response?.statusCode
            //初始化错误返回对象实例
            let errResponse = BaseErrorResponse()
                
            switch response.result{
                
                case .success(let res):
                    
                    if statusCode == 200{
                    
                        //初始化数据返回对象实例
                        let josnStr = res as? [String: Any]
                        let respModel = BaseResponse.deserialize(from: josnStr)
                        
                        if respModel != nil {
                            if respModel?.code == 200 {
                                success(respModel!)
                            }else{
                                let response = BaseErrorResponse.deserialize(from: josnStr)
                                failed(self.formateErrorResponse(response: response!))
                            }
                        }else{
                            errResponse.errCode = RequestResponseCode.dataParsingException
                            errResponse.errMessage = "数据解析异常"
                            failed(errResponse)
                        }
                    }else{
                        failed(self.formateErrorResponse(response: self.handleErrorResponse(res: res)))
                    }
                break
                
                case .failure(let error):
                    
                    failed(self.formateFailedResponse(faile: error))
                    
                break
            
            }
            
        })
    }
    
    /// 智慧用电设备 复位
    /// - Parameters:
    /// - deviceId: 设备id
    /// - success: 成功回调
    /// - failed: 失败回调
    func postSmartGatewayRest(deviceId:String, success:@escaping (_ response:BaseResponse) -> Void,failed:@escaping (_ failed:BaseErrorResponse) -> Void){
        
        let url = UrlHost.EleDevice.electricGatewayRestAPI + "&id=" + deviceId
        self.sessionManager?.request(url, method: .post, parameters: defaultParameters, encoder: JSONParameterEncoder.default, headers: nil, interceptor: nil).responseJSON(completionHandler: { (response) in
            
            let statusCode = response.response?.statusCode
            //初始化错误返回对象实例
            let errResponse = BaseErrorResponse()
                
            switch response.result{
                
                case .success(let res):
                    
                    if statusCode == 200{
                    
                        //初始化数据返回对象实例
                        let josnStr = res as? [String: Any]
                        let respModel = BaseResponse.deserialize(from: josnStr)
                        
                        if respModel != nil {
                            if respModel?.code == 200 {
                                success(respModel!)
                            }else{
                                let response = BaseErrorResponse.deserialize(from: josnStr)
                                failed(self.formateErrorResponse(response: response!))
                            }
                        }else{
                            errResponse.errCode = RequestResponseCode.dataParsingException
                            errResponse.errMessage = "数据解析异常"
                            failed(errResponse)
                        }
                    }else{
                        failed(self.formateErrorResponse(response: self.handleErrorResponse(res: res)))
                    }
                break
                
                case .failure(let error):
                    
                    failed(self.formateFailedResponse(faile: error))
                    
                break
            
            }
            
        })
    }
    
    /// 共用反控 接口
    /// - Parameters:
    /// - deviceId: 设备id
    /// - success: 成功回调
    /// - failed: 失败回调
    func postCommandPublic(url:String, inforModel:CommandReportModel, success:@escaping (_ response:BaseResponse) -> Void,failed:@escaping (_ failed:BaseErrorResponse) -> Void){
        
        self.sessionManager?.request(url, method: .post, parameters: inforModel, encoder: JSONParameterEncoder.default, headers: nil, interceptor: nil).responseJSON(completionHandler: { (response) in
            
            let statusCode = response.response?.statusCode
            //初始化错误返回对象实例
            let errResponse = BaseErrorResponse()
                
            switch response.result{
                
                case .success(let res):
                    
                    if statusCode == 200{
                    
                        //初始化数据返回对象实例
                        let josnStr = res as? [String: Any]
                        let respModel = BaseResponse.deserialize(from: josnStr)
                        
                        if respModel != nil {
                            if respModel?.code == 200 {
                                success(respModel!)
                            }else{
                                let response = BaseErrorResponse.deserialize(from: josnStr)
                                failed(self.formateErrorResponse(response: response!))
                            }
                        }else{
                            errResponse.errCode = RequestResponseCode.dataParsingException
                            errResponse.errMessage = "数据解析异常"
                            failed(errResponse)
                        }
                    }else{
                        failed(self.formateErrorResponse(response: self.handleErrorResponse(res: res)))
                    }
                break
                
                case .failure(let error):
                    
                    failed(self.formateFailedResponse(faile: error))
                    
                break
            
            }
            
        })
    }
    
    
    /// 获取智慧用电设备 通道实时数据
    /// - Parameters:
    ///   - success: 成功回调
    ///   - failed: 失败回调
    func getSmartGatewayChanleValueInfor(gatewayId:String, success:@escaping (_ response:SmartGatewayChannleValueResponse) -> Void,failed:@escaping (_ failed:BaseErrorResponse) -> Void){
        
        let parameters = ["gatewayId":gatewayId]

        self.sessionManager?.request(UrlHost.EleDevice.electricGatewayChanleValueAPI, method: .get, parameters:parameters, encoder: URLEncodedFormParameterEncoder.default, headers: nil, interceptor: nil).responseJSON(completionHandler: { (response) in
            
            let statusCode = response.response?.statusCode
            //初始化错误返回对象实例
            let errResponse = BaseErrorResponse()
                
            switch response.result{
                
                case .success(let res):
                    
                    if statusCode == 200{
                    
                        //初始化数据返回对象实例
                        let josnStr = res as? [String: Any]
                        let respModel = SmartGatewayChannleValueResponse.deserialize(from: josnStr)
                        
                        if respModel != nil {
                            if respModel?.code == 200 {
                                success(respModel!)
                            }else{
                                let response = BaseErrorResponse.deserialize(from: josnStr)
                                failed(self.formateErrorResponse(response: response!))
                            }
                        }else{
                            errResponse.errCode = RequestResponseCode.dataParsingException
                            errResponse.errMessage = "数据解析异常"
                            failed(errResponse)
                        }
                    }else{
                        failed(self.formateErrorResponse(response: self.handleErrorResponse(res: res)))
                    }
                break
                
                case .failure(let error):
                    
                    failed(self.formateFailedResponse(faile: error))
                    
                break
            
            }
            
        })
        
        
    }
    
    /// 公共接口 获取网关列表接口
    /// - Parameters:
    ///   - model_type: 设备类型 获取设备管理列表时返回的model
    ///   - keyword: 关键字搜索
    ///   - domainId: 中心ID筛选
    ///   - enterpriseId: 企业ID筛选
    ///   - buildingId: 建筑ID筛选
    ///   - keypartId: 部位ID筛选
    ///   - orderByColumn: 排序字段
    ///                    None = 0
    ///                    Status = 1
    ///                    HeartTime = 2
    ///                    Name = 3
    ///                    DomainName = 4
    ///                    EntName = 5
    ///                    BuildingName = 6
    ///                    KeyPartName = 7
    ///                    Code = 8
    ///                    CreateTime = 9
    ///                    Address = 10
    ///   - isDescOrder: 排序 true倒序 false正序
    ///   - pageIndex: 当前页
    ///   - success: 成功回调方法
    ///   - failed: 失败回调方法
    func getGatewayListPublic(model_type:String?,keyword:String,domainId:String,enterpriseId:String,buildingId:String,keypartId:String,orderByColumn:String,isDescOrder:Bool,pageIndex:Int,success:@escaping (_ response:DeviceHostListResponse) -> Void,failed:@escaping (_ failed:BaseErrorResponse) -> Void) {
        
        var parameters:[String:String]
        
        if nil != model_type {
            
            parameters = ["keyword":keyword,
            "domainId":domainId,
            "enterpriseId":enterpriseId,
            "buildingId":buildingId,
            "keypartId":keypartId,
            "orderByColumn":"9",
            "isDescOrder":String.init(isDescOrder),
            "pageIndex":String.init(pageIndex),
            "pageSize":"10",
            "model":model_type!]
            
            
        }else{
            
            parameters = ["keyword":keyword,
            "domainId":domainId,
            "enterpriseId":enterpriseId,
            "buildingId":buildingId,
            "keypartId":keypartId,
            "orderByColumn":"9",
            "isDescOrder":String.init(isDescOrder),
            "pageIndex":String.init(pageIndex),
            "pageSize":"10"]
            
        }
        
        sessionManager?.request(UrlHost.DeviceManager.getGatewayListAPI, method: .get, parameters: parameters, encoder: URLEncodedFormParameterEncoder.default, headers: nil, interceptor: nil).responseJSON(completionHandler: { (response) in
            
            let statusCode = response.response?.statusCode
            //初始化错误返回对象实例
            let errResponse = BaseErrorResponse()
                
            switch response.result{
                
                case .success(let res):
                    
                    if statusCode == 200{
                    
                        //初始化数据返回对象实例
                        let josnStr = res as? [String: Any]
                        let respModel = DeviceHostListResponse.deserialize(from: josnStr)
                        
                        if respModel != nil {
                                if respModel?.code == 200 {
                                    success(respModel!)
                                }else{
                                    let response = BaseErrorResponse.deserialize(from: josnStr)
                                    failed(self.formateErrorResponse(response: response!))
                                }
                            }else{
                                errResponse.errCode = RequestResponseCode.dataParsingException
                                errResponse.errMessage = "数据解析异常"
                                failed(errResponse)
                            }
                    }else{
                        failed(self.formateErrorResponse(response: self.handleErrorResponse(res: res)))
                    }
                break
                
                case .failure(let error):
                    
                    failed(self.formateFailedResponse(faile: error))
                    
                break
            
            }
            
        })
        
    }
    
    /// 公共获取网关下属器件列表接口
    /// - Parameters:
    ///   - isDeviceLevel: 设备级别：1：网关，2：主机，3：器件
    ///   - model_type: 设备类型 获取设备管理列表时返回的model
    ///   - id: 网关ID
    ///   - orderByColumn: 排序字段
    ///                    None = 0
    ///                    Status = 1
    ///                    HeartTime = 2
    ///                    Name = 3
    ///                    DomainName = 4
    ///                    EntName = 5
    ///                    BuildingName = 6
    ///                    KeyPartName = 7
    ///                    Code = 8
    ///                    CreateTime = 9
    ///                    Address = 10
    ///   - isDescOrder: 排序 true倒序 false正序
    ///   - pageIndex: 当前页
    ///   - success: 成功回调方法
    ///   - failed: 失败回调方法
    func getGatewaySubDeviceListPublic(isDeviceLevel:Int, model_type:String?,id:String,orderByColumn:String,isDescOrder:Bool,pageIndex:Int,success:@escaping (_ response:PublicSubDeviceListResponse) -> Void,failed:@escaping (_ failed:BaseErrorResponse) -> Void) {
        
        var parameters:[String:String]
            
        parameters = ["isDeviceLevel":String.init(isDeviceLevel),
        "orderByColumn":"9",
        "isDescOrder":String.init(isDescOrder),
        "pageIndex":String.init(pageIndex),
        "pageSize":"10",
        "model":model_type!,
        "id":id]
            
        sessionManager?.request(UrlHost.DeviceManager.getDeviceListAPI, method: .get, parameters: parameters, encoder: URLEncodedFormParameterEncoder.default, headers: nil, interceptor: nil).responseJSON(completionHandler: { (response) in
            
            let statusCode = response.response?.statusCode
            //初始化错误返回对象实例
            let errResponse = BaseErrorResponse()
                
            switch response.result{
                
                case .success(let res):
                    
                    if statusCode == 200{
                    
                        //初始化数据返回对象实例
                        let josnStr = res as? [String: Any]
                        let respModel = PublicSubDeviceListResponse.deserialize(from: josnStr)
                        
                        if respModel != nil {
                                if respModel?.code == 200 {
                                    success(respModel!)
                                }else{
                                    let response = BaseErrorResponse.deserialize(from: josnStr)
                                    failed(self.formateErrorResponse(response: response!))
                                }
                            }else{
                                errResponse.errCode = RequestResponseCode.dataParsingException
                                errResponse.errMessage = "数据解析异常"
                                failed(errResponse)
                            }
                    }else{
                        failed(self.formateErrorResponse(response: self.handleErrorResponse(res: res)))
                    }
                break
                
                case .failure(let error):
                    
                    failed(self.formateFailedResponse(faile: error))
                    
                break
            
            }
            
        })
        
    }
    
    /// 公共接口 获取网关 设备详情信息
    /// - Parameters:
    ///   - url: 接口URL
    ///   - id: 设备id
    ///   - success: 成功回调
    ///   - failed: 失败回调
    func getGatewayOrDeviceDetailInforPublic(model:String,equipmentID:String,isDeviceLevel:Int,success:@escaping (_ response:DeviceDetailResponse) -> Void,failed:@escaping (_ failed:BaseErrorResponse) -> Void){
        
        let parameters = ["equipmentID":equipmentID,
                          "model":model,
                          "isDeviceLevel":String.init(isDeviceLevel)]
        
        self.sessionManager?.request(UrlHost.DeviceManager.getGatewayOrDeviceDetailAPI, method: .get, parameters: parameters, encoder: URLEncodedFormParameterEncoder.default, headers: nil, interceptor: nil).responseJSON(completionHandler: { (response) in
            
            let statusCode = response.response?.statusCode
            //初始化错误返回对象实例
            let errResponse = BaseErrorResponse()
                
            switch response.result{
                
                case .success(let res):
                    
                    if statusCode == 200{
                    
                        //初始化数据返回对象实例
                        let josnStr = res as? [String: Any]
                        let respModel = DeviceDetailResponse.deserialize(from: josnStr)
                        
                        if respModel != nil {
                            if respModel?.code == 200 {
                                success(respModel!)
                            }else{
                                let response = BaseErrorResponse.deserialize(from: josnStr)
                                failed(self.formateErrorResponse(response: response!))
                            }
                        }else{
                            errResponse.errCode = RequestResponseCode.dataParsingException
                            errResponse.errMessage = "数据解析异常"
                            failed(errResponse)
                        }
                    }else{
                        failed(self.formateErrorResponse(response: self.handleErrorResponse(res: res)))
                    }
                break
                
                case .failure(let error):
                    
                    failed(self.formateFailedResponse(faile: error))
                    
                break
            
            }
            
        })
        
        
    }
    
    /// 公共接口  添加设备
    /// - Parameters:
    ///   - apiPath: 接口API
    ///   - jsonParame: 上传数据
    ///   - success: 成功回调
    ///   - failed: 失败回调
    func postAddDevicePublic(jsonParame:PublicSupperDeviceAddModel,success:@escaping (_ response:DeviceAddResponse) -> Void,failed:@escaping (_ failed:BaseErrorResponse) -> Void) {
        
        self.sessionManager?.request(UrlHost.DeviceManager.postAddGatewayOrDeviceAPI, method: .post, parameters: jsonParame, encoder: JSONParameterEncoder.default, headers: nil, interceptor: nil).responseJSON { (response) in
        
            
            let statusCode = response.response?.statusCode
            //初始化错误返回对象实例
            let errResponse = BaseErrorResponse()
                
            switch response.result{
                
                case .success(let res):

                    if statusCode == 200{
                    
                        //初始化数据返回对象实例
                        let josnStr = res as? [String: Any]
                        let respModel = DeviceAddResponse.deserialize(from: josnStr)
                        
                        if respModel != nil {
                                if respModel?.code == 200 {
                                    success(respModel!)
                                }else{
                                    let response = BaseErrorResponse.deserialize(from: josnStr)
                                    failed(self.formateErrorResponse(response: response!))
                                }
                            }else{
                                errResponse.errCode = RequestResponseCode.dataParsingException
                                errResponse.errMessage = "数据解析异常"
                                failed(errResponse)
                            }
                    }else{
                        failed(self.formateErrorResponse(response: self.handleErrorResponse(res: res)))
                    }
                break
                
                case .failure(let error):
            
                    failed(self.formateFailedResponse(faile: error))
                    
                break
            
            }
        
        }
        
    }
    /// 公共接口 编辑设备
    /// - Parameters:
    ///   - apiPath: 接口API
    ///   - jsonParame: 上传数据
    ///   - success: 成功回调
    ///   - failed: 失败回调
    func postEditDevicePublic(jsonParame:SupperDeviceEditPublicModel,success:@escaping (_ response:BaseResponse) -> Void,failed:@escaping (_ failed:BaseErrorResponse) -> Void) {
        
        self.sessionManager?.request(UrlHost.DeviceManager.postEditGatewayOrDeviceAPI, method: .post, parameters: jsonParame, encoder: JSONParameterEncoder.default, headers: nil, interceptor: nil).responseJSON(completionHandler: { (response) in
    
            let statusCode = response.response?.statusCode
            //初始化错误返回对象实例
            let errResponse = BaseErrorResponse()
                
            switch response.result{
                
                case .success(let res):
                    
                    if statusCode == 200{
                    
                        //初始化数据返回对象实例
                        let josnStr = res as? [String: Any]
                        let respModel = BaseResponse.deserialize(from: josnStr)
                        
                        if respModel != nil {
                            if respModel?.code == 200 {
                                success(respModel!)
                            }else{
                                let response = BaseErrorResponse.deserialize(from: josnStr)
                                failed(self.formateErrorResponse(response: response!))
                            }
                        }else{
                            errResponse.errCode = RequestResponseCode.dataParsingException
                            errResponse.errMessage = "数据解析异常"
                            failed(errResponse)
                        }
                    }else{
                        failed(self.formateErrorResponse(response: self.handleErrorResponse(res: res)))
                    }
                break
                
                case .failure(let error):
                    
                    failed(self.formateFailedResponse(faile: error))
                    
                break
            
            }
        })
        
    }
    
    /// 公共接口 删除设备 SignalControlCommandModel
    /// - Parameters:
    ///   - apiPath: 接口API
    ///   - jsonParame: 上传数据
    ///   - success: 成功回调
    ///   - failed: 失败回调
    func postDeleteGatewayOrDevicePublic(paraModel:DeleteGatewayOrDevicePublic,success:@escaping (_ response:DeleteDeviceResponse) -> Void,failed:@escaping (_ failed:BaseErrorResponse) -> Void) {
        
        
        self.sessionManager?.request(UrlHost.DeviceManager.postDeleteGatewayOrDeviceAPI, method: .post, parameters: paraModel, encoder: JSONParameterEncoder.default, headers: nil, interceptor: nil).responseJSON(completionHandler: { (response) in
            
                    let statusCode = response.response?.statusCode
                    //初始化错误返回对象实例
                    let errResponse = BaseErrorResponse()
                        
                    switch response.result{
                        
                        case .success(let res):
                            
                            if statusCode == 200{
                            
                                //初始化数据返回对象实例
                                let josnStr = res as? [String: Any]
                                let respModel = DeleteDeviceResponse.deserialize(from: josnStr)
                                
                                if respModel != nil {
                                    if respModel?.code == 200 {
                                        success(respModel!)
                                    }else{
                                        let response = BaseErrorResponse.deserialize(from: josnStr)
                                        failed(self.formateErrorResponse(response: response!))
                                    }
                                }else{
                                    errResponse.errCode = RequestResponseCode.dataParsingException
                                    errResponse.errMessage = "数据解析异常"
                                    failed(errResponse)
                                }
                            }else{
                                failed(self.formateErrorResponse(response: self.handleErrorResponse(res: res)))
                            }
                        break
                        
                        case .failure(let error):
                            
                            failed(self.formateFailedResponse(faile: error))
                            
                        break
                    
                    }
                })
        
        
    }
    
    /// 共用接口  设备单条命令反控
    /// - Parameters:
    /// - deviceId: 设备id
    /// - success: 成功回调
    /// - failed: 失败回调
    func postSignalCommandPublic(inforModel:SignalControlCommandModel, success:@escaping (_ response:BaseResponse) -> Void,failed:@escaping (_ failed:BaseErrorResponse) -> Void){
        
        self.sessionManager?.request(UrlHost.DeviceManager.postSignalCommandAPI, method: .post, parameters: inforModel, encoder: JSONParameterEncoder.default, headers: nil, interceptor: nil).responseJSON(completionHandler: { (response) in
            
            let statusCode = response.response?.statusCode
            //初始化错误返回对象实例
            let errResponse = BaseErrorResponse()
                
            switch response.result{
                
                case .success(let res):
                    
                    if statusCode == 200{
                    
                        //初始化数据返回对象实例
                        let josnStr = res as? [String: Any]
                        let respModel = BaseResponse.deserialize(from: josnStr)
                        
                        if respModel != nil {
                            if respModel?.code == 200 {
                                success(respModel!)
                            }else{
                                let response = BaseErrorResponse.deserialize(from: josnStr)
                                failed(self.formateErrorResponse(response: response!))
                            }
                        }else{
                            errResponse.errCode = RequestResponseCode.dataParsingException
                            errResponse.errMessage = "数据解析异常"
                            failed(errResponse)
                        }
                    }else{
                        failed(self.formateErrorResponse(response: self.handleErrorResponse(res: res)))
                    }
                break
                
                case .failure(let error):
                    
                    failed(self.formateFailedResponse(faile: error))
                    
                break
            
            }
            
        })
    }
    
   
    
    ///巡检相关接口
    /// 获取巡检频率
    /// - Parameters:
    ///   - success: 成功回调
    ///   - failed: 失败回调
    func getTaskFrequency(success:@escaping (_ response:InspectionFrequenceResponse) -> Void,failed:@escaping (_ failed:BaseErrorResponse) -> Void){
        
        self.sessionManager?.request(UrlHost.InspectionFrequency.TaskFrequency, method: .get, parameters:defaultParameters, encoder: URLEncodedFormParameterEncoder.default, headers: nil, interceptor: nil).responseJSON(completionHandler: { (response) in
            
            let statusCode = response.response?.statusCode
            //初始化错误返回对象实例
            let errResponse = BaseErrorResponse()
                
            switch response.result{
                
                case .success(let res):
                    
                    if statusCode == 200{
                    
                        //初始化数据返回对象实例
                        let josnStr = res as? [String: Any]
                        let respModel = InspectionFrequenceResponse.deserialize(from: josnStr)
                        
                        if respModel != nil {
                            if respModel?.code == 200 {
                                success(respModel!)
                            }else{
                                let response = BaseErrorResponse.deserialize(from: josnStr)
                                failed(self.formateErrorResponse(response: response!))
                            }
                        }else{
                            errResponse.errCode = RequestResponseCode.dataParsingException
                            errResponse.errMessage = "数据解析异常"
                            failed(errResponse)
                        }
                    }else{
                        failed(self.formateErrorResponse(response: self.handleErrorResponse(res: res)))
                    }
                break
                
                case .failure(let error):
                    
                    failed(self.formateFailedResponse(faile: error))
                    
                break
            
            }
            
        })
        
        
    }
    /// 获取巡检任务列表
    /// - Parameters:
    ///   - success: 成功回调
    ///   - failed: 失败回调
    func getTaskList(keyword:String,frequenceId:String,pageIndex:Int,success:@escaping (_ response:InspectionTaskListResponse) -> Void,failed:@escaping (_ failed:BaseErrorResponse) -> Void){
        
        let parameters = ["keyword":keyword,"frequenceId":frequenceId,"pageIndex":String(pageIndex),"pageSize":"10"]
        
        self.sessionManager?.request(UrlHost.InspectionTask.TaskList, method: .get, parameters:parameters, encoder: URLEncodedFormParameterEncoder.default, headers: nil, interceptor: nil).responseJSON(completionHandler: { (response) in
            
            let statusCode = response.response?.statusCode
            //初始化错误返回对象实例
            let errResponse = BaseErrorResponse()
                
            switch response.result{
                
                case .success(let res):
                    
                    if statusCode == 200{
                    
                        //初始化数据返回对象实例
                        let josnStr = res as? [String: Any]
                        let respModel = InspectionTaskListResponse.deserialize(from: josnStr)
                        
                        if respModel != nil {
                            if respModel?.code == 200 {
                                success(respModel!)
                            }else{
                                let response = BaseErrorResponse.deserialize(from: josnStr)
                                failed(self.formateErrorResponse(response: response!))
                            }
                        }else{
                            errResponse.errCode = RequestResponseCode.dataParsingException
                            errResponse.errMessage = "数据解析异常"
                            failed(errResponse)
                        }
                    }else{
                        failed(self.formateErrorResponse(response: self.handleErrorResponse(res: res)))
                    }
                break
                
                case .failure(let error):
                    
                    failed(self.formateFailedResponse(faile: error))
                    
                break
            
            }
            
        })
        
        
    }
    
    /// 获取巡检任务下巡检点列表
    /// - Parameters:
    ///   - success: 成功回调
    ///   - failed: 失败回调
    func getTaskPointList(resultTaskId:String,keyword:String,pageIndex:Int,success:@escaping (_ response:InspectionTaskPointListResponse) -> Void,failed:@escaping (_ failed:BaseErrorResponse) -> Void){
        
        let parameters = ["resultTaskId":resultTaskId,"keyword":keyword,"pageIndex":String(pageIndex),"pageSize":"10"]
        
    self.sessionManager?.request(UrlHost.InspectionTask.TaskPointList, method: .get, parameters:parameters, encoder: URLEncodedFormParameterEncoder.default, headers: nil, interceptor: nil).responseJSON(completionHandler: { (response) in
            
            let statusCode = response.response?.statusCode
            //初始化错误返回对象实例
            let errResponse = BaseErrorResponse()
                
            switch response.result{
                
                case .success(let res):
                    
                    if statusCode == 200{
                    
                        //初始化数据返回对象实例
                        let josnStr = res as? [String: Any]
                        let respModel = InspectionTaskPointListResponse.deserialize(from: josnStr)
                        
                        if respModel != nil {
                            if respModel?.code == 200 {
                                success(respModel!)
                            }else{
                                let response = BaseErrorResponse.deserialize(from: josnStr)
                                failed(self.formateErrorResponse(response: response!))
                            }
                        }else{
                            errResponse.errCode = RequestResponseCode.dataParsingException
                            errResponse.errMessage = "数据解析异常"
                            failed(errResponse)
                        }
                    }else{
                        failed(self.formateErrorResponse(response: self.handleErrorResponse(res: res)))
                    }
                break
                
                case .failure(let error):
                    
                    failed(self.formateFailedResponse(faile: error))
                    
                break
            
            }
            
        })
    
    }
    
    /// 获取历史巡检列表
    /// - Parameters:
    ///   - keyword: 关键字
    ///   - pageIndex: 页数索引
    ///   - frequenceId: 频率
    ///   - isFinish: 完成状态
    ///   - success: <#success description#>
    ///   - failed: <#failed description#>
    func getHistoryTaskList(keyword:String,pageIndex:Int,frequenceId:String,isFinish:String,success:@escaping (_ response:InspectionHistoryResponse) -> Void,failed:@escaping (_ failed:BaseErrorResponse) -> Void){
        
        let parameters = ["isFinish":isFinish,"frequenceId":frequenceId,"keyword":keyword,"pageIndex":String(pageIndex),"pageSize":"10"]
        
        self.sessionManager?.request(UrlHost.InspectionHistory.TaskList, method: .get, parameters:parameters, encoder: URLEncodedFormParameterEncoder.default, headers: nil, interceptor: nil).responseJSON(completionHandler: { (response) in
            
            let statusCode = response.response?.statusCode
            //初始化错误返回对象实例
            let errResponse = BaseErrorResponse()
                
            switch response.result{
                
                case .success(let res):
                    
                    if statusCode == 200{
                    
                        //初始化数据返回对象实例
                        let josnStr = res as? [String: Any]
                        let respModel = InspectionHistoryResponse.deserialize(from: josnStr)
                        
                        if respModel != nil {
                            if respModel?.code == 200 {
                                success(respModel!)
                            }else{
                                let response = BaseErrorResponse.deserialize(from: josnStr)
                                failed(self.formateErrorResponse(response: response!))
                            }
                        }else{
                            errResponse.errCode = RequestResponseCode.dataParsingException
                            errResponse.errMessage = "数据解析异常"
                            failed(errResponse)
                        }
                    }else{
                        failed(self.formateErrorResponse(response: self.handleErrorResponse(res: res)))
                    }
                break
                
                case .failure(let error):
                    
                    failed(self.formateFailedResponse(faile: error))
                    
                break
            
            }
            
        })
    
    }
    
    /// 获取隐患点列表
    /// - Parameters:
    ///   - keyword: 关键字
    ///   - pageIndex: 页数索引
    ///   - filterDate: 筛选时间
    ///   - success: <#success description#>
    ///   - failed: <#failed description#>
    func getTroublePointList(keyword:String,pageIndex:Int,filterDate:String,success:@escaping (_ response:InspectionTroublePointResponse) -> Void,failed:@escaping (_ failed:BaseErrorResponse) -> Void){
        
        let parameters = ["inDays":filterDate,"keyword":keyword,"pageIndex":String(pageIndex),"pageSize":"10"]
        
        self.sessionManager?.request(UrlHost.InspectionTroublePoint.TaskPointList, method: .get, parameters:parameters, encoder: URLEncodedFormParameterEncoder.default, headers: nil, interceptor: nil).responseJSON(completionHandler: { (response) in
            
            let statusCode = response.response?.statusCode
            //初始化错误返回对象实例
            let errResponse = BaseErrorResponse()
                
            switch response.result{
                
                case .success(let res):
                    
                    if statusCode == 200{
                    
                        //初始化数据返回对象实例
                        let josnStr = res as? [String: Any]
                        let respModel = InspectionTroublePointResponse.deserialize(from: josnStr)
                        
                        if respModel != nil {
                            if respModel?.code == 200 {
                                success(respModel!)
                            }else{
                                let response = BaseErrorResponse.deserialize(from: josnStr)
                                failed(self.formateErrorResponse(response: response!))
                            }
                        }else{
                            errResponse.errCode = RequestResponseCode.dataParsingException
                            errResponse.errMessage = "数据解析异常"
                            failed(errResponse)
                        }
                    }else{
                        failed(self.formateErrorResponse(response: self.handleErrorResponse(res: res)))
                    }
                break
                
                case .failure(let error):
                    
                    failed(self.formateFailedResponse(faile: error))
                    
                break
            
            }
            
        })
    
    }
    
    /// 获取巡检点绑定列表
    /// - Parameters:
    ///   - keyword: 关键字
    ///   - pageIndex: 页数索引
    ///   - bindState: 绑定类型
    ///   - success: <#success description#>
    ///   - failed: <#failed description#>
    func getBindPointList(keyword:String,pageIndex:Int,bindState:String,success:@escaping (_ response:InspectionBindPointResponse) -> Void,failed:@escaping (_ failed:BaseErrorResponse) -> Void){
        
        let parameters = ["bindingStatus":bindState,"keyword":keyword,"pageIndex":String(pageIndex),"pageSize":"10"]
        
        self.sessionManager?.request(UrlHost.InspectionBindPoint.PointList, method: .get, parameters:parameters, encoder: URLEncodedFormParameterEncoder.default, headers: nil, interceptor: nil).responseJSON(completionHandler: { (response) in
            
            let statusCode = response.response?.statusCode
            //初始化错误返回对象实例
            let errResponse = BaseErrorResponse()
                
            switch response.result{
                
                case .success(let res):
                    
                    if statusCode == 200{
                    
                        //初始化数据返回对象实例
                        let josnStr = res as? [String: Any]
                        let respModel = InspectionBindPointResponse.deserialize(from: josnStr)
                        
                        if respModel != nil {
                            if respModel?.code == 200 {
                                success(respModel!)
                            }else{
                                let response = BaseErrorResponse.deserialize(from: josnStr)
                                failed(self.formateErrorResponse(response: response!))
                            }
                        }else{
                            errResponse.errCode = RequestResponseCode.dataParsingException
                            errResponse.errMessage = "数据解析异常"
                            failed(errResponse)
                        }
                    }else{
                        failed(self.formateErrorResponse(response: self.handleErrorResponse(res: res)))
                    }
                break
                
                case .failure(let error):
                    
                    failed(self.formateFailedResponse(faile: error))
                    
                break
            
            }
            
        })
    
    }
    
    /// 获取隐患确认列表
    /// - Parameters:
    ///   - pageIndex: 页数索引
    ///   - success: <#success description#>
    ///   - failed: <#failed description#>
    func getTroubleConfirmPointList(pageIndex:Int,success:@escaping (_ response:InspectionTroubleConfirmResponse) -> Void,failed:@escaping (_ failed:BaseErrorResponse) -> Void){
        
        let parameters = ["pageIndex":String(pageIndex),"pageSize":"10"]
        
        self.sessionManager?.request(UrlHost.InspectionToubleConfirm.getTroubleConfirmListList, method: .get, parameters:parameters, encoder: URLEncodedFormParameterEncoder.default, headers: nil, interceptor: nil).responseJSON(completionHandler: { (response) in
            
            let statusCode = response.response?.statusCode
            //初始化错误返回对象实例
            let errResponse = BaseErrorResponse()
                
            switch response.result{
                
                case .success(let res):
                    
                    if statusCode == 200{
                    
                        //初始化数据返回对象实例
                        let josnStr = res as? [String: Any]
                        let respModel = InspectionTroubleConfirmResponse.deserialize(from: josnStr)
                        
                        if respModel != nil {
                            if respModel?.code == 200 {
                                success(respModel!)
                            }else{
                                let response = BaseErrorResponse.deserialize(from: josnStr)
                                failed(self.formateErrorResponse(response: response!))
                            }
                        }else{
                            errResponse.errCode = RequestResponseCode.dataParsingException
                            errResponse.errMessage = "数据解析异常"
                            failed(errResponse)
                        }
                    }else{
                        failed(self.formateErrorResponse(response: self.handleErrorResponse(res: res)))
                    }
                break
                
                case .failure(let error):
                    
                    failed(self.formateFailedResponse(faile: error))
                    
                break
            
            }
            
        })
    
    }
    
    /// 获取任务完成率
    func getTaskFinishRate(success:@escaping (_ response:InspectionTaskFinishRateResponse) -> Void,failed:@escaping (_ failed:BaseErrorResponse) -> Void){
        
        self.sessionManager?.request(UrlHost.InspectionTask.TaskFinishRate, method: .get, parameters:defaultParameters, encoder: URLEncodedFormParameterEncoder.default, headers: nil, interceptor: nil).responseJSON(completionHandler: { (response) in
            
            let statusCode = response.response?.statusCode
            //初始化错误返回对象实例
            let errResponse = BaseErrorResponse()
                
            switch response.result{
                
                case .success(let res):
                    
                    if statusCode == 200{
                    
                        //初始化数据返回对象实例
                        let josnStr = res as? [String: Any]
                        let respModel = InspectionTaskFinishRateResponse.deserialize(from: josnStr)
                        
                        if respModel != nil {
                            if respModel?.code == 200 {
                                success(respModel!)
                            }else{
                                let response = BaseErrorResponse.deserialize(from: josnStr)
                                failed(self.formateErrorResponse(response: response!))
                            }
                        }else{
                            errResponse.errCode = RequestResponseCode.dataParsingException
                            errResponse.errMessage = "数据解析异常"
                            failed(errResponse)
                        }
                    }else{
                        failed(self.formateErrorResponse(response: self.handleErrorResponse(res: res)))
                    }
                break
                
                case .failure(let error):
                    
                    failed(self.formateFailedResponse(faile: error))
                    
                break
            
            }
            
        })
    
    }
    
    /// 获取消息列表
    /// - Parameters:
    ///   - pageIndex: 页数索引
    ///   - success: <#success description#>
    ///   - failed: <#failed description#>
    func getNotiftionMessageList(pageIndex:Int,success:@escaping (_ response:InspectionMessageResponse) -> Void,failed:@escaping (_ failed:BaseErrorResponse) -> Void){
        
        let parameters = ["pageIndex":String(pageIndex),"pageSize":"10"]
        
        self.sessionManager?.request(UrlHost.InspectionPushMessage.MessageList, method: .get, parameters:parameters, encoder: URLEncodedFormParameterEncoder.default, headers: nil, interceptor: nil).responseJSON(completionHandler: { (response) in
            
            let statusCode = response.response?.statusCode
            //初始化错误返回对象实例
            let errResponse = BaseErrorResponse()
                
            switch response.result{
                
                case .success(let res):
                    
                    if statusCode == 200{
                    
                        //初始化数据返回对象实例
                        let josnStr = res as? [String: Any]
                        let respModel = InspectionMessageResponse.deserialize(from: josnStr)
                        
                        if respModel != nil {
                            if respModel?.code == 200 {
                                success(respModel!)
                            }else{
                                let response = BaseErrorResponse.deserialize(from: josnStr)
                                failed(self.formateErrorResponse(response: response!))
                            }
                        }else{
                            errResponse.errCode = RequestResponseCode.dataParsingException
                            errResponse.errMessage = "数据解析异常"
                            failed(errResponse)
                        }
                    }else{
                        failed(self.formateErrorResponse(response: self.handleErrorResponse(res: res)))
                    }
                break
                
                case .failure(let error):
                    
                    failed(self.formateFailedResponse(faile: error))
                    
                break
            
            }
            
        })
    
    }
    /// 获取未读消息数量
    func getUnReadPushNum(success:@escaping (_ response:InspectionUnReadMessageResponse) -> Void,failed:@escaping (_ failed:BaseErrorResponse) -> Void){
        
        self.sessionManager?.request(UrlHost.InspectionPushMessage.getUnReadMessageCount, method: .get, parameters:defaultParameters, encoder: URLEncodedFormParameterEncoder.default, headers: nil, interceptor: nil).responseJSON(completionHandler: { (response) in
            
            let statusCode = response.response?.statusCode
            //初始化错误返回对象实例
            let errResponse = BaseErrorResponse()
                
            switch response.result{
                
                case .success(let res):
                    
                    if statusCode == 200{
                    
                        //初始化数据返回对象实例
                        let josnStr = res as? [String: Any]
                        let respModel = InspectionUnReadMessageResponse.deserialize(from: josnStr)
                        
                        if respModel != nil {
                            if respModel?.code == 200 {
                                success(respModel!)
                            }else{
                                let response = BaseErrorResponse.deserialize(from: josnStr)
                                failed(self.formateErrorResponse(response: response!))
                            }
                        }else{
                            errResponse.errCode = RequestResponseCode.dataParsingException
                            errResponse.errMessage = "数据解析异常"
                            failed(errResponse)
                        }
                    }else{
                        failed(self.formateErrorResponse(response: self.handleErrorResponse(res: res)))
                    }
                break
                
                case .failure(let error):
                    
                    failed(self.formateFailedResponse(faile: error))
                    
                break
            
            }
            
        })
    
    }
    
    /// 修改未读消息数量
    func editUnReadPushNum(success:@escaping (_ response:BaseResponse) -> Void,failed:@escaping (_ failed:BaseErrorResponse) -> Void){
        
        let model = InspectionUpdataNotifyModel.init()
        model.all = true
        model.msdIds = Array.init(arrayLiteral: "")
        model.newStatus = "1"
    
    
        self.sessionManager?.request(UrlHost.InspectionPushMessage.editUnReadMessageCount, method: .post, parameters:model, encoder: JSONParameterEncoder.default, headers: nil, interceptor: nil).responseJSON(completionHandler: { (response) in
            
            let statusCode = response.response?.statusCode
            //初始化错误返回对象实例
            let errResponse = BaseErrorResponse()
                
            switch response.result{
                
                case .success(let res):
                    
                    if statusCode == 200{
                    
                        //初始化数据返回对象实例
                        let josnStr = res as? [String: Any]
                        let respModel = BaseResponse.deserialize(from: josnStr)
                        
                        if respModel != nil {
                            if respModel?.code == 200 {
                                success(respModel!)
                            }else{
                                let response = BaseErrorResponse.deserialize(from: josnStr)
                                failed(self.formateErrorResponse(response: response!))
                            }
                        }else{
                            errResponse.errCode = RequestResponseCode.dataParsingException
                            errResponse.errMessage = "数据解析异常"
                            failed(errResponse)
                        }
                    }else{
                        failed(self.formateErrorResponse(response: self.handleErrorResponse(res: res)))
                    }
                break
                
                case .failure(let error):
                    
                    failed(self.formateFailedResponse(faile: error))
                    
                break
            
            }
            
        })
    
    }
    
    
    /// 巡检点绑定
    /// - Parameters:
    ///   - tagCode: 标签编码
    ///   - forceBind: 是否强制绑定
    ///   - pointId: 巡检点id
    ///   - tagType: 标签类型 1二维码 0nfc
    ///   - success:
    ///   - failed:
    func bindPoint(tagCode:String,forceBind:String,pointId:String,tagType:String,success:@escaping (_ response:InspectionPointBindResponse) -> Void,failed:@escaping (_ failed:BaseErrorResponse) -> Void){
        
        let parameters = ["tagCode":tagCode,"forceBind":forceBind,"pointId":pointId,"tagType":tagType]
    
        self.sessionManager?.request(UrlHost.InspectionBindPoint.BindPoint, method: .post, parameters:parameters, encoder: JSONParameterEncoder.default, headers: nil, interceptor: nil).responseJSON(completionHandler: { (response) in
            
            let statusCode = response.response?.statusCode
            //初始化错误返回对象实例
            let errResponse = BaseErrorResponse()
                
            switch response.result{
                
                case .success(let res):
                    
                    if statusCode == 200{
                    
                        //初始化数据返回对象实例
                        let josnStr = res as? [String: Any]
                        let respModel = InspectionPointBindResponse.deserialize(from: josnStr)
                        
                        if respModel != nil {
                            if respModel?.code == 200 || respModel?.code == 500 {
                                success(respModel!)
                            }else{
                                
                                let response = BaseErrorResponse.deserialize(from: josnStr)
                                failed(self.formateErrorResponse(response: response!))
                                
                            }
                        }else{
                            errResponse.errCode = RequestResponseCode.dataParsingException
                            errResponse.errMessage = "数据解析异常"
                            failed(errResponse)
                        }
                    }else{
                        failed(self.formateErrorResponse(response: self.handleErrorResponse(res: res)))
                    }
                break
                
                case .failure(let error):
                    
                    failed(self.formateFailedResponse(faile: error))
                    
                break
            
            }
            
        })
    
    }
    
    
    /// 无隐患巡检点上报
    /// - Parameters:
    ///   - tagCode: 标签编码
    ///   - forceBind: 是否强制绑定
    ///   - pointId: 巡检点id
    ///   - tagType: 标签类型 1二维码 0nfc
    ///   - success:
    ///   - failed:
    func TaskResultUpload(resultModel:TaskResultUploadModel,success:@escaping (_ response:BaseResponse) -> Void,failed:@escaping (_ failed:BaseErrorResponse) -> Void){
    
        self.sessionManager?.request(UrlHost.InspectionTask.TaskResultUpload, method: .post, parameters:resultModel, encoder: JSONParameterEncoder.default, headers: nil, interceptor: nil).responseJSON(completionHandler: { (response) in
            
            let statusCode = response.response?.statusCode
            //初始化错误返回对象实例
            let errResponse = BaseErrorResponse()
                
            switch response.result{
                
                case .success(let res):
                    
                    if statusCode == 200{
                    
                        //初始化数据返回对象实例
                        let josnStr = res as? [String: Any]
                        let respModel = BaseResponse.deserialize(from: josnStr)
                        
                        if respModel != nil {
                            if respModel?.code == 200 || respModel?.code == 500 {
                                success(respModel!)
                            }else{
                                
                                let response = BaseErrorResponse.deserialize(from: josnStr)
                                failed(self.formateErrorResponse(response: response!))
                                
                            }
                        }else{
                            errResponse.errCode = RequestResponseCode.dataParsingException
                            errResponse.errMessage = "数据解析异常"
                            failed(errResponse)
                        }
                    }else{
                        failed(self.formateErrorResponse(response: self.handleErrorResponse(res: res)))
                    }
                break
                
                case .failure(let error):
                    
                    failed(self.formateFailedResponse(faile: error))
                    
                break
            
            }
            
        })
    
    }
    
    
    /// 快速巡检获取巡检项
    func fastInspectionGetProject(tagCode:String,tagType:String,success:@escaping (_ response:FastInspectionResponse) -> Void,failed:@escaping (_ failed:BaseErrorResponse) -> Void){
        
        let paramet = ["tagCode":tagCode,"tagType":tagType]
        
        self.sessionManager?.request(UrlHost.InspectionQuickly.getProject, method: .get, parameters:paramet, encoder: URLEncodedFormParameterEncoder.default, headers: nil, interceptor: nil).responseJSON(completionHandler: { (response) in
            
            let statusCode = response.response?.statusCode
            //初始化错误返回对象实例
            let errResponse = BaseErrorResponse()
                
            switch response.result{
                
                case .success(let res):
                    
                    if statusCode == 200{
                    
                        //初始化数据返回对象实例
                        let josnStr = res as? [String: Any]
                        let respModel = FastInspectionResponse.deserialize(from: josnStr)
                        
                        if respModel != nil {
                            if respModel?.code == 200 {
                                success(respModel!)
                            }else{
                                let response = BaseErrorResponse.deserialize(from: josnStr)
                                failed(self.formateErrorResponse(response: response!))
                            }
                        }else{
                            errResponse.errCode = RequestResponseCode.dataParsingException
                            errResponse.errMessage = "数据解析异常"
                            failed(errResponse)
                        }
                    }else{
                        failed(self.formateErrorResponse(response: self.handleErrorResponse(res: res)))
                    }
                break
                
                case .failure(let error):
                    
                    failed(self.formateFailedResponse(faile: error))
                    
                break
            
            }
            
        })
    
    }
    
    /// 隐患上报获取巡检点信息
    func inspectionTroubleUploadGetPonitInfor(tagCode:String,tagType:String,success:@escaping (_ response:FastInspectionResponse) -> Void,failed:@escaping (_ failed:BaseErrorResponse) -> Void){
        
        let paramet = ["tagCode":tagCode,"tagType":tagType]
        
        self.sessionManager?.request(UrlHost.InspectionTroubleUpload.getPointByTagApi, method: .get, parameters:paramet, encoder: URLEncodedFormParameterEncoder.default, headers: nil, interceptor: nil).responseJSON(completionHandler: { (response) in
            
            let statusCode = response.response?.statusCode
            //初始化错误返回对象实例
            let errResponse = BaseErrorResponse()
                
            switch response.result{
                
                case .success(let res):
                    
                    if statusCode == 200{
                    
                        //初始化数据返回对象实例
                        let josnStr = res as? [String: Any]
                        let respModel = FastInspectionResponse.deserialize(from: josnStr)
                        
                        if respModel != nil {
                            if respModel?.code == 200 {
                                success(respModel!)
                            }else{
                                let response = BaseErrorResponse.deserialize(from: josnStr)
                                failed(self.formateErrorResponse(response: response!))
                            }
                        }else{
                            errResponse.errCode = RequestResponseCode.dataParsingException
                            errResponse.errMessage = "数据解析异常"
                            failed(errResponse)
                        }
                    }else{
                        failed(self.formateErrorResponse(response: self.handleErrorResponse(res: res)))
                    }
                break
                
                case .failure(let error):
                    
                    failed(self.formateFailedResponse(faile: error))
                    
                break
            
            }
            
        })
    
    }
    /// 获取联动视频信息
    func getVideoPlayDetail(deviceId:String,videoPlayType:String,beginTime:String,endTime:String,success:@escaping (_ response:VideoPlayInforResponse) -> Void,failed:@escaping (_ failed:BaseErrorResponse) -> Void){
        
        let paramet = ["deviceId":deviceId,"videoPlayType":videoPlayType,"beginTime":beginTime,"endTime":endTime]
        
        self.sessionManager?.request(UrlHost.AlarmLog.getVideoPlayDetail, method: .get, parameters:paramet, encoder: URLEncodedFormParameterEncoder.default, headers: nil, interceptor: nil).responseJSON(completionHandler: { (response) in
            
            let statusCode = response.response?.statusCode
            //初始化错误返回对象实例
            let errResponse = BaseErrorResponse()
                
            switch response.result{
                
                case .success(let res):
                    
                    if statusCode == 200{
                    
                        //初始化数据返回对象实例
                        let josnStr = res as? [String: Any]
                        let respModel = VideoPlayInforResponse.deserialize(from: josnStr)
                        
                        if respModel != nil {
                            if respModel?.code == 200 {
                                success(respModel!)
                            }else{
                                let response = BaseErrorResponse.deserialize(from: josnStr)
                                failed(self.formateErrorResponse(response: response!))
                            }
                        }else{
                            errResponse.errCode = RequestResponseCode.dataParsingException
                            errResponse.errMessage = "数据解析异常"
                            failed(errResponse)
                        }
                    }else{
                        failed(self.formateErrorResponse(response: self.handleErrorResponse(res: res)))
                    }
                break
                
                case .failure(let error):
                    
                    failed(self.formateFailedResponse(faile: error))
                    
                break
            
            }
            
        })
    
    }
    
    /// 获取警情日志列表
    func getAlarmLogList(domainId:String,enterpriseId:String,eventHandler:String,pageIndex:Int,orderByColumn:String,eventType:String,dayAgo:String,success:@escaping (_ response:AlarmLogResponse) -> Void,failed:@escaping (_ failed:BaseErrorResponse) -> Void){
        
        let pageSize = String.init(format: "%d", 10)
        let pageIndexStr = String.init(format: "%d", pageIndex)
        let paramet = ["domainId":domainId,"enterpriseId":enterpriseId,"devClass":"99","eventHandler":eventHandler,"orderByColumn":orderByColumn,"isDescOrder":"true","pageIndex":pageIndexStr,"pageSize":pageSize,"eventType":eventType,"dayAgo":dayAgo]
        
    self.sessionManager?.request(UrlHost.AlarmLog.getAlarmList, method: .get, parameters:paramet, encoder: URLEncodedFormParameterEncoder.default, headers: nil, interceptor: nil).responseJSON(completionHandler: { (response) in
            
            let statusCode = response.response?.statusCode
            //初始化错误返回对象实例
            let errResponse = BaseErrorResponse()
                
            switch response.result{
                
                case .success(let res):
                    
                    if statusCode == 200{
                    
                        //初始化数据返回对象实例
                        let josnStr = res as? [String: Any]
                        let respModel = AlarmLogResponse.deserialize(from: josnStr)
                        
                        if respModel != nil {
                            if respModel?.code == 200 {
                                success(respModel!)
                            }else{
                                let response = BaseErrorResponse.deserialize(from: josnStr)
                                failed(self.formateErrorResponse(response: response!))
                            }
                        }else{
                            errResponse.errCode = RequestResponseCode.dataParsingException
                            errResponse.errMessage = "数据解析异常"
                            failed(errResponse)
                        }
                    }else{
                        failed(self.formateErrorResponse(response: self.handleErrorResponse(res: res)))
                    }
                break
                
                case .failure(let error):
                    
                    failed(self.formateFailedResponse(faile: error))
                    
                break
            
            }
            
        })
    
    }
    /// 获取项目子类型列表
    func getProjectSubTypeList(domainId:String,enterpriseId:String,typeId:String,childTypeName:String,success:@escaping (_ response:InspectionSubProjectResponse) -> Void,failed:@escaping (_ failed:BaseErrorResponse) -> Void){
        
        let pageSize = String.init(format: "%d", 50)
        let pageIndexStr = "1"
        let paramet = ["domainId":domainId,"enterpriseId":enterpriseId,"typeId":typeId,"childTypeName":"","orderByColumn":"1","isDescOrder":"true","pageIndex":pageIndexStr,"pageSize":pageSize]
        
    self.sessionManager?.request(UrlHost.SysAuth.sysGetProjectSubTypeAPI, method: .get, parameters:paramet, encoder: URLEncodedFormParameterEncoder.default, headers: nil, interceptor: nil).responseJSON(completionHandler: { (response) in
            
            let statusCode = response.response?.statusCode
            //初始化错误返回对象实例
            let errResponse = BaseErrorResponse()
                
            switch response.result{
                
                case .success(let res):
                    
                    if statusCode == 200{
                    
                        //初始化数据返回对象实例
                        let josnStr = res as? [String: Any]
                        let respModel = InspectionSubProjectResponse.deserialize(from: josnStr)
                        
                        if respModel != nil {
                            if respModel?.code == 200 {
                                success(respModel!)
                            }else{
                                let response = BaseErrorResponse.deserialize(from: josnStr)
                                failed(self.formateErrorResponse(response: response!))
                            }
                        }else{
                            errResponse.errCode = RequestResponseCode.dataParsingException
                            errResponse.errMessage = "数据解析异常"
                            failed(errResponse)
                        }
                    }else{
                        failed(self.formateErrorResponse(response: self.handleErrorResponse(res: res)))
                    }
                break
                
                case .failure(let error):
                    
                    failed(self.formateFailedResponse(faile: error))
                    
                break
            
            }
            
        })
    
    }
    
    /// 获取设备运行状态
    func getDeviceStatus(deviceId:String,eventId:String,deviceIdType:String,systemCategory:String,success:@escaping (_ response:InspectionLogDeviceStatusResponse) -> Void,failed:@escaping (_ failed:BaseErrorResponse) -> Void){
        
        let paramet = ["deviceId":deviceId,"eventId":eventId,"deviceIdType":deviceIdType,"systemCategory":systemCategory]
        
    self.sessionManager?.request(UrlHost.AlarmLog.getDeviceStatue, method: .get, parameters:paramet, encoder: URLEncodedFormParameterEncoder.default, headers: nil, interceptor: nil).responseJSON(completionHandler: { (response) in
            
            let statusCode = response.response?.statusCode
            //初始化错误返回对象实例
            let errResponse = BaseErrorResponse()
                
            switch response.result{
                
                case .success(let res):
                    
                    if statusCode == 200{
                    
                        //初始化数据返回对象实例
                        let josnStr = res as? [String: Any]
                        let respModel = InspectionLogDeviceStatusResponse.deserialize(from: josnStr)
                        
                        if respModel != nil {
                            if respModel?.code == 200 {
                                success(respModel!)
                            }else{
                                let response = BaseErrorResponse.deserialize(from: josnStr)
                                failed(self.formateErrorResponse(response: response!))
                            }
                        }else{
                            errResponse.errCode = RequestResponseCode.dataParsingException
                            errResponse.errMessage = "数据解析异常"
                            failed(errResponse)
                        }
                    }else{
                        failed(self.formateErrorResponse(response: self.handleErrorResponse(res: res)))
                    }
                break
                
                case .failure(let error):
                    
                    failed(self.formateFailedResponse(faile: error))
                    
                break
            
            }
            
        })
    
    }
    
    /// 获取警情详情状态
    func getAlarmDetail(deviceId:String,eventId:String,deviceIdType:String,systemCategory:String,success:@escaping (_ response:InspectionAlarmLogDetailResponse) -> Void,failed:@escaping (_ failed:BaseErrorResponse) -> Void){
        
        let paramet = ["deviceId":deviceId,"eventId":eventId,"deviceIdType":deviceIdType,"systemCategory":systemCategory]
        
    self.sessionManager?.request(UrlHost.AlarmLog.getAlarmDetail, method: .get, parameters:paramet, encoder: URLEncodedFormParameterEncoder.default, headers: nil, interceptor: nil).responseJSON(completionHandler: { (response) in
            
            let statusCode = response.response?.statusCode
            //初始化错误返回对象实例
            let errResponse = BaseErrorResponse()
                
            switch response.result{
                
                case .success(let res):
                    
                    if statusCode == 200{
                    
                        //初始化数据返回对象实例
                        let josnStr = res as? [String: Any]
                        let respModel = InspectionAlarmLogDetailResponse.deserialize(from: josnStr)
                        
                        if respModel != nil {
                            if respModel?.code == 200 {
                                success(respModel!)
                            }else{
                                let response = BaseErrorResponse.deserialize(from: josnStr)
                                failed(self.formateErrorResponse(response: response!))
                            }
                        }else{
                            errResponse.errCode = RequestResponseCode.dataParsingException
                            errResponse.errMessage = "数据解析异常"
                            failed(errResponse)
                        }
                    }else{
                        failed(self.formateErrorResponse(response: self.handleErrorResponse(res: res)))
                    }
                break
                
                case .failure(let error):
                    
                    failed(self.formateFailedResponse(faile: error))
                    
                break
            
            }
            
        })
    
    }
    
    /// 获取警情处理信息
    func getAlarmHandleDetail(deviceId:String,eventId:String,deviceIdType:String,systemCategory:String,success:@escaping (_ response:InspectionAlarmHandleDetailResponse) -> Void,failed:@escaping (_ failed:BaseErrorResponse) -> Void){
        
        let paramet = ["deviceId":deviceId,"eventId":eventId,"deviceIdType":deviceIdType,"systemCategory":systemCategory]
        
        self.sessionManager?.request(UrlHost.AlarmLog.getAlarmHandleDetail, method: .get, parameters:paramet, encoder: URLEncodedFormParameterEncoder.default, headers: nil, interceptor: nil).responseJSON(completionHandler: { (response) in
                
                let statusCode = response.response?.statusCode
                //初始化错误返回对象实例
                let errResponse = BaseErrorResponse()
                    
                switch response.result{
                    
                    case .success(let res):
                        
                        if statusCode == 200{
                        
                            //初始化数据返回对象实例
                            let josnStr = res as? [String: Any]
                            let respModel = InspectionAlarmHandleDetailResponse.deserialize(from: josnStr)
                            
                            if respModel != nil {
                                if respModel?.code == 200 {
                                    success(respModel!)
                                }else{
                                    let response = BaseErrorResponse.deserialize(from: josnStr)
                                    failed(self.formateErrorResponse(response: response!))
                                }
                            }else{
                                errResponse.errCode = RequestResponseCode.dataParsingException
                                errResponse.errMessage = "数据解析异常"
                                failed(errResponse)
                            }
                        }else{
                            failed(self.formateErrorResponse(response: self.handleErrorResponse(res: res)))
                        }
                    break
                    
                    case .failure(let error):
                        
                        failed(self.formateFailedResponse(faile: error))
                        
                    break
                
                }
                
            })
    
    }
    
    /// 获取维保任务列表
    func getProtectionTaskList(planName:String,frequency:String,pageIndex:Int,success:@escaping (_ response:ProtectionTaskList) -> Void,failed:@escaping (_ failed:BaseErrorResponse) -> Void){
        
        let pageSize = String.init(format: "%d", 10)
        let pageIndexStr = String.init(format: "%d", pageIndex)
        
        let paramet = ["planName":planName,"frequency":frequency,"pageIndex":pageIndexStr,"pageSize":pageSize]
        
        self.sessionManager?.request(UrlHost.FireProtection.getProtectionTaskList, method: .get, parameters:paramet, encoder: URLEncodedFormParameterEncoder.default, headers: nil, interceptor: nil).responseJSON(completionHandler: { (response) in
                
                let statusCode = response.response?.statusCode
                //初始化错误返回对象实例
                let errResponse = BaseErrorResponse()
                    
                switch response.result{
                    
                    case .success(let res):
                        
                        if statusCode == 200{
                        
                            //初始化数据返回对象实例
                            let josnStr = res as? [String: Any]
                            let respModel = ProtectionTaskList.deserialize(from: josnStr)
                            
                            if respModel != nil {
                                if respModel?.code == 200 {
                                    success(respModel!)
                                }else{
                                    let response = BaseErrorResponse.deserialize(from: josnStr)
                                    failed(self.formateErrorResponse(response: response!))
                                }
                            }else{
                                errResponse.errCode = RequestResponseCode.dataParsingException
                                errResponse.errMessage = "数据解析异常"
                                failed(errResponse)
                            }
                        }else{
                            failed(self.formateErrorResponse(response: self.handleErrorResponse(res: res)))
                        }
                    break
                    
                    case .failure(let error):
                        
                        failed(self.formateFailedResponse(faile: error))
                        
                    break
                
                }
                
            })
    
    }
    
    /// 获取维保计划列表
    func getProtectionPlanList(planName:String,pageIndex:Int,success:@escaping (_ response:ProtectionPlanList) -> Void,failed:@escaping (_ failed:BaseErrorResponse) -> Void){
        
        let pageSize = String.init(format: "%d", 10)
        let pageIndexStr = String.init(format: "%d", pageIndex)
        
        let paramet = ["planName":planName,"pageIndex":pageIndexStr,"pageSize":pageSize]
        
        self.sessionManager?.request(UrlHost.FireProtection.getProtectionPlanList, method: .get, parameters:paramet, encoder: URLEncodedFormParameterEncoder.default, headers: nil, interceptor: nil).responseJSON(completionHandler: { (response) in
                
                let statusCode = response.response?.statusCode
                //初始化错误返回对象实例
                let errResponse = BaseErrorResponse()
                    
                switch response.result{
                    
                    case .success(let res):
                        
                        if statusCode == 200{
                        
                            //初始化数据返回对象实例
                            let josnStr = res as? [String: Any]
                            let respModel = ProtectionPlanList.deserialize(from: josnStr)
                            
                            if respModel != nil {
                                if respModel?.code == 200 {
                                    success(respModel!)
                                }else{
                                    let response = BaseErrorResponse.deserialize(from: josnStr)
                                    failed(self.formateErrorResponse(response: response!))
                                }
                            }else{
                                errResponse.errCode = RequestResponseCode.dataParsingException
                                errResponse.errMessage = "数据解析异常"
                                failed(errResponse)
                            }
                        }else{
                            failed(self.formateErrorResponse(response: self.handleErrorResponse(res: res)))
                        }
                    break
                    
                    case .failure(let error):
                        
                        failed(self.formateFailedResponse(faile: error))
                        
                    break
                
                }
                
            })
    
    }
    
    /// 根据维保计划获取维保任务列表
    func getProtectionTaskListByPlanId(planId:String,pageIndex:Int,success:@escaping (_ response:ProtectionTaskDetailList) -> Void,failed:@escaping (_ failed:BaseErrorResponse) -> Void){
        
        let pageSize = String.init(format: "%d", 10)
        let pageIndexStr = String.init(format: "%d", pageIndex)
        
        let paramet = ["planId":planId,"pageIndex":pageIndexStr,"pageSize":pageSize]
        
        self.sessionManager?.request(UrlHost.FireProtection.getProtectionTaskListByPlanId, method: .get, parameters:paramet, encoder: URLEncodedFormParameterEncoder.default, headers: nil, interceptor: nil).responseJSON(completionHandler: { (response) in
                
                let statusCode = response.response?.statusCode
                //初始化错误返回对象实例
                let errResponse = BaseErrorResponse()
                    
                switch response.result{
                    
                    case .success(let res):
                        
                        if statusCode == 200{
                        
                            //初始化数据返回对象实例
                            let josnStr = res as? [String: Any]
                            let respModel = ProtectionTaskDetailList.deserialize(from: josnStr)
                            
                            if respModel != nil {
                                if respModel?.code == 200 {
                                    success(respModel!)
                                }else{
                                    let response = BaseErrorResponse.deserialize(from: josnStr)
                                    failed(self.formateErrorResponse(response: response!))
                                }
                            }else{
                                errResponse.errCode = RequestResponseCode.dataParsingException
                                errResponse.errMessage = "数据解析异常"
                                failed(errResponse)
                            }
                        }else{
                            failed(self.formateErrorResponse(response: self.handleErrorResponse(res: res)))
                        }
                    break
                    
                    case .failure(let error):
                        
                        failed(self.formateFailedResponse(faile: error))
                        
                    break
                
                }
                
            })
    
    }
    
    /// 获取维保工单列表
    func getProtectionOrderList(pageIndex:Int,success:@escaping (_ response:ProtectionOrderList) -> Void,failed:@escaping (_ failed:BaseErrorResponse) -> Void){
        
        let pageSize = String.init(format: "%d", 10)
        let pageIndexStr = String.init(format: "%d", pageIndex)
        
        let paramet = ["orderStatus":"","orderByColumn":"name","isDescOrder":"2","pageIndex":pageIndexStr,"pageSize":pageSize]
        
        self.sessionManager?.request(UrlHost.FireProtection.getOrderList, method: .get, parameters:paramet, encoder: URLEncodedFormParameterEncoder.default, headers: nil, interceptor: nil).responseJSON(completionHandler: { (response) in
                
                let statusCode = response.response?.statusCode
                //初始化错误返回对象实例
                let errResponse = BaseErrorResponse()
                    
                switch response.result{
                    
                    case .success(let res):
                        
                        if statusCode == 200{
                        
                            //初始化数据返回对象实例
                            let josnStr = res as? [String: Any]
                            let respModel = ProtectionOrderList.deserialize(from: josnStr)
                            
                            if respModel != nil {
                                if respModel?.code == 200 {
                                    success(respModel!)
                                }else{
                                    let response = BaseErrorResponse.deserialize(from: josnStr)
                                    failed(self.formateErrorResponse(response: response!))
                                }
                            }else{
                                errResponse.errCode = RequestResponseCode.dataParsingException
                                errResponse.errMessage = "数据解析异常"
                                failed(errResponse)
                            }
                        }else{
                            failed(self.formateErrorResponse(response: self.handleErrorResponse(res: res)))
                        }
                    break
                    
                    case .failure(let error):
                        
                        failed(self.formateFailedResponse(faile: error))
                        
                    break
                
                }
                
            })
    
    }
    
    /// 获取维保消息提醒列表
    func getProtectionMessageList(pageIndex:Int,success:@escaping (_ response:ProtectionMessageList) -> Void,failed:@escaping (_ failed:BaseErrorResponse) -> Void){
        
        let pageSize = String.init(format: "%d", 10)
        let pageIndexStr = String.init(format: "%d", pageIndex)
        
        let paramet = ["isOrderDesc":"true","pageIndex":pageIndexStr,"pageSize":pageSize]
        
        self.sessionManager?.request(UrlHost.FireProtection.getProtectionMessageList, method: .get, parameters:paramet, encoder: URLEncodedFormParameterEncoder.default, headers: nil, interceptor: nil).responseJSON(completionHandler: { (response) in
                
                let statusCode = response.response?.statusCode
                //初始化错误返回对象实例
                let errResponse = BaseErrorResponse()
                    
                switch response.result{
                    
                    case .success(let res):
                        
                        if statusCode == 200{
                        
                            //初始化数据返回对象实例
                            let josnStr = res as? [String: Any]
                            let respModel = ProtectionMessageList.deserialize(from: josnStr)
                            
                            if respModel != nil {
                                if respModel?.code == 200 {
                                    success(respModel!)
                                }else{
                                    let response = BaseErrorResponse.deserialize(from: josnStr)
                                    failed(self.formateErrorResponse(response: response!))
                                }
                            }else{
                                errResponse.errCode = RequestResponseCode.dataParsingException
                                errResponse.errMessage = "数据解析异常"
                                failed(errResponse)
                            }
                        }else{
                            failed(self.formateErrorResponse(response: self.handleErrorResponse(res: res)))
                        }
                    break
                    
                    case .failure(let error):
                        
                        failed(self.formateFailedResponse(faile: error))
                        
                    break
                
                }
                
            })
    
    }
    
    /// 获取维保材料列表
    func getMaterialsList(pageIndex:Int,success:@escaping (_ response:ProtectionMaterialsList) -> Void,failed:@escaping (_ failed:BaseErrorResponse) -> Void){
        
        let pageSize = String.init(format: "%d", 1000)
        let pageIndexStr = String.init(format: "%d", 1)
        
        let paramet = ["materialsType":"1","pageIndex":pageIndexStr,"pageSize":pageSize,"materialsName":""]
        
        self.sessionManager?.request(UrlHost.FireProtection.getMaterialsList, method: .get, parameters:paramet, encoder: URLEncodedFormParameterEncoder.default, headers: nil, interceptor: nil).responseJSON(completionHandler: { (response) in
                
                let statusCode = response.response?.statusCode
                //初始化错误返回对象实例
                let errResponse = BaseErrorResponse()
                    
                switch response.result{
                    
                    case .success(let res):
                        
                        if statusCode == 200{
                        
                            //初始化数据返回对象实例
                            let josnStr = res as? [String: Any]
                            let respModel = ProtectionMaterialsList.deserialize(from: josnStr)
                            
                            if respModel != nil {
                                if respModel?.code == 200 {
                                    success(respModel!)
                                }else{
                                    let response = BaseErrorResponse.deserialize(from: josnStr)
                                    failed(self.formateErrorResponse(response: response!))
                                }
                            }else{
                                errResponse.errCode = RequestResponseCode.dataParsingException
                                errResponse.errMessage = "数据解析异常"
                                failed(errResponse)
                            }
                        }else{
                            failed(self.formateErrorResponse(response: self.handleErrorResponse(res: res)))
                        }
                    break
                    
                    case .failure(let error):
                        
                        failed(self.formateFailedResponse(faile: error))
                        
                    break
                
                }
                
            })
    
    }
    /// 获取维保完成率
    func getMtaStatistics(success:@escaping (_ response:ProtectionHomeStatistics) -> Void,failed:@escaping (_ failed:BaseErrorResponse) -> Void){
        
        self.sessionManager?.request(UrlHost.FireProtection.getProtectionStatistics, method: .get,parameters:defaultParameters,encoder: URLEncodedFormParameterEncoder.default, headers: nil, interceptor: nil).responseJSON(completionHandler: { (response) in
                
                let statusCode = response.response?.statusCode
                //初始化错误返回对象实例
                let errResponse = BaseErrorResponse()
                    
                switch response.result{
                    
                    case .success(let res):
                        
                        if statusCode == 200{
                        
                            //初始化数据返回对象实例
                            let josnStr = res as? [String: Any]
                            let respModel = ProtectionHomeStatistics.deserialize(from: josnStr)
                            
                            if respModel != nil {
                                if respModel?.code == 200 {
                                    success(respModel!)
                                }else{
                                    let response = BaseErrorResponse.deserialize(from: josnStr)
                                    failed(self.formateErrorResponse(response: response!))
                                }
                            }else{
                                errResponse.errCode = RequestResponseCode.dataParsingException
                                errResponse.errMessage = "数据解析异常"
                                failed(errResponse)
                            }
                        }else{
                            failed(self.formateErrorResponse(response: self.handleErrorResponse(res: res)))
                        }
                    break
                    
                    case .failure(let error):
                        
                        failed(self.formateFailedResponse(faile: error))
                        
                    break
                
                }
                
            })
    
    }
    
    /// 获取入场打卡时间
    func getEntranceTime(success:@escaping (_ response:HandleEnterance) -> Void,failed:@escaping (_ failed:BaseErrorResponse) -> Void){
        
        self.sessionManager?.request(UrlHost.FireProtection.handleEntrance, method: .post, parameters:[""], encoder: JSONParameterEncoder.default, headers: nil, interceptor: nil).responseJSON(completionHandler: { (response) in
                
                let statusCode = response.response?.statusCode
                //初始化错误返回对象实例
                let errResponse = BaseErrorResponse()
                    
                switch response.result{
                    
                    case .success(let res):
                        
                        if statusCode == 200{
                        
                            //初始化数据返回对象实例
                            let josnStr = res as? [String: Any]
                            let respModel = HandleEnterance.deserialize(from: josnStr)
                            
                            if respModel != nil {
                                if respModel?.code == 200 {
                                    success(respModel!)
                                }else{
                                    let response = BaseErrorResponse.deserialize(from: josnStr)
                                    failed(self.formateErrorResponse(response: response!))
                                }
                            }else{
                                errResponse.errCode = RequestResponseCode.dataParsingException
                                errResponse.errMessage = "数据解析异常"
                                failed(errResponse)
                            }
                        }else{
                            failed(self.formateErrorResponse(response: self.handleErrorResponse(res: res)))
                        }
                    break
                    
                    case .failure(let error):
                        
                        failed(self.formateFailedResponse(faile: error))
                        
                    break
                
                }
                
            })
    
    }
    
    /// 获取维保任务详情
    func getProtectionDetailById(taskId:String?,success:@escaping (_ response:ProtectionTaskResponse) -> Void,failed:@escaping (_ failed:BaseErrorResponse) -> Void){
        
    
        self.sessionManager?.request(UrlHost.FireProtection.getProtectionTaskDetail, method: .get, parameters:["taskId":taskId], encoder: URLEncodedFormParameterEncoder.default, headers: nil, interceptor: nil).responseJSON(completionHandler: { (response) in
                
                let statusCode = response.response?.statusCode
                //初始化错误返回对象实例
                let errResponse = BaseErrorResponse()
                    
                switch response.result{
                    
                    case .success(let res):
                        
                        if statusCode == 200{
                        
                            //初始化数据返回对象实例
                            let josnStr = res as? [String: Any]
                            let respModel = ProtectionTaskResponse.deserialize(from: josnStr)
                            
                            if respModel != nil {
                                if respModel?.code == 200 {
                                    success(respModel!)
                                }else{
                                    let response = BaseErrorResponse.deserialize(from: josnStr)
                                    failed(self.formateErrorResponse(response: response!))
                                }
                            }else{
                                errResponse.errCode = RequestResponseCode.dataParsingException
                                errResponse.errMessage = "数据解析异常"
                                failed(errResponse)
                            }
                        }else{
                            failed(self.formateErrorResponse(response: self.handleErrorResponse(res: res)))
                        }
                    break
                    
                    case .failure(let error):
                        
                        failed(self.formateFailedResponse(faile: error))
                        
                    break
                
                }
                
            })
    
    }
    
    /// 获取工单详情
    func getProtectionOrderDetailById(acceptId:String?,success:@escaping (_ response:ProtectionOrderResponse) -> Void,failed:@escaping (_ failed:BaseErrorResponse) -> Void){
        
    
        self.sessionManager?.request(UrlHost.FireProtection.orderHandleDetail, method: .get, parameters:["acceptId":acceptId], encoder: URLEncodedFormParameterEncoder.default, headers: nil, interceptor: nil).responseJSON(completionHandler: { (response) in
                
                let statusCode = response.response?.statusCode
                //初始化错误返回对象实例
                let errResponse = BaseErrorResponse()
                    
                switch response.result{
                    
                    case .success(let res):
                        
                        if statusCode == 200{
                        
                            //初始化数据返回对象实例
                            let josnStr = res as? [String: Any]
                            let respModel = ProtectionOrderResponse.deserialize(from: josnStr)
                            
                            if respModel != nil {
                                if respModel?.code == 200 {
                                    success(respModel!)
                                }else{
                                    let response = BaseErrorResponse.deserialize(from: josnStr)
                                    failed(self.formateErrorResponse(response: response!))
                                }
                            }else{
                                errResponse.errCode = RequestResponseCode.dataParsingException
                                errResponse.errMessage = "数据解析异常"
                                failed(errResponse)
                            }
                        }else{
                            failed(self.formateErrorResponse(response: self.handleErrorResponse(res: res)))
                        }
                    break
                    
                    case .failure(let error):
                        
                        failed(self.formateFailedResponse(faile: error))
                        
                    break
                
                }
                
            })
    
    }
    /// 获取工单历史记录
    func getProtectionOrderHistoryRecById(orderId:String?,success:@escaping (_ response:ProtectionOrderHistoryRecResponse) -> Void,failed:@escaping (_ failed:BaseErrorResponse) -> Void){
        
    
        self.sessionManager?.request(UrlHost.FireProtection.orderHandleHistory, method: .get, parameters:["orderId":orderId], encoder: URLEncodedFormParameterEncoder.default, headers: nil, interceptor: nil).responseJSON(completionHandler: { (response) in
                
                let statusCode = response.response?.statusCode
                //初始化错误返回对象实例
                let errResponse = BaseErrorResponse()
                    
                switch response.result{
                    
                    case .success(let res):
                        
                        if statusCode == 200{
                        
                            //初始化数据返回对象实例
                            let josnStr = res as? [String: Any]
                            let respModel = ProtectionOrderHistoryRecResponse.deserialize(from: josnStr)
                            
                            if respModel != nil {
                                if respModel?.code == 200 {
                                    success(respModel!)
                                }else{
                                    let response = BaseErrorResponse.deserialize(from: josnStr)
                                    failed(self.formateErrorResponse(response: response!))
                                }
                            }else{
                                errResponse.errCode = RequestResponseCode.dataParsingException
                                errResponse.errMessage = "数据解析异常"
                                failed(errResponse)
                            }
                        }else{
                            failed(self.formateErrorResponse(response: self.handleErrorResponse(res: res)))
                        }
                    break
                    
                    case .failure(let error):
                        
                        failed(self.formateFailedResponse(faile: error))
                        
                    break
                
                }
                
            })
    
    }
    
    /// 隐患上报 正常巡检任务
    /// - Parameters:
    ///   - ResultPointId: <#ResultPointId description#>
    ///   - PointId: <#PointId description#>
    ///   - TypeId: <#TypeId description#>
    ///   - KeypartId: <#KeypartId description#>
    ///   - ChildTypeId: <#ChildTypeId description#>
    ///   - Content: <#Content description#>
    ///   - ItemResultList: <#ItemResultList description#>
    ///   - annexArr: <#annexArr description#>
    ///   - voiceArr: <#voiceArr description#>
    ///   - success: <#success description#>
    ///   - failed: <#failed description#>
    func TaskWarnningReport(ResultPointId:String,PointId:String,TypeId:String,KeypartId:String,ChildTypeId:String,Content:String,ItemResultList:Array<[String:String]>,videoArr:Array<AnnexModel>,imageArr:Array<AnnexModel>,audioArr:Array<AnnexModel>,success:@escaping (_ response:BaseResponse) -> Void,failed:@escaping (_ failed:BaseErrorResponse) -> Void) {
        
//        let arrData = NSKeyedArchiver.archivedData(withRootObject: ItemResultList)
        
        let arrData =  try! JSONSerialization.data(withJSONObject: ItemResultList, options: .prettyPrinted)
        
        self.sessionManager?.upload(multipartFormData: { (formData) in
            
            formData.append(Data.init(ResultPointId.utf8), withName: "ResultPointId")
            formData.append(Data.init(PointId.utf8), withName: "PointId")
            formData.append(Data.init(TypeId.utf8), withName: "TypeId")
            formData.append(Data.init(KeypartId.utf8), withName: "KeypartId")
            formData.append(Data.init(ChildTypeId.utf8), withName: "ChildTypeId")
            formData.append(Data.init(Content.utf8), withName: "Content")
            
            formData.append(arrData, withName: "ItemResultList")
            var num = 1
            for model in videoArr{
              
                let name = "video"+String(num)+".mp4"
                formData.append(try! Data.init(contentsOf: model.sourceFile!), withName: name, fileName: name, mimeType: "video/mp4")
                num+=1
                
            }
            
            for model in imageArr{
              
                let name = "image"+String(num)+".png"
                formData.append(model.imageSource!.pngData()!, withName: name, fileName: name, mimeType: "image/png")
                    num+=1
            }
                
            
            
            for model in audioArr{
                
                let name = "audio"+String(num)+".mp3"
                formData.append(try! Data.init(contentsOf: model.sourceFile!), withName: name,fileName: name, mimeType: "audio/mpeg")
                num+=1
                
            }
            
        }, to: UrlHost.InspectionTask.TaskWarningUpload).responseJSON(completionHandler: { (response) in
            
            let statusCode = response.response?.statusCode
            //初始化错误返回对象实例
            let errResponse = BaseErrorResponse()
                
            switch response.result{
                
                case .success(let res):
                    
                    if statusCode == 200{
                    
                        //初始化数据返回对象实例
                        let josnStr = res as? [String: Any]
                        let respModel = BaseResponse.deserialize(from: josnStr)
                        
                        if respModel != nil {
                            if respModel?.code == 200 {
                                success(respModel!)
                            }else{
                                let response = BaseErrorResponse.deserialize(from: josnStr)
                                failed(self.formateErrorResponse(response: response!))
                            }
                        }else{
                            errResponse.errCode = RequestResponseCode.dataParsingException
                            errResponse.errMessage = "数据解析异常"
                            failed(errResponse)
                        }
                    }else{
                        failed(self.formateErrorResponse(response: self.handleErrorResponse(res: res)))
                    }
                break
                
                case .failure(let error):
                    
                    failed(self.formateFailedResponse(faile: error))
                    
                break
            
            }
            
        })
        
        
    }
    
    
    /// 工单处理 execProtectionTask
    /// - Parameters:
    ///   - model: 上传数据model
    ///   - success: 成功回调
    ///   - failed: 失败回调
    func protectionOrderHandle(model:ProtectionHandleReportModel,success:@escaping (_ response:BaseResponse) -> Void,failed:@escaping (_ failed:BaseErrorResponse) -> Void) {
            
        let arrData =  try! JSONSerialization.data(withJSONObject: model.materialsArr as Any, options: .prettyPrinted)
            
            self.sessionManager?.upload(multipartFormData: { (formData) in
                
                formData.append(Data.init(model.acceptOrderId!.utf8), withName: "acceptOrderId")
                formData.append(Data.init(model.entranceTime!.utf8), withName: "entranceTime")
                formData.append(Data.init(model.handleText!.utf8), withName: "handleText")
                formData.append(Data.init(model.workHours!.utf8), withName: "workHours")
                formData.append(Data.init((model.MaintenanceBelocationText ?? "").utf8), withName: "MaintenanceBelocationText")
                formData.append(Data.init((model.MaintenanceLocationText ?? "").utf8), withName: "MaintenanceLocationText")
                formData.append(Data.init((model.Longitude ?? "").utf8), withName: "Longitude")
                formData.append(Data.init((model.Latitue ?? "").utf8), withName: "Latitue")
                formData.append(arrData, withName: "materials")
                var num = 1
                
                for model in model.videoArr {
                  
                    let name = "video"+String(num)+".mp4"
                    formData.append(try! Data.init(contentsOf: model.sourceFile!), withName: name, fileName: name, mimeType: "video/mp4")
                    num+=1
                    
                }
                
                for model in model.imageArr {
                  
                    let name = "image"+String(num)+".png"
                    formData.append(model.imageSource!.pngData()!, withName: name, fileName: name, mimeType: "image/png")
                        num+=1
                }
                    
                
                
                for model in model.audioArr {
                    
                    let name = "audio"+String(num)+".mp3"
                    formData.append(try! Data.init(contentsOf: model.sourceFile!), withName: name,fileName: name, mimeType: "audio/mpeg")
                    num+=1
                    
                }
                
            }, to: UrlHost.FireProtection.handleOrder).responseJSON(completionHandler: { (response) in
                
                let statusCode = response.response?.statusCode
                //初始化错误返回对象实例
                let errResponse = BaseErrorResponse()
                    
                switch response.result{
                    
                    case .success(let res):
                        
                        if statusCode == 200{
                        
                            //初始化数据返回对象实例
                            let josnStr = res as? [String: Any]
                            let respModel = BaseResponse.deserialize(from: josnStr)
                            
                            if respModel != nil {
                                if respModel?.code == 200 {
                                    success(respModel!)
                                }else{
                                    let response = BaseErrorResponse.deserialize(from: josnStr)
                                    failed(self.formateErrorResponse(response: response!))
                                }
                            }else{
                                errResponse.errCode = RequestResponseCode.dataParsingException
                                errResponse.errMessage = "数据解析异常"
                                failed(errResponse)
                            }
                        }else{
                            failed(self.formateErrorResponse(response: self.handleErrorResponse(res: res)))
                        }
                    break
                    
                    case .failure(let error):
                        
                        failed(self.formateFailedResponse(faile: error))
                        
                    break
                
                }
                
            })
            
            
        }
    
    /// 维保任务处理
    /// - Parameters:
    ///   - model: 上传数据model
    ///   - success: 成功回调
    ///   - failed: 失败回调
    func protectionTaskHandle(model:ProtectionTaskUploadModel,success:@escaping (_ response:BaseResponse) -> Void,failed:@escaping (_ failed:BaseErrorResponse) -> Void) {
            
        let arrData =  try! JSONSerialization.data(withJSONObject: model.materialsArr as Any, options: .prettyPrinted)
            
            self.sessionManager?.upload(multipartFormData: { (formData) in
                
                formData.append(Data.init(model.taskId!.utf8), withName: "taskId")
                formData.append(Data.init(model.entId!.utf8), withName: "entId")
                formData.append(Data.init(model.maintenancePersonnelId!.utf8), withName: "maintenancePersonnelId")
                formData.append(Data.init(model.entranceTime!.utf8), withName: "entranceTime")
                formData.append(Data.init(model.workHours!.utf8), withName: "workHours")
                formData.append(Data.init(model.maintenanceContent!.utf8), withName: "maintenanceContent")
                formData.append(Data.init(model.MaintenanceBelocationText!.utf8), withName: "maintenanceBelocationText")
                formData.append(Data.init(model.MaintenanceLocationText!.utf8), withName: "maintenanceLocationText")
                formData.append(Data.init(model.Longitude!.utf8), withName: "longitude")
                formData.append(Data.init(model.Latitue!.utf8), withName: "latitue")
                formData.append(arrData, withName: "materials")
                var num = 1
                
                for model in model.videoArr ?? []{
                  
                    let name = "video"+String(num)+".mp4"
                    formData.append(try! Data.init(contentsOf: model.sourceFile!), withName: name, fileName: name, mimeType: "video/mp4")
                    num+=1
                    
                }
                
                for model in model.imageArr ?? []{
                  
                    let name = "image"+String(num)+".png"
                    formData.append(model.imageSource!.pngData()!, withName: name, fileName: name, mimeType: "image/png")
                        num+=1
                }
                    
                
            }, to: UrlHost.FireProtection.execProtectionTask).responseJSON(completionHandler: { (response) in
                
                let statusCode = response.response?.statusCode
                //初始化错误返回对象实例
                let errResponse = BaseErrorResponse()
                    
                switch response.result{
                    
                    case .success(let res):
                        
                        if statusCode == 200{
                        
                            //初始化数据返回对象实例
                            let josnStr = res as? [String: Any]
                            let respModel = BaseResponse.deserialize(from: josnStr)
                            
                            if respModel != nil {
                                if respModel?.code == 200 {
                                    success(respModel!)
                                }else{
                                    let response = BaseErrorResponse.deserialize(from: josnStr)
                                    failed(self.formateErrorResponse(response: response!))
                                }
                            }else{
                                errResponse.errCode = RequestResponseCode.dataParsingException
                                errResponse.errMessage = "数据解析异常"
                                failed(errResponse)
                            }
                        }else{
                            failed(self.formateErrorResponse(response: self.handleErrorResponse(res: res)))
                        }
                    break
                    
                    case .failure(let error):
                        
                        failed(self.formateFailedResponse(faile: error))
                        
                    break
                
                }
                
            })
            
            
        }
    
    
    /// 隐患上报无任务
    /// - Parameters:
    ///   - ResultPointId: <#ResultPointId description#>
    ///   - PointId: <#PointId description#>
    ///   - TypeId: <#TypeId description#>
    ///   - KeypartId: <#KeypartId description#>
    ///   - ChildTypeId: <#ChildTypeId description#>
    ///   - Content: <#Content description#>
    ///   - ItemResultList: <#ItemResultList description#>
    ///   - annexArr: <#annexArr description#>
    ///   - voiceArr: <#voiceArr description#>
    ///   - success: <#success description#>
    ///   - failed: <#failed description#>
    func TroubleReport(ResultPointId:String,PointId:String,TypeId:String,KeypartId:String,ChildTypeId:String,Content:String,ItemResultList:Array<[String:String]>,videoArr:Array<AnnexModel>,imageArr:Array<AnnexModel>,voiceArr:Array<AnnexModel>,success:@escaping (_ response:BaseResponse) -> Void,failed:@escaping (_ failed:BaseErrorResponse) -> Void) {
            
    //        let arrData = NSKeyedArchiver.archivedData(withRootObject: ItemResultList)
            
            let arrData =  try! JSONSerialization.data(withJSONObject: ItemResultList, options: .prettyPrinted)
            
            self.sessionManager?.upload(multipartFormData: { (formData) in
                
                formData.append(Data.init(ResultPointId.utf8), withName: "ResultPointId")
                formData.append(Data.init(PointId.utf8), withName: "PointId")
                formData.append(Data.init(TypeId.utf8), withName: "TypeId")
                formData.append(Data.init(KeypartId.utf8), withName: "KeypartId")
                formData.append(Data.init(ChildTypeId.utf8), withName: "ChildTypeId")
                formData.append(Data.init(Content.utf8), withName: "Content")
                
                formData.append(arrData, withName: "ItemResultList")
                var num = 1
                
                for model in videoArr{
                  
                    let name = "video"+String(num)+".mp4"
                    formData.append(try! Data.init(contentsOf: model.sourceFile!), withName: name, fileName: name, mimeType: "video/mp4")
                    num+=1
                    
                }
                
                for model in imageArr{
                  
                    let name = "image"+String(num)+".png"
                    formData.append(model.imageSource!.pngData()!, withName: name, fileName: name, mimeType: "image/png")
                        num+=1
                }
                    
                
                
                for model in voiceArr{
                    
                    let name = "voice"+String(num)+".mp3"
                    formData.append(try! Data.init(contentsOf: model.sourceFile!), withName: name,fileName: name, mimeType: "audio/mpeg")
                    num+=1
                    
                }
                
            }, to: UrlHost.InspectionTask.TaskWarningUpload).responseJSON(completionHandler: { (response) in
                
                let statusCode = response.response?.statusCode
                //初始化错误返回对象实例
                let errResponse = BaseErrorResponse()
                    
                switch response.result{
                    
                    case .success(let res):
                        
                        if statusCode == 200{
                        
                            //初始化数据返回对象实例
                            let josnStr = res as? [String: Any]
                            let respModel = BaseResponse.deserialize(from: josnStr)
                            
                            if respModel != nil {
                                if respModel?.code == 200 {
                                    success(respModel!)
                                }else{
                                    let response = BaseErrorResponse.deserialize(from: josnStr)
                                    failed(self.formateErrorResponse(response: response!))
                                }
                            }else{
                                errResponse.errCode = RequestResponseCode.dataParsingException
                                errResponse.errMessage = "数据解析异常"
                                failed(errResponse)
                            }
                        }else{
                            failed(self.formateErrorResponse(response: self.handleErrorResponse(res: res)))
                        }
                    break
                    
                    case .failure(let error):
                        
                        failed(self.formateFailedResponse(faile: error))
                        
                    break
                
                }
                
            })
            
            
        }
    
    
    /// 隐患处理
    /// - Parameters:
    ///   - PonitInforId: <#PonitInforId description#>
    ///   - TroubleHandleId: <#TroubleHandleId description#>
    ///   - ConfirmResult: <#ConfirmResult description#>
    ///   - ConfirmContent: <#ConfirmContent description#>
    ///   - ItemResultList: <#ItemResultList description#>
    ///   - videoArr: <#videoArr description#>
    ///   - imageArr: <#imageArr description#>
    ///   - voiceArr: <#voiceArr description#>
    ///   - success: <#success description#>
    ///   - failed: <#failed description#>
    func handleTrouble(PointInforId:String,TroubleHandleId:String,ConfirmResult:String,ConfirmContent:String,ItemIdList:Array<[String:String]>,videoArr:Array<AnnexModel>,imageArr:Array<AnnexModel>,voiceArr:Array<AnnexModel>,success:@escaping (_ response:BaseResponse) -> Void,failed:@escaping (_ failed:BaseErrorResponse) -> Void) {

            
            let arrData =  try! JSONSerialization.data(withJSONObject: ItemIdList, options: .prettyPrinted)
            
            self.sessionManager?.upload(multipartFormData: { (formData) in
                
                formData.append(Data.init(PointInforId.utf8), withName: "PointInforId")
                formData.append(Data.init(TroubleHandleId.utf8), withName: "TroubleHandleId")
                formData.append(Data.init(ConfirmResult.utf8), withName: "ConfirmResult")
                formData.append(Data.init(ConfirmContent.utf8), withName: "ConfirmContent")
                
                formData.append(arrData, withName: "ItemIdList")
                var num = 1
                
                for model in videoArr{
                  
                    let name = "video"+String(num)+".mp4"
                    formData.append(try! Data.init(contentsOf: model.sourceFile!), withName: name, fileName: name, mimeType: "video/mp4")
                    num+=1
                    
                }
                
                for model in imageArr{
                  
                    let name = "image"+String(num)+".png"
                    formData.append(model.imageSource!.pngData()!, withName: name, fileName: name, mimeType: "image/png")
                        num+=1
                }
                    
                
                
                for model in voiceArr{
                    
                    let name = "voice"+String(num)+".mp3"
                    formData.append(try! Data.init(contentsOf: model.sourceFile!), withName: name,fileName: name, mimeType: "audio/mpeg")
                    num+=1
                    
                }
                
            }, to: UrlHost.InspectionToubleConfirm.handleTrouble).responseJSON(completionHandler: { (response) in
                
                let statusCode = response.response?.statusCode
                //初始化错误返回对象实例
                let errResponse = BaseErrorResponse()
                    
                switch response.result{
                    
                    case .success(let res):
                        
                        if statusCode == 200{
                        
                            //初始化数据返回对象实例
                            let josnStr = res as? [String: Any]
                            let respModel = BaseResponse.deserialize(from: josnStr)
                            
                            if respModel != nil {
                                if respModel?.code == 200 {
                                    success(respModel!)
                                }else{
                                    let response = BaseErrorResponse.deserialize(from: josnStr)
                                    failed(self.formateErrorResponse(response: response!))
                                }
                            }else{
                                errResponse.errCode = RequestResponseCode.dataParsingException
                                errResponse.errMessage = "数据解析异常"
                                failed(errResponse)
                            }
                        }else{
                            failed(self.formateErrorResponse(response: self.handleErrorResponse(res: res)))
                        }
                    break
                    
                    case .failure(let error):
                        
                        failed(self.formateFailedResponse(faile: error))
                        
                    break
                
                }
                
            })
            
            
        }
    
    
    
    /// 警情处理
    /// - Parameters:
    ///   - eventId: <#eventId description#>
    ///   - eventHandle: <#eventHandle description#>
    ///   - content: <#content description#>
    ///   - systemCategory: <#systemCategory description#>
    ///   - videoArr: <#videoArr description#>
    ///   - imageArr: <#imageArr description#>
    ///   - voiceArr: <#voiceArr description#>
    ///   - success: <#success description#>
    ///   - failed: <#failed description#>
        func alarmHandle(eventId:String,eventHandle:String,content:String,systemCategory:String,videoArr:Array<AnnexModel>,imageArr:Array<AnnexModel>,voiceArr:Array<AnnexModel>,success:@escaping (_ response:BaseResponse) -> Void,failed:@escaping (_ failed:BaseErrorResponse) -> Void) {
                
                
                self.sessionManager?.upload(multipartFormData: { (formData) in
                    
                    formData.append(Data.init(eventId.utf8), withName: "EventID")
                    formData.append(Data.init(eventHandle.utf8), withName: "EventHandle")
                    formData.append(Data.init(content.utf8), withName: "Content")
                    formData.append(Data.init(systemCategory.utf8), withName: "SystemCategory")
                
                    var num = 1
                    for model in videoArr{
                      
                        let name = "video"+String(num)+".mp4"
                        formData.append(try! Data.init(contentsOf: model.sourceFile!), withName: name, fileName: name, mimeType: "video/mp4")
                        num+=1
                        
                    }
                    
                    for model in imageArr{
                      
                        let name = "image"+String(num)+".png"
                        formData.append(model.imageSource!.pngData()!, withName: name, fileName: name, mimeType: "image/png")
                            num+=1
                    }
                        
                    
                    
                    for model in voiceArr{
                        
                        let name = "voice"+String(num)+".mp3"
                        formData.append(try! Data.init(contentsOf: model.sourceFile!), withName: name,fileName: name, mimeType: "audio/mpeg")
                        num+=1
                        
                    }
                    
                }, to: UrlHost.AlarmLog.handleAlarm).responseJSON(completionHandler: { (response) in
                    
                    let statusCode = response.response?.statusCode
                    //初始化错误返回对象实例
                    let errResponse = BaseErrorResponse()
                        
                    switch response.result{
                        
                        case .success(let res):
                            
                            if statusCode == 200{
                            
                                //初始化数据返回对象实例
                                let josnStr = res as? [String: Any]
                                let respModel = BaseResponse.deserialize(from: josnStr)
                                
                                if respModel != nil {
                                    if respModel?.code == 200 {
                                        success(respModel!)
                                    }else{
                                        let response = BaseErrorResponse.deserialize(from: josnStr)
                                        failed(self.formateErrorResponse(response: response!))
                                    }
                                }else{
                                    errResponse.errCode = RequestResponseCode.dataParsingException
                                    errResponse.errMessage = "数据解析异常"
                                    failed(errResponse)
                                }
                            }else{
                                failed(self.formateErrorResponse(response: self.handleErrorResponse(res: res)))
                            }
                        break
                        
                        case .failure(let error):
                            
                            failed(self.formateFailedResponse(faile: error))
                            
                        break
                    
                    }
                    
                })
                
                
            }
    
    
    /// 获取设备查询网关列表
    /// - Parameter url: 请求URL
    /// - Parameter keyword: 搜索用关键字
    /// - Parameter domainId: 筛选用中心ID
    /// - Parameter enterpriseId: 筛选用企业ID
    /// - Parameter buildingId: 筛选用建筑ID
    /// - Parameter keypartId: 筛选用部位ID
    /// - Parameter orderByColumn: 根据条件排序 默认0  None = 0,Status = 1,HeartTime = 2,Name = 3,omainName = 4,
    /// EntName = 5,BuildingName = 6,KeyPartName = 7,Code = 8,
    /// - Parameter isDescOrder: 是否倒序
    /// - Parameter pageIndex: 当前页数
    /// - Parameter success: 成功回调
    /// - Parameter failed: 失败回调
    func getQureyStatsGatewayList(url:String,keyword:String,domainId:String,enterpriseId:String,buildingId:String,keypartId:String,orderByColumn:String,isDescOrder:Bool,pageIndex:Int,success:@escaping (_ response:QureyStatsGatewayListResponse) -> Void,failed:@escaping (_ failed:BaseErrorResponse) -> Void) {
        
        var parameters:[String:String]
        parameters = ["keyword":keyword,
        "domainId":domainId,
        "enterpriseId":enterpriseId,
        "buildingId":buildingId,
        "keypartId":keypartId,
        "orderByColumn":"9",
        "isDescOrder":String.init(isDescOrder),
        "pageIndex":String.init(pageIndex),
        "pageSize":"10"]
            
        sessionManager?.request(url, method: .get, parameters: parameters, encoder: URLEncodedFormParameterEncoder.default, headers: nil, interceptor: nil).responseJSON(completionHandler: { (response) in
            
            let statusCode = response.response?.statusCode
            //初始化错误返回对象实例
            let errResponse = BaseErrorResponse()
                
            switch response.result{
                
                case .success(let res):
                    
                    if statusCode == 200{
                    
                        //初始化数据返回对象实例
                        let josnStr = res as? [String: Any]
                        let respModel = QureyStatsGatewayListResponse.deserialize(from: josnStr)
                        
                        if respModel != nil {
                                if respModel?.code == 200 {
                                    success(respModel!)
                                }else{
                                    let response = BaseErrorResponse.deserialize(from: josnStr)
                                    failed(self.formateErrorResponse(response: response!))
                                }
                            }else{
                                errResponse.errCode = RequestResponseCode.dataParsingException
                                errResponse.errMessage = "数据解析异常"
                                failed(errResponse)
                            }
                    }else{
                        failed(self.formateErrorResponse(response: self.handleErrorResponse(res: res)))
                    }
                break
                
                case .failure(let error):
                    
                    failed(self.formateFailedResponse(faile: error))
                    
                break
            
            }
            
        })
        
        
    }
    
    /// 设备查询 获取事件列表
    /// - Parameters:
    ///   - gatewayId: 设备id
    ///   - success: 成功回调
    ///   - failed: 失败回调
    func getQureyStatsGatewayEventList(gatewayId:String,success:@escaping (_ response:QureyStatsEventResponse) -> Void,failed:@escaping (_ failed:BaseErrorResponse) -> Void){
        
        let parameters = ["id":gatewayId]
        
        self.sessionManager?.request(UrlHost.QureyStats.getQureyStatsGatewayEventList, method: .get, parameters: parameters, encoder: URLEncodedFormParameterEncoder.default, headers: nil, interceptor: nil).responseJSON(completionHandler: { (response) in
            
            let statusCode = response.response?.statusCode
            //初始化错误返回对象实例
            let errResponse = BaseErrorResponse()
                
            switch response.result{
                
                case .success(let res):
                    
                    if statusCode == 200{
                    
                        //初始化数据返回对象实例
                        let josnStr = res as? [String: Any]
                        let respModel = QureyStatsEventResponse.deserialize(from: josnStr)
                        
                        if respModel != nil {
                            if respModel?.code == 200 {
                                success(respModel!)
                            }else{
                                let response = BaseErrorResponse.deserialize(from: josnStr)
                                failed(self.formateErrorResponse(response: response!))
                            }
                        }else{
                            errResponse.errCode = RequestResponseCode.dataParsingException
                            errResponse.errMessage = "数据解析异常"
                            failed(errResponse)
                        }
                    }else{
                        failed(self.formateErrorResponse(response: self.handleErrorResponse(res: res)))
                    }
                break
                
                case .failure(let error):
                    
                    failed(self.formateFailedResponse(faile: error))
                    
                break
            
            }
            
        })
        
        
    }
    /// 设备查询 获取GIS位置
    /// - Parameters:
    ///   - gatewayId: 设备id
    ///   - success: 成功回调
    ///   - failed: 失败回调
    func getQureyStatsGatewayGis(gatewayId:String,success:@escaping (_ response:QureyStatsGISResponse) -> Void,failed:@escaping (_ failed:BaseErrorResponse) -> Void){
        
        let parameters = ["id":gatewayId]
        
        self.sessionManager?.request(UrlHost.QureyStats.getQureyStatsGatewayGis, method: .get, parameters: parameters, encoder: URLEncodedFormParameterEncoder.default, headers: nil, interceptor: nil).responseJSON(completionHandler: { (response) in
            
            let statusCode = response.response?.statusCode
            //初始化错误返回对象实例
            let errResponse = BaseErrorResponse()
                
            switch response.result{
                
                case .success(let res):
                    
                    if statusCode == 200{
                    
                        //初始化数据返回对象实例
                        let josnStr = res as? [String: Any]
                        let respModel = QureyStatsGISResponse.deserialize(from: josnStr)
                        
                        if respModel != nil {
                            if respModel?.code == 200 {
                                success(respModel!)
                            }else{
                                let response = BaseErrorResponse.deserialize(from: josnStr)
                                failed(self.formateErrorResponse(response: response!))
                            }
                        }else{
                            errResponse.errCode = RequestResponseCode.dataParsingException
                            errResponse.errMessage = "数据解析异常"
                            failed(errResponse)
                        }
                    }else{
                        failed(self.formateErrorResponse(response: self.handleErrorResponse(res: res)))
                    }
                break
                
                case .failure(let error):
                    
                    failed(self.formateFailedResponse(faile: error))
                    
                break
            
            }
            
        })
        
        
    }
    /// 设备查询 获取通道列表
    /// - Parameters:
    ///   - gatewayId: 设备id
    ///   - success: 成功回调
    ///   - failed: 失败回调
    func getQureyStatsGatewayChannelList(gatewayId:String,success:@escaping (_ response:QureyStatsChannelResponse) -> Void,failed:@escaping (_ failed:BaseErrorResponse) -> Void){
        
        let parameters = ["id":gatewayId]
        
        self.sessionManager?.request(UrlHost.QureyStats.getQureyStatsGatewayChannelList, method: .get, parameters: parameters, encoder: URLEncodedFormParameterEncoder.default, headers: nil, interceptor: nil).responseJSON(completionHandler: { (response) in
            
            let statusCode = response.response?.statusCode
            //初始化错误返回对象实例
            let errResponse = BaseErrorResponse()
                
            switch response.result{
                
                case .success(let res):
                    
                    if statusCode == 200{
                    
                        //初始化数据返回对象实例
                        let josnStr = res as? [String: Any]
                        let respModel = QureyStatsChannelResponse.deserialize(from: josnStr)
                        
                        if respModel != nil {
                            if respModel?.code == 200 {
                                success(respModel!)
                            }else{
                                let response = BaseErrorResponse.deserialize(from: josnStr)
                                failed(self.formateErrorResponse(response: response!))
                            }
                        }else{
                            errResponse.errCode = RequestResponseCode.dataParsingException
                            errResponse.errMessage = "数据解析异常"
                            failed(errResponse)
                        }
                    }else{
                        failed(self.formateErrorResponse(response: self.handleErrorResponse(res: res)))
                    }
                break
                
                case .failure(let error):
                    
                    failed(self.formateFailedResponse(faile: error))
                    
                break
            
            }
            
        })
        
        
    }
    
    /// 设备查询 获取通道趋势图
    /// - Parameters:
    ///   - channelId: 设备id
    ///   - hour: 时间段
    ///   - success: 成功回调
    ///   - failed: 失败回调
    func getQureyStatsData(channelId:String,hour:String,success:@escaping (_ response:QureyStatsResponse) -> Void,failed:@escaping (_ failed:BaseErrorResponse) -> Void){
        
        let parameters = ["id":channelId,"hour":hour]
        
        self.sessionManager?.request(UrlHost.QureyStats.getQureyStats, method: .get, parameters: parameters, encoder: URLEncodedFormParameterEncoder.default, headers: nil, interceptor: nil).responseJSON(completionHandler: { (response) in
            
            let statusCode = response.response?.statusCode
            //初始化错误返回对象实例
            let errResponse = BaseErrorResponse()
                
            switch response.result{
                
                case .success(let res):
                    
                    if statusCode == 200{
                    
                        //初始化数据返回对象实例
                        let josnStr = res as? [String: Any]
                        let respModel = QureyStatsResponse.deserialize(from: josnStr)
                        
                        if respModel != nil {
                            if respModel?.code == 200 {
                                success(respModel!)
                            }else{
                                let response = BaseErrorResponse.deserialize(from: josnStr)
                                failed(self.formateErrorResponse(response: response!))
                            }
                        }else{
                            errResponse.errCode = RequestResponseCode.dataParsingException
                            errResponse.errMessage = "数据解析异常"
                            failed(errResponse)
                        }
                    }else{
                        failed(self.formateErrorResponse(response: self.handleErrorResponse(res: res)))
                    }
                break
                
                case .failure(let error):
                    
                    failed(self.formateFailedResponse(faile: error))
                    
                break
            
            }
            
        })
        
        
    }
    
    
    
    @objc func tokenTimeOut() {
        
        let controller = self.currentViewController()
        
        if controller != nil {
    
            DataArchiver.clearArchiver(keyArr:[ArchireKey.UserInfor])
            
            let loginVC = LoginViewController.init()
            loginVC.isPopBack = true;
            controller?.navigationController?.pushViewController(loginVC, animated: true)
        }
        
    }
    
    func currentViewController(base: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
        if let nav = base as? UINavigationController {
            return currentViewController(base: nav.visibleViewController)
        }
        if let tab = base as? UITabBarController {
            return currentViewController(base: tab.selectedViewController)
        }
        if let presented = base?.presentedViewController {
            return currentViewController(base: presented)
        }
        return base
    }
    
    
    
}




