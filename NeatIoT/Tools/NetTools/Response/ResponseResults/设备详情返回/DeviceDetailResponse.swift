//
//  DeviceDetailResponse.swift
//  NeatIoT
//
//  Created by neat on 2019/12/2.
//  Copyright © 2019 Neat. All rights reserved.
//

import UIKit
import HandyJSON

class DeviceDetailResponse: BaseResponse {
    
    var deviceInfor:DeviceInforBaseModel?

    override func mapping(mapper: HelpingMapper) {
        mapper <<<
        self.deviceInfor <-- "result"
    }
    

}
