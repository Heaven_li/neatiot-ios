//
//  GasDectSetDetailResponse.swift
//  NeatIoT
//
//  Created by Yang Blus on 2020/8/19.
//  Copyright © 2020 Neat. All rights reserved.
//

import UIKit
import HandyJSON

class GasDetectorSetDetailResponse: BaseResponse {
    
    var detailModel:GasDetectorSetModel?
    
    override func mapping(mapper: HelpingMapper) {
        mapper <<<
        self.detailModel <-- "result"
    }


}

class GasDetectorSetModel:HandyJSON{
    
    /**
    * 联动设备状态
    * 0:关闭
    * 1:打开
    * 2:未连接
    * 3:正在打开
    * 4:正在关闭
    * */
    var linkDeviceStatus:Int?
    ///0～15之间整数
    var volume:Int?
    ///语言1 中文2 英文
    var language:Int?
    ///上报周期 秒
    var reportPeriod:Int?
    
    
    required init() {
        
    }
    
    
}
