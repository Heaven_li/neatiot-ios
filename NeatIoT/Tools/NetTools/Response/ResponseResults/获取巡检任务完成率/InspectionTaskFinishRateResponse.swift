//
//  InspectionTaskFinishRateResponse.swift
//  NeatIoT
//
//  Created by neat on 2019/12/24.
//  Copyright © 2019 Neat. All rights reserved.
//

import UIKit
import HandyJSON
class InspectionTaskFinishRateResponse: BaseResponse {
    
    var resultData:InspectionRateModel?

    override func mapping(mapper: HelpingMapper) {
        mapper <<<
        self.resultData <-- "result"
    }

}

class InspectionRateModel: HandyJSON {
    
    var finishCount:Int?
    var notFinishCount:Int?
    var all:Int?
    
    required init(){
        
    }
}
