//
//  EleSearchResponse.swift
//  NeatIoT
//
//  Created by neat on 2019/11/26.
//  Copyright © 2019 Neat. All rights reserved.
//

import UIKit
import HandyJSON

class EleSearchResponse: BaseResponse {
    
    var device_infor:EleBindModel?

    override func mapping(mapper: HelpingMapper) {
        mapper <<<
        self.device_infor <-- "result"
    }
    
}

class EleBindModel: NSObject,HandyJSON {
    
    /// 设备id
    var device_id:String = ""
    /// 设备编码
    var device_code:String = ""
    /// 设备sim卡号
    var device_sim_code:String = ""
    /// 长传周期
    var device_time_out:String = ""
    /// 更新日期
    var device_update_time:String = ""
    /// 软件版本
    var device_sf_version:String = ""
    /// 是否绑定 false未绑定
    var device_is_bind:Bool = false
    
    required override init() {
        
    }
    
    func mapping(mapper: HelpingMapper) {
        mapper <<<
        self.device_id <-- "id"
        mapper <<<
        self.device_code <-- "code"
        mapper <<<
        self.device_sim_code <-- "sim"
        mapper <<<
        self.device_time_out <-- "timeout"
        mapper <<<
        self.device_update_time <-- "updateTime"
        mapper <<<
        self.device_sf_version <-- "version"
        mapper <<<
        self.device_is_bind <-- "isBind"
    }
}
