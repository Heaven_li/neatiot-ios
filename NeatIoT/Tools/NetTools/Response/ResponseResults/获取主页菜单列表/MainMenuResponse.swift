//
//  MainMenuResponse.swift
//  NeatIoT
//
//  Created by neat on 2019/10/24.
//  Copyright © 2019 Neat. All rights reserved.
//


import HandyJSON

class MainMenuResponse: BaseResponse {

    var response_result:Array<MenuModel> = []
    
    override func mapping(mapper: HelpingMapper) {
        mapper <<<
        self.response_result <-- "result"
    }
    
}

/// 功能item model
class MenuModel: HandyJSON {
    ///功能名称
    var name:String = ""
    ///图片名称
    var style_id:String = ""
    ///功能描述
    var description:String = ""
    ///设备型号
    var model:String?
    ///排序
    var sequence:Int = 0
    /// 通知数量
    var notifyNum:Int = 0
    /// 子功能数组
    var children:Array<MenuModel> = []
    
    required init() {
        
    }
    
    func mapping(mapper: HelpingMapper) {
        mapper <<<
        self.style_id <-- "styleId"
    }
    
}
