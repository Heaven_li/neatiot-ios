//
//  InspectionAlarmHandleDetailResponse.swift
//  NeatIoT
//
//  Created by Yang Blus on 2020/2/10.
//  Copyright © 2020 Neat. All rights reserved.
//

import UIKit
import HandyJSON

class InspectionAlarmHandleDetailResponse: BaseResponse {
    
    var alarmModel:InspectionAlarmHandleDetailResult?
    
    override func mapping(mapper: HelpingMapper) {
        mapper <<<
        self.alarmModel <-- "result"
    }
}

class InspectionAlarmHandleDetailResult: HandyJSON {
    
    
    /// 设备名称
    var deviceName:String?
    
    /// 设备状态
    var alarmCategoryStr:String?
    
    /// 设备图片
    var pictureUrl:String?
    
    /// 处理信息
    var eventHandler:InspectionAlarmHandelEventHandler?
    
    required init(){
   
    }

    
}

class InspectionAlarmHandelEventHandler:HandyJSON{
    
    /// 处理人
    var handleUName:String?
    /// 处理结果
    var handleResult:Int?
    /// 处理说明
    var handleContent:String?
    /// 上传附件数组
    var uploadInfo:Array<InspectionAlarmUploadInfor>?
    
    required init(){
        
    }
    
}

class InspectionAlarmUploadInfor:HandyJSON{
    
    ///文件类型 1图片（png）2语音（mp3）3视频（mp4）
    var fileDataType:Int?
    /// 缩略图
    var thumbnails:String?
    /// 文件地址
    var fileUrl:String?
    /// 语音文件时长
    var duration:String?
    
    required init(){
        
    }
}
