//
//  LoginResponse.swift
//  NeatIoT
//
//  Created by neat on 2019/10/12.
//  Copyright © 2019 Neat. All rights reserved.
//

import HandyJSON


class LoginResponse: BaseResponse {
    
    var response_data: LoginResultModel?
    
    override func mapping(mapper: HelpingMapper) {
        mapper <<<
        self.response_data <-- "result"
    }
    
}

class LoginResultModel:NSObject,HandyJSON,NSSecureCoding {
    
    static var supportsSecureCoding: Bool = true
    
    
    required override init() {
        
    }

    func encode(with coder: NSCoder) {

        coder.encode(user_id, forKey: "user_id")
        coder.encode(user_name, forKey: "user_name")
        coder.encode(user_token, forKey: "user_token")
        coder.encode(user_domain_id, forKey: "user_domain_id")
        coder.encode(user_enterprise_id, forKey: "user_enterprise_id")
        coder.encode(user_domain_name, forKey: "user_domain_name")
        coder.encode(user_enterprise_name, forKey: "user_enterprise_name")
        coder.encode(user_childDomain_name, forKey: "user_childDomain_name")
        coder.encode(user_roles, forKey: "user_roles")
        coder.encode(user_login, forKey: "user_login")
        
    }

    required init?(coder: NSCoder) {
        user_id = (coder.decodeObject(forKey: "user_id") as! String)
        user_name = (coder.decodeObject(forKey: "user_name") as! String)
        user_token = (coder.decodeObject(forKey: "user_token") as! String)
        user_domain_id = (coder.decodeObject(forKey: "user_domain_id") as! String)
        user_enterprise_id = (coder.decodeObject(forKey: "user_enterprise_id") as! String)
        user_domain_name = (coder.decodeObject(forKey: "user_domain_name") as! String)
        user_enterprise_name = (coder.decodeObject(forKey: "user_enterprise_name") as! String)
        user_childDomain_name = (coder.decodeObject(forKey: "user_childDomain_name") as! String)
        user_roles = (coder.decodeObject(forKey: "user_roles")) as? Array<String> ?? []
        user_login = coder.decodeBool(forKey: "user_login") 
    }

    ///用户id
    var user_id: String = ""
    /// 用户名
    var user_name: String = ""
    /// 验证token
    var user_token: String = ""
    /// 所属中心id
    var user_domain_id: String = ""
    /// 所属企业id
    var user_enterprise_id: String = ""
    /// 所属中心名称
    var user_domain_name: String = ""
    /// 所属企业名称
    var user_enterprise_name: String = ""
    /// 子中心名称
    var user_childDomain_name: String = ""
    /// 角色
    var user_roles: Array<String> = []
    
    /// 登录状态 默认未登录
    var user_login = false
    
    func mapping(mapper: HelpingMapper) {
        mapper <<<
        self.user_id <-- "userId"
        mapper <<<
        self.user_name <-- "name"
        mapper <<<
        self.user_token <-- "token"
        mapper <<<
        self.user_domain_id <-- "domainId"
        mapper <<<
        self.user_enterprise_id <-- "enterpriseId"
        mapper <<<
        self.user_domain_name <-- "domainName"
        mapper <<<
        self.user_enterprise_name <-- "entName"
        mapper <<<
        self.user_childDomain_name <-- "childDomainName"
        mapper <<<
        self.user_roles <-- "roles"
    }

}


