//
//  AlarmLogResponse.swift
//  NeatIoT
//
//  Created by Yang Blus on 2020/2/6.
//  Copyright © 2020 Neat. All rights reserved.
//

import UIKit
import HandyJSON
class AlarmLogResponse: BaseResponse {
    
    var resultModel:AlarmResultModel?

    override func mapping(mapper: HelpingMapper) {
        mapper <<<
        self.resultModel <-- "result"
    }

}

class AlarmResultModel: HandyJSON {
    
    /// 已处理
    var handled:Int?
    /// 未处理
    var notHandled:Int?
    /// 事件数组
    var itemArr:Array<AlarmItemModel>?
    
    required init(){
        
    }
    
    func mapping(mapper: HelpingMapper) {
        mapper <<<
        self.itemArr <-- "data"
    }
    
}
class AlarmItemModel: HandyJSON {
    
    /// 事件ID
    var id:String?
    /// 设备名称
    var deviceName:String?
    /// 系统类别
    var systemCategory:String?
    /// 设备类别
    var deviceIdType:Int?
    /// 事件类型
    var eventCategory:String?
    /// 事件类型文字
    var sourceCategory:String?
    /// 发生事件
    var occurTime:String?
    /// 设备ID
    var deviceId:String?
    /// 是否处理
    var isHandle:Bool?
    /// 处理时间
    var handleTime:String?
    /// 多少天前
    var daysAgo:Int?
    /// 多少小时前
    var hoursAgo:Int?
    /// 多少分钟前
    var minutesAgo:Int?
    
    
    required init(){
        
    }
    
}

