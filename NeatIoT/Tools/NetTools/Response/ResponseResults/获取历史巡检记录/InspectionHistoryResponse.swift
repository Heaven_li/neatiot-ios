//
//  InspectionHistoryResponse.swift
//  NeatIoT
//
//  Created by neat on 2019/12/18.
//  Copyright © 2019 Neat. All rights reserved.
//

import UIKit
import HandyJSON
class InspectionHistoryResponse: BaseResponse {
    
    
    var resultData:InspectionHistoryResultModel?
    
    
    override func mapping(mapper: HelpingMapper) {
        mapper <<<
        self.resultData <-- "result"
    }

}
