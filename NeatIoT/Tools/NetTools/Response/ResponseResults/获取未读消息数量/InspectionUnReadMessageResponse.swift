//
//  InspectionUnReadMessageResponse.swift
//  NeatIoT
//
//  Created by neat on 2019/12/24.
//  Copyright © 2019 Neat. All rights reserved.
//

import UIKit
import HandyJSON
class InspectionUnReadMessageResponse: BaseResponse {
    
    var unReadMessageCount:Int = 0

    override func mapping(mapper: HelpingMapper) {
        mapper <<<
        self.unReadMessageCount <-- "result"
    }

}
