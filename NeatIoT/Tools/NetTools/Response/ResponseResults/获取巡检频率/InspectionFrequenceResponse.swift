//
//  InspectionFrequenceResponse.swift
//  NeatIoT
//
//  Created by neat on 2019/12/18.
//  Copyright © 2019 Neat. All rights reserved.
//

import UIKit
import HandyJSON

/// 获取巡检频率返回数据
class InspectionFrequenceResponse: BaseResponse {
    
    var frequenceArr:Array<FrequenceModel>?

    override func mapping(mapper: HelpingMapper) {
        mapper <<<
        self.frequenceArr <-- "result"
    }

}
