//
//  VideoPlayInforResponse.swift
//  NeatIoT
//
//  Created by Yang Blus on 2020/2/12.
//  Copyright © 2020 Neat. All rights reserved.
//

import UIKit
import HandyJSON

class VideoPlayInforResponse: BaseResponse {
    
    var videoModel:VideoInforModel?
    
    override func mapping(mapper: HelpingMapper) {
        mapper <<<
        self.videoModel <-- "result"
    }

}

class VideoInforModel: HandyJSON {
    
    
    /// key
    var appKey:String?
    
    /// token
    var accessToken:String?
    
    /// 播放地址
    var playURL:String?
    
    required init(){
   
    }

    
}
