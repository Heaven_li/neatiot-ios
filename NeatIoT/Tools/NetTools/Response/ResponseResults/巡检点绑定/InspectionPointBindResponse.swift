//
//  InspectionPointBindResponse.swift
//  NeatIoT
//
//  Created by neat on 2019/12/25.
//  Copyright © 2019 Neat. All rights reserved.
//

import UIKit
import HandyJSON
class InspectionPointBindResponse: BaseResponse {
    
    var bindResult:InspectionPointBindResult?

    override func mapping(mapper: HelpingMapper) {
        mapper <<<
        self.bindResult <-- "result"
    }

}

class InspectionPointBindResult: HandyJSON {
    
    /// 之前绑定巡检点名称数组
    var oldBindPoints:Array<String>?
    
    required init(){
        
    }
    
}
