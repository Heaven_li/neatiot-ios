//
//  InspectionTaskListResponse.swift
//  NeatIoT
//
//  Created by neat on 2019/12/18.
//  Copyright © 2019 Neat. All rights reserved.
//

import UIKit
import HandyJSON
/// 巡检任务列表返回数据
class InspectionTaskListResponse: BaseResponse {
    
    var dataResult:TaskListModel?
    
    override func mapping(mapper: HelpingMapper) {
        mapper <<<
        self.dataResult <-- "result"
    }

}
