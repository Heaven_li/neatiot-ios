//
//  InspectionLogDeviceStatusResponse.swift
//  NeatIoT
//
//  Created by Yang Blus on 2020/2/10.
//  Copyright © 2020 Neat. All rights reserved.
//

import UIKit
import HandyJSON
class InspectionLogDeviceStatusResponse: BaseResponse {
    
    var deviceStatusModel:InspectionLogDeviceStatusModel?
    
    override func mapping(mapper: HelpingMapper) {
        mapper <<<
        self.deviceStatusModel <-- "result"
    }
    
    

}

class InspectionLogDeviceStatusModel: HandyJSON {
    
    /// 设备ID
    var id:String?
    /// 设备名称
    var name:String?
    /// 设备编码
    var deviceCode:String?
    /// 设备所属系统类型
    var systemCategory:Int?
    /// 主机数
    var hostCount:Int?
    /// 设备类型
    var deviceType:Int?
    /// 设备图片URL
    var pictureUrl:String?
    /// 设备在线状态
    var onlineStatus:Int?
    /// 设备状态
    var alarmStatus:Int?
    /// 信号强度
    var signalStatus:Int?
    /// 电池电量
    var batteryStatus:String?
    /// 当前值
    var curretnValue:Double?
    /// 心跳时间
    var heartTime:String?
    /// 设备展示数据项数组
    var dataItems:Array<EleItemDataModel>? = []
    
    required init(){
        
    }
    
}


