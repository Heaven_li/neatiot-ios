//
//  DeviceHostListResponse.swift
//  NeatIoT
//
//  Created by neat on 2019/10/30.
//  Copyright © 2019 Neat. All rights reserved.
//

import HandyJSON

class DeviceHostListResponse: BaseResponse {
    
    var response_result:DeviceHostListResult?
    
    override func mapping(mapper: HelpingMapper) {
        mapper <<<
        self.response_result <-- "result"
    }
    
}

class DeviceHostListResult: HandyJSON {
    
    /// 设备数量
    var deviceCount = 0
    /// 设备数组
    var deviceArr:Array<DeviceItemModel> = []
    
    required init(){
        
    }
    
    func mapping(mapper: HelpingMapper) {
        
        mapper <<<
        self.deviceCount <-- "totalCount"
        mapper <<<
        self.deviceArr <-- "data"
        
    }
    
}

class DeviceItemModel: HandyJSON {
    
    ///设备id
    var device_id:String = ""
    ///设备名称
    var device_name:String = ""
    ///设备编码
    var device_code:String = ""
    ///设备状态 1：离线 2：在线
    var device_status:Int = -1
    ///设备安装位置
    var device_address:String = ""
    /// 设备类型
    var device_category:String = ""
    /// 设备所属企业id
    var device_enterprise_id:String = ""
    /// 智慧用电sim号
    var device_sim = ""
    
    required init() {
        
    }
    
    func mapping(mapper: HelpingMapper) {
        mapper <<<
        self.device_id <-- "id"
        mapper <<<
        self.device_name <-- "name"
        mapper <<<
        self.device_code <-- "code"
        mapper <<<
        self.device_status <-- "status"
        mapper <<<
        self.device_address <-- "address"
        mapper <<<
        self.device_enterprise_id <-- "enterpriseId"
        mapper <<<
        self.device_sim <-- "sim"
        
    }
}
