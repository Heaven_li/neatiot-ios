//
//  SmartGatewayTaskInforResponse.swift
//  NeatIoT
//
//  Created by neat on 2019/12/6.
//  Copyright © 2019 Neat. All rights reserved.
//

import UIKit
import HandyJSON

class SmartGatewayTaskInforResponse: BaseResponse {
    
    var inforArr:Array<SmartGatewayTaskInforModel>?

    override func mapping(mapper: HelpingMapper) {
        mapper <<<
        self.inforArr <-- "result"
    }
}
