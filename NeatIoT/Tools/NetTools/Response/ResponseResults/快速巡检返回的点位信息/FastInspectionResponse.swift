//
//  FastInspectionResponse.swift
//  NeatIoT
//
//  Created by Yang Blus on 2020/2/5.
//  Copyright © 2020 Neat. All rights reserved.
//

import UIKit
import HandyJSON

class FastInspectionResponse: BaseResponse {
    
    var pointModel:InspectionTaskPointModel?

    override func mapping(mapper: HelpingMapper) {
        mapper <<<
        self.pointModel <-- "result"
    }

}
