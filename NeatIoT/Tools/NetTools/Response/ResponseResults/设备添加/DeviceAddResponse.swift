//
//  DeviceAddResponse.swift
//  NeatIoT
//
//  Created by neat on 2019/12/12.
//  Copyright © 2019 Neat. All rights reserved.
//

import UIKit
import HandyJSON

class DeviceAddResponse: BaseResponse {
    
    var deviceId:String?
    
    override func mapping(mapper: HelpingMapper) {
        mapper <<<
        self.deviceId <-- "result"
    }
}
