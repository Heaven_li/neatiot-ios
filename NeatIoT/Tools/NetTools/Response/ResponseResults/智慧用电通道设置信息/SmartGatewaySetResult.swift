//
//  SmartGatewaySetResult.swift
//  NeatIoT
//
//  Created by neat on 2019/12/10.
//  Copyright © 2019 Neat. All rights reserved.
//

import UIKit
import HandyJSON
/// 智慧用电网关设置返回result
class SmartGatewaySetResult: NSObject,HandyJSON {

    /// 网关id
    var gatewayId:String?
    /// 网关编码
    var gateWayCode:String?
    /// 网关名称
    var gatewayName:String?
    /// 网关上传频率
    var timeOut:Int?
    /// 网关通道数
    var totalCount:Int?
    /// 通道信息数组
    var componets:Array<ChanleSetModel>?
    
    required override init() {
        
    }
    
    
}
