//
//  SmartGatewayChanleSetResponse.swift
//  NeatIoT
//
//  Created by neat on 2019/12/10.
//  Copyright © 2019 Neat. All rights reserved.
//

import UIKit
import HandyJSON

class SmartGatewayChanleSetResponse: BaseResponse {
    
    
    /// result字段
    var gatewayInfor:SmartGatewaySetResult?
    
    
    override func mapping(mapper: HelpingMapper) {
        mapper <<<
        self.gatewayInfor <-- "result"
    }

}
