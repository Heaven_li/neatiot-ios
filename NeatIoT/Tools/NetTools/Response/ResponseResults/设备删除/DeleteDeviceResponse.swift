//
//  DeleteDeviceResponse.swift
//  NeatIoT
//
//  Created by neat on 2019/12/12.
//  Copyright © 2019 Neat. All rights reserved.
//

import UIKit
import HandyJSON
class DeleteDeviceResponse: BaseResponse {
    
    var messageArr:Array<String>?
    
    override func mapping(mapper: HelpingMapper) {
        mapper <<<
        self.messageArr <-- "result"
    }
}
