//
//  InspectionBindPointResponse.swift
//  NeatIoT
//
//  Created by neat on 2019/12/20.
//  Copyright © 2019 Neat. All rights reserved.
//

import UIKit
import HandyJSON
class InspectionBindPointResponse: BaseResponse {

    
    var resultData:InspectionBindResultModel?
    
    
    override func mapping(mapper: HelpingMapper) {
        mapper <<<
        self.resultData <-- "result"
    }
    
}
