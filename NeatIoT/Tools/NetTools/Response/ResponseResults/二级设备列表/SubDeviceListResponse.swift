//
//  SubDeviceListResponse.swift
//  NeatIoT
//
//  Created by neat on 2019/11/27.
//  Copyright © 2019 Neat. All rights reserved.
//

import UIKit
import HandyJSON

class SubDeviceListResponse: BaseResponse {
    
    
    /// 设备数组
    var deviceList:Array<SubDeviceItemModel> = []
    
    override func mapping(mapper: HelpingMapper) {
        mapper <<<
        self.deviceList <-- "result"
    }
    
    class SubDeviceItemModel: NSObject,HandyJSON {
        
        /// 设备id
        var id:String = ""
        /// 设备编码
        var code:String = ""
        /// 设备名称
        var name:String = ""
        /// 8140设备器件 状态
        var status:Int = -1
        
        ///有人泵房检测网关 在线状态 1离线 2在线
        var onlineStatus:Int = -1
        
        /// 运行状态 1运行 2停止
        var runStatus:Int = -1
        
        /// 手自动状态 1自动 2 手动
        var autoStatus:Int = -1
        
        /// 设备类型 1送风扇 2消防水泵
        var deviceType:Int = -1
        
        /// 设备类型文字
        var deviceTypeDesc:String = ""
        
        /// 电源状态文字
        var powerStatusDesc:String = ""
        
        /// 运行状态文字
        var runStatusDesc:String = ""
        
        /// 设备状态文字
        var alarmStatusDesc:String = ""
        
        /// 手自动状态文字
        var autoStatusDesc:String = ""
        
        
        
        required override init() {
            
        }
        
    }

}
