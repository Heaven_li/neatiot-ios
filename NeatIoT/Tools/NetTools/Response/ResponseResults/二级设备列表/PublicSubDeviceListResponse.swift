//
//  PubicSubDeviceListResponse.swift
//  NeatIoT
//
//  Created by Yang Blus on 2020/10/27.
//  Copyright © 2020 Neat. All rights reserved.
//

import UIKit
import HandyJSON


class PublicSubDeviceListResponse: BaseResponse {
    
    /// 设备数组
    var responseData:PublicSubDeviceResultModel = PublicSubDeviceResultModel.init()
    
    override func mapping(mapper: HelpingMapper) {
        mapper <<<
        self.responseData <-- "result"
    }
    
    
    class PublicSubDeviceResultModel: NSObject,HandyJSON{
        
        /// 数据总条数
        var totalCount:Int = 0
        /// 附属器件列表
        var data:Array<SubDeviceListResponse.SubDeviceItemModel> = []
        
        required override init() {
            
        }
        
        
    }
    
    
  

}
