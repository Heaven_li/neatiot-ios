//
//  InspectionSubProjectResponse.swift
//  NeatIoT
//
//  Created by Yang Blus on 2020/2/7.
//  Copyright © 2020 Neat. All rights reserved.
//

import UIKit
import HandyJSON
class InspectionSubProjectResponse: BaseResponse {
    
    var resultData:InspectionSubProjectResponseResult?
    
    override func mapping(mapper: HelpingMapper) {
        mapper <<<
        self.resultData <-- "result"
    }
}

class InspectionSubProjectResponseResult: HandyJSON {
    
    var totalCount:Int?
    
    var projectList:Array<InspectionSubProjectModel>?
    
    required init(){
        
    }
    
    func mapping(mapper: HelpingMapper) {
        mapper <<<
        self.projectList <-- "data"
    }
}

class InspectionSubProjectModel: HandyJSON {
    
    var id:String?
    
    var typeName:String?
    
    required init(){
        
    }
}
