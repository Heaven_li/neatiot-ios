//
//  InspectionTroublePointResponse.swift
//  NeatIoT
//
//  Created by neat on 2019/12/19.
//  Copyright © 2019 Neat. All rights reserved.
//

import UIKit
import HandyJSON
class InspectionTroublePointResponse: BaseResponse {
    
    var resultData:TroublePointResultModel?
    
    
    override func mapping(mapper: HelpingMapper) {
        mapper <<<
        self.resultData <-- "result"
    }

}
