//
//  SmartGatewayVersionResponse.swift
//  NeatIoT
//
//  Created by neat on 2019/12/9.
//  Copyright © 2019 Neat. All rights reserved.
//

import UIKit
import HandyJSON

class SmartGatewayVersionResponse: BaseResponse {
    
    var version_arr:Array<UpdateFileInforItem>?
    
    override func mapping(mapper: HelpingMapper) {
        mapper <<<
        self.version_arr <-- "result"
    }

}
