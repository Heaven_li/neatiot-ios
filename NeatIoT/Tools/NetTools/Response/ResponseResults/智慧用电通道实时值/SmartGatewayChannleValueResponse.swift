//
//  SmartGatewayChannleValueResponse.swift
//  NeatIoT
//
//  Created by neat on 2019/12/10.
//  Copyright © 2019 Neat. All rights reserved.
//

import UIKit
import HandyJSON
class SmartGatewayChannleValueResponse: BaseResponse {
    
    
    var dataArr:Array<ChannelValueModel>?
    
    override func mapping(mapper: HelpingMapper) {
        mapper <<<
        self.dataArr <-- "result"
    }

}
