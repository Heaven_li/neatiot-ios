//
//  ProtectionHomeStatistics.swift
//  NeatIoT
//
//  Created by Yang Blus on 2020/5/8.
//  Copyright © 2020 Neat. All rights reserved.
//

import UIKit
import HandyJSON

class ProtectionHomeStatistics: BaseResponse {
    
    var result:StatisticsResult?

}

class StatisticsResult: HandyJSON {
    
    /// 维保任务完成率
    var taskCompleteRate:Float? = 0
    
    /// 维保任务已经完成的数量
    var taskCompleteCount:Int? = 0
    
    /// 维保任务待完成的数量
    var taskNotCompleteCount:Int? = 0
    
    /// 没处理工单数量
    var notHandleOrderCount:Int? = 0
    
    /// 已经处理工单数量
    var handleOrderCount:Int? = 0
    
    required init(){}
    
    
    
}
