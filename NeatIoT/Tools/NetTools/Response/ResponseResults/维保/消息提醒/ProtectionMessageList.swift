//
//  ProtectionMessageList.swift
//  NeatIoT
//
//  Created by Yang Blus on 2020/5/8.
//  Copyright © 2020 Neat. All rights reserved.
//

import UIKit
import HandyJSON

class ProtectionMessageList: BaseResponse {
    
    var result:ProtectionMessageResult?

}

class ProtectionMessageResult: HandyJSON {
    
    required init(){}
    
    var totalCount:Int? = 0
    
    var messageArr:Array<ProtectionMessage>? = []
    
    func mapping(mapper: HelpingMapper) {
        mapper <<<
        self.messageArr <-- "data"
    }
    
    
}

class ProtectionMessage: HandyJSON {
    
    required init(){}
    /// 数据源ID
    var sourceId:String? = ""
    /// 编号
    var number:String? = ""
    /// 天数
    var days:String? = ""
    /// 发送时间
    var sendTime:String? = ""
    /// 业务数据类型（mta_contract：合同提醒 mta_plan：维保计划）
    var dataTable:String? = ""
    /// 状态（2：快过期，3：已超期）
    var flag:Int? = 2
    
    
}
