//
//  ProtectionRecHistory.swift
//  NeatIoT
//
//  Created by Yang Blus on 2020/5/8.
//  Copyright © 2020 Neat. All rights reserved.
//

import UIKit
import HandyJSON

class ProtectionRecHistory: BaseResponse {
    
    var result:ProtectionRecResult?

}

class ProtectionRecResult: HandyJSON {
    
    required init(){}
    
    var total:Int? = 0
    
    var recArr:Array<ProtectionRec>? = []
    
    func mapping(mapper: HelpingMapper) {
        mapper <<<
        self.recArr <-- "data"
    }
}

class ProtectionRec: HandyJSON {
    
    required init(){}
    
    var recId:String? = ""
    
    var handlerUserName:String? = ""
    
    var handlerOrderTime:String? = ""
    
    var handlerText:String? = ""
    
    func mapping(mapper: HelpingMapper) {
        mapper <<<
        self.recId <-- "id"
    }
    
    
    
}


