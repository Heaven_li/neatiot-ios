//
//  ProtectionOrderResponse.swift
//  NeatIoT
//
//  Created by Yang Blus on 2020/5/18.
//  Copyright © 2020 Neat. All rights reserved.
//

import UIKit
import HandyJSON
class ProtectionOrderResponse: BaseResponse {
    
    var result:ProtectionOrderResponseResult?

}

class ProtectionOrderResponseResult: HandyJSON {
    
    var historyCount:Int?
    
    var orderDetail:ProtectionOrderDetail?
    
    var uploadFiles:Array<UploadFile>?
    
    var materials:Array<MaterialsInfor>?
    
    
    required init() {
        
    }
    
    
}
class ProtectionOrderDetail: HandyJSON {
    
    /// 工时
    var workHours:String?
    
    var enterTime:String?
    
    var orderDes:String?
    
    var initiatorName:String?
    
    var entName:String?
    
    var handleText:String?
    
    var tel:String?
    
    
    
    required init() {
        
    }
    
}
