//
//  ProtectionOrderHistoryRecResponse.swift
//  NeatIoT
//
//  Created by Yang Blus on 2020/5/19.
//  Copyright © 2020 Neat. All rights reserved.
//

import UIKit
import HandyJSON
class ProtectionOrderHistoryRecResponse: BaseResponse {
    
    var result:ProtectionOrderHistoryRecResult?
    

}

class ProtectionOrderHistoryRecResult: HandyJSON {
    
    var totalCount:Int? = 0
    var data:Array<ProtectionOrderHistoryItem>?
    
    required init() {}
    
}

class ProtectionOrderHistoryItem: HandyJSON {
    
    var id:String?
    var handlerUserName:String?
    var handlerOrderTime:String?
    var handlerText:String?
    
    required init() {
        
    }
    
}
