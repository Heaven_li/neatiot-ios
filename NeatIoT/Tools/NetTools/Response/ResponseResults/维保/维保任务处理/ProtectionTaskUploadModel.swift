//
//  ProtectionTaskUploadModel.swift
//  NeatIoT
//
//  Created by Yang Blus on 2020/5/18.
//  Copyright © 2020 Neat. All rights reserved.
//

import UIKit

class ProtectionTaskUploadModel: NSObject {
    
    ///任务ID
    var taskId:String?
    ///企业ID
    var entId:String?
    ///处理人ID
    var maintenancePersonnelId:String?
    ///入场时间
    var entranceTime:String? = ""
    ///处理内容
    var maintenanceContent:String?
    ///工时
    var workHours:String?
    ///入场时位置信息文本
    var MaintenanceBelocationText:String? = ""
    ///位置信息文本
    var MaintenanceLocationText:String? = ""
    ///纬度
    var Longitude:String? = ""
    ///经度
    var Latitue:String? = ""
    ///视频数组
    var videoArr:Array<AnnexModel> = []
    ///图片数组
    var imageArr:Array<AnnexModel> = []
    ///耗材数组
    var materialsArr:Array<[String:String]> = []

}
