//
//  ProtectionOrderList.swift
//  NeatIoT
//
//  Created by Yang Blus on 2020/5/8.
//  Copyright © 2020 Neat. All rights reserved.
//

import UIKit
import HandyJSON

class ProtectionOrderList: BaseResponse {
    
    var result:ProtectionOrderResult?

}

class ProtectionOrderResult: HandyJSON{
    
    required init(){}
    
    var totalCount:Int? = 0
    
    var orderArr:Array<ProtectionOrder>? = []
    
    func mapping(mapper: HelpingMapper) {
        mapper <<<
        self.orderArr <-- "data"
    }
}

class ProtectionOrder: HandyJSON {
    
    required init(){}
    /// 受理id，处理时上传用
    var acceptId:String? = ""
    /// 工单主键
    var orderId:String? = ""
    /// 单位名称
    var entName:String? = ""
    /// 受理工单截止日期
    var acceptOrderUptoTime:String? = ""
    /// 发起人
    var initiatorName:String? = ""
    /// 联系方式
    var tel:String? = ""
    
    
    ///工单状态（[Description("待受理")]
    ///ToAccept = 1,
    ///[Description("待处理")]
    ///ToHandle = 2,
    ///[Description("待确认")]
    ///ToConfirm = 3,
    ///[Description("已完成")]
    ///Completed = 4,
    var orderStatusType:Int? = 0
    /// 当前服务器时间
    var serverTime:String? = ""
    /// 工单实际完成时间
    var realUptoTime:String? = ""
    /// 工单描述
    var orderDes:String? = ""
    /// 处理历史数
    var HistoryCount:Int? = 0
    /// 工单编号
    var orderNo:String? = ""
}
