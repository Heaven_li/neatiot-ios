//
//  HandleEnterance.swift
//  NeatIoT
//
//  Created by Yang Blus on 2020/5/13.
//  Copyright © 2020 Neat. All rights reserved.
//

import UIKit
import HandyJSON

class HandleEnterance: BaseResponse {

    
    var result:HandleEnteranceResult?
    
}

class HandleEnteranceResult: HandyJSON {
    
    var enterTime:String?
    
    required init(){}
    
    
    
}
