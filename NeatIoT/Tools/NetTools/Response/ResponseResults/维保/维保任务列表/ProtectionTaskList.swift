//
//  ProtectionTaskList.swift
//  NeatIoT
//
//  Created by Yang Blus on 2020/5/8.
//  Copyright © 2020 Neat. All rights reserved.
//

import UIKit
import HandyJSON

class ProtectionTaskList: BaseResponse {
    
    var result:ProtectionResult?

}


class ProtectionResult: HandyJSON {
    
    var totalCount:Int? = 0
    
    var taskList:Array<ProtectionTask>? = []
    
    required init(){}
    
    func mapping(mapper: HelpingMapper) {
        mapper <<<
        self.taskList <-- "data"
    }
    
}

class ProtectionTask: HandyJSON {
    
    /// 任务ID
    var taskId:String?
    /// 计划名称
    var planName:String?
    /// 企业名称
    var entName:String?
    /// 频率
    var frequency:String?
    /// 时间区间
    var intervalTime:String?
    /// 执行时间提醒
    var flagTime:String?
    /// 维保单位ID
    var maintenanceEntId:String?
    /// 维保单位名称
    var maintenanceEntName:String?
    
    required init(){}
    
    func mapping(mapper: HelpingMapper) {
        mapper <<<
        self.taskId <-- "id"
    }
    
}
