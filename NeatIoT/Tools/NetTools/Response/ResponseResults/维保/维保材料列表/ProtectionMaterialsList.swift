//
//  ProtectionMaterialsList.swift
//  NeatIoT
//
//  Created by Yang Blus on 2020/5/8.
//  Copyright © 2020 Neat. All rights reserved.
//

import UIKit
import HandyJSON

class ProtectionMaterialsList: BaseResponse {
    
    var result:ProtectionMaterialsResult?

}

class ProtectionMaterialsResult: HandyJSON {
    
    required init(){}
    
    var totalCount:Int? = 0
    
    var materialArr:Array<ProtectionMaterials>? = []
    
    func mapping(mapper: HelpingMapper) {
        mapper <<<
        self.materialArr <-- "data"
    }
    
}

class ProtectionMaterials: HandyJSON {
    
    required init(){}
    /// 材料id
    var materialsId:String? = ""
    /// 材料名称
    var materialsName:String? = ""
    /// 材料价格
    var materialsPrice:Float? = 0
    /// 材料单位
    var materialsUnit:String? = ""
    /// 材料类别（1，主材 2，辅材）
    var materialsType:Int? = 1
    /// 数量
    var materialsCount:Int = 0
    /// 实际支付金额
    var tureTotalMoney:Double = 0.0
    /// 添加时间
    var addScTime:Date?
    
    func mapping(mapper: HelpingMapper) {
        mapper <<<
        self.materialsId <-- "id"
    }
    
}
