//
//  ProtectionTaskDetailList.swift
//  NeatIoT
//
//  Created by Yang Blus on 2020/5/8.
//  Copyright © 2020 Neat. All rights reserved.
//

import UIKit
import HandyJSON

class ProtectionTaskDetailList: BaseResponse {
    
    var result:ProtectionTaskDetailResult?

}

class ProtectionTaskDetailResult: HandyJSON {
    
    required init(){}
    
    var totalCount:Int? = 0
    
    var taskArr:Array<ProtectionTaskDetail>? = []
    
    func mapping(mapper: HelpingMapper) {
        mapper <<<
        self.taskArr <-- "data"
    }
    
}

class ProtectionTaskDetail: HandyJSON {
    
    required init(){}
    
    /// 维保任务id
    var taskId:String? = ""
    /// 任务完成状态（1已完成2未完成）
    var taskStatus:Int? = 1
    /// 完成任务人名称
    var maintenancePersonName:String? = ""
    /// 超时完成状态（1已超时 异常 2未超时 正常）
    var overTimeStatus:Int? = 2
    /// 位置信息
    var address:String? = ""
    /// 完成或是超时完成时间
    var flagTime:String? = ""
    
    func mapping(mapper: HelpingMapper) {
        mapper <<<
        self.taskId <-- "id"
    }
    
    
}
