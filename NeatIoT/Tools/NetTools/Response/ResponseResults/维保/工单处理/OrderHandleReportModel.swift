//
//  OrderHandleReportModel.swift
//  NeatIoT
//
//  Created by Yang Blus on 2020/5/18.
//  Copyright © 2020 Neat. All rights reserved.
//

import UIKit

class ProtectionHandleReportModel: NSObject {
    
    ///工单受理ID
    var acceptOrderId:String?
    ///入场时间
    var entranceTime:String? = ""
    ///处理内容
    var handleText:String?
    ///工时
    var workHours:String?
    ///入场时位置信息文本
    var MaintenanceBelocationText:String? = ""
    ///位置信息文本
    var MaintenanceLocationText:String? = ""
    ///纬度
    var Longitude:String? = ""
    ///经度
    var Latitue:String? = ""
    ///视频数组
    var videoArr:Array<AnnexModel> = []
    ///音频数组
    var audioArr:Array<AnnexModel> = []
    ///图片数组
    var imageArr:Array<AnnexModel> = []
    ///耗材数组
    var materialsArr:Array<[String:String]> = []
    
    

}


//class ProtectionMaterialsModel: NSObject {
//    //耗材ID
//    var id:String?
//    //耗材数量
//    var dosage:Float?
//    //耗材实际支付总价
//    var amountPaid:Float?
//}
