//
//  ProtectionPlanLIst.swift
//  NeatIoT
//
//  Created by Yang Blus on 2020/5/8.
//  Copyright © 2020 Neat. All rights reserved.
//

import UIKit
import HandyJSON


class ProtectionPlanList: BaseResponse {
    
    var result:ProtectionPlanResult?

}

class ProtectionPlanResult: HandyJSON {
    
    var totalCount:Int? = 0
    
    var planArray:Array<ProtectionPlan>? = []
    
    required init(){}
    
    func mapping(mapper: HelpingMapper) {
        mapper <<<
        self.planArray <-- "data"
    }
    
}

class ProtectionPlan: HandyJSON {
    
    /// 维保计划id
    var planId:String? = ""
    /// 维保计划名称
    var planName:String? = ""
    /// 最近完成时间
    var trueTaskEtime:String? = ""
    /// 维保计划状态（1正常2异常）
    var planStatus:Int? = 1
    
    required init(){}
    
    func mapping(mapper: HelpingMapper) {
        mapper <<<
        self.planId <-- "id"
    }
}


