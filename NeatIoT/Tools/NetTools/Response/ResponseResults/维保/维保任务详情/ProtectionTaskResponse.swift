//
//  ProtectionTaskResponse.swift
//  NeatIoT
//
//  Created by Yang Blus on 2020/5/18.
//  Copyright © 2020 Neat. All rights reserved.
//

import UIKit
import HandyJSON
class ProtectionTaskResponse: BaseResponse {
    
    var result:ProtectionTaskResopnseResult?

}

class ProtectionTaskResopnseResult: HandyJSON {
    
    var taskDetail:ProtectionTaskRecDetail?
    
    var uploadFiles:Array<UploadFile>?
    
    var materials:Array<MaterialsInfor>?
    
    
    required init() {
        
    }
    
    
}

class ProtectionTaskRecDetail: HandyJSON {
    
    /// 入场时间
    var maintenanceTime:String?
    /// 工时 小时
    var workHours:String?
    /// 描述
    var taskDes:String?
    /// 维保人
    var taskUserName:String?
    /// GIS位置
    var address:String?
    
    required init(){}
}

class UploadFile: HandyJSON {
    
    /// 文件地址
    var fileUrl:String?
    
    /// 文件地址
    var fileMime:String?
    
    /// 文件类型 3:视频 2:语音 1:图片
    var fileType:Int = 0
    
    /// 文件大小
    var fileSize:String?
    
    /// 视频 图片文件缩略图URL
    var fileThumbnailUrl:String?
    
    /// 音频文件时长
    var fileTimeSpan:String?
    
    
    required init (){}
    
}

class MaterialsInfor: HandyJSON {
    
    required init(){}
    /// 耗材名称
    var name:String?
    
    /// 数量
    var dosage:String?
    
    /// 应付总额
    var actualCost:String?
    
    /// 实付总额
    var amountPaid:String?
    
}
