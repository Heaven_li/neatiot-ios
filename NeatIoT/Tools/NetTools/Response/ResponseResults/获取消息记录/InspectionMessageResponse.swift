//
//  InspectionMessageResponse.swift
//  NeatIoT
//
//  Created by neat on 2019/12/24.
//  Copyright © 2019 Neat. All rights reserved.
//

import UIKit
import HandyJSON
class InspectionMessageResponse: BaseResponse {
    
    var resultData:InspectionMessageResultModel?

    override func mapping(mapper: HelpingMapper) {
        mapper <<<
        self.resultData <-- "result"
    }

}

class InspectionMessageResultModel: HandyJSON {

    /// 数据量
    var totalCount:Int?
    
    /// 消息数组
    var messageArr:Array<InspectionMessageItemModel>?
    
    required init(){
        
        
    }
    
    func mapping(mapper: HelpingMapper) {
        mapper <<<
        self.messageArr <-- "data"
    }
}
class InspectionMessageItemModel: HandyJSON {

    /// 消息内容
    var message:String?
    /// 推送时间
    var pushTime:String?
    /// 天前
    var daysAgo:String?
    /// 小时前
    var hoursAgo:String?
    /// 秒前
    var secondsAgo:String?
    /// 是否已读
    var status:Int?
    
    required init(){
        
        
    }
    
    
}
