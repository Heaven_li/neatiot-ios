//
//  IDNameResponse.swift
//  NeatIoT
//
//  Created by neat on 2019/11/4.
//  Copyright © 2019 Neat. All rights reserved.
//

import UIKit
import HandyJSON

class IDNameResponse: BaseResponse {
    
    /// 键值数组
    var item_arr:Array<KeyValueModel> = []
    
    override func mapping(mapper: HelpingMapper) {
        mapper <<<
        self.item_arr <-- "result"
    }
    
}
