//
//  QureyStatsChannelResponse.swift
//  NeatIoT
//
//  Created by Yang Blus on 2020/7/3.
//  Copyright © 2020 Neat. All rights reserved.
//

import UIKit
import HandyJSON

class QureyStatsChannelResponse: BaseResponse {
    
    
    var channelArr:Array<QureyStatsChannelItemModel>?
    
    override func mapping(mapper: HelpingMapper) {
        mapper <<<
        self.channelArr <-- "result"
    }

}
class QureyStatsChannelItemModel: HandyJSON {
    
    required init() {
        
    }
    /// 通道ID
    var id:String?
    /// 值
    var currentValue:String?
    /// 通道名称
    var channelName:String?
    /// 单位
    var unit:String?
    /// 更新时间
    var updateTime:String?
    
}
