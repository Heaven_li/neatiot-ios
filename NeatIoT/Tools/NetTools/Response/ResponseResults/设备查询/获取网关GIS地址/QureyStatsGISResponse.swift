//
//  QureyStatsGISResponse.swift
//  NeatIoT
//
//  Created by Yang Blus on 2020/7/3.
//  Copyright © 2020 Neat. All rights reserved.
//

import UIKit
import HandyJSON

class QureyStatsGISResponse: BaseResponse {
    
    var gisModel:QureyStatsGISModel?
    
    override func mapping(mapper: HelpingMapper) {
        mapper <<<
        self.gisModel <-- "result"
    }

}

class QureyStatsGISModel: HandyJSON {
    
    required init() {
        
    }
    
    /// 经度
    var longitude:Double?
    /// 纬度
    var latitude:Double?
    /// gis位置
    var gisAddress:String?
    
}
