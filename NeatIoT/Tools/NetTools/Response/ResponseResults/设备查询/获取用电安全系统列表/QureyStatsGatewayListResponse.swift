//
//  QureyStatsGatewayListResponse.swift
//  NeatIoT
//
//  Created by Yang Blus on 2020/7/3.
//  Copyright © 2020 Neat. All rights reserved.
//

import UIKit
import HandyJSON
class QureyStatsGatewayListResponse: BaseResponse {
    
    var qureyListResponse:QureyStatsGatewayResultModel?

    override func mapping(mapper: HelpingMapper) {
        mapper <<<
        self.qureyListResponse <-- "result"
    }
    

}

class QureyStatsGatewayResultModel: HandyJSON {
    
    required init() {
        
    }
    
    var totalCount:Int?
    
    var data:Array<QureyStatsGatewayItemModel>?
    
}


class QureyStatsGatewayItemModel: HandyJSON {
    
    required init() {
        
    }
    
    /// 网关ID
    var gatewayId:String?
    /// 设备在线状态2在线 1离线
    var status:Int?
    /// 设备编码
    var code:String?
    /// 设备名称
    var name:String?
    /// 安装位置
    var address:String?
    
    func mapping(mapper: HelpingMapper) {
        mapper <<<
        self.gatewayId <-- "id"
    }
    
    
    
    
}
