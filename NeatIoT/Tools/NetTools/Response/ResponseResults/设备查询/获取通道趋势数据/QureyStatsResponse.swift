//
//  QureyStatsResponse.swift
//  NeatIoT
//
//  Created by Yang Blus on 2020/7/3.
//  Copyright © 2020 Neat. All rights reserved.
//

import UIKit
import HandyJSON
class QureyStatsResponse: BaseResponse {
    
    var dataArr:Array<QureyStatsItemModel>?
    
    override func mapping(mapper: HelpingMapper) {
        mapper <<<
        self.dataArr <-- "result"
    }

}

class QureyStatsItemModel: HandyJSON {
    
    
    required init() {
        
    }
    /// 值
    var value:Double?
    /// 时间
    var time:String?
}
