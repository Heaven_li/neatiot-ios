//
//  QureyStatsEventResponse.swift
//  NeatIoT
//
//  Created by Yang Blus on 2020/7/3.
//  Copyright © 2020 Neat. All rights reserved.
//

import UIKit
import HandyJSON
class QureyStatsEventResponse: BaseResponse {
    
    var itemArr:Array<QureyStatsEventItemModel>?
    
    override func mapping(mapper: HelpingMapper) {
        mapper <<<
        self.itemArr <-- "result"
    }

}

class QureyStatsEventItemModel: HandyJSON {
    
    required init() {
        
    }
    
    /// 事件类型 火警  = 1,故障= 2,状态 = 3,操作 = 4,报警= 5
    var eventType:Int = -1
    /// 事件内容
    var eventContent:String?
    /// 发生事件
    var occurTime:String?
    
}


