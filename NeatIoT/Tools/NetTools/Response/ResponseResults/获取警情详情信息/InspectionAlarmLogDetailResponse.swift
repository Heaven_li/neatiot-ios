//
//  InspectionAlarmLogDetailResponse.swift
//  NeatIoT
//
//  Created by Yang Blus on 2020/2/10.
//  Copyright © 2020 Neat. All rights reserved.
//

import UIKit
import HandyJSON
class InspectionAlarmLogDetailResponse: BaseResponse {
    
    var alarmModel:InspectionAlarmDetailResult?
    
    override func mapping(mapper: HelpingMapper) {
        mapper <<<
        self.alarmModel <-- "result"
    }

}

class InspectionAlarmDetailResult: HandyJSON {
    
    /// 事件ID
    var id:String?
    /// 设备ID
    var deviceId:String?
    /// 设备类型
    var deviceIdType:String?
    /// 设备类型名称
    var deviceIdTypeString:String?
    /// 设备编码
    var deviceCode:String?
    /// 设备名称
    var deviceName:String?
    /// 所属系统
    var systemCategory:String?
    /// 中心名称
    var domainName:String?
    /// 企业名称
    var enterpriseName:String?
    /// 建筑名称
    var buildingName:String?
    /// 部位名称
    var keypartName:String?
    /// 安装位置
    var address:String?
    /// 报警内容
    var alarmDesc:String?
    /// 报警类型
    var alarmCategoryStr:String?
    /// 发生时间
    var occurTime:String?
    /// 最后发生时间
    var lastOccurTime:String?
    /// 发生次数
    var times:String?
    ///当前值
    var value:String?
    ///阈值
    var threshold:String?
    ///在线状态
    var onlineStauts:Int?
    
    required init(){
        
    }
}
