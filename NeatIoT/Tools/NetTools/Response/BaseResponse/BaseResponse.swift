//
//  BaseResponse.swift
//  NeatIoT
//
//  Created by neat on 2019/10/12.
//  Copyright © 2019 Neat. All rights reserved.
//

import HandyJSON

class BaseResponse: HandyJSON{
    
    ///返回码
    var code: Int?
    /// 信息
    var message: String?
    
    required init() {}
    
    func mapping(mapper: HelpingMapper) {
        
    }

}
