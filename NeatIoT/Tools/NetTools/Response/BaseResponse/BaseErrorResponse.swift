//
//  BaseErrorResponse.swift
//  NeatIoT
//
//  Created by neat on 2019/10/25.
//  Copyright © 2019 Neat. All rights reserved.
//

import HandyJSON

class BaseErrorResponse: NSObject, HandyJSON{
    
    /// 错误码
    var errCode:Int = -1
    /// 错误提示
    var errMessage:String = "调用接口发生错误"
    
    
    required override init() {
        
    }
    
    
    func mapping(mapper: HelpingMapper) {
        
        mapper <<<
        self.errCode <-- "code"
        mapper <<<
        self.errMessage <-- "message"
        
    }
    

}
