//
//  DataVerifyResult.swift
//  NeatIoT
//
//  Created by neat on 2019/10/23.
//  Copyright © 2019 Neat. All rights reserved.
//

import UIKit


/// 验证结果model
class DataVerifyResult: NSObject {
    
    /// 验证结果 true:通过  false:失败
    var correct:Bool?
    /// 错误提示
    var errMessage:String?
    
    override init() {
        
    }
}


