//
//  DataCheck.swift
//  NeatIoT
//
//  Created by neat on 2019/10/23.
//  Copyright © 2019 Neat. All rights reserved.
//

import UIKit


/// 数据验证工具类
class DataVerify: NSObject {
    
    /// 验证IP地址端口号是否正确
    /// - Parameter host: IP地址端口号
    class func checkHost(host:String) -> DataVerifyResult {
        
        let verifyResult = DataVerifyResult.init()
        verifyResult.errMessage = "成功"
        verifyResult.correct = true
        
        do {
            
            if host.contains(":") {
                
                let strArr = host.split(separator: ":")
                
                //验证端口
                let portReg = try NSRegularExpression.init(pattern: "^[1-9]$|(^[1-9][0-9]$)|(^[1-9][0-9][0-9]$)|(^[1-9][0-9][0-9][0-9]$)|(^[1-6][0-5][0-5][0-3][0-5]$)", options: .anchorsMatchLines)
                let portArr = portReg.matches(in: String(strArr[1]), options: .anchored, range: NSRange.init(location: 0, length: String(strArr[1]).count))
                
                //验证IP地址
                let reg = try NSRegularExpression.init(pattern: "^((2(5[0-5]|[0-4]\\d))|[0-1]?\\d{1,2})(\\.((2(5[0-5]|[0-4]\\d))|[0-1]?\\d{1,2})){3}$", options: .anchorsMatchLines)
                let regArr = reg.matches(in: String(strArr[0]), options: .anchored, range: NSRange.init(location: 0, length: String(strArr[0]).count))
                if (!(regArr.count == 1 && portArr.count == 1)) {
                    verifyResult.errMessage = "IP地址及端口号格式错误"
                    verifyResult.correct = false
                }
            }else{
                verifyResult.errMessage = "IP地址及端口号格式错误"
                verifyResult.correct = false
            }
            
        } catch {
            
            debugPrint(error)
            verifyResult.errMessage = "匹配错误"
            verifyResult.correct = false
        }
        
        return verifyResult
        
    }
    
    /// 校验用户名是否符合规则
    /// - Parameter userName: 用户名
    class func checkUserName(userName:String) -> DataVerifyResult {
        
        let verifyResult = DataVerifyResult.init()
        verifyResult.correct = true
        verifyResult.errMessage = ""
        if !((userName.count >= 4) && (userName.count <= 16)) {
            verifyResult.correct = false
            verifyResult.errMessage = "用户名长度应为4~16位且字母开头"
        }
        
        do {
            
            let indexReg = try NSRegularExpression.init(pattern: "^[a-zA-Z]$", options: .anchorsMatchLines)
            let indexArr = indexReg.numberOfMatches(in: userName, options: [], range: NSRange.init(location: 0, length: 1))
        
            if indexArr != 0 {
                
                let otherReg = try NSRegularExpression.init(pattern: "^[A-Za-z0-9_]{3,15}$", options: .anchorsMatchLines)
                
                let otherMatchArr = otherReg.matches(in: userName, options: [], range: NSRange.init(location: 1, length: userName.count - 1))
                
                if otherMatchArr.count == 0 {
                    verifyResult.correct = false
                    verifyResult.errMessage = "用户名只能由数字、字母、下划线组成"
                }
                
            }else{
                verifyResult.correct = false
                verifyResult.errMessage = "用户名应以字母开头"
            }
            
        } catch {
            
            debugPrint(error)
            verifyResult.correct = false
            verifyResult.errMessage = "匹配错误"
        }
        
        return verifyResult
        
    }
    
    /// 校验密码
    /// - Parameter password: 密码
    class func checkPassword(password:String) -> DataVerifyResult {
        //长度大于6
        let passwordResult = DataVerifyResult.init()
        passwordResult.correct = true
        passwordResult.errMessage = "正确"
        if (password.count < 6){
            passwordResult.correct = false
            passwordResult.errMessage = "密码长度错误，至少为6位"
        }
        return passwordResult
    }
    
    /// 校验设备名称
    /// - Parameter deviceName: 设备名称次
    class func checkDeviceName(deviceName:String) -> DataVerifyResult {
        
        let verifyResult = DataVerifyResult.init()
        verifyResult.correct = true
        verifyResult.errMessage = "正确"
        if (!deviceName.isEmpty){
            //一个汉字两个字符
            
            do {
                let otherReg = try NSRegularExpression.init(pattern: "^[\\u4e00-\\u9fa5A-Za-z0-9_#]+$", options: .anchorsMatchLines)
                
                let otherMatchArr = otherReg.matches(in: deviceName, options: [], range: NSRange.init(location: 0, length: deviceName.count))
                
                if otherMatchArr.count == 0 {
                    verifyResult.correct = false
                    verifyResult.errMessage = "名称只能由汉字、数字、字母、下划线及#组成"
                }else{
                    //设备名称 不能超过20个字符 一个汉字两个字符
                    let isOk = self.getStrCharLenght(str: deviceName, maxLength: 20)
                    
                    if !isOk {
                        verifyResult.correct = false
                        verifyResult.errMessage = "设备名称长度不能超过20个字符，一个汉字两个字符"
                    }
                }
            } catch  {
                verifyResult.correct = false
                verifyResult.errMessage = "名称包含不支持字符"
            }

            
        }else{
            verifyResult.correct = false
            verifyResult.errMessage = "名称不能为空"
        }
        return verifyResult
    }
    
    /// 校验设备安装地址 
    /// - Parameter address: 设备安装地址
    class func checkDeviceAddress(address:String) -> DataVerifyResult {
        
        let verifyResult = DataVerifyResult.init()
        verifyResult.correct = true
        verifyResult.errMessage = "正确"
        if (!address.isEmpty){
            
            do {
                let otherReg = try NSRegularExpression.init(pattern: "^[\\u4e00-\\u9fa5A-Za-z0-9_#]+$", options: .anchorsMatchLines)
                
                let otherMatchArr = otherReg.matches(in: address, options: [], range: NSRange.init(location: 0, length: address.count))
                
                if otherMatchArr.count == 0 {
                    verifyResult.correct = false
                    verifyResult.errMessage = "安装位置只能由汉字、数字、字母、下划线及#组成"
                }else{
                    
                    //设备安装地址 不能超过20个字符 一个汉字两个字符
                    let isOk = self.getStrCharLenght(str: address, maxLength: 20)
                    
                    if !isOk {
                        verifyResult.correct = false
                        verifyResult.errMessage = "安装位置长度不能超过20个字符，一个汉字两个字符"
                    }
                    
                }
            } catch  {
                verifyResult.correct = false
                verifyResult.errMessage = "安装位置只能由汉字、数字、字母、下划线及#组成"
            }
            
        }else{
            verifyResult.correct = false
            verifyResult.errMessage = "安装位置不能为空"
        }
        return verifyResult
    }
    
    /// 校验设备编码
    /// - Parameter code: 设备编码
    class func checkDeviceCode(code:String) -> DataVerifyResult {
        
        let verifyResult = DataVerifyResult.init()
        verifyResult.correct = true
        verifyResult.errMessage = "正确"
        if (!code.isEmpty){
            
            do {
                let otherReg = try NSRegularExpression.init(pattern: "^[A-Za-z0-9.]+$", options: .anchorsMatchLines)
                
                let otherMatchArr = otherReg.matches(in: code, options: [], range: NSRange.init(location: 0, length: code.count))
                
                if otherMatchArr.count == 0 {
                    verifyResult.correct = false
                    verifyResult.errMessage = "编码只能由数字、字母及.组成"
                }
            } catch  {
                verifyResult.correct = false
                verifyResult.errMessage = "编码只能由数字、字母及.组成"
            }
            
        }else{
            
            verifyResult.correct = false
            verifyResult.errMessage = "编码不能为空"
            
        }
        
        return verifyResult
    }
    
    class func checkWorkHourCheck(workHour:String) -> DataVerifyResult{
        
        let verifyResult = DataVerifyResult.init()
        verifyResult.correct = true
        verifyResult.errMessage = "正确"
        
        if workHour.isEmpty {
            verifyResult.correct = false
            verifyResult.errMessage = "请输入工时"
            return verifyResult
        }
        
        /// 检查是否只是数据和 .
        do {
            let otherReg = try NSRegularExpression.init(pattern: "^[0-9.]+$", options: .anchorsMatchLines)
            
            let otherMatchArr = otherReg.matches(in: workHour, options: [], range: NSRange.init(location: 0, length: workHour.count))
            
            if otherMatchArr.count == 0 {
                verifyResult.correct = false
                verifyResult.errMessage = "工时只能由数字及.组成"
                return verifyResult
            }else{
                
                if workHour.contains("."){
                    //如果包含小数点
                    let chars = workHour.split(separator: ".")
                    
                    if chars.count > 1 {
                        let num = chars[0]
                        let dot = chars[1]
                        
                        if Int(String(num)) ?? 0 > 999 {
                            
                            verifyResult.correct = false
                            verifyResult.errMessage = "您输入的工时格式有误，工时只能在0至999小时范围内"
                            return verifyResult
                        }
                        if dot.count > 1 {
                            verifyResult.correct = false
                            verifyResult.errMessage = "您输入的工时格式有误，只能保留小数点后1位有效数"
                            return verifyResult
                        }
                    }else{
                        verifyResult.correct = false
                        verifyResult.errMessage = "您输入的工时格式有误，工时只能在0至999小时范围内"
                        return verifyResult
                    }
                    
                }else{
                    
                    let workTime = Int(workHour)
                    if workTime ?? 0 > 999 {
                        verifyResult.correct = false
                        verifyResult.errMessage = "您输入的工时格式有误，工时只能在0至999小时范围内"
                        return verifyResult
                    }
                    
                }
                
            }
        } catch  {
            verifyResult.correct = false
            verifyResult.errMessage = "工时只能由数字及.组成"
            return verifyResult
        }
        
        
        return verifyResult
        
    }
    
    class func getStrCharLenght(str:String,maxLength:Int) -> Bool {
        
        var totalLength = 0
        
        
        str.forEach { (chart) in
            
            if chart.utf8.count == 1{
                totalLength += 1
            }else{
                totalLength += 2
            }
            
        }
        
        if totalLength <= maxLength {
            return true
        }else{
            return false
        }
        
    }
    
    
    

}
