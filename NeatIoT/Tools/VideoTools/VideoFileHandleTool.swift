//
//  VideoFileHandleTool.swift
//  NeatIoT
//
//  Created by Yang Blus on 2020/3/4.
//  Copyright © 2020 Neat. All rights reserved.
//

import UIKit
import AVFoundation

class VideoFileHandleTool: NSObject {
    
    /// 获取视频缩略图
    /// - Parameter url: 视频地址(NSURL)
    class func getVideoThum(url:URL) -> UIImage {
        
        let asset = AVAsset.init(url: url)
        
        let gen = AVAssetImageGenerator.init(asset: asset)
        
        gen.appliesPreferredTrackTransform = true
        
        let time = CMTimeMakeWithSeconds(0.0, preferredTimescale: 600)
        
        var actualTime = CMTimeMake(value: 0,timescale: 0)
        
        let image = try! gen.copyCGImage(at: time, actualTime: &actualTime)
        
        let thumb = UIImage.init(cgImage: image)
        
        return thumb
        
    }

}
