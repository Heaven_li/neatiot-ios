//
//  VideoFormatTransformTool.swift
//  NeatIoT
//
//  Created by Yang Blus on 2020/3/4.
//  Copyright © 2020 Neat. All rights reserved.
//

import UIKit
import AVFoundation

class VideoFormatTransformTool: NSObject {
    
    /*
     视频转换格式.mov 转成 .mp4
     方法中sourceUrl参数为.mov的URL数据
     */
    class func moveFileTransformToMp4WithSourceUrl(sourceUrl: URL,finish:@escaping (_ datatUrl:URL)->Void,failed: @escaping ()->Void) {
            
            var dataurl:URL?
            let sourcePath = sourceUrl.path
            let subArr = sourcePath.split(separator: ".")
            if subArr.count != 0 {
                
                let charces = subArr[0]
                let outPutPath = String.init(charces) + "_video" + ".mp4"
                
                debugPrint("输出文件路径",outPutPath)
                
                if FileManager.default.isExecutableFile(atPath: outPutPath) {
                    //文件转码已存在 提取使用即可
                    dataurl = URL.init(fileURLWithPath: outPutPath)
                    finish(dataurl!)
                    return
                }
                
                //转码配置
                let avAsset = AVURLAsset.init(url: sourceUrl, options: nil)

                //取视频的时间并处理，用于上传
    //            let time = avAsset.duration
    //            let number = Float(CMTimeGetSeconds(time)) - Float(Int(CMTimeGetSeconds(time)))
    //            let totalSecond = number > 0.5 ? Int(CMTimeGetSeconds(time)) + 1 : Int(CMTimeGetSeconds(time))
                //let photoId = String(totalSecond)
                
                let exportSession = AVAssetExportSession.init(asset: avAsset, presetName: AVAssetExportPresetMediumQuality)
                exportSession?.shouldOptimizeForNetworkUse = true
                exportSession?.outputURL = URL.init(fileURLWithPath: outPutPath)
                exportSession?.outputFileType = AVFileType.mp4 //控制转码的格式
                exportSession?.exportAsynchronously(completionHandler: {
                    if exportSession?.status == AVAssetExportSession.Status.failed {
                        debugPrint("mp4------转码失败")
                        failed()
                    }
                    if exportSession?.status == AVAssetExportSession.Status.completed {
                        debugPrint("mp4------转码成功")
                        //转码成功后就可以通过dataurl获取视频的Data用于上传了
                        dataurl = URL.init(fileURLWithPath: outPutPath)
                        
                        finish(dataurl!) //上传视频的话是需要同时上传一张视频封面图片的，这里附带一个获取视频封面截图的方法，方法实现在下方
        //                let image = getVideoCropPicture(videoUrl: sourceUrl)
                    }
                })
                
            }

        }

}
