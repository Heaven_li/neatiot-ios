//
//  LoginDataArchiver.swift
//  NeatIoT
//
//  Created by neat on 2019/10/24.
//  Copyright © 2019 Neat. All rights reserved.
//


class DataArchiver: NSObject {
    
//    static private var filePath : String {
//        let manager = FileManager.default
//        let url = manager.urls(for: .documentDirectory, in: .userDomainMask).first! as NSURL
//        return url.appendingPathComponent("Archiver")!.path
//    }
    
    class private func getFilePath(component:String) -> String {
        let manager = FileManager.default
        let url = manager.urls(for: .documentDirectory, in: .userDomainMask).first! as NSURL
        return url.appendingPathComponent(component)!.path
    }
    
    
    class func archiver(value :AnyObject, key: String) -> Void {
        // 存储到第一步设置的路径之下  value 就是你要存储的数据
        
        do {
            if #available(iOS 11.0, *) {
               let archiverData = try NSKeyedArchiver.archivedData(withRootObject: value, requiringSecureCoding: false) as NSData
            
                UserDefaults.standard.set(archiverData, forKey: "userInforData")
            } else {
                // Fallback on earlier versions
                let isArchiver = NSKeyedArchiver.archiveRootObject(value, toFile: getFilePath(component:key))
                if isArchiver {
                    debugPrint("数据存储成功")
                }
            }
        } catch  {
            
            debugPrint("catch")
        }
        
        
    }
    
    class func unArchiver(key: String) -> AnyObject? {
        // 读取出来的数据格式就是你当时存储的格式，你存的 Data读取的就是 Data ,你存Model 读取的就是 Model。 注意读取出来的 value 是一个可选值 ，需要做处理之后使用
        
        if #available(iOS 11.0, *) {
            
            let archiveData = UserDefaults.standard.data(forKey: "userInforData")
            
            if archiveData == nil {
                return nil
            }
            
            let value = NSKeyedUnarchiver.unarchiveObject(with: archiveData ?? Data())
            return value as AnyObject
           
        } else {
            // Fallback on earlier versions
            if FileManager.default.fileExists(atPath: getFilePath(component:key)) {
                let  value = NSKeyedUnarchiver.unarchiveObject(withFile: getFilePath(component:key))
                return value as AnyObject
            }
        }
        
        return nil
        
    }
    
    class func clearArchiver(keyArr:Array<String>) -> Void{
       
        if #available(iOS 11.0, *) {
            UserDefaults.standard.removeObject(forKey: "userInforData")
        }else{
            for key in keyArr {
                if FileManager.default.fileExists(atPath: getFilePath(component:key)){
                    do {
                        
                        try FileManager.default.removeItem(atPath: getFilePath(component:key))
                                                
                    } catch {
                        debugPrint(error)
                    }
                    
                }
            }
           
        }
          
    }


}
