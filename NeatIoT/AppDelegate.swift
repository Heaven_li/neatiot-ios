//
//  AppDelegate.swift
//  NeatIoT
//
//  Created by neat on 2019/9/29.
//  Copyright © 2019 Neat. All rights reserved.
//

import UIKit
import CoreData
import AMapFoundationKit
import AVFoundation

@available(iOS 12.0, *)
@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    
//    lazy var managerObjectModel: NSManagedObjectModel = {
//        let contentUrl = Bundle.main.url(forResource: "NeatDatabase", withExtension: "momd")
//        let managerObjectModel = NSManagedObjectModel.init(contentsOf: contentUrl!)
//        return managerObjectModel!
//    }()
//
//    lazy var persistentStoreCoordinator: NSPersistentStoreCoordinator = {
//
//        let storeCoordinator = NSPersistentStoreCoordinator.init(managedObjectModel: self.managerObjectModel)
//        let sqliteURL = documentDir.appendingPathComponent("Model.sqlite")
//                let options = [NSMigratePersistentStoresAutomaticallyOption : true, NSInferMappingModelAutomaticallyOption : true]
//                var failureReason = "There was an error creating or loading the application's saved data."
//
//                do {
//                    try storeCoordinator.addPersistentStore(ofType: NSSQLiteStoreType, configurationName: nil, at: sqliteURL, options: options)
//                } catch {
//                    // Report any error we got.
//                    var dict = [String: Any]()
//                    dict[NSLocalizedDescriptionKey] = "Failed to initialize the application's saved data" as Any?
//                    dict[NSLocalizedFailureReasonErrorKey] = failureReason as Any?
//                    dict[NSUnderlyingErrorKey] = error as NSError
//                    let wrappedError = NSError(domain: "YOUR_ERROR_DOMAIN", code: 6666, userInfo: dict)
//                    print("Unresolved error \(wrappedError), \(wrappedError.userInfo)")
//                    abort()
//                }
//
//        return storeCoordinator
//    }()
//
//    lazy var context: NSManagedObjectContext = {
//        let context = NSManagedObjectContext.init(concurrencyType: NSManagedObjectContextConcurrencyType.mainQueueConcurrencyType)
//        context.persistentStoreCoordinator = persistentStoreCoordinator
//        return context
//    }()
//
//
//    lazy var documentDir: URL = {
//            let documentDir = FileManager.default.urls(for: FileManager.SearchPathDirectory.documentDirectory, in: FileManager.SearchPathDomainMask.userDomainMask).first
//            return documentDir!
//        }()
    
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        window = UIWindow.init(frame: UIScreen.main.bounds)
        
        window?.backgroundColor = UIColor.white
        
        
        let loginRes = DataArchiver.unArchiver(key:ArchireKey.UserInfor)
        
        if loginRes != nil {
            let userInforModel = loginRes as! LoginResultModel
            if userInforModel.user_login {
                window?.rootViewController = SupperNavigationViewController.init(rootViewController: HomeFunctionViewController.init())
            }
        }else{
           window?.rootViewController = SupperNavigationViewController.init(rootViewController: LoginViewController.init())
        }
        
        AMapServices.shared()?.apiKey = "79bb97c12f3b94a0fbe733f9a2098ff2"
        
        let entity = JPUSHRegisterEntity.init()
        entity.types = Int(JPAuthorizationOptions.alert.rawValue | JPAuthorizationOptions.badge.rawValue | JPAuthorizationOptions.sound.rawValue | JPAuthorizationOptions.providesAppNotificationSettings.rawValue)
        
        JPUSHService.register(forRemoteNotificationConfig: entity, delegate: self)
    
        JPUSHService.setup(withOption: launchOptions, appKey: "e414c32a1e777d2c30574338", channel: "企业版", apsForProduction: false)
        
        window?.makeKeyAndVisible()
        
        return true
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        UIApplication.shared.applicationIconBadgeNumber = 0
    }
    
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        JPUSHService.registerDeviceToken(deviceToken)
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        debugPrint("didFailToRegisterForRemoteNotificationsWithError")
    }
    
    func currentViewController(base: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
        if let nav = base as? UINavigationController {
            return currentViewController(base: nav.visibleViewController)
        }
        if let tab = base as? UITabBarController {
            return currentViewController(base: tab.selectedViewController)
        }
        if let presented = base?.presentedViewController {
            return currentViewController(base: presented)
        }
        return base
    }
    

   


}


@available(iOS 12.0, *)
extension AppDelegate:JPUSHRegisterDelegate{
    
    // iOS10
    func jpushNotificationCenter(_ center: UNUserNotificationCenter!, willPresent notification: UNNotification!, withCompletionHandler completionHandler: ((Int) -> Void)!) {
        
        let dic = notification.request.content.userInfo
        
        if notification.request.trigger is UNPushNotificationTrigger {
            JPUSHService.handleRemoteNotification(dic)
            
            let vc = self.currentViewController()
            vc?.navigationController?.pushViewController(LogViewController.init(), animated: true)
        }
        
    completionHandler(Int(UNNotificationPresentationOptions.alert.rawValue))
        
        
    }
    // iOS10
    func jpushNotificationCenter(_ center: UNUserNotificationCenter!, didReceive response: UNNotificationResponse!, withCompletionHandler completionHandler: (() -> Void)!) {
        
        let userInfo = response.notification.request.content.userInfo;
        if(response.notification.request.trigger is UNPushNotificationTrigger) {
            JPUSHService.handleRemoteNotification(userInfo)
            
            let vc = self.currentViewController()
            vc?.navigationController?.pushViewController(LogViewController.init(), animated: true)
            
        }
        // 系统要求执行这个方法
        completionHandler()
    }
    
    //iOS12
    func jpushNotificationCenter(_ center: UNUserNotificationCenter!, openSettingsFor notification: UNNotification!) {
        
        if ((notification != nil) && (notification.request.trigger is UNPushNotificationTrigger)){
            //从通知界面直接进入应用
            let vc = self.currentViewController()
            vc?.navigationController?.pushViewController(LogViewController.init(), animated: true)
            
            
        }else{
            //从通知设置界面进入应用
            
        }
    }
    
    func jpushNotificationAuthorization(_ status: JPAuthorizationStatus, withInfo info: [AnyHashable : Any]!) {
        
    }
    
    
    
    
    
    
}

