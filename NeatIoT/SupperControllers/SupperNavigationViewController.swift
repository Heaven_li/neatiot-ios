//
//  SupperNavigationViewController.swift
//  NeatIoT
//
//  Created by neat on 2019/10/10.
//  Copyright © 2019 Neat. All rights reserved.
//

import UIKit

class SupperNavigationViewController: UINavigationController {
    
    override var childForStatusBarHidden: UIViewController?{
        return self.topViewController
    }
    
    override var childForStatusBarStyle: UIViewController?{
        return self.topViewController
    }
    
    
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        self.interactivePopGestureRecognizer?.delegate = self
    
        // Do any additional setup after loading the view.
    }
    
    
    @objc func popEdge() {
        debugPrint("返回")
    }
    
    
    override func pushViewController(_ viewController: UIViewController, animated: Bool) {
        super.pushViewController(viewController, animated: animated)
        if self.topViewController is DeviceMainListViewController {
            ///添加消息通知
            let deviceMainVC = self.topViewController as! DeviceMainListViewController
            NotificationCenter.default.addObserver(deviceMainVC, selector: #selector(deviceMainVC.notRefreshListData), name: NSNotification.Name(rawValue: CustomNotificationName.ReloadDeviceListData), object: nil)
        }else if self.topViewController is ProtectionHandleViewController{
            ///添加消息通知
            let vc = self.topViewController as! ProtectionHandleViewController
            NotificationCenter.default.addObserver(vc, selector: #selector(vc.reciveMaterials(notify:)), name: NSNotification.Name(rawValue: CustomNotificationName.ReciveMaterialsData), object: nil)
            
        }else if self.topViewController is HostDeviceMainViewController{
            ///添加消息通知
            let vc = self.topViewController as! HostDeviceMainViewController
            NotificationCenter.default.addObserver(vc, selector:#selector(vc.refreshHostDeviceListData), name: NSNotification.Name(rawValue: CustomNotificationName.ReloadHostDeviceListData), object: nil)
            
        }
        
    }
    
    override func popViewController(animated: Bool) -> UIViewController? {
        
        if self.topViewController is DeviceMainListViewController {
            ///移除消息通知"
            let deviceMainVC = self.topViewController as! DeviceMainListViewController
            NotificationCenter.default.removeObserver(deviceMainVC, name: NSNotification.Name(rawValue: CustomNotificationName.ReloadDeviceListData), object: nil)
        }else if self.topViewController is ProtectionHandleViewController{
            
            let vc = self.topViewController as! ProtectionHandleViewController
            NotificationCenter.default.removeObserver(vc, name: NSNotification.Name(rawValue: CustomNotificationName.ReciveMaterialsData), object: nil)
            
        }else if self.topViewController is MaterialsViewController{
            CoreDataHelper.init().deleteMaterialsObjec()
        }else if self.topViewController is HostDeviceMainViewController{
            ///添加消息通知
            let vc = self.topViewController as! HostDeviceMainViewController
            NotificationCenter.default.removeObserver(vc, name: NSNotification.Name(rawValue: CustomNotificationName.ReloadHostDeviceListData), object: nil)
            
        }
        
        return super.popViewController(animated: animated)
    }
    
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension SupperNavigationViewController:UIGestureRecognizerDelegate{
    
    func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        
        let topVC = topViewController as! SupperViewController
        
        return topVC.isCanSwiperBack

    }
    
}



