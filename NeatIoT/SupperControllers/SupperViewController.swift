//
//  SupperViewController.swift
//  NeatIoT
//
//  Created by neat on 2019/10/9.
//  Copyright © 2019 Neat. All rights reserved.
//

import UIKit
import CoreServices
import AVFoundation

/// 所有试图控制器的父类
class SupperViewController: UIViewController {
    
    /// 页面是否支持滑动返回
    var isCanSwiperBack = true
    
    let imagePickerVC = UIImagePickerController.init()
    
    var getUserInfor: LoginResultModel? {
        let loginRes = DataArchiver.unArchiver(key:ArchireKey.UserInfor)
        
        if loginRes != nil {
            let userInforModel = loginRes as! LoginResultModel
            return userInforModel
        }else{
            return nil
        }
        
    }
    
    /// 导航栏标题
    var titleName: String {
        get {
            return String.init(self.navigationItem.title ?? "")
        }
        set {
            self.navigationItem.title = newValue
        }
    }
    
    /// 导航栏右侧item数组
    var rightItems: Array<NavItemModel> {
        get {
            return []
        }
        set {
            configerRightItems(items: newValue)
        }
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle{
        return .lightContent
    }
    
    override func viewDidLoad() {
    
        super.viewDidLoad()
        
        self.view.backgroundColor = NormalColor.viewControllerDefaultBgColor
        
        //设置导航栏样式
        self.navigationController?.navigationBar.tintColor = .white
        self.navigationItem.title = self.titleName
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.titleTextAttributes = [.foregroundColor:UIColor.white,.font:UIFont.systemFont(ofSize: 18)]
        
        self.navigationController?.navigationBar.setBackgroundImage(UIImage.init(named: "nav_bar_bg"), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage.init()
        
        let backBtn = UIButton.init(type: .custom)
        backBtn.frame = CGRect.init(x: 0, y: 0, width: 46, height: 46)
        backBtn.setImage(UIImage.init(named: "back"), for: .normal)
        backBtn.imageEdgeInsets = UIEdgeInsets.init(top: 0, left: -23, bottom: 0, right: 0)
        backBtn.addTarget(self, action: #selector(backClick), for: .touchUpInside)
        
        let backItemBtn = UIBarButtonItem.init(customView: backBtn)
        
        self.navigationItem.leftBarButtonItem = backItemBtn
        
    }
    
    /// 配置右侧按钮
    /// - Parameter items: 按钮数组
    func configerRightItems(items:Array<NavItemModel>) -> Void {
        
        var itemArr = Array<UIBarButtonItem>()
        
        for (index, value) in items.enumerated() {
            var buttonItem: UIBarButtonItem = UIBarButtonItem.init()
            if index == 0 {
                if value.itemIcon.isEmpty {
                    buttonItem = UIBarButtonItem.init(title: value.itemText, style: .plain, target: self, action: #selector(navRRClick))
                }else{
                    buttonItem = UIBarButtonItem.init(image: UIImage.init(named: value.itemIcon), style:.plain, target: self, action: #selector(navRRClick))
                }
            }else if index == 1{
                if value.itemIcon.isEmpty {
                    buttonItem = UIBarButtonItem.init(title: value.itemText, style: .plain, target: self, action: #selector(navRLClick))
                }else{
                    buttonItem = UIBarButtonItem.init(image: UIImage.init(named: value.itemIcon), style:.plain, target: self, action: #selector(navRLClick))
                }
            }
            itemArr.append(buttonItem)
        }
    
        self.navigationItem.rightBarButtonItems = itemArr
        
        self.automaticallyAdjustsScrollViewInsets = false
        
    }
    
    /// 返回
    @objc func backClick() -> Void {
        self.navigationController?.popViewController(animated: true)
    }
    
    /// 导航栏右侧最右按钮点击事件 如果只有一个按钮可重写该方法
    @objc func navRRClick() -> Void {
        
    }
    /// 导航栏右侧最左按钮点击事件
    @objc func navRLClick() -> Void {
        
    }
    
    func popTo(vcClass:UIViewController) {
        
        let childrenVCs = self.navigationController?.children ?? []
        for vc in childrenVCs {
            if type(of: vc) == type(of: vcClass) {
                self.navigationController?.popToViewController(vc, animated: true)
            }
        }
    }
    
    /// alert 弹出方法
    /// - Parameters:
    ///   - vc: 父VC
    ///   - title: 标题
    ///   - message: 描述
    ///   - confirmTitle: 确定
    ///   - cancleTitle: 取消
    ///   - confirmAction: 确定回调
    ///   - cancleAction: 取消回调
    func showAlert(vc:UIViewController,title:String?,message:String,leftTitle:String,rightTitle:String,leftAction:@escaping(_ action:UIAlertAction)->Void, rightAction:@escaping(_ action:UIAlertAction)->Void) {
        
        let alertController = UIAlertController.init(title: title, message: message, preferredStyle: .alert)
        let actionSure = UIAlertAction.init(title: leftTitle, style: .cancel) { (action) in
            leftAction(action)
        }
        let actionCancel = UIAlertAction.init(title: rightTitle, style: .destructive) { (action) in
            rightAction(action)
        }
        alertController.addAction(actionCancel)
        alertController.addAction(actionSure)
        
        
        vc.present(alertController, animated: true, completion: nil)
    }
    
    func showActionSheet(vc:UIViewController,title:String?,message:String,topTitle:String,bottomTitle:String,topAction:@escaping(_ action:UIAlertAction)->Void, bottomAction:@escaping(_ action:UIAlertAction)->Void) {
        
        let alertController = UIAlertController.init(title: title, message: message, preferredStyle: .actionSheet)
        let actionTop = UIAlertAction.init(title: topTitle, style: .default) { (action) in
            topAction(action)
        }
        let actionBottom = UIAlertAction.init(title: bottomTitle, style: .default) { (action) in
            bottomAction(action)
        }
        let cancel = UIAlertAction.init(title: "取消", style: .cancel) { (action) in
            alertController.dismiss(animated: true) {
                
            }
        }
        alertController.addAction(actionTop)
        alertController.addAction(actionBottom)
        alertController.addAction(cancel)
        
        
        vc.present(alertController, animated: true, completion: nil)
    }
    
    /// 相机 视频
    func imagePickerPresent() {
        
        imagePickerVC.delegate = self
        if UIImagePickerController.isSourceTypeAvailable(.camera){
            imagePickerVC.sourceType = .camera
        }else{
            NeatHud.showLoading(view: self.view, message: "相机不可用")
            return
        }
        if UIImagePickerController.isCameraDeviceAvailable(.rear){
            imagePickerVC.cameraDevice = .rear
            
        }else{
            NeatHud.showLoading(view: self.view, message: "后摄像头不可用")
            return
        }
        
        imagePickerVC.cameraFlashMode = .auto
        
        self.showActionSheet(vc: self, title: "请选择附件类型", message: "", topTitle: "图片", bottomTitle: "视频", topAction: { (action) in
            
            self.imagePickerVC.mediaTypes = [kUTTypeImage as String]
            self.imagePickerVC.cameraCaptureMode = .photo
            self.present(self.imagePickerVC, animated: true) {
                
            }
            
        }) { (action) in
            
            if (UIImagePickerController.availableMediaTypes(for: .camera)?.contains(kUTTypeMovie as String)) ?? false{
                //可以录视频
                self.imagePickerVC.mediaTypes = [kUTTypeMovie as String]
                self.imagePickerVC.cameraCaptureMode = .video
                self.imagePickerVC.videoMaximumDuration = 30
                self.present(self.imagePickerVC, animated: true) {
                    
                }
            }else{
                NeatHud.showLoading(view: self.view, message: "相机不可用")
                return
            }
            
        }
    }
    
    func didSelectAnnex(annexModel:AnnexModel) {
        
    }
    
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension SupperViewController:UIImagePickerControllerDelegate{
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        
        picker.dismiss(animated: true) {
            
        }
        
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        let mediaType = info[.mediaType] as! String
        
        if mediaType == "public.movie" {
            
            let videoUrl = info[.mediaURL]
            
            let videoImage = VideoFileHandleTool.getVideoThum(url: videoUrl as! URL)

            //创建新的附件model
            let annexModel = AnnexModel()
            annexModel.annexType = 2
            annexModel.isNetSource = false
            annexModel.imageSource = videoImage
            annexModel.sourceFile = videoUrl as! URL
            
            self.didSelectAnnex(annexModel: annexModel)
            
            
        }else{
            
            
            
            
            let image = info[.originalImage] as? UIImage
            
            let fixImage = image?.fixOrientation()
           
            //创建新的附件model
            let annexModel = AnnexModel()
            annexModel.annexType = 1
            annexModel.isNetSource = false
            annexModel.imageSource = fixImage
            
            self.didSelectAnnex(annexModel: annexModel)
            
        }
        
        picker.dismiss(animated: true) {
            
        }
        
    }
    
}

extension SupperViewController:UINavigationControllerDelegate{
    
}
