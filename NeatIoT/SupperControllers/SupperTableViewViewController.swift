//
//  SupperTableViewViewController.swift
//  NeatIoT
//
//  Created by neat on 2019/12/23.
//  Copyright © 2019 Neat. All rights reserved.
//

import UIKit

class SupperTableViewViewController: SupperViewController {
    
    public lazy var listContainView: UITableView = {
        
        let containView = UITableView.init()
        containView.backgroundColor = UIColor.clear
        containView.separatorStyle = .none
    
        let mj_h = MJRefreshNormalHeader.init(refreshingTarget: self, refreshingAction: #selector(refreshAction))
        mj_h.lastUpdatedTimeLabel!.isHidden = true
        containView.mj_header = mj_h
        
        let mj_f = MJRefreshAutoNormalFooter.init(refreshingTarget: self, refreshingAction: #selector(loadMoreAction))
        mj_f.isAutomaticallyRefresh = false
        containView.mj_footer = mj_f
        
        return containView
    }()
    

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @objc func refreshAction() {
        
       
    }
    @objc func loadMoreAction() {
        
        
    }
    
    func endRefresh(isUpdateFooter:Bool?,dataCount:Int,pageSize:Int) {
        
        self.listContainView.mj_header!.endRefreshing()
        
        if isUpdateFooter ?? false {
            if dataCount < pageSize {
                //没有更多数据 隐藏footer
                self.listContainView.mj_footer!.isHidden = true
            }else{
                //取消隐藏footer
                self.listContainView.mj_footer!.isHidden = false
                self.listContainView.mj_footer!.resetNoMoreData()
            }
        }
        
    }
    
    func endLoadMoreData(dataCount:Int,pageSize:Int){
        
        if dataCount < pageSize && dataCount > 0 {
            //没有更多数据
            self.listContainView.mj_footer!.endRefreshingWithNoMoreData()
            self.listContainView.mj_footer!.isHidden = true
        }else if dataCount == 0{
            //没有更多数据
            self.listContainView.mj_footer!.endRefreshingWithNoMoreData()
            self.listContainView.mj_footer!.isHidden = true
        }else{
            self.listContainView.mj_footer!.resetNoMoreData()
            self.listContainView.mj_footer!.isHidden = false
        }
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
