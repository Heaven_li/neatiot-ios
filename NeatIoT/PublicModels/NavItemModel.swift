//
//  NavItemModel.swift
//  NeatIoT
//
//  Created by neat on 2019/10/9.
//  Copyright © 2019 Neat. All rights reserved.
//

import UIKit

class NavItemModel: NSObject {
    
    /// 导航按钮标题
    var itemText: String = String()
    
    /// 按钮图标
    var itemIcon: String = String()

}
