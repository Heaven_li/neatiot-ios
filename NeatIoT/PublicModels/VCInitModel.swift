//
//  AddVCInitModel.swift
//  NeatIoT
//
//  Created by neat on 2019/11/14.
//  Copyright © 2019 Neat. All rights reserved.
//

import UIKit

/// 初始化controller  标题及类型 model
class VCInitModel: NSObject {
    
    /// 父设备菜单model
    var gatewayMenuModel:MenuModel?
    
    /// 设备类型 设备管理中使用
    var deviceCategory:String = ""
    
    /// 导航标题
    var navTitleText:String = ""

}
