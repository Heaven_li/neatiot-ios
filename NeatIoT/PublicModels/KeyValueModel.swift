//
//  KeyValueModel.swift
//  NeatIoT
//
//  Created by neat on 2019/11/1.
//  Copyright © 2019 Neat. All rights reserved.
//

import UIKit
import HandyJSON


/// id name model
class KeyValueModel: HandyJSON {
    
    
    /// ID
    var item_id = ""
    /// 名称
    var item_name = ""
    
    required init() {
        
    }
    
    func mapping(mapper: HelpingMapper) {
        
        mapper <<<
        self.item_id <-- "id"
        mapper <<<
        self.item_name <-- "name"
        
    }
    

}
