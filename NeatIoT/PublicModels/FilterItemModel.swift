//
//  FilterItemModel.swift
//  NeatIoT
//
//  Created by neat on 2019/11/4.
//  Copyright © 2019 Neat. All rights reserved.
//

import UIKit

enum FilterType {
    case DomainId
    case EntId
    case BuildingId
    case KeypartId
    case Frequency
    case FinishState
    case EventType
    case AppendTime
}


/// 筛选项model
class FilterItemModel: NSObject {
    
    /// 筛选项 标题
    var filterTitle:String = ""
    
    /// 筛选项 值
    var filterInfor:String = ""
    
    /// 筛选项 值对应的id
    var filterInforId:String = ""
    
    /// 筛选项 类别
    var filterType:FilterType = FilterType.DomainId
    
    /// 父级id
    var parentId:String = ""
    
    /// 筛选项index
    var filterIndex:Int = 0
    
    /// 是否可编辑 默认true
    var isEnable:Bool = true
    
}
