//
//  TitleInforModel.swift
//  NeatIoT
//
//  Created by neat on 2019/12/18.
//  Copyright © 2019 Neat. All rights reserved.
//

import UIKit

class TitleInforModel: NSObject {
    
    /// 标题
    var titleText:String?
    
    /// 内容
    var inforText:String?

}
