//
//  QureyStatsMainViewController.swift
//  NeatIoT
//
//  Created by Yang Blus on 2020/7/2.
//  Copyright © 2020 Neat. All rights reserved.
//

import UIKit

class QureyStatsMainViewController: SupperViewController {
    
    var menuModel:MenuModel?
    
    let FUNCTIONCELLIDENTIFIER = "FUNCTIONCELLIDENTIFIER"
    
    lazy var containCollectionView: LZCollectionView = {
        
        let flowLayout = UICollectionViewFlowLayout.init()
        flowLayout.scrollDirection = .vertical
        flowLayout.minimumLineSpacing = 1
        flowLayout.minimumInteritemSpacing = 1
        
        let collectionView = LZCollectionView.init(frame: CGRect.zero, collectionViewLayout: flowLayout)
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.backgroundColor = NormalColor.viewControllerDefaultBgColor
        
        collectionView.register(UINib.init(nibName: "HomeItemCollectionViewCell", bundle: Bundle.main), forCellWithReuseIdentifier: self.FUNCTIONCELLIDENTIFIER)
        
        return collectionView
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.titleName = self.menuModel?.name ?? "设备查询"
        
        self.view.addSubview(self.containCollectionView);
        
        
        self.containCollectionView.snp.makeConstraints { (make) in
            make.edges.equalTo(self.view)
        }

        
    }
    

}

extension QureyStatsMainViewController:UICollectionViewDelegate{
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let deviceMainVC = QureyStatsGatewaysViewController.init()
        deviceMainVC.menuModel = menuModel?.children[indexPath.item]
        self.navigationController?.pushViewController(deviceMainVC, animated: true)
        
    }
    
    
}

extension QureyStatsMainViewController:UICollectionViewDataSource{
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.menuModel?.children.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        
        let item = collectionView.dequeueReusableCell(withReuseIdentifier: FUNCTIONCELLIDENTIFIER, for: indexPath) as! HomeItemCollectionViewCell

        item.configerItem(menuModel: self.menuModel?.children[indexPath.item] ?? MenuModel.init())
        
        return item
            
        
        
    }
    
}
extension QureyStatsMainViewController: UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize.init(width:(NormalConstant.ScreenWidth-2)/3.0, height: (NormalConstant.ScreenWidth-2)/3.0)
    }
    

    
}

