//
//  QureyStatsGatewayTableViewCell.swift
//  NeatIoT
//
//  Created by Yang Blus on 2020/7/2.
//  Copyright © 2020 Neat. All rights reserved.
//

import UIKit

protocol QureyStatsGatewayTableViewCellDelegate {
    func leftEventTap(model:QureyStatsGatewayItemModel)
    func midEventTap(model:QureyStatsGatewayItemModel)
    func rightEventTap(model:QureyStatsGatewayItemModel)
}

class QureyStatsGatewayTableViewCell: UITableViewCell {
    
    @IBOutlet weak var bgView: UIView!
    
    @IBOutlet weak var onLineStateBgView: UIView!
    
    @IBOutlet weak var onLineStateLable: UILabel!
    
    @IBOutlet weak var mainTitleLable: UILabel!
    
    @IBOutlet weak var subTitleLable: UILabel!
    
    @IBOutlet weak var secSubTitleLable: UILabel!
    
    @IBOutlet weak var leftEventView: UIView!
    
    @IBOutlet weak var midEventView: UIView!
    
    @IBOutlet weak var rightEventView: UIView!
    
    var delegate:QureyStatsGatewayTableViewCellDelegate?
    
    var currentGatewayItemModel:QureyStatsGatewayItemModel?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.backgroundColor = NormalColor.viewControllerDefaultBgColor
        
        bgView.layer.cornerRadius = 4
        bgView.clipsToBounds = true
       
        onLineStateBgView.layer.cornerRadius = 4
        onLineStateBgView.clipsToBounds = true
        
        let leftTap = UITapGestureRecognizer.init(target: self, action: #selector(tapEvent(gesture:)))
        let midTap = UITapGestureRecognizer.init(target: self, action: #selector(tapEvent(gesture:)))
        let rightTap = UITapGestureRecognizer.init(target: self, action: #selector(tapEvent(gesture:)))
        
        leftEventView.isUserInteractionEnabled = true
        midEventView.isUserInteractionEnabled = true
        rightEventView.isUserInteractionEnabled = true
        
        leftEventView.addGestureRecognizer(leftTap)
        midEventView.addGestureRecognizer(midTap)
        rightEventView.addGestureRecognizer(rightTap)
        
        leftEventView.tag = 1;
        midEventView.tag = 2;
        rightEventView.tag = 3;
        
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @objc func tapEvent(gesture:UITapGestureRecognizer) {
        
        let viewTag = gesture.view?.tag
        
        if viewTag == 1 {
            debugPrint("Gis位置")
            self.delegate?.leftEventTap(model: currentGatewayItemModel!)
        }else if viewTag == 2 {
            debugPrint("事件列表")
            self.delegate?.midEventTap(model: currentGatewayItemModel!)
        }else if viewTag == 3 {
            debugPrint("实时监控")
            self.delegate?.rightEventTap(model: currentGatewayItemModel!)
        }
        
    }
    
    func configerCell(model:QureyStatsGatewayItemModel) {
        
        currentGatewayItemModel = model
        
        self.onLineStateBgView.backgroundColor = model.status == 1 ? #colorLiteral(red: 0.6, green: 0.6, blue: 0.6, alpha: 1):#colorLiteral(red: 0.3137254902, green: 0.8392156863, blue: 0.3882352941, alpha: 1)
        self.onLineStateLable.text = model.status == 1 ? "离线":"在线"
        self.mainTitleLable.text = model.name
        self.subTitleLable.text = "设备编码:" + (model.code ?? "")
        self.secSubTitleLable.text = "安装位置:" + (model.address ?? "")
        
    }
    
}
