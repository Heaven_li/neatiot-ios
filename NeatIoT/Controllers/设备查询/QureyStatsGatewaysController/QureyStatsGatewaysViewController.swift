//
//  QureyStatsGatewaysViewController.swift
//  NeatIoT
//
//  Created by Yang Blus on 2020/7/2.
//  Copyright © 2020 Neat. All rights reserved.
//

import UIKit

class QureyStatsGatewaysViewController: SupperTableViewViewController {
    
    let QureyStatsGatewayTableViewCellIdent = "QureyStatsGatewayTableViewCell"
    
    var menuModel:MenuModel?
    
    /// 搜索关键词
    var keywords:String = ""
    /// 筛选中心id
    var domainId:String = ""
    /// 筛选企业id
    var enterpriseId:String = ""
    /// 筛选建筑id
    var buildingId:String = ""
    /// 筛选部位id
    var keypartId:String = ""
    /// 当前页
    var pageIndex:Int = 1
    /// 当前编辑的筛选项目
    var currentFilterType = FilterType.DomainId
    /// 当前筛选项目数组
    var currentFilterArr:Array<FilterItemModel> = []
    /// 当前编辑的筛选item
    var currentFilterModel:FilterItemModel = FilterItemModel()
    /// 数据源
    var dataSource:Array<QureyStatsGatewayItemModel> = []
    
    lazy var searchBar: TopSearchView = {
        let topSearchView = TopSearchView.init()
        topSearchView.delegate = self
        return topSearchView
    }()
    
    lazy var filterView: FilterView = {
        
        let filterView = FilterView.init()
        filterView.delegate = self
        filterView.dataSource = FilterViewModel.initPartFilterData()
        
        return filterView
    }()
    
    lazy var inforSelectView: InforSelectView = {
        let inforSelectView = InforSelectView.init()
        inforSelectView.delegate = self
        return inforSelectView
    }()

    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        self.titleName = self.menuModel?.name ?? ""

        let filterItem = NavItemModel.init()
        filterItem.itemIcon = "nav_filter"
        
        self.rightItems = [filterItem]
        
        self.configerUI()
        
        getGateways()
        
    }
    
    override func navRRClick() {
        
        if self.filterView.isShow {
            self.filterView.dissmisFilterView()
        }else{
            self.filterView.showFilterView(view: self.view)
        }
        
    }
    
    override func refreshAction() {
        
        pageIndex = 1
        
        NeatRequest.instance.getQureyStatsGatewayList(url: UrlHost.QureyStats.getQureyStatsGatewayList, keyword: keywords, domainId: domainId, enterpriseId: enterpriseId, buildingId: buildingId, keypartId: keypartId, orderByColumn: "9", isDescOrder: true, pageIndex: pageIndex, success: { (response) in
            
            self.handleResponseData(response: response,isRefresh: true,isLoadMore: false)
            
            
        }) { (errorResponse) in
            
            NeatHud.showMessage(message: errorResponse.errMessage, view: self.view)

        }
        
    }
    
    override func loadMoreAction() {
        
        pageIndex += 1
        
        NeatRequest.instance.getQureyStatsGatewayList(url: UrlHost.QureyStats.getQureyStatsGatewayList, keyword: keywords, domainId: domainId, enterpriseId: enterpriseId, buildingId: buildingId, keypartId: keypartId, orderByColumn: "9", isDescOrder: true, pageIndex: pageIndex, success: { (response) in
            
            self.handleResponseData(response: response,isRefresh: false,isLoadMore: true)
            
            
        }) { (errorResponse) in
            
            NeatHud.showMessage(message: errorResponse.errMessage, view: self.view)

        }
        
        
        
    }
    
    func configerUI() {
        
        self.listContainView.delegate = self;
        self.listContainView.dataSource = self;
        self.listContainView.register(UINib.init(nibName: "QureyStatsGatewayTableViewCell", bundle: Bundle.main), forCellReuseIdentifier: QureyStatsGatewayTableViewCellIdent)
        
        
        self.view.addSubview(self.searchBar)
        self.searchBar.snp.makeConstraints { (make) in
            make.top.equalTo(self.view)
            make.leading.trailing.equalTo(self.view)
            make.height.equalTo(NormalConstant.TopSearchViewHeight)
        }
        self.view.addSubview(self.listContainView)
        self.listContainView.snp.makeConstraints { (make) in
            make.top.equalTo(self.searchBar.snp.bottom)
            make.leading.trailing.equalTo(self.view)
            make.bottom.equalTo(self.view)
        }
    }

    func getGateways() {
        
        self.listContainView.showLoadingView()
        
        NeatRequest.instance.getQureyStatsGatewayList(url: UrlHost.QureyStats.getQureyStatsGatewayList, keyword: keywords, domainId: domainId, enterpriseId: enterpriseId, buildingId: buildingId, keypartId: keypartId, orderByColumn: "9", isDescOrder: true, pageIndex: pageIndex, success: { (response) in
            
            self.listContainView.hidenHolderView()
            
            self.handleResponseData(response: response,isRefresh: false,isLoadMore: false)
            
            
        }) { (errorResponse) in
            
            if errorResponse.errCode == RequestResponseCode.dataParsingException {
                //数据异常
                NeatHud.dissmissHud(view: self.view)
                self.listContainView.showRequestFaildView {
                    
                }
            }else if errorResponse.errCode == RequestResponseCode.netCanNotContent || errorResponse.errCode == RequestResponseCode.requestTimeOut || errorResponse.errCode == RequestResponseCode.canNotContentHost{
                //无网络
                NeatHud.dissmissHud(view: self.view)
                self.listContainView.showNetErrorView(errorMessage: errorResponse.errMessage) {
                    self.getGateways()
                }
            }else{
                NeatHud.showMessage(message: errorResponse.errMessage, view: self.view)
            }
            
            
        }
        
    }
    
    func handleResponseData(response:QureyStatsGatewayListResponse,isRefresh:Bool,isLoadMore:Bool) {
        
        if isRefresh {
            
            self.listContainView.mj_header!.endRefreshing()
            self.listContainView.mj_footer!.resetNoMoreData()
            
            if (response.qureyListResponse?.data?.count ?? 0) < 10 && (response.qureyListResponse?.data?.count ?? 0) > 0 {
                self.listContainView.mj_footer!.endRefreshingWithNoMoreData()
            }else if (response.qureyListResponse?.totalCount == 0) {
                self.listContainView.showEmptyView()
            }
            
            dataSource.removeAll()
            dataSource.append(contentsOf: response.qureyListResponse?.data ?? [])
            
            
        }else if isLoadMore {
            
            self.listContainView.mj_footer!.endRefreshing()
            if (response.qureyListResponse?.data?.count ?? 0) < 10 && (response.qureyListResponse?.data?.count ?? 0) > 0 {
                self.listContainView.mj_footer!.endRefreshingWithNoMoreData()
            }else if response.qureyListResponse?.totalCount == 0 {
                pageIndex -= 1
                self.listContainView.mj_footer!.endRefreshingWithNoMoreData()
            }
            
            dataSource.append(contentsOf: response.qureyListResponse?.data ?? [])
            
            
        }else{
            
            dataSource.removeAll()
            self.listContainView.mj_footer!.resetNoMoreData()
            
            if (response.qureyListResponse?.data?.count ?? 0) < 10 && (response.qureyListResponse?.data?.count ?? 0) > 0 {
                self.listContainView.mj_footer!.endRefreshingWithNoMoreData()
            }else if (response.qureyListResponse?.totalCount == 0) {
                self.listContainView.showEmptyView()
            }
            
            dataSource.append(contentsOf: response.qureyListResponse?.data ?? [])
            
        }
        
        self.listContainView.reloadData()
        
        
    }
    
    /// 更新筛选项
    func updateFilter(filterModel:FilterItemModel) -> Bool {
        
        if filterModel.filterType != .DomainId {
            if filterModel.parentId == "" {
                var headerStr = "请先选择"
                if filterModel.filterType == .EntId {
                    headerStr = "请先选择中心"
                }else if filterModel.filterType == .BuildingId{
                    headerStr = "请先选择企业"
                }else if filterModel.filterType == .KeypartId{
                    headerStr = "请先选择建筑"
                }
                NeatHud.showMessage(message: headerStr, view: self.view)
                return false
            }else{
                return true
            }
        }else{
            return true
        }
    }
    
    /// 更新筛选UI数据源
    /// - Parameter filterModel: 筛选项model
    /// - Parameter valueModel: 选中valuemodel
    func reFormateData(filterModel: FilterItemModel,valueModel: KeyValueModel) {
        
        let filterDataSourceArr = self.filterView.dataSource
        
        //判断之前是否已经设置 做之前设置信息变更
        if filterModel.filterInforId != "" {
            //之前有设置
            if filterModel.filterInforId != valueModel.item_id {
                //变更 后续选项变更
                for index in filterModel.filterIndex..<filterDataSourceArr.count {
                    let model = filterDataSourceArr[index]
                    model.filterInfor = "全部"
                    model.filterInforId = ""
                }
            }
        }
        
        ///赋值当前筛选项
        let ItemModel = filterDataSourceArr[filterModel.filterIndex]
        ItemModel.filterInfor = valueModel.item_name
        ItemModel.filterInforId = valueModel.item_id
        
        ///更新下一个筛选项的parentid
        if filterModel.filterIndex < 3 {
            let nextModel = filterDataSourceArr[filterModel.filterIndex+1]
            nextModel.parentId = valueModel.item_id
        }
        
        
    }

}

extension QureyStatsGatewaysViewController:TopSearchViewDelegate{
    
    func doSearchWith(keyword: String) {
        self.keywords = keyword
        self.getGateways()
    }
    
}

extension QureyStatsGatewaysViewController:UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 137
    }
}

extension QureyStatsGatewaysViewController:UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: QureyStatsGatewayTableViewCellIdent, for: indexPath) as! QureyStatsGatewayTableViewCell
        cell.delegate = self
        cell.selectionStyle = .none
        cell.configerCell(model: dataSource[indexPath.row])
        return cell
    }
    
    
    
}

extension QureyStatsGatewaysViewController:QureyStatsGatewayTableViewCellDelegate{
    
    func leftEventTap(model: QureyStatsGatewayItemModel) {
        
        let vc = QureyStatsGISViewController.init()
        vc.menuModel = menuModel
        vc.gatewayModel = model
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func midEventTap(model: QureyStatsGatewayItemModel) {
        let vc = QureyStatsEventViewController.init()
        vc.menuModel = menuModel
        vc.gatewayModel = model
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func rightEventTap(model: QureyStatsGatewayItemModel) {
        let vc = QureyStatsChanlesViewController.init()
        vc.menuModel = menuModel
        vc.gatewayModel = model
        self.navigationController?.pushViewController(vc, animated: true)
    }

}

///筛选框 代理方法
extension QureyStatsGatewaysViewController:FilterViewDelegate{
    
    func selectItem(model: FilterItemModel) {
        
        //更新当前编辑的筛选项
        currentFilterModel = model
        
        let showSelectView = self.updateFilter(filterModel: model)
        if showSelectView {
            currentFilterType = model.filterType
            let configerModel = InforSelectConfigerModel.init()
            configerModel.isOccurFromFilter = true
            configerModel.inforTitle = model.filterTitle
            if model.filterType == .DomainId {
                configerModel.inforType = DeviceInforType.domain
            }else if model.filterType == .EntId{
                configerModel.inforType = DeviceInforType.enterprise
                configerModel.apiParamet = model.parentId
            }else if model.filterType == .BuildingId{
                configerModel.inforType = DeviceInforType.building
                configerModel.apiParamet = model.parentId
            }else if model.filterType == .KeypartId{
                configerModel.inforType = DeviceInforType.keypart
                configerModel.apiParamet = model.parentId
            }
            
            self.inforSelectView.showSelectView(view: self.view,model: configerModel)
        }
        
    }
    
    func confirmFilter(modelArr: Array<FilterItemModel>) {
        
        for model in modelArr {
            if model.filterType == .DomainId {
                self.domainId = model.filterInforId
            }else if model.filterType == .EntId{
                self.enterpriseId = model.filterInforId
            }else if model.filterType == .BuildingId{
                self.buildingId = model.filterInforId
            }else if model.filterType == .KeypartId{
                self.keypartId = model.filterInforId
            }
        }
        
        self.getGateways()
        
        self.filterView.dissmisFilterView()
        
    }
    
    func cancelFilter() {
        self.filterView.dissmisFilterView()
    }
    
}

///选择框 代理方法
extension QureyStatsGatewaysViewController:InforSelectViewDelegate{
    
    func selectInfor(model: KeyValueModel) {
        
        self.reFormateData(filterModel: currentFilterModel, valueModel: model)

        currentFilterArr = self.filterView.dataSource
        
        self.filterView.updateUI()
        
    }
    
}
