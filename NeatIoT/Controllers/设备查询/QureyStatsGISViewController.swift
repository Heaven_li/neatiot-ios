//
//  QureyStatsGISViewController.swift
//  NeatIoT
//
//  Created by Yang Blus on 2020/7/2.
//  Copyright © 2020 Neat. All rights reserved.
//

import UIKit
import MAMapKit

class QureyStatsGISViewController: SupperViewController {
    
    var menuModel:MenuModel?
    var gatewayModel:QureyStatsGatewayItemModel?
    
    lazy var backBtn: UIButton = {
        let backBtn = UIButton.init(type: .custom)
        backBtn.setImage(UIImage.init(named: "gis_back"), for: .normal)
        backBtn.addTarget(self, action: #selector(backBtnClick), for: .touchUpInside)
        return backBtn
    }()
    
    lazy var mapView: MAMapView = {
        let mapView = MAMapView.init(frame: self.view.bounds)
        mapView.delegate = self
        mapView.showsCompass = false
        mapView.userTrackingMode = .none
        mapView.maxZoomLevel = 18
        mapView.minZoomLevel = 15
        return mapView
    }()
    
    
    
    override func viewWillAppear(_ animated: Bool) {
            
        self.navigationController?.navigationBar.isHidden = true

    }
        
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = false
    }
    
    
    override var preferredStatusBarStyle: UIStatusBarStyle{
        .default
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        self.titleName = menuModel?.name ?? ""
        
        AMapServices.shared()?.enableHTTPS = true
        
        self.view.addSubview(self.mapView)
        self.mapView.snp.makeConstraints { (make) in
            make.edges.equalTo(self.view)
        }
        
        self.view.addSubview(self.backBtn)

        if NormalConstant.ISLHP {
            self.backBtn.snp.makeConstraints { (make) in
                make.leading.equalTo(self.view).offset(20)
                make.top.equalTo(self.view).offset(30+20)
            }

        }else{
            self.backBtn.snp.makeConstraints { (make) in
                make.leading.equalTo(self.view).offset(20)
                make.top.equalTo(self.view).offset(30)
            }
        }
        
        getGisData()
        
    }
    
    
    func getGisData() {
        
        NeatRequest.instance.getQureyStatsGatewayGis(gatewayId: (gatewayModel?.gatewayId)!, success: { (response) in
            
            self.handlePoint(model: response)
            
        }) { (error) in
            
            NeatHud.showMessage(message: error.errMessage, view: self.view)
        }
        
    }
    
    func handlePoint(model:QureyStatsGISResponse) {
        
        if model.gisModel?.latitude != 0 && model.gisModel?.longitude != 0{
            let pointAnnotation = MAPointAnnotation()
            
            let center = CLLocationCoordinate2D(latitude: model.gisModel!.latitude!, longitude: model.gisModel!.longitude!)
                 
             pointAnnotation.coordinate = center
             mapView.setCenter(center, animated: true)
             
             mapView.addAnnotation(pointAnnotation)
        }else{
            NeatHud.showMessage(message: "该设备没有找到位置信息", view: self.view)
        }
        
    }
    
    @objc func backBtnClick() {
        self.navigationController?.popViewController(animated: true)
    }

}
extension QureyStatsGISViewController:MAMapViewDelegate{
    
    func mapView(_ mapView: MAMapView!, viewFor annotation: MAAnnotation!) -> MAAnnotationView! {
        
//        注意：5.1.0后由于定位蓝点增加了平滑移动功能，如果在开启定位的情况先添加annotation，需要在此回调方法中判断annotation是否为MAUserLocation，从而返回正确的View。
        if (annotation.isKind(of: MAUserLocation.self)) {
           return nil;
        }else if annotation.isKind(of: MAPointAnnotation.self) {
            let pointReuseIndetifier = "pointReuseIndetifier"
            var annotationView: MAAnnotationView? = mapView.dequeueReusableAnnotationView(withIdentifier: pointReuseIndetifier)
            
            if annotationView == nil {
                annotationView = MAAnnotationView(annotation: annotation, reuseIdentifier: pointReuseIndetifier)
            }
            
            annotationView!.image = UIImage(named: "mark_point")
            //设置中心点偏移，使得标注底部中间点成为经纬度对应点
            annotationView!.centerOffset = CGPoint.init(x: 0, y: -18);
            
            return annotationView!
        }
        return nil
    }
}
