//
//  QureyStatsChannelTableViewCell.swift
//  NeatIoT
//
//  Created by Yang Blus on 2020/7/3.
//  Copyright © 2020 Neat. All rights reserved.
//

import UIKit

protocol QureyStatsChannelTableViewCellDelegate {
    func clickFunction(model:QureyStatsChannelItemModel)
}

class QureyStatsChannelTableViewCell: UITableViewCell {
    
    @IBOutlet weak var mainLable: UILabel!
    
    @IBOutlet weak var subLable: UILabel!
    
    @IBOutlet weak var secSubLable: UILabel!
    
    @IBOutlet weak var functionBtn: UIButton!
    
    var curretnModel:QureyStatsChannelItemModel?
    
    var delegate:QureyStatsChannelTableViewCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configerCell(model:QureyStatsChannelItemModel) {
        
        self.mainLable.text = model.channelName
        
        let subLableText = "当前实时值" + (model.currentValue ?? "") + (model.unit ?? "")
        self.subLable.text = subLableText
        
        let secSubLableText = "更新时间" + (model.updateTime ?? "")
        self.secSubLable.text = secSubLableText
        
        curretnModel = model
        
        
    }
    
    @IBAction func functionBtnClick(_ sender: Any) {
        
        self.delegate?.clickFunction(model: curretnModel!)
        
    }
    
    
}
