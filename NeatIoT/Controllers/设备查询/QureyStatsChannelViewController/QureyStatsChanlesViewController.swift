//
//  QureyStatsChanlesViewController.swift
//  NeatIoT
//
//  Created by Yang Blus on 2020/7/2.
//  Copyright © 2020 Neat. All rights reserved.
//

import UIKit

class QureyStatsChanlesViewController: SupperTableViewViewController {
    
    let QureyStatsChannelTableViewCellIdent = "QureyStatsChannelTableViewCell"
    
    var menuModel:MenuModel?
    var gatewayModel:QureyStatsGatewayItemModel?
    
    var dataSource:Array<QureyStatsChannelItemModel> = []

    override func viewDidLoad() {
        super.viewDidLoad()

        self.titleName = menuModel?.name ?? ""
        
        self.listContainView.register(UINib.init(nibName: "QureyStatsChannelTableViewCell", bundle: Bundle.main), forCellReuseIdentifier: QureyStatsChannelTableViewCellIdent)
        self.listContainView.delegate = self
        self.listContainView.dataSource = self
        self.listContainView.mj_footer = nil
        self.listContainView.mj_header = nil
        self.listContainView.separatorStyle = .singleLine
        self.listContainView.separatorColor = NormalColor.viewControllerDefaultBgColor
        self.listContainView.separatorInset = UIEdgeInsets.init(top: 0, left: 10, bottom: 0, right: 10)
        self.view.addSubview(self.listContainView)
        self.listContainView.snp.makeConstraints { (make) in
            make.edges.equalTo(self.view)
        }
        
        
        getChannelData()
    }
    

    func getChannelData() {
        
        NeatHud.showLoading(view: self.view, message: "")
        NeatRequest.instance.getQureyStatsGatewayChannelList(gatewayId: (self.gatewayModel?.gatewayId)!, success: { (response) in
            
            NeatHud.dissmissHud(view: self.view)
            self.handleResponseData(response: response)
            
        }) { (errorResponse) in
            
            if errorResponse.errCode == RequestResponseCode.dataParsingException {
                //数据异常
                NeatHud.dissmissHud(view: self.view)
                self.listContainView.showRequestFaildView {
                    
                }
            }else if errorResponse.errCode == RequestResponseCode.netCanNotContent || errorResponse.errCode == RequestResponseCode.requestTimeOut || errorResponse.errCode == RequestResponseCode.canNotContentHost{
                //无网络
                NeatHud.dissmissHud(view: self.view)
                self.listContainView.showNetErrorView(errorMessage: errorResponse.errMessage) {
                    self.getChannelData()
                }
            }else{
                NeatHud.showMessage(message: errorResponse.errMessage, view: self.view)
            }
            
        }
        
    }
    
    func handleResponseData(response:QureyStatsChannelResponse) {
        
        if response.channelArr?.count == 0 {
            self.listContainView.showEmptyView()
        }else{
            dataSource.removeAll()
            dataSource.append(contentsOf: response.channelArr ?? [])
        }
        self.listContainView.reloadData()
    }
    

}

extension QureyStatsChanlesViewController:UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 96
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.1
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView.init()
    }
    
}

extension QureyStatsChanlesViewController:UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        dataSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: QureyStatsChannelTableViewCellIdent, for: indexPath) as! QureyStatsChannelTableViewCell
        
        cell.selectionStyle = .none
        cell.delegate = self
        cell.configerCell(model: dataSource[indexPath.row])
        
        return cell
    }

}

extension QureyStatsChanlesViewController:QureyStatsChannelTableViewCellDelegate{
    
    func clickFunction(model: QureyStatsChannelItemModel) {
        
        let vc = QureyStatsViewController.init()
        vc.channelModel = model
        vc.menuModel = self.menuModel
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
   
}
