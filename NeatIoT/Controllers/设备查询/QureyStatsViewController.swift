//
//  QureyStatsViewController.swift
//  NeatIoT
//
//  Created by Yang Blus on 2020/7/2.
//  Copyright © 2020 Neat. All rights reserved.
//

import UIKit

class QureyStatsViewController: SupperViewController {
    
    var menuModel:MenuModel?
    var channelModel:QureyStatsChannelItemModel?
    
    var currentFre:String = "8"
    
    var titleArr:Array<String> = []
    
    var valueArr:Array<Double> = []
    
    lazy var aaChartView:   AAChartView = {
        let aaView = AAChartView.init()
        aaView.scrollEnabled = false
        return aaView
    }()
    
    lazy var unitLabel: UILabel = {
        let unitLabel = UILabel.init()
        unitLabel.text = "单位"
        unitLabel.backgroundColor = .white
        unitLabel.textColor = NormalColor.NormalFontColor
        unitLabel.font = UIFont.systemFont(ofSize: 14)
        return unitLabel
    }()
    
    lazy var freLable: UILabel = {
        let freLabel = UILabel.init()
        freLabel.text = "最近8个小时"
        freLabel.textAlignment = .center
        freLabel.textColor = NormalColor.NormalFontColor
        freLabel.font = UIFont.systemFont(ofSize: 15)
        
        freLabel.isUserInteractionEnabled = true
        let tap = UITapGestureRecognizer.init(target: self, action: #selector(tapFreLabel))
        freLabel.addGestureRecognizer(tap)
        
        return freLabel
    }()
    
    lazy var inforSelectView: InforSelectView = {
        let inforSelectView = InforSelectView.init()
        inforSelectView.delegate = self
        return inforSelectView
    }()
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.titleName = self.menuModel?.name as! String
        
        self.unitLabel.text = "单位(" + (self.channelModel?.unit)! + ")"
        
        self.view.addSubview(self.freLable)
        
        self.view.addSubview(self.unitLabel)
        
        self.view.addSubview(aaChartView)
        
        self.freLable.snp.makeConstraints { (make) in
            make.top.equalTo(self.view)
            make.leading.trailing.equalTo(self.view)
            make.height.equalTo(45)
        }
        self.unitLabel.snp.makeConstraints { (make) in
            make.top.equalTo(self.freLable.snp.bottom)
            make.leading.trailing.equalTo(self.view)
            make.height.equalTo(24)
        }
        self.aaChartView.snp.makeConstraints { (make) in
            make.top.equalTo(self.unitLabel.snp.bottom)
            make.leading.trailing.bottom.equalTo(self.view)
        }
        
        getData()
        
    }
    
    @objc func tapFreLabel() {
        
        let configerModel = InforSelectConfigerModel.init()
        configerModel.isOccurFromFilter = false
        configerModel.inforTitle = "选择筛选时间"
        configerModel.inforType = DeviceInforType.frequency_query
        self.inforSelectView.showSelectView(view: self.view,model: configerModel)
        
    }
    
    private func customAATooltipWithJSFuntion() -> AAOptions {
            
            let linearGradientColor = AAGradientColor.linearGradient(direction: .toBottom, startColor: "rgba(249, 168, 170, 1.0)", endColor: "rgba(249, 168, 170, 0.1)")
            
            let aaChartModel = AAChartModel()
                .chartType(.area)//图形类型
                .title("")
                .yAxisTitle("")
                .legendEnabled(false)
                .markerSymbolStyle(.borderBlank)//折线连接点样式为外边缘空白
                .dataLabelsEnabled(false)
                .scrollablePlotArea(
                    
                    AAScrollablePlotArea().minWidth(600).scrollPositionX(1)
            
                )
                .categories(titleArr)
                .series([
                    AASeriesElement()
                        .name("")
                        .lineWidth(3)
                        .color(AAColor.rgbaColor(249, 168, 170, 1))
                        .fillColor(linearGradientColor)
                        .fillOpacity(0.5)
                        .data(valueArr)
                    ,
                ])
            
            let aaTooltip = AATooltip()
                .useHTML(true)
                .formatter("""
    function () {
            return this.y
            }
    """)
                .valueDecimals(2)//设置取值精确到小数点后几位//设置取值精确到小数点后几位
                .backgroundColor("#f9999a")
                .borderColor("#f9999a")
                .style(AAStyle()
                    .color("#FFFFFF")
                    .fontSize(12)
            )
            
            
            let aaOptions = AAOptionsConstructor.configureChartOptions(aaChartModel)
            aaOptions.tooltip(aaTooltip)
            
            return aaOptions
    }
    
    
    func getData() {
        
        NeatHud.showLoading(view: self.view, message: "")
        
        NeatRequest.instance.getQureyStatsData(channelId:(self.channelModel?.id)!,hour:"8", success: { (response) in
            
            NeatHud.dissmissHud(view: self.view)
            self.hanlderData(resopnse: response)
            
        }) { (error) in

            NeatHud.showMessage(message: error.errMessage, view: self.view)
        }

        
    }
    
    func hanlderData(resopnse:QureyStatsResponse) {
        
        self.titleArr.removeAll()
        self.valueArr.removeAll()
        
        for model in resopnse.dataArr ?? [] {
            
            self.titleArr.append(model.time!)
            self.valueArr.append(model.value!)
            
        }
        
        let aaOptions = customAATooltipWithJSFuntion()
        
        aaChartView.aa_drawChartWithChartOptions(aaOptions)
    }

}
///选择框 代理方法
extension QureyStatsViewController:InforSelectViewDelegate{
    
    func selectInfor(model: KeyValueModel) {
        
        currentFre = model.item_id
        self.freLable.text = model.item_name
        self.getData()
        
    }
    
}
