//
//  QureyStatsEventTableViewCell.swift
//  NeatIoT
//
//  Created by Yang Blus on 2020/7/3.
//  Copyright © 2020 Neat. All rights reserved.
//

import UIKit

class QureyStatsEventTableViewCell: UITableViewCell {
    
    
    @IBOutlet weak var eventTypeLable: UILabel!
    
    @IBOutlet weak var eventContentLable: UILabel!
    
    @IBOutlet weak var appendTimeLable: UILabel!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configerCell(model:QureyStatsEventItemModel) {
        
    
        /// 火警  = 1,故障= 2,状态 = 3,操作 = 4,报警= 5
        if model.eventType == -1 {
            
            self.backgroundColor = NormalColor.viewControllerDefaultBgColor
            self.eventTypeLable.text = "事件类型"
            self.eventContentLable.text = "内容"
            self.appendTimeLable.text = "发生时间"
            self.eventTypeLable.textColor = NormalColor.NormalFontColor
            self.eventContentLable.textColor = NormalColor.NormalFontColor
            self.appendTimeLable.textColor = NormalColor.NormalFontColor
            
            return
            
        }else if model.eventType == 1 {
            self.backgroundColor = .white
            self.eventTypeLable.text = "火警"
            self.eventContentLable.text = model.eventContent
            self.appendTimeLable.text = model.occurTime
            self.eventTypeLable.textColor = NormalColor.Fire
            self.eventContentLable.textColor = NormalColor.NormalFontColor
            self.appendTimeLable.textColor = NormalColor.NormalFontColor
            
        }else if model.eventType == 2 {
            self.backgroundColor = .white
            self.eventTypeLable.text = "故障"
            self.eventContentLable.text = model.eventContent
            self.appendTimeLable.text = model.occurTime
            self.eventTypeLable.textColor = NormalColor.Fault
            self.eventContentLable.textColor = NormalColor.NormalFontColor
            self.appendTimeLable.textColor = NormalColor.NormalFontColor
            
        }else if model.eventType == 3 {
            self.backgroundColor = .white
            self.eventTypeLable.text = "状态"
            self.eventContentLable.text = model.eventContent
            self.appendTimeLable.text = model.occurTime
            self.eventTypeLable.textColor = NormalColor.NormalFontColor
            self.eventContentLable.textColor = NormalColor.NormalFontColor
            self.appendTimeLable.textColor = NormalColor.NormalFontColor
            
        }else if model.eventType == 4 {
            self.backgroundColor = .white
            self.eventTypeLable.text = "操作"
            self.eventContentLable.text = model.eventContent
            self.appendTimeLable.text = model.occurTime
            self.eventTypeLable.textColor = NormalColor.NormalFontColor
            self.eventContentLable.textColor = NormalColor.NormalFontColor
            self.appendTimeLable.textColor = NormalColor.NormalFontColor
            
        }else if model.eventType == 5 {
            self.backgroundColor = .white
            self.eventTypeLable.text = "报警"
            self.eventContentLable.text = model.eventContent
            self.appendTimeLable.text = model.occurTime
            self.eventTypeLable.textColor = NormalColor.Alarm
            self.eventContentLable.textColor = NormalColor.NormalFontColor
            self.appendTimeLable.textColor = NormalColor.NormalFontColor
            
        }
        
        
    }
    
}
