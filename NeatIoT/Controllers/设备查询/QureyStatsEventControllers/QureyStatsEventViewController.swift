//
//  QureyStatsEventViewController.swift
//  NeatIoT
//
//  Created by Yang Blus on 2020/7/2.
//  Copyright © 2020 Neat. All rights reserved.
//

import UIKit

class QureyStatsEventViewController: SupperTableViewViewController {
    
    let QureyStatsEventTableViewCellIdent = "QureyStatsEventTableViewCell"
    
    var menuModel:MenuModel?
    var gatewayModel:QureyStatsGatewayItemModel?
    var dataSource:Array<QureyStatsEventItemModel> = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.titleName = menuModel?.name ?? ""
        
        listContainView.mj_header = nil
        listContainView.mj_footer = nil
        listContainView.delegate = self
        listContainView.dataSource = self
        listContainView.separatorColor = NormalColor.viewControllerDefaultBgColor
        listContainView.separatorInset = UIEdgeInsets.init(top: 0, left: 10, bottom: 0, right: 10)
        listContainView.separatorStyle = .singleLineEtched
        listContainView.register(UINib.init(nibName: "QureyStatsEventTableViewCell", bundle: Bundle.main), forCellReuseIdentifier: QureyStatsEventTableViewCellIdent)
        
        self.view.addSubview(listContainView)
        self.listContainView.snp.makeConstraints { (make) in
            make.edges.equalTo(self.view)
        }
        
        getEventData()
    }
    
    func getEventData() {
        
        NeatHud.showLoading(view: self.view, message: "")
        
        NeatRequest.instance.getQureyStatsGatewayEventList(gatewayId: (self.gatewayModel?.gatewayId)!, success: { (response) in
            
            NeatHud.dissmissHud(view: self.view)
            
            let titleItem = QureyStatsEventItemModel.init()
            titleItem.eventType = -1
            titleItem.eventContent = "内容"
            titleItem.occurTime = "发生时间"
            
            self.dataSource.append(titleItem)
            
            self.dataSource.append(contentsOf: response.itemArr ?? [])
            
            if self.dataSource.count == 0 {
                
                self.listContainView.showEmptyView()
            }
            
            self.listContainView.reloadData()
            
        
            
        }) { (errorResponse) in
            
            if errorResponse.errCode == RequestResponseCode.dataParsingException {
                //数据异常
                NeatHud.dissmissHud(view: self.view)
                self.listContainView.showRequestFaildView {
                    
                }
            }else if errorResponse.errCode == RequestResponseCode.netCanNotContent || errorResponse.errCode == RequestResponseCode.requestTimeOut || errorResponse.errCode == RequestResponseCode.canNotContentHost{
                //无网络
                NeatHud.dissmissHud(view: self.view)
                self.listContainView.showNetErrorView(errorMessage: errorResponse.errMessage) {
                    self.getEventData()
                }
            }else{
                NeatHud.showMessage(message: errorResponse.errMessage, view: self.view)
            }
            
        }
        
    }

}

extension QureyStatsEventViewController:UITableViewDataSource{
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: QureyStatsEventTableViewCellIdent, for: indexPath) as! QureyStatsEventTableViewCell
        cell.configerCell(model: dataSource[indexPath.row])
        
        return cell
    }
    

    
    
    
}

extension QureyStatsEventViewController:UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 46
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.1
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView.init()
    }
    
    
}
