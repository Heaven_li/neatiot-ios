//
//  InspectionPointQRModel.swift
//  NeatIoT
//
//  Created by neat on 2019/12/25.
//  Copyright © 2019 Neat. All rights reserved.
//

import UIKit
import HandyJSON
class InspectionPointQRModel: HandyJSON {
    
//    {
//        "t": "1",
//        "v": "1",
//        "p": "0323f5267de74386cc3bfc75"
//    }
    
    /// 版本
    var v:Int?
    
    /// 类型 1位二维码 0为NFC
    var t:Int?
    
    /// 标签id
    var p:String?
    
    required init(){
        
    }

}
