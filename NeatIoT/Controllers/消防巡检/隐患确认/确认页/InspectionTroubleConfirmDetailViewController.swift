//
//  InspectionTroubleConfirmDetailViewController.swift
//  NeatIoT
//
//  Created by Yang Blus on 2020/2/6.
//  Copyright © 2020 Neat. All rights reserved.
//

import UIKit
import AVFoundation
import CoreServices

class InspectionTroubleConfirmDetailViewController: SupperViewController {

       
    let ReportNormalInforTableViewCellIdent:String = "ReportNormalInforTableViewCell"
    let ReportTextInforTableViewCellIdent:String = "ReportTextInforTableViewCell"
    let ReportMediaTableViewCellIdent:String = "ReportMediaTableViewCell"
    let VoiceTableViewCellIdent:String = "VoiceTableViewCell"
    let ConfirmInforTableViewCellIdent = "ConfirmInforTableViewCell"
    let ReportConfirmTableViewCellIdent = "ReportConfirmTableViewCell"

    var confirmPointModel: TroubleConfirmPointModel?
        
//        let imagePickerVC = UIImagePickerController.init()
        
    let recordSetting: [String: Any] = [AVSampleRateKey: NSNumber(value: 16000),//采样率
               AVFormatIDKey: NSNumber(value: kAudioFormatMPEG4AAC),//音频格式
               AVLinearPCMBitDepthKey: NSNumber(value: 16),//采样位数
               AVNumberOfChannelsKey: NSNumber(value: 1),//通道数
               AVEncoderAudioQualityKey: NSNumber(value: AVAudioQuality.min.rawValue)//录音质量

           ];
    
    var currentAudioPath:URL?
    var sendNavItem:UIBarButtonItem?
        
    var lastRadioClickIndex:IndexPath?
    var lastVoiceItemCell:VoiceItemTableViewCell?

    let voiceSemaphore = DispatchSemaphore.init(value: 0)
    
    let mediaSemaphore = DispatchSemaphore.init(value: 0)
    
    /// 上传数据分类
    var voiceArr:Array<AnnexModel> = []
    var imageArr:Array<AnnexModel> = []
    var videoArr:Array<AnnexModel> = []
    
    /// 数据源数组
    var mDataSourceArr:Array<ReportItemModel> = []
    
    /// 媒体数组
    var mAnnexArr:Array<AnnexModel> = []
    let anxueModel = ReportItemModel.init()
    
    /// 语音数组
    var mVoiceArr:Array<AnnexModel> = []
    let voiceModel = ReportItemModel.init()

    /// 确认
    let confiremResult = ReportItemModel.init()

    /// 隐患确认内容描述
    let confirmEditText = ReportItemModel.init()
    
    /// 音频录制
    let audioTool = AudioRecorderTool.sharedInstance
    
    lazy var editText: ReportItemModel = {
        /// 隐患内容描述
        let editText = ReportItemModel.init()
        editText.itemStyle = .TextViewStyle
        editText.titleText = "隐患内容描述"
        editText.inforText = ""
        editText.isCanEdit = true
        
        return editText
    }()
    
    lazy var addMediaModel: AnnexModel = {
        let add = AnnexModel.init()
        add.annexType = 1
        add.isAddNewItem = true
        add.imageSource = UIImage.init(named: "add_annex")
        return add
    }()
    
    lazy var addVoiceModel: AnnexModel = {
        let add = AnnexModel.init()
        add.annexType = 3
        add.isAddNewItem = true
        return add
    }()
    
    lazy var recHud: AudioRecHud = {
        let hud = AudioRecHud.init(frame: CGRect.zero)
        hud.delegate = self
        return hud
    }()
    
    lazy var containTableView: UITableView = {
        let tableView = UITableView.init(frame: CGRect.zero, style: UITableView.Style.plain)
        tableView.backgroundColor = UIColor.clear
        tableView.delegate = self
        tableView.dataSource = self
        tableView.separatorInset = UIEdgeInsets.init(top: 0, left: 8, bottom: 0, right: 8)
        tableView.bounces = false
        tableView.register(UINib.init(nibName: "ReportNormalInforTableViewCell", bundle: Bundle.main), forCellReuseIdentifier: ReportNormalInforTableViewCellIdent)
        tableView.register(UINib.init(nibName: "ReportTextInforTableViewCell", bundle: Bundle.main), forCellReuseIdentifier: ReportTextInforTableViewCellIdent)
        tableView.register(UINib.init(nibName: "VoiceTableViewCell", bundle: Bundle.main), forCellReuseIdentifier: VoiceTableViewCellIdent)
        tableView.register(UINib.init(nibName: "ConfirmInforTableViewCell", bundle: Bundle.main), forCellReuseIdentifier: ConfirmInforTableViewCellIdent)
        tableView.register(UINib.init(nibName: "ReportConfirmTableViewCell", bundle: Bundle.main), forCellReuseIdentifier: ReportConfirmTableViewCellIdent)
        tableView.register(ReportMediaTableViewCell.self, forCellReuseIdentifier: ReportMediaTableViewCellIdent)
        
        
        return tableView
    }()
    
    override func viewWillDisappear(_ animated: Bool) {
        
        if audioTool.isPlay {
            
            lastVoiceItemCell?.voice_image_view.stopAnimating()
            audioTool.stopPlayRec()
            
        }
        
    }


    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.titleName = "隐患确认"
        let sendItmeModel = NavItemModel.init()
        sendItmeModel.itemText = "发送"
        self.rightItems = [sendItmeModel]
        
        sendNavItem = self.navigationItem.rightBarButtonItems![0]

        self.view.addSubview(self.containTableView)
        self.containTableView.snp.makeConstraints { (make) in
            make.edges.equalTo(self.view)
        }
        
        configerData()
        
    }
        
    override func navRRClick() {
        self.configerUploadInfor()
    }
        
    func configerData() {
        
        ///巡检点名称
        let pointName = ReportItemModel.init()
        pointName.itemStyle = .NormalStyle
        pointName.titleText = "巡检点名称"
        pointName.inforText = self.confirmPointModel?.pointInfoName
        pointName.uploadInfor = ""
        
        /// 项目子类型
        let projectSubType = ReportItemModel.init()
        projectSubType.itemStyle = .NormalStyle
        projectSubType.titleText = "项目子类型"
        projectSubType.inforText = self.confirmPointModel?.childTypeName
        projectSubType.uploadInfor = ""
        
        /// 所属企业
        let enterpriseName = ReportItemModel.init()
        enterpriseName.itemStyle = .NormalStyle
        enterpriseName.titleText = "所属单位名称"
        enterpriseName.inforText = self.confirmPointModel?.enterpriseName
        enterpriseName.uploadInfor = ""
        
        /// 所属建筑
        let buildingName = ReportItemModel.init()
        buildingName.itemStyle = .NormalStyle
        buildingName.titleText = "所属建筑"
        buildingName.inforText = self.confirmPointModel?.builingName
        buildingName.uploadInfor = ""
        
        /// 所属部位
        let keypartName = ReportItemModel.init()
        keypartName.itemStyle = .NormalStyle
        keypartName.titleText = "所属部位"
        keypartName.inforText = self.confirmPointModel?.keypartName
        keypartName.uploadInfor = ""
        
        /// 详细地址
        let addressName = ReportItemModel.init()
        addressName.itemStyle = .NormalStyle
        addressName.titleText = "详细地址"
        addressName.inforText = self.confirmPointModel?.address
        addressName.uploadInfor = ""
        
        
        confirmEditText.itemStyle = .UnTextViewStyle
        confirmEditText.titleText = "隐患确认描述"
        confirmEditText.inforText = self.confirmPointModel?.confirmContent
        confirmEditText.isCanEdit = true
        confirmEditText.uploadInfor = ""
        
        confiremResult.itemStyle = .RadioStyle
        confiremResult.titleText = "隐患确认结果"
        confiremResult.inforText = "2"
        confiremResult.uploadInfor = "2"
        
        /// 上传附件
        mAnnexArr = [addMediaModel]
        
        anxueModel.itemStyle = .MedialStyle
        anxueModel.titleText = "上传附件"
        anxueModel.annexArr = mAnnexArr
        
        
        /// 语音附件
        mVoiceArr = [addVoiceModel]
        
        voiceModel.itemStyle = .VoiceStyle
        voiceModel.titleText = "语音附件"
        voiceModel.annexArr = mVoiceArr
        
        mDataSourceArr.append(pointName)
        mDataSourceArr.append(projectSubType)
        mDataSourceArr.append(enterpriseName)
        mDataSourceArr.append(buildingName)
        mDataSourceArr.append(keypartName)
        mDataSourceArr.append(addressName)
        mDataSourceArr.append(confirmEditText)
        mDataSourceArr.append(confiremResult)
        mDataSourceArr.append(editText)
        mDataSourceArr.append(anxueModel)
        mDataSourceArr.append(voiceModel)
        
    }
        
    override func didSelectAnnex(annexModel: AnnexModel) {
        //判断已有附件数量 如果多余8个则移除添加按钮 少于8个添加添加按钮
        if mAnnexArr.count == 8 {
            mAnnexArr.insert(annexModel, at: 0)
            mAnnexArr.removeLast()
        }else{
            mAnnexArr.insert(annexModel, at: 0)
        }

        self.updateAnnex()
    }
        
    func updateAnnex() {
        anxueModel.annexArr = mAnnexArr
        self.containTableView.reloadData()
    }
    
    func updateAudio() {
        voiceModel.annexArr = mVoiceArr
        self.containTableView.reloadData()
    }
    
    func showRecView() {
        
        self.view.addSubview(self.recHud)
        self.recHud.startRecTime()
        self.recHud.snp.makeConstraints { (make) in
            make.center.equalTo(self.view)
            make.width.equalTo(120)
            make.height.equalTo(120)
        }
        
    }
    
    func dismissRecHud(){
        self.recHud.endRecTime()
        self.recHud.removeFromSuperview()
        
    }
    
    func configerUploadInfor() {
        
        sendNavItem!.isEnabled = false
        
        //清空附件数组
        voiceArr.removeAll()
        imageArr.removeAll()
        videoArr.removeAll()
        
        for model in self.mVoiceArr {
            if !model.isAddNewItem {
                voiceArr.append(model)
            }
        }
        
        for model in self.mAnnexArr{

            if !model.isAddNewItem {
            
                if model.annexType == 1 {
                    //图片
                    let data = model.imageSource!.jpegData(compressionQuality: 0.1)!
                    model.imageSource = UIImage.init(data: data)
                    imageArr.append(model)
                    
                }else if model.annexType == 2 {
                    //视频
                    videoArr.append(model)
                    
                }
            }
        }
        
        if self.videoArr.count != 0{
            NeatHud.showLoading(view: self.view, message: "正在进行视频转码...")
            sourceDecode(isVideo: true, isAudio: false)
        }else{
            self.fileTransformFinish()
        }

    }
    func sourceDecode(isVideo:Bool,isAudio:Bool) {
        
        DispatchQueue.global().async {
            
            if isVideo{
                
                for videoModel in self.videoArr{
                    VideoFormatTransformTool.moveFileTransformToMp4WithSourceUrl(sourceUrl: videoModel.sourceFile!, finish: { (url) in
                        
                        videoModel.sourceFile = url
                        self.mediaSemaphore.signal()
                        
                    }) {
                        ///失败
                    
                    }
                    self.mediaSemaphore.wait()
                }
                
                debugPrint("视频转码完成")
                
            }
            
            self.fileTransformFinish()
            
        }
        
        
    }
    
    func fileTransformFinish() {
        
        let pointInforId = self.confirmPointModel!.pointInfoId ?? ""
        let troubleHandleId = self.confirmPointModel!.id!
        let confirmResultStr = self.confiremResult.uploadInfor!
        let confirmContentStr = self.confirmEditText.uploadInfor!
        var itemIdList:Array<[String:String]> = []
        
        for model in self.confirmPointModel!.troubleItemList! {
            var dic = [String:String]()
            dic["ItemId"] = model.itemId
            itemIdList.append(dic)
        }
        
        
        if confirmResultStr.isEmpty {
            DispatchQueue.main.async {
                NeatHud.showMessage(message: "请选择处理结果!", view: self.view)
            }
        }
        
        if videoArr.count == 0 && imageArr.count == 0 && voiceArr.count == 0 && confirmContentStr.isEmpty{
            DispatchQueue.main.async {
                self.sendNavItem!.isEnabled = true
                NeatHud.showMessage(message: "请至少上传语音、图片、视频或隐患内容其中一种信息", view: self.view)
            }
            return
        }
        
        DispatchQueue.main.async {
            NeatHud.showLoading(view: self.view, message: "正在上传确认信息...")
        }
        
        NeatRequest.instance.handleTrouble(PointInforId: pointInforId, TroubleHandleId: troubleHandleId, ConfirmResult: confirmResultStr, ConfirmContent: confirmContentStr, ItemIdList: itemIdList, videoArr: videoArr, imageArr: imageArr, voiceArr: voiceArr, success: { (response) in
            
            
            NeatHud.showMessage(message: response.message ?? "隐患确认成功", view: self.view) {
            
                self.navigationController?.popViewController(animated: true)
                self.sendNavItem?.isEnabled = true
            }
            
            
        }) { (error) in
            
            DispatchQueue.main.async {
                self.sendNavItem?.isEnabled = true
                NeatHud.showMessage(message: error.errMessage, view: self.view)
            }
        }
        
        
    
    }
        /*
        // MARK: - Navigation

        // In a storyboard-based application, you will often want to do a little preparation before navigation
        override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
            // Get the new view controller using segue.destination.
            // Pass the selected object to the new view controller.
        }
        */

    }

    extension InspectionTroubleConfirmDetailViewController:UITableViewDelegate{
        
        func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
            
            let model = mDataSourceArr[indexPath.row]
            
            if model.itemStyle == .NormalStyle || model.itemStyle == .RadioStyle{
                return 46
            }else if model.itemStyle == .TextViewStyle || model.itemStyle == .UnTextViewStyle{
                return 98
            }else if model.itemStyle == .MedialStyle{
                let itemWidth = (NormalConstant.ScreenWidth - 6*(4 - 1 + 2))/4
                if model.annexArr.count>=1 && model.annexArr.count <= 4 {
                    return itemWidth + 6 + 36
                }else{
                    return (itemWidth + 6)*2 + 36
                }
            }else{
                return CGFloat(48) * CGFloat(model.annexArr.count) + 36
            }
        }
        
        func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
            0.01
        }
        func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
            return UIView.init()
        }
        
    }
    extension InspectionTroubleConfirmDetailViewController:UITableViewDataSource{
        
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            
            let model = mDataSourceArr[indexPath.row]
            
            if model.itemStyle == .NormalStyle{
                
                let cell = tableView.dequeueReusableCell(withIdentifier: ReportNormalInforTableViewCellIdent, for: indexPath) as! ReportNormalInforTableViewCell
                cell.configerCell(model: model)
                cell.selectionStyle = .none
                return cell
                
            }else if model.itemStyle == .TextViewStyle{
                let cell = tableView.dequeueReusableCell(withIdentifier: ReportTextInforTableViewCellIdent, for: indexPath) as! ReportTextInforTableViewCell
                cell.configerCell(model: model)
                cell.selectionStyle = .none
                       
                return cell
            }else if model.itemStyle == .MedialStyle {
                var cell = tableView.dequeueReusableCell(withIdentifier: ReportMediaTableViewCellIdent, for: indexPath) as! ReportMediaTableViewCell
                if cell == nil{
                    cell = ReportMediaTableViewCell.init(style: .default, reuseIdentifier: ReportMediaTableViewCellIdent)
                }
                cell.delegate = self
                cell.configerCell(model: model)
                cell.selectionStyle = .none
                return cell
                
                
            }else if model.itemStyle == .UnTextViewStyle{
                
                let cell = tableView.dequeueReusableCell(withIdentifier: ConfirmInforTableViewCellIdent, for: indexPath) as! ConfirmInforTableViewCell
                cell.configerCell(model: model)
                cell.selectionStyle = .none
                return cell
                
            }else if model.itemStyle == .RadioStyle{
                
                let cell = tableView.dequeueReusableCell(withIdentifier: ReportConfirmTableViewCellIdent, for: indexPath) as! ReportConfirmTableViewCell
                cell.delegate = self
                cell.configerCell(model: model)
                cell.selectionStyle = .none
                return cell
            }else{
                
                let cell = tableView.dequeueReusableCell(withIdentifier: VoiceTableViewCellIdent, for: indexPath) as! VoiceTableViewCell
                cell.delegate = self
                cell.configerCell(mode: model)
                cell.selectionStyle = .none
                return cell
                
            }
            
           
        }
        
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return mDataSourceArr.count
        }
        
    }

    extension InspectionTroubleConfirmDetailViewController:ReportMediaTableViewCellDelegate{
        
        func didSelectItem(model: AnnexModel) {
            //点击
            //先判断是否还能添加 如果已满8个则添加按钮消失
            if model.isAddNewItem {
                self.imagePickerPresent()
            }else{
                //查看图片 或 视频
                let detailVC = MediaPlayViewController.init()
                detailVC.annexModel = model
                self.navigationController?.pushViewController(detailVC, animated: true)
            }
        }
        
        
        
        func didLongSelectItem(model:AnnexModel,indexPath: IndexPath) {
            //长按
            //如果不是添加按钮
            if !model.isAddNewItem {
                var isHaveAdd = false
                for item in mAnnexArr {
                    if item.isAddNewItem {
                        isHaveAdd = true
                    }
                }
                
                if isHaveAdd  {
                    //如果少于8个 则删除对应图片即可
                    mAnnexArr.remove(at: indexPath.item)
                }else{
                    //如果当前是8个附件 则删除后要添加添加按钮
                    mAnnexArr.remove(at: indexPath.item)
                    mAnnexArr.append(addMediaModel)
                }
                
                self.updateAnnex()
                
            }
            
            
        }
        
    }


    extension InspectionTroubleConfirmDetailViewController:AudioRecHudDelegate{
        
        func recTimeOut() {
            self.endRecVoice()
        }
        
    }


    extension InspectionTroubleConfirmDetailViewController:VoiceTableViewCellDelegate{
        
        func clickRecVoice(model: AnnexModel, index: IndexPath, cell: VoiceItemTableViewCell) {
            
            if lastRadioClickIndex != nil {
                //有记录之前点击过

                if index == lastRadioClickIndex {
                    //两次点击相同音频
                    if audioTool.isPlay {
                        audioTool.stopPlayRec()
                        cell.voice_image_view.stopAnimating()
                    }else{
                        self.playRecVoice(model: model)
                        cell.voice_image_view.startAnimating()
                    }
                }else{
                    //两次不同
                    if audioTool.isPlay {
                        audioTool.stopPlayRec()
                        lastVoiceItemCell?.voice_image_view.stopAnimating()
                        self.playRecVoice(model: model)
                        cell.voice_image_view.startAnimating()
                    }else{
                        self.playRecVoice(model: model)
                        cell.voice_image_view.startAnimating()
                    }
                }

                lastVoiceItemCell = cell
                lastRadioClickIndex = index

            }else{
                //首次点击
                lastRadioClickIndex = index
                lastVoiceItemCell = cell
                self.playRecVoice(model: model)
                cell.voice_image_view.startAnimating()
            }
            

            
        }
        /// 播放录音
        func playRecVoice(model: AnnexModel) {
            
            audioTool.playRec(model: model, finishPlay: {
                self.lastVoiceItemCell?.voice_image_view.stopAnimating()
            }) {
                self.lastVoiceItemCell?.voice_image_view.stopAnimating()
                NeatHud.showMessage(message: "播放失败", view: self.view)
            }
            
         }

        
        /// 开始录音
        func startRecVoice() {

            if audioTool.isPlay {
                AudioRecorderTool.sharedInstance.stopPlayRec()
            }
            
            audioTool.startRec(url: FileManagerTool.directoryURL(), start: {
                self.showRecView()
            }) { (isPermissionFalied, recFailed) in
                if isPermissionFalied {
                    self.showAlert(vc: self, title: "提示", message: "尚未取得麦克风使用权限,无法使用语音功能。是否去设置？", leftTitle: "取消", rightTitle: "去设置", leftAction: { (action) in
                        
                        
                    }) { (action) in
                        //去设置
                        let settingsUrl = URL(string:UIApplication.openSettingsURLString)
                        if #available(iOS 10.0, *) {
                            UIApplication.shared.open(settingsUrl!, options: [:], completionHandler: nil)
                        } else {
                            
                        }
                    }
                }
                if recFailed {
                    NeatHud.showMessage(message: "开启录音失败，稍后再试", view: self.view)
                }
            }
            
        }
        
        func endRecVoice() {
            
            self.dismissRecHud()
            audioTool.stopRec(finish: { (model) in

                //判断已有附件数量 如果多余8个则移除添加按钮 少于8个添加添加按钮
                if self.mVoiceArr.count == 3 {
                    self.mVoiceArr.insert(model, at: 0)
                    self.mVoiceArr.removeLast()
                }else{
                    self.mVoiceArr.insert(model, at: 0)
                }
                self.updateAudio()

            }) { (error) in
                NeatHud.showMessage(message: error, view: self.view)
            }
     
        }
        
        func deleteRecVoice(model: AnnexModel, index: IndexPath) {
            
            if audioTool.isPlay {
                audioTool.stopPlayRec()
            }
            
            if !model.isAddNewItem {
                var isHaveAdd = false
                for item in mVoiceArr {
                    if item.isAddNewItem {
                        isHaveAdd = true
                    }
                }
                
                if isHaveAdd  {
                    //如果少于3个 则删除对应图片即可
                    mVoiceArr.remove(at: index.item)
                }else{
                    //如果当前是3个附件 则删除后要添加添加按钮
                    mVoiceArr.remove(at: index.item)
                    mVoiceArr.append(addVoiceModel)
                }
                
                self.updateAudio()
                
            }
            
        }
        
        
    }

extension InspectionTroubleConfirmDetailViewController:ReportConfirmTableViewCellDelegate{
    
    func confirmRadioBtnChange(currentModel: ReportItemModel) {
        self.confiremResult.uploadInfor = currentModel.uploadInfor
    }
    
    
    
    
}


    
