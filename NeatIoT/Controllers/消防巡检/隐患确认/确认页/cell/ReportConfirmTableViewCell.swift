//
//  ReportConfirmTableViewCell.swift
//  NeatIoT
//
//  Created by Yang Blus on 2020/2/7.
//  Copyright © 2020 Neat. All rights reserved.
//

import UIKit

protocol ReportConfirmTableViewCellDelegate {
    func confirmRadioBtnChange(currentModel:ReportItemModel)
}

class ReportConfirmTableViewCell: UITableViewCell {
    
    var currentModel:ReportItemModel?
    var delegate:ReportConfirmTableViewCellDelegate?
    
    lazy var titleLable: UILabel = {
        let lable = UILabel.init()
        lable.font = UIFont.systemFont(ofSize: 16)
        lable.textColor = #colorLiteral(red: 0.121294491, green: 0.1292245686, blue: 0.141699791, alpha: 1)
        return lable
    }()
    
    lazy var checkView: CheckRadioGroupView = {
        
        let checkView = CheckRadioGroupView.init()
        checkView.isMuitleCheck = false
        checkView.delegate = self
        
        return checkView
    }()


    override func awakeFromNib() {
        
        super.awakeFromNib()
        
        self.addSubview(self.titleLable)
        
        self.titleLable.snp.makeConstraints { (make) in
            make.leading.equalTo(self).offset(12)
            make.top.bottom.equalTo(self)
            make.width.equalTo(120)
        }

        
        self.addSubview(self.checkView)
        
        checkView.snp.makeConstraints { (make) in
        
            make.top.equalTo(self).offset(10)
            make.bottom.equalTo(self).offset(-10)
            make.trailing.equalTo(self)
            make.width.equalTo(180)
            
        }
        
        let itemOne = CheckRadioItemModel.init()
        itemOne.isRadioCheck = true
        itemOne.radioTitle = "已完成"
        itemOne.radioKey = "2"
        
        let itemTwo = CheckRadioItemModel.init()
        itemTwo.isRadioCheck = false
        itemTwo.radioTitle = "未完成"
        itemTwo.radioKey = "3"
        
        checkView.radioItemArr = [itemOne,itemTwo]
    
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configerCell(model:ReportItemModel) {
        
        currentModel = model
        
        self.titleLable.text = currentModel!.titleText
        
        
    }
    
}

extension ReportConfirmTableViewCell:CheckRadioGroupViewDelegate{
    
    func checkRadioGroupValueChanged(checkRadioItems: [CheckRadioItemModel]) {
        
        for item in checkRadioItems {
                   
            debugPrint("当前选中项目：",item.radioKey,item.radioTitle)
            currentModel?.inforText = item.radioKey
            currentModel?.uploadInfor = item.radioKey
            self.delegate?.confirmRadioBtnChange(currentModel: currentModel!)
                   
        }
        
    }
    
}
