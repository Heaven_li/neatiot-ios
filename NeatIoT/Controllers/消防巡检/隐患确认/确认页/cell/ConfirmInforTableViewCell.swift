//
//  ConfirmInforTableViewCell.swift
//  NeatIoT
//
//  Created by Yang Blus on 2020/2/6.
//  Copyright © 2020 Neat. All rights reserved.
//

import UIKit

class ConfirmInforTableViewCell: UITableViewCell {

    @IBOutlet weak var textView: UITextView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configerCell(model:ReportItemModel) {
        
        self.textView.text = model.inforText
        
    }
    
}
