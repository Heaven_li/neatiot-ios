//
//  TroubleConfirmTableViewCell.swift
//  NeatIoT
//
//  Created by neat on 2019/12/20.
//  Copyright © 2019 Neat. All rights reserved.
//

import UIKit

protocol TroubleConfirmTableViewCellDelegate {
    func confirmTrouble(model:TroubleConfirmPointModel)
}

class TroubleConfirmTableViewCell: UITableViewCell {
    
    var currentModel:TroubleConfirmPointModel?

    @IBOutlet weak var confirmBtn: UIButton!
    
    @IBOutlet weak var pointNameLable: UILabel!
    
    @IBOutlet weak var comTimeLable: UILabel!
    
    @IBOutlet weak var detailAddressLable: UILabel!
    
    var delegate:TroubleConfirmTableViewCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func confirmBtnClick(_ sender: Any) {
        
        self.delegate?.confirmTrouble(model: currentModel!)
    }
    func configerCell(model:TroubleConfirmPointModel) {
        
        currentModel = model
        
        self.pointNameLable.text = model.pointInfoName
        
        self.comTimeLable.text = "截止时间:"+(model.confirmEndTime ?? "")
        
        self.detailAddressLable.text = "详细地址:"+(model.address ?? "")
        
    }
    
}
