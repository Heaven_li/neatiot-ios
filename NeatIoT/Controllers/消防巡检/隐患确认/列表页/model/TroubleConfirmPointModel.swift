//
//  ToublePointModel.swift
//  NeatIoT
//
//  Created by neat on 2019/12/20.
//  Copyright © 2019 Neat. All rights reserved.
//

import UIKit
import HandyJSON
class TroubleConfirmPointModel: HandyJSON {
    
    /// 点位id
    var id:String?
    /// 隐患id
    var troubleId:String?
    /// 确认最后结束时间
    var confirmEndTime:String?
    var pointInfoId:String?
    /// 点位名称
    var pointInfoName:String?
    /// 子类型名称
    var childTypeName:String?
    /// 企业名称
    var enterpriseName:String?
    /// 建筑名称
    var builingName:String?
    /// 部位名称
    var keypartName:String?
    /// 巡检点位置
    var address:String?
    /// 确认内容
    var confirmContent:String?
    /// 附件
    var uploadInfoList:String?
    var troubleItemList:Array<TroubleConfirmProjectModel>?
    
    required init() {
        
    }

}
