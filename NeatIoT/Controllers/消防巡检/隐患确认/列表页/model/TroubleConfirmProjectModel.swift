//
//  TroubleProjectModel.swift
//  NeatIoT
//
//  Created by neat on 2019/12/20.
//  Copyright © 2019 Neat. All rights reserved.
//

import UIKit
import HandyJSON
class TroubleConfirmProjectModel: HandyJSON {
    
    /// 项目id
    var itemId:String?
    
    /// 隐患id
    var troubleId:String?
    
    /// 项目名称
    var itemName:String?
    
    /// 巡检结果
    var itemResult:Int?
    
    required init(){
        
    }

}
