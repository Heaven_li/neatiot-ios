//
//  TroubleResultModel.swift
//  NeatIoT
//
//  Created by neat on 2019/12/20.
//  Copyright © 2019 Neat. All rights reserved.
//

import UIKit
import HandyJSON
class TroubleConfirmResultModel: HandyJSON {
    
    /// 数据总数
    var totalCount:Int?
    
    /// 数据总数
    var pointArr:Array<TroubleConfirmPointModel>?
    
    required init(){
        
    }
    
    func mapping(mapper: HelpingMapper) {
        mapper <<<
        self.pointArr <-- "data"
    }

}
