//
//  InspectionTroubleConfirmViewController.swift
//  NeatIoT
//
//  Created by neat on 2019/12/16.
//  Copyright © 2019 Neat. All rights reserved.
//

import UIKit

class InspectionTroubleConfirmViewController: SupperTableViewViewController {

    let cellIdent = "cellIdent"
    var menuModel:MenuModel?
    
    var currentPageIndex = 1
    
    var dataSourceArr:Array<TroubleConfirmPointModel> = []
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewDidLoad()
        getData()
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.titleName = self.menuModel?.name ?? "确认列表"
        configerUI()
        
    }
    
    func configerUI() {
        
        self.listContainView.separatorStyle = .singleLine
        self.listContainView.separatorInset = UIEdgeInsets.init(top: 0, left: 10, bottom: 0, right: 10)
        self.listContainView.delegate = self
        self.listContainView.dataSource = self
        self.listContainView.register(UINib.init(nibName: "TroubleConfirmTableViewCell", bundle: Bundle.main), forCellReuseIdentifier: self.cellIdent)
        
        self.view.addSubview(self.listContainView)
        self.listContainView.snp.makeConstraints { (make) in
            make.edges.equalTo(self.view)
        }
    }
    
    override func refreshAction() {
        
        currentPageIndex = 1
        NeatRequest.instance.getTroubleConfirmPointList(pageIndex: currentPageIndex, success: { (response) in
            
            self.dataSourceArr.removeAll()
            self.dataSourceArr = response.resultData!.pointArr!
            self.endRefresh(isUpdateFooter: true, dataCount: self.dataSourceArr.count, pageSize: 10)
            self.listContainView.reloadData()
            
        }) { (error) in
            
            NeatHud.showMessage(message: error.errMessage, view: self.view)
            
        }
    
    }
    override func loadMoreAction() {
        
        currentPageIndex+=1
        
        NeatRequest.instance.getTroubleConfirmPointList(pageIndex: currentPageIndex, success: { (response) in
            
            let pointArr = response.resultData?.pointArr ?? []
            if pointArr.count == 0{
                self.currentPageIndex -= 0
                
            }
            self.dataSourceArr.append(contentsOf: pointArr)
            self.endLoadMoreData(dataCount: pointArr.count, pageSize: 10)
            self.listContainView.reloadData()
            
        }) { (error) in
            
            self.currentPageIndex-=1
            NeatHud.showMessage(message: error.errMessage, view: self.view)
            
        }
    }
    
    
    
    func getData() {
        
        self.listContainView.showLoadingView()
        
        NeatRequest.instance.getTroubleConfirmPointList(pageIndex: currentPageIndex, success: { (response) in
            
            self.dataSourceArr = response.resultData?.pointArr ?? []
            if self.dataSourceArr.count == 0{
                self.listContainView.showEmptyView()
            }else{
                self.listContainView.hidenHolderView()
                self.endLoadMoreData(dataCount: self.dataSourceArr.count, pageSize: 10)
                self.listContainView.reloadData()
            }
            
        }) { (error) in
            
            if error.errCode == RequestResponseCode.dataParsingException {
                //数据异常
                NeatHud.dissmissHud(view: self.view)
                self.listContainView.showRequestFaildView {
                    
                }
            }else if error.errCode == RequestResponseCode.netCanNotContent || error.errCode == RequestResponseCode.requestTimeOut{
                //无网络
                NeatHud.dissmissHud(view: self.view)
                self.listContainView.showNetErrorView(errorMessage: error.errMessage) {
                    self.getData()
                }
            }else{
                NeatHud.showMessage(message: error.errMessage, view: self.view)
            }
            
        }
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension InspectionTroubleConfirmViewController:UITableViewDelegate{
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView.init()
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        0.01
    }
    
}

extension InspectionTroubleConfirmViewController:UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        self.dataSourceArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdent, for: indexPath) as! TroubleConfirmTableViewCell
        cell.delegate = self
        cell.configerCell(model: dataSourceArr[indexPath.row])
        cell.selectionStyle = .none
        return cell
    }
    
    
}

extension InspectionTroubleConfirmViewController:TroubleConfirmTableViewCellDelegate{
    
    func confirmTrouble(model: TroubleConfirmPointModel) {
        
        var confirmDetailVC = InspectionTroubleConfirmDetailViewController.init()
        confirmDetailVC.confirmPointModel = model
        self.navigationController?.pushViewController(confirmDetailVC, animated: true)
        
    }
    
    
}
