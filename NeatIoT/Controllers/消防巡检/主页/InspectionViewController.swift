//
//  InspectionViewController.swift
//  NeatIoT
//
//  Created by neat on 2019/12/13.
//  Copyright © 2019 Neat. All rights reserved.
//

import UIKit

class InspectionViewController: SupperViewController {
    
    let DASBOARDIDENTIFIER = "DasboardCollectionViewCell"
    
    let FUNCTIONCELLIDENTIFIER = "FunctionItemCollectionViewCell"
    ///巡检任务
    let patrol_task = "patrol_task";
    ///历史巡检
    let patrol_task_history = "patrol_task_history";
    ///隐患点
    let patrol_trouble_point = "patrol_trouble_point";
    ///隐患上报
    let patrol_trouble_submit = "patrol_trouble_submit";
    ///巡检点绑定
    let patrol_point_bind = "patrol_point_bind";
    ///消息提醒
    let patrol_msg_alert = "patrol_msg_alert";
    ///隐患确认
    let patrol_trouble_confirm = "patrol_trouble_confirm";
    ///数据传输
    let patrol_data_upload = "patrol_data_upload";
    ///快速巡检
    let patrol_instance_patrol = "patrol_instance_patrol";
    
    var dataSourcArr = Array<MenuModel>()
    
    var finishRateModel:InspectionRateModel?
    /// 数据看板model
    let dashboardModel = DasboardCollectionModel.init()
    
    var menuModel:MenuModel?
    
    lazy var containCollectionView: UICollectionView = {
        
        let layout = UICollectionViewFlowLayout.init()
        layout.scrollDirection = .vertical
        layout.minimumLineSpacing = 1
        layout.minimumInteritemSpacing = 1
        
        let collection = UICollectionView.init(frame: CGRect.zero, collectionViewLayout: layout)
        collection.delegate = self
        collection.dataSource = self
        
        collection.backgroundColor = NormalColor.viewControllerDefaultBgColor
        
        collection.register(UINib.init(nibName: "DasboardCollectionViewCell", bundle: Bundle.main), forCellWithReuseIdentifier: self.DASBOARDIDENTIFIER)
        collection.register(UINib.init(nibName: "FunctionItemCollectionViewCell", bundle: Bundle.main), forCellWithReuseIdentifier: self.FUNCTIONCELLIDENTIFIER)
        
        return collection
    }()
    
    
    
    override func viewWillAppear(_ animated: Bool) {
         self.navigationController?.navigationBar.setBackgroundImage(UIImage.init(named: "inspection_nav_bg"), for: .default)
         self.navigationController?.navigationBar.shadowImage = UIImage.init()
        
        getTaskFinishRate()
        getUnReadMessage()
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.navigationBar.setBackgroundImage(UIImage.init(named: "nav_bar_bg"), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage.init()
    }

    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        self.titleName = self.menuModel?.name ?? "消防巡检"
        
        self.dataSourcArr = self.menuModel!.children
        
        self.view.addSubview(self.containCollectionView)
        self.containCollectionView.snp.makeConstraints { (make) in
            make.edges.equalTo(self.view)
        }
        
    }
    
    func getUnReadMessage() {
        NeatRequest.instance.getUnReadPushNum(success: { (response) in
            
            self.updateUnReadNum(numCount: response.unReadMessageCount)
            
        }) { (error) in
            NeatHud.showMessage(message: error.errMessage, view: self.view)
        }
    }
    
    func getTaskFinishRate() {
        NeatRequest.instance.getTaskFinishRate(success: { (response) in
            
            self.finishRateModel = response.resultData
            
            self.formateTaskFinishRate()
            
        
            self.containCollectionView.reloadData()
        }) { (error) in
            NeatHud.showMessage(message: error.errMessage, view: self.view)
        }
    }
    func updateUnReadNum(numCount:Int) {
        
        for model in self.dataSourcArr {
            if model.style_id == patrol_msg_alert{
                model.notifyNum = numCount
            }
        }
        self.containCollectionView.reloadData()
    }
    
    func formateTaskFinishRate() {
        
        let notFinishCount = CGFloat(self.finishRateModel?.notFinishCount ?? 0)
        let finishCount = CGFloat(self.finishRateModel?.finishCount ?? 0)
        let totalCount = CGFloat(self.finishRateModel?.all ?? 0)
        
        var rateStr = "0"
        if totalCount == 0 {
            rateStr = "0"
        }else{
            rateStr = String.init(format: "%.2f", finishCount/totalCount)
        }
        
        dashboardModel.leftTitel = "待完成任务"
        dashboardModel.leftInfor = Float(notFinishCount)
        dashboardModel.midTitel = "已完成"
        dashboardModel.midInfor = Float(rateStr)
        dashboardModel.rightTitel = "已完成任务"
        dashboardModel.rightInfor = Float(finishCount)
        
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension InspectionViewController:UICollectionViewDelegate{
    
    
}
extension InspectionViewController:UICollectionViewDataSource{
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let menuModel = dataSourcArr[indexPath.item]
        
        if (menuModel.style_id == patrol_task){

            let vc = InspectionTaskViewController.init()
            vc.menuModel = menuModel
            self.navigationController?.pushViewController(vc, animated: true)
                       

        }else if (menuModel.style_id == patrol_task_history){

            let vc = InspectionHistoryViewController.init()
            vc.menuModel = menuModel
            self.navigationController?.pushViewController(vc, animated: true)

        }else if (menuModel.style_id == patrol_trouble_point){

            let vc = InspectionTroublePointViewController.init()
            vc.menuModel = menuModel
            self.navigationController?.pushViewController(vc, animated: true)

        }else if (menuModel.style_id == patrol_trouble_submit){

            let vc = InspectionTroubleUpdateViewController.init()
            vc.menuModel = menuModel
            self.navigationController?.pushViewController(vc, animated: true)

        }else if (menuModel.style_id == patrol_point_bind){

            let vc = InspectionBindViewController.init()
            vc.menuModel = menuModel
            self.navigationController?.pushViewController(vc, animated: true)

        }else if (menuModel.style_id == patrol_msg_alert){

            let vc = InspectionNotifyViewController.init()
            vc.menuModel = menuModel
            self.navigationController?.pushViewController(vc, animated: true)

        }else if (menuModel.style_id == patrol_trouble_confirm){

            let vc = InspectionTroubleConfirmViewController.init()
            vc.menuModel = menuModel
            self.navigationController?.pushViewController(vc, animated: true)

        }else if (menuModel.style_id == patrol_data_upload){

            NeatHud.showMessage(message: "功能尚未开放！", view: self.view)

        }else if (menuModel.style_id == patrol_instance_patrol){

            
            
            let vcInitModel = VCInitModel.init()
            vcInitModel.navTitleText = "二维码扫描"
            
            let swiperVC = QRScanViewController.init()
            swiperVC.vcInit = vcInitModel
            swiperVC.isFastInspection = true
            self.navigationController?.pushViewController(swiperVC, animated: true)
            
//            let vc = InspectionFastViewController.init()
//            vc.menuModel = menuModel
//            self.navigationController?.pushViewController(vc, animated: true)

        }
        
        
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 2
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return section == 0 ? 1:dataSourcArr.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if indexPath.section == 0 {
            
            let item = collectionView.dequeueReusableCell(withReuseIdentifier: DASBOARDIDENTIFIER, for: indexPath) as! DasboardCollectionViewCell
            if self.finishRateModel != nil {
                item.configerCell(dashboardModel: dashboardModel)
            }
            return item
            
        }else{
            
            let item = collectionView.dequeueReusableCell(withReuseIdentifier: FUNCTIONCELLIDENTIFIER, for: indexPath) as! FunctionItemCollectionViewCell
    
            item.configerItem(menuModel: dataSourcArr[indexPath.item])
            
            return item
            
        }
        
    }

}
extension InspectionViewController: UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        var itemSize = CGSize.zero
        if indexPath.section == 0 {
            itemSize = CGSize.init(width:NormalConstant.ScreenWidth, height: NormalConstant.ScreenWidth*0.46)
        }else{
            itemSize = CGSize.init(width:(NormalConstant.ScreenWidth-2)/3.0, height: (NormalConstant.ScreenWidth-2)/3.0)
        }
        return itemSize
    }
    

    
}


