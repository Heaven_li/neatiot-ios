//
//  InspectionFunctionItemCollectionViewCell.swift
//  NeatIoT
//
//  Created by neat on 2019/12/13.
//  Copyright © 2019 Neat. All rights reserved.
//

import UIKit

class InspectionFunctionItemCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var iconImageView: UIImageView!
    
    @IBOutlet weak var notifyNum: UILabel!
    @IBOutlet weak var mainTitleLable: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func configerItem(menuModel:MenuModel) -> Void {
        
        notifyNum.layer.cornerRadius = 8
        notifyNum.clipsToBounds = true
        notifyNum.text = "99"
        
        if menuModel.notifyNum == 0 {
            self.notifyNum.isHidden = true
        }else{
            self.notifyNum.isHidden = false
            if menuModel.notifyNum > 99{
                notifyNum.text = "99+"
            }else{
                notifyNum.text = NSString.init(format: "%d", menuModel.notifyNum) as String
            }
        }
        
        
        
        iconImageView.image = UIImage.init(named: menuModel.style_id)
        mainTitleLable.text = menuModel.name
        
    }

}
