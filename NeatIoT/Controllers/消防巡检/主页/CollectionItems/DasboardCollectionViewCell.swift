//
//  DasboardCollectionViewCell.swift
//  NeatIoT
//
//  Created by neat on 2019/12/13.
//  Copyright © 2019 Neat. All rights reserved.
//

import UIKit

class DasboardCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var standbyTaskNumLable: UILabel!
    
    @IBOutlet weak var rateOfFinishLable: UILabel!
    
    @IBOutlet weak var finishTaskNumLable: UILabel!
    
    var finishCount:CGFloat = 0
    var notFinishCount:CGFloat = 0
    var totalCount:CGFloat = 0
    let dotAngle = CGFloat(Double.pi/180*0.1)
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        debugPrint(self.bounds,self.center)
        
        
    }
    
    override func layoutSubviews() {
        
        debugPrint("layoutSubviews",self.bounds,self.center)
        
        drawCircle()
    }
    
    func configerCell(model:InspectionRateModel) {
        
        notFinishCount = CGFloat(model.notFinishCount ?? 0)
        finishCount = CGFloat(model.finishCount ?? 0)
        totalCount = CGFloat(model.all ?? 0)
        
        self.standbyTaskNumLable.text = String.init(format: "%.f", notFinishCount)
        
        var rateStr = "0"
        if totalCount == 0 {
            rateStr = "0"
        }else{
            rateStr = String.init(format: "%.2f", finishCount/totalCount)
        }
        
        
        let rate = Float(rateStr)!*Float(100)
        
        let newRateStr = String.init(format: "%.1f", rate)
        
        let text = newRateStr + "%"
        
        let rateRange = NSRange.init(location: 0, length: newRateStr.count)
        
        let unitRange = NSRange.init(location: newRateStr.count, length: 1)
        
        let attriStr = NSMutableAttributedString.init(string: text)
        attriStr.addAttribute(.font, value: UIFont.systemFont(ofSize: 24), range: rateRange)
        attriStr.addAttribute(.font, value: UIFont.systemFont(ofSize: 15), range: unitRange)

        self.rateOfFinishLable.attributedText = attriStr
        self.finishTaskNumLable.text = String.init(format: "%.f", finishCount )
        
    }
    
    func drawCircle() {
        
        //完成度 开始 结束角度
        let startAngle = CGFloat(Double.pi/2*3)
        var endAngle = CGFloat()
        let finishRate = finishCount/totalCount
        if finishRate > 0.25 {
            //完成率在0~270度之间
            endAngle = CGFloat(Double.pi*2*Double(finishCount/totalCount) - Double.pi/2)
            
        }else{
            //完成率在270～0度之间
            endAngle = CGFloat(Double.pi*2*Double(finishCount/totalCount) + Double.pi/2*3)
            
        }
        
        
        ///背景圆环贝塞尔曲线
        let radius = CGFloat(self.bounds.size.height * 0.65/2)
        let lineWidth = 0.02*self.bounds.size.width
        let bezierPath = UIBezierPath.init(arcCenter: self.center, radius: radius, startAngle: 0, endAngle: CGFloat(Double.pi*2), clockwise: true)
        
        ///背景圆环
        let circleLayer = CAShapeLayer.init()
        circleLayer.lineWidth = lineWidth
        circleLayer.strokeColor = UIColor.white.cgColor
        circleLayer.fillColor = UIColor.clear.cgColor
        circleLayer.path = bezierPath.cgPath
        self.layer.addSublayer(circleLayer)
        
        ///完成率圆环曲线
        let progressPath = UIBezierPath.init(arcCenter: self.center, radius: radius, startAngle: startAngle, endAngle: endAngle, clockwise: true)
    
        ///完成率圆环
        let progressLayer = CAShapeLayer.init()
        progressLayer.path = progressPath.cgPath
        progressLayer.strokeColor = #colorLiteral(red: 0.1785240769, green: 0.7347866893, blue: 0.8135085702, alpha: 1).cgColor
        progressLayer.lineWidth = lineWidth
        progressLayer.fillColor = UIColor.clear.cgColor
        progressLayer.lineCap = .round
        self.layer.addSublayer(progressLayer)
        

        ///开始圆点
        let beginDotStartAngle = startAngle + dotAngle
        let beginDotEndAngle = startAngle + 2*dotAngle
        let beginDotPath = UIBezierPath.init(arcCenter: self.center, radius: radius, startAngle: beginDotStartAngle, endAngle: beginDotEndAngle, clockwise: true)

        let beginLayer = CAShapeLayer.init()
        beginLayer.path = beginDotPath.cgPath
        beginLayer.strokeColor = UIColor.white.cgColor
        beginLayer.lineWidth = lineWidth*0.6
        beginLayer.fillColor = UIColor.clear.cgColor
        beginLayer.lineCap = .round
        self.layer.addSublayer(beginLayer)


        ///结束圆点
        let endDotStartAngle = endAngle - 2*dotAngle
        let endDotEndAngle = endAngle - dotAngle
        let endDotPath = UIBezierPath.init(arcCenter: self.center, radius: radius, startAngle: endDotStartAngle, endAngle: endDotEndAngle, clockwise: true)

        let endLayer = CAShapeLayer.init()
        endLayer.path = endDotPath.cgPath
        endLayer.strokeColor = UIColor.white.cgColor
        endLayer.lineWidth = lineWidth*0.6
        endLayer.fillColor = UIColor.clear.cgColor
        endLayer.lineCap = .round
        self.layer.addSublayer(endLayer)
        

    }
    
    

}
