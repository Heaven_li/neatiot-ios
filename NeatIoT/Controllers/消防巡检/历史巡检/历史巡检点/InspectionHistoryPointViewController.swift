//
//  InspectionHistoryPointViewController.swift
//  NeatIoT
//
//  Created by neat on 2019/12/18.
//  Copyright © 2019 Neat. All rights reserved.
//

import UIKit

class InspectionHistoryPointViewController: SupperViewController {
    
    
    let cellIdent = "InspectionHistoryPointTableViewCell"
    
    var pointArr:Array<InspectionHistoryPointModel>?

    @IBOutlet weak var containTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.titleName = "历史巡检点"
        
        containTableView.delegate = self
        containTableView.backgroundColor = .clear
        containTableView.dataSource = self
        containTableView.separatorStyle = .none
        containTableView.register(UINib.init(nibName: "InspectionHistoryPointTableViewCell", bundle: Bundle.main), forCellReuseIdentifier: cellIdent)
        

        // Do any additional setup after loading the view.
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension InspectionHistoryPointViewController:UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.01
    }
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView.init()
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 78
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = InspectionPointDetailViewController.init()
        vc.historyPointModel = self.pointArr![indexPath.row]
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}
extension InspectionHistoryPointViewController:UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.pointArr!.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdent, for: indexPath) as! InspectionHistoryPointTableViewCell
        cell.selectionStyle = .none
        cell.configerCell(model: self.pointArr![indexPath.row])
        
        let roundPath = UIBezierPath.init(roundedRect: CGRect.init(x: 0, y: 0, width: NormalConstant.ScreenWidth - 20, height: 78 - 10), cornerRadius: 5)
        
        let maskLayer = CAShapeLayer.init()
        maskLayer.path = roundPath.cgPath
        cell.bgView.layer.mask = maskLayer
        
        return cell
    }
    
    
    
    
}
