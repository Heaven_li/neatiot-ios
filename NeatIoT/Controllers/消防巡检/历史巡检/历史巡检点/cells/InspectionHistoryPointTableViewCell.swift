//
//  InspectionHistoryPointTableViewCell.swift
//  NeatIoT
//
//  Created by neat on 2019/12/18.
//  Copyright © 2019 Neat. All rights reserved.
//

import UIKit

class InspectionHistoryPointTableViewCell: UITableViewCell {

    @IBOutlet weak var pointNameLable: UILabel!
    @IBOutlet weak var pointAddressLable: UILabel!
    
    @IBOutlet weak var bgView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configerCell(model:InspectionHistoryPointModel) {
        
        self.pointNameLable.text = model.pointName
        
        self.pointAddressLable.text = "详细地址:"+model.address!
        
    }
    
}
