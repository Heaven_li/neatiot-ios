//
//  InspectionHistoryTableViewCell.swift
//  NeatIoT
//
//  Created by neat on 2019/12/18.
//  Copyright © 2019 Neat. All rights reserved.
//

import UIKit

class InspectionHistoryTableViewCell: UITableViewCell {

    @IBOutlet weak var taskNameLable: UILabel!
    
    @IBOutlet weak var finishTimeLable: UILabel!
    
    @IBOutlet weak var finishStateImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configerCell(model:InspectionHistoryTaskModel) {
        
        
        self.taskNameLable.text = model.taskName
        
        if model.isTimeout! {
            self.finishStateImageView.isHighlighted = false
            self.finishTimeLable.text = "完成时间:"+"-"
        }else{
            
            self.finishStateImageView.isHighlighted = true
            self.finishTimeLable.text = "完成时间:"+model.realEndTime!
        }
        
        
        
        
        
    }
    
    
    
}
