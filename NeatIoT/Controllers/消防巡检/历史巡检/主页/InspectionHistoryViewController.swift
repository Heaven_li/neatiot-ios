//
//  InspectionHistoryViewController.swift
//  NeatIoT
//
//  Created by neat on 2019/12/16.
//  Copyright © 2019 Neat. All rights reserved.
//

import UIKit

class InspectionHistoryViewController: SupperTableViewViewController {

    let CellIdent = "InspectionHistoryTableViewCell"
    
    var menuModel:MenuModel?
    
    var currentPageIndex:Int = 1
    var currentKeyword = ""
    var currentFrequence = ""
    var currentFinishState = "" //1:已超时 2:已完成
    
    var dataSourceArr:Array<InspectionHistoryTaskModel> = []
    
    /// 当前编辑的筛选item
    var currentFilterModel:FilterItemModel = FilterItemModel()
    /// 当前编辑的筛选项目
    var currentFilterType = FilterType.DomainId
    /// 当前筛选项目数组
    var currentFilterArr:Array<FilterItemModel> = []
    
    lazy var topSearchView: TopSearchView = {
        let topSearchView = TopSearchView.init()
        topSearchView.delegate = self
        return topSearchView
    }()
    
//    lazy var listContainView: UITableView = {
//        let containView = UITableView.init()
//        containView.delegate = self
//        containView.dataSource = self
//        containView.backgroundColor = .clear
//        containView.separatorInset = UIEdgeInsets.init(top: 0, left: 10, bottom: 0, right: 10)
//        containView.register(UINib.init(nibName: "InspectionHistoryTableViewCell", bundle: Bundle.main), forCellReuseIdentifier: self.CellIdent)
//        return containView
//    }()
    
    lazy var filterView: FilterView = {
        
        let filterView = FilterView.init()
        filterView.delegate = self
        filterView.dataSource = FilterViewModel.initHistoryFilterData()
        return filterView
    }()
    
    lazy var inforSelectView: InforSelectView = {
        let inforSelectView = InforSelectView.init()
        inforSelectView.delegate = self
        return inforSelectView
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.titleName = self.menuModel?.name ?? "巡检历史"
        
        let filterNavItem = NavItemModel.init()
        filterNavItem.itemIcon = "nav_filter"
        self.rightItems = [filterNavItem]
        
        configerUI()

        getTaskList()
        
    }
    @objc override func navRRClick() -> Void {
        
        if self.filterView.isShow {
            self.filterView.dissmisFilterView()
        }else{
            self.filterView.showFilterView(view: self.view)
        }
        
    }
    
    func configerUI() {
        
        self.view.addSubview(self.topSearchView)
        
        self.listContainView.delegate = self
        self.listContainView.dataSource = self
        self.listContainView.separatorStyle = .singleLine
        self.listContainView.separatorInset = UIEdgeInsets.init(top: 0, left: 5, bottom: 0, right: 5)
        self.listContainView.register(UINib.init(nibName: "InspectionHistoryTableViewCell", bundle: Bundle.main), forCellReuseIdentifier: self.CellIdent)
        self.view.addSubview(self.listContainView)
        
        self.topSearchView.snp.makeConstraints { (make) in
            make.top.left.right.equalTo(self.view)
            make.height.equalTo(NormalConstant.TopSearchViewHeight)
        }
        
        self.listContainView.snp.makeConstraints { (make) in
            make.top.equalTo(self.topSearchView.snp.bottom)
            make.left.right.equalTo(self.view)
            make.bottom.equalTo(self.view)
        }
        
    }
    
    override func refreshAction() {
        
        currentPageIndex = 1
        
        NeatRequest.instance.getHistoryTaskList(keyword: currentKeyword, pageIndex: currentPageIndex, frequenceId: currentFrequence, isFinish: currentFinishState, success: { (response) in
            
            self.dataSourceArr = response.resultData!.taskArr!
            self.listContainView.reloadData()
            self.endRefresh(isUpdateFooter: true, dataCount: self.dataSourceArr.count, pageSize: 10)
            
        }) { (error) in
            NeatHud.showMessage(message: error.errMessage, view: self.view)
        }
        
    }
    
    override func loadMoreAction() {
        
        currentPageIndex+=1
        
        NeatRequest.instance.getHistoryTaskList(keyword: currentKeyword, pageIndex: currentPageIndex, frequenceId: currentFrequence, isFinish: currentFinishState, success: { (response) in
            
            let taskArr = response.resultData?.taskArr ?? []
                
            if taskArr.count == 0{
                self.currentPageIndex-=1
            }
            
            self.endLoadMoreData(dataCount:taskArr.count, pageSize: 10)
            
            self.dataSourceArr.append(contentsOf: response.resultData!.taskArr!)
            self.listContainView.reloadData()
            
        }) { (error) in
            self.currentPageIndex-=1
            self.endLoadMoreData(dataCount:0, pageSize: 10)
            NeatHud.showMessage(message: error.errMessage, view: self.view)
        }
        
    }
    
    func getTaskList() {
        
        self.listContainView.showLoadingView()
        
        NeatRequest.instance.getHistoryTaskList(keyword: currentKeyword, pageIndex: currentPageIndex, frequenceId: currentFrequence, isFinish: currentFinishState, success: { (response) in
            
            self.dataSourceArr.removeAll()
            self.dataSourceArr = response.resultData!.taskArr!
            self.endLoadMoreData(dataCount: self.dataSourceArr.count, pageSize: 10)
            if self.dataSourceArr.count == 0{
                self.listContainView.showEmptyView()
            }else{
                self.listContainView.hidenHolderView()
                self.listContainView.reloadData()
                
            }
            
        }) { (error) in
            
            if error.errCode == RequestResponseCode.dataParsingException {
                //数据异常
                NeatHud.dissmissHud(view: self.view)
                self.listContainView.showRequestFaildView {
                    
                }
            }else if error.errCode == RequestResponseCode.netCanNotContent || error.errCode == RequestResponseCode.requestTimeOut{
                //无网络
                NeatHud.dissmissHud(view: self.view)
                self.listContainView.showNetErrorView(errorMessage: error.errMessage) {
                    self.getTaskList()
                }
            }else{
                NeatHud.showMessage(message: error.errMessage, view: self.view)
            }
        }
        
    }
    
    
    

    /// 更新筛选UI数据源
    /// - Parameter filterModel: 筛选项model
    /// - Parameter valueModel: 选中valuemodel
    func reFormateData(filterModel: FilterItemModel,valueModel: KeyValueModel) {
        
        let filterDataSourceArr = self.filterView.dataSource
        
        ///赋值当前筛选项
        let ItemModel = filterDataSourceArr[filterModel.filterIndex]
        ItemModel.filterInfor = valueModel.item_name
        ItemModel.filterInforId = valueModel.item_id
        
    }

}

extension InspectionHistoryViewController:TopSearchViewDelegate{
    
    func doSearchWith(keyword: String) {
        
        currentKeyword = keyword
        currentPageIndex = 1
        
        getTaskList()
        
    }
    
}

extension InspectionHistoryViewController:UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.01
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView.init()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let vc = InspectionHistoryPointViewController.init()
        vc.pointArr = dataSourceArr[indexPath.row].resultPointList
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
}

extension InspectionHistoryViewController:UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
     
        return dataSourceArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: CellIdent, for: indexPath) as! InspectionHistoryTableViewCell
        cell.configerCell(model: dataSourceArr[indexPath.row])
        cell.selectionStyle = .none
        return cell
        
    }
    
}

extension InspectionHistoryViewController:FilterViewDelegate{
    
    func selectItem(model: FilterItemModel) {
        
        //更新当前编辑的筛选项
        currentFilterModel = model
    
        let configerModel = InforSelectConfigerModel.init()
        configerModel.isOccurFromFilter = true
        configerModel.inforTitle = model.filterTitle
        if model.filterType == .Frequency {
            configerModel.inforType = DeviceInforType.frequency
        }else if model.filterType == .FinishState{
            configerModel.inforType = DeviceInforType.finishRate
        }
        
        self.inforSelectView.showSelectView(view: self.view,model: configerModel)
        
        
    }
    
    func confirmFilter(modelArr: Array<FilterItemModel>) {
        
        for model in modelArr {
            if model.filterType == .Frequency {
                currentFrequence = model.filterInforId
            }else if model.filterType == .FinishState{
                currentFinishState = model.filterInforId
            }
        }
        
        self.getTaskList()
        
        self.filterView.dissmisFilterView()
        
    }
    
    func cancelFilter() {
        
        self.filterView.dissmisFilterView()
        
    }
    
}

extension InspectionHistoryViewController:InforSelectViewDelegate{
    
    func selectInfor(model: KeyValueModel) {
        
        self.reFormateData(filterModel: currentFilterModel, valueModel: model)

        currentFilterArr = self.filterView.dataSource
        
        self.filterView.updateUI()
        
    }
    
}
