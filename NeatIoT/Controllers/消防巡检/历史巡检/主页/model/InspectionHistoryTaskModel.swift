//
//  InspectionHistoryTaskModel.swift
//  NeatIoT
//
//  Created by neat on 2019/12/18.
//  Copyright © 2019 Neat. All rights reserved.
//

import UIKit
import HandyJSON
class InspectionHistoryTaskModel: HandyJSON {
    
    
    /// 任务id
    var id:String?
    /// 任务名称
    var taskName:String?
    /// 是否超时
    var isTimeout:Bool?
    /// 是否完成
    var isFinish:Bool?
    /// 任务状态
    var status:Int?
    /// 完成时间
    var endTime:String?
    /// 实际完成时间
    var realEndTime:String?
    /// 巡检点列表
    var resultPointList:Array<InspectionHistoryPointModel>?
    
    required init(){
        
    }

}
