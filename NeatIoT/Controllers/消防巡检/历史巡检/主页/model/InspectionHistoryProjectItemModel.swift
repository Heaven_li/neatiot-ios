//
//  InspectionHistoryProjectItemModel.swift
//  NeatIoT
//
//  Created by neat on 2019/12/18.
//  Copyright © 2019 Neat. All rights reserved.
//

import UIKit
import HandyJSON
class InspectionHistoryProjectItemModel: HandyJSON {
    
    /// 巡检项目id
    var id:String?
    /// 巡检结果id
    var resultPointId:String?
    /// 巡检项目名称
    var itemName:String?
    /// 巡检状态 0:未知  1:是  2:否
    var result:Int?
    
    required init(){
        
    }

}
