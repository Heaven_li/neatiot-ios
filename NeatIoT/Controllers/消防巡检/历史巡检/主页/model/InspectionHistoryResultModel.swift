//
//  InspectionHistoryResultModel.swift
//  NeatIoT
//
//  Created by neat on 2019/12/18.
//  Copyright © 2019 Neat. All rights reserved.
//

import UIKit
import HandyJSON
class InspectionHistoryResultModel: HandyJSON {
    
    
    /// 数据条数
    var totalCount:Int?
    
    /// 任务条数
    var taskArr:Array<InspectionHistoryTaskModel>?
    
    
    required init(){
        
    }
    
    func mapping(mapper: HelpingMapper) {
        mapper <<<
        self.taskArr <-- "data"
    }

}
