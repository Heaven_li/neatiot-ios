//
//  InspectionHistoryPointModel.swift
//  NeatIoT
//
//  Created by neat on 2019/12/18.
//  Copyright © 2019 Neat. All rights reserved.
//

import UIKit
import HandyJSON
class InspectionHistoryPointModel: HandyJSON {

    /// 巡检结果id
    var resultTaskId:String?
    /// 巡检点id
    var pointId:String?
    /// 巡检点名称
    var pointName:String?
    /// 巡检点位置
    var address:String?
    /// 巡检项目子类型
    var childTypeName:String?
    /// 企业名称
    var enterpriseName:String?
    /// 建筑名称
    var buildingName:String?
    /// 部位名称
    var keypartName:String?
    /// 完成时间
    var handleTime:String?
    /// 巡检项目
    var resultItemList:Array<InspectionHistoryProjectItemModel>?
    
    required init(){
        
    }
    
    func mapping(mapper: HelpingMapper) {
        mapper <<<
        self.pointId <-- "id"
    }
}
