//
//  InspectionPointDetailViewController.swift
//  NeatIoT
//
//  Created by neat on 2019/12/17.
//  Copyright © 2019 Neat. All rights reserved.
//

import UIKit

class InspectionPointDetailViewController: SupperViewController {

    let rowHeight:CGFloat = 46
    let cellIndent = "InspectionInforTableViewCell"
    
    var taskPointModel:InspectionTaskPointModel?
    
    var historyPointModel:InspectionHistoryPointModel?
    
    var bindPointModel:InspectionBindPointModel?
    
    var dataSourceArr:Array<Array<TitleInforModel>> = []
    
    @IBOutlet weak var containTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.titleName = "巡检点详情"
        
        containTableView.delegate = self
        containTableView.dataSource = self
        containTableView.separatorInset = UIEdgeInsets.init(top: 0, left: 10, bottom: 0, right: 10)
        
        containTableView.register(UINib.init(nibName: "InspectionInforTableViewCell", bundle: Bundle.main), forCellReuseIdentifier: cellIndent)

        // Do any additional setup after loading the view.
        formateDataSource()
    }
    
    func formateDataSource() {
        
        var pointInforArr:Array<TitleInforModel> = []
        var projectInforArr:Array<TitleInforModel> = []
        
        let nameModel = TitleInforModel.init()
        nameModel.titleText = "巡检点名称"
        if self.taskPointModel != nil {
            nameModel.inforText = self.taskPointModel?.pointName
        }else if self.historyPointModel != nil {
            nameModel.inforText = self.historyPointModel?.pointName
        }else if self.bindPointModel != nil {
            nameModel.inforText = self.bindPointModel?.pointName
        }
        
        pointInforArr.append(nameModel)
        
        let projectSubTypeModel = TitleInforModel.init()
        projectSubTypeModel.titleText = "项目子类型"
        if self.taskPointModel != nil {
            projectSubTypeModel.inforText = self.taskPointModel?.projectSubTypeName
        }else if self.historyPointModel != nil {
            projectSubTypeModel.inforText = self.historyPointModel?.childTypeName
        }else if self.bindPointModel != nil {
            projectSubTypeModel.inforText = self.bindPointModel?.projectSubType
        }
        
        pointInforArr.append(projectSubTypeModel)
        
        let entModel = TitleInforModel.init()
        entModel.titleText = "所属单位"
        if self.taskPointModel != nil {
            entModel.inforText = self.taskPointModel?.enterpriseName
        }else if self.historyPointModel != nil {
            entModel.inforText = self.historyPointModel?.enterpriseName
        }else if self.bindPointModel != nil {
            entModel.inforText = self.bindPointModel?.enterpriseName
        }
        
        pointInforArr.append(entModel)
        
        let buildingModel = TitleInforModel.init()
        buildingModel.titleText = "所属建筑"
        if self.taskPointModel != nil {
            buildingModel.inforText = self.taskPointModel?.buildingName
        }else if self.historyPointModel != nil {
            buildingModel.inforText = self.historyPointModel?.buildingName
        }else if self.bindPointModel != nil {
            buildingModel.inforText = self.bindPointModel?.buildingName
        }
        
        pointInforArr.append(buildingModel)
        
        let keypartModel = TitleInforModel.init()
        keypartModel.titleText = "所属部位"
        if self.taskPointModel != nil {
            keypartModel.inforText = self.taskPointModel?.keypartName
        }else if self.historyPointModel != nil {
            keypartModel.inforText = self.historyPointModel?.keypartName
        }else if self.bindPointModel != nil {
            keypartModel.inforText = self.bindPointModel?.keypartName
        }
        
        pointInforArr.append(keypartModel)
        
        let addressModel = TitleInforModel.init()
        addressModel.titleText = "详细地址"
        if self.taskPointModel != nil {
            addressModel.inforText = self.taskPointModel?.address
        }else if self.historyPointModel != nil {
            addressModel.inforText = self.historyPointModel?.address
        }else if self.bindPointModel != nil {
            addressModel.inforText = self.bindPointModel?.address
        }
        
        pointInforArr.append(addressModel)
    
        dataSourceArr.append(pointInforArr)
        
        if self.taskPointModel != nil {
            
            for projectModel in self.taskPointModel!.projectList! {
                
                let model = TitleInforModel.init()
                model.titleText = projectModel.projectName
                
                if self.taskPointModel?.isFinished ?? false {
                    //巡检任务已完成
                    if projectModel.hasTrouble == 0 {
                        model.inforText = "未知"
                    }else if projectModel.hasTrouble == 1{
                        model.inforText = "是"
                    }else if projectModel.hasTrouble == 2{
                        model.inforText = "否"
                    }
                }else{
                    model.inforText = "未知"
                }
                
                projectInforArr.append(model)
                
                
            }
            
        }else if self.historyPointModel != nil {
            
            for projectModel in self.historyPointModel!.resultItemList! {
                
                let model = TitleInforModel.init()
                model.titleText = projectModel.itemName
        
                if projectModel.result == 0 {
                    model.inforText = "未知"
                }else if projectModel.result == 1{
                    model.inforText = "是"
                }else if projectModel.result == 2{
                    model.inforText = "否"
                }
                
                
                projectInforArr.append(model)
                
            }
            
        }else if self.bindPointModel != nil {
            for projectModel in self.bindPointModel!.projectList! {
                
                let model = TitleInforModel.init()
                model.titleText = projectModel.projectName
                model.inforText = ""
                
                projectInforArr.append(model)
                
            }
        }
        
        
        dataSourceArr.append(projectInforArr)
        
        self.containTableView.reloadData()
        
        
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension InspectionPointDetailViewController:UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 1 {
            return rowHeight
        }else{
            return 0.01
        }
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.01
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if section == 1{
            let view = InspectionInforHeaderView.init()
            view.titleText = "巡检项目"
            return view
        }else{
            return UIView.init()
        }
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView.init()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return rowHeight
    }
    
}
extension InspectionPointDetailViewController:UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return dataSourceArr.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return dataSourceArr[section].count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIndent, for: indexPath) as! InspectionInforTableViewCell
        cell.configerCell(model: dataSourceArr[indexPath.section][indexPath.row])
        return cell
    }
    
    
}
