//
//  InspectionBindPointModel.swift
//  NeatIoT
//
//  Created by neat on 2019/12/20.
//  Copyright © 2019 Neat. All rights reserved.
//

import UIKit
import HandyJSON

class InspectionBindPointModel: HandyJSON {
    
    /// 点位id
    var pointId:String?
    /// 点位名称
    var pointName:String?
    /// 点位位置
    var address:String?
    /// 点位 二维码编号
    var qrCode:String?
    /// 点位 NFC编号
    var nfcCode:String?
    /// 点位 项目类型
    var projectType:String?
    /// 点位 项目子类型
    var projectSubType:String?
    /// 点位 企业名称
    var enterpriseName:String?
    /// 点位 建筑名称
    var buildingName:String?
    /// 点位 部位名称
    var keypartName:String?
    /// 点位 巡检项目数组
    var projectList:Array<InspectionBindProjectModel>?
    
    required init(){
        
    }
    
}
