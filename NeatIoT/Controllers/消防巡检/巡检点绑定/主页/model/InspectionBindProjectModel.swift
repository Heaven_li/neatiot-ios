//
//  InspectionBindProjectItem.swift
//  NeatIoT
//
//  Created by neat on 2019/12/20.
//  Copyright © 2019 Neat. All rights reserved.
//

import UIKit
import HandyJSON
class InspectionBindProjectModel: HandyJSON {
    
    /// 项目id
    var projectId:String?
    /// 项目名称
    var projectName:String?
    /// 项目状态
    var hasTrouble:Int?
    /// 所属点位id
    var pointId:String?

    required init(){
        
    }

}
