//
//  InspectionBindViewController.swift
//  NeatIoT
//
//  Created by neat on 2019/12/16.
//  Copyright © 2019 Neat. All rights reserved.
//

import UIKit

class InspectionBindViewController: SupperTableViewViewController {

    var menuModel:MenuModel?
    let CellIdent = "InspectionBindPointTableViewCell"
    var currentPageIndex:Int = 1
    var currentKeyword = ""
    var currentBindState = "0"
    
    var dataSourceArr:Array<InspectionBindPointModel> = []
    
    lazy var topSearchView: TopSearchView = {
        let topSearchView = TopSearchView.init()
        topSearchView.delegate = self
        return topSearchView
    }()
    
    lazy var filterView: NormalFilterView = {
        
        let filterView = NormalFilterView.init()
        filterView.delegate = self
//        0    all
//        1     二维码已绑定
//        2     NFC已绑定
//        3     二维码未绑定
//        4     NFC未绑定
//        5     未绑定
        let oneFilter = NormalFilterIItemModel.init()
        oneFilter.filterID = "4"
        oneFilter.filterName = "NFC未绑定"
        oneFilter.isSelect = false
        
        let twoFilter = NormalFilterIItemModel.init()
        twoFilter.filterID = "3"
        twoFilter.filterName = "二维码未绑定"
        twoFilter.isSelect = false
        
        let thirdFilter = NormalFilterIItemModel.init()
        thirdFilter.filterID = "2"
        thirdFilter.filterName = "NFC已绑定"
        thirdFilter.isSelect = false
        
        let fourFilter = NormalFilterIItemModel.init()
        fourFilter.filterID = "1"
        fourFilter.filterName = "二维码已绑定"
        fourFilter.isSelect = false
        
        let fiveFilter = NormalFilterIItemModel.init()
        fiveFilter.filterID = "0"
        fiveFilter.filterName = "全部"
        fiveFilter.isSelect = true
        
        filterView.dataSource = [oneFilter,twoFilter,thirdFilter,fourFilter,fiveFilter]
        
        
        return filterView
    }()
    
    override func viewWillAppear(_ animated: Bool) {
        currentPageIndex = 1
        currentKeyword = ""
        getBindPoint()
        
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.titleName = self.menuModel?.name ?? "巡检点绑定"
        
        let filterNavItem = NavItemModel.init()
        filterNavItem.itemIcon = "nav_filter"
        self.rightItems = [filterNavItem]

        configerUI()
        
    }
    override func navRRClick() -> Void {
    
        self.view.addSubview(self.filterView)
        
        self.filterView.snp.makeConstraints { (make) in
            make.edges.equalTo(self.view)
        }
        
    }
    
    func configerUI() {
        
        self.listContainView.delegate = self
        self.listContainView.dataSource = self
        self.listContainView.register(UINib.init(nibName: "InspectionBindPointTableViewCell", bundle: Bundle.main), forCellReuseIdentifier: self.CellIdent)
        self.view.addSubview(self.topSearchView)
        self.view.addSubview(self.listContainView)
        
        self.topSearchView.snp.makeConstraints { (make) in
            make.top.left.right.equalTo(self.view)
            make.height.equalTo(NormalConstant.TopSearchViewHeight)
        }
        
        self.listContainView.snp.makeConstraints { (make) in
            make.top.equalTo(self.topSearchView.snp.bottom)
            make.left.right.equalTo(self.view)
            make.bottom.equalTo(self.view)
        }
        
    }
    
    override func refreshAction() {
        
        currentPageIndex = 1
        
        NeatRequest.instance.getBindPointList(keyword: currentKeyword, pageIndex: currentPageIndex, bindState: currentBindState, success: { (response) in
            
            self.dataSourceArr.removeAll()
            self.dataSourceArr = response.resultData!.pointArr!
            self.endRefresh(isUpdateFooter: true, dataCount: self.dataSourceArr.count, pageSize: 10)
            self.listContainView.reloadData()
            
        }) { (error) in
            
            NeatHud.showMessage(message: error.errMessage, view: self.view)
            
        }
    }
    
    override func loadMoreAction() {
        
        currentPageIndex += 1
        
        NeatRequest.instance.getBindPointList(keyword: currentKeyword, pageIndex: currentPageIndex, bindState: currentBindState, success: { (response) in
            
            let pointArr = response.resultData?.pointArr ?? []
                
            if pointArr.count == 0{
                self.currentPageIndex-=1
            }
            self.dataSourceArr.append(contentsOf: response.resultData!.pointArr!) 
            self.endLoadMoreData(dataCount:pointArr.count, pageSize: 10)
            self.listContainView.reloadData()
            
        }) { (error) in
            self.currentPageIndex-=1
            self.endLoadMoreData(dataCount:0, pageSize: 10)
            NeatHud.showMessage(message: error.errMessage, view: self.view)
            
        }
    }
    
    func getBindPoint() {
        
        self.listContainView.showLoadingView()
        
        NeatRequest.instance.getBindPointList(keyword: currentKeyword, pageIndex: currentPageIndex, bindState: currentBindState, success: { (response) in
            
            self.dataSourceArr.removeAll()
            self.dataSourceArr = response.resultData!.pointArr!
            
            if self.dataSourceArr.count == 0{
                self.listContainView.showEmptyView()
            }else{
                self.listContainView.hidenHolderView()
                self.listContainView.reloadData()
                self.endLoadMoreData(dataCount: self.dataSourceArr.count, pageSize: 10)
            }
            
        }) { (error) in
            
            if error.errCode == RequestResponseCode.dataParsingException {
                //数据异常
                NeatHud.dissmissHud(view: self.view)
                self.listContainView.showRequestFaildView {
                    
                }
            }else if error.errCode == RequestResponseCode.netCanNotContent || error.errCode == RequestResponseCode.requestTimeOut{
                //无网络
                NeatHud.dissmissHud(view: self.view)
                self.listContainView.showNetErrorView(errorMessage: error.errMessage) {
                    self.getBindPoint()
                }
            }else{
                NeatHud.showMessage(message: error.errMessage, view: self.view)
            }
        }
        
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension InspectionBindViewController:TopSearchViewDelegate{
    
    func doSearchWith(keyword: String) {
        
        currentKeyword = keyword
        
        self.getBindPoint()
        
    }
    
}

extension InspectionBindViewController:UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 117
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView.init()
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.01
    }
    
}

extension InspectionBindViewController:UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
     
        return dataSourceArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: CellIdent, for: indexPath) as! InspectionBindPointTableViewCell
        cell.configerCell(model: dataSourceArr[indexPath.row])
        cell.selectionStyle = .none
        cell.delegate = self
        
        let roundPath = UIBezierPath.init(roundedRect: CGRect.init(x: 0, y: 0, width: NormalConstant.ScreenWidth - 20, height: 117 - 10), cornerRadius: 5)
        let maskLayer = CAShapeLayer.init()
        maskLayer.path = roundPath.cgPath
        cell.bgView.layer.mask = maskLayer
        
        return cell
        
    }
    
}

extension InspectionBindViewController:NormalFilterViewDelegate{
    
    func didSelectFilterItme(model: NormalFilterIItemModel) {
        currentBindState = model.filterID!
        self.getBindPoint()
    }
  
}

extension InspectionBindViewController:InspectionBindPointTableViewCellDelegate{
    
    
    func nfcTap(model: InspectionBindPointModel) {
        NeatHud.showMessage(message: "iOS系统手机暂不支持绑定NFC标签", view: self.view)
    }
    
    
    func detailTap(model: InspectionBindPointModel) {
        
        let detailVC = InspectionPointDetailViewController.init()
        detailVC.bindPointModel = model
        self.navigationController?.pushViewController(detailVC, animated: true)
      
        
    }
    
    func qrTap(model: InspectionBindPointModel) {
        
        let vcInitModel = VCInitModel.init()
        vcInitModel.navTitleText = "二维码扫描"
        
        let swiperVC = QRScanViewController.init()
        swiperVC.vcInit = vcInitModel
        swiperVC.bindPointModel = model
        self.navigationController?.pushViewController(swiperVC, animated: true)
        
    }

    
    
    
    
}
