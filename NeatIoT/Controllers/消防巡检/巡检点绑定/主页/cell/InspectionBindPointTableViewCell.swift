//
//  InspectionBindPointTableViewCell.swift
//  NeatIoT
//
//  Created by neat on 2019/12/20.
//  Copyright © 2019 Neat. All rights reserved.
//

import UIKit

protocol InspectionBindPointTableViewCellDelegate {
    func detailTap(model:InspectionBindPointModel)
    func qrTap(model:InspectionBindPointModel)
    func nfcTap(model:InspectionBindPointModel)
}

class InspectionBindPointTableViewCell: UITableViewCell {
    
    var currentModel:InspectionBindPointModel?
    var delegate:InspectionBindPointTableViewCellDelegate?
    
    @IBOutlet weak var bgView: UIView!
    
    @IBOutlet weak var pointNameLabel: UILabel!
    
    @IBOutlet weak var pointBindStateLabel: UILabel!
    
    @IBOutlet weak var pointAddressLabel: UILabel!
    
 
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
    }

    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configerCell(model:InspectionBindPointModel) {
        
        currentModel = model
        self.pointNameLabel.text = model.pointName
        
        var bindState = "未绑定"
        var bindStateColor = #colorLiteral(red: 0.3331107199, green: 0.3331621289, blue: 0.3330945373, alpha: 1)
        
        if ((model.qrCode != nil && !(model.qrCode?.isEmpty ?? true)) && (model.nfcCode != nil && !(model.nfcCode?.isEmpty ?? true))) {
            bindState = "已绑定"
            bindStateColor = #colorLiteral(red: 0.4426696189, green: 0.8888741048, blue: 0.3889332727, alpha: 1)
        }else if (model.qrCode != nil && !(model.qrCode?.isEmpty ?? true)) {
            bindState = "二维码已绑定"
            bindStateColor = #colorLiteral(red: 0.4426696189, green: 0.8888741048, blue: 0.3889332727, alpha: 1)
        }else if (model.nfcCode != nil && !(model.nfcCode?.isEmpty ?? true)) {
            bindState = "NFC已绑定"
            bindStateColor = #colorLiteral(red: 0.4426696189, green: 0.8888741048, blue: 0.3889332727, alpha: 1)
        }
        
        self.pointBindStateLabel.text = bindState
        self.pointBindStateLabel.textColor = bindStateColor
        
        self.pointAddressLabel.text = "详细地址:"+model.address!
        
    }
    
    @IBAction func detailBtnClick(_ sender: Any) {
        self.delegate!.detailTap(model: currentModel!)
    }
    @IBAction func qrBtnClick(_ sender: Any) {
        self.delegate!.qrTap(model: currentModel!)
    }
    @IBAction func nfcBtnClick(_ sender: Any) {
        self.delegate!.nfcTap(model: currentModel!)
    }
    
    
}
