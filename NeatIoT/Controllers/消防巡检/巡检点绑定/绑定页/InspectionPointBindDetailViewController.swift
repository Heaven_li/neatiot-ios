//
//  InspectionPointBindDetailViewController.swift
//  NeatIoT
//
//  Created by neat on 2019/12/25.
//  Copyright © 2019 Neat. All rights reserved.
//

import UIKit

class InspectionPointBindDetailViewController: SupperViewController {
    
    
    
    var vcInitModel:VCInitModel?
    
    var qrModel:InspectionPointQRModel?
    
    var pointModel:InspectionBindPointModel?
    
    @IBOutlet weak var tagCodeLable: UILabel!
    
    @IBOutlet weak var pointIdLable: UILabel!
    
    @IBOutlet weak var pointNameLable: UILabel!
    
    @IBOutlet weak var pointAddressLable: UILabel!
    
    @IBOutlet weak var pointTypeLable: UILabel!
    
    @IBOutlet weak var pointSubTypeLable: UILabel!
    
    @IBOutlet weak var pointEntLable: UILabel!
    
    @IBOutlet weak var pointBuildingLable: UILabel!
    
    @IBOutlet weak var pointKeypartLable: UILabel!
    
    @IBOutlet weak var bindBtn: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.isCanSwiperBack = false
        self.titleName = vcInitModel?.navTitleText ?? "巡检点绑定"
        
        configerUI()

    }
    
    func configerUI() {
        
        self.bindBtn.layer.cornerRadius = 5
        self.bindBtn.clipsToBounds = true
        
        if self.titleName == "二维码" {
            self.tagCodeLable.text = "二维码编号:"+qrModel!.p!
        }else if self.titleName == "NFC"{
            self.tagCodeLable.text = "NFC编号:"+qrModel!.p!
        }
        
        self.pointNameLable.text = "巡检点名称:"+self.pointModel!.pointName!
        
        self.pointIdLable.text = self.pointModel!.pointId!
        
        self.pointAddressLable.text = "详细位置:"+self.pointModel!.address!
        
        self.pointTypeLable.text = "项目类型:"+self.pointModel!.projectType!
        
        self.pointSubTypeLable.text = "项目子类型:"+self.pointModel!.projectSubType!
        
        self.pointEntLable.text = "所属单位:"+self.pointModel!.enterpriseName!
        
        self.pointBuildingLable.text = "所属建筑:"+self.pointModel!.buildingName!
        
        self.pointKeypartLable.text = "所属部位:"+self.pointModel!.keypartName!
        
    }
    
    override func backClick() {
        
        self.popTo(vcClass: InspectionBindViewController.init())
        
    }


    @IBAction func bindBtnClick(_ sender: Any) {
        
        let tagType = String.init(format: "%d", self.qrModel!.t!)
        
        NeatRequest.instance.bindPoint(tagCode: self.qrModel!.p!, forceBind: "false", pointId: self.pointModel!.pointId!, tagType: tagType, success: { (response) in
            
            
            if response.code == 200{
                NeatHud.showMessage(message: "绑定成功", view: self.view) {
                    self.popTo(vcClass: InspectionBindViewController.init())
                }
            }else{
                if response.bindResult?.oldBindPoints?.count != 0 {
                
                        let messageStr = "该标签已被”"+(response.bindResult?.oldBindPoints![0])!+"”巡检点绑定，点击确定将与原点位解除绑定并绑定到该点位，是否确定？"
        
                        self.showAlert(vc: self, title: nil, message: messageStr, leftTitle: "取消绑定", rightTitle: "强制绑定", leftAction: { (action) in
                            self.popTo(vcClass: InspectionBindViewController.init())
                        }) { (action) in
                            self.forceBindPoint()
                        }
                    
                }
            }
            
        }) { (error) in
            NeatHud.showMessage(message: error.errMessage, view: self.view)
        }
    
    }
    
    func forceBindPoint() {
        
        let tagType = String.init(format: "%d", self.qrModel!.t!)
        
        NeatHud.showLoading(view: self.view, message: "")
        
        NeatRequest.instance.bindPoint(tagCode: self.qrModel!.p!, forceBind: "true", pointId: self.pointModel!.pointId!, tagType: tagType, success: { (response) in
            
            NeatHud.showMessage(message: "绑定成功", view: self.view) {
                self.popTo(vcClass: InspectionBindViewController.init())
            }
            
        }) { (error) in
            NeatHud.showMessage(message: error.errMessage, view: self.view)
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
