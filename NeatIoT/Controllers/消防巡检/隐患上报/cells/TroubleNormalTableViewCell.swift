//
//  TroubleNormalTableViewCell.swift
//  NeatIoT
//
//  Created by Yang Blus on 2020/2/7.
//  Copyright © 2020 Neat. All rights reserved.
//

import UIKit

protocol TroubleNormalTableViewCellDelegate {
    
    func didTapInforItem(model:ReportItemModel)
}


class TroubleNormalTableViewCell: UITableViewCell {
    
    @IBOutlet weak var titleLable: UILabel!
    
    @IBOutlet weak var inforLable: UILabel!
    
    @IBOutlet weak var arrowImageView: UIImageView!
    
    var delegate:TroubleNormalTableViewCellDelegate?
    
    var currentModel:ReportItemModel?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        let tap = UITapGestureRecognizer.init(target: self, action: #selector(tapGesture(tap:)))
        inforLable.isUserInteractionEnabled = true
        inforLable.addGestureRecognizer(tap)
        
        // Initialization code
    }
    
    func configerCell(model:ReportItemModel) {
        
        currentModel = model
        
        self.titleLable.text = model.titleText
        
        self.inforLable.text = model.inforText
        
        if model.isCanEdit {
            self.arrowImageView.isHidden = false
        }else{
            self.arrowImageView.isHidden = true
        }
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @objc func tapGesture(tap:UITapGestureRecognizer) {
        
        self.delegate?.didTapInforItem(model: currentModel!)
        
    }
    
}
