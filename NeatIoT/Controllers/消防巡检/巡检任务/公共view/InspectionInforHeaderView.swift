//
//  InspectionInforHeaderView.swift
//  NeatIoT
//
//  Created by neat on 2019/12/17.
//  Copyright © 2019 Neat. All rights reserved.
//

import UIKit

class InspectionInforHeaderView: UIView {
    
    lazy var lable: UILabel = {
        let lable = UILabel.init()
        lable.textAlignment = .left
        lable.font = UIFont.systemFont(ofSize: 15)
        lable.textColor = #colorLiteral(red: 0.3803921569, green: 0.3803921569, blue: 0.3803921569, alpha: 1)
        return lable
    }()
    
    var titleText: String {
        get {
            self.lable.text ?? ""
        }
        set {
            self.lable.text = newValue
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.backgroundColor = #colorLiteral(red: 0.9215686275, green: 0.9215686275, blue: 0.9215686275, alpha: 1)
        
        self.addSubview(self.lable)
        self.lable.snp.makeConstraints { (make) in
            make.left.equalTo(self).offset(20)
            make.right.equalTo(self).offset(-20)
            make.top.bottom.equalTo(self)
        }
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
