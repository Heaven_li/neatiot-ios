//
//  InspectionInforTableViewCell.swift
//  NeatIoT
//
//  Created by neat on 2019/12/17.
//  Copyright © 2019 Neat. All rights reserved.
//

import UIKit

class InspectionInforTableViewCell: UITableViewCell {

    @IBOutlet weak var titleLable: UILabel!
    
    @IBOutlet weak var inforLable: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configerCell(model:TitleInforModel) {
        
        self.titleLable.text = model.titleText
        self.inforLable.text = model.inforText
        
        if model.inforText == "未知" {
            self.inforLable.textColor = #colorLiteral(red: 0.3288772702, green: 0.328928113, blue: 0.3288612664, alpha: 1)
        }else if model.inforText == "是"{
            self.inforLable.textColor = #colorLiteral(red: 0.2850131094, green: 0.7037432194, blue: 0.3315860927, alpha: 1)
        }else if model.inforText == "否"{
            self.inforLable.textColor = #colorLiteral(red: 0.9526024608, green: 0.4205438658, blue: 0.6046893169, alpha: 1)
        }
        
        
    }
    
}
