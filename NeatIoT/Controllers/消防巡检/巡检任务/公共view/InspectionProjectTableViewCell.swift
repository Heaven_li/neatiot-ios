//
//  InspectionProjectTableViewCell.swift
//  NeatIoT
//
//  Created by neat on 2019/12/25.
//  Copyright © 2019 Neat. All rights reserved.
//

import UIKit

class InspectionProjectTableViewCell: UITableViewCell {
    
    var currentProjectModel:InspectionProjectItemModel?
    
    lazy var projectNameLabel: UILabel = {
        let lable = UILabel.init()
        lable.textColor = #colorLiteral(red: 0.1580955982, green: 0.1676715016, blue: 0.209913075, alpha: 1)
        lable.font = UIFont.systemFont(ofSize: 16)
        return lable
    }()
    
    /// 是
    lazy var tureRadioBtn: RadioCheckBox = {
        let checkBox = RadioCheckBox.init()
        checkBox.delegate = self
        checkBox.tag = 1
        return checkBox
    }()

    /// 否
    lazy var falseRadioBtn: RadioCheckBox = {
        let checkBox = RadioCheckBox.init()
        checkBox.delegate = self
        checkBox.tag = 2
        return checkBox
    }()
    

    override func awakeFromNib() {
        super.awakeFromNib()
       
        
        self.addSubview(self.projectNameLabel)
        
        self.addSubview(self.tureRadioBtn)
        
        self.addSubview(self.falseRadioBtn)
        
        self.projectNameLabel.snp.makeConstraints { (make) in
            make.leading.trailing.equalTo(10)
            make.top.equalTo(14)
        }
        
        self.tureRadioBtn.snp.makeConstraints { (make) in
            make.leading.equalTo(self.projectNameLabel)
            make.top.equalTo(self.projectNameLabel.snp.bottom).offset(6)
            make.bottom.equalTo(self).offset(14)
        }
        
        self.falseRadioBtn.snp.makeConstraints { (make) in
            make.leading.equalTo(self.tureRadioBtn.snp.trailing).offset(20)
            make.top.bottom.equalTo(self.tureRadioBtn)
        }
        
        
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
    func configerCell(model:InspectionProjectItemModel) {
        
        currentProjectModel = model
        
        self.projectNameLabel.text = model.projectName
        
        self.tureRadioBtn.checkState = true
        self.falseRadioBtn.checkState = false
        
    }
    
}
extension InspectionProjectTableViewCell:RadioCheckBoxDelegate{
    
    func checkBoxStateChanged(isSelect:Bool,view:RadioCheckBox) {
        
        if view.tag == 1{
            
            currentProjectModel?.hasTrouble = 1
            if isSelect {
                if self.falseRadioBtn.checkState {
                    self.falseRadioBtn.checkState = false
                }
                
                self.tureRadioBtn.isUserInteractionEnabled = true
                self.falseRadioBtn.isUserInteractionEnabled = false
            }
            
//            debugPrint("点击 模拟量",isSelect,"数字量",self.digitalQuantityRadioBtn.checkState)
            
        }else if view.tag == 2{
    
            currentProjectModel?.hasTrouble = 2
            if isSelect {
                if self.tureRadioBtn.checkState {
                    self.tureRadioBtn.checkState = false
                }
                self.falseRadioBtn.isUserInteractionEnabled = true
                self.tureRadioBtn.isUserInteractionEnabled = false
            }
            
//            debugPrint("点击 数字量",isSelect,"模拟量",self.analogQuantityRadioBtn.checkState)
            
        }
        
//        self.delegate?.checkCellDidChange(model: currentModel)
    }
    
    
    
    
}
