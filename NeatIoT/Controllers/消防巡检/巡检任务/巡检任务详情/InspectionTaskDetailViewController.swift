//
//  InspectionTaskDetailViewController.swift
//  NeatIoT
//
//  Created by neat on 2019/12/16.
//  Copyright © 2019 Neat. All rights reserved.
//

import UIKit

class InspectionTaskDetailViewController: SupperViewController {
    
    var taskInforModel:TaskItemModel?
    
    @IBOutlet weak var taskNameLable: UILabel!
    
    @IBOutlet weak var frequenceLable: UILabel!
    
    @IBOutlet weak var entNameLable: UILabel!
    
    @IBOutlet weak var inspectionPersonLable: UILabel!
    
    @IBOutlet weak var startDateLabel: UILabel!
    
    @IBOutlet weak var endDateLabel: UILabel!
    
    @IBOutlet weak var startTimeLabel: UILabel!
    
    @IBOutlet weak var endTimeLabel: UILabel!
    
    @IBOutlet weak var startTaskBtn: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.titleName = "巡检任务详情"
        
        self.startTaskBtn.layer.cornerRadius = 5
        
        self.taskNameLable.text = taskInforModel?.taskName
        self.frequenceLable.text = taskInforModel?.frequenceName
        self.entNameLable.text = taskInforModel?.enterpriseName
        self.inspectionPersonLable.text = taskInforModel?.executorName
        
        
        
        if !(taskInforModel?.beginTime?.isEmpty ?? true) {
            let charArr = taskInforModel?.beginTime?.split(separator: " ")
            let startDate = String(charArr?[0] ?? "")
            let startTime = String(charArr?[1] ?? "")
            self.startDateLabel.text = startDate
            self.startTimeLabel.text = startTime
            
        }
        if !(taskInforModel?.endTime?.isEmpty ?? true) {
            let charArr = taskInforModel?.endTime?.split(separator: " ")
            let endData = String(charArr?[0] ?? "")
            let endTime = String(charArr?[1] ?? "")
            self.endDateLabel.text = endData
            self.endTimeLabel.text = endTime
        }
        

    }


    @IBAction func startTaskBtnClick(_ sender: Any) {
        
        let vc = InspectionTaskPointListViewController.init()
        vc.taskInforModel = self.taskInforModel
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
