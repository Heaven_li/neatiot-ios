//
//  TaskResultUploadModel.swift
//  NeatIoT
//
//  Created by Yang Blus on 2020/2/4.
//  Copyright © 2020 Neat. All rights reserved.
//

import UIKit
import HandyJSON

class TaskResultUploadModel:HandyJSON,Encodable {
    
    /// 巡检点ID
    var PointId:String?
    
    /// 巡检任务ID
    var ResultPointId:String?
    
    /// 巡检项目数组
    var ItemResultList:Array<ItemResultModel> = []

    required init(){
        
    }

}

class ItemResultModel: HandyJSON,Encodable {
    
    
    /// 巡检项ID
    var ItemId:String?
    
    /// 巡检项检查结果
    var ItemResult:String?
    
    required init()
    {}
}
