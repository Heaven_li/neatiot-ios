//
//  InspectionResultCheckTableViewCell.swift
//  NeatIoT
//
//  Created by Yang Blus on 2020/2/3.
//  Copyright © 2020 Neat. All rights reserved.
//

import UIKit

class InspectionResultCheckTableViewCell: UITableViewCell {

    @IBOutlet weak var titleLable: UILabel!
    
    @IBOutlet weak var resultLable: UILabel!
    
    @IBOutlet weak var sepLine: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configerUI(model:InspectionProjectItemModel) {
        
        self.titleLable.text = model.projectName
        
        if model.hasTrouble == 1 {
            self.resultLable.text = "是"
        }else if model.hasTrouble == 2{
            self.resultLable.text = "否"
        }
        
        
    }
}
