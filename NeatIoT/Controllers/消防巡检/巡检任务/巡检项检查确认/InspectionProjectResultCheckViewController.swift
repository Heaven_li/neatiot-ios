//
//  InspectionProjectResultCheckViewController.swift
//  NeatIoT
//
//  Created by Yang Blus on 2020/2/3.
//  Copyright © 2020 Neat. All rights reserved.
//

import UIKit

class InspectionProjectResultCheckViewController: SupperTableViewViewController {
    
    var taskPointModel:InspectionTaskPointModel?
    private let CellIdent = "InspectionProjectCheckTableViewCell"
    var isFromFastCheck:Bool = false
    
    lazy var checkFinishBtn: UIButton = {
        let btn = UIButton.init(type: UIButton.ButtonType.custom)
        btn.backgroundColor = #colorLiteral(red: 0.9386304396, green: 0.5230231393, blue: 0.4963629327, alpha: 1)
        btn.addTarget(self, action: #selector(finishCheckClick), for: .touchUpInside)
        btn.setTitle("确认保存", for: .normal)
        btn.setTitleColor(UIColor.white, for: .normal)
        return btn
    }()
    
    lazy var reCheckBtn: UIButton = {
        let btn = UIButton.init(type: UIButton.ButtonType.custom)
        btn.setTitle("重新检查", for: .normal)
        btn.setTitleColor(UIColor.white, for: .normal)
        btn.addTarget(self, action: #selector(reCheckClick), for: .touchUpInside)
        btn.backgroundColor = #colorLiteral(red: 0.001834133524, green: 0.09896785766, blue: 0.3033299148, alpha: 1)
        return btn
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.titleName = "巡检项检查结果"
        self.configerUI()
        
    }
    
    func configerUI() {
        
        self.listContainView.delegate = self
        self.listContainView.dataSource = self
        self.listContainView.separatorInset = UIEdgeInsets.init(top: 0, left: 10, bottom: 0, right: 10)
        self.listContainView.separatorStyle = .singleLine
        self.listContainView.backgroundColor = .clear
        self.listContainView.register(UINib.init(nibName: "InspectionResultCheckTableViewCell", bundle: Bundle.main), forCellReuseIdentifier: CellIdent)
        self.view.addSubview(self.listContainView)
        self.view.addSubview(self.checkFinishBtn)
        self.view.addSubview(self.reCheckBtn)
        self.listContainView.mj_footer!.isHidden = true
        self.listContainView.mj_header!.isHidden = true
        
        self.listContainView.snp.makeConstraints { (make) in
            make.top.equalTo(self.view)
            make.leading.trailing.equalTo(self.view)
            make.bottom.equalTo(self.reCheckBtn.snp.top).offset(-10)
            
        }
        
        self.checkFinishBtn.layer.cornerRadius = 5
        self.checkFinishBtn.snp.makeConstraints { (make) in
            make.bottom.equalTo(self.view).offset(-10)
            make.leading.equalTo(self.view).offset(20)
            make.trailing.equalTo(self.view).offset(-20)
            make.height.equalTo(self.view).multipliedBy(0.06)
        }
        
        self.reCheckBtn.layer.cornerRadius = 5
        self.reCheckBtn.snp.makeConstraints { (make) in
            make.bottom.equalTo(self.checkFinishBtn.snp.top).offset(-8)
            make.leading.trailing.equalTo(self.checkFinishBtn)
            make.height.equalTo(self.checkFinishBtn)
        }
        
        
    }
    
    @objc func finishCheckClick() {
        
        for mode in taskPointModel!.projectList! {
            if mode.hasTrouble == 2 {
                //隐患上报
                let inspectionReportVC = InspectionReportViewController.init()
                inspectionReportVC.taskPointModel = self.taskPointModel
                self.navigationController?.pushViewController(inspectionReportVC, animated: true)
                return
            }
        }
        
        let resultModel = TaskResultUploadModel.init()
        resultModel.PointId = self.taskPointModel!.pointId
        resultModel.ResultPointId = self.taskPointModel!.resultPointId
        
        for model in self.taskPointModel!.projectList! {
            
            let uploadModel = ItemResultModel.init()
            uploadModel.ItemId = model.projectId
            uploadModel.ItemResult = String.init(model.hasTrouble!)
            
            resultModel.ItemResultList.append(uploadModel)
        }
        
        
        NeatRequest.instance.TaskResultUpload(resultModel: resultModel, success: { (response) in
            
            NeatHud.showMessage(message: response.message!, view: self.view) {
                if self.isFromFastCheck{
                    self.popTo(vcClass: InspectionViewController.init())
                }else{
                    self.popTo(vcClass: InspectionTaskPointListViewController.init())
                }
                
            }
            
        }) { (error) in
            NeatHud.showMessage(message: error.errMessage, view: self.view)
        }
    
    }
    
    @objc func reCheckClick() {
        
        self.navigationController?.popViewController(animated: true)
        
    }
    
}

extension InspectionProjectResultCheckViewController:UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 46
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        UIView.init()
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        0.01
    }
    
}

extension InspectionProjectResultCheckViewController:UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.taskPointModel!.projectList!.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: CellIdent, for: indexPath) as! InspectionResultCheckTableViewCell
        
        let model = self.taskPointModel!.projectList![indexPath.row]
        cell.configerUI(model: model)
        cell.selectionStyle = .none
        
        return cell
        
    }
    
    
}
