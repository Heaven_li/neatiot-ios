//
//  FrequenceModel.swift
//  NeatIoT
//
//  Created by neat on 2019/12/18.
//  Copyright © 2019 Neat. All rights reserved.
//

import UIKit
import HandyJSON
/// 频率item model
class FrequenceModel: HandyJSON {
    
    /// 频率id
    var id:String?
    
    /// 频率名称
    var rateName:String?
    
    required init(){
        
    }

}
