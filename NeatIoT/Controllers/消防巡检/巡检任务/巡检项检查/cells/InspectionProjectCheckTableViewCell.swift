//
//  InspectionProjectCheckTableViewCell.swift
//  NeatIoT
//
//  Created by neat on 2020/1/19.
//  Copyright © 2020 Neat. All rights reserved.
//

import UIKit

class InspectionProjectCheckTableViewCell: UITableViewCell {
    
    var currentModel:InspectionProjectItemModel?
    
    lazy var titleLable: UILabel = {
        let lable = UILabel.init()
        lable.textColor = #colorLiteral(red: 0.3098039329, green: 0.3098039329, blue: 0.3098039329, alpha: 1)
        return lable
    }()
    
    lazy var checkView: CheckRadioGroupView = {
        
        let checkView = CheckRadioGroupView.init()
        checkView.isMuitleCheck = false
        checkView.delegate = self
        
        return checkView
    }()

    override func awakeFromNib() {
        super.awakeFromNib()
        
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    func configerUI(model:InspectionProjectItemModel) {
        
        self.backgroundColor = .white
        
        currentModel = model
        self.titleLable.text = model.projectName;
        
        self.addSubview(self.titleLable)
        self.titleLable.snp.makeConstraints { (make) in
            make.top.equalTo(self).offset(8)
            make.right.equalTo(self).offset(-10)
            make.left.equalTo(self).offset(10)
        }
        
        self.addSubview(self.checkView)
        
        checkView.snp.makeConstraints { (make) in
        
            make.leading.equalTo(self.titleLable.snp.leading)
            make.bottom.equalTo(self.snp.bottom).offset(-8)
            make.width.equalTo(160)
            make.height.equalTo(34)
            
        }
        
        let itemOne = CheckRadioItemModel.init()
        itemOne.isRadioCheck = true
        itemOne.radioTitle = "是"
        itemOne.radioKey = "1"
        
        let itemTwo = CheckRadioItemModel.init()
        itemTwo.isRadioCheck = false
        itemTwo.radioTitle = "否"
        itemTwo.radioKey = "2"
        
        checkView.radioItemArr = [itemOne,itemTwo]
        
 
    }
    
}

extension InspectionProjectCheckTableViewCell:CheckRadioGroupViewDelegate{
    
    func checkRadioGroupValueChanged(checkRadioItems: [CheckRadioItemModel]) {
        
        for item in checkRadioItems {
                   
            debugPrint("当前选中项目：",item.radioKey,item.radioTitle)
            currentModel?.hasTrouble = Int(item.radioKey)
                   
        }
        
    }
    
}
