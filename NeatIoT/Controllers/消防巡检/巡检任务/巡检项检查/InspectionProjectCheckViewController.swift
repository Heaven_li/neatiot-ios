//
//  InspectionProjectCheckViewController.swift
//  NeatIoT
//
//  Created by neat on 2019/12/25.
//  Copyright © 2019 Neat. All rights reserved.
//

import UIKit

class InspectionProjectCheckViewController: SupperTableViewViewController {
    
    var isFromQuiclyInspection:Bool?
    var taskPointModel:InspectionTaskPointModel?
    var qrModel:InspectionPointQRModel?
    var vcInit:VCInitModel = VCInitModel()
    let CellIdent = "InspectionProjectCheckTableViewCell"
    var isFromFastCheck:Bool = false
    
    
    lazy var checkFinishBtn: UIButton = {
        let btn = UIButton.init(type: UIButton.ButtonType.custom)
        btn.backgroundColor = #colorLiteral(red: 0.9386304396, green: 0.5230231393, blue: 0.4963629327, alpha: 1)
        btn.addTarget(self, action: #selector(finishCheckClick), for: .touchUpInside)
        btn.setTitle("检查完成", for: .normal)
        btn.setTitleColor(UIColor.white, for: .normal)
        return btn
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.titleName = "巡检项检查"
        
        self.configerUI()
        
    }
    
    override func backClick() {
        if isFromQuiclyInspection ?? false {
            self.popTo(vcClass: InspectionViewController.init())
        }else{
            self.popTo(vcClass: InspectionTaskPointListViewController.init())
        }
    }
    
    func configerUI() {
        
        self.listContainView.delegate = self
        self.listContainView.bounces = false
        self.listContainView.dataSource = self
        self.listContainView.separatorInset = UIEdgeInsets.init(top: 0, left: 10, bottom: 0, right: 10)
        self.listContainView.separatorStyle = .singleLine
        self.listContainView.backgroundColor = .clear
        self.listContainView.register(InspectionProjectCheckTableViewCell.self, forCellReuseIdentifier: CellIdent)
        self.view.addSubview(self.listContainView)
        self.view.addSubview(self.checkFinishBtn)
        self.listContainView.mj_footer!.isHidden = true
        self.listContainView.mj_header!.isHidden = true
        
        self.listContainView.snp.makeConstraints { (make) in
            make.top.equalTo(self.view)
            make.leading.trailing.equalTo(self.view)
            make.bottom.equalTo(self.checkFinishBtn.snp.top).offset(-10)
            
        }
        self.checkFinishBtn.layer.cornerRadius = 5
        self.checkFinishBtn.snp.makeConstraints { (make) in
            make.bottom.equalTo(self.view).offset(-10)
            make.leading.equalTo(self.view).offset(20)
            make.trailing.equalTo(self.view).offset(-20)
            make.height.equalTo(self.view).multipliedBy(0.06)
        }
        
    }
    
    @objc func finishCheckClick() {
        
        let recCheckVC = InspectionProjectResultCheckViewController.init()
        recCheckVC.taskPointModel = self.taskPointModel
        self.navigationController?.pushViewController(recCheckVC, animated: true)
        
    }

}

extension InspectionProjectCheckViewController:UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 76
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        UIView.init()
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        0.01
    }
    
}

extension InspectionProjectCheckViewController:UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.taskPointModel!.projectList!.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: CellIdent, for: indexPath) as! InspectionProjectCheckTableViewCell
        
        let model = self.taskPointModel!.projectList![indexPath.row]
        cell.configerUI(model: model)
        cell.selectionStyle = .none
        
        return cell
        
    }
    
    
}
