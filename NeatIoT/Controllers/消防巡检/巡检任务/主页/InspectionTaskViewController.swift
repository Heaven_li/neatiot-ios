//
//  InspectionTaskViewController.swift
//  NeatIoT
//
//  Created by neat on 2019/12/16.
//  Copyright © 2019 Neat. All rights reserved.
//

import UIKit
import MJRefresh

class InspectionTaskViewController: SupperTableViewViewController {
    
    let TaskCellIdent = "InspectionTaskTableViewCell"
    
    var menuModel:MenuModel?
    
    var currentPageIndex:Int = 1
    var currentKeyword:String = ""
    var currentFrequence:String = ""
    
    var dataSourceArr:Array<TaskItemModel> = []
    
    lazy var filterView: NormalFilterView = {
        let filterView = NormalFilterView.init()
        filterView.delegate = self
        return filterView
    }()
    
    lazy var topSearchView: TopSearchView = {
        let topSearchView = TopSearchView.init()
        topSearchView.delegate = self
        return topSearchView
    }()
 
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.titleName = self.menuModel?.name ?? "巡检任务"
        
        let filterNavItem = NavItemModel.init()
        filterNavItem.itemIcon = "nav_filter"
        self.rightItems = [filterNavItem]

        configerUI()
        
        getFrequence()
        
        getTaskList()
    }
    
    @objc override func navRRClick() -> Void {
        
        self.view.addSubview(self.filterView)
        
        self.filterView.snp.makeConstraints { (make) in
            make.edges.equalTo(self.view)
        }
        
    }

    
    func configerUI() {
        
        self.view.addSubview(self.topSearchView)
        
        self.listContainView.delegate = self
        self.listContainView.dataSource = self
        self.listContainView.separatorStyle = .singleLine
        self.listContainView.separatorInset = UIEdgeInsets.init(top: 0, left: 5, bottom: 0, right: 5)
        self.listContainView.register(UINib.init(nibName: "InspectionTaskTableViewCell", bundle: Bundle.main), forCellReuseIdentifier: self.TaskCellIdent)
        self.view.addSubview(self.listContainView)
        
        self.topSearchView.snp.makeConstraints { (make) in
            make.top.left.right.equalTo(self.view)
            make.height.equalTo(NormalConstant.TopSearchViewHeight)
        }
        
        self.listContainView.snp.makeConstraints { (make) in
            make.top.equalTo(self.topSearchView.snp.bottom)
            make.left.right.equalTo(self.view)
            make.bottom.equalTo(self.view)
        }
        
    }
    
    override func refreshAction() {
        
        currentPageIndex = 1
        
        NeatRequest.instance.getTaskList(keyword: currentKeyword, frequenceId: currentFrequence, pageIndex: currentPageIndex, success: { (response) in
                
                self.dataSourceArr = response.dataResult?.taskArr ?? []
                self.listContainView.reloadData()
                self.endRefresh(isUpdateFooter: true, dataCount: self.dataSourceArr.count, pageSize: 10)
                
            }) { (error) in
                
                NeatHud.showMessage(message: error.errMessage, view: self.view)
        
            }
        
    }
    override func loadMoreAction() {
        
        currentPageIndex+=1
        NeatRequest.instance.getTaskList(keyword: currentKeyword, frequenceId: currentFrequence, pageIndex: currentPageIndex, success: { (response) in
            
                let taskArr = response.dataResult?.taskArr ?? []
                
                if taskArr.count == 0{
                    self.currentPageIndex-=1
                }
            
                self.endLoadMoreData(dataCount:taskArr.count, pageSize: 10)
            
                self.dataSourceArr.append(contentsOf: response.dataResult?.taskArr ?? [])
                self.listContainView.reloadData()
                
            }) { (error) in
                self.currentPageIndex-=1
                self.endLoadMoreData(dataCount:0, pageSize: 10)
                NeatHud.showMessage(message: error.errMessage, view: self.view)
            }
        
    }

    
    func getFrequence() {
        
        NeatRequest.instance.getTaskFrequency(success: { (response) in
        
            self.configerFrequenceData(frequenceArr: response.frequenceArr!)
            
        }) { (error) in
            NeatHud.showMessage(message: error.errMessage, view: self.view)
        }
        
    }
    
    func configerFrequenceData(frequenceArr:Array<FrequenceModel>) {
        
        var dataArr:Array<NormalFilterIItemModel> = []
        for frequenceModel in frequenceArr {
            
            let filter = NormalFilterIItemModel.init()
            filter.filterName = frequenceModel.rateName
            filter.filterID = frequenceModel.id
            filter.isSelect = false
            
            dataArr.append(filter)
        }
        
        let filterFive = NormalFilterIItemModel.init()
        filterFive.filterName = "全部"
        filterFive.filterID = ""
        filterFive.isSelect = true
        dataArr.append(filterFive)
        
        self.filterView.dataSource = dataArr
        
    }
    
    func getTaskList() {
        
        self.listContainView.showLoadingView()
        
        NeatRequest.instance.getTaskList(keyword: currentKeyword, frequenceId: currentFrequence, pageIndex: currentPageIndex, success: { (response) in
            
            
            self.dataSourceArr = response.dataResult?.taskArr ?? []
            self.endLoadMoreData(dataCount: self.dataSourceArr.count, pageSize: 10)
            if self.dataSourceArr.count == 0{
                self.listContainView.showEmptyView()
            }else{
                self.listContainView.hidenHolderView()
                self.listContainView.reloadData()
                
            }
            
        }) { (error) in
            
            if error.errCode == RequestResponseCode.dataParsingException {
                //数据异常
                NeatHud.dissmissHud(view: self.view)
                self.listContainView.showRequestFaildView {
                    
                }
            }else if error.errCode == RequestResponseCode.netCanNotContent || error.errCode == RequestResponseCode.requestTimeOut{
                //无网络
                NeatHud.dissmissHud(view: self.view)
                self.listContainView.showNetErrorView(errorMessage: error.errMessage) {
                    self.getTaskList()
                }
            }else{
                NeatHud.showMessage(message: error.errMessage, view: self.view)
            }
    
        }
        
    }


}

extension InspectionTaskViewController:NormalFilterViewDelegate{
    
    func didSelectFilterItme(model: NormalFilterIItemModel) {
        currentPageIndex = 1
        currentFrequence = model.filterID!
        getTaskList()
    }
    
}

extension InspectionTaskViewController:TopSearchViewDelegate{
    
    func doSearchWith(keyword: String) {
        
        currentPageIndex = 1
        currentKeyword = keyword
        getTaskList()
        
    }
    
}

extension InspectionTaskViewController:UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView.init()
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 90.0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let vc = InspectionTaskDetailViewController.init()
        vc.taskInforModel = dataSourceArr[indexPath.row]
        self.navigationController?.pushViewController(vc, animated: true)
    
    }
    
}
extension InspectionTaskViewController:UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return dataSourceArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: TaskCellIdent, for: indexPath) as! InspectionTaskTableViewCell
        cell.selectionStyle = .none
        cell.delegate = self
        cell.configerCell(model: dataSourceArr[indexPath.row])
        return cell
        
    }
    
    
}

extension InspectionTaskViewController:InspectionTaskTableViewCellDelegate{
    func startBtnClick(model:TaskItemModel) {
        let vc = InspectionTaskPointListViewController.init()
        vc.taskInforModel = model
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    

    
}

