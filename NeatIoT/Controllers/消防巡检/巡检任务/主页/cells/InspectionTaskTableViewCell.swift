//
//  InspectionTaskTableViewCell.swift
//  NeatIoT
//
//  Created by neat on 2019/12/16.
//  Copyright © 2019 Neat. All rights reserved.
//

import UIKit

protocol InspectionTaskTableViewCellDelegate {
    func startBtnClick(model:TaskItemModel)
}

class InspectionTaskTableViewCell: UITableViewCell {
    
    private var currentModel:TaskItemModel?

    @IBOutlet weak var taskNameLable: UILabel!
    
    @IBOutlet weak var taskFrequenceLable: UILabel!
    
    @IBOutlet weak var taskEndTime: UILabel!
    
    @IBOutlet weak var taskRemainingTimeLable: UILabel!
    
    @IBOutlet weak var startBtn: UIButton!
    
    var delegate:InspectionTaskTableViewCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configerCell(model:TaskItemModel) {
        
        currentModel = model
        
        self.taskNameLable.text = model.taskName
        self.taskFrequenceLable.text = "任务类型:"+model.frequenceName!
        self.taskEndTime.text = "结束时间:"+model.endTime!
        
        let dateFormatter = DateFormatter.init()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        
        let endDate = dateFormatter.date(from:model.endTime!)
        
        let currentDate = Date.init(timeIntervalSinceNow: 0)
        
        let remainingTime = endDate?.timeIntervalSince(currentDate)
        
        let remainingMin = Int(remainingTime!/TimeInterval(60))
        
        if remainingMin<=30 {
            self.taskRemainingTimeLable.text = "还剩"+String(remainingMin)+"分钟"
//            self.taskRemainingTimeLable.isHidden = false
        }else{
            self.taskRemainingTimeLable.text = ""
//            self.taskRemainingTimeLable.isHidden = true
        }
    }
    
    @IBAction func startBtnClick(_ sender: Any) {
        self.delegate?.startBtnClick(model: currentModel!)
    }
}
