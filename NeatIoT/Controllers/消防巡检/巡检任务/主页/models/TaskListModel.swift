//
//  TaskListModel.swift
//  NeatIoT
//
//  Created by neat on 2019/12/18.
//  Copyright © 2019 Neat. All rights reserved.
//

import UIKit
import HandyJSON
class TaskListModel: NSObject,HandyJSON {
    
    /// 数据总数
    var totalCount:Int?
    
    /// 任务列表
    var taskArr:Array<TaskItemModel>?
    
    override required init() {
        
    }
    
    func mapping(mapper: HelpingMapper) {
        mapper <<<
        self.taskArr <-- "data"
    }

}
