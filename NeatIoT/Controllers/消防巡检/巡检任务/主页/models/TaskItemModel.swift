//
//  TaskItemModel.swift
//  NeatIoT
//
//  Created by neat on 2019/12/18.
//  Copyright © 2019 Neat. All rights reserved.
//

import UIKit
import HandyJSON
/// 巡检任务model
class TaskItemModel: HandyJSON {
    
    /// 任务id
    var id:String?
    
    /// 任务名称
    var taskName:String?
    
    /// 开始时间
    var beginTime:String?

    /// 结束时间
    var endTime:String?
    
    /// 单位名称
    var enterpriseName:String?
    
    /// 巡检频率
    var frequenceName:String?
    
    /// 巡检人
    var executorName:String?
    
    required init(){
        
    }
}
