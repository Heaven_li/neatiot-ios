//
//  InspectionTaskPointListViewController.swift
//  NeatIoT
//
//  Created by neat on 2019/12/16.
//  Copyright © 2019 Neat. All rights reserved.
//

import UIKit

class InspectionTaskPointListViewController: SupperTableViewViewController {
    
    let TaskPointInforCellIdent = "TaskPointInforTableViewCell"
    
    var dataSourceArr:Array<InspectionTaskPointModel> = []
    
    var taskInforModel:TaskItemModel?
    
    var currentPageIndex:Int = 1
    var currentKeyword:String = ""
    
    lazy var topSearchView: TopSearchView = {
        let topSearchView = TopSearchView.init()
        topSearchView.delegate = self
        return topSearchView
    }()
    
    override func viewWillAppear(_ animated: Bool) {
        self.getPointList()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.titleName = "巡检点"
        
        self.listContainView.delegate = self
        self.listContainView.dataSource = self
        self.listContainView.register(UINib.init(nibName: "TaskPointInforTableViewCell", bundle: Bundle.main), forCellReuseIdentifier: self.TaskPointInforCellIdent)
        configerUI()
        self.getPointList()
        
    }
    
    func configerUI() {
        
        self.view.addSubview(self.topSearchView)
        self.view.addSubview(self.listContainView)
        
        self.topSearchView.snp.makeConstraints { (make) in
            make.top.left.right.equalTo(self.view)
            make.height.equalTo(NormalConstant.TopSearchViewHeight)
        }
        
        self.listContainView.snp.makeConstraints { (make) in
            make.top.equalTo(self.topSearchView.snp.bottom)
            make.left.right.equalTo(self.view)
            make.bottom.equalTo(self.view)
        }
        
    }
    
    override func refreshAction() {
        
        currentPageIndex = 1
        
    NeatRequest.instance.getTaskPointList(resultTaskId:self.taskInforModel!.id!,keyword: currentKeyword, pageIndex: currentPageIndex, success: { (response) in
            
            self.dataSourceArr = response.resultData?.pointArr ?? []
            self.listContainView.reloadData()
            self.endRefresh(isUpdateFooter: true, dataCount: self.dataSourceArr.count, pageSize: 10)
            
        }) { (error) in
            
            NeatHud.showMessage(message: error.errMessage, view: self.view)
        }
        
    }
    override func loadMoreAction() {
        
        currentPageIndex+=1
    
        NeatRequest.instance.getTaskPointList(resultTaskId:self.taskInforModel!.id!,keyword: currentKeyword, pageIndex: currentPageIndex, success: { (response) in
            
            let pointArr = response.resultData?.pointArr ?? []
            
            if pointArr.count == 0{
                self.currentPageIndex-=1
            }
            self.dataSourceArr.append(contentsOf: pointArr)
            self.endLoadMoreData(dataCount:pointArr.count, pageSize: 10)
            self.listContainView.reloadData()
            
        }) { (error) in
            self.currentPageIndex-=1
            self.endLoadMoreData(dataCount:0, pageSize: 10)
            NeatHud.showMessage(message: error.errMessage, view: self.view)
        }
        
    }
    
    func getPointList() {
        
        self.listContainView.showLoadingView()
        NeatRequest.instance.getTaskPointList(resultTaskId:self.taskInforModel!.id!,keyword: currentKeyword, pageIndex: currentPageIndex, success: { (response) in
            
            self.dataSourceArr = response.resultData?.pointArr ?? []
            if self.dataSourceArr.count == 0{
                self.listContainView.showEmptyView()
            }else{
                self.listContainView.hidenHolderView()
                self.listContainView.reloadData()
                self.endLoadMoreData(dataCount: self.dataSourceArr.count, pageSize: 10)
            }
            
        }) { (error) in
            
            if error.errCode == RequestResponseCode.dataParsingException {
                //数据异常
                NeatHud.dissmissHud(view: self.view)
                self.listContainView.showRequestFaildView {
                    
                }
            }else if error.errCode == RequestResponseCode.netCanNotContent || error.errCode == RequestResponseCode.requestTimeOut{
                //无网络
                NeatHud.dissmissHud(view: self.view)
                self.listContainView.showNetErrorView(errorMessage: error.errMessage) {
                    self.getPointList()
                }
            }else{
                NeatHud.showMessage(message: error.errMessage, view: self.view)
            }
            
            
        }
        
    }
    
}
extension InspectionTaskPointListViewController:TopSearchViewDelegate{
    
    func doSearchWith(keyword: String) {
        
        currentKeyword = keyword
        currentPageIndex = 1
        
        self.getPointList()
        
    }
    
}

extension InspectionTaskPointListViewController:UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView.init()
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 146
    }
    
}
extension InspectionTaskPointListViewController:UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.dataSourceArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: TaskPointInforCellIdent, for: indexPath) as! TaskPointInforTableViewCell
        cell.selectionStyle = .none
        cell.delegate = self
        cell.configerCell(model: dataSourceArr[indexPath.row])
        
        let roundPath = UIBezierPath.init(roundedRect: CGRect.init(x: 0, y: 0, width: NormalConstant.ScreenWidth - 20, height: 146 - 20), cornerRadius: 5)
        
        let maskLayer = CAShapeLayer.init()
        maskLayer.path = roundPath.cgPath
        
        cell.bgView.layer.mask = maskLayer
     
        return cell
        
    }
    
}

extension InspectionTaskPointListViewController:TaskPointInforTableViewCellDelegate{

    
    
    func pointDetailClick(model: InspectionTaskPointModel) {
        let detailVC = InspectionPointDetailViewController.init()
        detailVC.taskPointModel = model
        self.navigationController?.pushViewController(detailVC, animated: true)
    }
    
    func pointMarkClick() {
        NeatHud.showMessage(message: "该功能尚未开放！", view: self.view)
    }
    
    func pointScanClick(model: InspectionTaskPointModel) {
        
        if model.qrCode.isEmpty && model.nfcCode.isEmpty {
            
            NeatHud.showMessage(message: "该点位尚未绑定二维码及NFC标签，无法进行巡检。", view: self.view)
            return
        }else if model.qrCode.isEmpty && !(model.nfcCode.isEmpty){
            NeatHud.showMessage(message: "该点位尚未绑定二维码标签，无法进行巡检。请使用Android程序巡检", view: self.view)
            return
        }

        
        let vcInitModel = VCInitModel.init()
        vcInitModel.navTitleText = "二维码扫描"
        
        let swiperVC = QRScanViewController.init()
        swiperVC.vcInit = vcInitModel
        swiperVC.taskPointModel = model
        self.navigationController?.pushViewController(swiperVC, animated: true)
        
    }
    
    
    
}

