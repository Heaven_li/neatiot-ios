//
//  InspectionP.swift
//  NeatIoT
//
//  Created by neat on 2019/12/18.
//  Copyright © 2019 Neat. All rights reserved.
//

import UIKit
import HandyJSON
class InspectionTaskPointListResultData: HandyJSON {
    
    var totalCount:Int?
    
    var pointArr:Array<InspectionTaskPointModel>?
    
    required init(){
        
    }
    
    func mapping(mapper: HelpingMapper) {
        mapper <<<
        self.pointArr <-- "data"
    }

}
