//
//  InspectionTaskPointModel.swift
//  NeatIoT
//
//  Created by neat on 2019/12/18.
//  Copyright © 2019 Neat. All rights reserved.
//

import UIKit
import HandyJSON
class InspectionTaskPointModel: HandyJSON {
    
    /// 任务id
    var resultPointId:String?
    /// 巡检点id
    var pointId:String?
    /// 巡检点名称
    var pointName:String?
    /// 巡检点位置
    var address:String?
    /// 绑定二维码
    var qrCode:String = ""
    /// 绑定NFC码
    var nfcCode:String = ""
    /// 巡检项目类型
    var projectTypeName:String?
    /// 巡检项目类型id
    var projectTypeId:String?
    /// 巡检项目子类型
    var projectSubTypeName:String?
    /// 巡检项目子类型id
    var projectTypeSubId:String?
    /// 企业名称
    var enterpriseName:String?
    /// 建筑名称
    var buildingName:String?
    /// 部位id
    var keypartId:String?
    /// 部位名称
    var keypartName:String?
    /// 是否完成
    var isFinished:Bool = false
    /// 完成时间
    var finishTime:String?
    /// 是否有隐患
    var hasTrouble:Bool = false
    /// 巡检项目
    var projectList:Array<InspectionProjectItemModel>?
    
    required init(){
        
    }

}
