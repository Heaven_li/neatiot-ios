//
//  InspectionProjectItemModel.swift
//  NeatIoT
//
//  Created by neat on 2019/12/18.
//  Copyright © 2019 Neat. All rights reserved.
//

import UIKit
import HandyJSON
/// 巡检项model
class InspectionProjectItemModel: HandyJSON {
    
    /// 巡检项目id
    var projectId:String?
    /// 巡检项目名称
    var projectName:String?
    /// 巡检项目状态 0:未知  1:是  2:否
    var hasTrouble:Int?
    
    required init(){
        
    }

}
