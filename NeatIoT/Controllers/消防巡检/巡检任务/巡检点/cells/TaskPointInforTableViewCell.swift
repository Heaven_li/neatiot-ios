//
//  TaskPointInforTableViewCell.swift
//  NeatIoT
//
//  Created by neat on 2019/12/16.
//  Copyright © 2019 Neat. All rights reserved.
//

import UIKit


protocol TaskPointInforTableViewCellDelegate {
    func pointDetailClick(model:InspectionTaskPointModel)
    func pointMarkClick()
    func pointScanClick(model:InspectionTaskPointModel)
}

class TaskPointInforTableViewCell: UITableViewCell {
    
    var currentModel:InspectionTaskPointModel?
    
    @IBOutlet weak var finishImageView: UIImageView!
    
    @IBOutlet weak var finishTime: UILabel!
    
    @IBOutlet weak var pointNameLable: UILabel!
    
    @IBOutlet weak var bgView: UIView!
    
    @IBOutlet weak var pointAddressLable: UILabel!
    
    @IBOutlet weak var pointScanBtn: UIButton!
    
    var delegate:TaskPointInforTableViewCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
    }
    
    func configerCell(model:InspectionTaskPointModel) {
        
        currentModel = model
        self.pointNameLable.text = model.pointName
        self.pointAddressLable.text = "详细地址:"+model.address!
        
        if (model.isFinished ?? false) {
            self.finishImageView.isHidden = false
            self.finishTime.isHidden = false
            self.finishTime.text = model.finishTime
            self.pointScanBtn.isEnabled = false
        }else{
            self.pointScanBtn.isEnabled = true
            self.finishImageView.isHidden = true
            self.finishTime.isHidden = true
        }
        
//        if model.qrCode?.isEmpty ?? true{
//            self.pointScanBtn.isEnabled = false
//        }else{
//            self.pointScanBtn.isEnabled = true
//        }
        
    }

    @IBAction func pointDetailBtnClick(_ sender: Any) {
        self.delegate!.pointDetailClick(model:currentModel!)
    }
    
    @IBAction func pointMarkClick(_ sender: Any) {
        self.delegate!.pointMarkClick()
    }
    
    @IBAction func pointScanClick(_ sender: Any) {
        
        self.delegate!.pointScanClick(model:currentModel!)
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
