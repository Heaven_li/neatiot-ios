//
//  InspectionNotifyViewController.swift
//  NeatIoT
//
//  Created by neat on 2019/12/16.
//  Copyright © 2019 Neat. All rights reserved.
//

import UIKit

class InspectionNotifyViewController: SupperTableViewViewController {

    var menuModel:MenuModel?
    
    let cellIdent = "NotifyItemTableViewCellIdent"
    
    var currentPageIndex = 1
    var dataSourceArr:Array<InspectionMessageItemModel> = []

    @IBOutlet weak var containTableView: UITableView!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.titleName = self.menuModel?.name ?? "消息提醒"
        
        configerUI()
        
        getMessageList()
        
        updataMessageList()
    
    }
    
    func configerUI() {
        self.listContainView.delegate = self
        self.listContainView.dataSource = self
        self.listContainView.separatorStyle = .singleLine
        self.listContainView.separatorInset = UIEdgeInsets.init(top: 0, left: 10, bottom: 0, right: 10)
        self.listContainView.register(UINib.init(nibName: "NotifyItemTableViewCell", bundle: Bundle.main), forCellReuseIdentifier: cellIdent)
        self.view.addSubview(self.listContainView)
        self.listContainView.snp.makeConstraints { (make) in
            make.edges.equalTo(self.view)
        }
    }
    
    func getMessageList() {
        
        self.listContainView.showLoadingView()
        NeatRequest.instance.getNotiftionMessageList(pageIndex: currentPageIndex, success: { (response) in
            
            self.dataSourceArr = response.resultData!.messageArr!
            if self.dataSourceArr.count == 0{
                self.listContainView.showEmptyView()
            }else{
                self.listContainView.hidenHolderView()
            }
            self.endLoadMoreData(dataCount: self.dataSourceArr.count, pageSize: 10)
            self.listContainView.reloadData()
            
        }) { (error) in
            
            if error.errCode == RequestResponseCode.dataParsingException {
                //数据异常
                NeatHud.dissmissHud(view: self.view)
                self.listContainView.showRequestFaildView {
                    
                }
            }else if error.errCode == RequestResponseCode.netCanNotContent || error.errCode == RequestResponseCode.requestTimeOut{
                //无网络
                NeatHud.dissmissHud(view: self.view)
                self.listContainView.showNetErrorView(errorMessage: error.errMessage) {
                    self.getMessageList()
                }
            }else{
                NeatHud.showMessage(message: error.errMessage, view: self.view)
            }
            
        }
    }
    
    func updataMessageList() {
        NeatRequest.instance.editUnReadPushNum(success: { (response) in
            
        }) { (error) in
            
        }
    }
    
    override func refreshAction() {
        
        currentPageIndex = 1
        
        NeatRequest.instance.getNotiftionMessageList(pageIndex: currentPageIndex, success: { (response) in
            
            self.dataSourceArr = response.resultData!.messageArr!
            self.endRefresh(isUpdateFooter: true, dataCount: self.dataSourceArr.count, pageSize: 10)
            self.listContainView.reloadData()
            
        }) { (error) in
            
            NeatHud.showMessage(message: error.errMessage, view: self.view)
            
        }
        
    }
    override func loadMoreAction() {
        
        currentPageIndex += 1
        
        NeatRequest.instance.getNotiftionMessageList(pageIndex: currentPageIndex, success: { (response) in
            
            let messageArr = response.resultData!.messageArr!
            if messageArr.count == 0{
                self.currentPageIndex -= 1
            }
            self.dataSourceArr.append(contentsOf: messageArr)
            self.endLoadMoreData(dataCount: messageArr.count, pageSize: 10)
            self.listContainView.reloadData()
            
        }) { (error) in
            
            self.currentPageIndex -= 1
            self.endLoadMoreData(dataCount:0, pageSize: 10)
            NeatHud.showMessage(message: error.errMessage, view: self.view)
            
        }
    }
  
}

extension InspectionNotifyViewController:UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.dataSourceArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdent, for: indexPath) as! NotifyItemTableViewCell
        cell.configerCell(model: self.dataSourceArr[indexPath.row])
        cell.selectionStyle = .none
        return cell
    }
    
}
extension InspectionNotifyViewController:UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.01
    }
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView.init()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
}
