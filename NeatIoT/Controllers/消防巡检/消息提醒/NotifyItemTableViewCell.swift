//
//  NotifyItemTableViewCell.swift
//  NeatIoT
//
//  Created by neat on 2019/12/17.
//  Copyright © 2019 Neat. All rights reserved.
//

import UIKit

class NotifyItemTableViewCell: UITableViewCell {

    @IBOutlet weak var messageInforLable: UILabel!
    
    @IBOutlet weak var appenTimeLable: UILabel!
    
    @IBOutlet weak var beforeTimelable: UILabel!
    
    @IBOutlet weak var newDot: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configerCell(model:InspectionMessageItemModel) {
        
        self.messageInforLable.text = model.message
        
        self.appenTimeLable.text = "发送时间:"+(model.pushTime ?? "-")
        
        if model.daysAgo != nil {
            self.beforeTimelable.text = (model.daysAgo ?? "-")+"天前"
        }else if model.hoursAgo != nil{
            self.beforeTimelable.text = (model.hoursAgo ?? "-")+"小时前"
        }else if model.secondsAgo != nil{
            self.beforeTimelable.text = (model.secondsAgo ?? "-")+"秒前"
        }
        
        if model.status == 0 {
            self.newDot.isHidden = false
        }else{
            self.newDot.isHidden = true
        }
        
        
        
    }
    
}
