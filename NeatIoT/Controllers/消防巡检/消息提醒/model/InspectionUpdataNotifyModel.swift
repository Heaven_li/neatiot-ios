//
//  InspectionUpdataNotifyModel.swift
//  NeatIoT
//
//  Created by neat on 2019/12/24.
//  Copyright © 2019 Neat. All rights reserved.
//

import UIKit
import HandyJSON
class InspectionUpdataNotifyModel: HandyJSON,Encodable {

    ///是否全部更新
    var all:Bool?
    
    ///消息id数组
    var msdIds:Array<String>?
    
    ///新状态
    var newStatus:String?

    required init() {
        
    }
    
//    func mapping(mapper: HelpingMapper) {
//        mapper <<<
//        "all" <-- self.isAllUpdate
//    }


}
