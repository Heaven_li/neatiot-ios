//
//  ProjectRecItemTableViewCell.swift
//  NeatIoT
//
//  Created by neat on 2019/12/19.
//  Copyright © 2019 Neat. All rights reserved.
//

import UIKit

class ProjectRecItemTableViewCell: UITableViewCell {

    @IBOutlet weak var projectNameLable: UILabel!
    
    @IBOutlet weak var projectState: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configerCell(model:InspectionTroublePointDetailCellModel) {
        
        self.projectNameLable.text = model.titleText
        
        self.projectState.text = model.inforText
        
    }
    
}
