//
//  ProjectRecBeforeTableViewCell.swift
//  NeatIoT
//
//  Created by neat on 2019/12/19.
//  Copyright © 2019 Neat. All rights reserved.
//

import UIKit

class ProjectRecBeforeTableViewCell: UITableViewCell {

    
    @IBOutlet weak var handlePerson: UILabel!
    @IBOutlet weak var handleTime: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configerCell(model:InspectionTroublePointDetailCellModel) {
        
        self.handlePerson.text = model.titleText
        self.handleTime.text = model.inforText
        
    }
    
}
