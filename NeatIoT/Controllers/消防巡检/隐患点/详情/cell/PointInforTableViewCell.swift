//
//  PointInforTableViewCell.swift
//  NeatIoT
//
//  Created by neat on 2019/12/20.
//  Copyright © 2019 Neat. All rights reserved.
//

import UIKit

class PointInforTableViewCell: UITableViewCell {

    @IBOutlet weak var titleLable: UILabel!
    @IBOutlet weak var inforLable: UILabel!
    
    @IBOutlet weak var sepLine: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configerCell(model:TitleInforModel) {
        
        self.titleLable.text = model.titleText
        self.inforLable.text = model.inforText
        
    }
    
    func configerCell(model:InspectionTroublePointDetailCellModel) {
        
        self.titleLable.text = model.titleText
        self.inforLable.text = model.inforText
        self.sepLine.isHidden = model.isHidenSepLine ?? false
        
    }
    
}
