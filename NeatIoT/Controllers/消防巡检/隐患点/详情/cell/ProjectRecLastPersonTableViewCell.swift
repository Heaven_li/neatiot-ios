//
//  ProjectRecLastPersonTableViewCell.swift
//  NeatIoT
//
//  Created by neat on 2019/12/20.
//  Copyright © 2019 Neat. All rights reserved.
//

import UIKit

class ProjectRecLastPersonTableViewCell: UITableViewCell {
    
    @IBOutlet weak var handlePersonName: UILabel!
    
    @IBOutlet weak var handleTimeLable: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configerCell(model:InspectionTroublePointDetailCellModel) {
        self.handlePersonName.text = model.titleText
        self.handleTimeLable.text = model.inforText
    }
    
}
