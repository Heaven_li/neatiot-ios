//
//  InspectionTroublePointDetailViewController.swift
//  NeatIoT
//
//  Created by neat on 2019/12/19.
//  Copyright © 2019 Neat. All rights reserved.
//

import UIKit

class InspectionTroublePointDetailViewController: SupperViewController {
    let rowHeight:CGFloat = 46
    let NormalCellIdent = "NormalCellIdent"
    let RecCellIdent = "RecCellIdent"
    let LastPersonCellIdent = "LastPersonCellIdent"
    let BeforePersonRecCellIdent = "BeforePersonRecCellIdent"
    
    var pointModel:TroublePointModel?
    
    var dataSourceArr:Array<Array<InspectionTroublePointDetailCellModel>> = []
    
    lazy var containTableView: UITableView = {
        let tableview = UITableView.init()
        tableview.delegate = self
        tableview.dataSource = self
        tableview.separatorStyle = .none
        tableview.backgroundColor = .clear
        tableview.register(UINib.init(nibName: "PointInforTableViewCell", bundle: Bundle.main), forCellReuseIdentifier: self.NormalCellIdent)
        tableview.register(UINib.init(nibName: "ProjectRecLastPersonTableViewCell", bundle: Bundle.main), forCellReuseIdentifier: self.LastPersonCellIdent)
        tableview.register(UINib.init(nibName: "ProjectRecBeforeTableViewCell", bundle: Bundle.main), forCellReuseIdentifier: self.BeforePersonRecCellIdent)
        tableview.register(UINib.init(nibName: "ProjectRecItemTableViewCell", bundle: Bundle.main), forCellReuseIdentifier: self.RecCellIdent)
        return tableview
    }()

    override func viewDidLoad() {
        super.viewDidLoad()

        self.titleName = "隐患点详情"
        
        self.view.addSubview(self.containTableView)
        self.containTableView.snp.makeConstraints { (make) in
            make.edges.equalTo(self.view)
        }
        
        formateData()
        
        self.containTableView.reloadData()
        
    }
    
    func formateData() {
        
        var pointInforArr:Array<InspectionTroublePointDetailCellModel> = []
        var projectInforArr:Array<InspectionTroublePointDetailCellModel> = []
        
        let nameModel = InspectionTroublePointDetailCellModel.init()
        nameModel.titleText = "巡检点名称"
        nameModel.inforText = self.pointModel?.pointName
        nameModel.cellStyle = .PointItem
        pointInforArr.append(nameModel)
        
        let projectSubTypeModel = InspectionTroublePointDetailCellModel.init()
        projectSubTypeModel.titleText = "项目子类型"
        projectSubTypeModel.inforText = self.pointModel?.subTypeName
        projectSubTypeModel.cellStyle = .PointItem
        pointInforArr.append(projectSubTypeModel)
        
        let entModel = InspectionTroublePointDetailCellModel.init()
        entModel.titleText = "所属单位"
        entModel.inforText = self.pointModel?.entName
        entModel.cellStyle = .PointItem
        pointInforArr.append(entModel)
        
        let buildingModel = InspectionTroublePointDetailCellModel.init()
        buildingModel.titleText = "所属建筑"
        buildingModel.inforText = self.pointModel?.buildingName
        buildingModel.cellStyle = .PointItem
        pointInforArr.append(buildingModel)
        
        let keypartModel = InspectionTroublePointDetailCellModel.init()
        keypartModel.titleText = "所属部位"
        keypartModel.inforText = self.pointModel?.keypartName
        keypartModel.cellStyle = .PointItem
        pointInforArr.append(keypartModel)
        
        let addressModel = InspectionTroublePointDetailCellModel.init()
        addressModel.titleText = "详细地址"
        addressModel.inforText = self.pointModel?.pointAddress
        addressModel.isHidenSepLine = true
        addressModel.cellStyle = .PointItem
        pointInforArr.append(addressModel)
    
        dataSourceArr.append(pointInforArr)
        
        for index in 0..<self.pointModel!.troubleList!.count {
            
            let troubleModel = self.pointModel?.troubleList![index]
            
            let model = InspectionTroublePointDetailCellModel.init()
            model.titleText = troubleModel!.personName
            model.inforText = troubleModel!.submitTime
            if index == 0 {
                model.cellStyle = .LastPerson
            }else{
                model.cellStyle = .BeforePerson
            }
            
            projectInforArr.append(model)
            
            for projectModel in troubleModel!.projectItems! {
                
                let model = InspectionTroublePointDetailCellModel.init()
                model.titleText = projectModel.projectName
                model.inforText = "否"
                model.cellStyle = .ProjectItem
                projectInforArr.append(model)
            }
        }
        
        dataSourceArr.append(projectInforArr)
        
    }

}

extension InspectionTroublePointDetailViewController:UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        if section == 0 {
            return rowHeight
        }else{
            return 0.01
        }
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        if section == 0 {
            let view = InspectionInforHeaderView.init()
            view.titleText = "巡检项目"
            return view
        }else{
            return UIView.init()
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if indexPath.section == 0 {
            return rowHeight
        }else{
            let model = dataSourceArr[indexPath.section][indexPath.row]
            
            switch model.cellStyle {
            case .PointItem:
                return rowHeight
                
            case .BeforePerson:
                return 50
                
            case .LastPerson:
                return 50
                
            case .ProjectItem:
                return 32
               
            default:
                return rowHeight
            }
            
        }
    }
    
}
extension InspectionTroublePointDetailViewController:UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        dataSourceArr[section].count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: NormalCellIdent, for: indexPath) as! PointInforTableViewCell
            cell.selectionStyle = .none
            cell.configerCell(model: dataSourceArr[indexPath.section][indexPath.row])
            return cell
        }else{
            
            let model = dataSourceArr[indexPath.section][indexPath.row]
            
            if model.cellStyle == .LastPerson {
                let cell = tableView.dequeueReusableCell(withIdentifier: LastPersonCellIdent, for: indexPath) as! ProjectRecLastPersonTableViewCell
                cell.selectionStyle = .none
                cell.configerCell(model: model)
                return cell
            }else if model.cellStyle == .BeforePerson {
                let cell = tableView.dequeueReusableCell(withIdentifier: BeforePersonRecCellIdent, for: indexPath) as! ProjectRecBeforeTableViewCell
                cell.selectionStyle = .none
                cell.configerCell(model: model)
                return cell
            }else {
                let cell = tableView.dequeueReusableCell(withIdentifier: RecCellIdent, for: indexPath) as! ProjectRecItemTableViewCell
                cell.selectionStyle = .none
                cell.configerCell(model: model)
                return cell
            }
        
        }
        
    }
    
    
}
