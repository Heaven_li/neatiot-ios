//
//  InspectionTroublePointDetailCellModel.swift
//  NeatIoT
//
//  Created by neat on 2019/12/20.
//  Copyright © 2019 Neat. All rights reserved.
//
import UIKit

enum CellStyle {
    case ProjectItem
    case PointItem
    case LastPerson
    case BeforePerson
}

class InspectionTroublePointDetailCellModel: NSObject {
    
    /// 标题
    var titleText:String?
    
    /// 内容
    var inforText:String?
    
    /// cell样式
    var cellStyle:CellStyle?
    
    /// 是否隐藏分割线 true 隐藏
    var isHidenSepLine:Bool = false

}
