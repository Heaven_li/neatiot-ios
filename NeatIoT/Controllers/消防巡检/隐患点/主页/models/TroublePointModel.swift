//
//  TroublePointModel.swift
//  NeatIoT
//
//  Created by neat on 2019/12/19.
//  Copyright © 2019 Neat. All rights reserved.
//

import UIKit
import HandyJSON
class TroublePointModel: HandyJSON {

    /// 巡检点名称
    var pointName:String?
    /// 巡检点地址
    var pointAddress:String?
    /// 项目子类型
    var subTypeName:String?
    /// 企业名称
    var entName:String?
    /// 建筑名称
    var buildingName:String?
    /// 部位名称
    var keypartName:String?
    /// 问题记录数组
    var troubleList:Array<TroublePointRecModel>?
    
    required init() {
        
    }

}
