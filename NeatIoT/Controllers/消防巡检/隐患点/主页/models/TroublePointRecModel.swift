//
//  TroublePointRecModel.swift
//  NeatIoT
//
//  Created by neat on 2019/12/19.
//  Copyright © 2019 Neat. All rights reserved.
//

import UIKit
import HandyJSON
class TroublePointRecModel: HandyJSON {
    
    
    /// 检查人
    var personName:String?
    
    /// 提交时间
    var submitTime:String?
    
    /// 项目列表
    var projectItems:Array<TroublePointProjectModel>?
    
    
    required init(){
        
    }

}
