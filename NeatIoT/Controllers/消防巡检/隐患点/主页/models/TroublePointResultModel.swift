//
//  TroublePointResultModel.swift
//  NeatIoT
//
//  Created by neat on 2019/12/19.
//  Copyright © 2019 Neat. All rights reserved.
//

import UIKit
import HandyJSON
class TroublePointResultModel: HandyJSON {
    /// 数据数量
    var totalCount:Int?
    /// 项目名称
    var pointArr:Array<TroublePointModel>?
    
    required init(){
        
    }
    
    func mapping(mapper: HelpingMapper) {
         mapper <<<
        self.pointArr <-- "data"
    }
}
