//
//  InspectionTroublePointViewController.swift
//  NeatIoT
//
//  Created by neat on 2019/12/16.
//  Copyright © 2019 Neat. All rights reserved.
//

import UIKit

class InspectionTroublePointViewController: SupperTableViewViewController {

    let CellIdent = "CellIdent"
    
    var menuModel:MenuModel?
    
    var currentPageIndex:Int = 1
    var currentKeyword:String = ""
    var currentFilterDate:String = ""
    
    var dataSourceArr:Array<TroublePointModel> = []
    
    lazy var filterView: NormalFilterView = {
        
        let filterView = NormalFilterView.init()
        filterView.delegate = self
        
        let oneFilter = NormalFilterIItemModel.init()
        oneFilter.filterID = "7"
        oneFilter.filterName = "7天内"
        oneFilter.isSelect = false
        
        let twoFilter = NormalFilterIItemModel.init()
        twoFilter.filterID = "30"
        twoFilter.filterName = "30天内"
        twoFilter.isSelect = false
        
        let thirdFilter = NormalFilterIItemModel.init()
        thirdFilter.filterID = "90"
        thirdFilter.filterName = "90天内"
        thirdFilter.isSelect = false
        
        let fourFilter = NormalFilterIItemModel.init()
        fourFilter.filterID = "180"
        fourFilter.filterName = "180天内"
        fourFilter.isSelect = false
        
        let fiveFilter = NormalFilterIItemModel.init()
        fiveFilter.filterID = ""
        fiveFilter.filterName = "全部"
        fiveFilter.isSelect = true
        
        filterView.dataSource = [oneFilter,twoFilter,thirdFilter,fourFilter,fiveFilter]
        
        return filterView
    }()
    
//    lazy var containTableView: UITableView = {
//        let containView = UITableView.init()
//        containView.delegate = self
//        containView.dataSource = self
//        containView.backgroundColor = .clear
//        containView.separatorStyle = .none
//        containView.register(UINib.init(nibName: "TroublePointTableViewCell", bundle: Bundle.main), forCellReuseIdentifier: self.CellIdent)
//        return containView
//    }()
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.titleName = self.menuModel?.name ?? "隐患点"
        let filterNavItem = NavItemModel.init()
        filterNavItem.itemIcon = "nav_filter"
        self.rightItems = [filterNavItem]
    
        getTroublePointData()
        
        configerUI()
    }
    
    func configerUI() {
        
        self.listContainView.delegate = self
        self.listContainView.dataSource = self
        self.listContainView.register(UINib.init(nibName: "TroublePointTableViewCell", bundle: Bundle.main), forCellReuseIdentifier: self.CellIdent)
        
        self.view.addSubview(self.listContainView)
        self.listContainView.snp.makeConstraints { (make) in
            make.edges.equalTo(self.view)
        }
        
    }
    
    @objc override func navRRClick() -> Void {
    
        self.view.addSubview(self.filterView)
        
        self.filterView.snp.makeConstraints { (make) in
            make.edges.equalTo(self.view)
        }
        
        
        
    }
    
    override func refreshAction() {
        
        currentPageIndex = 1
        
        NeatRequest.instance.getTroublePointList(keyword: currentKeyword, pageIndex: currentPageIndex, filterDate: currentFilterDate, success: { (response) in
            
            self.dataSourceArr = response.resultData!.pointArr!
            self.endRefresh(isUpdateFooter: true, dataCount: self.dataSourceArr.count, pageSize: 10)
            self.listContainView.reloadData()
            
        }) { (error) in
            
            NeatHud.showMessage(message: error.errMessage, view: self.view)
            
        }
    }
    
    override func loadMoreAction() {
        
        currentPageIndex += 1
        
        NeatRequest.instance.getTroublePointList(keyword: currentKeyword, pageIndex: currentPageIndex, filterDate: currentFilterDate, success: { (response) in
            
            let pointArr = response.resultData?.pointArr ?? []
            
            if pointArr.count == 0{
                self.currentPageIndex-=1
            }
            self.endLoadMoreData(dataCount:pointArr.count, pageSize: 10)
            self.dataSourceArr.append(contentsOf: response.resultData!.pointArr!)
            
            self.listContainView.reloadData()
            
        }) { (error) in
            
            self.currentPageIndex -= 1
            self.endLoadMoreData(dataCount:0, pageSize: 10)
            NeatHud.showMessage(message: error.errMessage, view: self.view)
            
        }
    }
    
    func getTroublePointData() {
        
        self.listContainView.showLoadingView()
        
        NeatRequest.instance.getTroublePointList(keyword: currentKeyword, pageIndex: currentPageIndex, filterDate: currentFilterDate, success: { (response) in
            
            self.dataSourceArr.removeAll()
            self.dataSourceArr = response.resultData!.pointArr!
            
            if self.dataSourceArr.count == 0{
                self.listContainView.showEmptyView()
            }else{
                self.listContainView.hidenHolderView()
                self.listContainView.reloadData()
                self.endLoadMoreData(dataCount: self.dataSourceArr.count, pageSize: 10)
            }
            
        }) { (error) in
            
            self.listContainView.hidenHolderView()
            
            if error.errCode == RequestResponseCode.dataParsingException {
                //数据异常
                NeatHud.dissmissHud(view: self.view)
                self.listContainView.showRequestFaildView {
                    
                }
            }else if error.errCode == RequestResponseCode.netCanNotContent || error.errCode == RequestResponseCode.requestTimeOut{
                //无网络
                NeatHud.dissmissHud(view: self.view)
                self.listContainView.showNetErrorView(errorMessage: error.errMessage) {
                    self.getTroublePointData()
                }
            }else{
                NeatHud.showMessage(message: error.errMessage, view: self.view)
            }
            
        }
        
    }

}

extension InspectionTroublePointViewController:NormalFilterViewDelegate{
    
    func didSelectFilterItme(model: NormalFilterIItemModel) {
        currentFilterDate = model.filterID!
        self.getTroublePointData()
    }
    
    
}

extension InspectionTroublePointViewController:UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 78
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.01
    }
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView.init()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = InspectionTroublePointDetailViewController.init()
        vc.pointModel = dataSourceArr[indexPath.row]
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}
extension InspectionTroublePointViewController:UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.dataSourceArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: CellIdent, for: indexPath) as! TroublePointTableViewCell
        cell.selectionStyle = .none
        cell.configerCell(model: dataSourceArr[indexPath.row])
        
        let roundPath = UIBezierPath.init(roundedRect: CGRect.init(x: 0, y: 0, width: NormalConstant.ScreenWidth - 20, height: 78 - 10), cornerRadius: 5)
        let maskLayer = CAShapeLayer.init()
        maskLayer.path = roundPath.cgPath
        cell.bgView.layer.mask = maskLayer
        
        return cell
    }
    
    
}
