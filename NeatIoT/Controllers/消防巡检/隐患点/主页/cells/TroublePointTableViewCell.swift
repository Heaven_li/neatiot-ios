//
//  TroublePointTableViewCell.swift
//  NeatIoT
//
//  Created by neat on 2019/12/19.
//  Copyright © 2019 Neat. All rights reserved.
//

import UIKit

class TroublePointTableViewCell: UITableViewCell {

    @IBOutlet weak var bgView: UIView!
    
    @IBOutlet weak var pointCountNum: UILabel!
    
    @IBOutlet weak var pointNameLable: UILabel!
    
    @IBOutlet weak var pointAddressLable: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.pointCountNum.layer.cornerRadius = 10
        self.pointCountNum.clipsToBounds = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configerCell(model:TroublePointModel) {
        
        self.pointCountNum.text = String.init(format: "%d", (model.troubleList?.first?.projectItems!.count)!)
        self.pointNameLable.text = model.pointName
        self.pointAddressLable.text = "详细地址:"+model.pointAddress!
        
    }
    
}
