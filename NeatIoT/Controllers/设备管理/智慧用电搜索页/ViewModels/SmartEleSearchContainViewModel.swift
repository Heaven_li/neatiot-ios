//
//  SmartEleSearchContainViewModel.swift
//  NeatIoT
//
//  Created by neat on 2019/11/26.
//  Copyright © 2019 Neat. All rights reserved.
//

import UIKit

class SmartEleSearchContainViewModel: NSObject {
    
    
    class func getEleInforByCode(code:String,view:SmartEleSearchContainView) {
        
        NeatRequest.instance.getNoBindEleGateway(code: code, success: { (response) in
            
            view.eleResponseModel = response
            
        }) { (error) in
            
            NeatHud.showMessage(message: error.errMessage, view: view)
            
        }
        
    }

}
