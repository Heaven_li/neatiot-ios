//
//  SmartEleSearchViewController.swift
//  NeatIoT
//
//  Created by neat on 2019/11/25.
//  Copyright © 2019 Neat. All rights reserved.
//

import UIKit

class SmartEleSearchViewController: SupperViewController {
    
    var vcInitModel:VCInitModel = VCInitModel()
    
    lazy var topSearchView: TopSearchView = {
        let topSearchView = TopSearchView.init()
        topSearchView.delegate = self
        topSearchView.hasScan = true
        return topSearchView
    }()
    
    
    lazy var containView: SmartEleSearchContainView = {
        let view = SmartEleSearchContainView.init()
        view.delegate = self
        return view
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.titleName = "添加绑定"

        self.view.addSubview(self.topSearchView)
        
        self.view.addSubview(self.containView)
        
        self.topSearchView.snp.makeConstraints { (make) in
            make.top.left.right.equalTo(self.view)
            make.height.equalTo(56)
        }
        
        self.containView.snp.makeConstraints { (make) in
            make.top.equalTo(self.topSearchView.snp.bottom)
            make.bottom.equalTo(self.view.snp.bottom)
            make.leading.trailing.equalTo(self.view)
        }
        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension SmartEleSearchViewController:TopSearchViewDelegate{
    
    func scanQRClick() {
        
        let vcModel = VCInitModel()
        vcModel.deviceCategory = self.vcInitModel.deviceCategory
        vcModel.navTitleText = "扫一扫"
        let qrVC = QRScanViewController.init()
        qrVC.delegate = self
        qrVC.vcInit = vcModel
        self.navigationController?.pushViewController(qrVC, animated: true)
    }
    
    func doSearchWith(keyword: String) {
        
        if !keyword.isEmpty {
            SmartEleSearchContainViewModel.getEleInforByCode(code: keyword, view: self.containView)
        }
        
    }
    
    
}

extension SmartEleSearchViewController:SmartEleSearchContainViewDelegate{
    
    func bindBtnClick(deviceInforModel: EleBindModel) {
        
        let model = VCInitModel()
        model.deviceCategory = DeviceModel.SmartElectricitySystem.SmartPowerGateway
        model.navTitleText = "智慧用电网关"
        let addVC = DeviceAddViewController.init()
        addVC.addModel = model
        addVC.delegate = self
        addVC.deviceFatherID = deviceInforModel.device_id
        self.navigationController?.pushViewController(addVC, animated: true)
        
    }
    
    
}

extension SmartEleSearchViewController:DeviceAddViewControllerDelegate{
    
    func addSuccess(dataSource: Array<Dictionary<String, EditItemModel>>) {
        self.containView.bindInforData = dataSource
    }
    
    
}

extension SmartEleSearchViewController:QRScanViewControllerDelegate{
    
    func scanResult(qrString string: String) {
        self.topSearchView.textfield.text = string
        doSearchWith(keyword: string)
    }
    
    
}
