//
//  SmartEleSearchContainView.swift
//  NeatIoT
//
//  Created by neat on 2019/11/26.
//  Copyright © 2019 Neat. All rights reserved.
//

import UIKit


protocol SmartEleSearchContainViewDelegate {
    func bindBtnClick(deviceInforModel:EleBindModel)
}

class SmartEleSearchContainView: UIView {
    
    let lableHSpace:CGFloat = 10
    let lableVSpace:CGFloat = 10
    let lableHeight:CGFloat = 25
    
    var delegate:SmartEleSearchContainViewDelegate?
    
    private var responseModel:EleSearchResponse = EleSearchResponse()
    
    var eleResponseModel: EleSearchResponse {
        get {
            responseModel
        }
        set {
            responseModel = newValue
            
            configerDeviceInfor(responseModel: responseModel)
            
            if !responseModel.device_infor!.device_is_bind {
                configerBindView()
            }
        }
    }
    
    var bindInforData:Array<Dictionary<String,EditItemModel>>{
        get{
            return []
        }
        set{
            if !self.bindView.isHidden {
                self.bindView.isHidden = true
            }
            configerBindInfor(dataSource: newValue)
        }
    }
    
    lazy var containScrollView: UIScrollView = {
        let containScrollView = UIScrollView.init()
        containScrollView.backgroundColor = NormalColor.viewControllerDefaultBgColor
        return containScrollView
    }()
    
    /// 设备信息view
    lazy var deviceInforContainView: UIView = {
        let containView = UIView.init()
        containView.backgroundColor = .white
        return containView
    }()
    
    /// 绑定信息view
    lazy var bindInforContainView: UIView = {
        let bindInforView = UIView.init()
        bindInforView.backgroundColor = .white
        return bindInforView
    }()
    
    /// 绑定view
    lazy var bindView: UIView = {
        let view = UIView.init()
        view.backgroundColor = .white
        return view
    }()

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.addSubview(self.containScrollView)
        
        self.containScrollView.addSubview(self.deviceInforContainView)
        
        self.containScrollView.addSubview(self.bindInforContainView)
        
        self.containScrollView.addSubview(self.bindView)
        
        self.deviceInforContainView.isHidden = true
        self.bindInforContainView.isHidden = true
        self.bindView.isHidden = true
        
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        
        self.containScrollView.frame = self.bounds
        self.containScrollView.contentSize = CGSize.init(width: self.bounds.size.width, height: self.bounds.size.height)
        
        self.deviceInforContainView.frame = CGRect.init(x: 0, y: 0, width: self.bounds.size.width, height:self.lableHeight * 5 + self.lableVSpace * 6)
        
        let y = self.deviceInforContainView.frame.origin.y + self.deviceInforContainView.frame.size.height + 10
        
        self.bindInforContainView.frame = CGRect.init(x: 0, y: y, width: self.bounds.size.width, height: 350)
        
        self.bindView.frame = CGRect.init(x: 0, y: y, width: self.bounds.size.width, height: 45)
        
        
    }
    
    func configerDeviceInfor(responseModel:EleSearchResponse) {
        
        var testArr = Array<String>()
        
        testArr.append("设备编码："+responseModel.device_infor!.device_code)
        testArr.append("SIM卡号："+responseModel.device_infor!.device_sim_code)
        testArr.append("软件版本："+responseModel.device_infor!.device_sf_version)
        testArr.append("上传周期："+responseModel.device_infor!.device_time_out)
        testArr.append("更新日期："+responseModel.device_infor!.device_update_time)
        
        for index in 0..<testArr.count {
            
            let indexCG = CGFloat.init(integerLiteral: index)
            
            let label = UILabel.init(frame: CGRect.init(x: lableHSpace, y: lableVSpace + indexCG * (lableHeight + lableVSpace), width: self.bounds.width - lableHSpace*2, height: lableHeight))
            label.font = UIFont.systemFont(ofSize: 16)
            label.textColor = #colorLiteral(red: 0.2509803922, green: 0.2509803922, blue: 0.2509803922, alpha: 1)
            label.text = testArr[index]
            
            self.deviceInforContainView.addSubview(label)
            
        }
        
        self.deviceInforContainView.isHidden = false
        
    }
    
    func configerBindInfor(dataSource:Array<Dictionary<String,EditItemModel>>) {
        
        let titleLable = UILabel.init(frame: CGRect.init(x: 10, y: 10, width: self.bounds.size.width - 20, height: 25))
        titleLable.text = "绑定信息"
        titleLable.textColor = #colorLiteral(red: 0.2509803922, green: 0.2509803922, blue: 0.2509803922, alpha: 1)
        titleLable.font = UIFont.systemFont(ofSize: 16)
        self.bindInforContainView.addSubview(titleLable)
        
        
        let sepLineY = titleLable.frame.origin.y + titleLable.frame.size.height + 10
        let sepLine = UIImageView.init(image: UIImage.init(named: "grad_sep_line"))
        sepLine.frame = CGRect.init(x: 10, y: sepLineY, width: self.bounds.width - 20, height: 1)
        self.bindInforContainView.addSubview(sepLine)
        
        let titleArr = ["","中心","企业","建筑","部位","位置","负责人","电话","变比"]
        
        for index in 1..<dataSource.count {
            
            let indexCG = CGFloat.init(integerLiteral: index-1)
            let titleLableY = lableVSpace + indexCG * (lableHeight + lableVSpace) + sepLineY + 1
            
            //取出对应项目的内容
            var model = EditItemModel()
            let dic = dataSource[index]
            for key in dic.keys {
                model = dic[key]!
            }
            
            //标题lable
            let titleLabel = UILabel.init(frame: CGRect.init(x: lableHSpace, y: titleLableY, width: 65, height: lableHeight))
            titleLabel.font = UIFont.systemFont(ofSize: 16)
            titleLabel.textColor = #colorLiteral(red: 0.2509803922, green: 0.2509803922, blue: 0.2509803922, alpha: 1)
            
            let titleStr = titleArr[index]+"："
            
            if titleStr.count < 4 {
                let attrString = NSMutableAttributedString.init(string: titleStr)
                attrString.addAttribute(.kern, value: NSNumber.init(integerLiteral: 17), range: NSRange.init(location: 0, length: 1))

                titleLabel.attributedText = attrString
            }else{
                titleLabel.text = titleStr
            }
    
            let inforLableX = titleLabel.frame.origin.x + titleLabel.frame.size.width
            let inforLableWidth = self.bounds.size.width - inforLableX - lableHSpace
            
            let inforLable = UILabel.init(frame: CGRect.init(x: inforLableX, y: titleLableY, width: inforLableWidth, height: lableHeight))
            inforLable.font = UIFont.systemFont(ofSize: 16)
            inforLable.text = model.inforText
            inforLable.textColor = #colorLiteral(red: 0.2509803922, green: 0.2509803922, blue: 0.2509803922, alpha: 1)
            inforLable.textAlignment = .left
            
            self.bindInforContainView.addSubview(titleLabel)
            self.bindInforContainView.addSubview(inforLable)
            
        }
    
        self.bindInforContainView.isHidden = false
    }
    
    func configerBindView() {
        
        let titleLable = UILabel.init(frame: CGRect.init(x: 10, y: 10, width: self.bounds.size.width - 20, height: 25))
        titleLable.text = "绑定信息"
        titleLable.textColor = #colorLiteral(red: 0.2509803922, green: 0.2509803922, blue: 0.2509803922, alpha: 1)
        titleLable.font = UIFont.systemFont(ofSize: 16)
        self.bindView.addSubview(titleLable)
        
        let bindBtn = UIButton.init(type: .custom)
        bindBtn.setTitle("绑定", for: .normal)
        bindBtn.titleLabel?.font = UIFont.systemFont(ofSize: 16)
        bindBtn.backgroundColor = #colorLiteral(red: 0.9372549057, green: 0.5411764979, blue: 0.5607843399, alpha: 1)
        bindBtn.layer.cornerRadius = 15
        bindBtn.addTarget(self, action: #selector(bindBtnClick), for: .touchUpInside)
        self.bindView.addSubview(bindBtn)
        
        bindBtn.snp.makeConstraints { (make) in
            make.right.equalTo(self.bindView.snp.right).offset(-10)
            make.height.equalTo(30)
            make.width.equalTo(60)
            make.centerY.equalTo(self.bindView.snp.centerY)
        }
        
        self.bindView.isHidden = false
        
    }
    
    @objc func bindBtnClick() {
        self.delegate?.bindBtnClick(deviceInforModel: self.eleResponseModel.device_infor!)
    }

}
