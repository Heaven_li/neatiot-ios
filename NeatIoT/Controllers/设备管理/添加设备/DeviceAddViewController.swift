//
//  DeviceAddViewController.swift
//  NeatIoT
//
//  Created by neat on 2019/11/12.
//  Copyright © 2019 Neat. All rights reserved.
//

import UIKit

protocol DeviceAddViewControllerDelegate {
    func addSuccess(dataSource:Array<Dictionary<String,EditItemModel>>)
}
class DeviceAddViewController: SupperViewController {
    
    var addModel:VCInitModel?
    
    /// 添加网关下附属器件时取建筑用的企业id
    var enterpriseID:String = ""
    /// 添加网关下附属器件时用到的父器件id
    var deviceFatherID:String = ""
    
    /// 当前编辑model
    var curretnEditModel:EditItemModel?
    
    var delegate:DeviceAddViewControllerDelegate?
    
    lazy var inputPopView: InputPopView = {
        let popview = InputPopView.init()
        popview.delegate = self
        return popview
    }()
    
    lazy var chosePopView: SelectPopView = {
        let popView = SelectPopView.init()
        popView.leftBtnImage = UIImage.init(named: "input_icon")!
        popView.rightBtnImage = UIImage.init(named: "qr_code")!
        popView.delegate = self
        return popView
    }()
    
    lazy var inforSelectPopView: InforSelectView = {
        let view = InforSelectView.init()
        view.delegate = self
        return view
    }()
    
    lazy var containView: AddDeviceContainView = {
        let view = AddDeviceContainView.init()
        view.delegate = self
        return view
    }()
    
    lazy var textFielde: UITextField = {
        let tf = UITextField.init()
        return tf
    }()
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        self.titleName = "添加"+self.addModel!.navTitleText
        
        self.view.addSubview(self.textFielde)
        
        self.view.addSubview(self.containView)
        
        self.inputPopView.showInputTo(view: self.view)
        
        self.containView.snp.makeConstraints { (make) in
            make.top.leading.trailing.equalTo(self.view)
            if NormalConstant.ISLHP{
                make.bottom.equalTo(self.view.snp.bottom)
            }else{
                make.bottom.equalTo(self.view.snp.bottom)
            }
        }
        
        self.containView.dataSource = AddDeviceContainViewModel.initDataSource(deviceCategory: (self.addModel!.gatewayMenuModel?.model)!)
        AddDeviceContainViewModel.deviceEnterpriseID = self.enterpriseID
        AddDeviceContainViewModel.deviceParentID = self.deviceFatherID
    
    }
    
    override func viewWillAppear(_ animated: Bool) {
        NotificationCenter.default.addObserver(self.inputPopView, selector: #selector(self.inputPopView.keyboardWillShow(notify:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self.inputPopView, selector: #selector(self.inputPopView.keyboardWillHiden(notify:)), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self.inputPopView, name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.removeObserver(self.inputPopView, name: UIResponder.keyboardWillHideNotification, object: nil)
    }

}

extension DeviceAddViewController:AddDeviceContainViewDelegate{
    
    func addSuccess(response: DeviceAddResponse) {
        
        if self.addModel?.deviceCategory == DeviceModel.SmartElectricitySystem.SmartPowerGateway {
            
            self.delegate?.addSuccess(dataSource: self.containView.dataSource)
            self.navigationController?.popViewController(animated: true)
            
        }else{
            
            
            let menuModel = MenuModel.init()
            menuModel.model = self.addModel?.gatewayMenuModel?.model
            menuModel.name = self.addModel?.gatewayMenuModel?.name ?? ""
            
            let initModel = VCInitModel.init()
            initModel.gatewayMenuModel = menuModel
            initModel.navTitleText = menuModel.name
            
            let vc = DeviceDetailViewController.init()
            vc.initModel = initModel
            vc.deviceId = response.deviceId!
            vc.deviceFatherID = self.deviceFatherID
            vc.isShowBottomControlBar = true
            self.navigationController?.pushViewController(vc, animated: true)
            
        }
    }

    
    func didChangeSignalType(model: EditItemModel) {
        
        AddDeviceContainViewModel.updataWithModelWithInforType(model: model)
        self.containView.dataSource = AddDeviceContainViewModel.dataSourceArr
        self.containView.containTableView.reloadData()
    }
    
    func didSelectItem(model: EditItemModel) {
        //开始编辑时 记录当前编辑项
        curretnEditModel = model
        
        if model.editType == .InputStyle {
            
            if !model.isEditAble {
                NeatHud.showMessage(message: model.titleText+"不可修改", view: self.view)
                return
            }
            
            //输入
            self.inputPopView.currentEditItemModel = model
            if model.inforType == DeviceInforType.true_mean_type || model.inforType == DeviceInforType.false_mean_type ||
               model.inforType == DeviceInforType.lower_limit_warning || model.inforType == DeviceInforType.lower_lower_limit_warning ||
            model.inforType == DeviceInforType.upper_limit_warning || model.inforType == DeviceInforType.upper_upper_limit_warning{
                self.inputPopView.isShowCheck = true
            }else{
                self.inputPopView.isShowCheck = false
            }
            self.inputPopView.currentEditItemModel = model
            
            self.textFielde.becomeFirstResponder()
            
        }else if model.editType == .SelectStyle{
            
            //列表选择
            let configerModel = InforSelectConfigerModel.init()
            configerModel.deviceCategory = (self.addModel!.gatewayMenuModel?.model)!
            
            if addModel?.gatewayMenuModel?.model == DeviceModel.FireSystem.IntegratedTransmissionHost
                || addModel?.gatewayMenuModel?.model == DeviceModel.WaterSystem.NeatWaterSingal
                || addModel?.gatewayMenuModel?.model == DeviceModel.WaterSystem.NT8160A_device{
                
                //如果网关下器件
                if model.inforType == DeviceInforType.building{
                
                    configerModel.apiParamet = self.enterpriseID
                    
                }else if model.inforType == DeviceInforType.keypart{
                    
                    let str = getItmeData(itemInforType: DeviceInforType.building)
                    
                    if str.isEmpty {
                        NeatHud.showMessage(message: "请先选择建筑", view: self.view)
                        return
                    }else{
                        configerModel.apiParamet = str
                    }
                    
                }
                
            }else{
                
                if model.inforType == DeviceInforType.enterprise{
                    
                    let str = getItmeData(itemInforType: DeviceInforType.domain)
                    
                    if str.isEmpty {
                        NeatHud.showMessage(message: "请先选择中心", view: self.view)
                        return
                    }else{
                        configerModel.apiParamet = str
                    }
                
                }else if model.inforType == DeviceInforType.building{
                    
                    let str = getItmeData(itemInforType: DeviceInforType.enterprise)
                    
                    if str.isEmpty {
                        NeatHud.showMessage(message: "请先选择企业", view: self.view)
                        return
                    }else{
                        configerModel.apiParamet = str
                    }
                    
                }else if model.inforType == DeviceInforType.keypart{
                    
                    let str = getItmeData(itemInforType: DeviceInforType.building)
                    
                    if str.isEmpty {
                        NeatHud.showMessage(message: "请先选择建筑", view: self.view)
                        return
                    }else{
                        configerModel.apiParamet = str
                    }
                    
                }
                
            }
            
            configerModel.inforType = model.inforType
            configerModel.inforTitle = model.titleText
            self.inforSelectPopView.showSelectView(view: self.view, model: configerModel)
            
        }else if model.editType == .ChoseStyle{
            
            //选择
            self.chosePopView.show(view: self.view,model:model)
            
        }else if model.editType == .GISStyle{
            
            //地图
            let gisVC = GISViewController.init()
            gisVC.delegate = self
            gisVC.isEditState = true
            gisVC.curretnEidtItemModel = model
            self.navigationController?.pushViewController(gisVC, animated: true)
            
        }
    
    }

    /// 根据数据类型 获取上传信息
    /// - Parameter itemInforType: 数据类型
    func getItmeData(itemInforType:String) -> String {
        let dataSource = AddDeviceContainViewModel.dataSourceArr
        var retureStr = ""
        for dic in dataSource {
            if dic.keys.contains(itemInforType) {
                let domainId = dic[itemInforType]!.uploadInfor
                if !domainId.isEmpty {
                    retureStr = domainId
                }
            }
        }
        return retureStr
    }
    
    /// 更新随动变更数据
    /// - Parameter model: 当前选中的数据信息
    func refreshDeviceLocalInforByItemInforType(model: KeyValueModel) {
        
        let dataSource = AddDeviceContainViewModel.dataSourceArr
        
        if curretnEditModel!.inforType == DeviceInforType.domain {
            
            let str = getItmeData(itemInforType: DeviceInforType.domain)
            
            if str != model.item_id {
                //有更改 更新企业 建筑 部位
                
                for dic in dataSource {
                    if dic.keys.contains(DeviceInforType.enterprise) {
                        let itemModel = dic[DeviceInforType.enterprise]!
                        itemModel.inforText = ""
                        itemModel.uploadInfor = ""
                    }else if dic.keys.contains(DeviceInforType.building) {
                        let itemModel = dic[DeviceInforType.building]!
                        itemModel.inforText = ""
                        itemModel.uploadInfor = ""
                    }else if dic.keys.contains(DeviceInforType.keypart) {
                        let itemModel = dic[DeviceInforType.keypart]!
                        itemModel.inforText = ""
                        itemModel.uploadInfor = ""
                    }
                }
                
            }
            
        }else if curretnEditModel!.inforType == DeviceInforType.enterprise {
            
            let str = getItmeData(itemInforType: DeviceInforType.enterprise)
            if str != model.item_id {
                //有更改 更新 建筑 部位
                for dic in dataSource {
                    if dic.keys.contains(DeviceInforType.building) {
                        let itemModel = dic[DeviceInforType.building]!
                        itemModel.inforText = ""
                        itemModel.uploadInfor = ""
                    }else if dic.keys.contains(DeviceInforType.keypart) {
                        let itemModel = dic[DeviceInforType.keypart]!
                        itemModel.inforText = ""
                        itemModel.uploadInfor = ""
                    }
                }
            }
            
        }else if curretnEditModel!.inforType == DeviceInforType.building {
            
            let str = getItmeData(itemInforType: DeviceInforType.building)
            if str != model.item_id {
                //有更改 更新 部位
                for dic in dataSource {
                    if dic.keys.contains(DeviceInforType.keypart) {
                        let itemModel = dic[DeviceInforType.keypart]!
                        itemModel.inforText = ""
                        itemModel.uploadInfor = ""
                    }
                }
            }
            
        }
    }
}

extension DeviceAddViewController:InputPopViewDelegate{
    
    func didInputText(text: String, isSelect: Bool) {
        
        curretnEditModel?.inforText = text
        curretnEditModel?.uploadInfor = text
        curretnEditModel?.uploadIsWarning = isSelect
        
        AddDeviceContainViewModel.updataWithModelWithInforType(model: curretnEditModel!)
        self.containView.containTableView.reloadData()
    }

}

extension DeviceAddViewController:SelectPopViewDelegate{
    
    func didSelectItem(item: ButtonItem, editModel: EditItemModel) {
        switch item {
            case .left:
               
                self.inputPopView.currentEditItemModel = editModel
                self.inputPopView.isShowCheck = false
                self.textFielde.becomeFirstResponder()
                
                break
            case .right:
                
                let vcModel = VCInitModel()
                vcModel.deviceCategory = self.addModel!.deviceCategory
                vcModel.navTitleText = "扫一扫"
                let qrVC = QRScanViewController.init()
                qrVC.delegate = self
                qrVC.vcInit = vcModel
                self.navigationController?.pushViewController(qrVC, animated: true)
            
                break
            default:
                break
        }
    }
}

extension DeviceAddViewController:InforSelectViewDelegate{
    
    func selectInfor(model: KeyValueModel) {
    
        //更新会根据当前编辑内容做界面变更的数据项
        refreshDeviceLocalInforByItemInforType(model: model)
        
        curretnEditModel?.inforText = model.item_name
        curretnEditModel?.uploadInfor = model.item_id
        
        AddDeviceContainViewModel.updataWithModelWithInforType(model: curretnEditModel!)
        self.containView.dataSource = AddDeviceContainViewModel.dataSourceArr

        
    }
    
}

extension DeviceAddViewController:GISViewControllerDelegate{
    
    func didFinishGISMark(model: EditItemModel) {
        
        curretnEditModel?.inforText = model.uploadInfor
        curretnEditModel?.uploadInfor = model.uploadInfor
        curretnEditModel?.latitude = model.latitude
        curretnEditModel?.longitude = model.longitude
        AddDeviceContainViewModel.updataWithModelWithInforType(model: curretnEditModel!)
        self.containView.dataSource = AddDeviceContainViewModel.dataSourceArr
    
    }
    
}

extension DeviceAddViewController:QRScanViewControllerDelegate{
    
    func scanResult(qrString: String) {
        
        if ( addModel?.deviceCategory == DeviceModel.WaterSystem.wireless_NT8327){
            //neat 水网关
            
            let str = qrString.getDeviceCode(nativeStr: qrString, insertIndex: 2)
            curretnEditModel?.inforText = str
            curretnEditModel?.uploadInfor = str

        }else if (addModel?.deviceCategory == DeviceModel.WaterSystem.NeatWaterSingal){
            //neat 水信号
            let str = qrString.getDeviceCode(nativeStr: qrString, insertIndex: 2)
            curretnEditModel?.inforText = str
            curretnEditModel?.uploadInfor = str

        }else{
            curretnEditModel?.inforText = qrString
            curretnEditModel?.uploadInfor = qrString
        }
        
        AddDeviceContainViewModel.updataWithModelWithInforType(model: curretnEditModel!)
        self.containView.dataSource = AddDeviceContainViewModel.dataSourceArr
        
    }
    
    
}
