//
//  DeviceNormalItemTableViewCell.swift
//  NeatIoT
//
//  Created by neat on 2019/11/13.
//  Copyright © 2019 Neat. All rights reserved.
//

import UIKit

protocol DeviceNormalItemTableViewCellDelegate {
    
    func inputItemByModel(model:EditItemModel)
}

class DeviceNormalItemTableViewCell: UITableViewCell {
    
    var currentModel:EditItemModel = EditItemModel()
    
    var delegate:DeviceNormalItemTableViewCellDelegate?
    
    @IBOutlet weak var titleLable: UILabel!
    
    @IBOutlet weak var arrowImageView: UIImageView!
    
    @IBOutlet weak var inforLable: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        let gesture = UITapGestureRecognizer.init(target: self, action: #selector(tapGesture(gesture:)))
        self.inforLable.addGestureRecognizer(gesture)
        
        
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configerCell(itemDic:[String:EditItemModel]) {
        
        self.inforLable.isUserInteractionEnabled = true
        
        for model in itemDic.values {
            currentModel = model
        }
        
//        if !currentModel.isEditAble {
//            self.inforLable.isUserInteractionEnabled = false
//        }
        
        if currentModel != nil {
            if currentModel.editType == .SelectStyle {
                self.arrowImageView.isHidden = false
                
                self.arrowImageView.isHidden = !currentModel.isEditState
            }else{
                self.arrowImageView.isHidden = true
            }
            
            self.titleLable.text = currentModel.titleText
            
            if currentModel.inforText.isEmpty{
                
                if currentModel.isEditState {
                    self.inforLable.text = currentModel.holderText
                }else{
                    self.inforLable.text = ""
                }
                self.inforLable.textColor = #colorLiteral(red: 0.5372058749, green: 0.5372863412, blue: 0.5371883512, alpha: 1)
            }else{
                self.inforLable.text = currentModel.inforText
                self.inforLable.textColor = #colorLiteral(red: 0.4432226503, green: 0.4432226503, blue: 0.4432226503, alpha: 1)
            }
        }
        
        
    }
    
    @objc func tapGesture(gesture:UITapGestureRecognizer) {
        
        self.delegate?.inputItemByModel(model: currentModel)
        
    }
    
}
