//
//  CheckBoxTableViewCell.swift
//  NeatIoT
//
//  Created by neat on 2019/11/20.
//  Copyright © 2019 Neat. All rights reserved.
//

import UIKit


protocol CheckBoxTableViewCellDelegate {
    func checkCellDidChange(model:EditItemModel)
}

class CheckBoxTableViewCell: UITableViewCell {
    
    var delegate:CheckBoxTableViewCellDelegate?
    
    var currentModel:EditItemModel = EditItemModel()
    
    lazy var titleLable: UILabel = {
        let lable = UILabel.init()
        lable.font = UIFont.systemFont(ofSize: 16)
        return lable
    }()
    
    lazy var checkView: CheckRadioGroupView = {
        
        let checkView = CheckRadioGroupView.init()
        checkView.isMuitleCheck = false
        checkView.delegate = self
        
        return checkView
    }()
    
    
    override func awakeFromNib() {
        super.awakeFromNib()

        self.addSubview(self.titleLable)
        
        self.titleLable.snp.makeConstraints { (make) in
            make.leading.equalTo(self).offset(16)
            make.top.bottom.equalTo(self)
            make.width.equalTo(100)
        }
        
        self.addSubview(self.checkView)
        
        checkView.snp.makeConstraints { (make) in
        
            make.top.equalTo(self).offset(10)
            make.bottom.equalTo(self).offset(-10)
            make.trailing.equalTo(self)
            make.width.equalTo(200)
            
        }
        
        let itemOne = CheckRadioItemModel.init()
        itemOne.isRadioCheck = false
        itemOne.radioTitle = "模拟量"
        itemOne.radioKey = "1"
        
        let itemTwo = CheckRadioItemModel.init()
        itemTwo.isRadioCheck = false
        itemTwo.radioTitle = "数字量"
        itemTwo.radioKey = "2"
        
        checkView.radioItemArr = [itemOne,itemTwo]
        
        
    }
    
    func configerCell(itemDic:[String:EditItemModel]) {
    
        
        for model in itemDic.values {
            currentModel = model
        }
        
        if currentModel.isEditAble && currentModel.isEditState{
            self.checkView.isUserInteractionEnabled = true
        }else{
            self.checkView.isUserInteractionEnabled = false
        }
        
        
        
        self.titleLable.text = currentModel.titleText
        
        if currentModel.uploadInfor == "1" {
            
            let itemOne = CheckRadioItemModel.init()
            itemOne.isRadioCheck = true
            itemOne.radioTitle = currentModel.leftRadioTitle
            itemOne.radioKey = "1"
            
            let itemTwo = CheckRadioItemModel.init()
            itemTwo.isRadioCheck = false
            itemTwo.radioTitle = currentModel.rightRadioTitle
            itemTwo.radioKey = "2"
            
            checkView.radioItemArr = [itemOne,itemTwo]
            
        }else if currentModel.uploadInfor == "2" {
            
            let itemOne = CheckRadioItemModel.init()
            itemOne.isRadioCheck = false
            itemOne.radioTitle = currentModel.leftRadioTitle
            itemOne.radioKey = "1"
            
            let itemTwo = CheckRadioItemModel.init()
            itemTwo.isRadioCheck = true
            itemTwo.radioTitle = currentModel.rightRadioTitle
            itemTwo.radioKey = "2"
            
            checkView.radioItemArr = [itemOne,itemTwo]
            
        }
        
        
        

        
    }
    

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}

extension CheckBoxTableViewCell:CheckRadioGroupViewDelegate{
    
    func checkRadioGroupValueChanged(checkRadioItems: [CheckRadioItemModel]) {
        
        for item in checkRadioItems {
                   
            debugPrint("当前选中项目：",item.radioKey,item.radioTitle)
            currentModel.inforText = item.radioKey
            currentModel.uploadInfor = item.radioKey
            
            self.delegate?.checkCellDidChange(model: currentModel)

        }
        
    }
    
}
