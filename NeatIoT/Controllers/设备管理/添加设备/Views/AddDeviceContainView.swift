//
//  AddDeviceContainView.swift
//  NeatIoT
//
//  Created by neat on 2019/11/13.
//  Copyright © 2019 Neat. All rights reserved.
//

import UIKit

protocol AddDeviceContainViewDelegate {
    
    func didSelectItem(model:EditItemModel)
    
    func didChangeSignalType(model:EditItemModel)
    
    func addSuccess(response:DeviceAddResponse)
}

class AddDeviceContainView: UIView {
    
    let TextCellIdent = "TextCellIdent"
    let CheckBoxCellIdent = "CheckBoxCellIdent"
    
    var delegate:AddDeviceContainViewDelegate?
    
    private var dataSourceArr:Array<Dictionary<String,EditItemModel>> = []
    
    var dataSource: Array<Dictionary<String,EditItemModel>> {
        get {
            dataSourceArr
        }
        set {
            dataSourceArr = newValue
            self.containTableView.reloadData()
        }
    }
    
    lazy var containTableView: UITableView = {
        
        let tableview = UITableView.init(frame: CGRect.zero, style: UITableView.Style.plain)
        tableview.bounces = false
        tableview.delegate = self
        tableview.dataSource = self
        tableview.backgroundColor = UIColor.clear
        tableview.separatorStyle = .singleLine
        tableview.separatorColor = NormalColor.tableviewSepLineColor
        tableview.separatorInset = UIEdgeInsets.init(top: 0, left: 3, bottom: 0, right: 3)
        tableview.register(UINib.init(nibName: "DeviceNormalItemTableViewCell", bundle: Bundle.main), forCellReuseIdentifier: self.TextCellIdent)
        tableview.register(UINib.init(nibName: "CheckBoxTableViewCell", bundle: Bundle.main), forCellReuseIdentifier: self.CheckBoxCellIdent)
        
        return tableview
    }()
    
    lazy var commiteBtn: LoadingButton = {
        
        let button = LoadingButton.init()
        button.titleText = "确认保存"
        button.delegate = self
        return button
    }()
    
    lazy var lhpHolderView: UIView = {
        let view = UIView.init()
        view.backgroundColor = #colorLiteral(red: 0.9743468165, green: 0.5453266501, blue: 0.5514759421, alpha: 1)
        return view
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        
        self.addSubview(self.containTableView)
        self.addSubview(self.commiteBtn)
        
        if NormalConstant.ISLHP {
            
            self.addSubview(self.lhpHolderView)
            self.lhpHolderView.snp.makeConstraints { (make) in
                make.leading.trailing.bottom.equalTo(self)
                make.height.equalTo(NormalConstant.LHPBottom_Space)
            }
        }
        self.commiteBtn.snp.makeConstraints { (make) in
            
            if NormalConstant.ISLHP{
                make.bottom.equalTo(self.lhpHolderView.snp.top)
            }else{
                make.bottom.equalTo(self)
            }
            
            make.leading.trailing.equalTo(self)
            make.height.equalTo(self.snp.height).multipliedBy(0.08)
        }
        
        self.containTableView.snp.makeConstraints { (make) in
            make.top.leading.trailing.equalTo(self)
            make.bottom.equalTo(self.commiteBtn.snp.top)
        }
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}

extension AddDeviceContainView:LoadingButtonDelegate{
    func didClickButton() {
        commiteBtn.isLoading = true
        lhpHolderView.backgroundColor = .darkGray
        AddDeviceContainViewModel.getUploadInforModel(success: {(response) in
            self.commiteBtn.isLoading = false
            self.delegate?.addSuccess(response: response)
        }) { (error) in
            self.commiteBtn.isLoading = false
            self.lhpHolderView.backgroundColor = #colorLiteral(red: 0.9743468165, green: 0.5453266501, blue: 0.5514759421, alpha: 1)
            NeatHud.showMessage(message: error.errMessage, view: self)
        }
    }

}

extension AddDeviceContainView:UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.1
    }
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView.init()
    }
    
}

extension AddDeviceContainView:UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let model = dataSourceArr[indexPath.row]
        var editModel = EditItemModel()
        if model.values.count != 0 {
            for item in model.values {
                editModel = item
            }
        }
        
        if editModel.editType == ItemEditStyle.RadioBtnStyle {
            let cell = tableView.dequeueReusableCell(withIdentifier: CheckBoxCellIdent, for: indexPath) as!CheckBoxTableViewCell
            cell.selectionStyle = .none
            cell.configerCell(itemDic:dataSourceArr[indexPath.row])
            cell.delegate = self
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: TextCellIdent, for: indexPath) as!DeviceNormalItemTableViewCell
            cell.selectionStyle = .none
            cell.delegate = self
            cell.configerCell(itemDic: dataSourceArr[indexPath.row])
            return cell
        }
        
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.dataSourceArr.count
    }
    
}

extension AddDeviceContainView:DeviceNormalItemTableViewCellDelegate{
    
    func inputItemByModel(model: EditItemModel) {
        self.delegate?.didSelectItem(model: model)
    }

}

extension AddDeviceContainView:CheckBoxTableViewCellDelegate{
    
    func checkCellDidChange(model: EditItemModel) {
        self.delegate?.didChangeSignalType(model: model)
    }
    
}
