//
//  AddDeviceContainViewModel.swift
//  NeatIoT
//
//  Created by neat on 2019/11/13.
//  Copyright © 2019 Neat. All rights reserved.
//

import UIKit

/// 添加设备数据model 用于配置添加页面展示数据
class AddDeviceContainViewModel: NSObject {
    
    
    
    /// 当前设备类型
    private static var currentCategory = ""
    
    private static var currentDeviceParentID:String = ""
    
    private static var currentEnterpriseID:String = ""
    
    static var deviceEnterpriseID:String {
        get {
            currentEnterpriseID
        }
        set {
            currentEnterpriseID = newValue
        }
    }
    
    static var deviceParentID:String {
        get {
            currentDeviceParentID
        }
        set {
            currentDeviceParentID = newValue
        }
    }
    
    /// 非水信号设备
    private static var dataSource:Array<Dictionary<String,EditItemModel>> = []
    /// 水信号 模拟量 数据源
    private static var analogDataSource:Array<Dictionary<String,EditItemModel>> = []
    /// 水信号 数字量 数据源
    private static var digitalDataSource:Array<Dictionary<String,EditItemModel>> = []
    /// 万讯数据源
    private static var wxDataSource:Array<Dictionary<String,EditItemModel>> = []
    /// 拓普索尔数据源
    private static var tpseDataSource:Array<Dictionary<String,EditItemModel>> = []
    /// 铭控8167c数据源
    private static var mkDataSource:Array<Dictionary<String,EditItemModel>> = []
    /// 获取当前展示数据源
    static var dataSourceArr: Array<Dictionary<String,EditItemModel>> {
        return self.dataSource
    }

    /// 根据设备类型配置展示数据源
    /// - Parameter deviceCategory: 设备类型
    class func initDataSource(deviceCategory:String) -> Array<Dictionary<String,EditItemModel>> {
        
        currentCategory = deviceCategory
        
        if deviceCategory == DeviceModel.FireSystem.uitd_NT9009{
            //一体化传输装置
            let typeArr = [DeviceInforType.code,DeviceInforType.name,DeviceInforType.type,DeviceInforType.message_protocol
                ,DeviceInforType.manufacturer,DeviceInforType.domain,DeviceInforType.enterprise,DeviceInforType.building,
            DeviceInforType.keypart,DeviceInforType.address]
            return formateDataSource(typeArr: typeArr, categery: deviceCategory)
            
        }else if deviceCategory == DeviceModel.FireSystem.IntegratedTransmissionHost{
            //一体化主机
            let typeArr = [DeviceInforType.code,DeviceInforType.name,DeviceInforType.type,DeviceInforType.manufacturer,DeviceInforType.building,DeviceInforType.keypart,DeviceInforType.address]
            return formateDataSource(typeArr: typeArr, categery: deviceCategory)
            
        }else if deviceCategory == DeviceModel.WaterSystem.wireless_NT8327{
            //neat水网关
            let typeArr = [DeviceInforType.code,DeviceInforType.name,DeviceInforType.domain,DeviceInforType.enterprise,DeviceInforType.building,DeviceInforType.keypart,DeviceInforType.address]
            return formateDataSource(typeArr: typeArr, categery: deviceCategory)
            
        }else if deviceCategory == DeviceModel.WaterSystem.NeatWaterSingal{
            //neat水信号 模拟量
            let analogTypeArr = [DeviceInforType.code,DeviceInforType.name,DeviceInforType.signal_type,DeviceInforType.type,DeviceInforType.building,DeviceInforType.keypart,DeviceInforType.address,DeviceInforType.unit,DeviceInforType.threshold_lower_limit,DeviceInforType.threshold_upper_limit,DeviceInforType.lower_lower_limit_warning,DeviceInforType.lower_limit_warning,DeviceInforType.upper_limit_warning,DeviceInforType.upper_upper_limit_warning]
            //neat水信号 数字量
            let digitalTypeArr = [DeviceInforType.code,DeviceInforType.name,DeviceInforType.signal_type,DeviceInforType.building,DeviceInforType.keypart,DeviceInforType.address,DeviceInforType.true_mean_type,DeviceInforType.false_mean_type]
            //初始化模拟量
            analogDataSource = formateDataSource(typeArr: analogTypeArr, categery: deviceCategory)
            //初始化数字量
            digitalDataSource = formateDataSource(typeArr: digitalTypeArr, categery: deviceCategory)
            
            return formateDataSource(typeArr: analogTypeArr, categery: deviceCategory)
            
        }else if deviceCategory == DeviceModel.SmartElectricitySystem.SmartPowerGateway{
            //智慧用电网关
            let typeArr = [DeviceInforType.name,DeviceInforType.domain,DeviceInforType.enterprise,DeviceInforType.building,DeviceInforType.keypart,DeviceInforType.address,DeviceInforType.principal,DeviceInforType.contact_number,DeviceInforType.ratio_coefficient]
            return formateDataSource(typeArr: typeArr, categery: deviceCategory)
            
        }else if deviceCategory == DeviceModel.NBDevice.Probe{
            
            //NB设备
            let typeArr = [DeviceInforType.code,DeviceInforType.name,DeviceInforType.domain,DeviceInforType.enterprise,DeviceInforType.building,DeviceInforType.keypart,DeviceInforType.address,DeviceInforType.gis]
            return formateDataSource(typeArr: typeArr, categery: deviceCategory)
            
        }else if deviceCategory == DeviceModel.SmartElectricitySystem.ElectricGateway_NT8127A {
            
            //8127A
            let typeArr = [DeviceInforType.imei,DeviceInforType.name,DeviceInforType.connection_type,DeviceInforType.domain,DeviceInforType.enterprise,DeviceInforType.building,DeviceInforType.keypart,DeviceInforType.address,DeviceInforType.gis]
            return formateDataSource(typeArr: typeArr, categery: deviceCategory)
            
        }else if deviceCategory == DeviceModel.SmartElectricitySystem.ElectricGateway_NT8127C {
            
            //8127C
            let typeArr = [DeviceInforType.imei,DeviceInforType.name,DeviceInforType.connection_type,DeviceInforType.domain,DeviceInforType.enterprise,DeviceInforType.building,DeviceInforType.keypart,DeviceInforType.address,DeviceInforType.gis]
            return formateDataSource(typeArr: typeArr, categery: deviceCategory)
            
        }else if deviceCategory == DeviceModel.SmartElectricitySystem.ElectricGateway_NT8128A {
            
            //8128A
            let typeArr = [DeviceInforType.imei,DeviceInforType.name,DeviceInforType.connection_type,DeviceInforType.domain,DeviceInforType.enterprise,DeviceInforType.building,DeviceInforType.keypart,DeviceInforType.address,DeviceInforType.gis]
            return formateDataSource(typeArr: typeArr, categery: deviceCategory)
            
        }else if deviceCategory == DeviceModel.WaterSystem.Wgw_NT8164B {
            
            //万讯 液位
            let wxTypeArr = [DeviceInforType.code,DeviceInforType.name,DeviceInforType.connection_type,DeviceInforType.imei,DeviceInforType.imsi,DeviceInforType.domain,DeviceInforType.enterprise,DeviceInforType.building,DeviceInforType.keypart,DeviceInforType.address]
            
            wxDataSource = formateDataSource(typeArr: wxTypeArr, categery: deviceCategory)
            
            return formateDataSource(typeArr: wxTypeArr, categery: deviceCategory)
            
        }else if deviceCategory == DeviceModel.WaterSystem.Wgw_NT8161B {
            
            //万讯 液压
            let wxTypeArr = [DeviceInforType.code,DeviceInforType.name,DeviceInforType.connection_type,DeviceInforType.imei,DeviceInforType.imsi,DeviceInforType.domain,DeviceInforType.enterprise,DeviceInforType.building,DeviceInforType.keypart,DeviceInforType.address]
            
            wxDataSource = formateDataSource(typeArr: wxTypeArr, categery: deviceCategory)
            
            return formateDataSource(typeArr: wxTypeArr, categery: deviceCategory)
            
        }else if deviceCategory == DeviceModel.WaterSystem.Wgw_NT8167A {
            
            //拓普索尔 消火检测终端
            let tpseTypeArr = [DeviceInforType.code,DeviceInforType.name,DeviceInforType.connection_type,DeviceInforType.domain,DeviceInforType.enterprise,DeviceInforType.building,DeviceInforType.keypart,DeviceInforType.address,DeviceInforType.gis]
            tpseDataSource = formateDataSource(typeArr: tpseTypeArr, categery: deviceCategory)
            
            return formateDataSource(typeArr: tpseTypeArr, categery: deviceCategory)
            
        }else if deviceCategory == DeviceModel.WaterSystem.Wgw_NT8167C {
            
            //铭控8167c网关
            let tpseTypeArr = [DeviceInforType.code,DeviceInforType.name,DeviceInforType.connection_type,DeviceInforType.imei,DeviceInforType.domain,DeviceInforType.enterprise,DeviceInforType.building,DeviceInforType.keypart,DeviceInforType.address,DeviceInforType.gis]
            mkDataSource = formateDataSource(typeArr: tpseTypeArr, categery: deviceCategory)
            
            return formateDataSource(typeArr: tpseTypeArr, categery: deviceCategory)
            
        }else if deviceCategory == DeviceModel.WaterSystem.Wgw_NT8164A {
            
            //拓普索尔 液压
            let tpseTypeArr = [DeviceInforType.code,DeviceInforType.name,DeviceInforType.connection_type,DeviceInforType.domain,DeviceInforType.enterprise,DeviceInforType.building,DeviceInforType.keypart,DeviceInforType.address,DeviceInforType.gis]
            tpseDataSource = formateDataSource(typeArr: tpseTypeArr, categery: deviceCategory)
            
            return formateDataSource(typeArr: tpseTypeArr, categery: deviceCategory)
            
        }else if deviceCategory == DeviceModel.WaterSystem.Wgw_NT8161A {
            
            //拓普索尔 液位
            let tpseTypeArr = [DeviceInforType.code,DeviceInforType.name,DeviceInforType.connection_type,DeviceInforType.domain,DeviceInforType.enterprise,DeviceInforType.building,DeviceInforType.keypart,DeviceInforType.address,DeviceInforType.gis]
            tpseDataSource = formateDataSource(typeArr: tpseTypeArr, categery: deviceCategory)
            
            return formateDataSource(typeArr: tpseTypeArr, categery: deviceCategory)
            
        }else if deviceCategory == DeviceModel.FamilySystem.HomeGateway {
            
            //家用网关
            let tpseTypeArr = [DeviceInforType.code,DeviceInforType.name,DeviceInforType.domain,DeviceInforType.enterprise,DeviceInforType.building,DeviceInforType.keypart,DeviceInforType.address,DeviceInforType.gis]
            
            return formateDataSource(typeArr: tpseTypeArr, categery: deviceCategory)
            
        }else if deviceCategory == DeviceModel.GasDetector.HM710NVM || deviceCategory == DeviceModel.GasDetector.WS2CG{
            
            //海曼可燃气体
            let tpseTypeArr = [DeviceInforType.imei,DeviceInforType.name,DeviceInforType.connection_type,DeviceInforType.domain,DeviceInforType.enterprise,DeviceInforType.building,DeviceInforType.keypart,DeviceInforType.address,DeviceInforType.gis]
            
            return formateDataSource(typeArr: tpseTypeArr, categery: deviceCategory)
            
        }else if deviceCategory == DeviceModel.WaterSystem.NT8160A {
            
            //有人泵房检测网关
            let typeArr = [DeviceInforType.code,DeviceInforType.domain,DeviceInforType.enterprise,DeviceInforType.building,DeviceInforType.keypart,DeviceInforType.address,DeviceInforType.gis]
            return formateDataSource(typeArr: typeArr, categery: deviceCategory)
            
        }else if deviceCategory == DeviceModel.WaterSystem.NT8160A_device {
            
            //有人泵房检测网关下属器件
            
            let usrTypeArr = [DeviceInforType.code,DeviceInforType.usrDeviceType,DeviceInforType.name,DeviceInforType.building,DeviceInforType.keypart,DeviceInforType.address]
            
            return formateDataSource(typeArr: usrTypeArr, categery: deviceCategory)
            
        }
        
        return []
        
    }
    
    /// 格式化数据源
    /// - Parameters:
    ///   - typeArr: 编辑项目类型
    ///   - categery: 设备类型
    class private func formateDataSource(typeArr:[String],categery:String) -> Array<Dictionary<String,EditItemModel>> {
        
        var dataArr:Array<Dictionary<String,EditItemModel>> = []
        
        for type in typeArr {
            
            var item_dic:Dictionary<String,EditItemModel> = [:]
            
        
            let device_item = EditItemModel.init()
            device_item.isEditAble = true
            device_item.isEditState = true
            
            if type == DeviceInforType.code{
                //编码
                device_item.titleText = getItemTitleByDeviceCategory(deviceCategory: categery, type: DeviceInforType.code)
                device_item.holderText = getInputItemPlaceHolderByDeviceCategory(deviceCategory: categery, type: DeviceInforType.code)
                device_item.editType = getItemEditTypeByDeviceCategory(deviceCategory: categery, type: DeviceInforType.code)
                device_item.inforType = type
                device_item.keyboardType = .numbersAndPunctuation
                
                item_dic = [DeviceInforType.code:device_item]
                
            }else if type == DeviceInforType.name{
                //名称
                device_item.titleText = getItemTitleByDeviceCategory(deviceCategory: categery, type: DeviceInforType.name)
                device_item.holderText = "请输入"+device_item.titleText
                device_item.editType = .InputStyle
                device_item.inforType = type
                device_item.keyboardType = .default
                
                item_dic = [DeviceInforType.name:device_item]
                
            }else if type == DeviceInforType.type{
                //类型
                device_item.titleText = getItemTitleByDeviceCategory(deviceCategory: categery, type: DeviceInforType.type)
                device_item.holderText = "请选择"+device_item.titleText
                device_item.editType = .SelectStyle
                device_item.inforType = type
                
                item_dic = [DeviceInforType.type:device_item]
                
            }else if type == DeviceInforType.address{
                //安装位置
                device_item.titleText = getItemTitleByDeviceCategory(deviceCategory: categery, type: DeviceInforType.address)
                device_item.holderText = "请输入"+device_item.titleText
                device_item.editType = .InputStyle
                device_item.inforType = type
                device_item.keyboardType = .default
                
                item_dic = [DeviceInforType.address:device_item]
                
            }else if type == DeviceInforType.message_protocol{
                //通信协议
                device_item.titleText = getItemTitleByDeviceCategory(deviceCategory: categery, type: DeviceInforType.message_protocol)
                device_item.holderText = "请选择"+device_item.titleText
                device_item.editType = .SelectStyle
                device_item.inforType = type
                
                item_dic = [DeviceInforType.message_protocol:device_item]
                
            }else if type == DeviceInforType.manufacturer{
                //厂商
                device_item.titleText = getItemTitleByDeviceCategory(deviceCategory: categery, type: DeviceInforType.manufacturer)
                device_item.holderText = "请选择"+device_item.titleText
                device_item.editType = .SelectStyle
                device_item.inforType = type
                
                item_dic = [DeviceInforType.manufacturer:device_item]
                
            }else if type == DeviceInforType.domain{
                //中心
                
                device_item.titleText = getItemTitleByDeviceCategory(deviceCategory: categery, type: DeviceInforType.domain)
                device_item.holderText = "请选择"+device_item.titleText
                device_item.editType = .SelectStyle
                device_item.inforType = type
                
                let loginRes = DataArchiver.unArchiver(key:ArchireKey.UserInfor)
                
                if loginRes != nil {
                    let userInforModel = loginRes as! LoginResultModel
                    if userInforModel.user_childDomain_name.isEmpty {
                        //中心唯一
                        device_item.editType = .InputStyle
                        device_item.inforText = userInforModel.user_domain_name
                        device_item.isEditAble = false
                        device_item.uploadInfor = userInforModel.user_domain_id
                        
                    }
                    
                }
                
                
                item_dic = [DeviceInforType.domain:device_item]
                
            }else if type == DeviceInforType.enterprise{
                //企业
                device_item.titleText = getItemTitleByDeviceCategory(deviceCategory: categery, type: DeviceInforType.enterprise)
                device_item.holderText = "请选择"+device_item.titleText
                device_item.editType = .SelectStyle
                device_item.inforType = type
                
                let loginRes = DataArchiver.unArchiver(key:ArchireKey.UserInfor)
                
                if loginRes != nil {
                    let userInforModel = loginRes as! LoginResultModel
                    if !userInforModel.user_enterprise_name.isEmpty {
                        //企业唯一
                        device_item.editType = .InputStyle
                        device_item.inforText = userInforModel.user_enterprise_name
                        device_item.isEditAble = false
                        device_item.uploadInfor = userInforModel.user_enterprise_id
                        
                    }
                    
                }
                
                item_dic = [DeviceInforType.enterprise:device_item]
                
            }else if type == DeviceInforType.building{
                //建筑
                device_item.titleText = getItemTitleByDeviceCategory(deviceCategory: categery, type: DeviceInforType.building)
                device_item.holderText = "请选择"+device_item.titleText
                device_item.editType = .SelectStyle
                device_item.inforType = type
                
                item_dic = [DeviceInforType.building:device_item]
                
            }else if type == DeviceInforType.keypart{
                //部位
                device_item.titleText = getItemTitleByDeviceCategory(deviceCategory: categery, type: DeviceInforType.keypart)
                device_item.holderText = "请选择"+device_item.titleText
                device_item.editType = .SelectStyle
                device_item.inforType = type
                
                item_dic = [DeviceInforType.keypart:device_item]
                
            }else if type == DeviceInforType.gis{
                //地图描点
                device_item.titleText = getItemTitleByDeviceCategory(deviceCategory: categery, type: DeviceInforType.gis)
                device_item.holderText = "请选择"+device_item.titleText
                device_item.editType = .GISStyle
                
                item_dic = [DeviceInforType.gis:device_item]
                
            }else if type == DeviceInforType.unit{
                //测量单位
                device_item.titleText = getItemTitleByDeviceCategory(deviceCategory: categery, type: DeviceInforType.unit)
                device_item.holderText = ""
                device_item.editType = .InputStyle
                device_item.inforType = type
                device_item.isEditAble = false
                
                item_dic = [DeviceInforType.unit:device_item]
                
            }else if type == DeviceInforType.signal_type{
                //信号类型
                device_item.titleText = getItemTitleByDeviceCategory(deviceCategory: categery, type: DeviceInforType.signal_type)
                device_item.holderText = ""
                device_item.editType = .RadioBtnStyle
                device_item.inforType = type
                device_item.inforText = "1"
                device_item.uploadInfor = "1"
                device_item.leftRadioTitle = "模拟量"
                device_item.rightRadioTitle = "数字量"
                item_dic = [DeviceInforType.signal_type:device_item]
                
            }else if type == DeviceInforType.usrDeviceType{
                //有人泵房检测附属 设备类型
                device_item.titleText = getItemTitleByDeviceCategory(deviceCategory: categery, type: DeviceInforType.usrDeviceType)
                device_item.holderText = ""
                device_item.editType = .RadioBtnStyle
                device_item.inforType = type
                device_item.inforText = "1"
                device_item.uploadInfor = "1"
                device_item.leftRadioTitle = "风机"
                device_item.rightRadioTitle = "水泵"
                item_dic = [DeviceInforType.usrDeviceType:device_item]
                
            }else if type == DeviceInforType.connection_type{
                //通讯方式
                device_item.titleText = getItemTitleByDeviceCategory(deviceCategory: categery, type: DeviceInforType.connection_type)
                device_item.holderText = "请选择"+device_item.titleText
                device_item.editType = .SelectStyle
                device_item.inforType = type
                
                item_dic = [DeviceInforType.connection_type:device_item]
                
            }else if type == DeviceInforType.true_mean_type{
                //真(1)值含义
                device_item.titleText = getItemTitleByDeviceCategory(deviceCategory: categery, type: DeviceInforType.true_mean_type)
                device_item.holderText = "请输入"+device_item.titleText
                device_item.editType = .InputStyle
                device_item.inforType = type
                device_item.keyboardType = .numbersAndPunctuation
                
                item_dic = [DeviceInforType.true_mean_type:device_item]
                
            }else if type == DeviceInforType.false_mean_type{
                //假(0)值含义
                device_item.titleText = getItemTitleByDeviceCategory(deviceCategory: categery, type: DeviceInforType.false_mean_type)
                device_item.holderText = "请输入"+device_item.titleText
                device_item.editType = .InputStyle
                device_item.inforType = type
                device_item.keyboardType = .numbersAndPunctuation
                
                item_dic = [DeviceInforType.false_mean_type:device_item]
                
            }else if type == DeviceInforType.threshold_lower_limit{
                //阈值下限
                device_item.titleText = getItemTitleByDeviceCategory(deviceCategory: categery, type: DeviceInforType.threshold_lower_limit)
                device_item.holderText = "请输入"+device_item.titleText
                device_item.editType = .InputStyle
                device_item.inforType = type
                device_item.keyboardType = .decimalPad
                
                item_dic = [DeviceInforType.threshold_lower_limit:device_item]
                
            }else if type == DeviceInforType.threshold_upper_limit{
                //阈值上限
                device_item.titleText = getItemTitleByDeviceCategory(deviceCategory: categery, type: DeviceInforType.threshold_upper_limit)
                device_item.holderText = "请输入"+device_item.titleText
                device_item.editType = .InputStyle
                device_item.inforType = type
                device_item.keyboardType = .decimalPad
                
                item_dic = [DeviceInforType.threshold_upper_limit:device_item]
                
            }else if type == DeviceInforType.lower_limit_warning{
                //下限报警值
                device_item.titleText = getItemTitleByDeviceCategory(deviceCategory: categery, type: DeviceInforType.lower_limit_warning)
                device_item.holderText = "请输入"+device_item.titleText
                device_item.editType = .InputStyle
                device_item.inforType = type
                device_item.keyboardType = .decimalPad
                
                item_dic = [DeviceInforType.lower_limit_warning:device_item]
                
            }else if type == DeviceInforType.lower_lower_limit_warning{
                //下限报警值
                device_item.titleText = getItemTitleByDeviceCategory(deviceCategory: categery, type: DeviceInforType.lower_lower_limit_warning)
                device_item.holderText = "请输入"+device_item.titleText
                device_item.editType = .InputStyle
                device_item.inforType = type
                device_item.keyboardType = .decimalPad
                
                item_dic = [DeviceInforType.lower_lower_limit_warning:device_item]
                
            }else if type == DeviceInforType.upper_limit_warning{
                //上限报警值
                device_item.titleText = getItemTitleByDeviceCategory(deviceCategory: categery, type: DeviceInforType.upper_limit_warning)
                device_item.holderText = "请输入"+device_item.titleText
                device_item.editType = .InputStyle
                device_item.inforType = type
                device_item.keyboardType = .decimalPad
                
                item_dic = [DeviceInforType.upper_limit_warning:device_item]
                
            }else if type == DeviceInforType.upper_upper_limit_warning{
                //上上限报警值
                device_item.titleText = getItemTitleByDeviceCategory(deviceCategory: categery, type: DeviceInforType.upper_upper_limit_warning)
                device_item.holderText = "请输入"+device_item.titleText
                device_item.editType = .InputStyle
                device_item.inforType = type
                device_item.keyboardType = .decimalPad
                
                item_dic = [DeviceInforType.upper_upper_limit_warning:device_item]
                
            }else if type == DeviceInforType.principal{
                //负责人
                device_item.titleText = getItemTitleByDeviceCategory(deviceCategory: categery, type: DeviceInforType.principal)
                device_item.holderText = "请输入"+device_item.titleText
                device_item.editType = .InputStyle
                device_item.inforType = type
                device_item.keyboardType = .default
                
                item_dic = [DeviceInforType.principal:device_item]
                
            }else if type == DeviceInforType.contact_number{
                //联系电话
                device_item.titleText = getItemTitleByDeviceCategory(deviceCategory: categery, type: DeviceInforType.contact_number)
                device_item.holderText = "请输入"+device_item.titleText
                device_item.editType = .InputStyle
                device_item.inforType = type
                device_item.keyboardType = .phonePad
                
                item_dic = [DeviceInforType.contact_number:device_item]
                
            }else if type == DeviceInforType.ratio_coefficient{
                //变比系数
                device_item.titleText = getItemTitleByDeviceCategory(deviceCategory: categery, type: DeviceInforType.ratio_coefficient)
                device_item.holderText = "请输入"+device_item.titleText
                device_item.editType = .InputStyle
                device_item.inforType = type
                device_item.keyboardType = .decimalPad
                
                item_dic = [DeviceInforType.ratio_coefficient:device_item]
            }else if type == DeviceInforType.imei{
                //IMEI号
                device_item.titleText = getItemTitleByDeviceCategory(deviceCategory: categery, type: DeviceInforType.imei)
                device_item.holderText = "请输入"+device_item.titleText
                device_item.editType = .ChoseStyle
                device_item.inforType = type
                device_item.keyboardType = .numbersAndPunctuation
                
                item_dic = [DeviceInforType.imei:device_item]
            }else if type == DeviceInforType.imsi{
                //IMSI号
                device_item.titleText = getItemTitleByDeviceCategory(deviceCategory: categery, type: DeviceInforType.imsi)
                device_item.holderText = "请输入"+device_item.titleText
                device_item.editType = .InputStyle
                device_item.inforType = type
                device_item.keyboardType = .numbersAndPunctuation
                
                item_dic = [DeviceInforType.imsi:device_item]
            }
            
            dataArr.append(item_dic)
        }
        
        dataSource = dataArr
        
        return dataArr
        
    }
    
    /// 根据设备类型 和 编辑项目类型 返回项目title
    /// - Parameters:
    ///   - deviceCategory: 设备类型
    ///   - type: 项目类型
    class private func getItemTitleByDeviceCategory(deviceCategory:String,type:String) -> String {
        
        if deviceCategory == DeviceModel.FireSystem.uitd_NT9009{
            //一体化传输装置
            if type == DeviceInforType.code{
                //编码
                return "设备编码"
            }else if type == DeviceInforType.name{
                //名称
                return "设备名称"
            }else if type == DeviceInforType.type{
                //类型
                return "设备类型"
            }
        }else if deviceCategory == DeviceModel.FireSystem.IntegratedTransmissionHost{
            //一体化主机
            if type == DeviceInforType.code{
                //编码
                return "主机编码"
            }else if type == DeviceInforType.name{
                //名称
                return "主机名称"
            }else if type == DeviceInforType.type{
                //类型
                return "主机类别"
            }
        }else if deviceCategory == DeviceModel.WaterSystem.wireless_NT8327{
            //neat水网关
            if type == DeviceInforType.code{
                //编码
                return "设备编码"
            }else if type == DeviceInforType.name{
                //名称
                return "设备名称"
            }else if type == DeviceInforType.type{
                //类型
                return "设备类型"
            }
        }else if deviceCategory == DeviceModel.WaterSystem.NeatWaterSingal{
            //neat水信号
            if type == DeviceInforType.code{
                //编码
                return "信号编码"
            }else if type == DeviceInforType.name{
                //名称
                return "信号名称"
            }else if type == DeviceInforType.type{
                //类型
                return "设备类型"
            }else if type == DeviceInforType.signal_type{
                //信号类型
                return "信号类型"
            }
            
        }else if deviceCategory == DeviceModel.WaterSystem.Wgw_NT8164B {
            //万讯 液位
            if type == DeviceInforType.code{
                //编码
                return "设备编码"
            }else if type == DeviceInforType.name{
                //名称
                return "设备名称"
            }
            
        }else if deviceCategory == DeviceModel.WaterSystem.Wgw_NT8161B {
            //万讯 液压
            if type == DeviceInforType.code{
                //编码
                return "设备编码"
            }else if type == DeviceInforType.name{
                //名称
                return "设备名称"
            }
            
        }else if deviceCategory == DeviceModel.WaterSystem.Wgw_NT8167A {
            //拓普索尔 消火检测终端
            if type == DeviceInforType.code{
                //编码
                return "设备编码"
            }else if type == DeviceInforType.name{
                //名称
                return "设备名称"
            }
            
        }else if deviceCategory == DeviceModel.WaterSystem.Wgw_NT8167C {
            //铭控 消防栓检测终端
            if type == DeviceInforType.code{
                //编码
                return "设备ID"
            }else if type == DeviceInforType.name{
                //名称
                return "设备名称"
            }
            
        }else if deviceCategory == DeviceModel.WaterSystem.Wgw_NT8164A {
            //拓普索尔 液压
            if type == DeviceInforType.code{
                //编码
                return "设备编码"
            }else if type == DeviceInforType.name{
                //名称
                return "设备名称"
            }
            
        }else if deviceCategory == DeviceModel.WaterSystem.Wgw_NT8161A {
            //拓普索尔 液位
            if type == DeviceInforType.code{
                //编码
                return "设备编码"
            }else if type == DeviceInforType.name{
                //名称
                return "设备名称"
            }
            
        }else if deviceCategory == DeviceModel.FamilySystem.HomeGateway {
            //家用网关
            if type == DeviceInforType.code{
                //编码
                return "设备编码"
            }else if type == DeviceInforType.name{
                //名称
                return "设备名称"
            }
            
        }else if deviceCategory == DeviceModel.SmartElectricitySystem.SmartPowerGateway{
            //智慧用电网关
            if type == DeviceInforType.name{
                //名称
                return "设备名称"
            }
        }else if deviceCategory == DeviceModel.SmartElectricitySystem.ElectricGateway_NT8128A{
            //8128A
            if type == DeviceInforType.name{
                //名称
                return "设备名称"
            }
        }else if deviceCategory == DeviceModel.SmartElectricitySystem.ElectricGateway_NT8127A{
            //8127A
            if type == DeviceInforType.name{
                //名称
                return "设备名称"
            }
        }else if deviceCategory == DeviceModel.SmartElectricitySystem.ElectricGateway_NT8127C{
            //8127C
            if type == DeviceInforType.name{
                //名称
                return "设备名称"
            }
        }else if deviceCategory == DeviceModel.NBDevice.Probe{
            //NB设备
            if type == DeviceInforType.code{
                //编码
                return "设备编码"
            }else if type == DeviceInforType.name{
                //名称
                return "设备名称"
            }
        }else if deviceCategory == DeviceModel.GasDetector.HM710NVM || deviceCategory == DeviceModel.GasDetector.WS2CG{
            //海曼可燃气体探测器
            if type == DeviceInforType.name{
                //名称
                return "设备名称"
            }
        }else if deviceCategory == DeviceModel.WaterSystem.NT8160A{
            //有人泵房检测网关
            if type == DeviceInforType.code{
                //编码
                return "设备编码"
            }else if type == DeviceInforType.name{
                //名称
                return "设备名称"
            }
        }else if deviceCategory == DeviceModel.WaterSystem.NT8160A_device{
            //有人泵房检测网关 下属器件
            if type == DeviceInforType.code{
                //编码
                return "设备编码"
            }else if type == DeviceInforType.name{
                //名称
                return "设备名称"
            }else if type == DeviceInforType.usrDeviceType{
                //信号类型
                return "设备类型"
            }
        }
        else{
            // 默认展示内容
            if type == DeviceInforType.code{
                //编码
                return "设备编码"
            }else if type == DeviceInforType.name{
                //名称
                return "设备名称"
            }
            
        }
        
        if type == DeviceInforType.address{
            //安装位置
            return "安装位置"
        }else if type == DeviceInforType.message_protocol{
            //通信协议
            return "报文协议"
        }else if type == DeviceInforType.manufacturer{
            //厂商
            return "生产厂商"
        }else if type == DeviceInforType.domain{
            //中心
            return "所属中心"
        }else if type == DeviceInforType.enterprise{
            //企业
            return "所属企业"
        }else if type == DeviceInforType.building{
            //建筑
            return "所属建筑"
        }else if type == DeviceInforType.keypart{
            //部位
            return "所属部位"
        }else if type == DeviceInforType.gis{
            //地图描点
            return "GIS定位"
        }else if type == DeviceInforType.unit{
            //测量单位
            return "单位"
        }else if type == DeviceInforType.connection_type{
            //通讯方式
            return "联网方式"
        }else if type == DeviceInforType.true_mean_type{
            //真(1)值含义
            return "真(1)值含义"
        }else if type == DeviceInforType.false_mean_type{
            //假(0)值含义
            return "假(0)值含义"
        }else if type == DeviceInforType.threshold_lower_limit{
            //阈值下限
            return "阈值下限"
        }else if type == DeviceInforType.threshold_upper_limit{
            //阈值上限
            return "阈值上限"
        }else if type == DeviceInforType.lower_limit_warning{
            //下限报警值
            return "下限报警值"
        }else if type == DeviceInforType.lower_lower_limit_warning{
            //下限报警值
            return "下下限报警值"
        }else if type == DeviceInforType.upper_limit_warning{
            //上限报警值
            return "上限报警值"
        }else if type == DeviceInforType.upper_upper_limit_warning{
            //上上限报警值
            return "上上限报警值"
        }else if type == DeviceInforType.principal{
            //负责人
            return "负责人"
        }else if type == DeviceInforType.contact_number{
            //联系电话
            return "联系电话"
        }else if type == DeviceInforType.ratio_coefficient{
            //变比系数
            return "变比系数"
        }else if type == DeviceInforType.imei{
            //IMEI号
            return "IMEI"
        }else if type == DeviceInforType.imsi{
            //IMSI号
            return "IMSI"
        }
        
        return ""
    }
    
    
    /// 根据设备类型 返回input类型item的palceHolder
    /// - Parameters:
    ///   - deviceCategory: 设备类型
    ///   - type: 项目类型
    class private func getInputItemPlaceHolderByDeviceCategory(deviceCategory:String,type:String) -> String {
        
        if deviceCategory == DeviceModel.FireSystem.uitd_NT9009{
            //一体化传输装置
            if type == DeviceInforType.code{
                //编码
                return "扫描或输入设备编码"
            }else if type == DeviceInforType.name{
                //名称
                return "设备名称"
            }else if type == DeviceInforType.type{
                //类型
                return "设备类型"
            }
        }else if deviceCategory == DeviceModel.FireSystem.IntegratedTransmissionHost{
            //一体化主机
            if type == DeviceInforType.code{
                //编码
                return "扫描或输入主机编码"
            }else if type == DeviceInforType.name{
                //名称
                return "主机名称"
            }else if type == DeviceInforType.type{
                //类型
                return "主机类别"
            }
        }else if deviceCategory == DeviceModel.WaterSystem.wireless_NT8327{
            //neat水网关
            if type == DeviceInforType.code{
                //编码
                return "扫描或输入设备编码"
            }else if type == DeviceInforType.name{
                //名称
                return "设备名称"
            }else if type == DeviceInforType.type{
                //类型
                return "设备类型"
            }
        }else if deviceCategory == DeviceModel.WaterSystem.NeatWaterSingal{
            //neat水信号
            if type == DeviceInforType.code{
                //编码
                return "00.01.01.00"
            }else if type == DeviceInforType.name{
                //名称
                return "信号名称"
            }else if type == DeviceInforType.type{
                //类型
                return "设备类型"
            }
        }else if deviceCategory == DeviceModel.SmartElectricitySystem.SmartPowerGateway{
            //智慧用电网关
            if type == DeviceInforType.name{
                //名称
                return "设备名称"
            }
        }else if deviceCategory == DeviceModel.NBDevice.Probe{
            //NB设备
            if type == DeviceInforType.code{
                //编码
                return "扫描或输入设备编码"
            }else if type == DeviceInforType.name{
                //名称
                return "设备名称"
            }
        }else if deviceCategory == DeviceModel.SmartElectricitySystem.ElectricGateway_NT8128A{
            //8128A
            if type == DeviceInforType.code{
                //名称
                return "扫描或输入设备编码"
            }
        }else if deviceCategory == DeviceModel.SmartElectricitySystem.ElectricGateway_NT8127A{
            //8127A
            if type == DeviceInforType.code{
                //名称
                return "扫描或输入设备编码"
            }
        }else if deviceCategory == DeviceModel.WaterSystem.Wgw_NT8164B {
            //万讯 液位
            if type == DeviceInforType.code{
                //编码
                return "扫描或输入设备编码"
            }
            
        }else if deviceCategory == DeviceModel.WaterSystem.Wgw_NT8161B {
            //万讯 液压
            if type == DeviceInforType.code{
                //编码
                return "扫描或输入设备编码"
            }
            
        }else if deviceCategory == DeviceModel.WaterSystem.Wgw_NT8167A {
            //拓普索尔 消火检测终端
            if type == DeviceInforType.code{
                //编码
                return "扫描或输入设备编码"
            }
            
        }else if deviceCategory == DeviceModel.WaterSystem.Wgw_NT8167C {
            //铭控 消防终端检测
            if type == DeviceInforType.code{
                //设备ID
                return "扫描或输入设备ID"
            }
            
        }else if deviceCategory == DeviceModel.WaterSystem.Wgw_NT8164A {
            //拓普索尔 液压
            if type == DeviceInforType.code{
                //编码
                return "扫描或输入设备编码"
            }
            
        }else if deviceCategory == DeviceModel.WaterSystem.Wgw_NT8161A {
            //拓普索尔 液位
            if type == DeviceInforType.code{
                //编码
                return "扫描或输入设备编码"
            }
            
        }else if deviceCategory == DeviceModel.FamilySystem.HomeGateway{
            //8140
            if type == DeviceInforType.code{
                //编码
                return "扫描或输入设备编码"
            }
        }else if deviceCategory == DeviceModel.GasDetector.HM710NVM || deviceCategory == DeviceModel.GasDetector.WS2CG{
            //海曼可燃气体
            if type == DeviceInforType.code{
                //编码
                return "扫描或输入设备编码"
            }
        }else if deviceCategory == DeviceModel.WaterSystem.NT8160A{
            
            if type == DeviceInforType.code{
                //编码
                return "扫描或输入设备编码"
            }else if type == DeviceInforType.name{
                //名称
                return "设备名称"
            }
            
        }else if deviceCategory == DeviceModel.WaterSystem.NT8160A_device{
            
            if type == DeviceInforType.code{
                //编码
                return "请选择设备编码"
            }else if type == DeviceInforType.name{
                //名称
                return "设备名称"
            }else if type == DeviceInforType.type{
                //类型
                return "设备类型"
            }
            
        }
        
        if type == DeviceInforType.address{
            //安装位置
            return "安装位置"
        }else if type == DeviceInforType.message_protocol{
            //通信协议
            return "报文协议"
        }else if type == DeviceInforType.manufacturer{
            //厂商
            return "生产厂商"
        }else if type == DeviceInforType.domain{
            //中心
            return "所属中心"
        }else if type == DeviceInforType.enterprise{
            //企业
            return "所属企业"
        }else if type == DeviceInforType.building{
            //建筑
            return "所属建筑"
        }else if type == DeviceInforType.keypart{
            //部位
            return "所属部位"
        }else if type == DeviceInforType.gis{
            //地图描点
            return "GIS定位"
        }else if type == DeviceInforType.unit{
            //测量单位
            return "单位"
        }else if type == DeviceInforType.signal_type{
            //信号类型
            return "信号类型"
        }else if type == DeviceInforType.connection_type{
            //通讯方式
            return "联网方式"
        }else if type == DeviceInforType.true_mean_type{
            //真(1)值含义
            return "真(1)值含义"
        }else if type == DeviceInforType.false_mean_type{
            //假(0)值含义
            return "假(0)值含义"
        }else if type == DeviceInforType.threshold_lower_limit{
            //阈值下限
            return "阈值下限"
        }else if type == DeviceInforType.threshold_upper_limit{
            //阈值上限
            return "阈值上限"
        }else if type == DeviceInforType.lower_limit_warning{
            //下限报警值
            return "下限报警值"
        }else if type == DeviceInforType.lower_lower_limit_warning{
            //下限报警值
            return "下下限报警值"
        }else if type == DeviceInforType.upper_limit_warning{
            //上限报警值
            return "上限报警值"
        }else if type == DeviceInforType.upper_upper_limit_warning{
            //上上限报警值
            return "上上限报警值"
        }else if type == DeviceInforType.principal{
            //负责人
            return "负责人"
        }else if type == DeviceInforType.contact_number{
            //联系电话
            return "联系电话"
        }else if type == DeviceInforType.ratio_coefficient{
            //变比系数
            return "变比系数"
        }else if type == DeviceInforType.imei{
            //IMEI号
            return "IMEI"
        }else if type == DeviceInforType.imsi{
            //IMSI号
            return "IMSI"
        }
        
        return ""
    }
    
    /// 获取item 编辑样式
    /// - Parameters:
    ///   - deviceCategory: 设备model
    ///   - type: 编辑项
    /// - Returns: 样式
    class func getItemEditTypeByDeviceCategory(deviceCategory:String,type:String) -> ItemEditStyle {
        
        if type == DeviceInforType.code {
            if deviceCategory == DeviceModel.WaterSystem.NT8160A_device {
                return .SelectStyle
            }else{
                return .ChoseStyle
            }
        }else{
            return .InputStyle
        }
        
    }
    
    
    /// 更新数据源
    /// - Parameter model: 新的数据model
    class func updataWithModelWithInforType(model:EditItemModel) {
        
        if currentCategory == DeviceModel.WaterSystem.NeatWaterSingal {
            //水信号类型
            if model.inforType == DeviceInforType.signal_type {
                //如果是信号类型变更 变更对应信号类型数据源 如果是相同的数据项同时修改
                if model.inforText == "2" {
                    dataSource.removeAll()
                    
                    digitalDataSource = updateInfor(model: model, dataSource: digitalDataSource)
                    
                    dataSource.append(contentsOf: digitalDataSource)
                    
                }else if model.inforText == "1"{
                    dataSource.removeAll()
                    
                    analogDataSource = updateInfor(model: model, dataSource: analogDataSource)
                    
                    dataSource.append(contentsOf: analogDataSource)
                }
                
            }else{
                
                //普通数据变更
                for index in 0..<dataSource.count {
                    let dic = dataSource[index]
                    if dic.keys.contains(model.inforType){
                        
                        var newDic = dic
                        newDic.updateValue(model, forKey: model.inforType)
                        
                        dataSource[index] = newDic
                        
                        //如果设置了 水信号类型 同步变更 单位
                        if model.inforType == DeviceInforType.type {
                            
                            if model.uploadInfor == "1" {
                                //液压
                                dataSource = changeEditItemInforByType(inforType:DeviceInforType.unit,infor: "MPa",targetArr: dataSource)
                                analogDataSource = changeEditItemInforByType(inforType:DeviceInforType.unit,infor: "MPa",targetArr: analogDataSource)
                                digitalDataSource = changeEditItemInforByType(inforType:DeviceInforType.unit,infor: "MPa",targetArr: digitalDataSource)
                                
                            }else if model.uploadInfor == "2"{
                                //液位
                               dataSource = changeEditItemInforByType(inforType:DeviceInforType.unit,infor: "cm",targetArr: dataSource)
                                analogDataSource = changeEditItemInforByType(inforType:DeviceInforType.unit,infor: "cm",targetArr: analogDataSource)
                                digitalDataSource = changeEditItemInforByType(inforType:DeviceInforType.unit,infor: "cm",targetArr: digitalDataSource)
                            }
                            
                        }
                        
                        break
                    }
                }
                
                analogDataSource = updateInfor(model: model, dataSource: analogDataSource)
                
                digitalDataSource = updateInfor(model: model, dataSource: digitalDataSource)
                
            }
            
        }else{
            //普通数据变更
            dataSource = updateInfor(model: model, dataSource: dataSource)
        }
    }
    
    /// 更新数据项
    /// - Parameters:
    ///   - model: 更新数据项
    ///   - dataSource: 数据源
    class func updateInfor(model:EditItemModel,dataSource: Array<Dictionary<String, EditItemModel>>) -> Array<Dictionary<String, EditItemModel>>{
        
        var dataArr = dataSource
        
        for index in 0..<dataSource.count {
            let dic = dataSource[index]
            if dic.keys.contains(model.inforType){
                
                var newDic = dic
                newDic.updateValue(model, forKey: model.inforType)
                
                dataArr[index] = newDic
                break
            }
        }
        
        return dataArr
    }
    
    class func changeEditItemInforByType(inforType:String,infor:String,targetArr:Array<Dictionary<String, EditItemModel>>) -> Array<Dictionary<String, EditItemModel>>{
        
        var dataArr = targetArr
        
        for index in 0..<targetArr.count {
            
            let dic = targetArr[index]
            if dic.keys.contains(inforType){
                
                var newDic = dic
                let model = newDic[inforType]
                model?.uploadInfor = infor
                model?.inforText = infor
                newDic.updateValue(model!, forKey: inforType)
                
                dataArr[index] = newDic
            }
            
        }
        
        return dataArr
        
    }
    
    /// 通过展示数据数组 获取上送数据model
    class func getUploadInforModel(success:@escaping (_ response:DeviceAddResponse)->Void,failed:@escaping (_ failed:BaseErrorResponse)->Void){
        
        if currentCategory == DeviceModel.FireSystem.uitd_NT9009{
            //一体化传输装置
            let model = UITDHostModel.init().formateHostDataToModel(dataArr: dataSource)
            
            let checkResult = CheckDeviceInforModel.checkDataFormat(deviceCategory: currentCategory, inforModel: model)
            
            if !checkResult.isAccess {
            
                let errModel = BaseErrorResponse.init()
                errModel.errMessage = checkResult.message
                failed(errModel)
                
                return
            }
            
            NeatRequest.instance.addDevice(apiPath: UrlHost.FireUITD.addUITDAPI, jsonParame: model, success: { (response) in
                success(response)
            }) { (error) in
                failed(error)
            }

            
        }else if currentCategory == DeviceModel.FireSystem.IntegratedTransmissionHost{
            //一体化主机
            let model = UITDHostModel.init().formateHostDataToModel(dataArr: dataSource)
            
            model.deviceId = self.currentDeviceParentID
            model.enterpriseId = self.currentEnterpriseID
            
            let checkResult = CheckDeviceInforModel.checkDataFormat(deviceCategory: currentCategory, inforModel: model)
            
            if !checkResult.isAccess {
            
                let errModel = BaseErrorResponse.init()
                errModel.errMessage = checkResult.message
                failed(errModel)
                
                return
            }
            
            
            NeatRequest.instance.addDevice(apiPath: UrlHost.FireUITD.addUITDDeviceAPI, jsonParame: model, success: { (response) in
                success(response)
            }) { (error) in
                failed(error)
            }
            
        }else if currentCategory == DeviceModel.WaterSystem.wireless_NT8327{
            //neat水网关
            let model = WaterGatewayModel.init().formateWaterGatewayDataToModel(dataArr: dataSource)
            //暂时写死 以后会陆续添加厂商
            model.manufacturer = "4000"
            
            let checkResult = CheckDeviceInforModel.checkDataFormat(deviceCategory: currentCategory, inforModel: model)
            
            if !checkResult.isAccess {
            
                let errModel = BaseErrorResponse.init()
                errModel.errMessage = checkResult.message
                failed(errModel)
                
                return
            }
            
            NeatRequest.instance.addDevice(apiPath: UrlHost.WaterDevice.addWaterGatewayAPI, jsonParame: model, success: { (response) in
                success(response)
            }) { (error) in
                failed(error)
            }

        }else if currentCategory == DeviceModel.WaterSystem.NeatWaterSingal{
            //neat水信号
            let model = WaterSignalModel.init().formateWaterSignalDataToModel(dataArr: dataSource)
            
            model.deviceId = self.currentDeviceParentID
            model.enterpriseId = self.currentEnterpriseID
            
            let checkResult = CheckDeviceInforModel.checkDataFormat(deviceCategory: currentCategory, inforModel: model)
            
            if !checkResult.isAccess {
            
                let errModel = BaseErrorResponse.init()
                errModel.errMessage = checkResult.message
                failed(errModel)
                
                return
            }
            
            NeatRequest.instance.addDevice(apiPath: UrlHost.WaterDevice.addWaterSignalAPI, jsonParame: model, success: { (response) in
                success(response)
            }) { (error) in
                failed(error)
            }
            
        }else if currentCategory == DeviceModel.SmartElectricitySystem.SmartPowerGateway{
            //智慧用电网关
            let model = SmartEleModel.init().formateSmartEleToModel(dataArr: dataSource)
            model.id = self.currentDeviceParentID
            
            let checkResult = CheckDeviceInforModel.checkDataFormat(deviceCategory: currentCategory, inforModel: model)
            
            if !checkResult.isAccess {
            
                let errModel = BaseErrorResponse.init()
                errModel.errMessage = checkResult.message
                failed(errModel)
                
                return
            }
            
            NeatRequest.instance.addDevice(apiPath: UrlHost.EleDevice.bindElectricGatewayAPI, jsonParame: model, success: { (response) in
                success(response)
            }) { (error) in
                failed(error)
            }
            
        }else if currentCategory == DeviceModel.NBDevice.Probe{
            //NB设备
            let model = NBDeviceModel.init().formateNBDeviceToModel(dataArr: dataSource)
            
            let checkResult = CheckDeviceInforModel.checkDataFormat(deviceCategory: currentCategory, inforModel: model)
            
            if !checkResult.isAccess {
            
                let errModel = BaseErrorResponse.init()
                errModel.errMessage = checkResult.message
                failed(errModel)
                
                return
            }
            
            NeatRequest.instance.addDevice(apiPath: UrlHost.NBDevice.addNBDeviceAPI, jsonParame: model, success: { (response) in
                success(response)
            }) { (error) in
                failed(error)
            }
        }else if currentCategory == DeviceModel.SmartElectricitySystem.ElectricGateway_NT8127A {
            
            let model = ElectricGatewayModel.init().formateElectricGatewayToModel(dataArr: dataSource)
            model.model = "NT8127A"
            model.manufacturer = "neat"
            let checkResult = CheckDeviceInforModel.checkDataFormat(deviceCategory: currentCategory, inforModel: model)
            
            if !checkResult.isAccess {
            
                let errModel = BaseErrorResponse.init()
                errModel.errMessage = checkResult.message
                failed(errModel)
                
                return
            }
            NeatRequest.instance.addDevice(apiPath: UrlHost.EleDevice.addIntegratedElectricGatewayListApi, jsonParame: model, success: { (response) in
                success(response)
            }) { (error) in
                failed(error)
            }
            
        }else if currentCategory == DeviceModel.SmartElectricitySystem.ElectricGateway_NT8127C {
            
            let model = ElectricGatewayModel.init().formateElectricGatewayToModel(dataArr: dataSource)
            model.model = "NT8127C"
            model.manufacturer = "neat"
            let checkResult = CheckDeviceInforModel.checkDataFormat(deviceCategory: currentCategory, inforModel: model)
            
            if !checkResult.isAccess {
            
                let errModel = BaseErrorResponse.init()
                errModel.errMessage = checkResult.message
                failed(errModel)
                
                return
            }
            NeatRequest.instance.addDevice(apiPath: UrlHost.EleDevice.addIntegratedElectricGatewayListApi, jsonParame: model, success: { (response) in
                success(response)
            }) { (error) in
                failed(error)
            }
            
        }else if currentCategory == DeviceModel.SmartElectricitySystem.ElectricGateway_NT8128A{
            
            let model = ElectricGatewayModel.init().formateElectricGatewayToModel(dataArr: dataSource)
            model.model = "NT8128A"
            model.manufacturer = "neat"
            let checkResult = CheckDeviceInforModel.checkDataFormat(deviceCategory: currentCategory, inforModel: model)
            
            if !checkResult.isAccess {
            
                let errModel = BaseErrorResponse.init()
                errModel.errMessage = checkResult.message
                failed(errModel)
                
                return
            }
            NeatRequest.instance.addDevice(apiPath: UrlHost.EleDevice.addIntegratedElectricGatewayListApi, jsonParame: model, success: { (response) in
                success(response)
            }) { (error) in
                failed(error)
            }
            
        }else if currentCategory == DeviceModel.WaterSystem.Wgw_NT8161A ||
            currentCategory == DeviceModel.WaterSystem.Wgw_NT8164A ||
            currentCategory == DeviceModel.WaterSystem.Wgw_NT8167A {
            
            let model = TPSEWaterSourceMonModel.init().formateWaterSourceMonToModel(dataArr: dataSource)
            if currentCategory == DeviceModel.WaterSystem.Wgw_NT8161A {
                model.model = "NT8161A"
            }else if currentCategory == DeviceModel.WaterSystem.Wgw_NT8164A {
                model.model = "NT8164A"
            }else if currentCategory == DeviceModel.WaterSystem.Wgw_NT8167A {
                model.model = "NT8167A"
            }
            
            let checkResult = CheckDeviceInforModel.checkDataFormat(deviceCategory: currentCategory, inforModel: model)
            
            if !checkResult.isAccess {
            
                let errModel = BaseErrorResponse.init()
                errModel.errMessage = checkResult.message
                failed(errModel)
                
                return
            }
            
            NeatRequest.instance.addDevice(apiPath: UrlHost.WaterDevice.addWaterSourceAPI, jsonParame: model, success: { (response) in
                success(response)
            }) { (error) in
                failed(error)
            }
            
        }else if currentCategory == DeviceModel.WaterSystem.Wgw_NT8167C {
            
            let model = MKAddModel.init().formateMkToModel(dataArr: dataSource)
            
            model.model = "NT8167C"
            
            let checkResult = CheckDeviceInforModel.checkDataFormat(deviceCategory: currentCategory, inforModel: model)
            
            if !checkResult.isAccess {
            
                let errModel = BaseErrorResponse.init()
                errModel.errMessage = checkResult.message
                failed(errModel)
                
                return
            }
            
            NeatRequest.instance.addDevice(apiPath: UrlHost.WaterDevice.addWaterSourceAPI, jsonParame: model, success: { (response) in
                success(response)
            }) { (error) in
                failed(error)
            }
            
            
        }else if currentCategory == DeviceModel.WaterSystem.Wgw_NT8161B ||
            currentCategory == DeviceModel.WaterSystem.Wgw_NT8164B  {
            
            let model = WXWaterSourceModel.init().formateWXWaterSourceToModel(dataArr: dataSource)
            if currentCategory == DeviceModel.WaterSystem.Wgw_NT8161B {
                model.model = "NT8161B"
            }else if currentCategory == DeviceModel.WaterSystem.Wgw_NT8164B {
                model.model = "NT8164B"
            }
            
            let checkResult = CheckDeviceInforModel.checkDataFormat(deviceCategory: currentCategory, inforModel: model)
            
            if !checkResult.isAccess {
            
                let errModel = BaseErrorResponse.init()
                errModel.errMessage = checkResult.message
                failed(errModel)
                
                return
            }
            
            NeatRequest.instance.addDevice(apiPath: UrlHost.WaterDevice.addWaterSourceAPI, jsonParame: model, success: { (response) in
                success(response)
            }) { (error) in
                failed(error)
            }
            
        }else if currentCategory == DeviceModel.FamilySystem.HomeGateway{
            
            let model = HomeGatewayAddModel.init().formateElectricGatewayToModel(dataArr:dataSource)
            
            let checkResult = CheckDeviceInforModel.checkDataFormat(deviceCategory: currentCategory, inforModel: model)
            
            if !checkResult.isAccess {
            
                let errModel = BaseErrorResponse.init()
                errModel.errMessage = checkResult.message
                failed(errModel)
                
                return
            }
            
            NeatRequest.instance.addDevice(apiPath: UrlHost.HomeGateway.addHomeGatewayAPI, jsonParame: model, success: { (response) in
                success(response)
            }) { (error) in
                failed(error)
            }
            
            
        }else if currentCategory == DeviceModel.GasDetector.WS2CG ||
            currentCategory == DeviceModel.GasDetector.HM710NVM  {
            
            let model = GasDetectorAddModel.init().formateGasDetectorToModel(dataArr: dataSource)
            if currentCategory == DeviceModel.GasDetector.WS2CG {
                model.model = "WS2CGNB"
            }else if currentCategory == DeviceModel.GasDetector.HM710NVM {
                model.model = "HM710NVMNB"
            }
            
            let checkResult = CheckDeviceInforModel.checkDataFormat(deviceCategory: currentCategory, inforModel: model)
            
            if !checkResult.isAccess {
            
                let errModel = BaseErrorResponse.init()
                errModel.errMessage = checkResult.message
                failed(errModel)
                
                return
            }
            
            NeatRequest.instance.addDevice(apiPath: UrlHost.GasDetector.addGasDetectorAPI, jsonParame: model, success: { (response) in
                success(response)
            }) { (error) in
                failed(error)
            }
            
        }else if currentCategory == DeviceModel.WaterSystem.NT8160A{
            //有人 泵房检测 网关
            
            let model = YSGatewayAddModel.init().formateYSGatewayToModel(dataArr: dataSource)
    
            
            let checkResult = CheckDeviceInforModel.checkDataFormat(deviceCategory: currentCategory, inforModel: model)
            
            if !checkResult.isAccess {
            
                let errModel = BaseErrorResponse.init()
                errModel.errMessage = checkResult.message
                failed(errModel)
                
                return
            }
            
            let addModel = PublicSupperDeviceAddModel.init()
            
            addModel.model = currentCategory
            addModel.isDeviceLevel = DeviceLevel.Gateway
            addModel.deviceInfor = model
            
            NeatRequest.instance.postAddDevicePublic(jsonParame: addModel) { (response) in
                
                success(response)
                
            } failed: { (error) in
                
                failed(error)
                
            }

            
        }else if currentCategory == DeviceModel.WaterSystem.NT8160A_device{
            //有人 泵房检测 网关 下属器件
            
            let model = YSDeviceAddModel.init().formateYSDeviceToModel(dataArr: dataSource)
            model.model = currentCategory
            model.gatewayId = currentDeviceParentID
            
            let checkResult = CheckDeviceInforModel.checkDataFormat(deviceCategory: currentCategory, inforModel: model)
            
            if !checkResult.isAccess {
            
                let errModel = BaseErrorResponse.init()
                errModel.errMessage = checkResult.message
                failed(errModel)
                
                return
            }
            
            let addModel = PublicSupperDeviceAddModel.init()
            
            addModel.model = currentCategory
            addModel.isDeviceLevel = DeviceLevel.Device
            addModel.deviceInfor = model
            
            NeatRequest.instance.postAddDevicePublic(jsonParame: addModel) { (response) in
                
                success(response)
                
            } failed: { (error) in
                
                failed(error)
                
            }
            
        }
    }
    

}
