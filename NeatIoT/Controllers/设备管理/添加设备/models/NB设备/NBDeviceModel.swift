//
//  NBDeviceModel.swift
//  NeatIoT
//
//  Created by neat on 2019/11/25.
//  Copyright © 2019 Neat. All rights reserved.
//

import UIKit

class NBDeviceModel: AddDeviceBaseModel {
    
    /// 根据给定数据数组 返回对应数据对象
    /// - Parameter dataArr: 上送数据对象
    func formateNBDeviceToModel(dataArr:Array<Dictionary<String,EditItemModel>>) -> NBDeviceModel {
    
        let model = NBDeviceModel()
        
        for dic in dataArr {
            
            var editModel = EditItemModel()
            if dic.keys.contains(DeviceInforType.code) {
                //设备编码
                editModel = dic[DeviceInforType.code]!
                model.code = editModel.uploadInfor
                
            }else if dic.keys.contains(DeviceInforType.name){
                //设备名称
                editModel = dic[DeviceInforType.name]!
                model.name = editModel.uploadInfor
                
            }else if dic.keys.contains(DeviceInforType.domain){
                //中心
                editModel = dic[DeviceInforType.domain]!
                model.domainId = editModel.uploadInfor
                
            }else if dic.keys.contains(DeviceInforType.enterprise){
                //企业
                editModel = dic[DeviceInforType.enterprise]!
                model.enterpriseId = editModel.uploadInfor
                
            }else if dic.keys.contains(DeviceInforType.building){
                //建筑
                editModel = dic[DeviceInforType.building]!
                model.buildingId = editModel.uploadInfor
                
            }else if dic.keys.contains(DeviceInforType.keypart){
                //部位
                editModel = dic[DeviceInforType.keypart]!
                model.keypartId = editModel.uploadInfor
                
            }else if dic.keys.contains(DeviceInforType.address){
                //安装位置
                editModel = dic[DeviceInforType.address]!
                model.address = editModel.uploadInfor
                
            }else if dic.keys.contains(DeviceInforType.gis){
                //gis
                editModel = dic[DeviceInforType.gis]!
                model.latitude = editModel.latitude
                model.longitude = editModel.longitude
                model.gisaddress = editModel.uploadInfor
                    
            }
            
        }
        
        return model
    }
    

}
