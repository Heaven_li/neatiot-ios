//
//  AddDeviceBaseModel.swift
//  NeatIoT
//
//  Created by neat on 2019/11/13.
//  Copyright © 2019 Neat. All rights reserved.
//

import UIKit
import HandyJSON

/// 添加设备父级model
class AddDeviceBaseModel: NSObject, HandyJSON,Encodable{
    
    /// 设备名称
    var name:String? = nil
    /// 设备码
    var code:String? = nil
    /// 设备安装位置
    var address:String? = nil
    
    /// 设备类型
    var deviceType:String? = nil
    /// 设备型号
    var model:String? = nil
    /// 报文协议
    var messageProtocol:String? = nil
    /// 厂商
    var manufacturer:String? = nil
    /// 中心id
    var domainId:String? = nil
    /// 企业id
    var enterpriseId:String? = nil
    /// 建筑id
    var buildingId:String? = nil
    /// 部位id
    var keypartId:String? = nil
    
    /// 所属主机id (水信号 及 火器件使用)
    var deviceId:String? = nil
    /// 统一的 器件添加时 父设备ID 
    var gatewayId:String? = nil

    
    /// Neat水公共属性
    
    /// 信号类型 true 模拟量 false 数字量
    var isYC:Bool?
    
    ///水信号模拟量
    /// 计量单位
    var unit:String?
    /// 阈值上限
    var maximum:String?
    /// 阈值下限
    var minimum:String?
    /// 上限报警值
    var highLimit1:String?
    /// 上限报警值 是否报警
    var enableHighLimit1Alarm:Bool?
    /// 上上限报警值
    var highLimit2:String?
    /// 上上限报警值 是否报警
    var enableHighLimit2Alarm:Bool?
    /// 下限报警值
    var lowLimit1:String?
    /// 下限报警值 是否报警
    var enableLowLimit1Alarm:Bool?
    /// 下下限报警值
    var lowLimit2:String?
    /// 下下限报警值 是否报警
    var enableLowLimit2Alarm:Bool?
    
    ///水信号数字量
    /// 真值含义
    var trueMeaning:String?
    /// 真值 是否报警
    var enableTureAlarm:Bool?
    /// 假值含义
    var falseMeaning:String?
    /// 假值 是否报警
    var enableFalseAlarm:Bool?
    
    
    /// 一体式水
    /// 联网方式
    var connectionType:String?
    
    
    /// NB设备
    
    /// GIS详细地址
    var gisaddress:String?
    /// NB设备所在 纬度
    var latitude:String?
    /// NB设备所在 经度
    var longitude:String?
    
    /// 智慧用电
    /// 网关id
    var id:String?
    /// 负责人
    var owner:String?
    /// 联系电话
    var phone:String?
    /// 变比系数
    var ratio:String?
    
    /// IMEI号
    var imei:String?
    /// IMSI号
    var imsi:String?

    
    required override init() {
        
    }
    
    

}
