//
//  EditItemModel.swift
//  NeatIoT
//
//  Created by neat on 2019/11/13.
//  Copyright © 2019 Neat. All rights reserved.
//

import UIKit

/// 编辑方式枚举
enum ItemEditStyle {
    
    case InputStyle     ///输入
    case SelectStyle    ///选择
    case GISStyle       ///地图
    case ChoseStyle     ///选项弹出框
    case RadioBtnStyle  ///复选框
}

/// 添加设备时编辑项model
class EditItemModel: NSObject {
    
    /// 标题
    var titleText:String = ""
    /// 内容
    var inforText:String = ""
    /// 占位文字
    var holderText:String = ""
    /// 编辑类型
    var editType:ItemEditStyle = .InputStyle
    /// 是否可编辑 ture可编辑 false不可编辑 默认true
    var isEditAble:Bool = true
    /// 数据项类型 见DeviceInforType类
    var inforType:String = ""
    /// 上传服务器数据
    var uploadInfor:String = ""
    /// neat水信号时是否报警字段
    var uploadIsWarning:Bool = false
    /// 左侧radio按钮 title
    var leftRadioTitle:String = ""
    /// 右侧radio按钮 title
    var rightRadioTitle:String = ""
    /// 键盘样式
    var keyboardType:UIKeyboardType = .default
    /// NB设备所在 纬度
    var latitude:String?
    /// NB设备所在 经度
    var longitude:String?
    /// 是否在编辑状态
    var isEditState:Bool = false

    

}
