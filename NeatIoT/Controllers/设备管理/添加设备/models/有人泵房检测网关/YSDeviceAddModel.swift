//
//  YSDeviceAddModel.swift
//  NeatIoT
//
//  Created by Yang Blus on 2020/10/27.
//  Copyright © 2020 Neat. All rights reserved.
//

import UIKit

class YSDeviceAddModel: AddDeviceBaseModel {
    
    /// 根据给定数据数组 返回对应数据对象
    /// - Parameter dataArr: 上送数据对象
    func formateYSDeviceToModel(dataArr:Array<Dictionary<String,EditItemModel>>) -> YSDeviceAddModel {
    
        let model = YSDeviceAddModel()
        
        
        
        for dic in dataArr {
            
            var editModel = EditItemModel()
            if dic.keys.contains(DeviceInforType.code) {
                //设备编码
                editModel = dic[DeviceInforType.code]!
                model.code = editModel.uploadInfor
                
            }else if dic.keys.contains(DeviceInforType.name){
                //设备名称
                editModel = dic[DeviceInforType.name]!
                model.name = editModel.uploadInfor
                
            }else if dic.keys.contains(DeviceInforType.usrDeviceType){
                //信号类型
                editModel = dic[DeviceInforType.usrDeviceType]!
                if editModel.uploadInfor == "1"{
                    model.deviceType = "1"
                }else{
                    model.deviceType = "2"
                }
                
            }else if dic.keys.contains(DeviceInforType.building){
                //建筑
                editModel = dic[DeviceInforType.building]!
                model.buildingId = editModel.uploadInfor
                
            }else if dic.keys.contains(DeviceInforType.keypart){
                //部位
                editModel = dic[DeviceInforType.keypart]!
                model.keypartId = editModel.uploadInfor
                
            }else if dic.keys.contains(DeviceInforType.address){
                //安装位置
                editModel = dic[DeviceInforType.address]!
                model.address = editModel.uploadInfor
                
            }
            
            
        
            
        }
        
        return model
    }

}
