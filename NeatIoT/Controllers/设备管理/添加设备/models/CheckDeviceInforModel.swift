//
//  CheckDeviceInforModel.swift
//  NeatIoT
//
//  Created by Yang Blus on 2020/3/21.
//  Copyright © 2020 Neat. All rights reserved.
//

import UIKit

class CheckDeviceInforModel: NSObject {
    
    
    class func checkDataFormat(deviceCategory:String,inforModel:AddDeviceBaseModel) -> (isAccess:Bool,message:String){
        
        if deviceCategory == DeviceModel.FireSystem.uitd_NT9009{
            
            //一体化传输装置
            if (inforModel.code == nil || inforModel.code?.isEmpty ?? false){
                
                return (false,"设备编码不能为空!")
                
            }else if (inforModel.name == nil || inforModel.name?.isEmpty ?? false){
                
                return (false,"设备名称不能为空!")
                
            }else if (inforModel.deviceType == nil || inforModel.deviceType?.isEmpty ?? false){
                
                return (false,"您尚未选择设备类型！")
                
            }else if (inforModel.messageProtocol == nil || inforModel.messageProtocol?.isEmpty ?? false){
                
                return (false,"您尚未选择报文协议！")
                
            }else if (inforModel.manufacturer == nil || inforModel.manufacturer?.isEmpty ?? false){
                
                return (false,"您尚未选择生产厂商！")
                
            }else if (inforModel.domainId == nil || inforModel.domainId?.isEmpty ?? false){
                
                return (false,"您尚未选择所属中心！")
                
            }else if (inforModel.enterpriseId == nil || inforModel.enterpriseId?.isEmpty ?? false){
                
                return (false,"您尚未选择所属单位！")
                
            }else if (inforModel.address == nil || inforModel.address?.isEmpty ?? false){
                
                return (false,"安装位置不能为空!")
                
            }
            
        } else if deviceCategory == DeviceModel.FireSystem.IntegratedTransmissionHost{
            
            //一体化主机
            if (inforModel.code == nil || inforModel.code?.isEmpty ?? false){
                
                return (false,"主机编码不能为空!")
                
            }else if (inforModel.name == nil || inforModel.name?.isEmpty ?? false){
                
                return (false,"主机名称不能为空!")
                
            }else if (inforModel.deviceType == nil || inforModel.deviceType?.isEmpty ?? false){
                
                return (false,"您尚未选择主机类别！")
                
            }else if (inforModel.manufacturer == nil || inforModel.manufacturer?.isEmpty ?? false){
                
                return (false,"您尚未选择生产厂商!")
                
            }else if (inforModel.address == nil || inforModel.address?.isEmpty ?? false){
                
                return (false,"安装位置不能为空！")
                
            }
                
        }else if deviceCategory == DeviceModel.WaterSystem.wireless_NT8327{
            
            //neat水网关
            if (inforModel.code == nil || inforModel.code?.isEmpty ?? false){
                
                return (false,"设备编码不能为空!")
                
            }else if (inforModel.name == nil || inforModel.name?.isEmpty ?? false){
                
                return (false,"设备名称不能为空!")
                
            }else if (inforModel.domainId == nil || inforModel.domainId?.isEmpty ?? false){
                
                return (false,"您尚未选择所属中心！")
                
            }else if (inforModel.enterpriseId == nil || inforModel.enterpriseId?.isEmpty ?? false){
                
                return (false,"您尚未选择所属单位！")
                
            }else if (inforModel.address == nil || inforModel.address?.isEmpty ?? false){
                
                return (false,"安装位置不能为空!")
                
            }
            
        } else if deviceCategory == DeviceModel.WaterSystem.NeatWaterSingal{
            
            //neat水信号
            if (inforModel.code == nil || inforModel.code?.isEmpty ?? false){
                
                return (false,"水信号编码不能为空!")
                
            }else if (inforModel.name == nil || inforModel.name?.isEmpty ?? false){
                
                return (false,"水信号名称不能为空!")
                
            }else if (inforModel.address == nil || inforModel.address?.isEmpty ?? false){
                
                return (false,"安装位置不能为空!")
                
            }
            
            if inforModel.isYC ?? false {
                
                //模拟量
                if (inforModel.deviceType == nil || inforModel.deviceType?.isEmpty ?? false){
                    
                    return (false,"您尚未选中设备类型！")
                    
                }else if (inforModel.minimum == nil || inforModel.minimum?.isEmpty ?? false){
                    
                    return (false,"您尚未输入阈值下限！")
                    
                }else if (inforModel.maximum == nil || inforModel.maximum?.isEmpty ?? false){
                    
                    return (false,"您尚未输入阈值上限!")
                    
                }else if (inforModel.lowLimit1 == nil || inforModel.lowLimit1?.isEmpty ?? false){
                    
                    return (false,"您尚未输入下限报警值！")
                    
                }else if (inforModel.lowLimit2 == nil || inforModel.lowLimit2?.isEmpty ?? false){
                    
                    return (false,"您尚未输入下下限报警值!")
                    
                }else if (inforModel.highLimit1 == nil || inforModel.highLimit1?.isEmpty ?? false){
                    
                    return (false,"您尚未输入上限报警值！")
                    
                }else if (inforModel.highLimit2 == nil || inforModel.highLimit2?.isEmpty ?? false){
                    
                    return (false,"您尚未输入上上限报警值!")
                    
                }
                
            }else{
                
                //数字量
                if (inforModel.trueMeaning == nil || inforModel.trueMeaning?.isEmpty ?? false){
                    
                    return (false,"您尚未输入真(1)值含义！")
                    
                }else if (inforModel.falseMeaning == nil || inforModel.falseMeaning?.isEmpty ?? false){
                    
                    return (false,"您尚未输入假(0)值含义！")
                    
                }
                
            }
            
           
        }else if deviceCategory == DeviceModel.SmartElectricitySystem.SmartPowerGateway{
            //智慧用电网关
            if (inforModel.name == nil || inforModel.name?.isEmpty ?? false){
                
                return (false,"设备名称不能为空!")
                
            }else if (inforModel.domainId == nil || inforModel.domainId?.isEmpty ?? false){
                
                return (false,"您尚未选择所属中心！")
                
            }else if (inforModel.enterpriseId == nil || inforModel.enterpriseId?.isEmpty ?? false){
                
                return (false,"您尚未选择所属单位！")
                
            }else if (inforModel.address == nil || inforModel.address?.isEmpty ?? false){
                
                return (false,"安装位置不能为空!")
                
            }else if (inforModel.ratio == nil || inforModel.ratio?.isEmpty ?? false){
                
                return (false,"变比系数不能为空!")
                
            }
            if inforModel.ratio?.count ?? 0 < 5 {
                if !(Float(inforModel.ratio ?? "0") ?? 0 > Float(0) && Float(inforModel.ratio ?? "0") ?? 0 < Float(1200)) {
                    return (false,"变比系数不正确！")
                }
            }else{
                return (false,"变比系数不正确！")
            }
            
        }else if deviceCategory == DeviceModel.NBDevice.Probe || deviceCategory == DeviceModel.NBDevice.Probe {
            //NB设备
            if (inforModel.code == nil || inforModel.code?.isEmpty ?? false){
                
                return (false,"设备编码不能为空!")
                
            }else if (inforModel.name == nil || inforModel.name?.isEmpty ?? false){
                
                return (false,"设备名称不能为空!")
                
            }else if (inforModel.domainId == nil || inforModel.domainId?.isEmpty ?? false){
                
                return (false,"您尚未选择所属中心！")
                
            }else if (inforModel.enterpriseId == nil || inforModel.enterpriseId?.isEmpty ?? false){
                
                return (false,"您尚未选择所属单位！")
                
            }else if (inforModel.address == nil || inforModel.address?.isEmpty ?? false){
                
                return (false,"安装位置不能为空!")
                
            }else if (inforModel.gisaddress == nil || inforModel.gisaddress?.isEmpty ?? false){
                
                return (false,"您尚未为设备设置GIS位置!")
                
            }
        
        }else if deviceCategory == DeviceModel.SmartElectricitySystem.ElectricGateway_NT8127A ||
            deviceCategory == DeviceModel.SmartElectricitySystem.ElectricGateway_NT8128A ||
            deviceCategory == DeviceModel.SmartElectricitySystem.ElectricGateway_NT8127C {
            
            if (inforModel.imei == nil || inforModel.imei?.isEmpty ?? false){
                
                return (false,"IMEI号不能为空!")
                
            }else if (inforModel.name == nil || inforModel.name?.isEmpty ?? false){
                
                return (false,"设备名称不能为空!")
                
            }else if (inforModel.connectionType == nil || inforModel.connectionType?.isEmpty ?? false){
                
                return (false,"您尚未选择联网方式！")
                
            }else if (inforModel.domainId == nil || inforModel.domainId?.isEmpty ?? false){
                
                return (false,"您尚未选择所属中心！")
                
            }else if (inforModel.enterpriseId == nil || inforModel.enterpriseId?.isEmpty ?? false){
                
                return (false,"您尚未选择所属单位！")
                
            }else if (inforModel.address == nil || inforModel.address?.isEmpty ?? false){
                
                return (false,"安装位置不能为空!")
                
            }else if (inforModel.gisaddress == nil || inforModel.gisaddress?.isEmpty ?? false){
                
                return (false,"您尚未为设备设置GIS位置!")
                
            }
            
        }else if deviceCategory == DeviceModel.WaterSystem.Wgw_NT8161A ||
            deviceCategory == DeviceModel.WaterSystem.Wgw_NT8164A ||
            deviceCategory == DeviceModel.WaterSystem.Wgw_NT8167A {
        
            if (inforModel.code == nil || inforModel.code?.isEmpty ?? false){
                
                return (false,"设备编码不能为空!")
                
            }else if (inforModel.name == nil || inforModel.name?.isEmpty ?? false){
                
                return (false,"设备名称不能为空!")
                
            }else if (inforModel.domainId == nil || inforModel.domainId?.isEmpty ?? false){
                
                return (false,"您尚未选择所属中心！")
                
            }else if (inforModel.enterpriseId == nil || inforModel.enterpriseId?.isEmpty ?? false){
                
                return (false,"您尚未选择所属单位！")
                
            }else if (inforModel.address == nil || inforModel.address?.isEmpty ?? false){
                
                return (false,"安装位置不能为空!")
                
            }else if (inforModel.gisaddress == nil || inforModel.gisaddress?.isEmpty ?? false){
                
                return (false,"您尚未为设备设置GIS位置!")
                
            }
        
        }else if deviceCategory == DeviceModel.WaterSystem.Wgw_NT8167C {
        
            if (inforModel.code == nil || inforModel.code?.isEmpty ?? false){
                
                return (false,"设备ID不能为空!")
                
            }else if (inforModel.name == nil || inforModel.name?.isEmpty ?? false){
                
                return (false,"设备名称不能为空!")
                
            }else if (inforModel.imei == nil || inforModel.imei?.isEmpty ?? false){
                
                return (false,"imei号不能为空!")
                
            }else if (inforModel.domainId == nil || inforModel.domainId?.isEmpty ?? false){
                
                return (false,"您尚未选择所属中心！")
                
            }else if (inforModel.enterpriseId == nil || inforModel.enterpriseId?.isEmpty ?? false){
                
                return (false,"您尚未选择所属单位！")
                
            }else if (inforModel.address == nil || inforModel.address?.isEmpty ?? false){
                
                return (false,"安装位置不能为空!")
                
            }else if (inforModel.gisaddress == nil || inforModel.gisaddress?.isEmpty ?? false){
                
                return (false,"您尚未为设备设置GIS位置!")
                
            }
        
        }else if deviceCategory == DeviceModel.FamilySystem.HomeGateway {
            //8140
            if (inforModel.code == nil || inforModel.code?.isEmpty ?? false){
                
                return (false,"设备编码不能为空!")
                
            }else if (inforModel.name == nil || inforModel.name?.isEmpty ?? false){
                
                return (false,"设备名称不能为空!")
                
            }else if (inforModel.domainId == nil || inforModel.domainId?.isEmpty ?? false){
                
                return (false,"您尚未选择所属中心！")
                
            }else if (inforModel.enterpriseId == nil || inforModel.enterpriseId?.isEmpty ?? false){
                
                return (false,"您尚未选择所属单位！")
                
            }else if (inforModel.address == nil || inforModel.address?.isEmpty ?? false){
                
                return (false,"安装位置不能为空!")
                
            }else if (inforModel.gisaddress == nil || inforModel.gisaddress?.isEmpty ?? false){
                
                return (false,"您尚未为设备设置GIS位置!")
                
            }
        
        }else if deviceCategory == DeviceModel.GasDetector.HM710NVM || deviceCategory == DeviceModel.GasDetector.WS2CG {
            
            //海曼可燃气体探测器
            if (inforModel.imei == nil || inforModel.imei?.isEmpty ?? false){
                
                return (false,"IMEI号不能为空!")
                
            }else if (inforModel.name == nil || inforModel.name?.isEmpty ?? false){
                
                return (false,"设备名称不能为空!")
                
            }else if (inforModel.connectionType == nil || inforModel.connectionType?.isEmpty ?? false){
                
                return (false,"您尚未选择联网方式！")
                
            }else if (inforModel.domainId == nil || inforModel.domainId?.isEmpty ?? false){
                
                return (false,"您尚未选择所属中心！")
                
            }else if (inforModel.enterpriseId == nil || inforModel.enterpriseId?.isEmpty ?? false){
                
                return (false,"您尚未选择所属单位！")
                
            }else if (inforModel.address == nil || inforModel.address?.isEmpty ?? false){
                
                return (false,"安装位置不能为空!")
                
            }else if (inforModel.gisaddress == nil || inforModel.gisaddress?.isEmpty ?? false){
                
                return (false,"您尚未为设备设置GIS位置!")
                
            }
        
        }else if deviceCategory == DeviceModel.WaterSystem.NT8160A {
            
            //有人泵房检测网关
            if (inforModel.code == nil || inforModel.code?.isEmpty ?? false){
                
                return (false,"设备编码不能为空!")
                
            }
//            else if (inforModel.name == nil || inforModel.name?.isEmpty ?? false){
//                
//                return (false,"设备名称不能为空!")
//                
//            }
            else if (inforModel.domainId == nil || inforModel.domainId?.isEmpty ?? false){
                
                return (false,"您尚未选择所属中心！")
                
            }else if (inforModel.enterpriseId == nil || inforModel.enterpriseId?.isEmpty ?? false){
                
                return (false,"您尚未选择所属单位！")
                
            }else if (inforModel.address == nil || inforModel.address?.isEmpty ?? false){
                
                return (false,"安装位置不能为空!")
                
            }else if (inforModel.gisaddress == nil || inforModel.gisaddress?.isEmpty ?? false){
                
                return (false,"您尚未为设备设置GIS位置!")
                
            }
        
        }else if deviceCategory == DeviceModel.WaterSystem.NT8160A_device {
            
            //有人泵房检测网关 下属器件
            if (inforModel.code == nil || inforModel.code?.isEmpty ?? false){
                
                return (false,"设备编码不能为空!")
                
            }else if (inforModel.name == nil || inforModel.name?.isEmpty ?? false){
                
                return (false,"设备名称不能为空!")
                
            }else if (inforModel.address == nil || inforModel.address?.isEmpty ?? false){
                
                return (false,"安装位置不能为空!")
                
            }
        
        }

        return (true,"数据正常")
        
    }
        
    
}
