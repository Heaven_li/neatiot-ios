//
//  SmartEleModel.swift
//  NeatIoT
//
//  Created by neat on 2019/11/25.
//  Copyright © 2019 Neat. All rights reserved.
//

import UIKit

class SmartEleModel: AddDeviceBaseModel {
    
    /// 根据给定数据数组 返回对应数据对象
    /// - Parameter dataArr: 上送数据对象
    func formateSmartEleToModel(dataArr:Array<Dictionary<String,EditItemModel>>) -> SmartEleModel {
    
        let model = SmartEleModel()
        
        for dic in dataArr {
            
            var editModel = EditItemModel()
            if dic.keys.contains(DeviceInforType.name){
                //设备名称
                editModel = dic[DeviceInforType.name]!
                model.name = editModel.uploadInfor
                
            }else if dic.keys.contains(DeviceInforType.domain){
                //中心
                editModel = dic[DeviceInforType.domain]!
                model.domainId = editModel.uploadInfor
                
            }else if dic.keys.contains(DeviceInforType.enterprise){
                //企业
                editModel = dic[DeviceInforType.enterprise]!
                model.enterpriseId = editModel.uploadInfor
                
            }else if dic.keys.contains(DeviceInforType.building){
                //建筑
                editModel = dic[DeviceInforType.building]!
                model.buildingId = editModel.uploadInfor
                
            }else if dic.keys.contains(DeviceInforType.keypart){
                //部位
                editModel = dic[DeviceInforType.keypart]!
                model.keypartId = editModel.uploadInfor
                
            }else if dic.keys.contains(DeviceInforType.address){
                //安装位置
                editModel = dic[DeviceInforType.address]!
                model.address = editModel.uploadInfor
                
            }else if dic.keys.contains(DeviceInforType.principal){
                //负责人
                editModel = dic[DeviceInforType.principal]!
                model.owner = editModel.uploadInfor
                    
            }else if dic.keys.contains(DeviceInforType.contact_number){
                //联系电话
                editModel = dic[DeviceInforType.contact_number]!
                model.phone = editModel.uploadInfor
                    
            }else if dic.keys.contains(DeviceInforType.ratio_coefficient){
                //变比系数
                editModel = dic[DeviceInforType.ratio_coefficient]!
                model.ratio = editModel.uploadInfor
                    
            }
            
        }
        
        return model
    }

}
