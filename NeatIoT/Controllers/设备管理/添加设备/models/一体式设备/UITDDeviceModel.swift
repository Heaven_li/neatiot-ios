//
//  UITDDeviceModel.swift
//  NeatIoT
//
//  Created by neat on 2019/11/13.
//  Copyright © 2019 Neat. All rights reserved.
//

import UIKit

/// 一体式火器件
class UITDDeviceModel: AddDeviceBaseModel {
    
//    /// 设备类型
//    var deviceType:String = ""
//    /// 厂商
//    var manufacturer:String = ""
//    /// 所属主机id
//    var deviceId:String = ""
//    /// 企业id
//    var enterpriseId:String = ""
//    /// 建筑id
//    var buildingId:String = ""
//    /// 部位id
//    var keypartId:String = ""
    
    /// 根据给定数据数组 返回对应数据对象
    /// - Parameter dataArr: 上送数据对象
    func formateDeviceDataToModel(dataArr:Array<Dictionary<String,EditItemModel>>) -> UITDDeviceModel {
    
        let model = UITDDeviceModel()
        
        for dic in dataArr {
            
            var editModel = EditItemModel()
            if dic.keys.contains(DeviceInforType.code) {
                //设备编码
                editModel = dic[DeviceInforType.code]!
                model.code = editModel.uploadInfor
                
            }else if dic.keys.contains(DeviceInforType.name){
                //设备名称
                editModel = dic[DeviceInforType.name]!
                model.name = editModel.uploadInfor
                
            }else if dic.keys.contains(DeviceInforType.type){
                //设备类型
                editModel = dic[DeviceInforType.type]!
                model.deviceType = editModel.uploadInfor
                
            }else if dic.keys.contains(DeviceInforType.manufacturer){
                //生产厂商
                editModel = dic[DeviceInforType.manufacturer]!
                model.manufacturer = editModel.uploadInfor
                
            }else if dic.keys.contains(DeviceInforType.building){
                //建筑
                editModel = dic[DeviceInforType.building]!
                model.buildingId = editModel.uploadInfor
                
            }else if dic.keys.contains(DeviceInforType.keypart){
                //部位
                editModel = dic[DeviceInforType.keypart]!
                model.keypartId = editModel.uploadInfor
                
            }else if dic.keys.contains(DeviceInforType.address){
                //安装位置
                editModel = dic[DeviceInforType.address]!
                model.address = editModel.uploadInfor
                
            }
            
        }
        
        return model
    }

}
