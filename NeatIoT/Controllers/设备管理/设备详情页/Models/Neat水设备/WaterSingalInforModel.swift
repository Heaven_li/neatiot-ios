//
//  WaterSingalInforModel.swift
//  NeatIoT
//
//  Created by neat on 2019/12/5.
//  Copyright © 2019 Neat. All rights reserved.
//

import UIKit

class WaterSingalInforModel: DeviceInforBaseModel {
    
    
    /// 根据给定数据数组 返回对应数据对象
    /// - Parameter dataArr: 上送数据对象
    func formateWaterSignalDataToModel(dataArr:Array<Dictionary<String,EditItemModel>>) -> WaterSingalInforModel {
    
        let model = WaterSingalInforModel()
        
        /// 用作判断是否是模拟量 false 或 数字量 true
        var isYC = false
        for dic in dataArr {
            
            if dic.keys.contains(DeviceInforType.signal_type){
                //信号类型
                let editModel = dic[DeviceInforType.signal_type]!
                if editModel.uploadInfor == "1"{
                    isYC = true
                }else{
                    isYC = false
                }
                
            }
            
        }
        
        for dic in dataArr {
            
            var editModel = EditItemModel()
            if dic.keys.contains(DeviceInforType.code) {
                //设备编码
                editModel = dic[DeviceInforType.code]!
                model.code = editModel.uploadInfor
                
            }else if dic.keys.contains(DeviceInforType.name){
                //设备名称
                editModel = dic[DeviceInforType.name]!
                model.name = editModel.uploadInfor
                
            }else if dic.keys.contains(DeviceInforType.signal_type){
                //信号类型
                editModel = dic[DeviceInforType.signal_type]!
                if editModel.uploadInfor == "1"{
                    model.isYC = true
                }else{
                    model.isYC = false
                }
                
            }else if dic.keys.contains(DeviceInforType.enterprise){
                //企业
                editModel = dic[DeviceInforType.enterprise]!
                model.enterpriseId = editModel.uploadInfor
                
            }else if dic.keys.contains(DeviceInforType.building){
                //建筑
                editModel = dic[DeviceInforType.building]!
                model.buildingId = editModel.uploadInfor
                
            }else if dic.keys.contains(DeviceInforType.keypart){
                //部位
                editModel = dic[DeviceInforType.keypart]!
                model.keypartId = editModel.uploadInfor
                
            }else if dic.keys.contains(DeviceInforType.address){
                //安装位置
                editModel = dic[DeviceInforType.address]!
                model.address = editModel.uploadInfor
                
            }
            
            if isYC{
                
                if dic.keys.contains(DeviceInforType.unit){
                    //单位
                    editModel = dic[DeviceInforType.unit]!
                    model.unit = editModel.uploadInfor
                    
                }else if dic.keys.contains(DeviceInforType.threshold_upper_limit){
                    //阈值上限
                    editModel = dic[DeviceInforType.threshold_upper_limit]!
                    model.maximum = editModel.uploadInfor
                    
                }else if dic.keys.contains(DeviceInforType.type){
                    //阈值上限
                    editModel = dic[DeviceInforType.type]!
                    model.deviceType = editModel.uploadInfor
                    
                }else if dic.keys.contains(DeviceInforType.threshold_lower_limit){
                    //阈值下限
                    editModel = dic[DeviceInforType.threshold_lower_limit]!
                    model.minimum = editModel.uploadInfor
                    
                }else if dic.keys.contains(DeviceInforType.lower_limit_warning){
                    //下限报警值
                    editModel = dic[DeviceInforType.lower_limit_warning]!
                    model.lowLimit1 = editModel.uploadInfor
                    model.enableLowLimit2Alarm = editModel.uploadIsWarning
                    
                }else if dic.keys.contains(DeviceInforType.lower_lower_limit_warning){
                    //下下限报警值
                    editModel = dic[DeviceInforType.lower_lower_limit_warning]!
                    model.lowLimit2 = editModel.uploadInfor
                    model.enableLowLimit1Alarm = editModel.uploadIsWarning
                    
                }else if dic.keys.contains(DeviceInforType.upper_limit_warning){
                    //上限报警值
                    editModel = dic[DeviceInforType.upper_limit_warning]!
                    model.highLimit1 = editModel.uploadInfor
                    model.enableHighLimit1Alarm = editModel.uploadIsWarning
                    
                }else if dic.keys.contains(DeviceInforType.upper_upper_limit_warning){
                    //上上限报警值
                    editModel = dic[DeviceInforType.upper_upper_limit_warning]!
                    model.highLimit2 = editModel.uploadInfor
                    model.enableHighLimit2Alarm = editModel.uploadIsWarning
                    
                }
                
            }else{
                
                if dic.keys.contains(DeviceInforType.true_mean_type){
                    //真值含义
                    editModel = dic[DeviceInforType.true_mean_type]!
                    model.trueMeaning = editModel.uploadInfor
                    model.enableTureAlarm = editModel.uploadIsWarning
                    
                }else if dic.keys.contains(DeviceInforType.false_mean_type){
                    //假值含义
                    editModel = dic[DeviceInforType.false_mean_type]!
                    model.falseMeaning = editModel.uploadInfor
                    model.enableFalseAlarm = editModel.uploadIsWarning
                    
                }
                
            }
            
            
            
        }
        
        return model
    }

}
