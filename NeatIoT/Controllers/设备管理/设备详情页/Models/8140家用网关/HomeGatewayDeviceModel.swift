//
//  HomeGatewayDeviceModel.swift
//  NeatIoT
//
//  Created by Yang Blus on 2020/7/20.
//  Copyright © 2020 Neat. All rights reserved.
//

import UIKit

class HomeGatewayDeviceModel: DeviceInforBaseModel {
    
    /// 根据给定数据数组 返回对应数据对象
    /// - Parameter dataArr: 上送数据对象
    func formateHomeGatewayDeviceDataToModel(dataArr:Array<Dictionary<String,EditItemModel>>) -> HomeGatewayDeviceModel {
    
        let model = HomeGatewayDeviceModel()
        
        for dic in dataArr {
            
            var editModel = EditItemModel()
            if dic.keys.contains(DeviceInforType.name){
                //设备名称
                editModel = dic[DeviceInforType.name]!
                model.name = editModel.uploadInfor
                
            }else if dic.keys.contains(DeviceInforType.address){
                //安装位置
                editModel = dic[DeviceInforType.address]!
                model.address = editModel.uploadInfor
                
            }
            
        }
        
        return model
    }

}
