//
//  DeleteGatewayOrDevicePublic.swift
//  NeatIoT
//
//  Created by Yang Blus on 2020/10/27.
//  Copyright © 2020 Neat. All rights reserved.
//

import UIKit
import HandyJSON

class DeleteGatewayOrDevicePublic: NSObject,HandyJSON,Encodable {
    
    /// 设备model
    var model:String = ""
    /// 设备级别 1:网关 2主机 3:器件
    var isDeviceLevel:Int = -1
    /// 要删除设备ID数组
    var ids:Array<String> = []
    
    override required init() {
        
    }

}
