//
//  WaterSourceInforModel.swift
//  NeatIoT
//
//  Created by neat on 2019/12/5.
//  Copyright © 2019 Neat. All rights reserved.
//

import UIKit

class WaterSourceInforModel: DeviceInforBaseModel {
    
    /// 根据给定数据数组 返回对应数据对象
    /// - Parameter dataArr: 上送数据对象
    func formateWaterSourceMonToModel(dataArr:Array<Dictionary<String,EditItemModel>>) -> WaterSourceInforModel {
    
        let model = WaterSourceInforModel()
        
        for dic in dataArr {
            
            var editModel = EditItemModel()
            if dic.keys.contains(DeviceInforType.code) {
                //设备编码
                editModel = dic[DeviceInforType.code]!
                model.code = editModel.uploadInfor
                
            }else if dic.keys.contains(DeviceInforType.name){
                //设备名称
                editModel = dic[DeviceInforType.name]!
                model.name = editModel.uploadInfor
                
            }else if dic.keys.contains(DeviceInforType.type){
                //类型
                editModel = dic[DeviceInforType.type]!
                model.deviceType = editModel.uploadInfor
                
            }else if dic.keys.contains(DeviceInforType.domain){
                //中心
                editModel = dic[DeviceInforType.domain]!
                model.domainId = editModel.uploadInfor
                
            }else if dic.keys.contains(DeviceInforType.enterprise){
                //企业
                editModel = dic[DeviceInforType.enterprise]!
                model.enterpriseId = editModel.uploadInfor
                
            }else if dic.keys.contains(DeviceInforType.building){
                //建筑
                editModel = dic[DeviceInforType.building]!
                model.buildingId = editModel.uploadInfor
                
            }else if dic.keys.contains(DeviceInforType.keypart){
                //部位
                editModel = dic[DeviceInforType.keypart]!
                model.keypartId = editModel.uploadInfor
                
            }else if dic.keys.contains(DeviceInforType.address){
                //安装位置
                editModel = dic[DeviceInforType.address]!
                model.address = editModel.uploadInfor
                
            }else if dic.keys.contains(DeviceInforType.unit){
                //单位
                editModel = dic[DeviceInforType.unit]!
                model.unit = editModel.uploadInfor
                    
            }else if dic.keys.contains(DeviceInforType.manufacturer){
                //单位
                editModel = dic[DeviceInforType.manufacturer]!
                model.manufacturer = editModel.uploadInfor
                    
            }else if dic.keys.contains(DeviceInforType.connection_type){
                //通讯方式
                editModel = dic[DeviceInforType.connection_type]!
                model.connectionType = editModel.uploadInfor
                    
            }
            
            if model.manufacturer == "4001" {
                //拓普索尔
                
            }else if model.manufacturer == "4003"{
                //万讯
                if dic.keys.contains(DeviceInforType.imsi){
                    //imsi
                    editModel = dic[DeviceInforType.imsi]!
                    model.imsi = editModel.uploadInfor
                        
                }else if dic.keys.contains(DeviceInforType.imei){
                    //imei
                    editModel = dic[DeviceInforType.imei]!
                    model.imei = editModel.uploadInfor
                        
                }
                
            }
            
        }
        
        return model
    }

}
