//
//  SupperDeviceEditPublicModel.swift
//  NeatIoT
//
//  Created by Yang Blus on 2020/10/27.
//  Copyright © 2020 Neat. All rights reserved.
//

import UIKit
import HandyJSON

class SupperDeviceEditPublicModel: NSObject,HandyJSON,Encodable {
    
    
    /// 设备model 菜单中返回
    var model:String = ""
    
    /// 设备级别 1:网关 2:主机 3:器件
    var isDeviceLevel:Int = 3
    
    /// 设备信息model
    var deviceInfor:DeviceInforBaseModel = DeviceInforBaseModel.init();
    
    required override init() {
        
    }
}
