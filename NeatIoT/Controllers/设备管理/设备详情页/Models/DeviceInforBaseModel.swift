//
//  DeviceInforBaseModel.swift
//  NeatIoT
//
//  Created by neat on 2019/12/2.
//  Copyright © 2019 Neat. All rights reserved.
//

import UIKit
import HandyJSON
/// 获取设备详情返回数据的父类bean
class DeviceInforBaseModel: NSObject,HandyJSON,Encodable {
    
    ///公共
    
    /// 设备id
    var id:String?
    /// 设备编码
    var code:String?
    /// 设备名称
    var name:String?
    /// 设备报警信息
    var alarmStatus:Int?
    /// 安装位置
    var address:String?
    /// 设备图片地址
    var pictureUrl:String?
    /// 设备所属中心id
    var domainId:String?
    /// 设备所属中心名称
    var domainName:String?
    /// 设备所属企业id
    var enterpriseId:String?
    /// 设备所属企业名称
    var enterpriseName:String?
    /// 设备所属建筑id
    var buildingId:String?
    /// 设备所属建筑名称
    var buildingName:String?
    /// 设备所属部位id
    var keypartId:String?
    /// 设备所属部位名称
    var keypartName:String?
    
    
    /// 设备在线状态
    var onlineStatus:Int?
    /// 设备类型id
    var deviceType:String?
    /// 设备型号
    var model:String?
    /// 设备类型名称
    var deviceTypeName:String?
    /// 设备厂商id
    var manufacturer:String?
    /// 设备厂商名称
    var manufacturerName:String?
    
    /// 水设备共用
    /// 信号强度
    var signalStatus:String?
    /// 信号数量
    var signalCount:String?
    
    /// 电量设备共用
    /// 电量
    var batteryStatus:String?
    
    ///一体式传输设备
    /// 下属主机数
    var fireSystemCount:String?
    /// 报文协议
    var messageProtocol:String?
    
    
    ///一体式水源
    
    /// 单位
    var unit:String?
    /// 最大值
    var maximum:String?
    /// 最小值
    var minimum:String?
    /// 连接方式
    var connectionType:String?
    /// imei
    var imei:String?
    /// imsi
    var imsi:String?
    
    /// NB设备
    /// 纬度
    var latitude:String?
    /// 经度
    var longitude:String?
    /// 描点位置
    var gisaddress:String?
    
    
    
    /// 父设备id
    var deviceId:String?
    
    
    /// NEAT水信号
    /// 是否是模拟量 true模拟量  false数字量
    var isYC:Bool?
    /// 实时值
    var value:String?
    
    /// 上上限报警值
    var highLimit2:String?
    /// 上上限是否报警
    var enableHighLimit2Alarm:Bool?
    /// 上限报警值
    var highLimit1:String?
    /// 上限是否报警
    var enableHighLimit1Alarm:Bool?
    /// 下下限报警值
    var lowLimit2:String?
    /// 下下限是否报警
    var enableLowLimit2Alarm:Bool?
    /// 下限报警值
    var lowLimit1:String?
    /// 下限是否报警
    var enableLowLimit1Alarm:Bool?
    
    /// 真值是否报警
    var enableTureAlarm:Bool?
    /// 真值含义
    var trueMeaning:String?
    /// 假值是否报警
    var enableFalseAlarm:Bool?
    /// 假值含义
    var falseMeaning:String?
    
    /// 智慧用电
    /// 负责人
    var owner:String?
    /// 联系电话
    var phone:String?
    /// 软件版本
    var version:String?
    /// sim卡号
    var sim:String?
    /// 变比系数
    var ratio:String?
    /// 通道数
    var channelCount:String?
    /// 上传周期
    var timeout:String?
    /// 8127A 8128A数据项数组
    var dataItems:Array<EleItemDataModel> = []
    
    /// 8140
    /// 器件数
    var deviceCount:String?
    
    /// 有人泵房网关
    /// 电源状态 1:通电 2:断电
    var powerStatus:Int?
    /// 电源状态 文字
    var powerStatusDesc:String?
    
    /// 手自动状态 1:自动 2:手动
    var operatingStatus:Int?
    /// 手自动状态 文字
    var operatingStatusDesc:String?
    
    /// 运行状态 1:运行 2:停止
    var runStatus:Int?
    /// 运行状态 文字
    var runStatusDesc:String?
    
    
    
    required override init() {
        
    }
    
    
}

class EleItemDataModel: HandyJSON,Encodable {
    
    required init() {}
    ///数据项名称
    var itemName:String? = ""
    ///数据项单位
    var itemUnit:String? = ""
    ///数据项值
    var itemValue:String? = ""
    
}
