//
//  DeletDeviceModel.swift
//  NeatIoT
//
//  Created by neat on 2019/12/12.
//  Copyright © 2019 Neat. All rights reserved.
//

import UIKit
import HandyJSON

class DeletDeviceModel: NSObject,HandyJSON,Encodable {
    
    /// 一体式主机要填写的 传输装置id
    var uitdId:String = ""
    /// 一体式主机要填写的 主机id
    var ids:Array<String> = []
    
    required override init() {
        
    }
    
}
