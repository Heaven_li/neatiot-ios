//
//  IntegrateEleGateway.swift
//  NeatIoT
//
//  Created by Yang Blus on 2020/5/25.
//  Copyright © 2020 Neat. All rights reserved.
//

import UIKit

class IntegrateEleGateway: DeviceInforBaseModel {
    
    
    func formateSmartEleToModel(dataArr:Array<Dictionary<String,EditItemModel>>) -> IntegrateEleGateway {
        
        let model = IntegrateEleGateway()
        
        for dic in dataArr {
            
            var editModel = EditItemModel()
            if dic.keys.contains(DeviceInforType.name){
                //设备名称
                editModel = dic[DeviceInforType.name]!
                model.name = editModel.uploadInfor
                
            }else if dic.keys.contains(DeviceInforType.building){
                //建筑
                editModel = dic[DeviceInforType.building]!
                model.buildingId = editModel.uploadInfor
                
            }else if dic.keys.contains(DeviceInforType.keypart){
                //部位
                editModel = dic[DeviceInforType.keypart]!
                model.keypartId = editModel.uploadInfor
                
            }else if dic.keys.contains(DeviceInforType.address){
                //安装位置
                editModel = dic[DeviceInforType.address]!
                model.address = editModel.uploadInfor
                
            }else if dic.keys.contains(DeviceInforType.gis){
                //gis
                editModel = dic[DeviceInforType.gis]!
                model.latitude = editModel.latitude
                model.longitude = editModel.longitude
                model.gisaddress = editModel.uploadInfor
                    
            }else if dic.keys.contains(DeviceInforType.model){
                //model
                editModel = dic[DeviceInforType.model]!
                model.model = editModel.uploadInfor
                    
            }
            
        }
        
        return model
        
    }

}
