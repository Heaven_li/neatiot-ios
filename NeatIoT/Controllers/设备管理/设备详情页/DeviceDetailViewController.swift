//
//  DeviceDetailViewController.swift
//  NeatIoT
//
//  Created by neat on 2019/11/19.
//  Copyright © 2019 Neat. All rights reserved.
//

import UIKit

protocol DeviceDetailViewControllerDelegate {
    
    /// 刷新数据
    func refreshListData()
}

class DeviceDetailViewController: SupperViewController {

    var initModel:VCInitModel?
    /// NB设备编码
    var deviceCode:String = ""
    /// 设备id
    var deviceId:String = ""
    /// 添加器件时用到的企业id
    var enterpriseID:String = ""
    /// 父器件id
    var deviceFatherID:String = ""
    /// 编辑状态 true 在编辑
    var editState:Bool = false
    /// 是否是8127A 8128A
    var isEleContain:Bool = false
    /// 是否显示底部工具栏
    var isShowBottomControlBar:Bool = false
    
    /// 当前编辑model
    var curretnEditModel:EditItemModel?
    
    var delegate:DeviceDetailViewControllerDelegate?
    
    lazy var eleContainView: DeviceDetailContainView = {
        let view = DeviceDetailContainView.init()
        view.delegate = self
        return view
    }()
    
    
    lazy var inputPopView: InputPopView = {
        let popview = InputPopView.init()
        popview.delegate = self
        return popview
    }()
    
    lazy var chosePopView: SelectPopView = {
        let popView = SelectPopView.init()
        popView.leftBtnImage = UIImage.init(named: "input_icon")!
        popView.rightBtnImage = UIImage.init(named: "qr_code")!
        popView.delegate = self
        return popView
    }()
    
    lazy var inforSelectPopView: InforSelectView = {
        let view = InforSelectView.init()
        view.delegate = self
        return view
    }()
    
    lazy var textFielde: UITextField = {
        let tf = UITextField.init()
        return tf
    }()
    
    lazy var bottomControlBar: UIStackView = {
        let stackView = UIStackView.init()
        stackView.axis = .horizontal
        stackView.alignment = .fill
        stackView.distribution = .fillEqually
        
        let backBtn = UIButton.init(type: .custom)
        backBtn.titleLabel?.font = UIFont.systemFont(ofSize: 16)
        backBtn.setTitle("返回列表", for: .normal)
        backBtn.backgroundColor = #colorLiteral(red: 0.8039215686, green: 0.7960784314, blue: 0.8039215686, alpha: 1)
        backBtn.addTarget(self, action: #selector(backToList), for: .touchUpInside)
        
        let containBtn = UIButton.init(type: .custom)
        containBtn.titleLabel?.font = UIFont.systemFont(ofSize: 16)
        containBtn.setTitle("继续添加", for: .normal)
        containBtn.backgroundColor = #colorLiteral(red: 0.9859903455, green: 0.4849994183, blue: 0.4872213602, alpha: 1)
        containBtn.addTarget(self, action: #selector(containToAdd), for: .touchUpInside)
        
        stackView.addArrangedSubview(backBtn)
        stackView.addArrangedSubview(containBtn)
    
        return stackView
    }()
    
    override func viewWillAppear(_ animated: Bool) {
        NotificationCenter.default.addObserver(self.inputPopView, selector: #selector(self.inputPopView.keyboardWillShow(notify:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self.inputPopView, selector: #selector(self.inputPopView.keyboardWillHiden(notify:)), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self.inputPopView, name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.removeObserver(self.inputPopView, name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        self.titleName = "设备详情"
        
        self.inputPopView.showInputTo(view: self.view)
        
        configerUI()
        
        getInfor()

    }
    
    func configerUI() {
    
        self.view.addSubview(self.textFielde)
        if self.isShowBottomControlBar {
            
            //判断设备类型 显示不同继续添加文字
        
            self.view.addSubview(self.bottomControlBar)
            self.bottomControlBar.snp.makeConstraints { (make) in
                make.bottom.leading.trailing.equalTo(self.view)
                make.height.equalTo(self.view.snp.height).multipliedBy(0.08)
            }
            self.view.addSubview(self.eleContainView)
            self.eleContainView.snp.makeConstraints { (make) in
                make.top.leading.trailing.equalTo(self.view)
                make.bottom.equalTo(self.bottomControlBar.snp.top)
            }
        }else{
            
            let editItem = NavItemModel.init()
            editItem.itemIcon = "delete_device"
            
            let deleteItem = NavItemModel.init()
            deleteItem.itemIcon = "device_edit"
            
            self.rightItems = [deleteItem,editItem]
            
            self.view.addSubview(self.eleContainView)
            self.eleContainView.snp.makeConstraints { (make) in
                make.edges.equalTo(self.view)
            }
        }
        
    }
    
    @objc func backToList() {
        
        
        if self.initModel?.gatewayMenuModel?.model == DeviceModel.FireSystem.IntegratedTransmissionHost ||
            self.initModel?.gatewayMenuModel?.model == DeviceModel.WaterSystem.NeatWaterSingal ||
            self.initModel?.gatewayMenuModel?.model == DeviceModel.WaterSystem.NT8160A_device{

            NotificationCenter.default.post(name: NSNotification.Name(rawValue: CustomNotificationName.ReloadHostDeviceListData), object: nil)
            self.popTo(vcClass: HostDeviceMainViewController.init())
        }else{
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: CustomNotificationName.ReloadDeviceListData), object: nil)
            self.popTo(vcClass: DeviceMainListViewController.init())
        }
    }
    
    @objc func containToAdd() {
        
        let menuModel = MenuModel.init()
        menuModel.model = self.initModel?.gatewayMenuModel?.model
        menuModel.name = self.initModel?.gatewayMenuModel?.name ?? ""
        
        let initModel = VCInitModel.init()
        initModel.gatewayMenuModel = menuModel
        initModel.navTitleText = menuModel.name
        
        
        let addVC = DeviceAddViewController.init()
        addVC.addModel = initModel
        
        if self.initModel?.gatewayMenuModel?.model == DeviceModel.FireSystem.uitd_NT9009 {
            
            addVC.addModel?.gatewayMenuModel?.model = DeviceModel.FireSystem.IntegratedTransmissionHost
            addVC.deviceFatherID = self.deviceId
            addVC.enterpriseID = self.enterpriseID
            
        }else if self.initModel?.gatewayMenuModel?.model == DeviceModel.WaterSystem.wireless_NT8327{
            
            addVC.addModel?.gatewayMenuModel?.model = DeviceModel.WaterSystem.NeatWaterSingal
            addVC.deviceFatherID = self.deviceId
            addVC.enterpriseID = self.enterpriseID
            
        }else if self.initModel?.gatewayMenuModel?.model == DeviceModel.WaterSystem.NT8160A{
            
            addVC.addModel?.gatewayMenuModel?.model = DeviceModel.WaterSystem.NT8160A_device
            addVC.deviceFatherID = self.deviceId
            addVC.enterpriseID = self.enterpriseID
            
        }else if self.initModel?.gatewayMenuModel?.model == DeviceModel.WaterSystem.NT8160A_device{
            
            addVC.addModel?.gatewayMenuModel?.model = DeviceModel.WaterSystem.NT8160A_device
            addVC.deviceFatherID = self.deviceFatherID
            addVC.enterpriseID = self.enterpriseID
            
        }else if self.initModel?.gatewayMenuModel?.model == DeviceModel.FireSystem.IntegratedTransmissionHost{
            
            addVC.addModel?.gatewayMenuModel?.model = DeviceModel.FireSystem.IntegratedTransmissionHost
            addVC.deviceFatherID = self.deviceFatherID
            addVC.enterpriseID = self.enterpriseID
            
        }else if self.initModel?.gatewayMenuModel?.model == DeviceModel.WaterSystem.NeatWaterSingal{
            
            addVC.addModel?.gatewayMenuModel?.model = DeviceModel.WaterSystem.NeatWaterSingal
            addVC.deviceFatherID = self.deviceFatherID
            addVC.enterpriseID = self.enterpriseID
            
        }else{
            
        }
        
        self.navigationController?.pushViewController(addVC, animated: true)
        
    }
    
    @objc func getInfor(_ isShowLoading:Bool = false) {
        
        if isShowLoading {
            NeatHud.showLoading(view: self.view, message: "")
        }
        
        DeviceDetailContainViewModel.getDeviceInforBy(model: self.initModel!, deviceId: deviceId, getData: { (dataArr, inforModel) in
            
                if isShowLoading {
                    NeatHud.dissmissHud(view: self.view)
                }

                self.eleContainView.configerData(dataSource: dataArr, model: inforModel)

                self.deviceId = inforModel.deviceID
                self.enterpriseID = inforModel.enterpriseID
                self.deviceCode = inforModel.deviceCode

            }) { (errorResponse) in
                
                if isShowLoading {
                    NeatHud.dissmissHud(view: self.view)
                }
                
                if errorResponse.errCode == RequestResponseCode.dataParsingException {
                    //数据异常
                    self.eleContainView.containTableView.showRequestFaildView {
                        
                    }
                }else if errorResponse.errCode == RequestResponseCode.netCanNotContent || errorResponse.errCode == RequestResponseCode.requestTimeOut || errorResponse.errCode == RequestResponseCode.canNotContentHost{
                    //无网络
                    self.eleContainView.containTableView.showNetErrorView(errorMessage: errorResponse.errMessage) {
                        self.getInfor(true)
                    }
                }else{
                    
                    self.eleContainView.containTableView.showRequestFaildView(errMessage: errorResponse.errMessage) {
                        self.getInfor(true)
                    }
                }
            }
        
    }
    override func backClick() {
        
        if editState {
            
            self.showAlert(vc: self, title: "提示", message: "修改尚未保存,是否放弃修改", leftTitle: "确定", rightTitle: "取消", leftAction: { (action) in
                self.navigationController?.popViewController(animated: true)
            }) { (action) in
                    
            }
            
        }else{
            self.navigationController?.popViewController(animated: true)
        }
        
    }
    
    /// 导航栏右侧最右按钮点击事件 如果只有一个按钮可重写该方法
    override func navRRClick() -> Void {
        
        if !editState {
            
            startEditState()
            
            DeviceDetailContainViewModel.startEdit()
            
            self.eleContainView.reloadData(dataSource: DeviceDetailContainViewModel.dataSourceArr)

        }else{
            
            endEditState()
            DeviceDetailContainViewModel.endEdit()
        
            self.eleContainView.reloadData(dataSource: DeviceDetailContainViewModel.dataSourceArr)

            //判断数据是否有更改
            //如果更新
            DeviceDetailContainViewModel.getUploadInforModel(success: {(response) in
                if response.code == 200{
                    NeatHud.showMessage(message: "更改成功", view: self.view)
                }else{
                    NeatHud.showMessage(message: response.message ?? "修改失败,稍后再试", view: self.view)
                }
            }) { (error) in
                NeatHud.showMessage(message: error.errMessage, view: self.view)
            }
            
        }
        
        
    }
    
    /// 开始编辑
    func startEditState() {
        
        let editItem = NavItemModel.init()
        editItem.itemIcon = "delete_device"
        
        let deleteItem = NavItemModel.init()
        deleteItem.itemIcon = "nav_save"
        
        self.rightItems = [deleteItem,editItem]
        
        editState = true
        
    }
    
    /// 结束编辑
    func endEditState() {
        
        let editItem = NavItemModel.init()
        editItem.itemIcon = "delete_device"
        
        let deleteItem = NavItemModel.init()
        deleteItem.itemIcon = "device_edit"
        
        self.rightItems = [deleteItem,editItem]
        
        editState = false
        
    }
    
    /// 导航栏右侧最左按钮点击事件
    override func navRLClick() -> Void {
        
        if editState {
            
            self.showAlert(vc: self, title: "提示", message: "修改尚未保存,确定要删除该设备吗？", leftTitle: "确定", rightTitle: "取消", leftAction: { (action) in
                self.deleteDevice()
            }) { (action) in
                    
            }
            
        }else{
            self.showAlert(vc: self, title: "提示", message: "确定要删除该设备吗？", leftTitle: "确定", rightTitle: "取消", leftAction: { (action) in
                self.deleteDevice()
            }) { (action) in
                    
            }
        }
        
    }
    
    func deleteDevice() {
        
        let model = DeletDeviceModel.init()
        
        if self.initModel?.gatewayMenuModel?.model == DeviceModel.FireSystem.IntegratedTransmissionHost {
            //消防主机
            model.uitdId = self.deviceFatherID
            model.ids = [self.deviceId]
        
        }else{
            //其他类型
            model.ids = [self.deviceId]
 
        }
        
        //删除
        NeatHud.showLoading(view: self.view, message: "")
        
        DeviceDetailContainViewModel.deleteDeviceInforBy(model: self.initModel!, paramJson: model, resopnse: { (response) in
            
            if response.messageArr?.count == 0{
                
                NeatHud.dissmissHud(view: self.view)
                
                if self.initModel?.gatewayMenuModel?.model == DeviceModel.FireSystem.uitd_NT9009 ||
                    self.initModel?.gatewayMenuModel?.model == DeviceModel.WaterSystem.wireless_NT8327 ||
                    self.initModel?.gatewayMenuModel?.model == DeviceModel.WaterSystem.NT8160A{
                    
                    var popVC:UIViewController?
                    let viewControllers = self.navigationController?.children
                    for vc in viewControllers ?? [] {
                        if vc is DeviceMainListViewController {
                            popVC = vc
                        }
                    }
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: CustomNotificationName.ReloadDeviceListData), object: nil)
                    self.navigationController?.popToViewController(popVC!, animated: true)
                    
                }else{
                    self.delegate?.refreshListData()
                    self.navigationController?.popViewController(animated: true)
                }
                
            }else{
                NeatHud.showMessage(message: response.messageArr![0], view: self.view)
            }
            

        }) { (error) in
            
            NeatHud.showMessage(message: error, view: self.view)

        }
    }
    
    /// 根据数据类型 获取上传信息
    /// - Parameter itemInforType: 数据类型
    func getItmeData(itemInforType:String) -> String {
        let dataSource = DeviceDetailContainViewModel.dataSourceArr
        var retureStr = ""
        for dic in dataSource {
            if dic.keys.contains(itemInforType) {
                let domainId = dic[itemInforType]!.uploadInfor
                if !domainId.isEmpty {
                    retureStr = domainId
                }
            }
        }
        return retureStr
    }
    
    /// 更新随动变更数据
    /// - Parameter model: 当前选中的数据信息
    func refreshDeviceLocalInforByItemInforType(model: KeyValueModel) {
        
        let dataSource = DeviceDetailContainViewModel.dataSourceArr
        
        if curretnEditModel!.inforType == DeviceInforType.domain {
            
            let str = getItmeData(itemInforType: DeviceInforType.domain)
            
            if str != model.item_id {
                //有更改 更新企业 建筑 部位
                for dic in dataSource {
                    if dic.keys.contains(DeviceInforType.enterprise) {
                        let itemModel = dic[DeviceInforType.enterprise]!
                        itemModel.inforText = ""
                        itemModel.uploadInfor = ""
                    }else if dic.keys.contains(DeviceInforType.building) {
                        let itemModel = dic[DeviceInforType.building]!
                        itemModel.inforText = ""
                        itemModel.uploadInfor = ""
                    }else if dic.keys.contains(DeviceInforType.keypart) {
                        let itemModel = dic[DeviceInforType.keypart]!
                        itemModel.inforText = ""
                        itemModel.uploadInfor = ""
                    }
                }
                
            }
            
        }else if curretnEditModel!.inforType == DeviceInforType.enterprise {
            
            let str = getItmeData(itemInforType: DeviceInforType.enterprise)
            if str != model.item_id {
                //有更改 更新 建筑 部位
                for dic in dataSource {
                    if dic.keys.contains(DeviceInforType.building) {
                        let itemModel = dic[DeviceInforType.building]!
                        itemModel.inforText = ""
                        itemModel.uploadInfor = ""
                    }else if dic.keys.contains(DeviceInforType.keypart) {
                        let itemModel = dic[DeviceInforType.keypart]!
                        itemModel.inforText = ""
                        itemModel.uploadInfor = ""
                    }
                }
            }
            
        }else if curretnEditModel!.inforType == DeviceInforType.building {
            
            let str = getItmeData(itemInforType: DeviceInforType.building)
            if str != model.item_id {
                //有更改 更新 部位
                for dic in dataSource {
                    if dic.keys.contains(DeviceInforType.keypart) {
                        let itemModel = dic[DeviceInforType.keypart]!
                        itemModel.inforText = ""
                        itemModel.uploadInfor = ""
                    }
                }
            }
            
        }
        
    }

}

extension DeviceDetailViewController:DeviceDetailContainViewDelegate{
  
    func didChangeSignalType(model: EditItemModel) {
        DeviceDetailContainViewModel.updataWithModelWithInforType(model: model)
        self.eleContainView.reloadData(dataSource: DeviceDetailContainViewModel.dataSourceArr)
    }

    /// 功能按钮点击
    func functionBtnClick(inforModel: DeviceInforViewModel, sender: LoadingButton) {

        if inforModel.deviceCategory == DeviceModel.SmartElectricitySystem.SmartPowerGateway {

            //智慧用电维护
            let controlVC = SmareGatewayControlViewController.init()
            controlVC.deviceInforModel = inforModel
            self.navigationController?.pushViewController(controlVC, animated: true)

        }else if inforModel.deviceCategory == DeviceModel.NBDevice.Probe{

            //消音
            sender.isLoading = true
            NeatRequest.instance.postNBCmute(gid: self.deviceCode, cid: self.deviceCode, success: { (response) in

                sender.isLoading = false
                NeatHud.showMessage(message: response.message ?? "消音指令下发成功!", view: self.view)

            }) { (errorResponse) in
                
                sender.isLoading = false
                NeatHud.showMessage(message: errorResponse.errMessage, view: self.view)
            }

        }else if inforModel.deviceCategory == DeviceModel.FamilySystem.HomeGatewayDevice{
            
            //测试
            sender.isLoading = true
            NeatRequest.instance.postHomeGatewayDeviceTest(id: self.deviceId, success: { (response) in
    
                sender.isLoading = false
                NeatHud.showMessage(message: response.message ?? "测试指令下发成功!", view: self.view)
                
            }) { (error) in
                
                sender.isLoading = false
                NeatHud.showMessage(message: error.errMessage, view: self.view)
            }
            
            
        }else if inforModel.deviceCategory == DeviceModel.GasDetector.HM710NVM ||
                inforModel.deviceCategory == DeviceModel.GasDetector.WS2CG{
                
            //可燃气体探测器 操作页
            let vc = GasDetectorViewController.init()
            vc.inforModel = inforModel
            self.navigationController?.pushViewController(vc, animated: true)
                
                
        }else if inforModel.deviceCategory == DeviceModel.SmartElectricitySystem.ElectricGateway_NT8127C{
            
            let vc = HTCXHandleViewController.init();
            vc.inforModel = inforModel
            self.navigationController?.pushViewController(vc, animated: true)
            
        }else if inforModel.deviceCategory == DeviceModel.WaterSystem.NT8160A_device{
            
            var message:String = "";
            if inforModel.runStatus == 1 {
                //设备当前运行状态
                message = "确定要停止吗？";
            }else if inforModel.runStatus == 2 {
                //设备当前停止状态
                message = "确定要启动吗？";
            }
            
            let commandModel = SignalControlCommandModel.init()
            commandModel.isDeviceLevel = DeviceLevel.Device
            commandModel.equipmentID = self.deviceId
            commandModel.commandType = 2
            commandModel.model = self.initModel?.gatewayMenuModel?.model ?? ""
            if inforModel.runStatus == 1 {
                //设备当前运行状态
                commandModel.commandValue = 3
            }else if inforModel.runStatus == 2 {
                //设备当前停止状态
                commandModel.commandValue = 2
            }
            
            
            showAlert(vc: self, title: "", message: message, leftTitle: "确定", rightTitle: "取消") { (confirmAction) in
                
                self.postCommand(commandModel: commandModel)
                
            } rightAction: { (cancelAction) in
                
                
            }

        }
    }
    
    private func postCommand(commandModel:SignalControlCommandModel){
        
        NeatHud.showLoading(view: self.view, message: "正在下发指令")
        
        NeatRequest.instance.postSignalCommandPublic(inforModel: commandModel) { (response) in
            
            NeatHud.showMessage(message: response.message ?? "指令下发成功!", view: self.view)
            //刷新 设备列表页
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: CustomNotificationName.ReloadDeviceListData), object: nil)
            self.getInfor(false)
            
        } failed: { (error) in
            
            NeatHud.showMessage(message: error.errMessage, view: self.view)
            
        }
        
    }

    /// 暂时用来回掉智慧用电通道点击事件
    func didClickDeviceData(inforModel: DeviceInforViewModel) {
        
        if inforModel.deviceCategory == DeviceModel.SmartElectricitySystem.SmartPowerGateway {
            let controlVC = SmartGatewayChannelViewController.init()
            controlVC.deviceInforModel = inforModel
            self.navigationController?.pushViewController(controlVC, animated: true)
        }else if inforModel.deviceCategory == DeviceModel.FamilySystem.HomeGateway{
            
            
            let model:VCInitModel = VCInitModel()
            model.navTitleText = inforModel.deviceName
            model.deviceCategory = inforModel.deviceCategory
            
            let itemModel = DeviceItemModel()
            itemModel.device_id = inforModel.deviceID
            
            let vc = HostDeviceMainViewController.init()
            vc.initModel = model
            vc.fatherDeviceModel = itemModel
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
    }


    func eleStartRefresh() {
        if editState {
            self.endEditState()
        }

        self.perform(#selector(getInfor(_:)), with: self, afterDelay: 1)
    }


    func eleDidSelectItem(model: EditItemModel) {

        if model.editType != ItemEditStyle.GISStyle {
            //先判断在不在编辑状态
            if !model.isEditState {
                return
            }
            //判断是否可编辑
            if !model.isEditAble{
                NeatHud.showMessage(message: model.titleText+"不可编辑", view: self.view)
                return
            }
        }


        //开始编辑时 记录当前编辑项
        curretnEditModel = model

        if model.editType == .InputStyle {

           //输入
           self.inputPopView.currentEditItemModel = model
           if model.inforType == DeviceInforType.true_mean_type || model.inforType == DeviceInforType.false_mean_type ||
              model.inforType == DeviceInforType.lower_limit_warning || model.inforType == DeviceInforType.lower_lower_limit_warning ||
           model.inforType == DeviceInforType.upper_limit_warning || model.inforType == DeviceInforType.upper_upper_limit_warning{
               self.inputPopView.isShowCheck = true
           }else{
               self.inputPopView.isShowCheck = false
           }
           self.inputPopView.currentEditItemModel = model

           self.textFielde.becomeFirstResponder()

        }else if model.editType == .SelectStyle{

           //列表选择
           let configerModel = InforSelectConfigerModel.init()
           configerModel.deviceCategory = self.initModel!.deviceCategory

           if initModel?.deviceCategory == DeviceModel.FireSystem.IntegratedTransmissionHost
                || initModel?.deviceCategory == DeviceModel.WaterSystem.NeatWaterSingal
                || initModel?.deviceCategory == DeviceModel.WaterSystem.NT8160A_device{

               //如果是水信号 或 火主机
               if model.inforType == DeviceInforType.building{

                   configerModel.apiParamet = self.enterpriseID

               }else if model.inforType == DeviceInforType.keypart{

                   let str = getItmeData(itemInforType: DeviceInforType.building)

                   if str.isEmpty {
                       NeatHud.showMessage(message: "请先选择建筑", view: self.view)
                       return
                   }else{
                       configerModel.apiParamet = str
                   }

               }

           }else{

               if model.inforType == DeviceInforType.enterprise{

                   let str = getItmeData(itemInforType: DeviceInforType.domain)

                   if str.isEmpty {
                       NeatHud.showMessage(message: "请先选择中心", view: self.view)
                       return
                   }else{
                       configerModel.apiParamet = str
                   }

               }else if model.inforType == DeviceInforType.building{

                   let str = getItmeData(itemInforType: DeviceInforType.enterprise)

                   if str.isEmpty {
                       NeatHud.showMessage(message: "请先选择企业", view: self.view)
                       return
                   }else{
                       configerModel.apiParamet = str
                   }

               }else if model.inforType == DeviceInforType.keypart{

                   let str = getItmeData(itemInforType: DeviceInforType.building)

                   if str.isEmpty {
                       NeatHud.showMessage(message: "请先选择建筑", view: self.view)
                       return
                   }else{
                       configerModel.apiParamet = str
                   }
               }
           }

           configerModel.inforType = model.inforType
           configerModel.inforTitle = model.titleText
           self.inforSelectPopView.showSelectView(view: self.view, model: configerModel)

       }else if model.editType == .ChoseStyle{

           //选择
           self.chosePopView.show(view: self.view,model:model)

       }else if model.editType == .GISStyle{

           //地图
           let gisVC = GISViewController.init()
           gisVC.delegate = self
           gisVC.isEditState = model.isEditState
           gisVC.curretnEidtItemModel = model
           self.navigationController?.pushViewController(gisVC, animated: true)
            
       }
    }
}

extension DeviceDetailViewController:InputPopViewDelegate{

    func didInputText(text: String, isSelect: Bool) {

        curretnEditModel?.inforText = text
        curretnEditModel?.uploadInfor = text
        curretnEditModel?.uploadIsWarning = isSelect

        DeviceDetailContainViewModel.updataWithModelWithInforType(model: curretnEditModel!)
        
        self.eleContainView.containTableView.reloadData()

    }

}

extension DeviceDetailViewController:SelectPopViewDelegate{
    
    func didSelectItem(item: ButtonItem, editModel: EditItemModel) {
        switch item {
            case .left:
               
                self.inputPopView.currentEditItemModel = editModel
                self.inputPopView.isShowCheck = false
                self.textFielde.becomeFirstResponder()
                
                break
            case .right:
                
                let vcModel = VCInitModel()
                vcModel.deviceCategory = self.initModel!.deviceCategory
                vcModel.navTitleText = "扫一扫"
                let qrVC = QRScanViewController.init()
                qrVC.delegate = self
                qrVC.vcInit = vcModel
                self.navigationController?.pushViewController(qrVC, animated: true)
            
                break
            default:
                break
        }
    }
    
}

extension DeviceDetailViewController:InforSelectViewDelegate{
    
    func selectInfor(model: KeyValueModel) {
        
        //更新会根据当前编辑内容做界面变更的数据项
        refreshDeviceLocalInforByItemInforType(model: model)
        
        curretnEditModel?.inforText = model.item_name
        curretnEditModel?.uploadInfor = model.item_id
        
        DeviceDetailContainViewModel.updataWithModelWithInforType(model: curretnEditModel!)
        
        self.eleContainView.reloadData(dataSource: DeviceDetailContainViewModel.dataSourceArr)

        
        
    }
     
}

extension DeviceDetailViewController:GISViewControllerDelegate{
    
    func didFinishGISMark(model: EditItemModel) {
        curretnEditModel?.inforText = model.uploadInfor
        curretnEditModel?.uploadInfor = model.uploadInfor
        curretnEditModel?.latitude = model.latitude
        curretnEditModel?.longitude = model.longitude
        DeviceDetailContainViewModel.updataWithModelWithInforType(model: curretnEditModel!)
        
        self.eleContainView.reloadData(dataSource: DeviceDetailContainViewModel.dataSourceArr)

    }

}
extension DeviceDetailViewController:QRScanViewControllerDelegate{
    
    func scanResult(qrString string: String) {
        curretnEditModel?.inforText = string
        curretnEditModel?.uploadInfor = string
        DeviceDetailContainViewModel.updataWithModelWithInforType(model: curretnEditModel!)
        
        self.eleContainView.reloadData(dataSource: DeviceDetailContainViewModel.dataSourceArr)

        
    }
    
    
}
