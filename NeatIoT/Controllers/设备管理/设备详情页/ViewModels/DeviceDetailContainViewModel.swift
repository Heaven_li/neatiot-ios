//
//  DeviceDetailContainViewModel.swift
//  NeatIoT
//
//  Created by neat on 2019/12/2.
//  Copyright © 2019 Neat. All rights reserved.
//

import UIKit

class DeviceDetailContainViewModel: NSObject {
    
    static private var urlStr:String = ""
    
    static private var currentCategory = ""
    
    private static var respData:DeviceDetailResponse = DeviceDetailResponse()
    
    /// 非水信号设备
    private static var dataSource:Array<Dictionary<String,EditItemModel>> = []
    /// 水信号 模拟量 数据源
    private static var analogDataSource:Array<Dictionary<String,EditItemModel>> = []
    /// 水信号 数字量 数据源
    private static var digitalDataSource:Array<Dictionary<String,EditItemModel>> = []
    /// 万讯数据源
    private static var wxDataSource:Array<Dictionary<String,EditItemModel>> = []
    /// 拓普索尔数据源
    private static var tpseDataSource:Array<Dictionary<String,EditItemModel>> = []
    
    /// 获取当前展示数据源
    static var dataSourceArr: Array<Dictionary<String,EditItemModel>> {
        return self.dataSource
    }
    
    static var currentInforModel = DeviceInforBaseModel()
    
    
    class func deleteDeviceInforBy(model:VCInitModel,paramJson:DeletDeviceModel,resopnse:@escaping (_ resultStr:DeleteDeviceResponse)  -> Void,failed:@escaping (_ error:String) -> Void){
        
        var isDeviceLevel = DeviceLevel.Gateway
        
        currentCategory = model.gatewayMenuModel?.model ?? ""
        
        if currentCategory == DeviceModel.FireSystem.uitd_NT9009 {
            urlStr = UrlHost.FireUITD.deleteUITDAPI
        }else if currentCategory == DeviceModel.FireSystem.IntegratedTransmissionHost{
            urlStr = UrlHost.FireUITD.deleteUITDDeviceAPI
        }else if currentCategory == DeviceModel.NBDevice.Probe{
            urlStr = UrlHost.NBDevice.deleteNBDeviceAPI
        }else if currentCategory == DeviceModel.WaterSystem.wireless_NT8327{
            urlStr = UrlHost.WaterDevice.deleteWaterGatewayAPI
        }else if currentCategory == DeviceModel.WaterSystem.NeatWaterSingal{
            urlStr = UrlHost.WaterDevice.deleteWaterSignalAPI
        }else if currentCategory == DeviceModel.WaterSystem.Wgw_NT8167A ||
                    currentCategory == DeviceModel.WaterSystem.Wgw_NT8167C ||
                    currentCategory == DeviceModel.WaterSystem.Wgw_NT8164A ||
                    currentCategory == DeviceModel.WaterSystem.Wgw_NT8161A ||
                    currentCategory == DeviceModel.WaterSystem.Wgw_NT8161B ||
                    currentCategory == DeviceModel.WaterSystem.Wgw_NT8164B  {
            urlStr = UrlHost.WaterDevice.deleteWaterSourceAPI
        }else if currentCategory == DeviceModel.SmartElectricitySystem.SmartPowerGateway{
            urlStr = UrlHost.EleDevice.deleteElectricGatewayAPI
        }else if currentCategory == DeviceModel.SmartElectricitySystem.ElectricGateway_NT8127A ||
                    currentCategory == DeviceModel.SmartElectricitySystem.ElectricGateway_NT8127C ||
                    currentCategory == DeviceModel.SmartElectricitySystem.ElectricGateway_NT8128A{
            urlStr = UrlHost.EleDevice.deleteIntegratedElectricGatewayApi
        }else if currentCategory == DeviceModel.FamilySystem.HomeGateway{
            urlStr = UrlHost.HomeGateway.deleteHomeGatewayAPI
        }else if currentCategory == DeviceModel.FamilySystem.HomeGatewayDevice{
            urlStr = UrlHost.HomeGateway.deleteHomeGatewayDeviceAPI
        }else if currentCategory == DeviceModel.GasDetector.HM710NVM ||
                    currentCategory == DeviceModel.GasDetector.WS2CG{
            urlStr = UrlHost.GasDetector.deleteGasDetectorAPI
        }else if currentCategory == DeviceModel.WaterSystem.NT8160A{
            
            isDeviceLevel = DeviceLevel.Gateway
            
        }else if currentCategory == DeviceModel.WaterSystem.NT8160A_device{
            
            isDeviceLevel = DeviceLevel.Device
            
        }
        
        if currentCategory == DeviceModel.WaterSystem.NT8160A ||
            currentCategory == DeviceModel.WaterSystem.NT8160A_device {
            
            let model = DeleteGatewayOrDevicePublic.init()
            model.model = currentCategory
            model.isDeviceLevel = isDeviceLevel
            model.ids = paramJson.ids
            
            NeatRequest.instance.postDeleteGatewayOrDevicePublic(paraModel: model) { (response) in
                resopnse(response)
            } failed: { (error) in
                failed(error.errMessage)
            }

            
            
        }else{
            
            NeatRequest.instance.deleteDevice(apiPath: urlStr, paraModel: paramJson, success: { (response) in
                
                resopnse(response)
                
            }) { (error) in
                failed(error.errMessage)
            }
        }
        
        
    
    }
    
    class func getDeviceInforBy(model:VCInitModel,deviceId:String,getData:@escaping (_ dataArr:Array<Dictionary<String, EditItemModel>>,_ inforModel:DeviceInforViewModel)  -> Void,failed:@escaping (_ error:BaseErrorResponse) -> Void){
        
        var isDeviceLevel = DeviceLevel.Gateway
        
        currentCategory = model.gatewayMenuModel?.model ?? ""
        
        if currentCategory == DeviceModel.FireSystem.uitd_NT9009 {
            urlStr = UrlHost.FireUITD.uiteDetailAPI
        }else if currentCategory == DeviceModel.FireSystem.IntegratedTransmissionHost{
            urlStr = UrlHost.FireUITD.hostDetailAPI
        }else if currentCategory == DeviceModel.NBDevice.Probe{
            urlStr = UrlHost.NBDevice.nbDetailAPI
        }else if currentCategory == DeviceModel.WaterSystem.wireless_NT8327{
            urlStr = UrlHost.WaterDevice.waterGatewayDetailAPI
        }else if currentCategory == DeviceModel.WaterSystem.NeatWaterSingal{
            urlStr = UrlHost.WaterDevice.waterSignalDetailAPI
        }else if currentCategory == DeviceModel.SmartElectricitySystem.SmartPowerGateway{
            urlStr = UrlHost.EleDevice.electricGatewayDetailAPI
        }else if currentCategory == DeviceModel.SmartElectricitySystem.ElectricGateway_NT8127A ||
                    currentCategory == DeviceModel.SmartElectricitySystem.ElectricGateway_NT8127C ||
                    currentCategory == DeviceModel.SmartElectricitySystem.ElectricGateway_NT8128A {
            urlStr = UrlHost.EleDevice.getIntegratedElectricGatewayDetailApi
        }else if currentCategory == DeviceModel.WaterSystem.Wgw_NT8164B ||
                    currentCategory == DeviceModel.WaterSystem.Wgw_NT8161B {
            urlStr = UrlHost.WaterDevice.integratedDetailAPI
        }else if currentCategory == DeviceModel.WaterSystem.Wgw_NT8167A ||
                    currentCategory == DeviceModel.WaterSystem.Wgw_NT8164A ||
                    currentCategory == DeviceModel.WaterSystem.Wgw_NT8161A ||
                    currentCategory == DeviceModel.WaterSystem.Wgw_NT8167C{
            urlStr = UrlHost.WaterDevice.integratedDetailAPI
        }else if currentCategory == DeviceModel.FamilySystem.HomeGateway{
            urlStr = UrlHost.HomeGateway.homeGatewayDetailAPI
        }else if currentCategory == DeviceModel.FamilySystem.HomeGatewayDevice{
            urlStr = UrlHost.HomeGateway.homeGatewayDeviceDetailAPI
        }else if currentCategory == DeviceModel.GasDetector.HM710NVM ||
                    currentCategory == DeviceModel.GasDetector.WS2CG{
            urlStr = UrlHost.GasDetector.gasDetectorDetailAPI
        }else if currentCategory == DeviceModel.WaterSystem.NT8160A{
            
            isDeviceLevel = DeviceLevel.Gateway
            
        }else if currentCategory == DeviceModel.WaterSystem.NT8160A_device{
            
            isDeviceLevel = DeviceLevel.Device
            
        }
        
        var dataArr:Array<Dictionary<String, EditItemModel>> = []
        
        if currentCategory == DeviceModel.WaterSystem.NT8160A ||
            currentCategory == DeviceModel.WaterSystem.NT8160A_device {
            
            NeatRequest.instance.getGatewayOrDeviceDetailInforPublic(model: model.gatewayMenuModel!.model!, equipmentID: deviceId, isDeviceLevel: isDeviceLevel) { (response) in
                
                respData = response
                
                dataArr.removeAll()
                dataArr.append(contentsOf: handleDataWithCategory(response: response))
                
                let deviceInforModel = DeviceInforViewModel.formateModelByResponse(response: response, category: currentCategory)
                getData(dataArr,deviceInforModel)
                
            } failed: { (error) in
                
                failed(error)
                
            }
            
        }else{
            
            NeatRequest.instance.getDeviceDetailInfor(url: urlStr, id: deviceId, success: { (response) in
                
                respData = response
                
                dataArr.removeAll()
                dataArr.append(contentsOf: handleDataWithCategory(response: response))
                
                let deviceInforModel = DeviceInforViewModel.formateModelByResponse(response: response, category: currentCategory)
                getData(dataArr,deviceInforModel)
                
            }) { (error) in
                
                failed(error)
                
            }
            
        }
    
    }
    
    class func handleDataWithCategory(response:DeviceDetailResponse) -> Array<Dictionary<String, EditItemModel>> {
        
        var dataArr:Array<Dictionary<String, EditItemModel>> = []
            
        currentInforModel = response.deviceInfor ?? DeviceInforBaseModel()
        
        if currentCategory == DeviceModel.FireSystem.uitd_NT9009 {
            
            let typeArr = [DeviceInforType.code,DeviceInforType.name,DeviceInforType.type,DeviceInforType.message_protocol,DeviceInforType.manufacturer,DeviceInforType.domain,DeviceInforType.enterprise,DeviceInforType.building,
            DeviceInforType.keypart,DeviceInforType.address]
            dataArr.removeAll()
            dataArr.append(contentsOf:formateDataSource(typeArr: typeArr, categery: currentCategory, deviceModel: currentInforModel))
            
        }else if currentCategory == DeviceModel.FireSystem.IntegratedTransmissionHost{
            
            let typeArr = [DeviceInforType.code,DeviceInforType.name,DeviceInforType.type,DeviceInforType.manufacturer,DeviceInforType.building,
            DeviceInforType.keypart,DeviceInforType.address]
            dataArr.removeAll()
            dataArr.append(contentsOf:formateDataSource(typeArr: typeArr, categery: currentCategory, deviceModel: currentInforModel))
            
        }else if currentCategory == DeviceModel.NBDevice.Probe{
            
            let typeArr = [DeviceInforType.code,DeviceInforType.name,DeviceInforType.type,DeviceInforType.domain,DeviceInforType.enterprise,DeviceInforType.building,
                           DeviceInforType.keypart,DeviceInforType.address,DeviceInforType.gis]
            dataArr.removeAll()
            dataArr.append(contentsOf:formateDataSource(typeArr: typeArr, categery: currentCategory, deviceModel: currentInforModel))
            
        }else if currentCategory == DeviceModel.WaterSystem.wireless_NT8327{
            
            let typeArr = [DeviceInforType.code,DeviceInforType.name,DeviceInforType.domain,DeviceInforType.enterprise,DeviceInforType.building,
            DeviceInforType.keypart,DeviceInforType.address]
            dataArr.removeAll()
            dataArr.append(contentsOf:formateDataSource(typeArr: typeArr, categery: currentCategory, deviceModel: currentInforModel))
            
        }else if currentCategory == DeviceModel.WaterSystem.NeatWaterSingal{
            //判断信号类型 输入数据源
            var typeArr:Array<String> = []
            let analogArr = [DeviceInforType.code,DeviceInforType.name,DeviceInforType.signal_type,DeviceInforType.type,DeviceInforType.building,DeviceInforType.keypart,DeviceInforType.address,DeviceInforType.unit,DeviceInforType.threshold_lower_limit,DeviceInforType.threshold_upper_limit,DeviceInforType.lower_lower_limit_warning,DeviceInforType.lower_limit_warning,DeviceInforType.upper_limit_warning,DeviceInforType.upper_upper_limit_warning]
            let digitalArr = [DeviceInforType.code,DeviceInforType.name,DeviceInforType.signal_type,DeviceInforType.building,DeviceInforType.keypart,DeviceInforType.address,DeviceInforType.true_mean_type,DeviceInforType.false_mean_type]
            if currentInforModel.isYC ?? false {
                //模拟量
                typeArr = analogArr
            }else{
                //数字量
                typeArr = digitalArr
            }
            
            dataArr.removeAll()
            dataArr.append(contentsOf:formateDataSource(typeArr: typeArr, categery: currentCategory, deviceModel: currentInforModel))
            
            //初始化模拟量
            currentInforModel.isYC = true
            analogDataSource = formateDataSource(typeArr: analogArr, categery: currentCategory, deviceModel: currentInforModel)
            //初始化数字量
            currentInforModel.isYC = false
            digitalDataSource = formateDataSource(typeArr: digitalArr, categery: currentCategory, deviceModel: currentInforModel)
            
        }else if currentCategory == DeviceModel.SmartElectricitySystem.SmartPowerGateway{
            
            let typeArr = [DeviceInforType.code,DeviceInforType.sim,DeviceInforType.timeout,DeviceInforType.soft_version,DeviceInforType.name,DeviceInforType.ratio_coefficient,DeviceInforType.principal,DeviceInforType.contact_number,DeviceInforType.domain,DeviceInforType.enterprise,DeviceInforType.building,
            DeviceInforType.keypart,DeviceInforType.address]
            dataArr.removeAll()
            dataArr.append(contentsOf:formateDataSource(typeArr: typeArr, categery: currentCategory, deviceModel: currentInforModel))
            
        }else if currentCategory == DeviceModel.SmartElectricitySystem.ElectricGateway_NT8127A ||
            currentCategory == DeviceModel.SmartElectricitySystem.ElectricGateway_NT8127C ||
            currentCategory == DeviceModel.SmartElectricitySystem.ElectricGateway_NT8128A {
            
            
            let typeArr = [DeviceInforType.imei,DeviceInforType.name,DeviceInforType.model,DeviceInforType.connection_type,DeviceInforType.domain,DeviceInforType.enterprise,DeviceInforType.building,
            DeviceInforType.keypart,DeviceInforType.address,DeviceInforType.gis]
            dataArr.removeAll()
            dataArr.append(contentsOf:formateDataSource(typeArr: typeArr, categery: currentCategory, deviceModel: currentInforModel))
            
        }else if currentCategory == DeviceModel.WaterSystem.Wgw_NT8161A || currentCategory == DeviceModel.WaterSystem.Wgw_NT8164A ||
            currentCategory == DeviceModel.WaterSystem.Wgw_NT8167A {
            /// tpse
            let typeArr = [DeviceInforType.code,DeviceInforType.name,DeviceInforType.manufacturer,DeviceInforType.connection_type,DeviceInforType.domain,DeviceInforType.enterprise,DeviceInforType.building,DeviceInforType.keypart,DeviceInforType.address,DeviceInforType.gis]
            
            
            dataArr.removeAll()
            dataArr.append(contentsOf:formateDataSource(typeArr: typeArr, categery: currentCategory, deviceModel: currentInforModel))
            
        }else if currentCategory == DeviceModel.WaterSystem.Wgw_NT8167C  {
            /// 铭控 消防栓检测终端
            let typeArr = [DeviceInforType.code,DeviceInforType.name,DeviceInforType.manufacturer,DeviceInforType.connection_type,DeviceInforType.imei,DeviceInforType.domain,DeviceInforType.enterprise,DeviceInforType.building,DeviceInforType.keypart,DeviceInforType.address,DeviceInforType.gis]
            
            
            dataArr.removeAll()
            dataArr.append(contentsOf:formateDataSource(typeArr: typeArr, categery: currentCategory, deviceModel: currentInforModel))
            
        }else if currentCategory == DeviceModel.WaterSystem.Wgw_NT8161B || currentCategory == DeviceModel.WaterSystem.Wgw_NT8164B {
            /// wx
            let typeArr = [DeviceInforType.code,DeviceInforType.name,DeviceInforType.manufacturer,DeviceInforType.connection_type,DeviceInforType.imei,DeviceInforType.imsi,DeviceInforType.domain,DeviceInforType.enterprise,DeviceInforType.building,DeviceInforType.keypart,DeviceInforType.address]
            dataArr.removeAll()
            dataArr.append(contentsOf:formateDataSource(typeArr: typeArr, categery: currentCategory, deviceModel: currentInforModel))
            
        }else if currentCategory == DeviceModel.WaterSystem.wireless_NT8327 {
            
            let typeArr = [DeviceInforType.code,DeviceInforType.name,DeviceInforType.domain,DeviceInforType.enterprise,DeviceInforType.building,
            DeviceInforType.keypart,DeviceInforType.address]
            dataArr.removeAll()
            dataArr.append(contentsOf:formateDataSource(typeArr: typeArr, categery: currentCategory, deviceModel: currentInforModel))
            
        }else if currentCategory == DeviceModel.FamilySystem.HomeGateway {
            //8140网关 展示信息合成
            let typeArr = [DeviceInforType.code,DeviceInforType.name,DeviceInforType.domain,DeviceInforType.enterprise,DeviceInforType.building,
                           DeviceInforType.keypart,DeviceInforType.address,DeviceInforType.gis]
            dataArr.removeAll()
            dataArr.append(contentsOf:formateDataSource(typeArr: typeArr, categery: currentCategory, deviceModel: currentInforModel))
            
        }else if currentCategory == DeviceModel.FamilySystem.HomeGatewayDevice{
            //8140网关下器件 展示信息合成
            let typeArr = [DeviceInforType.code,DeviceInforType.name,DeviceInforType.type,DeviceInforType.address]
            dataArr.removeAll()
            dataArr.append(contentsOf:formateDataSource(typeArr: typeArr, categery: currentCategory, deviceModel: currentInforModel))
            
        }else if currentCategory == DeviceModel.GasDetector.HM710NVM ||
            currentCategory == DeviceModel.GasDetector.WS2CG{
            
            let typeArr = [DeviceInforType.imei,DeviceInforType.name,DeviceInforType.connection_type,DeviceInforType.domain,DeviceInforType.enterprise,DeviceInforType.building,
                           DeviceInforType.keypart,DeviceInforType.address,DeviceInforType.gis]
            dataArr.removeAll()
            dataArr.append(contentsOf:formateDataSource(typeArr: typeArr, categery: currentCategory, deviceModel: currentInforModel))
            
        }else if currentCategory == DeviceModel.WaterSystem.NT8160A {
            
            let typeArr = [DeviceInforType.code,DeviceInforType.name,DeviceInforType.domain,DeviceInforType.enterprise,DeviceInforType.building,
                           DeviceInforType.keypart,DeviceInforType.address,DeviceInforType.gis]
            dataArr.removeAll()
            dataArr.append(contentsOf:formateDataSource(typeArr: typeArr, categery: currentCategory, deviceModel: currentInforModel))
            
            
            
        }else if currentCategory == DeviceModel.WaterSystem.NT8160A_device {
            
            let typeArr = [DeviceInforType.code,DeviceInforType.usrDeviceType,DeviceInforType.name,DeviceInforType.building,
                           DeviceInforType.keypart,DeviceInforType.address]
            dataArr.removeAll()
            dataArr.append(contentsOf:formateDataSource(typeArr: typeArr, categery: currentCategory, deviceModel: currentInforModel))
            
        }
        
        dataSource = dataArr
        
        return dataArr
    }
    
    /// 格式化数据源
    /// - Parameters:
    ///   - typeArr: 编辑项目类型
    ///   - categery: 设备类型
    class private func formateDataSource(typeArr:[String],categery:String,deviceModel:DeviceInforBaseModel) -> Array<Dictionary<String,EditItemModel>> {
        
        var dataArr:Array<Dictionary<String,EditItemModel>> = []
        
        for type in typeArr {
            
            var item_dic:Dictionary<String,EditItemModel> = [:]
            
            let device_item = EditItemModel.init()
            
            device_item.isEditState = false
            device_item.isEditAble = getItemTitleByDeviceCategory(deviceCategory: categery, type: type).editAble
            if type == DeviceInforType.code{
                //编码
                device_item.titleText = getItemTitleByDeviceCategory(deviceCategory: categery, type: DeviceInforType.code).title
                device_item.holderText = "请扫描或输入"+device_item.titleText
                device_item.editType = .ChoseStyle
                device_item.inforType = type
                device_item.inforText = deviceModel.code ?? ""
                device_item.uploadInfor = deviceModel.code ?? ""
                device_item.keyboardType = .numbersAndPunctuation
                
                item_dic = [DeviceInforType.code:device_item]
                
            }else if type == DeviceInforType.name{
                //名称
                device_item.titleText = getItemTitleByDeviceCategory(deviceCategory: categery, type: DeviceInforType.name).title
                device_item.holderText = "请输入"+device_item.titleText
                device_item.editType = .InputStyle
                device_item.inforType = type
                device_item.keyboardType = .default
                device_item.inforText = deviceModel.name ?? ""
                device_item.uploadInfor = deviceModel.name ?? ""
                
                item_dic = [DeviceInforType.name:device_item]
                
            }else if type == DeviceInforType.type{
                //类型
                device_item.titleText = getItemTitleByDeviceCategory(deviceCategory: categery, type: DeviceInforType.type).title
                device_item.holderText = "请选择"+device_item.titleText
                device_item.editType = .SelectStyle
                device_item.inforType = type
                
                if currentCategory == DeviceModel.WaterSystem.NeatWaterSingal {
                    if nil != deviceModel.deviceType {
                        if deviceModel.deviceType == "1" {
                            device_item.inforText = "液压"
                        }else if deviceModel.deviceType == "2"{
                            device_item.inforText = "液位"
                        }else{
                            device_item.inforText = ""
                        }
                        device_item.uploadInfor = deviceModel.deviceType ?? ""
                    }
                    
                }else{
                    device_item.inforText = deviceModel.deviceTypeName ?? ""
                    device_item.uploadInfor = deviceModel.deviceType ?? ""
                }
                
                item_dic = [DeviceInforType.type:device_item]
                
            }else if type == DeviceInforType.model{
                //型号
                device_item.titleText = getItemTitleByDeviceCategory(deviceCategory: categery, type: DeviceInforType.model).title
                device_item.holderText = "请选择"+device_item.titleText
                device_item.editType = .SelectStyle
                device_item.inforType = type
                device_item.inforText = deviceModel.model ?? ""
                device_item.uploadInfor = deviceModel.model ?? ""
                item_dic = [DeviceInforType.model:device_item]
                
            }else if type == DeviceInforType.address{
                //安装位置
                device_item.titleText = getItemTitleByDeviceCategory(deviceCategory: categery, type: DeviceInforType.address).title
                device_item.holderText = "请输入"+device_item.titleText
                device_item.editType = .InputStyle
                device_item.inforType = type
                device_item.keyboardType = .default
                device_item.inforText = deviceModel.address ?? ""
                device_item.uploadInfor = deviceModel.address ?? ""
                
                item_dic = [DeviceInforType.address:device_item]
                
            }else if type == DeviceInforType.message_protocol{
                //通信协议
                device_item.titleText = getItemTitleByDeviceCategory(deviceCategory: categery, type: DeviceInforType.message_protocol).title
                device_item.holderText = "请选择"+device_item.titleText
                device_item.editType = .SelectStyle
                device_item.inforType = type
                device_item.inforText = deviceModel.messageProtocol ?? ""
                device_item.uploadInfor = deviceModel.messageProtocol ?? ""
                
                item_dic = [DeviceInforType.message_protocol:device_item]
                
            }else if type == DeviceInforType.manufacturer{
                //厂商
                device_item.titleText = getItemTitleByDeviceCategory(deviceCategory: categery, type: DeviceInforType.manufacturer).title
                device_item.holderText = "请选择"+device_item.titleText
                device_item.editType = .SelectStyle
                device_item.inforText = deviceModel.manufacturerName ?? ""
                device_item.uploadInfor = deviceModel.manufacturer ?? ""
                device_item.inforType = type
                
                item_dic = [DeviceInforType.manufacturer:device_item]
                
            }else if type == DeviceInforType.domain{
                //中心
                device_item.titleText = getItemTitleByDeviceCategory(deviceCategory: categery, type: DeviceInforType.domain).title
                device_item.holderText = "请选择"+device_item.titleText
                device_item.editType = .SelectStyle
                device_item.inforType = type
                device_item.inforText = deviceModel.domainName ?? ""
                device_item.uploadInfor = deviceModel.domainId ?? ""
                
                item_dic = [DeviceInforType.domain:device_item]
                
            }else if type == DeviceInforType.enterprise{
                //企业
                device_item.titleText = getItemTitleByDeviceCategory(deviceCategory: categery, type: DeviceInforType.enterprise).title
                device_item.holderText = "请选择"+device_item.titleText
                device_item.editType = .SelectStyle
                device_item.inforType = type
                device_item.inforText = deviceModel.enterpriseName ?? ""
                device_item.uploadInfor = deviceModel.enterpriseId ?? ""
                
                item_dic = [DeviceInforType.enterprise:device_item]
                
            }else if type == DeviceInforType.building{
                //建筑
                device_item.titleText = getItemTitleByDeviceCategory(deviceCategory: categery, type: DeviceInforType.building).title
                device_item.holderText = "请选择"+device_item.titleText
                device_item.editType = .SelectStyle
                device_item.inforType = type
                device_item.inforText = deviceModel.buildingName ?? ""
                device_item.uploadInfor = deviceModel.buildingId ?? ""
                
                item_dic = [DeviceInforType.building:device_item]
                
            }else if type == DeviceInforType.keypart{
                //部位
                device_item.titleText = getItemTitleByDeviceCategory(deviceCategory: categery, type: DeviceInforType.keypart).title
                device_item.holderText = "请选择"+device_item.titleText
                device_item.editType = .SelectStyle
                device_item.inforType = type
                device_item.inforText = deviceModel.keypartName ?? ""
                device_item.uploadInfor = deviceModel.keypartId ?? ""
                
                item_dic = [DeviceInforType.keypart:device_item]
                
            }else if type == DeviceInforType.gis{
                //地图描点
                device_item.titleText = getItemTitleByDeviceCategory(deviceCategory: categery, type: DeviceInforType.gis).title
                device_item.holderText = "请选择"+device_item.titleText
                device_item.editType = .GISStyle
                device_item.inforText = deviceModel.gisaddress ?? ""
                device_item.uploadInfor = deviceModel.gisaddress ?? ""
                device_item.latitude = deviceModel.latitude ?? ""
                device_item.longitude = deviceModel.longitude ?? ""
                
                item_dic = [DeviceInforType.gis:device_item]
                
            }else if type == DeviceInforType.unit{
                //测量单位
                device_item.titleText = getItemTitleByDeviceCategory(deviceCategory: categery, type: DeviceInforType.unit).title
                device_item.holderText = ""
                device_item.editType = .InputStyle
                device_item.inforType = type
                device_item.isEditAble = false
                device_item.inforText = deviceModel.unit ?? ""
                device_item.uploadInfor = deviceModel.unit ?? ""
                
                item_dic = [DeviceInforType.unit:device_item]
                
            }else if type == DeviceInforType.signal_type{
                //信号类型
                device_item.titleText = getItemTitleByDeviceCategory(deviceCategory: categery, type: DeviceInforType.signal_type).title
                device_item.holderText = ""
                device_item.editType = .RadioBtnStyle
                device_item.inforType = type
                if deviceModel.isYC! {
                    device_item.inforText = "1"
                    device_item.uploadInfor = "1"
                }else{
                    device_item.inforText = "2"
                    device_item.uploadInfor = "2"
                }
                
                device_item.leftRadioTitle = "模拟量"
                device_item.rightRadioTitle = "数字量"
                item_dic = [DeviceInforType.signal_type:device_item]
                
            }else if type == DeviceInforType.usrDeviceType{
                //信号类型
                device_item.titleText = getItemTitleByDeviceCategory(deviceCategory: categery, type: DeviceInforType.signal_type).title
                device_item.holderText = ""
                device_item.editType = .RadioBtnStyle
                device_item.inforType = type
                if deviceModel.deviceType == "1" {
                    device_item.inforText = "1"
                    device_item.uploadInfor = "1"
                }else{
                    device_item.inforText = "2"
                    device_item.uploadInfor = "2"
                }
                device_item.leftRadioTitle = "风机"
                device_item.rightRadioTitle = "水泵"
                item_dic = [DeviceInforType.usrDeviceType:device_item]
                
            }else if type == DeviceInforType.connection_type{
                //通讯方式
                device_item.titleText = getItemTitleByDeviceCategory(deviceCategory: categery, type: DeviceInforType.connection_type).title
                device_item.holderText = "请选择"+device_item.titleText
                device_item.editType = .SelectStyle
                device_item.inforType = type
                device_item.uploadInfor = deviceModel.connectionType!
                if deviceModel.connectionType == DeviceConnectType.DirectConnectionType {
                    device_item.inforText = "直连"
                }else if deviceModel.connectionType == DeviceConnectType.MobileConnectionType {
                    device_item.inforText = "中国移动"
                }else if deviceModel.connectionType == DeviceConnectType.TelecommunConnectionType {
                    device_item.inforText = "中国电信"
                }else if deviceModel.connectionType == DeviceConnectType.WFConnectionType {
                    device_item.inforText = "潍坊平台"
                }
                
                item_dic = [DeviceInforType.connection_type:device_item]
                
            }else if type == DeviceInforType.true_mean_type{
                //真(1)值含义
                device_item.titleText = getItemTitleByDeviceCategory(deviceCategory: categery, type: DeviceInforType.true_mean_type).title
                device_item.holderText = "请输入"+device_item.titleText
                device_item.editType = .InputStyle
                device_item.inforType = type
                device_item.keyboardType = .numbersAndPunctuation
                device_item.inforText = deviceModel.trueMeaning ?? ""
                device_item.uploadInfor = deviceModel.trueMeaning ?? ""
                device_item.uploadIsWarning = deviceModel.enableTureAlarm ?? false
            
                item_dic = [DeviceInforType.true_mean_type:device_item]
                
            }else if type == DeviceInforType.false_mean_type{
                //假(0)值含义
                device_item.titleText = getItemTitleByDeviceCategory(deviceCategory: categery, type: DeviceInforType.false_mean_type).title
                device_item.holderText = "请输入"+device_item.titleText
                device_item.editType = .InputStyle
                device_item.inforType = type
                device_item.keyboardType = .numbersAndPunctuation
                device_item.inforText = deviceModel.falseMeaning ?? ""
                device_item.uploadInfor = deviceModel.falseMeaning ?? ""
                device_item.uploadIsWarning = deviceModel.enableFalseAlarm ?? false
                item_dic = [DeviceInforType.false_mean_type:device_item]
                
            }else if type == DeviceInforType.threshold_lower_limit{
                //阈值下限
                device_item.titleText = getItemTitleByDeviceCategory(deviceCategory: categery, type: DeviceInforType.threshold_lower_limit).title
                device_item.holderText = "请输入"+device_item.titleText
                device_item.editType = .InputStyle
                device_item.inforType = type
                device_item.keyboardType = .numbersAndPunctuation
                device_item.inforText = deviceModel.minimum ?? ""
                device_item.uploadInfor = deviceModel.minimum ?? ""
                
                item_dic = [DeviceInforType.threshold_lower_limit:device_item]
                
            }else if type == DeviceInforType.threshold_upper_limit{
                //阈值上限
                device_item.titleText = getItemTitleByDeviceCategory(deviceCategory: categery, type: DeviceInforType.threshold_upper_limit).title
                device_item.holderText = "请输入"+device_item.titleText
                device_item.editType = .InputStyle
                device_item.inforType = type
                device_item.keyboardType = .numbersAndPunctuation
                device_item.inforText = deviceModel.maximum ?? ""
                device_item.uploadInfor = deviceModel.maximum ?? ""
                
                item_dic = [DeviceInforType.threshold_upper_limit:device_item]
                
            }else if type == DeviceInforType.lower_limit_warning{
                //下限报警值
                device_item.titleText = getItemTitleByDeviceCategory(deviceCategory: categery, type: DeviceInforType.lower_limit_warning).title
                device_item.holderText = "请输入"+device_item.titleText
                device_item.editType = .InputStyle
                device_item.inforType = type
                device_item.keyboardType = .numbersAndPunctuation
                device_item.inforText = deviceModel.lowLimit1 ?? ""
                device_item.uploadInfor = deviceModel.lowLimit1 ?? ""
                device_item.uploadIsWarning = deviceModel.enableLowLimit1Alarm ?? false
                item_dic = [DeviceInforType.lower_limit_warning:device_item]
                
            }else if type == DeviceInforType.lower_lower_limit_warning{
                //下下限报警值
                device_item.titleText = getItemTitleByDeviceCategory(deviceCategory: categery, type: DeviceInforType.lower_lower_limit_warning).title
                device_item.holderText = "请输入"+device_item.titleText
                device_item.editType = .InputStyle
                device_item.inforType = type
                device_item.keyboardType = .numbersAndPunctuation
                device_item.inforText = deviceModel.lowLimit2 ?? ""
                device_item.uploadInfor = deviceModel.lowLimit2 ?? ""
                device_item.uploadIsWarning = deviceModel.enableLowLimit2Alarm ?? false
                item_dic = [DeviceInforType.lower_lower_limit_warning:device_item]
                
            }else if type == DeviceInforType.upper_limit_warning{
                //上限报警值
                device_item.titleText = getItemTitleByDeviceCategory(deviceCategory: categery, type: DeviceInforType.upper_limit_warning).title
                device_item.holderText = "请输入"+device_item.titleText
                device_item.editType = .InputStyle
                device_item.inforType = type
                device_item.keyboardType = .numbersAndPunctuation
                device_item.inforText = deviceModel.highLimit1 ?? ""
                device_item.uploadInfor = deviceModel.highLimit1 ?? ""
                device_item.uploadIsWarning = deviceModel.enableHighLimit1Alarm ?? false
                item_dic = [DeviceInforType.upper_limit_warning:device_item]
                
            }else if type == DeviceInforType.upper_upper_limit_warning{
                //上上限报警值
                device_item.titleText = getItemTitleByDeviceCategory(deviceCategory: categery, type: DeviceInforType.upper_upper_limit_warning).title
                device_item.holderText = "请输入"+device_item.titleText
                device_item.editType = .InputStyle
                device_item.inforType = type
                device_item.keyboardType = .numbersAndPunctuation
                device_item.inforText = deviceModel.highLimit2 ?? ""
                device_item.uploadInfor = deviceModel.highLimit2 ?? ""
                device_item.uploadIsWarning = deviceModel.enableHighLimit2Alarm ?? false
                item_dic = [DeviceInforType.upper_upper_limit_warning:device_item]
                
            }else if type == DeviceInforType.principal{
                //负责人
                device_item.titleText = getItemTitleByDeviceCategory(deviceCategory: categery, type: DeviceInforType.principal).title
                device_item.holderText = "请输入"+device_item.titleText
                device_item.editType = .InputStyle
                device_item.inforType = type
                device_item.keyboardType = .numbersAndPunctuation
                device_item.inforText = deviceModel.owner ?? ""
                device_item.uploadInfor = deviceModel.owner ?? ""
                item_dic = [DeviceInforType.principal:device_item]
                
            }else if type == DeviceInforType.contact_number{
                //联系电话
                device_item.titleText = getItemTitleByDeviceCategory(deviceCategory: categery, type: DeviceInforType.contact_number).title
                device_item.holderText = "请输入"+device_item.titleText
                device_item.editType = .InputStyle
                device_item.inforType = type
                device_item.keyboardType = .phonePad
                device_item.inforText = deviceModel.phone ?? ""
                device_item.uploadInfor = deviceModel.phone ?? ""
                item_dic = [DeviceInforType.contact_number:device_item]
                
            }else if type == DeviceInforType.ratio_coefficient{
                //变比系数
                device_item.titleText = getItemTitleByDeviceCategory(deviceCategory: categery, type: DeviceInforType.ratio_coefficient).title
                device_item.holderText = "请输入"+device_item.titleText
                device_item.editType = .InputStyle
                device_item.inforType = type
                device_item.keyboardType = .numbersAndPunctuation
                device_item.inforText = deviceModel.ratio ?? ""
                device_item.uploadInfor = deviceModel.ratio ?? ""
                item_dic = [DeviceInforType.ratio_coefficient:device_item]
            }else if type == DeviceInforType.imei{
                //IMEI号
                device_item.titleText = getItemTitleByDeviceCategory(deviceCategory: categery, type: DeviceInforType.imei).title
                device_item.holderText = "请输入"+device_item.titleText
                device_item.editType = .ChoseStyle
                device_item.inforType = type
                device_item.keyboardType = .numbersAndPunctuation
                device_item.inforText = deviceModel.imei ?? ""
                device_item.uploadInfor = deviceModel.imei ?? ""
                item_dic = [DeviceInforType.imei:device_item]
            }else if type == DeviceInforType.imsi{
                //IMSI号
                device_item.titleText = getItemTitleByDeviceCategory(deviceCategory: categery, type: DeviceInforType.imsi).title
                device_item.holderText = "请输入"+device_item.titleText
                device_item.editType = .InputStyle
                device_item.inforType = type
                device_item.keyboardType = .numbersAndPunctuation
                device_item.inforText = deviceModel.imsi ?? ""
                device_item.uploadInfor = deviceModel.imsi ?? ""
                item_dic = [DeviceInforType.imsi:device_item]
                
            }else if type == DeviceInforType.sim{
                //sim号
                device_item.titleText = getItemTitleByDeviceCategory(deviceCategory: categery, type: DeviceInforType.sim).title
                device_item.isEditAble = false
                device_item.inforText = deviceModel.sim ?? ""
                device_item.uploadInfor = deviceModel.sim ?? ""
                item_dic = [DeviceInforType.sim:device_item]
            }else if type == DeviceInforType.timeout{
                //上传周期
                device_item.titleText = getItemTitleByDeviceCategory(deviceCategory: categery, type: DeviceInforType.timeout).title
                device_item.isEditAble = false
                device_item.inforText = deviceModel.timeout ?? ""
                device_item.uploadInfor = deviceModel.timeout ?? ""
                item_dic = [DeviceInforType.timeout:device_item]
            }else if type == DeviceInforType.soft_version{
                //软件版本
                device_item.titleText = getItemTitleByDeviceCategory(deviceCategory: categery, type: DeviceInforType.soft_version).title
                device_item.isEditAble = false
                device_item.inforText = deviceModel.version ?? ""
                device_item.uploadInfor = deviceModel.version ?? ""
                item_dic = [DeviceInforType.soft_version:device_item]
            }
            
            dataArr.append(item_dic)
        }
        
        return dataArr
        
    }
    
    /// 根据设备类型 和 编辑项目类型 返回项目title
    /// - Parameters:
    ///   - deviceCategory: 设备类型
    ///   - itemType: 项目类型
    class private func getItemTitleByDeviceCategory(deviceCategory:String,type:String) -> (title: String, editAble: Bool) {
        
        if deviceCategory == DeviceModel.FireSystem.uitd_NT9009{
            //一体化传输装置
            if type == DeviceInforType.code{
                //编码
                return ("设备编码",true)
            }else if type == DeviceInforType.name{
                //名称
                return ("设备名称",true)
            }else if type == DeviceInforType.type{
                //类型
                return ("设备类型",true)
            }else if type == DeviceInforType.manufacturer{
                //厂商
                return ("生产厂商",true)
            }
        }else if deviceCategory == DeviceModel.FireSystem.IntegratedTransmissionHost{
            //一体化主机
            if type == DeviceInforType.code{
                //编码
                return ("主机编码",true)
            }else if type == DeviceInforType.name{
                //名称
                return ("主机名称",true)
            }else if type == DeviceInforType.type{
                //类型
                return ("主机类别",true)
            }else if type == DeviceInforType.manufacturer{
                //厂商
                return ("生产厂商",true)
            }
        }else if deviceCategory == DeviceModel.WaterSystem.wireless_NT8327{
            //neat水网关
            if type == DeviceInforType.code{
                //编码
                return ("设备编码",true)
            }else if type == DeviceInforType.name{
                //名称
                return ("设备名称",true)
            }else if type == DeviceInforType.type{
                //类型
                return ("设备类型",true)
            }
        }else if deviceCategory == DeviceModel.WaterSystem.NeatWaterSingal{
            //neat水信号
            if type == DeviceInforType.code{
                //编码
                return ("信号编码",true)
            }else if type == DeviceInforType.name{
                //名称
                return ("信号名称",true)
            }else if type == DeviceInforType.type{
                //类型
                return ("设备类型",true)
            }
        }else if deviceCategory == DeviceModel.WaterSystem.Wgw_NT8161B ||
            deviceCategory == DeviceModel.WaterSystem.Wgw_NT8164B ||
            deviceCategory == DeviceModel.WaterSystem.Wgw_NT8161A ||
            deviceCategory == DeviceModel.WaterSystem.Wgw_NT8164A ||
            deviceCategory == DeviceModel.WaterSystem.Wgw_NT8167A{
            //一体式水
            if type == DeviceInforType.code{
                //编码
                return ("设备编码",true)
            }else if type == DeviceInforType.name{
                //名称
                return ("设备名称",true)
            }else if type == DeviceInforType.type{
                //类型
                return ("设备类型",true)
            }else if type == DeviceInforType.manufacturer{
                //生产厂商不可编辑
                return ("生产厂商",false)
            }
        }else if deviceCategory == DeviceModel.WaterSystem.Wgw_NT8167C{
            //一体式水
            if type == DeviceInforType.code{
                //编码
                return ("设备ID",true)
            }else if type == DeviceInforType.name{
                //名称
                return ("设备名称",true)
            }else if type == DeviceInforType.manufacturer{
                //厂商
                return ("生产厂商",false)
            }
        }else if deviceCategory == DeviceModel.SmartElectricitySystem.SmartPowerGateway{
            //智慧用电网关
            if type == DeviceInforType.code{
                //名称
                return ("设备编码",false)
            }else if type == DeviceInforType.name{
                //名称
                return ("设备名称",true)
            }
        }else if deviceCategory == DeviceModel.NBDevice.Probe{
            //NB设备
            if type == DeviceInforType.code{
                //编码  不可编辑
                return ("设备编码",false)
            }else if type == DeviceInforType.name{
                //名称
                return ("设备名称",true)
            }else if type == DeviceInforType.type{
                //类型
                return ("设备类型",false)
            }
        }else if deviceCategory == DeviceModel.SmartElectricitySystem.ElectricGateway_NT8127A ||
                deviceCategory == DeviceModel.SmartElectricitySystem.ElectricGateway_NT8127C ||
                 deviceCategory == DeviceModel.SmartElectricitySystem.ElectricGateway_NT8128A{
            //8127A 8128A
            if type == DeviceInforType.name{
                //名称
                return ("设备名称",true)
            }else if type == DeviceInforType.manufacturer{
                //生产厂商不可编辑
                return ("生产厂商",false)
            }else if type == DeviceInforType.model {
                //设备型号不可编辑
                return ("设备型号",false)
            }
        }else if deviceCategory == DeviceModel.FamilySystem.HomeGateway{
            if type == DeviceInforType.code{
                //编码
                return ("设备编码",true)
            }else if type == DeviceInforType.name{
                //名称
                return ("设备名称",true)
            }
        }else if deviceCategory == DeviceModel.FamilySystem.HomeGatewayDevice{
            if type == DeviceInforType.code{
                //编码
                return ("器件编码",false)
            }else if type == DeviceInforType.name{
                //名称
                return ("器件名称",true)
            }else if type == DeviceInforType.type{
                //类型
                return ("器件类型",false)
            }
        }else if deviceCategory == DeviceModel.GasDetector.HM710NVM || deviceCategory == DeviceModel.GasDetector.WS2CG {
            if type == DeviceInforType.code{
                //编码
                return ("设备编码",true)
            }else if type == DeviceInforType.name{
                //名称
                return ("设备名称",true)
            }
        }else if deviceCategory == DeviceModel.WaterSystem.NT8160A{
            
            if type == DeviceInforType.code{
                //编码
                return ("设备编码",false)
            }else if type == DeviceInforType.name{
                //名称
                return ("设备名称",false)
            }
            
        }else if deviceCategory == DeviceModel.WaterSystem.NT8160A_device{
            
            if type == DeviceInforType.code{
                //编码
                return ("设备编码",false)
            }else if type == DeviceInforType.name{
                //名称
                return ("设备名称",true)
            }else if type == DeviceInforType.usrDeviceType{
                //类型
                return ("设备类型",false)
            }
            
        }
        if type == DeviceInforType.address{
            //安装位置
            return ("安装位置",true)
        }else if type == DeviceInforType.message_protocol{
            //通信协议
            return ("报文协议",true)
        }else if type == DeviceInforType.domain{
            //中心
            return ("所属中心",true)
        }else if type == DeviceInforType.enterprise{
            //企业
            return ("所属企业",true)
        }else if type == DeviceInforType.building{
            //建筑
            return ("所属建筑",true)
        }else if type == DeviceInforType.keypart{
            //部位
            return ("所属部位",true)
        }else if type == DeviceInforType.gis{
            //地图描点
            return ("GIS定位",true)
        }else if type == DeviceInforType.unit{
            //测量单位
            return ("单位",true)
        }else if type == DeviceInforType.signal_type{
            //信号类型
            return ("信号类型",true)
        }else if type == DeviceInforType.connection_type{
            //通讯方式
            return ("联网方式",true)
        }else if type == DeviceInforType.true_mean_type{
            //真(1)值含义
            return ("真(1)值含义",true)
        }else if type == DeviceInforType.false_mean_type{
            //假(0)值含义
            return ("假(0)值含义",true)
        }else if type == DeviceInforType.threshold_lower_limit{
            //阈值下限
            return ("阈值下限",true)
        }else if type == DeviceInforType.threshold_upper_limit{
            //阈值上限
            return ("阈值上限",true)
        }else if type == DeviceInforType.lower_limit_warning{
            //下限报警值
            return ("下限报警值",true)
        }else if type == DeviceInforType.lower_lower_limit_warning{
            //下限报警值
            return ("下下限报警值",true)
        }else if type == DeviceInforType.upper_limit_warning{
            //上限报警值
            return ("上限报警值",true)
        }else if type == DeviceInforType.upper_upper_limit_warning{
            //上上限报警值
            return ("上上限报警值",true)
        }else if type == DeviceInforType.principal{
            //负责人
            return ("负责人",true)
        }else if type == DeviceInforType.contact_number{
            //联系电话
            return ("联系电话",true)
        }else if type == DeviceInforType.ratio_coefficient{
            //变比系数
            return ("变比系数",true)
        }else if type == DeviceInforType.imei{
            //IMEI号
            return ("IMEI",true)
        }else if type == DeviceInforType.imsi{
            //IMSI号
            return ("IMSI",true)
        }else if type == DeviceInforType.sim{
            //SIM卡号
            return ("SIM卡号",false)
        }else if type == DeviceInforType.timeout{
            //上传周期
            return ("上传周期",false)
        }else if type == DeviceInforType.soft_version{
            //软件版本
            return ("软件版本",false)
        }
        
        return ("",false)
    }
    
    class func startEdit() {
        
        changeEditState(dataArr: dataSource, editState: true)
        if currentCategory == DeviceModel.WaterSystem.NeatWaterSingal {
            changeEditState(dataArr: analogDataSource, editState: true)
            changeEditState(dataArr: digitalDataSource, editState: true)
        }
        
    }
    
    class func endEdit() {
        
        changeEditState(dataArr: dataSource, editState: false)
        if currentCategory == DeviceModel.WaterSystem.NeatWaterSingal {
            changeEditState(dataArr: analogDataSource, editState: false)
            changeEditState(dataArr: digitalDataSource, editState: false)
        }
        
    }
    
    /// 变更编辑状态
    /// - Parameters:
    ///   - dataArr: 数据源
    ///   - editState: 变更值
    class func changeEditState(dataArr:Array<Dictionary<String, EditItemModel>>,editState:Bool) {
        for index in 0..<dataArr.count {
            let dic = dataArr[index]
            for item in dic.values{
                item.isEditState = editState
            }
        }
    }
    
    
    /// 更新数据源
    /// - Parameter model: 新的数据model
    class func updataWithModelWithInforType(model:EditItemModel) {
        
        if currentCategory == DeviceModel.WaterSystem.NeatWaterSingal {
            //水信号类型
            if model.inforType == DeviceInforType.signal_type {
                //如果是信号类型变更 变更对应信号类型数据源 如果是相同的数据项同时修改
                if model.inforText == "2" {
                    dataSource.removeAll()
                    dataSource.append(contentsOf: digitalDataSource)
                    
                }else if model.inforText == "1"{
                    dataSource.removeAll()
                    analogDataSource = updateInfor(model: model, dataSource: analogDataSource)
                    dataSource.append(contentsOf: analogDataSource)
                }
                
            }else{
                
                //普通数据变更
                for index in 0..<dataSource.count {
                    let dic = dataSource[index]
                    if dic.keys.contains(model.inforType){
                        
                        var newDic = dic
                        newDic.updateValue(model, forKey: model.inforType)
                        
                        dataSource[index] = newDic
                        
                        //如果设置了 水信号类型 同步变更 单位
                        if model.inforType == DeviceInforType.type {
                            
                            if model.uploadInfor == "1" {
                                //液压
                                dataSource = changeEditItemInforByType(inforType:DeviceInforType.unit,infor: "MPa",targetArr: dataSource)
                                analogDataSource = changeEditItemInforByType(inforType:DeviceInforType.unit,infor: "MPa",targetArr: analogDataSource)
                                digitalDataSource = changeEditItemInforByType(inforType:DeviceInforType.unit,infor: "MPa",targetArr: digitalDataSource)
                                
                            }else if model.uploadInfor == "2"{
                                //液位
                               dataSource = changeEditItemInforByType(inforType:DeviceInforType.unit,infor: "cm",targetArr: dataSource)
                                analogDataSource = changeEditItemInforByType(inforType:DeviceInforType.unit,infor: "cm",targetArr: analogDataSource)
                                digitalDataSource = changeEditItemInforByType(inforType:DeviceInforType.unit,infor: "cm",targetArr: digitalDataSource)
                            }
                            
                        }
                        
                        break
                    }
                }
                
                analogDataSource = updateInfor(model: model, dataSource: analogDataSource)
                
                digitalDataSource = updateInfor(model: model, dataSource: digitalDataSource)
                
            }
            
        }else{
            //普通数据变更
            dataSource = updateInfor(model: model, dataSource: dataSource)
        }
    }
    
    /// 更新数据项
    /// - Parameters:
    ///   - model: 更新数据项
    ///   - dataSource: 数据源
    class func updateInfor(model:EditItemModel,dataSource: Array<Dictionary<String, EditItemModel>>) -> Array<Dictionary<String, EditItemModel>>{
        
        var dataArr = dataSource
        
        for index in 0..<dataSource.count {
            let dic = dataSource[index]
            if dic.keys.contains(model.inforType){
                
                var newDic = dic
                newDic.updateValue(model, forKey: model.inforType)
                
                dataArr[index] = newDic
                break
            }
        }
        
        return dataArr
    }
    
    class func changeEditItemInforByType(inforType:String,infor:String,targetArr:Array<Dictionary<String, EditItemModel>>) -> Array<Dictionary<String, EditItemModel>>{
        
        var dataArr = targetArr
        
        for index in 0..<targetArr.count {
            
            let dic = targetArr[index]
            if dic.keys.contains(inforType){
                
                var newDic = dic
                let model = newDic[inforType]
                model?.uploadInfor = infor
                model?.inforText = infor
                newDic.updateValue(model!, forKey: inforType)
                
                dataArr[index] = newDic
            }
            
        }
        
        return dataArr
        
    }
    
    
    /// 通过展示数据数组 获取上送数据model
    class func getUploadInforModel(success:@escaping (_ response:BaseResponse)->Void,failed:@escaping (_ failed:BaseErrorResponse)->Void){
        
        if currentCategory == DeviceModel.FireSystem.uitd_NT9009{
            //一体化传输装置
            let model = IntegratedTransmissionDeviceInforModel.init().formateTransmissionDeviceInforToModel(dataArr: dataSource)
            model.id = respData.deviceInfor?.id
            
            NeatRequest.instance.editDevice(apiPath: UrlHost.FireUITD.editUITDAPI, jsonParame: model, success: { (response) in
                success(response)
            }) { (error) in
                failed(error)
            }

            
        }else if currentCategory == DeviceModel.FireSystem.IntegratedTransmissionHost{
            //一体化主机
            let model = IntergratedHostDeviceInforModel.init().formateHostDeviceDataToModel(dataArr: dataSource)
            model.id = respData.deviceInfor?.id
            model.deviceId = respData.deviceInfor?.deviceId
            model.enterpriseId = respData.deviceInfor?.enterpriseId
            
            NeatRequest.instance.editDevice(apiPath: UrlHost.FireUITD.editUITDDeviceAPI, jsonParame: model, success: { (response) in
                success(response)
            }) { (error) in
                failed(error)
            }
            
        }else if currentCategory == DeviceModel.WaterSystem.wireless_NT8327{
            //neat水网关
            let model = WaterGatewayInforModel.init().formateWaterGatewayDataToModel(dataArr: dataSource)
            model.id = respData.deviceInfor?.id
            //暂时写死 以后会陆续添加厂商
            model.manufacturer = "4000"
            
            NeatRequest.instance.editDevice(apiPath: UrlHost.WaterDevice.editWaterGatewayAPI, jsonParame: model, success: { (response) in
                success(response)
            }) { (error) in
                failed(error)
            }

        }else if currentCategory == DeviceModel.WaterSystem.NeatWaterSingal{
            //neat水信号
            let model = WaterSingalInforModel.init().formateWaterSignalDataToModel(dataArr: dataSource)
            
            model.id = respData.deviceInfor?.id
            model.deviceId = respData.deviceInfor?.deviceId
            
            NeatRequest.instance.editDevice(apiPath: UrlHost.WaterDevice.editWaterSignalAPI, jsonParame: model, success: { (response) in
                success(response)
            }) { (error) in
                failed(error)
            }
            
        }else if currentCategory == DeviceModel.SmartElectricitySystem.SmartPowerGateway{
            //智慧用电网关
            let model = SmareEleInforModel.init().formateSmartEleToModel(dataArr: dataSource)
            model.id = respData.deviceInfor?.id
        
            NeatRequest.instance.editDevice(apiPath: UrlHost.EleDevice.editElectricGatewayByCodeAPI, jsonParame: model, success: { (response) in
                success(response)
            }) { (error) in
                failed(error)
            }
            
            
        }else if currentCategory == DeviceModel.NBDevice.Probe{
            //NB设备
            let model = NBInforModel.init().formateNBDeviceToModel(dataArr: dataSource)
            model.id = respData.deviceInfor?.id
            
            NeatRequest.instance.editDevice(apiPath: UrlHost.NBDevice.editNBDeviceAPI, jsonParame: model, success: { (response) in
                success(response)
            }) { (error) in
                failed(error)
            }
        
        }else if currentCategory == DeviceModel.SmartElectricitySystem.ElectricGateway_NT8127A ||
            currentCategory == DeviceModel.SmartElectricitySystem.ElectricGateway_NT8127C ||
            currentCategory == DeviceModel.SmartElectricitySystem.ElectricGateway_NT8128A{
            // 8127A 8127C 8128A
            let model = IntegrateEleGateway.init().formateSmartEleToModel(dataArr: dataSource)
            model.id = respData.deviceInfor?.id
            
            NeatRequest.instance.editDevice(apiPath: UrlHost.EleDevice.editIntegratedElectricGatewayApi, jsonParame: model, success: { (response) in
                success(response)
            }) { (error) in
                failed(error)
            }
            
        }else if currentCategory == DeviceModel.WaterSystem.Wgw_NT8161A ||
            currentCategory == DeviceModel.WaterSystem.Wgw_NT8164A ||
            currentCategory == DeviceModel.WaterSystem.Wgw_NT8167A {
            // 拓普索尔
            let model = TpseWaterInforModel.init().formateTpseWaterToModel(dataArr: dataSource)
            model.id = respData.deviceInfor?.id
            
            NeatRequest.instance.editDevice(apiPath: UrlHost.WaterDevice.editWaterSourceAPI, jsonParame: model, success: { (response) in
                success(response)
            }) { (error) in
                failed(error)
            }
            
        }else if currentCategory == DeviceModel.WaterSystem.Wgw_NT8167C{
            // 铭控
            let model = MkInforModel.init().formateMkToModel(dataArr: dataSource)
            model.id = respData.deviceInfor?.id
            
            NeatRequest.instance.editDevice(apiPath: UrlHost.WaterDevice.editWaterSourceAPI, jsonParame: model, success: { (response) in
                success(response)
            }) { (error) in
                failed(error)
            }
        }
        else if currentCategory == DeviceModel.WaterSystem.Wgw_NT8164B ||
            currentCategory == DeviceModel.WaterSystem.Wgw_NT8161B{
            // 万讯
            let model = WxWaterInforModel.init().formateWxWaterToModel(dataArr: dataSource)
            model.id = respData.deviceInfor?.id
            
            NeatRequest.instance.editDevice(apiPath: UrlHost.WaterDevice.editWaterSourceAPI, jsonParame: model, success: { (response) in
                success(response)
            }) { (error) in
                failed(error)
            }
            
        }else if currentCategory == DeviceModel.FamilySystem.HomeGateway{
            ///8140
            let model = HomeGatewayModel.init().formateHomeGatewayDeviceInforToModel(dataArr: dataSource)
            model.id = respData.deviceInfor?.id
            NeatRequest.instance.editDevice(apiPath: UrlHost.HomeGateway.editHomeGatewayAPI, jsonParame: model, success: { (response) in
                success(response)
            }) { (error) in
                failed(error)
            }
            
        }else if currentCategory == DeviceModel.FamilySystem.HomeGatewayDevice{
            ///8140 器件
            let model = HomeGatewayDeviceModel.init().formateHomeGatewayDeviceDataToModel(dataArr: dataSource)
            model.id = respData.deviceInfor?.id
            NeatRequest.instance.editDevice(apiPath: UrlHost.HomeGateway.editHomeGatewayDeviceAPI, jsonParame: model, success: { (response) in
                success(response)
            }) { (error) in
                failed(error)
            }
            
        }else if currentCategory == DeviceModel.GasDetector.HM710NVM || currentCategory == DeviceModel.GasDetector.WS2CG{
            ///海曼可燃气体
            let model = GasDetectorModel.init().formateGasDetectorInforToModel(dataArr: dataSource)
            model.id = respData.deviceInfor?.id
            NeatRequest.instance.editDevice(apiPath: UrlHost.GasDetector.editGasDetectorAPI, jsonParame: model, success: { (response) in
                success(response)
            }) { (error) in
                failed(error)
            }
            
        }else if currentCategory == DeviceModel.WaterSystem.NT8160A {
            ///有人泵房检测
            let model = YSGatewayEditModel.init().formateYSGatewayToModel(dataArr: dataSource)
            model.id = respData.deviceInfor?.id
            let addModel = SupperDeviceEditPublicModel.init()
            addModel.deviceInfor = model
            addModel.isDeviceLevel = DeviceLevel.Gateway
            addModel.model = currentCategory
            
            NeatRequest.instance.postEditDevicePublic(jsonParame: addModel) { (response) in
                success(response)
            } failed: { (error) in
                failed(error)
            }

            
            
        }else if currentCategory == DeviceModel.WaterSystem.NT8160A_device {
            
            let model = YSDeviceEditModel.init().formateYSDeviceToModel(dataArr: dataSource)
            model.id = respData.deviceInfor?.id
            let addModel = SupperDeviceEditPublicModel.init()
            addModel.deviceInfor = model
            addModel.isDeviceLevel = DeviceLevel.Device
            addModel.model = currentCategory
            
            NeatRequest.instance.postEditDevicePublic(jsonParame: addModel) { (response) in
                success(response)
            } failed: { (error) in
                failed(error)
            }
            
        }
    }

}
