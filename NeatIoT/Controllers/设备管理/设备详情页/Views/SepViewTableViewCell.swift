//
//  SepViewTableViewCell.swift
//  NeatIoT
//
//  Created by neat on 2019/12/4.
//  Copyright © 2019 Neat. All rights reserved.
//

import UIKit

class SepViewTableViewCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.selectionStyle = .none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
