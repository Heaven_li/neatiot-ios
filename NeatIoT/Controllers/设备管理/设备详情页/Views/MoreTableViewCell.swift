//
//  MoreTableViewCell.swift
//  NeatIoT
//
//  Created by Yang Blus on 2020/5/25.
//  Copyright © 2020 Neat. All rights reserved.
//

import UIKit


protocol MoreTableViewCellDelegate {
    func didClickMore(isMore:Bool)
}

class MoreTableViewCell: UITableViewCell {

    @IBOutlet weak var bgStackView: UIStackView!
    
    @IBOutlet weak var titleLable: UILabel!
    
    @IBOutlet weak var arrowImageView: UIImageView!
    
    var moreState:Bool = false
    
    var isMoreState: Bool {
        get {
            moreState
        }
        set {
            moreState = newValue
            self.updateUI()
        }
    }
    
    @IBOutlet weak var coverView: UIView!
    
    var delegate:MoreTableViewCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.selectionStyle = .none
        
        let tap = UITapGestureRecognizer.init(target: self, action: #selector(tapMore))
        self.coverView.isUserInteractionEnabled = true
        self.coverView.addGestureRecognizer(tap)
        
    }

    
    @objc func tapMore() {

        isMoreState = !isMoreState

        if isMoreState {
            self.arrowImageView.transform = CGAffineTransform.init(rotationAngle: CGFloat(Double.pi))
            self.titleLable.text = "向上收起"

        }else{
            self.arrowImageView.transform = .identity
            self.titleLable.text = "展开全部"
        }

        self.delegate?.didClickMore(isMore: isMoreState)

    }
    
    func updateUI() {
        if isMoreState {
            self.arrowImageView.transform = CGAffineTransform.init(rotationAngle: CGFloat(Double.pi))
            self.titleLable.text = "向上收起"
            
        }else{
            self.arrowImageView.transform = .identity
            self.titleLable.text = "展开全部"
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
