//
//  DeviceInforTableViewCell.swift
//  NeatIoT
//
//  Created by neat on 2019/12/4.
//  Copyright © 2019 Neat. All rights reserved.
//

import UIKit

protocol DeviceInforTableViewCellDelegate {
    /// 刷新
    func startRefresh(inforModel: DeviceInforViewModel)
    
    /// 点击功能按钮
    func functionBtnClick(inforModel: DeviceInforViewModel)
    
    /// 点击通道
    func chancelClick(inforModel: DeviceInforViewModel)
}

class DeviceInforTableViewCell: UITableViewCell {
    
    var delegate:DeviceInforTableViewCellDelegate?
    
    private var currentInforModel:DeviceInforViewModel?
    
    lazy var inforView: DeviceInforView = {
        let inforView = DeviceInforView.init()
        inforView.delegate = self
        return inforView
    }()

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.addSubview(self.inforView)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func configerCell(inforModel: DeviceInforViewModel) {
        
        currentInforModel = inforModel
        
        if inforModel.deviceCategory == DeviceCategory.FireSystem.IntegratedTransmissionHost ||
            inforModel.deviceCategory == DeviceCategory.FireSystem.IntegratedDevice ||
            inforModel.deviceCategory == DeviceCategory.WaterSystem.NeatWaterSingal ||
            inforModel.deviceCategory == DeviceCategory.SmartElectricitySystem.SmartPowerChanel ||
            inforModel.deviceCategory == DeviceCategory.SmartElectricitySystem.ElectricGateway_NT8127A ||
            inforModel.deviceCategory == DeviceCategory.SmartElectricitySystem.ElectricGateway_NT8128A{
            inforView.frame = CGRect.init(x: 0, y: 0, width: NormalConstant.ScreenWidth, height: 106)
        }else{
            inforView.frame = CGRect.init(x: 0, y: 0, width: NormalConstant.ScreenWidth, height: 160)
        }
        
        inforView.configerUIWithData(inforModel: inforModel)
    }
    
    
    
}

extension DeviceInforTableViewCell:DeviceInforViewDelegate{
    
    func startRefresh() {
        self.delegate?.startRefresh(inforModel: currentInforModel!)
    }
    
    func functionBtnClick() {
        self.delegate?.functionBtnClick(inforModel: currentInforModel!)
    }
    
    func chancelClick() {
        self.delegate?.chancelClick(inforModel: currentInforModel!)
    }
    
    
}
