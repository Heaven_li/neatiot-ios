//
//  EleGatewayContainView.swift
//  NeatIoT
//
//  Created by Yang Blus on 2020/5/25.
//  Copyright © 2020 Neat. All rights reserved.
//

import UIKit

protocol DeviceDetailContainViewDelegate {
    
    /// 暂时用来回掉智慧用电通道点击事件
    func didClickDeviceData(inforModel: DeviceInforViewModel)
    /// 信号量按钮点击
    func didChangeSignalType(model:EditItemModel)
    
    func eleDidSelectItem(model:EditItemModel)
    /// 刷新
    func eleStartRefresh()
    /// 功能按钮点击
    func functionBtnClick(inforModel: DeviceInforViewModel,sender: LoadingButton)
    
}

class DeviceDetailContainView: UIView {
    
    let TextCellIdent = "TextCellIdent"
    let CheckBoxCellIdent = "CheckBoxCellIdent"
    let DeviceInforIdent = "DeviceInforIdent"
    let SepCellIdent = "SepCellIdent"
    let MoreCellIdent = "MoreCellIdent"
    let DataCellIdent = "DataCellIdent"
    
    private var dataSourceArr:Array<Dictionary<String,EditItemModel>> = []
    
    private var inforModel: DeviceInforViewModel?
    
    /// 数据项全部数据项
    private var dataArr:Array<DeviceDataInforModel> = []
    /// 当前展示全部数据项
    private var currentDataArr:Array<DeviceDataInforModel> = []
    /// 设备基础信息model
    private var deviceBasicModel:DeviceBasicInforModel = DeviceBasicInforModel.init()
    
    var delegate:DeviceDetailContainViewDelegate?
    
    lazy var containTableView: LZTableView = {
        
        let tableView = LZTableView.init(frame: CGRect.zero, style:.plain)
        tableView.backgroundColor = UIColor.clear
        tableView.delegate = self
        tableView.dataSource = self
        tableView.bounces = false
        tableView.separatorInset = UIEdgeInsets.init(top: 0, left: 5, bottom: 0, right: 5)
        
        tableView.register(UINib.init(nibName: "DeviceNormalItemTableViewCell", bundle: Bundle.main), forCellReuseIdentifier: self.TextCellIdent)
        tableView.register(UINib.init(nibName: "CheckBoxTableViewCell", bundle: Bundle.main), forCellReuseIdentifier: self.CheckBoxCellIdent)
        tableView.register(UINib.init(nibName: "SepViewTableViewCell", bundle: Bundle.main), forCellReuseIdentifier: self.SepCellIdent)
        tableView.register(UINib.init(nibName: "MoreTableViewCell", bundle: Bundle.main), forCellReuseIdentifier: self.MoreCellIdent)
        tableView.register(UINib.init(nibName: "DeviceDataTableViewCell", bundle: Bundle.main), forCellReuseIdentifier: self.DataCellIdent)
        //DeviceDataInforTableViewCell
        
        tableView.register(UINib.init(nibName: "DeviceStateTableViewCell", bundle: Bundle.main), forCellReuseIdentifier: self.DeviceInforIdent)
        tableView.register(UINib.init(nibName: "DeviceDataInforTableViewCell", bundle: Bundle.main), forCellReuseIdentifier: self.DataCellIdent)
        tableView.register(UINib.init(nibName: "MoreTableViewCell", bundle: Bundle.main), forCellReuseIdentifier: self.MoreCellIdent)
        
        return tableView
    }()

    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.addSubview(self.containTableView)
        self.containTableView.snp.makeConstraints { (make) in
            make.edges.equalTo(self)
        }
        
        self.containTableView.showLoadingView(UIImage.init(named: "device_detail_load_bg"))
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func configerData(dataSource: Array<Dictionary<String,EditItemModel>>,model:DeviceInforViewModel) {
        
        self.containTableView.hidenHolderView()
        
        dataSourceArr = dataSource
        inforModel = model
        
        deviceBasicModel.iconUrl = model.iconURL
        deviceBasicModel.deviceName = model.deviceName
        deviceBasicModel.deviceState = model.deviceState
        deviceBasicModel.functionBtnEnAbleImageName = model.functionBtnEnAbleImageName
        deviceBasicModel.functionBtnAbleImageName = model.functionBtnAbleImageName
        deviceBasicModel.functionBtnAble = model.functionBtnAble
        deviceBasicModel.currentValue = model.realTimeValue
        deviceBasicModel.functionBtnText = model.functionBtnTitleText
    
        self.initData(model: model)
        
        self.containTableView.reloadData()
    }
    /// 初始化设备数据
    func initData(model:DeviceInforViewModel) {
        
        dataArr.removeAll()
        currentDataArr.removeAll()
        
        dataArr.append(contentsOf: model.dataItems ?? [])
    
        self.restDeviceInforData()
        
    }
    /// 重置设备数据源数组
    func restDeviceInforData() {
        
        currentDataArr.removeAll()
        
        for (index,modelItem) in dataArr.enumerated() {
                
            if index <= 3 {
                currentDataArr.append(modelItem)
            }
            
        }
        
    }
    /// 加载全部设备数据
    func loadMoreDeviceInforData() {
        
        currentDataArr.removeAll()
        
        currentDataArr.append(contentsOf: dataArr)
        
    }

    func reloadData(dataSource: Array<Dictionary<String,EditItemModel>>) {
        dataSourceArr = dataSource
        self.containTableView.reloadData()
    }

}

extension DeviceDetailContainView:UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if dataArr.count == 0 {
            if indexPath.section == 0 {
                return NormalConstant.DeviceBasicViewHeight
            }else if indexPath.section == 1 {
                ///分割cell
                return NormalConstant.DeviceInforContainSepHeight
            }else{
                return 50
            }
        }else if dataArr.count <= 4 {
            if indexPath.section == 0 {
                return NormalConstant.DeviceBasicViewHeight
            }else if indexPath.section == 1 {
                ///数据项
                if currentDataArr.count%4 == 0 {
                    return CGFloat(currentDataArr.count/4) * NormalConstant.DeviceDataViewHeight
                }else{
                    return CGFloat(currentDataArr.count/4 + 1) * NormalConstant.DeviceDataViewHeight
                }
                
            }else if indexPath.section == 2 {
                ///分割cell
                return NormalConstant.DeviceInforContainSepHeight
            }else{
                return 50
            }
        }else{
            if indexPath.section == 0 {
                return NormalConstant.DeviceBasicViewHeight
            }else if indexPath.section == 1 {
                ///数据项
                if currentDataArr.count%4 == 0 {
                    return CGFloat(currentDataArr.count/4) * NormalConstant.DeviceDataViewHeight
                }else{
                    return CGFloat(currentDataArr.count/4 + 1) * NormalConstant.DeviceDataViewHeight
                }
                
            }else if indexPath.section == 2 {
                ///more cell
                return NormalConstant.DeviceInforContainMoreHeight
            }else if indexPath.section == 3 {
                ///分割cell
                return NormalConstant.DeviceInforContainSepHeight
            }else{
                return 50
            }
        }
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        
        return 0.1
     
    }
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView.init()
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        if dataArr.count == 0 {
            return 3
        }else if dataArr.count <= 4 {
            //不需要显示展开
            return 4
        }else{
            return 5
        }
        
    }
    
}
extension DeviceDetailContainView:UITableViewDataSource{
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if dataArr.count == 0 {
            if section == 2 {
                return dataSourceArr.count
            }else{
                return 1
            }
        }else if dataArr.count <= 4 {
            if section == 3 {
                return dataSourceArr.count
            }else{
                return 1
            }
        }else{
            if section == 4 {
                return dataSourceArr.count
            }else if section == 1{
                return 1
            }else{
                return 1
            }
        }
        
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if dataArr.count == 0 {
            
            if indexPath.section == 0 {
                                        
                let cell = tableView.dequeueReusableCell(withIdentifier: DeviceInforIdent, for: indexPath) as!DeviceStateTableViewCell
                cell.selectionStyle = .none
                cell.delegate = self
                cell.configerCell(model: deviceBasicModel)
                return cell
                        
            }else if indexPath.section == 1{
                        
                let cell = tableView.dequeueReusableCell(withIdentifier: SepCellIdent, for: indexPath) as!SepViewTableViewCell
                cell.selectionStyle = .none
                return cell
                
            }else{
                        
                let model = dataSourceArr[indexPath.row]
                var editModel = EditItemModel()
                if model.values.count != 0 {
                    for item in model.values {
                        editModel = item
                    }
                }
                
                if editModel.editType == ItemEditStyle.RadioBtnStyle {
                    let cell = tableView.dequeueReusableCell(withIdentifier: CheckBoxCellIdent, for: indexPath) as!CheckBoxTableViewCell
                    cell.selectionStyle = .none
                    cell.configerCell(itemDic:dataSourceArr[indexPath.row])
                    cell.delegate = self
                    return cell
                }else{
                    let cell = tableView.dequeueReusableCell(withIdentifier: TextCellIdent, for: indexPath) as!DeviceNormalItemTableViewCell
                    cell.selectionStyle = .none
                    cell.delegate = self
                    cell.configerCell(itemDic: dataSourceArr[indexPath.row])
                    return cell
                }
                
            }
            
        }else if dataArr.count <= 4 {
            
            if indexPath.section == 0 {
                                        
                let cell = tableView.dequeueReusableCell(withIdentifier: DeviceInforIdent, for: indexPath) as!DeviceStateTableViewCell
                cell.selectionStyle = .none
                cell.delegate = self
                cell.configerCell(model: deviceBasicModel)
                return cell
                        
            }else if indexPath.section == 1{
                
                let cell = tableView.dequeueReusableCell(withIdentifier: DataCellIdent, for: indexPath) as! DeviceDataInforTableViewCell
                cell.delegate = self
                cell.configerCell(models: currentDataArr)
                cell.selectionStyle = .none
                return cell
                
            }else if indexPath.section == 2{
                
                let cell = tableView.dequeueReusableCell(withIdentifier: SepCellIdent, for: indexPath) as!SepViewTableViewCell
                cell.selectionStyle = .none
                return cell
                
            }else{
                        
                let model = dataSourceArr[indexPath.row]
                var editModel = EditItemModel()
                if model.values.count != 0 {
                    for item in model.values {
                        editModel = item
                    }
                }
                
                if editModel.editType == ItemEditStyle.RadioBtnStyle {
                    let cell = tableView.dequeueReusableCell(withIdentifier: CheckBoxCellIdent, for: indexPath) as!CheckBoxTableViewCell
                    cell.selectionStyle = .none
                    cell.configerCell(itemDic:dataSourceArr[indexPath.row])
                    cell.delegate = self
                    return cell
                }else{
                    let cell = tableView.dequeueReusableCell(withIdentifier: TextCellIdent, for: indexPath) as!DeviceNormalItemTableViewCell
                    cell.selectionStyle = .none
                    cell.delegate = self
                    cell.configerCell(itemDic: dataSourceArr[indexPath.row])
                    return cell
                }
                
            }
            
        }else{
            
            if indexPath.section == 0 {
                                
                    let cell = tableView.dequeueReusableCell(withIdentifier: DeviceInforIdent, for: indexPath) as!DeviceStateTableViewCell
                    cell.selectionStyle = .none
                    cell.delegate = self
                    cell.configerCell(model: deviceBasicModel)
                    return cell
                                
            }else if indexPath.section == 1{
                
                let cell = tableView.dequeueReusableCell(withIdentifier: DataCellIdent, for: indexPath) as! DeviceDataInforTableViewCell
                cell.delegate = self
                cell.configerCell(models: currentDataArr)
                cell.selectionStyle = .none
                return cell
                
            }else if indexPath.section == 2{
                
                let cell = tableView.dequeueReusableCell(withIdentifier: MoreCellIdent, for: indexPath) as! MoreTableViewCell
                cell.delegate = self
                
                if self.currentDataArr.count <= 4 {
                    cell.isMoreState = false
                }
                
                cell.selectionStyle = .none
                return cell
                
            }else if indexPath.section == 3{
                
                let cell = tableView.dequeueReusableCell(withIdentifier: SepCellIdent, for: indexPath) as!SepViewTableViewCell
                cell.selectionStyle = .none
                return cell
                
            }else{
                        
                let model = dataSourceArr[indexPath.row]
                var editModel = EditItemModel()
                if model.values.count != 0 {
                    for item in model.values {
                        editModel = item
                    }
                }
                
                if editModel.editType == ItemEditStyle.RadioBtnStyle {
                    let cell = tableView.dequeueReusableCell(withIdentifier: CheckBoxCellIdent, for: indexPath) as!CheckBoxTableViewCell
                    cell.selectionStyle = .none
                    cell.configerCell(itemDic:dataSourceArr[indexPath.row])
                    cell.delegate = self
                    return cell
                }else{
                    let cell = tableView.dequeueReusableCell(withIdentifier: TextCellIdent, for: indexPath) as!DeviceNormalItemTableViewCell
                    cell.selectionStyle = .none
                    cell.delegate = self
                    cell.configerCell(itemDic: dataSourceArr[indexPath.row])
                    return cell
                }
                
            }
            
        }
        
        
    }
    
    
    
}

extension DeviceDetailContainView:CheckBoxTableViewCellDelegate{
    
    func checkCellDidChange(model: EditItemModel) {
        self.delegate?.didChangeSignalType(model: model)
    }
    

}

extension DeviceDetailContainView:DeviceDataInforTableViewCellDelegate{
    
    func didClickDataItem(model: DeviceDataInforModel) {
        if model.titleText == "通道数量" || model.titleText == "器件数"{
            self.delegate?.didClickDeviceData(inforModel: inforModel!)
        }
    }

}

extension DeviceDetailContainView:DeviceNormalItemTableViewCellDelegate{
    
    func inputItemByModel(model: EditItemModel) {
        self.delegate?.eleDidSelectItem(model: model)
    }

}

extension DeviceDetailContainView:DeviceStateTableViewCellDelegate{
    
    
    func refreshClick() {
        self.delegate?.eleStartRefresh()
    }
    
    func functionClick(sender: LoadingButton) {
        self.delegate?.functionBtnClick(inforModel: inforModel!,sender: sender)
    }
    
    
    
}

extension DeviceDetailContainView:MoreTableViewCellDelegate {
    
    func didClickMore(isMore: Bool) {
        
        if isMore {
            
            self.loadMoreDeviceInforData()
            
            let indexSet = IndexSet.init(integer: 1)
            
            self.containTableView.reloadSections(indexSet, with: .automatic)
        }else{
            
            self.restDeviceInforData()
            
            let indexSet = IndexSet.init(integer: 1)
            
            self.containTableView.reloadSections(indexSet, with: .automatic)
        }
        
    }

}
