//
//  GISViewController.swift
//  NeatIoT
//
//  Created by neat on 2019/11/25.
//  Copyright © 2019 Neat. All rights reserved.
//

import UIKit
import MAMapKit
import AMapSearchKit

protocol GISViewControllerDelegate {
    func didFinishGISMark(model:EditItemModel)
}


class GISViewController: SupperViewController {
    
    var curretnEidtItemModel:EditItemModel!
    
    /// 是否是编辑状态
    var isEditState:Bool = false
    
    var delegate:GISViewControllerDelegate?
    
    lazy var backBtn: UIButton = {
        let backBtn = UIButton.init(type: .custom)
        backBtn.setImage(UIImage.init(named: "gis_back"), for: .normal)
        backBtn.addTarget(self, action: #selector(backBtnClick), for: .touchUpInside)
        return backBtn
    }()
    
    lazy var finishBtn: UIButton = {
        let finishBtn = UIButton.init(type: .custom)
        finishBtn.setTitle("完成", for: .normal)
        finishBtn.setTitleColor(.black, for: .normal)
        finishBtn.titleLabel?.font = UIFont.systemFont(ofSize: 16)
        finishBtn.backgroundColor = UIColor.white
        finishBtn.addTarget(self, action: #selector(finishBtnClick), for: .touchUpInside)
        finishBtn.layer.cornerRadius = 5
        return finishBtn
    }()
    
    lazy var mapView: MAMapView = {
        let mapView = MAMapView.init(frame: self.view.bounds)
        mapView.delegate = self
        mapView.showsCompass = false
        mapView.userTrackingMode = .follow
        mapView.maxZoomLevel = 18
        mapView.minZoomLevel = 15
        return mapView
    }()
    
    lazy var mapSearch: AMapSearchAPI = {
        
        let search = AMapSearchAPI()
        search!.delegate = self
        return search!
        
    }()
    
    lazy var markPoint: UIImageView = {
        let point = UIImageView.init(image: UIImage.init(named: "mark_point"), highlightedImage: nil)
        return point
    }()
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.navigationController?.navigationBar.isHidden = true
        
        if !isEditState {
            
            self.finishBtn.isHidden = true
            self.markPoint.isHidden = true
            
            let pointAnnotation = MAPointAnnotation()
            
            if (!(self.curretnEidtItemModel.latitude?.isEmpty ?? true) || !(self.curretnEidtItemModel.longitude?.isEmpty ?? true)){
                
                let center = CLLocationCoordinate2D(latitude: Double(self.curretnEidtItemModel.latitude!)!, longitude: Double(self.curretnEidtItemModel.longitude!)!)
                
                pointAnnotation.coordinate = center
                mapView.setCenter(center, animated: true)
                
            }
            
            mapView.addAnnotation(pointAnnotation)
            
        }else{
            self.markPoint.isHidden = false;
            self.finishBtn.isHidden = false;
        }
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = false
    }
    
    
    override var preferredStatusBarStyle: UIStatusBarStyle{
        .default
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        AMapServices.shared()?.enableHTTPS = true
        
        self.view.addSubview(self.mapView)
        
        self.view.addSubview(self.backBtn)
        
        self.view.addSubview(self.finishBtn)
        
        self.view.addSubview(self.markPoint)
        
        if NormalConstant.ISLHP {
            self.backBtn.snp.makeConstraints { (make) in
                make.leading.equalTo(self.view).offset(20)
                make.top.equalTo(self.view).offset(30+20)
            }
            
            self.finishBtn.snp.makeConstraints { (make) in
                make.trailing.equalTo(self.view).offset(-20)
                make.top.equalTo(self.view).offset(30+20)
                make.width.equalTo(50)
            }
        }else{
            self.backBtn.snp.makeConstraints { (make) in
                make.leading.equalTo(self.view).offset(20)
                make.top.equalTo(self.view).offset(30)
            }
            
            self.finishBtn.snp.makeConstraints { (make) in
                make.trailing.equalTo(self.view).offset(-20)
                make.top.equalTo(self.view).offset(30)
                make.width.equalTo(50)
            }
        }
        
        
        self.markPoint.snp.makeConstraints { (make) in
            make.centerX.equalTo(self.mapView.snp.centerX)
            make.centerY.equalTo(self.mapView.snp.centerY).offset(-23.4)
        }

    }
    
    @objc func backBtnClick() {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func finishBtnClick() {
        let coordinate = self.mapView.centerCoordinate
        
        let latitude = coordinate.latitude
        
        let longitude = coordinate.longitude
        
        let latitudeStr = String.init(latitude)
        
        let longitudeStr = String.init(longitude)
        
    
        let request = AMapReGeocodeSearchRequest()
        request.location = AMapGeoPoint.location(withLatitude: CGFloat(coordinate.latitude), longitude: CGFloat(coordinate.longitude))
        request.requireExtension = true
        
        self.mapSearch.aMapReGoecodeSearch(request)
        
        self.curretnEidtItemModel.latitude = latitudeStr
        self.curretnEidtItemModel.longitude = longitudeStr
        
    }
    
}

extension GISViewController:AMapSearchDelegate{
    
    
    /// 成功
    /// - Parameters:
    ///   - request: <#request description#>
    ///   - response: <#response description#>
    func onReGeocodeSearchDone(_ request: AMapReGeocodeSearchRequest!, response: AMapReGeocodeSearchResponse!) {
        
        if response.regeocode == nil {
            return
        }
    
        
        //市
        let city = response.regeocode.addressComponent.city ?? ""
        //区
        let district = response.regeocode.addressComponent.district ?? ""
        //乡镇街道
        let township = response.regeocode.addressComponent.township ?? ""
        //街道名称
        let street = response.regeocode.addressComponent.streetNumber.street ?? ""
        //街道号
        let streetNumber = response.regeocode.addressComponent.streetNumber.number ?? ""
        
        let reGeocodeAddress = city + district + township + street + streetNumber
        
        debugPrint(reGeocodeAddress)
        
        self.curretnEidtItemModel.uploadInfor = reGeocodeAddress
        
        self.delegate?.didFinishGISMark(model: self.curretnEidtItemModel)
        self.navigationController?.popViewController(animated: true)
        
    }
    
    /// 检索失败
    /// - Parameters:
    ///   - request: <#request description#>
    ///   - error: <#error description#>
    func aMapSearchRequest(_ request: Any!, didFailWithError error: Error!) {
        
        
        let nsError = error as NSError?
        
//        NSLog("Error:\(error) - \(ErrorInfoUtility.errorDescription(withCode: (nsError?.code)!))")
//
//        debugPrint(nsError?.userInfo.values)
        
        NeatHud.showMessage(message: ErrorInfoUtility.errorDescription(withCode: (nsError?.code)!), view: self.view)
        
    }
    
    
    
    
}
extension GISViewController:MAMapViewDelegate{
    
    func mapView(_ mapView: MAMapView!, viewFor annotation: MAAnnotation!) -> MAAnnotationView! {
        
//        注意：5.1.0后由于定位蓝点增加了平滑移动功能，如果在开启定位的情况先添加annotation，需要在此回调方法中判断annotation是否为MAUserLocation，从而返回正确的View。
        if (annotation.isKind(of: MAUserLocation.self)) {
           return nil;
        }else if annotation.isKind(of: MAPointAnnotation.self) {
            let pointReuseIndetifier = "pointReuseIndetifier"
            var annotationView: MAAnnotationView? = mapView.dequeueReusableAnnotationView(withIdentifier: pointReuseIndetifier)
            
            if annotationView == nil {
                annotationView = MAAnnotationView(annotation: annotation, reuseIdentifier: pointReuseIndetifier)
            }
            
            annotationView!.image = UIImage(named: "mark_point")
            //设置中心点偏移，使得标注底部中间点成为经纬度对应点
            annotationView!.centerOffset = CGPoint.init(x: 0, y: -18);
            
            return annotationView!
        }
        
        return nil
    }
}



