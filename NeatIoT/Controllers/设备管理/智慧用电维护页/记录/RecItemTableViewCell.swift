//
//  RecItemTableViewCell.swift
//  NeatIoT
//
//  Created by neat on 2019/12/6.
//  Copyright © 2019 Neat. All rights reserved.
//

import UIKit

class RecItemTableViewCell: UITableViewCell {

    @IBOutlet weak var leftLable: UILabel!
    @IBOutlet weak var midLable: UILabel!
    @IBOutlet weak var rightLable: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configerCell(model:SmartGatewayTaskInforModel) {
        
        leftLable.text = model.type
        if model.isSuccesed{
            midLable.textColor = #colorLiteral(red: 0.3019607843, green: 0.7490196078, blue: 0.3411764706, alpha: 1)
        }else{
            midLable.textColor = #colorLiteral(red: 0.968627451, green: 0.5019607843, blue: 0.5098039216, alpha: 1)
        }
        midLable.text = model.detailInfro
        rightLable.text = model.endTime
        
        
    }
    
    
    
}
