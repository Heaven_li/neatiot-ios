//
//  SmartGatewayTaskInforModel.swift
//  NeatIoT
//
//  Created by neat on 2019/12/6.
//  Copyright © 2019 Neat. All rights reserved.
//

import UIKit
import HandyJSON
class SmartGatewayTaskInforModel: NSObject,HandyJSON {
    
    /// 类型
    var type:String = ""
    /// 开始时间
    var startTime:String = ""
    /// 结束时间
    var endTime:String = ""
    /// 是否成功
    var isSuccesed:Bool = false
    /// 记录内容
    var detailInfro:String = ""
    
    required override init() {
        
    }

    func mapping(mapper: HelpingMapper) {
        mapper <<<
        self.detailInfro <-- "description"
        mapper <<<
        self.isSuccesed <-- "finished"
    }

}
