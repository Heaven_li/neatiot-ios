//
//  SmartGatewayRecViewController.swift
//  NeatIoT
//
//  Created by neat on 2019/12/6.
//  Copyright © 2019 Neat. All rights reserved.
//

import UIKit

class SmartGatewayRecViewController: SupperViewController {
    
    let cellIdent = "RecItemCell"
    
    var deviceInforModel:DeviceInforViewModel?
    
    private var dataSource:Array<SmartGatewayTaskInforModel> = []

    @IBOutlet weak var tableView: UITableView!

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.titleName = "运维记录"
        
        self.tableView.dataSource = self
        self.tableView.delegate = self
        
        self.tableView.register(UINib.init(nibName: "RecItemTableViewCell", bundle: Bundle.main), forCellReuseIdentifier: cellIdent)
        
        getData()
        
    }
    
    func getData() {
        
        NeatRequest.instance.getSmartGatewayRec(id: deviceInforModel!.deviceID, success: { (response) in
            
            self.dataSource = response.inforArr ?? []
            self.tableView.reloadData()
            
        }) { (error) in
            
            NeatHud.showMessage(message: error.errMessage, view: self.view)
        }
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension SmartGatewayRecViewController:UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 46
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 46
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let view = UIView.init()
        view.backgroundColor = NormalColor.viewControllerDefaultBgColor
        
        let kindLable = UILabel.init()
        kindLable.text = "类别"
        kindLable.textColor = #colorLiteral(red: 0.2586170137, green: 0.2463852763, blue: 0.2367299497, alpha: 1)
        kindLable.font = UIFont.systemFont(ofSize: 15)
        
        
        let resultLable = UILabel.init()
        resultLable.text = "结果"
        resultLable.textColor = #colorLiteral(red: 0.2586170137, green: 0.2463852763, blue: 0.2367299497, alpha: 1)
        resultLable.font = UIFont.systemFont(ofSize: 15)
        
        let timeLable = UILabel.init()
        timeLable.text = "完成时间"
        timeLable.textColor = #colorLiteral(red: 0.2586170137, green: 0.2463852763, blue: 0.2367299497, alpha: 1)
        timeLable.font = UIFont.systemFont(ofSize: 15)
        
        let stackView = UIStackView.init(frame: CGRect.init(x: 10, y: 0, width: NormalConstant.ScreenWidth - 20, height: 53))
        stackView.axis = .horizontal
        stackView.alignment = .center
        stackView.distribution = .equalSpacing
        stackView.spacing = 5
        
        stackView.addArrangedSubview(kindLable)
        stackView.addArrangedSubview(resultLable)
        stackView.addArrangedSubview(timeLable)
        
        view.addSubview(stackView)
        
        stackView.snp.makeConstraints { (make) in
            make.leading.equalTo(view.snp.leading).offset(10)
            make.trailing.equalTo(view.snp.trailing).offset(-10)
            make.top.equalTo(view.snp.top)
            make.bottom.equalTo(view.snp.bottom)
        }
        
        kindLable.snp.makeConstraints { (make) in
            make.width.equalTo(64)
        }
        timeLable.snp.makeConstraints { (make) in
            make.width.equalTo(146)
        }
        
        return view
        
    }
    
}

extension SmartGatewayRecViewController:UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        dataSource.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdent, for: indexPath) as! RecItemTableViewCell
        cell.selectionStyle = .none
        cell.configerCell(model: dataSource[indexPath.row])
        return cell
    }
    
}
