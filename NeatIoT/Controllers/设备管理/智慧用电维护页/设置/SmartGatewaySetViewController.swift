//
//  SmartGatewaySetViewController.swift
//  NeatIoT
//
//  Created by neat on 2019/12/6.
//  Copyright © 2019 Neat. All rights reserved.
//

import UIKit

class SmartGatewaySetViewController: SupperViewController {

    let NormalSetTableViewCellIdent = "NormalSetTableViewCell"
    let ChanleSetTableViewCellIdent = "ChanleSetTableViewCell"
    
    var deviceInforModel:DeviceInforViewModel?
    
    var currentEditModel:ChanleSetModel?
    
    var currentEditIndexPath:IndexPath?
    
    var dataSourceArr:Array<ChanleSetModel> = []

    @IBOutlet weak var saveBtn: UIButton!
    @IBOutlet weak var containTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.titleName = "网关设置"
        
        self.containTableView.separatorStyle = .none
        self.containTableView.register(UINib.init(nibName: "NormalSetTableViewCell", bundle: Bundle.main), forCellReuseIdentifier: NormalSetTableViewCellIdent)
        self.containTableView.register(UINib.init(nibName: "ChanleSetTableViewCell", bundle: Bundle.main), forCellReuseIdentifier: ChanleSetTableViewCellIdent)
        self.containTableView.delegate = self
        self.containTableView.dataSource = self
        
        
        getChanleInfor()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow(notify:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHiden(notify:)), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    func getChanleInfor() {
        
        NeatRequest.instance.getSmartGatewayChanleSetInfor(gatewayId: self.deviceInforModel!.deviceID, success: { (response) in
            
            debugPrint(response)
            self.dataSourceArr.append(contentsOf: response.gatewayInfor?.componets ?? [])
            let model = ChanleSetModel()
            model.uperLimitValue = String(response.gatewayInfor?.timeOut ?? 0)
            self.dataSourceArr.insert(model, at: 0)
            
            self.containTableView.reloadData()
            
        }) { (error) in
            NeatHud.showMessage(message: error.errMessage, view: self.view)
        }
        
    }
    @IBAction func saveBtnClick(_ sender: Any) {
        
        let upModel = UpdateChanleSet().formateUploadModelWithData(dataSource: dataSourceArr)
        upModel.gatewayId = self.deviceInforModel?.deviceID
        
        NeatRequest.instance.postSmartGatewayUpdateChanleInfor(model: upModel, success: { (response) in
            NeatHud.showMessage(message: response.message ?? "保存成功", view: self.view)
        }) { (error) in
            NeatHud.showMessage(message: error.errMessage, view: self.view)
        }
        
    }
    
    var firstY:CGFloat = 0
    @objc func keyboardWillShow(notify:Notification) {
        
        
        let beginFrame = notify.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as! CGRect
        
        let endFrame = notify.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as! CGRect
        
        let time = notify.userInfo?[UIResponder.keyboardAnimationDurationUserInfoKey] as! TimeInterval
        
        let animation = notify.userInfo?[UIResponder.keyboardAnimationCurveUserInfoKey] as! Int
        
        var y = beginFrame.origin.y - endFrame.origin.y
        
        if !self.isEditing {
        
            firstY = y
            
        }else{
            
            y = firstY + y
            firstY = y
        }
        
        
        let yOffSet = self.containTableView.contentOffset.y
        
        let coverRect = CGRect.init(x: 0, y: endFrame.origin.y+yOffSet, width: endFrame.size.width, height: endFrame.size.height)
        
        let coverIndexPathArr = containTableView.indexPathsForRows(in: coverRect)
        
        if (coverIndexPathArr?.contains(currentEditIndexPath!) ?? false) {
            
            UIView.animate(withDuration: time, animations: {
                
                self.containTableView.transform = CGAffineTransform.init(translationX: 0, y: -y)
                
                UIView.setAnimationCurve(UIView.AnimationCurve.init(rawValue: animation)!)
                
                
            }) { (finish) in
                self.containTableView.becomeFirstResponder()
                

            }
            
        }
        
    
    }
    
    @objc func keyboardWillHiden(notify:Notification) {
        
        let time = notify.userInfo?[UIResponder.keyboardAnimationDurationUserInfoKey] as! TimeInterval
        
        let animation = notify.userInfo?[UIResponder.keyboardAnimationCurveUserInfoKey] as! Int
        
        UIView.animate(withDuration: time, animations: {
            
            self.containTableView.transform = .identity
            
            UIView.setAnimationCurve(UIView.AnimationCurve.init(rawValue: animation)!)
            
        }) { (finish) in
        
            
        }
    }

}

extension SmartGatewaySetViewController:UITableViewDelegate{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if indexPath.row == 0 {
            return 45
        }else{
            return 45 * 3
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        debugPrint(dataSourceArr[indexPath.row])
    }
    
}

extension SmartGatewaySetViewController:UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSourceArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let model = dataSourceArr[indexPath.row]
        
        if indexPath.row == 0 {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: NormalSetTableViewCellIdent, for: indexPath) as! NormalSetTableViewCell
            cell.delegate = self
            cell.configerCell(model: model,indexPath: indexPath)
            cell.selectionStyle = .none
            return cell
            
        }else{
            
            let cell = tableView.dequeueReusableCell(withIdentifier: ChanleSetTableViewCellIdent, for: indexPath) as! ChanleSetTableViewCell
            cell.delegate = self
            cell.configerCell(model: model,indexPath: indexPath)
            cell.selectionStyle = .none
            return cell
            
        }
    }
}

extension SmartGatewaySetViewController:NormalSetTableViewCellDelegate{
    func shouldBeginEdit(model: ChanleSetModel, indexPath: IndexPath) {
        currentEditModel = model
        currentEditIndexPath = indexPath
    }
    
    func didFinishEdit(model: ChanleSetModel, indexPath: IndexPath) {
        dataSourceArr[indexPath.row] = model
        self.containTableView.reloadData()
       
    }
    
}

extension SmartGatewaySetViewController:ChanleSetTableViewCellDelegate{
    

    func willBeginEdit(model: ChanleSetModel, indexPath: IndexPath) {
        currentEditModel = model
        currentEditIndexPath = indexPath
    }

    func finishedEdit(model: ChanleSetModel, indexPath: IndexPath) {

        dataSourceArr[indexPath.row] = model
        self.containTableView.reloadData()
    }
    
    
}

