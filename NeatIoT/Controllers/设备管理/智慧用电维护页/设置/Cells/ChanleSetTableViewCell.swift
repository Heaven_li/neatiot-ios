//
//  ChanleSetTableViewCell.swift
//  NeatIoT
//
//  Created by neat on 2019/12/9.
//  Copyright © 2019 Neat. All rights reserved.
//

import UIKit

protocol ChanleSetTableViewCellDelegate {
    
    func willBeginEdit(model:ChanleSetModel, indexPath:IndexPath)
    func finishedEdit(model:ChanleSetModel, indexPath:IndexPath)
}

class ChanleSetTableViewCell: UITableViewCell {
    
    @IBOutlet weak var chanleTitleLable: UILabel!
    
    @IBOutlet weak var chanleUnitLable: UILabel!
    
    @IBOutlet weak var upperTitleLable: UILabel!
    
    @IBOutlet weak var upperInputTextField: UITextField!
    
    @IBOutlet weak var lowerTitleLable: UILabel!
    
    @IBOutlet weak var lowerInputTextField: UITextField!
    
    var delegate:ChanleSetTableViewCellDelegate?
    
    private var currentModel:ChanleSetModel?
    
    private var currentIndexPath:IndexPath?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.upperInputTextField.delegate = self
        self.upperInputTextField.tag = 10000
        self.lowerInputTextField.delegate = self
        self.lowerInputTextField.tag = 10001
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func configerCell(model:ChanleSetModel,indexPath:IndexPath) {
        
        currentIndexPath = indexPath
        currentModel = model
        
        self.chanleTitleLable.text = "通道"+(model.code ?? "")
        //3温度 9电流 128漏电流
        if model.analogueType == 3 {
            self.chanleUnitLable.text = "单位：温度(ºC)"
        }else if model.analogueType == 9{
            self.chanleUnitLable.text = "单位：电流(A)"
        }else if model.analogueType == 128{
            self.chanleUnitLable.text = "单位：漏电流(mA)"
        }
        
        self.upperInputTextField.text = model.uperLimitValue
        
        self.lowerInputTextField.text = model.lowerLimitValue
    }
    
}

extension ChanleSetTableViewCell:UITextFieldDelegate{
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if textField.tag == 10000 {
            currentModel?.uperLimitValue = textField.text
        }else{
            currentModel?.lowerLimitValue = textField.text
        }
        textField.resignFirstResponder()
        
        self.delegate?.finishedEdit(model: currentModel!,indexPath: currentIndexPath!)
        
        return true
    }
    
    
    
    
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == self.upperInputTextField {
            self.delegate?.willBeginEdit(model: currentModel!,indexPath: currentIndexPath!)
        }else{
            self.delegate?.willBeginEdit(model: currentModel!,indexPath: currentIndexPath!)
        }
        return true
    }
    
}
