//
//  NormalSetTableViewCell.swift
//  NeatIoT
//
//  Created by neat on 2019/12/9.
//  Copyright © 2019 Neat. All rights reserved.
//

import UIKit

protocol NormalSetTableViewCellDelegate {
    func shouldBeginEdit(model:ChanleSetModel,indexPath:IndexPath)
    func didFinishEdit(model:ChanleSetModel,indexPath:IndexPath)
}

class NormalSetTableViewCell: UITableViewCell {

    @IBOutlet weak var titleLable: UILabel!
    
    @IBOutlet weak var inputTextField: UITextField!
    
    var delegate:NormalSetTableViewCellDelegate?
    
    private var currentModel:ChanleSetModel?
    
    private var currentIndexPath:IndexPath?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.inputTextField.delegate = self
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configerCell(model:ChanleSetModel,indexPath:IndexPath) {
        
        currentIndexPath = indexPath
        currentModel = model
        self.inputTextField.text = model.uperLimitValue
    }
    
}
extension NormalSetTableViewCell:UITextFieldDelegate{
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        currentModel?.uperLimitValue = textField.text
        self.delegate!.didFinishEdit(model:currentModel!,indexPath:currentIndexPath!)
        return true
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        self.delegate!.shouldBeginEdit(model:currentModel!,indexPath:currentIndexPath!)
        return true
    }
    
}
