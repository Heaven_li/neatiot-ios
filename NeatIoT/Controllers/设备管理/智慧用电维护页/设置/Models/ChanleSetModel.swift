//
//  ChanleSetModel.swift
//  NeatIoT
//
//  Created by neat on 2019/12/10.
//  Copyright © 2019 Neat. All rights reserved.
//

import UIKit
import HandyJSON
/// 智慧用电设置通道设置model
class ChanleSetModel: NSObject,Encodable,HandyJSON {
    
    /// 通道号
    var code:String?
    
    /// 通道类型 3温度 9电流 128漏电流
    var analogueType:Int?
    
    /// 通道ID
    var id:String?
    
    /// 上限值
    var uperLimitValue:String?
    
    /// 下限值
    var lowerLimitValue:String?
    
    required override init() {
        
    }
}
