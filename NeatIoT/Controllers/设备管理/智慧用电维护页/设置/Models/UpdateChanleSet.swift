//
//  UpdateChanleSet.swift
//  NeatIoT
//
//  Created by neat on 2019/12/10.
//  Copyright © 2019 Neat. All rights reserved.
//

import UIKit


class UpdateChanleSet:NSObject,Encodable{
    
    /// 上传频率
    var timeout:String?
    /// 网关id
    var gatewayId:String?
    /// 通道信息数组
    var channels:Array<ChanleSetModel> = []
    
    func formateUploadModelWithData(dataSource:Array<ChanleSetModel>) -> UpdateChanleSet {
        
        let updateModel:UpdateChanleSet = UpdateChanleSet()
        
        var channelArr:Array<ChanleSetModel> = []
        
        for index in 0..<dataSource.count {
            let model = dataSource[index]
            if index == 0 {
                updateModel.timeout = model.uperLimitValue
            }else{
                channelArr.append(dataSource[index])
            }
        }
        
        updateModel.channels.append(contentsOf: channelArr)
        
        return updateModel
        
    }
    
    

}



