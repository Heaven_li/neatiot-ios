//
//  SmartGatewayRecoverViewController.swift
//  NeatIoT
//
//  Created by neat on 2019/12/6.
//  Copyright © 2019 Neat. All rights reserved.
//

import UIKit

class SmartGatewayRecoverViewController: SupperViewController {
    
    var deviceInforModel:DeviceInforViewModel?

    @IBOutlet weak var recoverBtn: UIButton!
    
    @IBOutlet weak var codeLable: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.titleName = "网关复位"
        
        self.recoverBtn.backgroundColor = #colorLiteral(red: 0.968627451, green: 0.5019607843, blue: 0.5098039216, alpha: 1)
        self.recoverBtn.layer.cornerRadius = 5
        
        self.codeLable.text = deviceInforModel?.deviceCode

        // Do any additional setup after loading the view.
    }


    @IBAction func corverBtnClick(_ sender: Any) {
        
        NeatRequest.instance.postSmartGatewayRest(deviceId: self.deviceInforModel!.deviceID, success: { (response) in
            NeatHud.showMessage(message: response.message ?? "复位成功", view: self.view)
        }) { (error) in
            NeatHud.showMessage(message: error.errMessage, view: self.view)
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
