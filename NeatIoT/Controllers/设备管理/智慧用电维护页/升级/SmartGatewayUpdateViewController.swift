//
//  SmartGatewayUpdateViewController.swift
//  NeatIoT
//
//  Created by neat on 2019/12/6.
//  Copyright © 2019 Neat. All rights reserved.
//

import UIKit

class SmartGatewayUpdateViewController: SupperViewController {
    
    var deviceInforModel:DeviceInforViewModel?
    
    var dataSourceArr:Array<UpdateFileInforItem> = []
    
    var versionViews:Array<RadioCheckBox> = []
    
    var selectFileID:String = ""

    @IBOutlet weak var containStackView: UIStackView!
    
    @IBOutlet weak var updateBtn: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.titleName = "版本升级"
        
        self.updateBtn.layer.cornerRadius = 5
        
        getVersionList()
        
    }
    
    func configerVersionView() {
        
        let version_num = dataSourceArr.count
        
        if version_num == 0 {
            return
        }
        
        let line_num = version_num / 2
        //如果版本数 是二的倍数
        for index in 0..<line_num {
            
            let firstItemIndex = index * 2
            let secondItemIndex = firstItemIndex + 1
            
            let lineContainStackView = UIStackView.init()
            lineContainStackView.axis = .horizontal
            lineContainStackView.alignment = .center
            lineContainStackView.distribution = .fillEqually
            
            let firstCheckBtn = RadioCheckBox.init()
            firstCheckBtn.titleText = dataSourceArr[firstItemIndex].versionName
            firstCheckBtn.tag = firstItemIndex
            firstCheckBtn.delegate = self
            firstCheckBtn.checkState = false
            
            let secondCheckBtn = RadioCheckBox.init()
            secondCheckBtn.titleText = dataSourceArr[secondItemIndex].versionName
            secondCheckBtn.tag = secondItemIndex
            secondCheckBtn.delegate = self
            secondCheckBtn.checkState = false
            
            lineContainStackView.addArrangedSubview(firstCheckBtn)
            lineContainStackView.addArrangedSubview(secondCheckBtn)
            
            firstCheckBtn.snp.makeConstraints { (make) in
                make.height.equalTo(30)
            }
            secondCheckBtn.snp.makeConstraints { (make) in
                make.height.equalTo(30)
            }
            
            versionViews.append(firstCheckBtn)
            versionViews.append(secondCheckBtn)
            
            self.containStackView.addArrangedSubview(lineContainStackView)
            
            lineContainStackView.snp.makeConstraints { (make) in
                make.width.equalTo(self.containStackView)
            }
        }
        
        if version_num%2 != 0 {
            
            let lineContainStackView = UIStackView.init()
            lineContainStackView.axis = .horizontal
            lineContainStackView.alignment = .center
            lineContainStackView.distribution = .fillEqually
            
            let firstCheckBtn = RadioCheckBox.init()
            firstCheckBtn.titleText = dataSourceArr[version_num-1].versionName
            firstCheckBtn.tag = version_num-1
            firstCheckBtn.delegate = self
            firstCheckBtn.checkState = false
            
            lineContainStackView.addArrangedSubview(firstCheckBtn)
            
            firstCheckBtn.snp.makeConstraints { (make) in
                make.height.equalTo(30)
            }
            
            versionViews.append(firstCheckBtn)
            
            self.containStackView.addArrangedSubview(lineContainStackView)
            
            lineContainStackView.snp.makeConstraints { (make) in
                make.width.equalTo(self.containStackView)
            }
            
        }
        
        let defaultSeletRadio = versionViews[0]
        defaultSeletRadio.checkState = true
        self.selectFileID = dataSourceArr[0].softwareId
        
    
        
    }
    
    /// 更新软件版本 选中样式
    /// - Parameters:
    ///   - tag: radioBtn tag
    func updateCheckbox(tag:Int) {
        
        for radioBtn in versionViews {
            
            if radioBtn.tag == tag {
                self.selectFileID = dataSourceArr[tag].softwareId
                radioBtn.checkState = true
            }else{
                radioBtn.checkState = false
            }
            
        }
    
    }
    
    func getVersionList() {
        
        NeatRequest.instance.getSmartGatewayUpgradeFile(success: { (response) in
            
            self.dataSourceArr.append(contentsOf: response.version_arr ?? [])
            self.configerVersionView()
            
        }) { (error) in
            NeatHud.showMessage(message: error.errMessage, view: self.view)
        }
    }
    
    func postGatewayUpgrad() {
        NeatRequest.instance.postSmartGatewayUpgrade(gatewayId: deviceInforModel!.deviceID, fileId:selectFileID, success: { (response) in
            
            NeatHud.showMessage(message: response.message!, view: self.view)
            
        }) { (error) in
            NeatHud.showMessage(message: error.errMessage, view: self.view)
        }
    }
    

    @IBAction func updateBtnClick(_ sender: Any) {
        
        postGatewayUpgrad()
        
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension SmartGatewayUpdateViewController:RadioCheckBoxDelegate{
    func checkBoxStateChanged(isSelect: Bool, view: RadioCheckBox) {
        
        self.updateCheckbox(tag: view.tag)
        
    }
    
    
}
