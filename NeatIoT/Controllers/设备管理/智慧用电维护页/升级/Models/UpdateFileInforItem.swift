//
//  UpdateFileInforItem.swift
//  NeatIoT
//
//  Created by neat on 2019/12/9.
//  Copyright © 2019 Neat. All rights reserved.
//

import UIKit
import HandyJSON

class UpdateFileInforItem: NSObject,HandyJSON {
    
    var softwareId:String = ""
    
    var versionName:String = ""
    
    var datetime:String = ""
    
    required override init() {
        
    }
    
    func mapping(mapper: HelpingMapper) {
        
        mapper <<<
        self.softwareId <-- "id"
        mapper <<<
        self.versionName <-- "version"
        mapper <<<
        self.datetime <-- "datetime"
        
    }
    

}
