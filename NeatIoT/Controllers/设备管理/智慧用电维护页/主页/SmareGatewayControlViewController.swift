//
//  SmareGatewayControlViewController.swift
//  NeatIoT
//
//  Created by neat on 2019/12/6.
//  Copyright © 2019 Neat. All rights reserved.
//

import UIKit

class SmareGatewayControlViewController: SupperViewController {
    
    var deviceInforModel:DeviceInforViewModel?

    @IBOutlet weak var recoverItem: UIView!
    
    @IBOutlet weak var setItem: UIView!
    
    @IBOutlet weak var updateItem: UIView!
    
    @IBOutlet weak var recodeItem: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.titleName = "相关操作"

        // Do any additional setup after loading the view.
    }

    @IBAction func recoverTap(_ sender: Any) {
        debugPrint("复位")
        
        let recover = SmartGatewayRecoverViewController.init()
        recover.deviceInforModel = self.deviceInforModel
        self.navigationController?.pushViewController(recover, animated: true)
    }
    @IBAction func setTap(_ sender: Any) {
        debugPrint("设置")
        let setVC = SmartGatewaySetViewController.init()
        setVC.deviceInforModel = self.deviceInforModel
        self.navigationController?.pushViewController(setVC, animated: true)
    }
    @IBAction func updateTap(_ sender: Any) {
        debugPrint("升级")
        let updataVC = SmartGatewayUpdateViewController.init()
        updataVC.deviceInforModel = self.deviceInforModel
        self.navigationController?.pushViewController(updataVC, animated: true)
    }
    @IBAction func recordTap(_ sender: Any) {
        debugPrint("记录")
        
        let recVC = SmartGatewayRecViewController.init()
        recVC.deviceInforModel = self.deviceInforModel
        self.navigationController?.pushViewController(recVC, animated: true)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
