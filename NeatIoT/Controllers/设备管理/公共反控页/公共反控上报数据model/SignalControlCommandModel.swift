//
//  SignalControlCommandModel.swift
//  NeatIoT
//
//  Created by Yang Blus on 2020/10/27.
//  Copyright © 2020 Neat. All rights reserved.
//

import UIKit

class SignalControlCommandModel: NSObject,Encodable {
    
    ///设备model
    var model:String = ""
    
    ///设备级别：1：网关，2：主机，3：器件
    var isDeviceLevel:Int = -1
    
    /// 接受命令网关或器件或主机ID
    var equipmentID:String = ""
    
    ///指令类型
    ///设置 = 1
    ///命令 = 2
    ///适用于所有设备
    var commandType:Int = -1
    
    ///指令值
    ///自检 = 1
    ///启动 = 2
    ///停止 = 3
    ///适用于所有设备
    var commandValue:Int = -1
    
    

}
