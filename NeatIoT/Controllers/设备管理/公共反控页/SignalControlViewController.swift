//
//  SignalControlViewController.swift
//  NeatIoT
//
//  Created by Yang Blus on 2020/10/14.
//  Copyright © 2020 Neat. All rights reserved.
//

import UIKit

enum ControlCommandType {
    ///消音
    case Selence
    ///复位
    case Reset
    ///分闸
    case Floodgate
    ///自检
    case SelfCheck
    
}


/// 消音 复位 自检等反控操作共用页
class SignalControlViewController: SupperViewController {

    @IBOutlet weak var desLable: UILabel!
    
    @IBOutlet weak var codeLable: UILabel!
    
    @IBOutlet weak var confirmBtn: UIButton!
    
    /// 设备信息model
    public var inforModel: DeviceInforViewModel?
    
    /// 描述文字
    public var desStr: String = ""
    
    /// 导航栏标题
    public var navTitle: String = ""
    
    /// 控制类型
    public var commandType: ControlCommandType?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.confirmBtn.layer.cornerRadius = 5
        self.confirmBtn.clipsToBounds = true
        
        self.titleName = navTitle
        
        self.view.backgroundColor = .white
        
        let confirmTitle = "确认" + navTitle
        
        self.confirmBtn.setTitle(confirmTitle, for: .normal)
        
        self.desLable.text = self.desStr;
        
        if inforModel?.deviceCategory == DeviceModel.SmartElectricitySystem.ElectricGateway_NT8127C {
            //航天长兴
            let titleStr = "当前设备:"
            let codeStr = self.inforModel!.deviceIMEI
            let deviceCodeStr = titleStr + codeStr
            
            let attrStr = NSMutableAttributedString.init(string: deviceCodeStr)
            attrStr.addAttributes([.foregroundColor:#colorLiteral(red: 0.3537780334, green: 0.8005358108, blue: 0.2051676415, alpha: 1)], range: NSRange.init(location: titleStr.count, length: codeStr.count))
            
            self.codeLable.attributedText = attrStr
            
        }
        
    }

    @IBAction func confirmBtnClick(_ sender: Any) {
        
        var apiUrl = ""
        let reportModel = CommandReportModel.init();
        reportModel.id = inforModel?.deviceID
        if self.commandType == .Selence {
            //消音
            apiUrl = UrlHost.EleDevice.electricSelenceCommandAPI
        }else if self.commandType == .Reset {
            //复位
            apiUrl = UrlHost.EleDevice.electricResetCommandAPI
        }
        if apiUrl.isEmpty {
            return
        }
        
        NeatHud.showLoading(view: self.view, message: "")
        
        NeatRequest.instance.postCommandPublic(url: apiUrl, inforModel: reportModel) { (response) in
            NeatHud.showMessage(message: response.message ?? "指令下发成功", view: self.view)
        } failed: { (error) in
            NeatHud.showMessage(message: error.errMessage , view: self.view)
        }
        
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
