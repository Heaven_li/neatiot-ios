//
//  CommandReportModel.swift
//  NeatIoT
//
//  Created by Yang Blus on 2020/10/14.
//  Copyright © 2020 Neat. All rights reserved.
//

import UIKit

class CommandReportModel: NSObject,Encodable {
    
    /// 设备ID
    var id:String?
    /// 清除分闸0 保持分闸1
    var param:Int?

}
