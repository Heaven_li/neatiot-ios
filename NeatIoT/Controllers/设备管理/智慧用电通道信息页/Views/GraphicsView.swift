//
//  GraphicsView.swift
//  NeatIoT
//
//  Created by neat on 2019/12/10.
//  Copyright © 2019 Neat. All rights reserved.
//

import UIKit

class GraphicsView: UIView {
    
    let bgCircleColor = #colorLiteral(red: 0.8867290616, green: 0.8874035478, blue: 0.8868335485, alpha: 1).cgColor
    let insideArcNormalColor = #colorLiteral(red: 0.4024794102, green: 0.8632317185, blue: 0.4634534717, alpha: 1).cgColor
    let insideArcWarningColor = #colorLiteral(red: 0.9859903455, green: 0.4849994183, blue: 0.4872213602, alpha: 1).cgColor
    let centerCircelColor = #colorLiteral(red: 0.3254901961, green: 0.7176470588, blue: 1, alpha: 1).cgColor
    let clockTextColor = #colorLiteral(red: 0.2235294118, green: 0.2, blue: 0.1882352941, alpha: 1)
    let bgCircleWidth:CGFloat = 8
    let insideARCWidth:CGFloat = 12
    let centerCircelScale:CGFloat = 0.05
    let typeLableWidthScale:CGFloat = 0.43
    
    ///view 的中心点
    var centerX:CGFloat?
    var centerY:CGFloat?
    var centerPoint:CGPoint?
    ///view size
    var viewHeight:CGFloat?
    var viewWidth:CGFloat?
    
    /// 背景圆环半径
    var bgCircleRadio:CGFloat?
    /// 量程 及 刻度圆环半径
    var scaleCircleRadio:CGFloat?
    /// 中心指针圆点半径
    var centerCircleRadio:CGFloat?
    
    private var channelValueModel:ChannelValueModel?
    
    
    /// 类型lable
    lazy var typeLable: UILabel = {
        let lable = UILabel.init()
        lable.textAlignment = .center
        lable.textColor = self.clockTextColor
        lable.font = UIFont.systemFont(ofSize: 15)
        return lable
    }()
    /// 单位lable
    lazy var unitLable: UILabel = {
        let lable = UILabel.init()
        lable.textAlignment = .center
        lable.textColor = self.clockTextColor
        lable.font = UIFont.systemFont(ofSize: 12)
        return lable
    }()
    /// 最小量程lable
    lazy var minLable: UILabel = {
        let lable = UILabel.init()
        lable.textAlignment = .left
        lable.textColor = self.clockTextColor
        lable.font = UIFont.systemFont(ofSize: 10)
        return lable
    }()
    /// 最大量程lable
    lazy var maxLable: UILabel = {
        let lable = UILabel.init()
        lable.textAlignment = .right
        lable.textColor = self.clockTextColor
        lable.font = UIFont.systemFont(ofSize: 10)
        return lable
    }()
    /// 当前值lable
    lazy var valueLable: UILabel = {
        let lable = UILabel.init()
        lable.textAlignment = .center
        lable.textColor = self.clockTextColor
        lable.font = UIFont.boldSystemFont(ofSize: 16)
        return lable
    }()
    
    lazy var pointImageView: UIImageView = {
        let imageView = UIImageView.init(image: UIImage.init(named: "point"))
        return imageView
    }()
    
    override func layoutSubviews() {
        initData(rect: self.bounds)
        drawBgCircle()
        drawGraphArc()
        drawScaleLine()
        configerText()
        drawPointer()
        drawCenterCircle()
    }
    
    func initData(rect:CGRect) {
        
        centerX = rect.size.width/2.0
        centerY = rect.size.height/2.0
        viewWidth = rect.size.width
        viewHeight = rect.size.height
        
        centerPoint = CGPoint.init(x: centerX!, y: centerY!)
        bgCircleRadio = (viewWidth! - bgCircleWidth*CGFloat(2))/2.0
        scaleCircleRadio = bgCircleRadio! - bgCircleWidth/2.0 - insideARCWidth/2.0
        centerCircleRadio = viewWidth! * centerCircelScale
        
    }
    
    
    /// 画最外面的圆环
    func drawBgCircle() {
        
        let startAngle:CGFloat = 0.0
        let endAngle:CGFloat = CGFloat(Double.pi*2.0)
        
        let circlePath = UIBezierPath.init(arcCenter: centerPoint!, radius: bgCircleRadio!, startAngle: startAngle, endAngle: endAngle, clockwise: true)
    
        let shapeLayer = CAShapeLayer.init()
        shapeLayer.lineWidth = bgCircleWidth
        shapeLayer.strokeColor = bgCircleColor
        shapeLayer.fillColor = UIColor.white.cgColor
        shapeLayer.path = circlePath.cgPath
        
        self.layer.addSublayer(shapeLayer)
        
        
    }
    
    /// 画量程圆弧
    func drawGraphArc() {
        
        let startAngle:CGFloat = CGFloat((Double.pi/4.0)*3.0)
        let endAngle:CGFloat = CGFloat(Double.pi/4.0)
        
        //将圆分为8份 每份45度
        //画一个270度 圆弧 从135度开始
        
        if channelValueModel != nil {
            
            let upperLimit = channelValueModel!.upperLimitValue!
            let maxValue = channelValueModel!.maxValue!
            let minValue = channelValueModel!.minValue!
            
            let rangeValue = abs(maxValue) + abs(minValue)
            let normalRate = upperLimit/CGFloat(rangeValue)
            
            let normalAngle = CGFloat(CGFloat((Double.pi/4.0)*6.0) * normalRate)
            
            ///正常量程弧度
            let normalStartAngle:CGFloat = CGFloat(startAngle)
            let normalEndAngle:CGFloat = CGFloat(normalAngle + startAngle)
            ///报警量程弧度
            let warningStartAngle:CGFloat = CGFloat(normalAngle + startAngle)
            let warningEndAngle:CGFloat = endAngle
            
            let normalCirclePath = UIBezierPath.init(arcCenter: centerPoint!, radius: scaleCircleRadio!, startAngle: normalStartAngle, endAngle: normalEndAngle, clockwise: true)
            
            let warningCirclePath = UIBezierPath.init(arcCenter: centerPoint!, radius: scaleCircleRadio!, startAngle: warningStartAngle, endAngle: warningEndAngle, clockwise: true)
            
            let normalShapeLayer = CAShapeLayer.init()
            normalShapeLayer.lineWidth = insideARCWidth
            normalShapeLayer.strokeColor = insideArcNormalColor
            normalShapeLayer.fillColor = UIColor.clear.cgColor
            normalShapeLayer.path = normalCirclePath.cgPath
            
            let warningShapeLayer = CAShapeLayer.init()
            warningShapeLayer.lineWidth = insideARCWidth
            warningShapeLayer.strokeColor = insideArcWarningColor
            warningShapeLayer.fillColor = UIColor.clear.cgColor
            warningShapeLayer.path = warningCirclePath.cgPath
            
            self.layer.addSublayer(normalShapeLayer)
            
            self.layer.addSublayer(warningShapeLayer)
            
        }
        
    }
    
    /// 画表盘刻度线
    func drawScaleLine() {
        

        let defaultStartAngle = Double.pi/2.0 + Double.pi/4.0*1.5
        let totalAngle = Double.pi + Double.pi/4
        let spaceAngle = totalAngle/80.0
        
        for index in 0..<81 {
            
            var startAngle:Double = 0
            var endAngle:Double = 0
            
            if index%10 == 0 {
    
                startAngle = defaultStartAngle + spaceAngle*Double(index)
                endAngle = startAngle + Double.pi/150
                
            }else{
                
                startAngle = defaultStartAngle + spaceAngle*Double(index)
                endAngle = startAngle + Double.pi/200
            }
            
            let scaleLinePath = UIBezierPath.init(arcCenter: centerPoint!, radius: scaleCircleRadio!, startAngle: CGFloat(startAngle), endAngle: CGFloat(endAngle), clockwise: true)
            
            let scaleShape = CAShapeLayer.init()
            scaleShape.strokeColor = UIColor.white.cgColor
            scaleShape.fillColor = UIColor.black.cgColor
            scaleShape.path = scaleLinePath.cgPath
            if index%10 == 0 {
                scaleShape.lineWidth = insideARCWidth
            }else{
                scaleShape.lineWidth = insideARCWidth*0.6
            }
        
            self.layer.addSublayer(scaleShape)
            
            
        }

    }
    
    /// 画中心圆点
    func drawCenterCircle() {
        
        let path = UIBezierPath.init(arcCenter: centerPoint!, radius: centerCircleRadio!, startAngle: 0, endAngle: CGFloat(Double.pi*2), clockwise: true)
        
        let centerCircelShape = CAShapeLayer.init()
        centerCircelShape.path = path.cgPath
        centerCircelShape.fillColor = centerCircelColor
        
        self.layer.addSublayer(centerCircelShape)
        
        
    }
    
    func configerText() {
        
        //类型
        
        let height = scaleCircleRadio! - insideARCWidth/2.0 - centerCircleRadio!
        
        let typeLableCenterY = centerY! - height/2.0 - centerCircleRadio!
        let unitLableCenterY = centerY! + height/4.0 + centerCircleRadio!
        let valueLableCenterY = unitLableCenterY + height/2.0 + insideARCWidth/2.0
        
        let typeLableCenterX = centerX
        let width = viewWidth!*typeLableWidthScale
        
        /// 总长
        let totalWidth = scaleCircleRadio!/CGFloat(sin(Double.pi/4)) - insideARCWidth
        let minCenterX = centerX! - totalWidth/4.0
        let maxCenterX = centerX! + totalWidth/4.0
        let minCenterY = centerY! + totalWidth/2.0
        let maxCenterY = centerY! + totalWidth/2.0
        
        self.typeLable.frame = CGRect.init(x: 0, y: 0, width: width, height: height)
        self.typeLable.center = CGPoint.init(x: typeLableCenterX!, y: typeLableCenterY)
        
        self.unitLable.frame = CGRect.init(x: 0, y: 0, width: 60, height: height/2.0)
        self.unitLable.center = CGPoint.init(x: typeLableCenterX!, y: unitLableCenterY)
        
        self.valueLable.frame = CGRect.init(x: 0, y: 0, width: 60, height: height/2.0)
        self.valueLable.center = CGPoint.init(x: typeLableCenterX!, y: valueLableCenterY)
        
        self.minLable.frame = CGRect.init(x: 0, y: 0, width: totalWidth/2.0, height: 10)
        self.minLable.center = CGPoint.init(x: minCenterX, y: minCenterY)
        
        self.maxLable.frame = CGRect.init(x: 0, y: 0, width: totalWidth/2.0, height: 10)
        self.maxLable.center = CGPoint.init(x: maxCenterX, y: maxCenterY)
        

        self.addSubview(self.typeLable)
        
        //最小 最大量程
        self.addSubview(self.minLable)
        self.addSubview(self.maxLable)
        
        //单位
        self.addSubview(self.unitLable)
    
        //当前值
        
        self.addSubview(self.valueLable)
  
    }
    
    func drawPointer() {
        
        self.pointImageView.layer.anchorPoint = CGPoint.init(x: 0.5, y: 0.8)
        self.pointImageView.center = centerPoint!
        self.pointImageView.sizeToFit()
        
        self.addSubview(self.pointImageView)
        
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.backgroundColor = .white
        
        
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func configerDataModel(model:ChannelValueModel) {
        
        channelValueModel = model
        
        if self.channelValueModel?.valueType == 3 {
            //温度
            self.typeLable.text = "温度"
            self.unitLable.text = "℃"
            
        }else if self.channelValueModel?.valueType == 9 {
            //电流
            self.typeLable.text = "电流"
            self.unitLable.text = "A"
        }else if self.channelValueModel?.valueType == 128 {
            //漏电流
            self.typeLable.text = "漏电流"
            self.unitLable.text = "mA"
        }
        
        self.minLable.text = String.init(format: "%d", self.channelValueModel!.minValue!)
        self.maxLable.text = String.init(format: "%d", self.channelValueModel!.maxValue!)
        self.valueLable.text = String.init(format: "%.1f", self.channelValueModel!.value!)
        
        
        let currentValue = abs(channelValueModel!.value!) + CGFloat(abs(channelValueModel!.minValue!))
        let minValue = channelValueModel?.minValue
        let maxValue = channelValueModel?.maxValue
        
        let totalValue = abs(minValue!) + abs(maxValue!)
        
        let rate = currentValue/CGFloat(totalValue)
        
        let angle = Double.pi/4*6*Double(rate)
        
        let rotationAngle = angle - Double.pi/4.0 * 3
        self.pointImageView.transform = CGAffineTransform.init(rotationAngle: CGFloat(rotationAngle))
        
        let animation = CAKeyframeAnimation.init(keyPath: "transform.rotation.z")
        animation.values = [NSNumber.init(value: Double.pi/4*5),
                                         NSNumber.init(value: Double.pi/4*5 + angle/2),
                                                       NSNumber.init(value: Double.pi/4*5 + angle)]
        animation.keyTimes = [0, 0.3, 0.8]
        animation.duration = 1
        self.pointImageView.layer.add(animation, forKey: "rotation")

    }


}
