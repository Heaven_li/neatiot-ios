//
//  ChannelCollectionViewCell.swift
//  NeatIoT
//
//  Created by neat on 2019/12/10.
//  Copyright © 2019 Neat. All rights reserved.
//

import UIKit

class ChannelCollectionViewCell: UICollectionViewCell {
    
    let customTextColor = #colorLiteral(red: 0.4588235294, green: 0.4588235294, blue: 0.4588235294, alpha: 1)
    
    lazy var channelLable: UILabel = {
        let lable = UILabel.init()
        lable.textColor = self.customTextColor
        lable.font = UIFont.systemFont(ofSize: 15)
        lable.textAlignment = .left
        return lable
    }()
    
    lazy var updateLable: UILabel = {
        let lable = UILabel.init()
        lable.textColor = self.customTextColor
        lable.numberOfLines = 2
        lable.font = UIFont.systemFont(ofSize: 12)
        lable.textAlignment = .left
        lable.adjustsFontSizeToFitWidth = true
        lable.minimumScaleFactor = 0.5
        return lable
    }()
    
    lazy var graphicsView: GraphicsView = {
        let view = GraphicsView.init()
        return view
    }()

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.addSubview(self.channelLable)
        self.addSubview(self.graphicsView)
        self.addSubview(self.updateLable)
        
        self.channelLable.snp.makeConstraints { (make) in
            make.top.equalTo(self).offset(6)
            make.leading.equalTo(self).offset(6)
            make.trailing.equalTo(self).offset(-8)
            make.height.equalTo(21)
        }
        
        self.graphicsView.snp.makeConstraints { (make) in
            make.leading.equalTo(self.snp.leading).offset(10)
            make.trailing.equalTo(self.snp.trailing).offset(-10)
            make.height.equalTo(self.graphicsView.snp.width)
            make.centerY.equalTo(self)
        }
        
        self.updateLable.snp.makeConstraints { (make) in
            make.top.equalTo(self.graphicsView.snp.bottom).offset(6)
            make.bottom.equalTo(self).offset(-6)
            make.leading.equalTo(self).offset(6)
            make.trailing.equalTo(self).offset(-8)
            
        }
        
    }
    
    override func draw(_ rect: CGRect) {
        
        let path = UIBezierPath.init(roundedRect: rect, byRoundingCorners: .allCorners, cornerRadii: CGSize.init(width: 5, height: 5))
        
        let maskLayer = CAShapeLayer.init()
        maskLayer.path = path.cgPath
        
        self.layer.mask = maskLayer
        
    }
    
    func configerCell(model:ChannelValueModel) {
        self.channelLable.text = "通道"+(model.code ?? "1")
        self.updateLable.text = "更新时间:" + (model.datetime ?? "未知")
        self.graphicsView.configerDataModel(model: model)
    }

}
