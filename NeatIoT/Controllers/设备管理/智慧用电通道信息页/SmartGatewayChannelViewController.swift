//
//  SmartGatewayChannelViewController.swift
//  NeatIoT
//
//  Created by neat on 2019/12/10.
//  Copyright © 2019 Neat. All rights reserved.
//

import UIKit

class SmartGatewayChannelViewController: SupperViewController {
    
    let itemVHSpace:CGFloat = 5
    
    let ChannelCollectionViewCellIdent = "ChannelCollectionViewCell"
    
    var deviceInforModel:DeviceInforViewModel?
    
    var dataSourceArr:Array<ChannelValueModel> = []
    
    @IBOutlet weak var containCollectionView: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.titleName = "通道实时数据"
        
        containCollectionView.register(UINib.init(nibName: "ChannelCollectionViewCell", bundle: Bundle.main), forCellWithReuseIdentifier: ChannelCollectionViewCellIdent)
        
        containCollectionView.delegate = self
        containCollectionView.dataSource = self
        
        
        getData()
        
    }
    
    func getData() {
        NeatRequest.instance.getSmartGatewayChanleValueInfor(gatewayId: self.deviceInforModel!.deviceID, success: { (response) in
            
            self.dataSourceArr.append(contentsOf: response.dataArr ?? [])
            self.containCollectionView.reloadData()
            
        }) { (error) in
            NeatHud.showMessage(message: error.errMessage, view: self.view)
        }
    }


}

extension SmartGatewayChannelViewController:UICollectionViewDelegate{
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let itemWidth = (NormalConstant.ScreenWidth - itemVHSpace * 3)/2.0
        
        return CGSize.init(width: itemWidth, height: itemWidth * 1.3)
        
    }
    
}

extension SmartGatewayChannelViewController:UICollectionViewDataSource{
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.dataSourceArr.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let item = collectionView.dequeueReusableCell(withReuseIdentifier: ChannelCollectionViewCellIdent, for: indexPath) as! ChannelCollectionViewCell
        
        item.configerCell(model: dataSourceArr[indexPath.item])
        
        return item
    }
    
}

extension SmartGatewayChannelViewController:UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return itemVHSpace
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets.init(top: itemVHSpace, left: itemVHSpace, bottom: itemVHSpace, right: itemVHSpace)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat{
        return itemVHSpace
    }

}
