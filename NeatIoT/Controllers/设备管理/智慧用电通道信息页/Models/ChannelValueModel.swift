//
//  ChannelValueModel.swift
//  NeatIoT
//
//  Created by neat on 2019/12/10.
//  Copyright © 2019 Neat. All rights reserved.
//

import UIKit
import HandyJSON

/// 智慧用电通道 实时数据 model
class ChannelValueModel: NSObject,HandyJSON {
    
    /// 通道号
    var code:String?
    
    /// 通道类型 3温度 9电流 128漏电流
    var valueType:Int?
    
    /// 通道ID
    var id:String?
    
    /// 上限值
    var upperLimitValue:CGFloat?
    
    /// 下限值
    var lowerLimitValue:CGFloat?
    
    /// 最大
    var maxValue:Int?
    
    /// 最小
    var minValue:Int?
    
    /// 当前值
    var value:CGFloat?
    
    /// 更新时间
    var datetime:String?
    
    override required init() {
        
    }
    

}
