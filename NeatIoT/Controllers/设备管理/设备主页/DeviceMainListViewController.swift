//
//  DeviceListViewController.swift
//  NeatIoT
//
//  Created by neat on 2019/10/29.
//  Copyright © 2019 Neat. All rights reserved.
//

import UIKit

class DeviceMainListViewController: SupperViewController {
    
    /// 设备菜单model
    var menuModel:MenuModel?
    /// 获取列表接口
    var getListApi:String = ""
    /// 搜索关键词
    var keywords:String = ""
    /// 筛选中心id
    var domainId:String = ""
    /// 筛选企业id
    var enterpriseId:String = ""
    /// 筛选建筑id
    var buildingId:String = ""
    /// 筛选部位id
    var keypartId:String = ""
    /// 当前页
    var pageIndex:Int = 1
    /// 当前编辑的筛选项目
    var currentFilterType = FilterType.DomainId
    /// 当前筛选项目数组
    var currentFilterArr:Array<FilterItemModel> = []
    /// 当前编辑的筛选item
    var currentFilterModel:FilterItemModel = FilterItemModel()
    
    lazy var topSearchView: TopSearchView = {
        let topSearchView = TopSearchView.init()
        topSearchView.delegate = self
        return topSearchView
    }()
    
    lazy var listContainView: DeviceMainListContainView = {
        let containView = DeviceMainListContainView.init()
        containView.delegate = self
        return containView
    }()
    
    lazy var filterView: FilterView = {
        
        let filterView = FilterView.init()
        filterView.delegate = self
        filterView.dataSource = FilterViewModel.initPartFilterData()
        
        return filterView
    }()
    
    lazy var inforSelectView: InforSelectView = {
        let inforSelectView = InforSelectView.init()
        inforSelectView.delegate = self
        return inforSelectView
    }()
    
    override func viewDidLoad() {
        
        super.viewDidLoad()

        self.title = menuModel?.name
        
        let addItem = NavItemModel.init()
        addItem.itemIcon = "nav_add"
        
        let filterItem = NavItemModel.init()
        filterItem.itemIcon = "nav_filter"
        
        self.rightItems = [filterItem,addItem]
        
        configerUI()
        
        let loginResult = self.getUserInfor
    
        if nil != loginResult {
            getDeviceList(domainId: loginResult!.user_domain_id, entId: "", buildingId: "", keypartId: "",keyword: "")
        }
        
    }
    
    @objc func notRefreshListData() {
        let loginResult = self.getUserInfor
        
        if nil != loginResult {
            getDeviceList(domainId: loginResult!.user_domain_id, entId: "", buildingId: "", keypartId: "",keyword: "")
        }
    }
    
    func configerUI() {
        
        self.view.addSubview(self.topSearchView)
        self.view.addSubview(self.listContainView)
        
        self.topSearchView.snp.makeConstraints { (make) in
            make.top.left.right.equalTo(self.view)
            make.height.equalTo(NormalConstant.TopSearchViewHeight)
        }
        
        self.listContainView.snp.makeConstraints { (make) in
            make.top.equalTo(self.topSearchView.snp.bottom)
            make.left.right.equalTo(self.view)
            make.bottom.equalTo(self.view)
        }
        
    }
    
    /// 获取设备列表
    func getDeviceList(domainId:String,entId:String,buildingId:String,keypartId:String,keyword:String) {
        
        ContainViewModel.domainId = domainId
        ContainViewModel.enterpriseId = entId
        ContainViewModel.buildingId = buildingId
        ContainViewModel.keypartId = keypartId
        ContainViewModel.keywords = keyword
        ContainViewModel.pageIndex = 1
        
        ContainViewModel.getDeviceList(menuModel: self.menuModel!, containView: self.listContainView)
    
    }
    
    /// 添加
    override func navRLClick() {
        
        let model = ContainViewModel.getDeviceCategoryByMenuModel(menuModel: self.menuModel!)
        
        if model.deviceCategory == DeviceModel.SmartElectricitySystem.SmartPowerGateway {
            let addVC = SmartEleSearchViewController.init()
            
            self.navigationController?.pushViewController(addVC, animated: true)
        }else{
            let addVC = DeviceAddViewController.init()
            addVC.addModel = model
            self.navigationController?.pushViewController(addVC, animated: true)
        }
        
    }
    
    /// 筛选
    override func navRRClick() {
        
        if self.filterView.isShow {
            self.filterView.dissmisFilterView()
        }else{
            self.filterView.showFilterView(view: self.view)
        }
        
    }
    
    /// 更新筛选项
    func updateFilter(filterModel:FilterItemModel) -> Bool {
        
        if filterModel.filterType != .DomainId {
            if filterModel.parentId == "" {
                var headerStr = "请先选择"
                if filterModel.filterType == .EntId {
                    headerStr = "请先选择中心"
                }else if filterModel.filterType == .BuildingId{
                    headerStr = "请先选择企业"
                }else if filterModel.filterType == .KeypartId{
                    headerStr = "请先选择建筑"
                }
                NeatHud.showMessage(message: headerStr, view: self.view)
                return false
            }else{
                return true
            }
        }else{
            return true
        }
    }
    
    /// 更新筛选UI数据源
    /// - Parameter filterModel: 筛选项model
    /// - Parameter valueModel: 选中valuemodel
    func reFormateData(filterModel: FilterItemModel,valueModel: KeyValueModel) {
        
        let filterDataSourceArr = self.filterView.dataSource
        
        //判断之前是否已经设置 做之前设置信息变更
        if filterModel.filterInforId != "" {
            //之前有设置
            if filterModel.filterInforId != valueModel.item_id {
                //变更 后续选项变更
                for index in filterModel.filterIndex..<filterDataSourceArr.count {
                    let model = filterDataSourceArr[index]
                    model.filterInfor = "全部"
                    model.filterInforId = ""
                }
            }
        }
        
        ///赋值当前筛选项
        let ItemModel = filterDataSourceArr[filterModel.filterIndex]
        ItemModel.filterInfor = valueModel.item_name
        ItemModel.filterInforId = valueModel.item_id
        
        ///更新下一个筛选项的parentid
        if filterModel.filterIndex < 3 {
            let nextModel = filterDataSourceArr[filterModel.filterIndex+1]
            nextModel.parentId = valueModel.item_id
        }
        
        
    }
}

extension DeviceMainListViewController:TopSearchViewDelegate{
    
    func doSearchWith(keyword: String) {
        self.keywords = keyword
        self.getDeviceList(domainId: domainId, entId: enterpriseId, buildingId: buildingId, keypartId: keypartId,keyword: keywords)
    }

}

///筛选框 代理方法
extension DeviceMainListViewController:FilterViewDelegate{
    
    func selectItem(model: FilterItemModel) {
        
        //更新当前编辑的筛选项
        currentFilterModel = model
        
        let showSelectView = self.updateFilter(filterModel: model)
        if showSelectView {
            currentFilterType = model.filterType
        
            let configerModel = InforSelectConfigerModel.init()
            configerModel.isOccurFromFilter = true
            configerModel.inforTitle = model.filterTitle
            if model.filterType == .DomainId {
                configerModel.inforType = DeviceInforType.domain
            }else if model.filterType == .EntId{
                configerModel.inforType = DeviceInforType.enterprise
                configerModel.apiParamet = model.parentId
            }else if model.filterType == .BuildingId{
                configerModel.inforType = DeviceInforType.building
                configerModel.apiParamet = model.parentId
            }else if model.filterType == .KeypartId{
                configerModel.inforType = DeviceInforType.keypart
                configerModel.apiParamet = model.parentId
            }
            
            self.inforSelectView.showSelectView(view: self.view,model: configerModel)
        }
        
    }
    
    func confirmFilter(modelArr: Array<FilterItemModel>) {
        
        for model in modelArr {
            if model.filterType == .DomainId {
                self.domainId = model.filterInforId
            }else if model.filterType == .EntId{
                self.enterpriseId = model.filterInforId
            }else if model.filterType == .BuildingId{
                self.buildingId = model.filterInforId
            }else if model.filterType == .KeypartId{
                self.keypartId = model.filterInforId
            }
        }
        
        self.getDeviceList(domainId: domainId, entId: enterpriseId, buildingId: buildingId, keypartId: keypartId,keyword: keywords)
        
        self.filterView.dissmisFilterView()
        
    }
    
    func cancelFilter() {
        self.filterView.dissmisFilterView()
    }
    
}

///选择框 代理方法
extension DeviceMainListViewController:InforSelectViewDelegate{
    
    func selectInfor(model: KeyValueModel) {
        
        self.reFormateData(filterModel: currentFilterModel, valueModel: model)

        currentFilterArr = self.filterView.dataSource
        
        self.filterView.updateUI()
        
    }
    
}

extension DeviceMainListViewController:DeviceMainListContainViewDelegate{
    
    func didSelectItem(itemModel: DeviceItemModel) {
        
         let menuModel = MenuModel.init()

        menuModel.name = self.menuModel?.name ?? ""
        menuModel.model = self.menuModel?.model
        
        let model = VCInitModel()
        model.navTitleText = self.menuModel!.name
        model.gatewayMenuModel = menuModel
        
        if model.gatewayMenuModel?.model == DeviceModel.FireSystem.uitd_NT9009 ||
            model.gatewayMenuModel?.model == DeviceModel.WaterSystem.wireless_NT8327 ||
            model.gatewayMenuModel?.model == DeviceModel.FamilySystem.HomeGateway ||
            model.gatewayMenuModel?.model == DeviceModel.WaterSystem.NT8160A{
            
            //跳转到 设备主页
            let vc = HostDeviceMainViewController.init()
            vc.initModel = model
            vc.fatherDeviceModel = itemModel
            self.navigationController?.pushViewController(vc, animated: true)
            
        }else{
            
            //跳转到 设备详情页
            let vc = DeviceDetailViewController.init()
            vc.initModel = model
            vc.delegate = self
            vc.deviceId = itemModel.device_id
            self.navigationController?.pushViewController(vc, animated: true)
            
        }
        
    }
    
    
}
extension DeviceMainListViewController:DeviceDetailViewControllerDelegate{
    
    func refreshListData() {
        
        getDeviceList(domainId: "", entId: "", buildingId: "", keypartId: "",keyword: "")
        
    }

}
