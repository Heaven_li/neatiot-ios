//
//  DeviceMainListContainView.swift
//  NeatIoT
//
//  Created by neat on 2019/10/29.
//  Copyright © 2019 Neat. All rights reserved.
//

import UIKit


protocol DeviceMainListContainViewDelegate {
    
    /// 选中item
    /// - Parameter itemModel: item 数据model
    func didSelectItem(itemModel:DeviceItemModel)
}

/// 列表内容容器
class DeviceMainListContainView: UIView {
    
    private let CellIdentifier = "HostDeviceItemTableViewCell"
    private var dataSourceArr:Array<DeviceItemModel> = []
    
    var delegate:DeviceMainListContainViewDelegate?
    
    lazy var containTableView: UITableView = {
        
        let tableview = LZTableView.init(frame: CGRect.zero, style: UITableView.Style.plain)
        tableview.delegate = self
        tableview.dataSource = self
        tableview.backgroundColor = UIColor.clear
        tableview.register(UINib.init(nibName: "HostDeviceItemTableViewCell", bundle: Bundle.main), forCellReuseIdentifier: self.CellIdentifier)
        tableview.separatorStyle = .singleLine
        tableview.separatorColor = NormalColor.tableviewSepLineColor
        tableview.separatorInset = UIEdgeInsets.init(top: 0, left: 3, bottom: 0, right: 3)
        let mj_h = MJRefreshNormalHeader.init(refreshingTarget: self, refreshingAction: #selector(refreshAction))
        mj_h.lastUpdatedTimeLabel!.isHidden = true
        tableview.mj_header = mj_h
        
        let mj_f = MJRefreshAutoNormalFooter.init(refreshingTarget: self, refreshingAction: #selector(loadMoreAction))
        mj_f.isAutomaticallyRefresh = false
        tableview.mj_footer = mj_f
        
        return tableview
        
    }()
    
    override init(frame: CGRect) {
        
        super.init(frame: frame)
        
        self.addSubview(self.containTableView)
        
        self.containTableView.snp.makeConstraints { (make) in
            make.edges.equalTo(self)
        }
        
    }
    
    @objc func refreshAction() {
        ContainViewModel.pageIndex = 0
        ContainViewModel.refreshRequest()
    }
    
    @objc func loadMoreAction() {
        ContainViewModel.pageIndex += 1
        ContainViewModel.loadMoreRequest()
    }
    
    
    /// 显示加载中
    func showLoading() {
        self.containTableView.showLoadingView()
    }
    
    /// 更新UI方法
    /// - Parameter dataSource: 数据源
    func reloadView(response:DeviceHostListResponse) {
        
        self.dataSourceArr.removeAll()
        self.dataSourceArr.append(contentsOf: response.response_result!.deviceArr)
        handleRefreshAndPull(response: response)
        self.containTableView.reloadData()
        
        if self.dataSourceArr.count == 0 {
            self.containTableView.showEmptyView()
        }else{
            self.containTableView.hidenHolderView()
        }
        
    }
    
    /// 失败方法
    /// - Parameter error: 错误内容
    func networkFailed(error:BaseErrorResponse) {
        self.containTableView.showRequestFaildView {
            ContainViewModel.reSendRequest()
        }
    }
    
    /// 加载更多
    /// - Parameter dataSource: 返回数据源
    func loadMore(response:DeviceHostListResponse) {
        
        self.dataSourceArr.append(contentsOf:response.response_result!.deviceArr)
        
        handleRefreshAndPull(response: response)
        
        self.containTableView.reloadData()
        
    }
    
    func refreshData(response:DeviceHostListResponse) {
        
        self.dataSourceArr.removeAll()
        self.dataSourceArr.append(contentsOf: response.response_result!.deviceArr)
        
        handleRefreshAndPull(response: response)
        
        self.containTableView.reloadData()
        
    }
    
    func handleRefreshAndPull(response:DeviceHostListResponse) {
        
        if self.containTableView.mj_footer!.isRefreshing {
            if self.dataSourceArr.count == response.response_result?.deviceCount {
                self.containTableView.mj_footer!.isHidden = true
            }else{
                self.containTableView.mj_footer!.endRefreshing()
                self.containTableView.mj_footer!.resetNoMoreData()
                self.containTableView.mj_footer!.isHidden = false
            }
        }else if self.containTableView.mj_header!.isRefreshing{
            self.containTableView.mj_header!.endRefreshing()
            if self.dataSourceArr.count == response.response_result?.deviceCount {
                self.containTableView.mj_footer!.isHidden = true
            }else{
                self.containTableView.mj_footer!.isHidden = false
            }
        }else{
            if self.dataSourceArr.count == response.response_result?.deviceCount {
                self.containTableView.mj_footer!.isHidden = true
            }
        }
        
    }
    
    
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}

extension DeviceMainListContainView:UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 90
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView.init()
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.1
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.delegate?.didSelectItem(itemModel: dataSourceArr[indexPath.row])
    }

    
}

extension DeviceMainListContainView:UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSourceArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifier, for: indexPath) as! HostDeviceItemTableViewCell
        cell.configerCell(itemModel: dataSourceArr[indexPath.row])
        cell.selectionStyle = .none
        return cell
    }
}


