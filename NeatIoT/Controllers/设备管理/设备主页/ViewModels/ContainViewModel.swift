//
//  ContainViewModel.swift
//  NeatIoT
//
//  Created by neat on 2019/11/11.
//  Copyright © 2019 Neat. All rights reserved.
//

import UIKit

class ContainViewModel: NSObject {
    
    /// 搜索关键词
    static var keywords:String = ""
    /// 筛选中心id
    static var domainId:String = ""
    /// 筛选企业id
    static var enterpriseId:String = ""
    /// 筛选建筑id
    static var buildingId:String = ""
    /// 筛选部位id
    static var keypartId:String = ""
    /// 当前页
    static var pageIndex:Int = 1
    
    /// 设备菜单model
    static var currentUrl:String = ""
    
    /// 当前view
    static var currentView:DeviceMainListContainView = DeviceMainListContainView()
    
    /// 当前menumodel
    static var currentMenuModel:MenuModel = MenuModel()
    
    /// 配置页面通用信息
    private class func getAPIByType(menuModel:MenuModel) -> String{
        
        /// 接口地址
        var apiUrl:String = ""
        

        if menuModel.style_id == "device_smart_gateway" {
            //智慧用电网关
            apiUrl = UrlHost.EleDevice.electricGatewayListAPI
        }
        else if menuModel.style_id == "iot_gateway_NT8140" {
            //家用网关 8140
            apiUrl = UrlHost.HomeGateway.homeGatewayListAPI
        }
        else if menuModel.style_id == "device_nb" {
            //NB烟感
            apiUrl = UrlHost.NBDevice.nbDeviceListAPI
        }else
            if  menuModel.style_id == "uitd_NT9009" {
            //菜单v2 9009
            apiUrl = UrlHost.FireUITD.uitdListAPI
        }else if  menuModel.style_id == "nb_8131_8132" {
            //菜单v2 nb
            apiUrl = UrlHost.NBDevice.nbDeviceListAPI
        }else if  menuModel.style_id == "electric_gateway_NT8126" {
            //菜单v2 8126
            apiUrl = UrlHost.EleDevice.electricGatewayListAPI
        }else if  menuModel.style_id == "electric_gateway_NT8127A" {
            //菜单v2 8127A
            apiUrl = UrlHost.EleDevice.integratedElectricGatewayListApi
        }else if  menuModel.style_id == "electric_gateway_NT8128A" {
            //菜单v2 8128A
            apiUrl = UrlHost.EleDevice.integratedElectricGatewayListApi
        }else if menuModel.style_id == "wgw_NT8161A"{
            //菜单v2 NT8161A
            apiUrl = UrlHost.WaterDevice.integratedWGWListAPI
        }else if menuModel.style_id == "wgw_NT8164A"{
            //菜单v2 NT8164A
            apiUrl = UrlHost.WaterDevice.integratedWGWListAPI
        }else if menuModel.style_id == "wgw_NT8167A"{
            //菜单v2 NT8167A
            apiUrl = UrlHost.WaterDevice.integratedWGWListAPI
        }else if  menuModel.style_id == "wgw_NT8167C" {
            //菜单v2 8167C
            apiUrl = UrlHost.WaterDevice.integratedWGWListAPI
        }else if menuModel.style_id == "mk_smart_gateway_8167c"{
            //菜单v2 NT8167C铭控
            apiUrl = UrlHost.WaterDevice.integratedWGWListAPI
        }else if menuModel.style_id == "wgw_NT8161B"{
            //菜单v2 NT8161B
            apiUrl = UrlHost.WaterDevice.integratedWGWListAPI
        }else if menuModel.style_id == "wgw_NT8164B"{
            //菜单v2 NT8164B
            apiUrl = UrlHost.WaterDevice.integratedWGWListAPI
        }else if menuModel.style_id == "wireless_NT8327"{
            //菜单v2 NT8327
            apiUrl = UrlHost.WaterDevice.waterGatewayListAPI
        }else if menuModel.style_id == "GasDetector_HM710NVMNB"{
            //HM710NVM-NB 海曼可燃气体
            apiUrl = UrlHost.GasDetector.gasDetectorListAPI
        }else if menuModel.style_id == "GasDetector_WS2CGNB"{
            //WS2CG-NB 海曼可燃气体
            apiUrl = UrlHost.GasDetector.gasDetectorListAPI
        }else if menuModel.style_id == "electric_gateway_NT8127C"{
            //NT8127C 航天长兴
            apiUrl = UrlHost.EleDevice.integratedElectricGatewayListApi
        }
        
        return apiUrl
    }
    /// 配置页面通用信息
    class func getDeviceCategoryByMenuModel(menuModel:MenuModel) -> VCInitModel{
        
        /// 接口地址
        let model:VCInitModel = VCInitModel()
        model.navTitleText = menuModel.name
        model.gatewayMenuModel = menuModel

//        if menuModel.style_id == "device_smart_gateway" {
//            //智慧用电网关
//            model.deviceCategory = DeviceModel.SmartElectricitySystem.SmartPowerGateway
//            
//        }
//        else if menuModel.style_id == "iot_gateway_NT8140" {
//            //家用网关
//            model.deviceCategory = DeviceModel.FamilySystem.HomeGateway
//
//        }
//        else if menuModel.style_id == "device_nb" {
//            //NB烟感
//            model.deviceCategory = DeviceModel.NBProbe.Smoke
//
//        }else if  menuModel.style_id == "uitd_NT9009" {
//            //菜单v2 9009
//            model.deviceCategory = DeviceModel.FireSystem.uitd_NT9009
//
//        }else if  menuModel.style_id == "nb_8131_8132" {
//            //菜单v2 nb
//            model.deviceCategory = DeviceModel.NBProbe.Smoke
//
//        }else if  menuModel.style_id == "electric_gateway_NT8126" {
//            //菜单v2 8126
//            model.deviceCategory = DeviceModel.SmartElectricitySystem.SmartPowerGateway
//
//        }else if  menuModel.style_id == "electric_gateway_NT8127A" {
//            //菜单v2 8127A
//            model.deviceCategory = DeviceModel.SmartElectricitySystem.ElectricGateway_NT8127A
//
//        }else if  menuModel.style_id == "electric_gateway_NT8127C" {
//            //菜单v2 8127C
//            model.deviceCategory = DeviceModel.SmartElectricitySystem.ElectricGateway_NT8127C
//
//        }else if  menuModel.style_id == "electric_gateway_NT8128A" {
//            //菜单v2 8128A
//            model.deviceCategory = DeviceModel.SmartElectricitySystem.ElectricGateway_NT8128A
//
//        }else if menuModel.style_id == "wgw_NT8161A"{
//            //菜单v2 NT8161A
//            model.deviceCategory = DeviceModel.WaterSystem.Wgw_NT8161A
//        }else if menuModel.style_id == "wgw_NT8164A"{
//            //菜单v2 NT8164A
//            model.deviceCategory = DeviceModel.WaterSystem.Wgw_NT8164A
//        }else if menuModel.style_id == "wgw_NT8167A"{
//            //菜单v2 NT8167A
//            model.deviceCategory = DeviceModel.WaterSystem.Wgw_NT8167A
//        }else if menuModel.style_id == "wgw_NT8167C"{
//            //菜单v2 NT8167C
//            model.deviceCategory = DeviceModel.WaterSystem.Wgw_NT8167C
//        }else if menuModel.style_id == "wgw_NT8161B"{
//            //菜单v2 NT8161B
//            model.deviceCategory = DeviceModel.WaterSystem.Wgw_NT8161B
//        }else if menuModel.style_id == "wgw_NT8164B"{
//            //菜单v2 NT8164B
//            model.deviceCategory = DeviceModel.WaterSystem.Wgw_NT8164B
//        }else if menuModel.style_id == "wireless_NT8327"{
//            //菜单v2 NT8327
//            model.deviceCategory = DeviceModel.WaterSystem.wireless_NT8327
//        }else if menuModel.style_id == "GasDetector_HM710NVMNB"{
//            //海曼可燃气体
//            model.deviceCategory = DeviceModel.GasDetector.HM710NVM
//        }else if menuModel.style_id == "GasDetector_WS2CGNB"{
//            //海曼可燃气体
//            model.deviceCategory = DeviceModel.GasDetector.WS2CG
//        }else if menuModel.style_id == "IO808EWR"{
//            //有人泵房检测网关
//            model.deviceCategory = DeviceModel.WaterSystem.Usr_IO808EWR
//        }
        
        return model
    }
    
    /// 获取设备列表
    class func getDeviceList(menuModel:MenuModel,containView:DeviceMainListContainView) {
        
        let url = getAPIByType(menuModel: menuModel)
        
        self.currentUrl = url
        
        self.currentView = containView
        
        self.currentMenuModel = menuModel
        
        containView.showLoading()
        
        if menuModel.style_id == "NT8160A" {
            
            NeatRequest.instance.getGatewayListPublic(model_type: menuModel.model, keyword: keywords, domainId: domainId, enterpriseId: enterpriseId, buildingId: buildingId, keypartId: keypartId, orderByColumn: "", isDescOrder: true, pageIndex: pageIndex) { (response) in
                
                    for model in response.response_result!.deviceArr {
                        model.device_category = menuModel.style_id
                    }

                    containView.reloadView(response: response)
                
            } failed: { (error) in
                
                containView.networkFailed(error: error)
                
            }

        }else{
            
            NeatRequest.instance.getDeviceHostList(url: url,model_type: menuModel.model, keyword: keywords, domainId: domainId, enterpriseId: enterpriseId, buildingId: buildingId, keypartId: keypartId, orderByColumn: "", isDescOrder: true, pageIndex: pageIndex, success: { (response) in

                for model in response.response_result!.deviceArr {
                    model.device_category = menuModel.style_id
                }

                containView.reloadView(response: response)

            }) { (error) in

                containView.networkFailed(error: error)

            }
            
        }
 
    }
    
    /// 重新发送请求
    class func reSendRequest(){
        
        currentView.showLoading()
        
        if currentMenuModel.style_id == "NT8160A" {
            
            NeatRequest.instance.getGatewayListPublic(model_type: currentMenuModel.model, keyword: keywords, domainId: domainId, enterpriseId: enterpriseId, buildingId: buildingId, keypartId: keypartId, orderByColumn: "", isDescOrder: true, pageIndex: pageIndex) { (response) in
                
                for model in response.response_result!.deviceArr {
                    model.device_category = currentMenuModel.style_id
                }

                currentView.reloadView(response: response)
                
            } failed: { (error) in
                
                currentView.networkFailed(error: error)
                
            }

        }else{
            
            NeatRequest.instance.getDeviceHostList(url: currentUrl,model_type: currentMenuModel.model, keyword: keywords, domainId: domainId, enterpriseId: enterpriseId, buildingId: buildingId, keypartId: keypartId, orderByColumn: "", isDescOrder: true, pageIndex: pageIndex, success: { (response) in

                for model in response.response_result!.deviceArr {
                    model.device_category = currentMenuModel.style_id
                }

                currentView.reloadView(response: response)

            }) { (error) in

                currentView.networkFailed(error: error)

            }
            
        }
        
        
    }
    
    /// 刷新请求
    class func refreshRequest(){
        
        pageIndex = 1
        
        if currentMenuModel.style_id == "NT8160A" {
            
            NeatRequest.instance.getGatewayListPublic(model_type: currentMenuModel.model, keyword: keywords, domainId: domainId, enterpriseId: enterpriseId, buildingId: buildingId, keypartId: keypartId, orderByColumn: "", isDescOrder: true, pageIndex: pageIndex) { (response) in
                
                for model in response.response_result!.deviceArr {
                    model.device_category = currentMenuModel.style_id
                }

                currentView.reloadView(response: response)
                
            } failed: { (error) in
                
                currentView.networkFailed(error: error)
                
            }

        }else{
            
            NeatRequest.instance.getDeviceHostList(url: currentUrl,model_type: currentMenuModel.model, keyword: keywords, domainId: domainId, enterpriseId: enterpriseId, buildingId: buildingId, keypartId: keypartId, orderByColumn: "", isDescOrder: true, pageIndex: pageIndex, success: { (response) in

                for model in response.response_result!.deviceArr {
                    model.device_category = currentMenuModel.style_id
                }

                currentView.reloadView(response: response)

            }) { (error) in

                currentView.networkFailed(error: error)

            }
            
        }
        
    }
    
    /// 加载更多
    class func loadMoreRequest(){
        
        if currentMenuModel.style_id == "NT8160A" {
            
            NeatRequest.instance.getGatewayListPublic(model_type: currentMenuModel.model, keyword: keywords, domainId: domainId, enterpriseId: enterpriseId, buildingId: buildingId, keypartId: keypartId, orderByColumn: "", isDescOrder: true, pageIndex: pageIndex) { (response) in
                
                for model in response.response_result!.deviceArr {
                    model.device_category = currentMenuModel.style_id
                }

                currentView.loadMore(response: response)
                
            } failed: { (error) in
                
                currentView.networkFailed(error: error)
                
            }

        }else{
            
            NeatRequest.instance.getDeviceHostList(url: currentUrl,model_type: currentMenuModel.model, keyword: keywords, domainId: domainId, enterpriseId: enterpriseId, buildingId: buildingId, keypartId: keypartId, orderByColumn: "", isDescOrder: true, pageIndex: pageIndex, success: { (response) in

                for model in response.response_result!.deviceArr {
                    model.device_category = currentMenuModel.style_id
                }

                currentView.loadMore(response: response)

            }) { (error) in

                currentView.networkFailed(error: error)

            }
            
        }
        
    }

}
