//
//  HostContainViewModel.swift
//  NeatIoT
//
//  Created by neat on 2019/11/29.
//  Copyright © 2019 Neat. All rights reserved.
//

import UIKit

class HostContainViewModel: NSObject {
    
    private static var currentMenuModel:MenuModel?
    private static var currentView:HostDeviceMainContainView = HostDeviceMainContainView()
    static var requestUrl = ""
    
    static var paramteName = ""
    
    static var pageIndex = 1
    
    static var currentDeviceId = ""
    
    class func getListRequest(menuModel:MenuModel,deviceId:String,containView:HostDeviceMainContainView) {
        
        currentView = containView
        
        currentDeviceId = deviceId
        
        currentMenuModel = menuModel
        
        
        if menuModel.model == DeviceModel.WaterSystem.NT8160A {
            
            NeatRequest.instance.getGatewaySubDeviceListPublic(isDeviceLevel: 3, model_type: menuModel.model, id: deviceId, orderByColumn: "9", isDescOrder: true, pageIndex: pageIndex) { (response) in
                
                containView.reloadView(deviceArr: response.responseData.data)
                
            } failed: { (error) in
                
                containView.networkFailed(error: error)
                
            }

            
        }else{
            
            
            if menuModel.model == DeviceModel.FireSystem.uitd_NT9009{
                requestUrl = UrlHost.FireUITD.hostListAPI
                paramteName = "id"
            }else if menuModel.model == DeviceModel.WaterSystem.wireless_NT8327{
                requestUrl = UrlHost.WaterDevice.waterSignalListAPI
                paramteName = "deviceId"
            }else if menuModel.model == DeviceModel.FamilySystem.HomeGateway {
                requestUrl = UrlHost.HomeGateway.homeGatewayDeviceListAPI
                paramteName = "id"
            }
    
    
            containView.showLoading()
            NeatRequest.instance.getSubDeviceList(url: requestUrl,idParameter:paramteName, fatherId: deviceId, isDescOrder: true, pageIndex: pageIndex, success: { (response) in
    
                containView.reloadView(deviceArr: response.deviceList)
    
            }) { (error) in
    
                containView.networkFailed(error: error)
    
            }
            
        }
        
        

    }
    
    class func reSendRequest() {
//        currentView.showLoading()
//        NeatRequest.instance.getSubDeviceList(url: requestUrl,idParameter:paramteName, fatherId: currentDeviceId, isDescOrder: true, pageIndex: pageIndex, success: { (response) in
//
//            currentView.reloadView(response: response)
//
//        }) { (error) in
//
//            currentView.networkFailed(error: error)
//
//        }
        
        currentView.showLoading()
        
        if currentMenuModel!.model == DeviceModel.WaterSystem.NT8160A {
            
            NeatRequest.instance.getGatewaySubDeviceListPublic(isDeviceLevel: 3, model_type: currentMenuModel!.model, id: currentDeviceId, orderByColumn: "9", isDescOrder: true, pageIndex: pageIndex) { (response) in
                
                currentView.reloadView(deviceArr: response.responseData.data)
                
            } failed: { (error) in
                
                currentView.networkFailed(error: error)
                
            }

            
        }else{
            
            
            if currentMenuModel!.model == DeviceModel.FireSystem.uitd_NT9009{
                requestUrl = UrlHost.FireUITD.hostListAPI
                paramteName = "id"
            }else if currentMenuModel!.model == DeviceModel.WaterSystem.wireless_NT8327{
                requestUrl = UrlHost.WaterDevice.waterSignalListAPI
                paramteName = "deviceId"
            }else if currentMenuModel!.model == DeviceModel.FamilySystem.HomeGateway {
                requestUrl = UrlHost.HomeGateway.homeGatewayDeviceListAPI
                paramteName = "id"
            }

            NeatRequest.instance.getSubDeviceList(url: requestUrl,idParameter:paramteName, fatherId: currentDeviceId, isDescOrder: true, pageIndex: pageIndex, success: { (response) in
    
                currentView.reloadView(deviceArr: response.deviceList)
    
            }) { (error) in
    
                currentView.networkFailed(error: error)
    
            }
            
        }
    }
    
    class func refreshRequest() {
        pageIndex = 1
        
        if currentMenuModel!.model == DeviceModel.WaterSystem.NT8160A {
            
            NeatRequest.instance.getGatewaySubDeviceListPublic(isDeviceLevel: 3, model_type: currentMenuModel!.model, id: currentDeviceId, orderByColumn: "9", isDescOrder: true, pageIndex: pageIndex) { (response) in
                
                currentView.reloadView(deviceArr: response.responseData.data)
                
            } failed: { (error) in
                
                currentView.networkFailed(error: error)
                
            }
            
        }else{
            
            NeatRequest.instance.getSubDeviceList(url: requestUrl,idParameter:paramteName, fatherId: currentDeviceId, isDescOrder: true, pageIndex: pageIndex, success: { (response) in
                
                currentView.reloadView(deviceArr: response.deviceList)
                
            }) { (error) in
                
                currentView.networkFailed(error: error)
        
            }
            
        }
        
        
    }
    class func loadMoreRequest() {
        
        pageIndex += 1
        
        if currentMenuModel!.model == DeviceModel.WaterSystem.NT8160A {
            
            NeatRequest.instance.getGatewaySubDeviceListPublic(isDeviceLevel: 3, model_type: currentMenuModel!.model, id: currentDeviceId, orderByColumn: "9", isDescOrder: true, pageIndex: pageIndex) { (response) in
                
                currentView.reloadView(deviceArr: response.responseData.data)
                
            } failed: { (error) in
                
                currentView.networkFailed(error: error)
                
            }
        
        }else{
            
            NeatRequest.instance.getSubDeviceList(url: requestUrl,idParameter:paramteName, fatherId: currentDeviceId, isDescOrder: true, pageIndex: pageIndex, success: { (response) in
                
                currentView.reloadView(deviceArr: response.deviceList)
                
            }) { (error) in
                
                NeatHud.showMessage(message: error.errMessage, view: currentView)
        
            }
            
        }
        
        
    }

}
