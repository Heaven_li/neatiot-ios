//
//  HostDeviceMainViewController.swift
//  NeatIoT
//
//  Created by neat on 2019/11/19.
//  Copyright © 2019 Neat. All rights reserved.
//

import UIKit


class HostDeviceMainViewController: SupperViewController {
    

    var initModel:VCInitModel = VCInitModel()
    /// 父设备的数据model
    var fatherDeviceModel: DeviceItemModel?
    
    lazy var containView: HostDeviceMainContainView = {
        let view = HostDeviceMainContainView.init()
        view.delegate = self
        return view
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.titleName = initModel.navTitleText
        
        let addItem = NavItemModel.init()
        addItem.itemIcon = "nav_add"
        
        let filterItem = NavItemModel.init()
        filterItem.itemIcon = "nav_detail"
        
        if self.initModel.gatewayMenuModel?.model == DeviceModel.FamilySystem.HomeGateway {
            ///家用网关器件 不可添加
            self.rightItems = [filterItem]
        }else{
            self.rightItems = [filterItem,addItem]
        }
        
        confirgerUI()
        
        self.containView.menuModel = initModel.gatewayMenuModel!
        
        getData()

    }
    
    func getData() {
        HostContainViewModel.pageIndex = 1
        HostContainViewModel.getListRequest(menuModel: initModel.gatewayMenuModel!, deviceId: fatherDeviceModel!.device_id, containView: self.containView)
    }
    
    
    
    func confirgerUI() {
        self.view.addSubview(self.containView)
        
        self.containView.snp.makeConstraints { (make) in
            make.edges.equalTo(self.view)
        }
    }
    
    /// 导航栏右侧最右按钮点击事件 如果只有一个按钮可重写该方法
    override func navRRClick() -> Void {
        
        let vc = DeviceDetailViewController.init()
        vc.initModel = self.initModel
        vc.deviceId = self.fatherDeviceModel!.device_id
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    /// 导航栏右侧最左按钮点击事件
    override func navRLClick() -> Void {
        
        let gatewayMenuModel = MenuModel.init();
        gatewayMenuModel.model = initModel.gatewayMenuModel?.model
        gatewayMenuModel.name = initModel.gatewayMenuModel?.name ?? ""
        
        let currentModel = VCInitModel.init()
        currentModel.gatewayMenuModel = gatewayMenuModel
        
        
        if self.initModel.gatewayMenuModel?.model == DeviceModel.FireSystem.uitd_NT9009 {
            //一体式传输设备主页
            gatewayMenuModel.model = DeviceModel.FireSystem.IntegratedTransmissionHost
            currentModel.navTitleText = "一体式主机"
        }else if self.initModel.gatewayMenuModel?.model == DeviceModel.WaterSystem.wireless_NT8327{
            //neat水网关
            gatewayMenuModel.model = DeviceModel.WaterSystem.NeatWaterSingal
            currentModel.navTitleText = "水信号"
        }else if self.initModel.gatewayMenuModel?.model == DeviceModel.WaterSystem.NT8160A{
            //有人泵房检测网关
            gatewayMenuModel.model = DeviceModel.WaterSystem.NT8160A_device
            currentModel.navTitleText = "添加下属设备"
        }
        
        
    
        let addVC = DeviceAddViewController.init()
        addVC.addModel = currentModel
        addVC.deviceFatherID = self.fatherDeviceModel!.device_id
        addVC.enterpriseID = self.fatherDeviceModel!.device_enterprise_id
        self.navigationController?.pushViewController(addVC, animated: true)
        
    }
    
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}



extension HostDeviceMainViewController:HostDeviceMainContainViewDelegate{
    
    func usrSwitchControlBtnValueChange(isOn: Bool,model:SubDeviceListResponse.SubDeviceItemModel) {
        
        var message = "";
        if isOn {
            //启动
            message = "确定要启动吗？"
            
        }else{
            //停止
            message = "确定要停止吗？"
            
        }
        
        let commandModel = SignalControlCommandModel.init()
        commandModel.model = self.initModel.gatewayMenuModel?.model ?? ""
        commandModel.isDeviceLevel = DeviceLevel.Device
        commandModel.equipmentID = model.id
        commandModel.commandType = 2
        commandModel.commandValue = isOn ? 2:3
        
        showAlert(vc: self, title: "", message: message, leftTitle: "确定", rightTitle: "取消") { (confirmAction) in
            
            self.postCommand(commandModel: commandModel)
            
        } rightAction: { (cancelAction) in
            
            
        }
    
        
    }
    
    private func postCommand(commandModel:SignalControlCommandModel){
        
        NeatHud.showLoading(view: self.view, message: "正在下发指令")
        
        NeatRequest.instance.postSignalCommandPublic(inforModel: commandModel) { (response) in
            
            NeatHud.showMessage(message: response.message ?? "指令下发成功", view: self.view)
            self.getData()
            
            
        } failed: { (error) in
            
            NeatHud.showMessage(message: error.errMessage, view: self.view)
            self.getData()
            
        }
        
    }

    
    func didSeletItem(model: SubDeviceListResponse.SubDeviceItemModel) {
        
        //跳转到 设备详情页
        let vc = DeviceDetailViewController.init()
        
        let gatewayMenuModel = MenuModel.init();
        gatewayMenuModel.model = initModel.gatewayMenuModel?.model
        gatewayMenuModel.name = initModel.gatewayMenuModel?.name ?? ""

        let currentModel = VCInitModel.init()
        currentModel.gatewayMenuModel = gatewayMenuModel
        
        if self.initModel.gatewayMenuModel?.model == DeviceModel.FireSystem.uitd_NT9009 {
            //一体式传输设备主页
            currentModel.gatewayMenuModel?.model = DeviceModel.FireSystem.IntegratedTransmissionHost
            currentModel.navTitleText = "设备详情"
        }else if self.initModel.gatewayMenuModel?.model == DeviceModel.WaterSystem.wireless_NT8327{
            //neat水网关
            currentModel.gatewayMenuModel?.model = DeviceModel.WaterSystem.NeatWaterSingal
            currentModel.navTitleText = "设备详情"
        }else if self.initModel.gatewayMenuModel?.model == DeviceModel.FamilySystem.HomeGateway{
            //8140
            currentModel.gatewayMenuModel?.model = DeviceModel.FamilySystem.HomeGatewayDevice
            currentModel.navTitleText = "设备详情"
        }else if self.initModel.gatewayMenuModel?.model == DeviceModel.WaterSystem.NT8160A{
            //有人泵房检测
            currentModel.gatewayMenuModel?.model = DeviceModel.WaterSystem.NT8160A_device
            currentModel.navTitleText = "设备详情"
            
        }
        
        vc.initModel = currentModel
        vc.deviceId = model.id
        vc.delegate = self
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    @objc func refreshHostDeviceListData() {
        HostContainViewModel.getListRequest(menuModel: initModel.gatewayMenuModel!, deviceId: fatherDeviceModel!.device_id, containView: self.containView)
//           HostContainViewModel.getListRequest(deviceCategory: initModel.deviceCategory, deviceId: fatherDeviceModel!.device_id, containView: self.containView)
       }

}

extension HostDeviceMainViewController:DeviceDetailViewControllerDelegate{
    func refreshListData() {
//        HostContainViewModel.getListRequest(deviceCategory: initModel.deviceCategory, deviceId: fatherDeviceModel!.device_id, containView: self.containView)
        HostContainViewModel.getListRequest(menuModel: initModel.gatewayMenuModel!, deviceId: fatherDeviceModel!.device_id, containView: self.containView)
    }
    
    
}


