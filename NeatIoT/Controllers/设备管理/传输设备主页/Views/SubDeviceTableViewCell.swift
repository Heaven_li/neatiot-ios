//
//  SubDeviceTableViewCell.swift
//  NeatIoT
//
//  Created by neat on 2019/11/29.
//  Copyright © 2019 Neat. All rights reserved.
//

import UIKit


class SubDeviceTableViewCell: UITableViewCell {

    @IBOutlet weak var titleLable: UILabel!
    
    @IBOutlet weak var desLable: UILabel!
    
    @IBOutlet weak var stateBgView: UIView!
    
    @IBOutlet weak var stateLable: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        self.stateBgView.layer.cornerRadius = 5

    }
    
    func configerCell(model:SubDeviceListResponse.SubDeviceItemModel) {
        
        self.titleLable.text = model.name
        self.desLable.text = "设备编码："+model.code
        
        if model.status != -1 {
            
            self.stateBgView.isHidden = false
            
            if model.status == AlarmStateType.Alarm {
               //报警
                self.stateBgView.backgroundColor = NormalColor.Alarm
                self.stateLable.text = "报警"
            }else if model.status == AlarmStateType.Fault{
               //故障
                self.stateBgView.backgroundColor = NormalColor.Fault
                self.stateLable.text = "故障"
            }else if model.status == AlarmStateType.Fire{
               //火警
                self.stateBgView.backgroundColor = NormalColor.Fire
                self.stateLable.text = "火警"
            }else if model.status == AlarmStateType.Normal{
               //正常
                self.stateBgView.backgroundColor = NormalColor.Normal
                self.stateLable.text = "正常"
            }else if model.status == AlarmStateType.NotFound{
               //未知
                self.stateBgView.backgroundColor = NormalColor.NotFound
                self.stateLable.text = "未知"
            }else if model.status == AlarmStateType.Offline{
               //离线
                self.stateBgView.backgroundColor = NormalColor.Offline
                self.stateLable.text = "离线"
            }
            
        }else{
            
            self.stateBgView.isHidden = true
            
        }
    }
    
}
