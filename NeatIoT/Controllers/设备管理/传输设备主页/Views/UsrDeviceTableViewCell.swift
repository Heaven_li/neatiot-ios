//
//  UsrDeviceTableViewCell.swift
//  NeatIoT
//
//  Created by Yang Blus on 2020/10/27.
//  Copyright © 2020 Neat. All rights reserved.
//

import UIKit

protocol UsrDeviceTableViewCellDelegate {
    func controllSwitchBtnValueChange(isOn:Bool,model:SubDeviceListResponse.SubDeviceItemModel)
}

class UsrDeviceTableViewCell: UITableViewCell {
    
    private var currentModel:SubDeviceListResponse.SubDeviceItemModel = SubDeviceListResponse.SubDeviceItemModel.init()
    
    var usrDelegate:UsrDeviceTableViewCellDelegate?
    
    /// 设备在线离线状态背景
    @IBOutlet weak var onLineStateView: UIView!
    /// 设备在线离线状态文字
    @IBOutlet weak var onLineStateLable: UILabel!
    /// 设备名称
    @IBOutlet weak var deviceNameLable: UILabel!
    
    /// 设备类型
    @IBOutlet weak var deviceTypeLable: UILabel!
    
    /// 开启停止按钮
    @IBOutlet weak var startStopSwitch: UISwitch!
    
    /// 设备状态背景view
    @IBOutlet weak var alarmBgView: UIView!
    
    /// 设备状态文字
    @IBOutlet weak var alarmTitleLable: UILabel!
    
    /// 设备运行状态背景view
    @IBOutlet weak var deviceRunStateBgView: UIView!
    
    /// 设备运行状态文字
    @IBOutlet weak var deviceRunStateTitleLable: UILabel!
    
    /// 手自动背景view
    @IBOutlet weak var autoBgView: UIView!
    
    /// 手自动状态文字
    @IBOutlet weak var autoTitleLable: UILabel!
    
    /// 电源状态背景view
    @IBOutlet weak var powerBgView: UIView!
    /// 电源状态文字
    @IBOutlet weak var powerTitleLable: UILabel!
    
    
    lazy var switchButton: NeatSwitchButton = {
        let button = NeatSwitchButton.init()
        button.delegate = self
        return button
    }()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.onLineStateView.layer.cornerRadius = 3
        self.onLineStateView.clipsToBounds = true
        
        self.alarmBgView.layer.cornerRadius = 3
        self.alarmBgView.clipsToBounds = true
        
        self.deviceRunStateBgView.layer.cornerRadius = 3
        self.deviceRunStateBgView.clipsToBounds = true
        
        self.autoBgView.layer.cornerRadius = 3
        self.autoBgView.clipsToBounds = true

        self.powerBgView.layer.cornerRadius = 3
        self.powerBgView.clipsToBounds = true
        
        self.addSubview(self.switchButton)
        
        self.switchButton.snp.makeConstraints { (make) in
            make.left.equalTo(self.deviceNameLable.snp.right).offset(10)
            make.right.equalTo(self).offset(-10)
            make.centerY.equalTo(self)
            make.width.equalTo(64)
            make.height.equalTo(34)
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configerCell(model:SubDeviceListResponse.SubDeviceItemModel) {
        
        currentModel = model;
        
        if model.onlineStatus != -1 {
            self.onLineStateView.backgroundColor = model.onlineStatus == 1 ? #colorLiteral(red: 0.6, green: 0.6, blue: 0.6, alpha: 1):#colorLiteral(red: 0.3137254902, green: 0.8392156863, blue: 0.3882352941, alpha: 1)
            self.onLineStateLable.text = model.onlineStatus == 1 ? "离线":"在线"
        }else{
            self.onLineStateLable.backgroundColor = #colorLiteral(red: 0.6, green: 0.6, blue: 0.6, alpha: 1)
            self.onLineStateLable.text = "未知"
        }
        
        self.deviceNameLable.text = model.name
        
        self.deviceTypeLable.text = "设备类型:" + model.deviceTypeDesc
        
        self.alarmTitleLable.text = model.alarmStatusDesc
        
        self.deviceRunStateTitleLable.text = model.runStatusDesc
        
        self.autoTitleLable.text = model.autoStatusDesc
        
        self.powerTitleLable.text = model.powerStatusDesc
        
        ///判断设备类型以显示或隐藏 启动停止按钮
        if model.deviceType == 1 {
            
            if model.autoStatus == 1 {
                //自动模式下 才可以控制
                //送风扇 可以启动停止
                self.switchButton.isHidden = false
                
                if model.runStatus == 1 {
                    //运行
                    self.switchButton.checkOn = true
                }else{
                    self.switchButton.checkOn = false
                }
                
            }else if model.autoStatus == 2 {
                //手动模式下 不可控制
                //送风扇 可以启动停止
                self.switchButton.isHidden = true
            }else{
                
                self.switchButton.isHidden = true
                
            }
            
        }else if model.deviceType == 2 {
            //消防水泵 暂不支持启动停止
            self.switchButton.isHidden = true
            
        }
        
    }
    

    
}

extension UsrDeviceTableViewCell:NeatSwitchButtonClickDelegate{
    
    func switchBtnClick(isCheck: Bool) {
        
        self.usrDelegate?.controllSwitchBtnValueChange(isOn: isCheck,model: currentModel)
        
    }
    
}
