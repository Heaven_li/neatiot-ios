//
//  HostDeviceMainContainView.swift
//  NeatIoT
//
//  Created by neat on 2019/11/29.
//  Copyright © 2019 Neat. All rights reserved.
//

import UIKit
import MJRefresh


protocol HostDeviceMainContainViewDelegate {
    
    func didSeletItem(model:SubDeviceListResponse.SubDeviceItemModel)
    
    func usrSwitchControlBtnValueChange(isOn:Bool,model:SubDeviceListResponse.SubDeviceItemModel)
}

class HostDeviceMainContainView: UIView {
    
    private var currentMenuModel:MenuModel = MenuModel.init()
    
    private var cellHight:CGFloat = 64
    
    private let cellIdentStr = "SubDeviceTableViewCell"
    
    private let usrCellIdentStr = "UsrDeviceTableViewCell"
    
    private var dataSource: Array<SubDeviceListResponse.SubDeviceItemModel> = []
    
    var delegate:HostDeviceMainContainViewDelegate?
    
    var menuModel: MenuModel {
        get {
            currentMenuModel
        }
        set {
            currentMenuModel = newValue
            
            if currentMenuModel.model == DeviceModel.WaterSystem.NT8160A {
                cellHight = 80
            }
            
        }
    }
    
    lazy var containTableView: UITableView = {
        
        let tableview = LZTableView.init(frame: CGRect.zero, style: UITableView.Style.plain)
        tableview.delegate = self
        tableview.dataSource = self
        tableview.backgroundColor = UIColor.clear
        tableview.register(UINib.init(nibName: "SubDeviceTableViewCell", bundle: Bundle.main), forCellReuseIdentifier: self.cellIdentStr)
        tableview.register(UINib.init(nibName: "UsrDeviceTableViewCell", bundle: Bundle.main), forCellReuseIdentifier: self.usrCellIdentStr)
        tableview.separatorStyle = .singleLine
        tableview.separatorColor = NormalColor.tableviewSepLineColor
        tableview.separatorInset = UIEdgeInsets.init(top: 0, left: 3, bottom: 0, right: 3)
        let mj_h = MJRefreshNormalHeader.init(refreshingTarget: self, refreshingAction: #selector(refreshAction))
        mj_h.lastUpdatedTimeLabel?.isHidden = true
        tableview.mj_header = mj_h
        
        let mj_f = MJRefreshAutoNormalFooter.init(refreshingTarget: self, refreshingAction: #selector(loadMoreAction))
        mj_f.isAutomaticallyRefresh = false
        tableview.mj_footer = mj_f
        
        return tableview
    }()
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.addSubview(self.containTableView)
        
        self.containTableView.snp.makeConstraints { (make) in
            make.edges.equalTo(self)
        }
        
    }
    
    @objc func refreshAction() {
        HostContainViewModel.pageIndex = 0
        HostContainViewModel.refreshRequest()
    }
    
    @objc func loadMoreAction() {
        HostContainViewModel.pageIndex += 1
        HostContainViewModel.loadMoreRequest()
    }
    
    /// 显示加载中
    func showLoading() {
        self.containTableView.showLoadingView()
    }
    
    /// 更新UI方法
    /// - Parameter dataSource: 数据源
    func reloadView(deviceArr:Array<SubDeviceListResponse.SubDeviceItemModel>) {
        
        self.dataSource.removeAll()
        self.dataSource.append(contentsOf: deviceArr)
        handleRefreshAndPull(deviceArr)
        self.containTableView.reloadData()
        
        if self.dataSource.count == 0 {
            self.containTableView.showEmptyView()
        }else{
            self.containTableView.hidenHolderView()
        }
        
    }
    
    /// 失败方法
    /// - Parameter error: 错误内容
    func networkFailed(error:BaseErrorResponse) {
        self.containTableView.showRequestFaildView {
            ContainViewModel.reSendRequest()
        }
    }
    
    /// 加载更多
    /// - Parameter deviceArr: 设备数组
    func loadMore(deviceArr:Array<SubDeviceListResponse.SubDeviceItemModel>) {
        
        self.dataSource.append(contentsOf:deviceArr)
        
        handleRefreshAndPull(deviceArr)
        
        self.containTableView.reloadData()
        
    }
    
    /// 刷新
    /// - Parameter deviceArr: 设备数组
    func refreshData(deviceArr:Array<SubDeviceListResponse.SubDeviceItemModel>) {
        
        self.dataSource.removeAll()
        self.dataSource.append(contentsOf: deviceArr)
        
        handleRefreshAndPull(deviceArr)
        
        self.containTableView.reloadData()
        
    }
    
    func handleRefreshAndPull(_ deviceArr:Array<SubDeviceListResponse.SubDeviceItemModel>) {
        
       
        if self.containTableView.mj_footer!.isRefreshing {
            
            if deviceArr.count < 10 {
                //没有更多数据
                self.containTableView.mj_footer!.endRefreshingWithNoMoreData()
                self.containTableView.mj_footer!.isHidden = true
                
            }else{
                self.containTableView.mj_footer!.endRefreshing()
                self.containTableView.mj_footer!.isHidden = false
            }
            
            
        }else if self.containTableView.mj_header!.isRefreshing{
            
            self.containTableView.mj_header!.endRefreshing()
            if deviceArr.count < 10 {
                //没有更多数据
                self.containTableView.mj_footer!.endRefreshingWithNoMoreData()
                self.containTableView.mj_footer!.isHidden = true
            }else{
                self.containTableView.mj_footer!.resetNoMoreData()
                self.containTableView.mj_footer!.isHidden = false
            }
            
        }else{
            
            if deviceArr.count < 10 {
                //没有更多数据
                self.containTableView.mj_footer!.endRefreshingWithNoMoreData()
                self.containTableView.mj_footer!.isHidden = true
            }else{
                self.containTableView.mj_footer!.resetNoMoreData()
                self.containTableView.mj_footer!.isHidden = false
            }
            
        }
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    

}

extension HostDeviceMainContainView:UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return cellHight
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.1
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView.init()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        
        self.delegate?.didSeletItem(model: dataSource[indexPath.row])
    }
    
    
    
}
extension HostDeviceMainContainView:UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if menuModel.model == DeviceModel.WaterSystem.NT8160A {
            let cell = tableView.dequeueReusableCell(withIdentifier: usrCellIdentStr, for: indexPath) as! UsrDeviceTableViewCell
            
            cell.selectionStyle = .none
            cell.usrDelegate = self
            cell.configerCell(model: dataSource[indexPath.row])
            
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentStr, for: indexPath) as! SubDeviceTableViewCell
            
            cell.selectionStyle = .none
            cell.configerCell(model: dataSource[indexPath.row])
            
            return cell
        }
        
    }
    
    
}

extension HostDeviceMainContainView:UsrDeviceTableViewCellDelegate{
    
    func controllSwitchBtnValueChange(isOn: Bool, model: SubDeviceListResponse.SubDeviceItemModel) {
        self.delegate?.usrSwitchControlBtnValueChange(isOn: isOn,model:model)
    }
    
  
}
