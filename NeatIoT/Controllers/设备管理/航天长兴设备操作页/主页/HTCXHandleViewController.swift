//
//  HTCXHandleViewController.swift
//  NeatIoT
//
//  Created by Yang Blus on 2020/10/14.
//  Copyright © 2020 Neat. All rights reserved.
//

import UIKit

class HTCXHandleViewController: SupperViewController {
    
    /// 设备信息model
    public var inforModel: DeviceInforViewModel?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.titleName = "相关操作"

    
        // Do any additional setup after loading the view.
    }

    @IBAction func tapSelence(_ sender: Any) {
        
        debugPrint("消音")
        
        let vc = SignalControlViewController.init()
        vc.inforModel = self.inforModel
        vc.desStr = "执行消音操作后，设备将不再发出报警声音。"
        vc.commandType = .Selence
        vc.navTitle = "消音"
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func tapReSet(_ sender: Any) {
        
        debugPrint("复位")
        
        let vc = SignalControlViewController.init()
        vc.inforModel = self.inforModel
        vc.desStr = "执行复位操作后，设备将重新启动恢复正常状态。"
        vc.navTitle = "复位"
        vc.commandType = .Reset
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    @IBAction func tapSelfCheck(_ sender: Any) {
        
        debugPrint("自检")
        let reportModel = CommandReportModel.init();
        reportModel.id = inforModel?.deviceID
        
        NeatHud.showLoading(view: self.view, message: "")
        
        NeatRequest.instance.postCommandPublic(url: UrlHost.EleDevice.electricSelfCheckCommandAPI, inforModel: reportModel) { (response) in
            NeatHud.showMessage(message: response.message ?? "指令下发成功", view: self.view)
        } failed: { (error) in
            NeatHud.showMessage(message: error.errMessage , view: self.view)
        }
        
    }
    @IBAction func tapFloodgate(_ sender: Any) {
        
        debugPrint("保持分闸")
        
        let reportModel = CommandReportModel.init();
        reportModel.id = inforModel?.deviceID
        reportModel.param = 1
        
        NeatHud.showLoading(view: self.view, message: "")
        
        NeatRequest.instance.postCommandPublic(url: UrlHost.EleDevice.electricFloodgateCommandAPI, inforModel: reportModel) { (response) in
            NeatHud.showMessage(message: response.message ?? "指令下发成功", view: self.view)
        } failed: { (error) in
            NeatHud.showMessage(message: error.errMessage , view: self.view)
        }
        
    }
    @IBAction func tapFloodgateClose(_ sender: Any) {
        
        debugPrint("清除分闸")
        
        let reportModel = CommandReportModel.init();
        reportModel.id = inforModel?.deviceID
        reportModel.param = 0
        
        NeatHud.showLoading(view: self.view, message: "")
        
        NeatRequest.instance.postCommandPublic(url: UrlHost.EleDevice.electricFloodgateCommandAPI, inforModel: reportModel) { (response) in
            NeatHud.showMessage(message: response.message ?? "指令下发成功", view: self.view)
        } failed: { (error) in
            NeatHud.showMessage(message: error.errMessage , view: self.view)
        }
    }
    

}
