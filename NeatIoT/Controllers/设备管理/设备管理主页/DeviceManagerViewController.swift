//
//  DeviceManagerViewController.swift
//  NeatIoT
//
//  Created by neat on 2019/10/28.
//  Copyright © 2019 Neat. All rights reserved.
//

import UIKit

class DeviceManagerViewController: SupperViewController {
    
    var menuModel:MenuModel?
    
    
    lazy var containView: DeviceManagerContainView = {
        let containView = DeviceManagerContainView.init()
        containView.delegate = self
        return containView
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.titleName = "选择设备分类"
        
        self.view.addSubview(self.containView)
        self.containView.dataSource = menuModel!.children
        
        self.containView.snp.makeConstraints { (make) in
            make.edges.equalTo(self.view)
        }

    }


}

extension DeviceManagerViewController:DeviceManagerContainViewDelegate{
    
    func didSelectItem(model:MenuModel){
        
        let deviceMainVC = DeviceMainListViewController.init()
        deviceMainVC.menuModel = model
        self.navigationController?.pushViewController(deviceMainVC, animated: true)
        
    }
    
}
