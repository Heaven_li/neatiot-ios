//
//  DeviceManagerCollectionViewCell.swift
//  NeatIoT
//
//  Created by neat on 2019/10/29.
//  Copyright © 2019 Neat. All rights reserved.
//

import UIKit

class DeviceManagerCollectionViewCell: UICollectionViewCell {
    
    
    @IBOutlet weak var deviceIconImageView: UIImageView!
    @IBOutlet weak var deviceName: UILabel!
    @IBOutlet weak var desLable: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func configerItem(menuModel:MenuModel) -> Void {
        
        deviceIconImageView.image = UIImage.init(named: menuModel.style_id)
        deviceName.text = menuModel.name
        desLable.text = String.init(format: "(%@)", menuModel.description)
        
        
    }
    
    

}
