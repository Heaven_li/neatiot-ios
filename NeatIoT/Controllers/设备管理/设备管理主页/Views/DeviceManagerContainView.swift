//
//  DeviceManagerContainView.swift
//  NeatIoT
//
//  Created by neat on 2019/10/28.
//  Copyright © 2019 Neat. All rights reserved.
//

import UIKit

protocol DeviceManagerContainViewDelegate {
    func didSelectItem(model:MenuModel)
}

class DeviceManagerContainView: UIView {

    let FUNCTIONCELLIDENTIFIER = "DeviceManagerCollectionViewCell"
    
    var dataSourcArr = Array<MenuModel>()
    
    var delegate:DeviceManagerContainViewDelegate?
    
    var dataSource: Array<MenuModel> {
        get {
            return Array()
        }
        set {
            dataSourcArr.removeAll()
            dataSourcArr.append(contentsOf: newValue)
            self.containCollectionView.reloadData()
        }
    }
    
    lazy var containCollectionView: UICollectionView = {
        
        let flowLayout = UICollectionViewFlowLayout.init()
        flowLayout.scrollDirection = .vertical
        flowLayout.minimumLineSpacing = 1
        flowLayout.minimumInteritemSpacing = 1
        
        let collectionView = UICollectionView.init(frame: CGRect.zero, collectionViewLayout: flowLayout)
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.backgroundColor = NormalColor.viewControllerDefaultBgColor
        
        collectionView.register(UINib.init(nibName: "DeviceManagerCollectionViewCell", bundle: Bundle.main), forCellWithReuseIdentifier: self.FUNCTIONCELLIDENTIFIER)
        
        return collectionView
    
    }()
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.addSubview(self.containCollectionView)
        self.containCollectionView.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
extension DeviceManagerContainView:UICollectionViewDataSource{
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let item = collectionView.dequeueReusableCell(withReuseIdentifier: FUNCTIONCELLIDENTIFIER, for: indexPath) as! DeviceManagerCollectionViewCell
        
                item.configerItem(menuModel: dataSourcArr[indexPath.item])
                
                return item
    }
    
}
extension DeviceManagerContainView:UICollectionViewDelegate{
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return dataSourcArr.count
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.delegate?.didSelectItem(model: dataSourcArr[indexPath.item])
    }
}
extension DeviceManagerContainView: UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize.init(width:(NormalConstant.ScreenWidth-2)/3.0, height: NormalConstant.ScreenWidth/3.0)
    }
    

    
}
