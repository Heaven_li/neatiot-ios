//
//  GasReportTimeViewController.swift
//  NeatIoT
//
//  Created by Yang Blus on 2020/8/19.
//  Copyright © 2020 Neat. All rights reserved.
//

import UIKit

class GasReportTimeViewController: SupperViewController {

    var cellModel:DasDetectorCellModel?
    var deviceId:String?
    
    @IBOutlet weak var confirmButton: UIButton!
    
    @IBOutlet weak var inputTextField: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()

        self.titleName = "上报周期"

        self.confirmButton.layer.cornerRadius = 5
        self.confirmButton.clipsToBounds = true
        
        self.inputTextField.text = String(cellModel?.inforStr ?? 0)
    }


    @IBAction func confirmButtonClick(_ sender: Any) {
        
        let input = self.inputTextField.text
        
        if input != nil && !input!.isEmpty {
            if Int(input!) != 0 {
                let setModel = GasDetectorChangeSetModel.init()
                setModel.commandType = 1
                setModel.deviceId = self.deviceId
                setModel.handleType = 4
                setModel.setData = Int(self.inputTextField.text!)
                
                NeatRequest.instance.postGasDetetorSet(setModel: setModel, success: { (response) in
                    
                    NeatHud.showMessage(message: "设置成功", view: self.view)
                    
                }) { (error) in
                    
                    NeatHud.showMessage(message: error.errMessage, view: self.view)
                    
                    
                }
            }else{
                NeatHud.showMessage(message: "上传周期不能为0", view: self.view)
            }
        }else{
            NeatHud.showMessage(message: "上传周期不能为空", view: self.view)
        }
        
        
        
        
        
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
