//
//  GasLaunSetViewController.swift
//  NeatIoT
//
//  Created by Yang Blus on 2020/8/19.
//  Copyright © 2020 Neat. All rights reserved.
//

import UIKit

class GasLaunSetViewController: SupperViewController {

    ///
    var cellModel:DasDetectorCellModel?
    ///设备ID
    var deviceId:String?
    
    @IBOutlet weak var confirmButton: UIButton!
    @IBOutlet weak var enImageView: UIImageView!
    @IBOutlet weak var cnImageView: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.titleName = "设置语言"

        self.confirmButton.layer.cornerRadius = 5
        self.confirmButton.clipsToBounds = true
        
        
        let enTap = UITapGestureRecognizer.init(target: self, action: #selector(enTap(tap:)))
        let cnTap = UITapGestureRecognizer.init(target: self, action: #selector(cnTap(tap:)))
        
        enImageView.isUserInteractionEnabled = true
        enImageView.addGestureRecognizer(enTap)
        
        cnImageView.isUserInteractionEnabled = true
        cnImageView.addGestureRecognizer(cnTap)
        
        if self.cellModel?.inforStr == 1  {
            //中文
            enImageView.isHighlighted = false
            cnImageView.isHighlighted = true
        }else{
            //英文
            enImageView.isHighlighted = true
            cnImageView.isHighlighted = false
        }
        
    }

    @IBAction func confirmButtonClick(_ sender: Any) {
        
        let setModel = GasDetectorChangeSetModel.init()
        setModel.commandType = 1
        setModel.deviceId = self.deviceId
        setModel.handleType = 3
        setModel.setData = enImageView.isHighlighted ? 2:1
        
        NeatHud.showLoading(view: self.view, message: "正在下发语言设置指令")
        
        NeatRequest.instance.postGasDetetorSet(setModel: setModel, success: { (response) in
            
            NeatHud.showMessage(message: "设置成功", view: self.view)
            
        }) { (error) in
            
            NeatHud.showMessage(message: error.errMessage, view: self.view)
            
            
        }
        
        
    }
    
    @objc func enTap(tap:UITapGestureRecognizer) {
        
        enImageView.isHighlighted = !enImageView.isHighlighted
        
        cnImageView.isHighlighted = !enImageView.isHighlighted
        
    }
    
    @objc func cnTap(tap:UITapGestureRecognizer) {
        
        cnImageView.isHighlighted = !cnImageView.isHighlighted
        
        enImageView.isHighlighted = !cnImageView.isHighlighted
        
    }

}
