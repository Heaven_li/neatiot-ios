//
//  GasDetectorViewController.swift
//  NeatIoT
//
//  Created by Yang Blus on 2020/8/18.
//  Copyright © 2020 Neat. All rights reserved.
//

import UIKit

class GasDetectorViewController: SupperViewController {
    
    let GasDetectorSwitchTableViewCellIdent = "GasDetectorSwitchTableViewCell"
    let GasDetectorSetTableViewCellIdent = "GasDetectorSetTableViewCell"
    
    var inforModel: DeviceInforViewModel?
    
    var dataSourceArr:Array<DasDetectorCellModel> = []
    
    lazy var inforSelectPopView: InforSelectView = {
        let view = InforSelectView.init()
        view.delegate = self
        return view
    }()
    
    lazy var containTableView: UITableView = {
        let tableview = UITableView.init(frame: CGRect.zero, style: .plain)
        tableview.isScrollEnabled = false
        tableview.delegate = self
        tableview.dataSource = self
        tableview.backgroundColor = NormalColor.viewControllerDefaultBgColor
        tableview.separatorInset = UIEdgeInsets.init(top: 0, left: 15, bottom: 0, right: 15)
        tableview.register(UINib.init(nibName: "GasDetectorSwitchTableViewCell", bundle: Bundle.main), forCellReuseIdentifier: GasDetectorSwitchTableViewCellIdent)
        tableview.register(UINib.init(nibName: "GasDetectorSetTableViewCell", bundle: Bundle.main), forCellReuseIdentifier: GasDetectorSetTableViewCellIdent)

        
        return tableview
    }()
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        getSetInfor()
        
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.titleName = "相关操作"
        
        self.view.addSubview(self.containTableView)
        self.containTableView.snp.makeConstraints { (make) in
            make.edges.equalTo(self.view)
        }
        
        

        
    }
    
    func getSetInfor() {
        
        NeatRequest.instance.getGasDetectorSetDetailInfor(url: UrlHost.GasDetector.getGasDetectorSetAPI, id: inforModel!.deviceID, success: { (response) in
            
            self.configerData(model: response.detailModel ?? GasDetectorSetModel.init())
            
        }) { (error) in
            
            
            
        }
        
    }
    
    func configerData(model:GasDetectorSetModel) {
        
        dataSourceArr.removeAll()
        
        let switchCell = DasDetectorCellModel.init()
        switchCell.titleStr = "机械手状态"
        switchCell.inforStr = model.linkDeviceStatus
        switchCell.inforType = 1
        
        let voice = DasDetectorCellModel.init()
        voice.titleStr = "音量大小"
        voice.inforStr = model.volume
        voice.inforType = 2
        voice.dataType = .VoiceCol
        
        let lanu = DasDetectorCellModel.init()
        lanu.titleStr = "语言"
        lanu.inforStr = model.language
        lanu.inforType = 2
        lanu.dataType = .Langu
        
        let time = DasDetectorCellModel.init()
        time.titleStr = "上报周期(s)"
        time.inforStr = model.reportPeriod
        time.inforType = 2
        time.dataType = .ReportFre
        
        let handle = DasDetectorCellModel.init()
        handle.titleStr = "远程自检"
        handle.inforType = 3
        
        if inforModel!.deviceCategory == DeviceModel.GasDetector.HM710NVM {
            //长方形 机械手状态 上报周期 远程自检
            
            if switchCell.inforStr == 0 {
                //关闭
                dataSourceArr.append(switchCell)
            }else if switchCell.inforStr == 1 {
                //打开
                dataSourceArr.append(switchCell)
            }else if switchCell.inforStr == 2 {
                //未连接
            
            }
            
            
         
        
        }else if inforModel!.deviceCategory == DeviceModel.GasDetector.WS2CG {
            //长方形 机械手状态 音量大小 语言 上报周期 远程自检
            dataSourceArr.append(switchCell)
            dataSourceArr.append(voice)
            dataSourceArr.append(lanu)
            dataSourceArr.append(time)
          
        }
        
        self.containTableView.reloadData()
        
    }

}

extension GasDetectorViewController:InforSelectViewDelegate{
    
    
    func selectInfor(model: KeyValueModel) {
        
        
        let setModel = GasDetectorChangeSetModel.init()
        setModel.commandType = 1
        setModel.deviceId = inforModel?.deviceID
        setModel.handleType = 4
        setModel.setData = Int(model.item_id)
        
        NeatRequest.instance.postGasDetetorSet(setModel: setModel, success: { (response) in
            
            NeatHud.showMessage(message: "设置成功", view: self.view)
            
            
            
        }) { (error) in
            
            NeatHud.showMessage(message: error.errMessage, view: self.view)
            
            
        }
        
    }
    
}

extension GasDetectorViewController:UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return dataSourceArr.count
        }else{
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        

        if indexPath.section == 0 {
            
            let model = dataSourceArr[indexPath.row]
            
            if model.inforType == 1 {
                //带开关样式
                let cell = tableView.dequeueReusableCell(withIdentifier: GasDetectorSwitchTableViewCellIdent, for: indexPath) as! GasDetectorSwitchTableViewCell
                cell.configerCell(model: model)
                cell.delegate = self
                cell.backgroundColor = .white
                cell.selectionStyle = .none
                return cell
                
            }else {
                //设置样式
                let cell = tableView.dequeueReusableCell(withIdentifier: GasDetectorSetTableViewCellIdent, for: indexPath) as! GasDetectorSetTableViewCell
                cell.backgroundColor = .white
                cell.selectionStyle = .none
                cell.configerCell(model: model)
                return cell
                
            }
        }else{
            //控制样式
            let cell = tableView.dequeueReusableCell(withIdentifier: GasDetectorSetTableViewCellIdent, for: indexPath) as! GasDetectorSetTableViewCell
            cell.backgroundColor = .white
            cell.selectionStyle = .none
            
            let handle = DasDetectorCellModel.init()
            handle.titleStr = "远程自检"
            handle.inforType = 3
            
            cell.configerCell(model: handle)
        
            return cell
            
        }
    }
    
}

extension GasDetectorViewController:UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.section == 0 {
            
            let model = dataSourceArr[indexPath.row]
            if model.inforType != 1 {
                
                switch model.dataType {
                    case .VoiceCol:
                        let vc = GasVoiceSetViewController.init()
                        vc.deviceId = self.inforModel?.deviceID
                        vc.cellModel = model
                        self.navigationController?.pushViewController(vc, animated: true)
                        break
                        
                    case .Langu:
                        let vc = GasLaunSetViewController.init()
                        vc.deviceId = self.inforModel?.deviceID
                        vc.cellModel = model
                        self.navigationController?.pushViewController(vc, animated: true)
                        break
                        
                    case .ReportFre:
                        
                        
                        //列表选择
                        let configerModel = InforSelectConfigerModel.init()
                        configerModel.deviceCategory = self.inforModel!.deviceCategory
                        configerModel.inforType = DeviceInforType.gas_detector_fre
                        configerModel.inforTitle = "上报周期"
                        self.inforSelectPopView.showSelectView(view: self.view, model: configerModel)
                        
                    
                        break
                    
                }
                
            }
            
        }else{
            
            ///远程自检
            let setModel = GasDetectorChangeSetModel.init()
            setModel.commandType = 2
            setModel.deviceId = self.inforModel?.deviceID
            setModel.handleType = 1
            
            NeatHud.showLoading(view: self.view, message: "正在下发自检指令")
            NeatRequest.instance.postGasDetetorSet(setModel: setModel, success: { (response) in
                
                NeatHud.showMessage(message: "指令下发成功", view: self.view)
                
            }) { (error) in
                
                NeatHud.showMessage(message: error.errMessage, view: self.view)
                
                
            }
            
        }
        
        
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if indexPath.section == 0 {
            
            let model = dataSourceArr[indexPath.row]
            
            if model.inforType == 1 {
                //带开关样式
                return 66
                
            }else {
                //设置样式
                return 44
                
            }
        }else{
            //控制样式
            return 44
            
        }
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.01
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView.init()
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return section == 1 ? 20:0
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = UIView.init()
        view.backgroundColor = NormalColor.viewControllerDefaultBgColor
        
        return view
    }
    
    
}

extension GasDetectorViewController:GasDetectorSwitchTableViewCellDelegate{
    
    func switchBtnChange(isOn:Bool,switchBtn:UISwitch) {
        
        let setModel = GasDetectorChangeSetModel.init()
        setModel.commandType = 1
        setModel.deviceId = self.inforModel!.deviceID
        setModel.handleType = 1
        setModel.setData = isOn ? 1:0
        
        NeatHud.showLoading(view: self.view, message: "正在下发机械手操作指令")
        NeatRequest.instance.postGasDetetorSet(setModel: setModel, success: { (response) in
            
            NeatHud.showMessage(message: "指令下发成功", view: self.view)
        }) { (error) in
            
            NeatHud.showMessage(message: error.errMessage, view: self.view)
            
            switchBtn.isOn = !switchBtn.isOn
            
        }
        
    }
    
}
