//
//  GasDetectorChangeSetModel.swift
//  NeatIoT
//
//  Created by Yang Blus on 2020/8/19.
//  Copyright © 2020 Neat. All rights reserved.
//

import UIKit

class GasDetectorChangeSetModel: NSObject,Encodable {
    
    ///设备ID
    var deviceId:String?
    ///1:设置
    ///2:指令
    var commandType:Int?
    ///设置：1->联动设备状态 2->音量 3->语音 4->上报周期
    ///指令：1->远程自检
    var handleType:Int?
    ///设置操作时 设置值
    ///联动开关：1->打开 2->关闭
    ///音量大小：0～100之间整数
    ///语言：1->中文 2->英文
    ///上报周期：整数
    var setData:Int?

}
