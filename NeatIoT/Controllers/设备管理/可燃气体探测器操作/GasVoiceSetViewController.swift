//
//  GasVoiceSetViewController.swift
//  NeatIoT
//
//  Created by Yang Blus on 2020/8/19.
//  Copyright © 2020 Neat. All rights reserved.
//

import UIKit

class GasVoiceSetViewController: SupperViewController {

    
    @IBOutlet weak var confirmButton: UIButton!
    @IBOutlet weak var voiceSlider: UISlider!
    
    var cellModel:DasDetectorCellModel?
    var deviceId:String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.titleName = "音量设置"
        
        self.confirmButton.layer.cornerRadius = 5
        self.confirmButton.clipsToBounds = true
        
        self.voiceSlider.value = Float(cellModel!.inforStr ?? 0)
  
    }

    @IBAction func confirmButtonClick(_ sender: Any) {
        
        
        let setModel = GasDetectorChangeSetModel.init()
        setModel.commandType = 1
        setModel.deviceId = self.deviceId
        setModel.handleType = 2
        setModel.setData = Int(self.voiceSlider.value)
        
        NeatHud.showLoading(view: self.view, message: "正在下发音量设置指令")
        
        NeatRequest.instance.postGasDetetorSet(setModel: setModel, success: { (response) in
            
            NeatHud.showMessage(message: "设置成功", view: self.view)
            
        }) { (error) in
            
            NeatHud.showMessage(message: error.errMessage, view: self.view)
            
            
        }
        
    }
    
    @IBAction func sliderValueChanged(_ sender: Any) {
        
        
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
