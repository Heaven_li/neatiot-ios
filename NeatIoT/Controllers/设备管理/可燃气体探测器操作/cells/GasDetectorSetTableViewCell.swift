//
//  GasDetectorSetTableViewCell.swift
//  NeatIoT
//
//  Created by Yang Blus on 2020/8/18.
//  Copyright © 2020 Neat. All rights reserved.
//

import UIKit

class GasDetectorSetTableViewCell: UITableViewCell {

    @IBOutlet weak var titleLable: UILabel!
    
    
    @IBOutlet weak var inforLable: UILabel!
    
    
    @IBOutlet weak var arrowImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configerCell(model:DasDetectorCellModel) {
        
        if model.inforType == 2 {
            
            self.inforLable.isHidden = false
            self.arrowImageView.isHidden = false
            
            self.titleLable.text = model.titleStr ?? ""
            
            if model.titleStr == "音量大小" {
                self.inforLable.text = String(model.inforStr ?? -1)
            }else if model.titleStr == "语言" {
                self.inforLable.text = (model.inforStr == 1) ? "中文":"英文"
            }else if model.titleStr == "上报周期(s)"{
                self.inforLable.text = String(model.inforStr ?? -1)
            }
            
            
        }else{
            
            self.titleLable.text = model.titleStr ?? ""
            
            self.inforLable.isHidden = true
            self.arrowImageView.isHidden = true
            
        }
        
    }
    
}
