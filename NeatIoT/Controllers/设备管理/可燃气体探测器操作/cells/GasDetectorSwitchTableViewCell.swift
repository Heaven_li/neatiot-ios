//
//  GasDetectorSwitchTableViewCell.swift
//  NeatIoT
//
//  Created by Yang Blus on 2020/8/18.
//  Copyright © 2020 Neat. All rights reserved.
//

import UIKit

protocol GasDetectorSwitchTableViewCellDelegate {
    
    func switchBtnChange(isOn:Bool,switchBtn:UISwitch)
    
}

class GasDetectorSwitchTableViewCell: UITableViewCell {
    
    @IBOutlet weak var switchBtn: UISwitch!
    
    public var delegate:GasDetectorSwitchTableViewCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configerCell(model:DasDetectorCellModel){
        
        self.switchBtn.isOn = model.inforStr == 1 ? true:false
        
    }
    @IBAction func switchValueChange(_ sender: Any) {
        
        
        self.delegate?.switchBtnChange(isOn: self.switchBtn.isOn,switchBtn: self.switchBtn)
        
    
    }
    
}
