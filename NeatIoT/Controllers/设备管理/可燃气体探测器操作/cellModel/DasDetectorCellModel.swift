//
//  DasDetectorCellModel.swift
//  NeatIoT
//
//  Created by Yang Blus on 2020/8/18.
//  Copyright © 2020 Neat. All rights reserved.
//

import UIKit
public enum DataType {
    case VoiceCol
    case Langu
    case ReportFre
}

class DasDetectorCellModel: NSObject {

    /// 标题
    var titleStr:String?
    /**
    内容 设置操作时 设置值
    联动开关：1->打开 2->关闭
    音量大小：0～100之间整数
    语言：1->中文 2->英文
    上报周期：整数
     */
    var inforStr:Int?
    
    /// 数据类型 1 switch 2 设置 3 事件
    var inforType:Int?
    
    /// 数据项类型
    var dataType:DataType = .ReportFre
    
    
    
}
