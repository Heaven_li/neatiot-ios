//
//  LogDetailInforModel.swift
//  NeatIoT
//
//  Created by Yang Blus on 2020/2/10.
//  Copyright © 2020 Neat. All rights reserved.
//

import UIKit

class LogDetailInforModel: NSObject {
    
    var titleText:String?
    
    var inforText:String?
    
    var isHeaderView:Bool = false

}
