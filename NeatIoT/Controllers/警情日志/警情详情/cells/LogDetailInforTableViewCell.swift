//
//  LogDetailInforTableViewCell.swift
//  NeatIoT
//
//  Created by Yang Blus on 2020/2/10.
//  Copyright © 2020 Neat. All rights reserved.
//

import UIKit

class LogDetailInforTableViewCell: UITableViewCell {

    @IBOutlet weak var titleLable: UILabel!
    @IBOutlet weak var inforLable: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configerCell(model:LogDetailInforModel) {
        
        self.titleLable.text = model.titleText
        
        self.inforLable.text = model.inforText
        
    }
    
    
    
}
