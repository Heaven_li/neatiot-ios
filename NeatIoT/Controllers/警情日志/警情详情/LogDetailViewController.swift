//
//  LogDetailViewController.swift
//  NeatIoT
//
//  Created by Yang Blus on 2020/2/10.
//  Copyright © 2020 Neat. All rights reserved.
//

import UIKit

class LogDetailViewController: SupperViewController {
    
    let LogDetailInforTableViewCellIdent = "LogDetailInforTableViewCell"
    let LogDetailHeaderTableViewCellIdent = "LogDetailHeaderTableViewCell"
    
    var mDataSource:Array<Array<LogDetailInforModel>> = []
    
    var alarmItemModel:AlarmItemModel?
    
    lazy var containTableView: UITableView = {
        
        let tableView = LZTableView.init(frame: CGRect.zero, style: UITableView.Style.plain)
        
        tableView.tableFooterView = UIView.init()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.bounces = false
        tableView.backgroundColor = .clear
        tableView.separatorInset = UIEdgeInsets.init(top: 0, left: 6, bottom: 0, right: 6)
        tableView.register(UINib.init(nibName: "LogDetailInforTableViewCell", bundle: Bundle.main), forCellReuseIdentifier: LogDetailInforTableViewCellIdent)
        tableView.register(UINib.init(nibName: "LogDetailHeaderTableViewCell", bundle: Bundle.main), forCellReuseIdentifier: LogDetailHeaderTableViewCellIdent)
        
        return tableView
        
    }()
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        self.titleName = "警情详情"
        
        self.view.addSubview(self.containTableView)
        self.containTableView.snp.makeConstraints { (make) in
            make.edges.equalTo(self.view)
        }

        getAlarmDetail()
        
    }
    
    func getAlarmDetail() {
        
        NeatHud.showLoading(view: self.view, message: "")
        
        NeatRequest.instance.getAlarmDetail(deviceId: (alarmItemModel!.deviceId!), eventId: (alarmItemModel!.id!), deviceIdType: String(alarmItemModel!.deviceIdType!), systemCategory: (alarmItemModel!.systemCategory!), success: { (response) in
            
            NeatHud.dissmissHud(view: self.view)
            
            self.configerData(response: response)
            
        }) { (error) in
        
            NeatHud.dissmissHud(view: self.view)
            self.containTableView.showRequestFaildView(errMessage: error.errMessage) {
                self.getAlarmDetail()
            }
            
        }
    }
    
    func configerData(response:InspectionAlarmLogDetailResponse) {
        
        let alarmModel = response.alarmModel!
        
        let deviceName = LogDetailInforModel.init()
        deviceName.titleText = "设备名称"
        deviceName.inforText = alarmModel.deviceName
        
        let deviceCode = LogDetailInforModel.init()
        deviceCode.titleText = "设备编码"
        deviceCode.inforText = alarmModel.deviceCode
        
        let deviceType = LogDetailInforModel.init()
        deviceType.titleText = "设备类型"
        deviceType.inforText = alarmModel.deviceIdTypeString
        
        let domainName = LogDetailInforModel.init()
        domainName.titleText = "所属中心"
        domainName.inforText = alarmModel.domainName
        
        let entName = LogDetailInforModel.init()
        entName.titleText = "所属单位"
        entName.inforText = alarmModel.enterpriseName
        
        let buildName = LogDetailInforModel.init()
        buildName.titleText = "所属建筑"
        buildName.inforText = alarmModel.buildingName
        
        let keypartName = LogDetailInforModel.init()
        keypartName.titleText = "所属部位"
        keypartName.inforText = alarmModel.keypartName
        
        let address = LogDetailInforModel.init()
        address.titleText = "安装位置"
        address.inforText = alarmModel.address
        
        let alarmType = LogDetailInforModel.init()
        alarmType.titleText = "报警类型"
        if alarmModel.alarmCategoryStr == "fault" {
            alarmType.inforText = "故障"
        }else if alarmModel.alarmCategoryStr == "fire"{
            alarmType.inforText = "火警"
        }else if alarmModel.alarmCategoryStr == "alarm"{
            alarmType.inforText = "报警"
        }
        
        let alarmDes = LogDetailInforModel.init()
        alarmDes.titleText = "报警内容"
        alarmDes.inforText = alarmModel.alarmDesc
        
        let alarmOccus = LogDetailInforModel.init()
        alarmOccus.titleText = "首次报警"
        alarmOccus.inforText = alarmModel.occurTime
        
        let alarmCount = LogDetailInforModel.init()
        alarmCount.titleText = "报警次数"
        alarmCount.inforText = alarmModel.times
        
        let alarmLastOccus = LogDetailInforModel.init()
        alarmLastOccus.titleText = "末次报警"
        alarmLastOccus.inforText = alarmModel.lastOccurTime
        
    
        let alarmOnlineState = LogDetailInforModel.init()
        alarmOnlineState.titleText = "联网状态"
        if alarmModel.onlineStauts == 2 {
            //在线
            alarmOnlineState.inforText = "在线"
        }else if alarmModel.onlineStauts == 1 {
            //离线
            alarmOnlineState.inforText = "离线"
        }else {
            //未知
            alarmOnlineState.inforText = "未知"
        }
        
        let currentValue = LogDetailInforModel.init()
        currentValue.titleText = "当前值"
        currentValue.inforText = alarmModel.value
        
        let threshold = LogDetailInforModel.init()
        threshold.titleText = "阈值"
        threshold.inforText = alarmModel.threshold
        
        let inforArr = [deviceName,deviceCode,deviceType,domainName,entName,buildName,keypartName]
        
        mDataSource.append(inforArr)
        
        let headerView = LogDetailInforModel.init()
        headerView.titleText = "警情信息"
        headerView.isHeaderView = true
    
        if ((alarmModel.systemCategory == String(DeciceSystemCategory.Water)) && (alarmModel.deviceIdType == "2")){
            //水信号
            let alarmModel = [alarmType,alarmDes,alarmOccus,alarmLastOccus,alarmCount,alarmOnlineState,currentValue,threshold]
            mDataSource.append(alarmModel)
        }else{
            //其他设备
            let alarmModel = [alarmType,alarmDes,alarmOccus]
            mDataSource.append(alarmModel)
        }
        
        containTableView.reloadData()
        
    }

}

extension LogDetailViewController:UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 46
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        if section == 0 {
            return 46
        }else{
            return 0.01
        }
        
    }
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        if section == 0 {
            let bgView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: NormalConstant.ScreenWidth, height: 36))
            bgView.backgroundColor = #colorLiteral(red: 0.9318110943, green: 0.9319417477, blue: 0.9317700267, alpha: 1)
            
            let lable = UILabel.init(frame: CGRect.init(x: 0, y: 0, width: NormalConstant.ScreenWidth, height: 36))
            lable.text = "警情信息"
            lable.textColor = #colorLiteral(red: 0.3331107199, green: 0.3331621289, blue: 0.3330945373, alpha: 1)
            lable.font = UIFont.systemFont(ofSize: 15)
            lable.backgroundColor = #colorLiteral(red: 0.9318110943, green: 0.9319417477, blue: 0.9317700267, alpha: 1)
            bgView.addSubview(lable)
            lable.snp.makeConstraints { (make) in
                make.leading.trailing.equalTo(bgView).offset(12)
                make.top.bottom.equalTo(bgView)
            }
            return bgView
        }else{
            return UIView.init()
        }
        
    }
}
extension LogDetailViewController:UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return mDataSource.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return mDataSource[section].count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let model = mDataSource[indexPath.section][indexPath.row]
        
        let cell = tableView.dequeueReusableCell(withIdentifier: LogDetailInforTableViewCellIdent, for: indexPath) as! LogDetailInforTableViewCell
        cell.configerCell(model: model)
        cell.selectionStyle = .none
        
        return cell
       
    }
    
    
    
}
