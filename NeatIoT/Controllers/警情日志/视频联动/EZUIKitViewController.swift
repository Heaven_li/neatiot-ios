//
//  EZUIKitViewController.swift
//  NeatIoT
//
//  Created by Yang Blus on 2020/2/12.
//  Copyright © 2020 Neat. All rights reserved.
//

import UIKit

class EZUIKitViewController: SupperViewController {
    
    var appKey:String?

    var accessToken:String?

    var playUrl:String?

    var mPlayer:EZUIPlayer?

    override func viewDidLoad() {

        super.viewDidLoad()

//        appKey = "e7065cd1c78a416ea1b44e5bf6a638a9"
//        accessToken = "at.0gs15swy7h9fk19k7pdsos0nb2d20dgm-5bzdbyqada-0xzxl2n-rds2rrvik"
//        playUrl = "ezopen://open.ys7.com/706527555/1.hd.live"

        EZUIKit.initWithAppKey(self.appKey)
        EZUIKit.setAccessToken(self.accessToken)

        self.mPlayer = EZUIPlayer.createPlayer(withUrl: self.playUrl)
        
        self.mPlayer!.mDelegate = self

        self.mPlayer!.previewView.frame = self.view.bounds

        self.view.addSubview(self.mPlayer!.previewView)
        
        self.mPlayer!.startPlay()

    }
    
}

extension EZUIKitViewController:EZUIPlayerDelegate{
    
    func ezuiPlayerFinished(_ player: EZUIPlayer!) {
        
    }
    
    func ezuiPlayerPrepared(_ player: EZUIPlayer!) {
        
    }
    
    func ezuiPlayer(_ player: EZUIPlayer!, didPlayFailed error: EZUIError!) {
        
            
        NeatHud.showMessage(message: error.errorString, view: self.view)
            
       
        
    }
    
}
