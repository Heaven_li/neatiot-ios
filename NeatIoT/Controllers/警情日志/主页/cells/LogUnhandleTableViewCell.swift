//
//  LogUnhandleTableViewCell.swift
//  NeatIoT
//
//  Created by Yang Blus on 2020/2/5.
//  Copyright © 2020 Neat. All rights reserved.
//

import UIKit

protocol LogUnhandleTableViewCellDelegate {
    
    /// 联动视频
    /// - Parameter model: 警情信息
    func videoDetail(model:AlarmItemModel)
    
    /// 处理警情日志
    /// - Parameter model: 警情信息
    func handleAlarm(model:AlarmItemModel)
    
    /// 查看警情日志
    /// - Parameter model: 警情信息
    func getDetailAlarmHandleInfor(model:AlarmItemModel)
    
}


class LogUnhandleTableViewCell: UITableViewCell {
    
    private let Fire = "fire";
    private let Fault = "fault";
    private let Alarm = "alarm";
    private let RealAlarm = "realfire";

    @IBOutlet weak var item_icon: UIImageView!
    
    @IBOutlet weak var item_title: UILabel!
    
    @IBOutlet weak var item_time: UILabel!
    @IBOutlet weak var item_infor: UILabel!
    @IBOutlet weak var item_append_time: UILabel!
    @IBOutlet weak var videoBtn: UIImageView!
    @IBOutlet weak var handle_btn: UIImageView!
    @IBOutlet weak var detail_btn: UIImageView!
    
    
    var currentItemModel:AlarmItemModel?
    
    var delegate:LogUnhandleTableViewCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        let videoTapGesture = UITapGestureRecognizer.init(target: self, action: #selector(videoDetailBtnTap(tap:)))
        self.videoBtn.isUserInteractionEnabled = true
        self.videoBtn.addGestureRecognizer(videoTapGesture)
        
        let handleTapGesture = UITapGestureRecognizer.init(target: self, action: #selector(handleBtnTap(tap:)))
        self.handle_btn.isUserInteractionEnabled = true
        self.handle_btn.addGestureRecognizer(handleTapGesture)
        
        let detailTapGesture = UITapGestureRecognizer.init(target: self, action: #selector(detailBtnTap(tap:)))
        self.detail_btn.isUserInteractionEnabled = true
        self.detail_btn.addGestureRecognizer(detailTapGesture)
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configerCell(model:AlarmItemModel) {
        
        currentItemModel = model
        
        self.item_title.text = model.deviceName
        
        if model.eventCategory == Fault {
            //故障
            self.item_icon.image = UIImage.init(named: "log_fault")
        }else if model.eventCategory == Alarm{
            //报警
            self.item_icon.image = UIImage.init(named: "log_alarm")
        }else if model.eventCategory == Fire{
            //火警
            self.item_icon.image = UIImage.init(named: "log_fire")
        }else if model.eventCategory == RealAlarm{
            //真实火警
            self.item_icon.image = UIImage.init(named: "log_realfire")
        }
        
        if model.isHandle ?? false {
            //已处理
            self.item_infor.text = "处理内容:" + (model.sourceCategory ?? "")
            self.item_append_time.text = "处理时间:" + (model.handleTime ?? "")
            self.videoBtn.isHidden = true
            self.handle_btn.isHidden = true
            self.detail_btn.isHidden = false
            
        }else{
            //未处理
            self.item_infor.text = "报警内容:" + (model.sourceCategory ?? "")
            self.item_append_time.text = "发生时间:" + (model.occurTime ?? "")
            self.videoBtn.isHidden = false
            self.handle_btn.isHidden = false
            self.detail_btn.isHidden = true
        }
        
        if model.daysAgo != 0 {
            self.item_time.text = String(model.daysAgo ?? 0)+"天前"
        }else if model.hoursAgo != 0{
            self.item_time.text = String(model.hoursAgo ?? 0)+"小时前"
        }else if model.minutesAgo != 0{
            self.item_time.text = String(model.minutesAgo ?? 0)+"分钟前"
        }
        
    }
    
    @objc func videoDetailBtnTap(tap:UITapGestureRecognizer) {
        
        self.delegate?.videoDetail(model: currentItemModel!)
        
    }
    
    @objc func handleBtnTap(tap:UITapGestureRecognizer) {
        
        self.delegate?.handleAlarm(model: currentItemModel!)
        
    }
    
    @objc func detailBtnTap(tap:UITapGestureRecognizer) {
        
        self.delegate?.getDetailAlarmHandleInfor(model: currentItemModel!)
        
    }
    
}
