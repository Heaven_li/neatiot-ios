//
//  LogViewController.swift
//  NeatIoT
//
//  Created by neat on 2019/12/13.
//  Copyright © 2019 Neat. All rights reserved.
//

import UIKit

class LogViewController: SupperViewController {

    var menuModel:MenuModel?
    
    let LogUnhandleTableViewCellIdent = "LogUnhandleTableViewCellIdent"
    let LogHandleTableViewCellIdent = "LogHandleTableViewCellIdent"
    
    var isCurrentUnHandle:Bool = false
    /// 筛选中心id
    var domainId:String = ""
    /// 筛选企业id
    var enterpriseId:String = ""
    /// 事件类型id
    var eventId:String = "all"
    /// 发生时间id 默认7天
    var appendTimeId:String = "2"
    /// 当前展示数据类型 10为已完成 1为未处理
    var currentInforType:String = "1"
    var orderByColumn:String = "1"
    /// 当前页
    var pageIndex:Int = 1
    /// 当前编辑的筛选项目
    var currentFilterType = FilterType.DomainId
    /// 当前筛选项目数组
    var currentFilterArr:Array<FilterItemModel> = []
    /// 当前编辑的筛选item
    var currentFilterModel:FilterItemModel = FilterItemModel()
    /// 数据源
    var mDataSource:Array<AlarmItemModel> = []
    
    lazy var filterView: FilterView = {
        
        let filterView = FilterView.init()
        filterView.delegate = self
        filterView.dataSource = FilterViewModel.initLogFilterData()
        
        return filterView
    }()
    
    lazy var inforSelectView: InforSelectView = {
        let inforSelectView = InforSelectView.init()
        inforSelectView.delegate = self
        return inforSelectView
    }()
    
    lazy var unHandleBtn: CustomeSegmentButton = {
        let btn = CustomeSegmentButton.init(frame: CGRect.zero)
        btn.isUserInteractionEnabled = true
        let tap = UITapGestureRecognizer.init(target: self, action: #selector(unHandleTap(tap:)))
        btn.hightlight = true
        btn.titleText = "未处理"
        btn.addGestureRecognizer(tap)
        
        return btn
    }()
    
    lazy var handleBtn: CustomeSegmentButton = {
        let btn = CustomeSegmentButton.init(frame: CGRect.zero)
        btn.isUserInteractionEnabled = true
        let tap = UITapGestureRecognizer.init(target: self, action: #selector(handleTap(tap:)))
        btn.hightlight = false
        btn.titleText = "已处理"
        btn.addGestureRecognizer(tap)
        return btn
    }()
    
    lazy var containTableView: UITableView = {
        let tableview = LZTableView.init(frame: CGRect.zero, style: UITableView.Style.plain)
        tableview.backgroundColor = .white
        tableview.separatorInset = UIEdgeInsets.init(top: 0, left: 0, bottom: 0, right: 0)
        tableview.delegate = self
        tableview.dataSource = self
        
        tableview.register(UINib.init(nibName: "LogUnhandleTableViewCell", bundle: Bundle.main), forCellReuseIdentifier: LogUnhandleTableViewCellIdent)
        
        let mj_h = MJRefreshNormalHeader.init(refreshingTarget: self, refreshingAction: #selector(refreshAction))
        mj_h.lastUpdatedTimeLabel!.isHidden = true
        tableview.mj_header = mj_h
        
        let mj_f = MJRefreshAutoNormalFooter.init(refreshingTarget: self, refreshingAction: #selector(loadMoreAction))
        mj_f.isAutomaticallyRefresh = false
        tableview.mj_footer = mj_f
        
        
        return tableview
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let filterItem = NavItemModel.init()
        filterItem.itemIcon = "nav_filter"
        
        self.rightItems = [filterItem]
        
        self.titleName = self.menuModel?.name ?? "警情日志"
        
        let loginRes = DataArchiver.unArchiver(key:ArchireKey.UserInfor)
        
        if loginRes != nil {
            let userInforModel = loginRes as! LoginResultModel
            domainId = userInforModel.user_domain_id
        }
        
        self.view.addSubview(self.unHandleBtn)
        self.view.addSubview(self.handleBtn)
        self.view.addSubview(self.containTableView)
        
        self.unHandleBtn.snp.makeConstraints { (make) in
            make.top.leading.equalTo(self.view)
            make.height.equalTo(46)
            make.width.equalTo(self.view.snp.width).multipliedBy(0.5)
        }
        self.handleBtn.snp.makeConstraints { (make) in
            make.top.trailing.equalTo(self.view)
            make.height.equalTo(46)
            make.width.equalTo(self.view.snp.width).multipliedBy(0.5)
        }

        self.containTableView.snp.makeConstraints { (make) in
            make.top.equalTo(self.handleBtn.snp.bottom)
            make.leading.trailing.equalTo(self.view)
            make.bottom.equalTo(self.view.snp.bottom)
        }
        
        getData()
        
    }
    
    @objc func refreshAction() {
        pageIndex = 0
        self.refreshRequest()
    }
    
    @objc func loadMoreAction() {
        pageIndex += 1
        self.loadMoreRequest()
    }
    
    @objc func unHandleTap(tap:UITapGestureRecognizer) {
        
        isCurrentUnHandle = true
        currentInforType = "1"
        orderByColumn = "1"
        pageIndex = 1
        self.unHandleBtn.hightlight = true
        self.handleBtn.hightlight = false
        self.containTableView.mj_footer!.resetNoMoreData()
        getData()
        
    }
    
    @objc func handleTap(tap:UITapGestureRecognizer) {
        
        isCurrentUnHandle = false
        currentInforType = "10"
        orderByColumn = "2"
        pageIndex = 1
        self.unHandleBtn.hightlight = false
        self.handleBtn.hightlight = true
        self.containTableView.mj_footer!.resetNoMoreData()
        getData()
    }
    
    func getData() {
        
        self.containTableView.showLoadingView()
        
        
        
        NeatRequest.instance.getAlarmLogList(domainId: domainId, enterpriseId: enterpriseId, eventHandler: currentInforType, pageIndex: pageIndex,orderByColumn:orderByColumn, eventType: eventId, dayAgo: appendTimeId, success: { (response) in
            
            self.containTableView.hidenHolderView()
            
            self.mDataSource = response.resultModel?.itemArr ?? []
            self.unHandleBtn.titleText = "未处理"+String(response.resultModel?.notHandled ?? 0)
            self.handleBtn.titleText = "已处理"+String(response.resultModel?.handled ?? 0)
            
            self.containTableView.reloadData()
            
        }) { (error) in
            
            if error.errCode == RequestResponseCode.dataParsingException {
                //数据异常
                NeatHud.dissmissHud(view: self.view)
                self.containTableView.showRequestFaildView {
                    
                }
            }else if error.errCode == RequestResponseCode.netCanNotContent || error.errCode == RequestResponseCode.requestTimeOut{
                //无网络
                NeatHud.dissmissHud(view: self.view)
                self.containTableView.showNetErrorView(errorMessage: error.errMessage) {
                    self.getData()
                }
            }else{
                NeatHud.showMessage(message: error.errMessage, view: self.view)
            }
        
        }
    }
    
    func refreshRequest() {
        
        NeatRequest.instance.getAlarmLogList(domainId: domainId, enterpriseId: enterpriseId, eventHandler: currentInforType, pageIndex: pageIndex,orderByColumn:orderByColumn, eventType: eventId, dayAgo: appendTimeId, success: { (response) in
            
            self.containTableView.mj_header!.endRefreshing()
            
            self.mDataSource = response.resultModel?.itemArr ?? []
            
            if self.mDataSource.count < 10{
                //没有更多
                self.containTableView.mj_footer!.isHidden = true
                self.containTableView.mj_footer!.endRefreshingWithNoMoreData()
            }else{
                self.containTableView.mj_footer!.resetNoMoreData()
            }
            
            self.unHandleBtn.titleText = "未处理"+String(response.resultModel?.notHandled ?? 0)
            self.handleBtn.titleText = "已处理"+String(response.resultModel?.handled ?? 0)
            
            self.containTableView.reloadData()
            
        }) { (error) in
            
            self.containTableView.mj_header!.endRefreshing()
            
            NeatHud.showMessage(message: error.errMessage, view: self.view)
        
        }
        
    }
    
    func loadMoreRequest() {
        
        NeatRequest.instance.getAlarmLogList(domainId: domainId, enterpriseId: enterpriseId, eventHandler: currentInforType, pageIndex: pageIndex,orderByColumn:orderByColumn, eventType: eventId, dayAgo: appendTimeId, success: { (response) in
            
            self.containTableView.mj_footer!.endRefreshing()
            
            let newArr = response.resultModel?.itemArr ?? []
            
            if newArr.count == 0{
                self.pageIndex-=1
                self.containTableView.mj_footer!.endRefreshingWithNoMoreData()
            }else if newArr.count < 10{
                self.containTableView.mj_footer!.endRefreshingWithNoMoreData()
                self.mDataSource.append(contentsOf:response.resultModel?.itemArr ?? [])
            }else{
                self.mDataSource.append(contentsOf:response.resultModel?.itemArr ?? [])
            }
            
            
            self.unHandleBtn.titleText = "未处理"+String(response.resultModel?.notHandled ?? 0)
            self.handleBtn.titleText = "已处理"+String(response.resultModel?.handled ?? 0)
            
            self.containTableView.reloadData()
            
        }) { (error) in
            
            self.containTableView.mj_footer!.endRefreshing()
            NeatHud.showMessage(message: error.errMessage, view: self.view)
        
        }
        
    }
    
    /// 筛选
    override func navRRClick() {
        
        if self.filterView.isShow {
            self.filterView.dissmisFilterView()
        }else{
            
            self.filterView.showFilterView(view: self.view)
        }
        
    }
    
    /// 更新筛选项
    func updateFilter(filterModel:FilterItemModel) -> Bool {
        
        if filterModel.filterType  == .EntId {
            if filterModel.parentId == "" {
                let headerStr = "请先选择中心"
                NeatHud.showMessage(message: headerStr, view: self.view)
                return false
            }else{
                return true
            }
        }else{
            return true
        }
    }
    /// 更新筛选UI数据源
    /// - Parameter filterModel: 筛选项model
    /// - Parameter valueModel: 选中valuemodel
    func reFormateData(filterModel: FilterItemModel,valueModel: KeyValueModel) {
        
        let filterDataSourceArr = self.filterView.dataSource
        
        //判断之前是否已经设置 做之前设置信息变更
        if filterModel.filterInforId != "" {
            //之前有设置
            if filterModel.filterInforId != valueModel.item_id {
                //变更 后续选项变更
                for index in filterModel.filterIndex..<filterDataSourceArr.count {
                    let model = filterDataSourceArr[index]
                    model.filterInfor = "全部"
                    model.filterInforId = ""
                }
                
            }
        }
        
        ///赋值当前筛选项
        let ItemModel = filterDataSourceArr[filterModel.filterIndex]
        ItemModel.filterInfor = valueModel.item_name
        ItemModel.filterInforId = valueModel.item_id
        
        ///更新下一个筛选项的parentid
        if filterModel.filterIndex < 3 {
            let nextModel = filterDataSourceArr[filterModel.filterIndex+1]
            nextModel.parentId = valueModel.item_id
        }
        
        
    }
    
    


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

///筛选框 代理方法
extension LogViewController:FilterViewDelegate{
    
    func selectItem(model: FilterItemModel) {
        
        //更新当前编辑的筛选项
        currentFilterModel = model
        
        let showSelectView = self.updateFilter(filterModel: model)
        if showSelectView {
            currentFilterType = model.filterType
        
            let configerModel = InforSelectConfigerModel.init()
            configerModel.isOccurFromFilter = true
            configerModel.inforTitle = model.filterTitle
            if model.filterType == .DomainId {
                configerModel.inforType = DeviceInforType.domain
            }else if model.filterType == .EntId{
                configerModel.inforType = DeviceInforType.enterprise
                configerModel.apiParamet = model.parentId
            }else if model.filterType == .EventType{
                configerModel.inforType = DeviceInforType.eventTyep
            }else if model.filterType == .AppendTime{
                configerModel.inforType = DeviceInforType.appendTime
            }
            
            self.inforSelectView.showSelectView(view: self.view,model: configerModel)
        }
        
    }
    
    func confirmFilter(modelArr: Array<FilterItemModel>) {
        
        for model in modelArr {
            if model.filterType == .DomainId {
                self.domainId = model.filterInforId
            }else if model.filterType == .EntId{
                self.enterpriseId = model.filterInforId
            }else if model.filterType == .EventType{
                if model.filterInfor == "全部" {
                    self.eventId = "all"
                }else{
                    self.eventId = model.filterInforId
                }
                
            }else if model.filterType == .AppendTime{
                if model.filterInfor == "全部" {
                    self.appendTimeId = "0"
                }else{
                    self.appendTimeId = model.filterInforId
                }
                
            }
        }
        pageIndex = 1
        self.getData()
        
        self.filterView.dissmisFilterView()
        
    }
    
    func cancelFilter() {
        self.filterView.dissmisFilterView()
    }
    
}

///选择框 代理方法
extension LogViewController:InforSelectViewDelegate{
    
    func selectInfor(model: KeyValueModel) {
        
        self.reFormateData(filterModel: currentFilterModel, valueModel: model)

        currentFilterArr = self.filterView.dataSource
        
        self.filterView.updateUI()
        
    }
    
}

extension LogViewController:UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView.init()
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.01
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let model = mDataSource[indexPath.row]
        
        let vc = LogDetailViewController.init()
        vc.alarmItemModel = model
        
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
}

extension LogViewController:UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return mDataSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        let model = mDataSource[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: LogUnhandleTableViewCellIdent, for: indexPath) as! LogUnhandleTableViewCell
        cell.selectionStyle = .none
        cell.configerCell(model: model)
        cell.delegate = self
        return cell
        
    }
    
}

extension LogViewController:LogUnhandleTableViewCellDelegate{
    
    func videoDetail(model: AlarmItemModel) {
        // 联动视频
        NeatRequest.instance.getVideoPlayDetail(deviceId: model.deviceId!, videoPlayType: "1", beginTime: "", endTime: "", success: { (response) in

            let vc = EZUIKitViewController.init()
            vc.playUrl = response.videoModel?.playURL
            vc.appKey = response.videoModel?.appKey
            vc.accessToken = response.videoModel?.accessToken
            self.navigationController?.pushViewController(vc, animated: true)
            
        }) { (error) in
            
            NeatHud.showMessage(message: error.errMessage, view: self.view)
        }
        
        
        
    }
    
    func handleAlarm(model: AlarmItemModel) {
        // 警情处理
        let handleVC = LogHandleViewController.init()
        handleVC.alarmModel = model
        handleVC.delegate = self
        self.navigationController?.pushViewController(handleVC, animated: true)
    
    }
    
    func getDetailAlarmHandleInfor(model: AlarmItemModel) {
        // 警情处理详情
        let handleVC = InspectionAlarmHandleDetailViewController.init()
        handleVC.alarmModel = model
        self.navigationController?.pushViewController(handleVC, animated: true)
    }
    
}

extension LogViewController:LogHandleViewControllerDelegate{
    
    func didFinishHandle() {
        self.getData()
    }

}


