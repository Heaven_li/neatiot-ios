//
//  CustomeSegmentView.swift
//  NeatIoT
//
//  Created by Yang Blus on 2020/2/5.
//  Copyright © 2020 Neat. All rights reserved.
//

import UIKit

class CustomeSegmentButton: UIView {
    
    private var isHightlight:Bool = true
    var hightlight: Bool {
        get {
            isHightlight
        }
        set {
            isHightlight = newValue
            if newValue {
                self.titleLable.textColor = #colorLiteral(red: 0.01041139849, green: 0.3763569593, blue: 0.5799399614, alpha: 1)
                self.backgroundColor = .white
                self.selectView.isHidden = false
            }else{
                self.titleLable.textColor = #colorLiteral(red: 0.3333333433, green: 0.3333333433, blue: 0.3333333433, alpha: 1)
                self.backgroundColor = #colorLiteral(red: 0.9177394594, green: 0.9168062578, blue: 0.9331069656, alpha: 1)
                self.selectView.isHidden = true
            }
        }
    }
    var titleText: String {
        get {
            ""
        }
        set {
            self.titleLable.text = newValue
        }
    }
    
    lazy var titleLable: UILabel = {
        let lable = UILabel.init(frame: CGRect.zero)
        lable.textColor = .lightGray
        lable.textAlignment = .center
        lable.font = UIFont.systemFont(ofSize: 14)
        return lable
    }()
    
    lazy var selectView: UIView = {
        let view = UIView.init(frame: CGRect.zero)
        view.backgroundColor = #colorLiteral(red: 0.01041139849, green: 0.3763569593, blue: 0.5799399614, alpha: 1)
        return view
    }()
    
    

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.addSubview(self.titleLable)
        self.addSubview(self.selectView)
        
        self.titleLable.snp.makeConstraints { (make) in
            make.top.leading.trailing.equalTo(self)
            make.bottom.equalTo(self.selectView.snp.top)
        }
        self.selectView.snp.makeConstraints { (make) in
            make.bottom.equalTo(self)
            make.leading.trailing.equalTo(self)
            make.height.equalTo(self.snp.height).multipliedBy(0.1)
        }
        
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
