//
//  LogHandleViewController.swift
//  NeatIoT
//
//  Created by Yang Blus on 2020/2/10.
//  Copyright © 2020 Neat. All rights reserved.
//

import UIKit
import AVFoundation
import CoreServices

protocol LogHandleViewControllerDelegate {
    func didFinishHandle()
}

class LogHandleViewController: SupperViewController {
    
    let ReportNormalInforTableViewCellIdent:String = "ReportNormalInforTableViewCell"
    let ReportTextInforTableViewCellIdent:String = "ReportTextInforTableViewCell"
    let ReportMediaTableViewCellIdent:String = "ReportMediaTableViewCell"
    let VoiceTableViewCellIdent:String = "VoiceTableViewCell"
    let DeviceInforCellIdent = "DeviceInforCell"
    let LogHandleResultTableViewCellIdent = "LogHandleResultTableViewCell"
    
    let SepCellIdent = "SepCellIdent"
    let MoreCellIdent = "MoreCellIdent"
    let DataCellIdent = "DataCellIdent"
    
    /// 警情日志 item model
    var alarmModel: AlarmItemModel?
    
    var sendNavItem:UIBarButtonItem?
    
    var delegate:LogHandleViewControllerDelegate?
    
//    let imagePickerVC = UIImagePickerController.init()
    
    let recordSetting: [String: Any] = [AVSampleRateKey: NSNumber(value: 16000),//采样率
               AVFormatIDKey: NSNumber(value: kAudioFormatMPEG4AAC),//音频格式
               AVLinearPCMBitDepthKey: NSNumber(value: 16),//采样位数
               AVNumberOfChannelsKey: NSNumber(value: 1),//通道数
               AVEncoderAudioQualityKey: NSNumber(value: AVAudioQuality.min.rawValue)//录音质量

           ];
    let audioSession = AVAudioSession.sharedInstance()
    
    var currentAudioPath:URL?
    
    var currentEditModel:ReportItemModel?
    
    var lastRadioClickIndex:IndexPath?
    var lastVoiceItemCell:VoiceItemTableViewCell?
    var deviceStatusModel:InspectionLogDeviceStatusModel?
    
    let deviceBasicCellModel = DeviceBasicInforModel.init()
    
    
    private var deviceInfor = LogHandleDeviceInforModel.init()
    
    /// 数据项全部数据项
    private var dataArr:Array<DeviceDataInforModel> = []
    /// 当前展示全部数据项
    private var currentDataArr:Array<DeviceDataInforModel> = []
    
    let voiceSemaphore = DispatchSemaphore.init(value: 0)
    
    let mediaSemaphore = DispatchSemaphore.init(value: 0)
    
    /// 上传数据分类
    var voiceArr:Array<AnnexModel> = []
    var imageArr:Array<AnnexModel> = []
    var videoArr:Array<AnnexModel> = []
    
    /// 数据源数组
    var mDataSourceArr:Array<ReportItemModel> = []
    
    /// 媒体数组
    var mAnnexArr:Array<AnnexModel> = []
    let anxueModel = ReportItemModel.init()
    
    /// 语音数组
    var mVoiceArr:Array<AnnexModel> = []
    let voiceModel = ReportItemModel.init()
    
    /// 音频录制
    let audioTool = AudioRecorderTool.sharedInstance
    
    ///巡检点名称
    let pointName = ReportItemModel.init()
    /// 处理结果
    let handleResult = ReportItemModel.init()
    /// 项目类型
    let projectType = ReportItemModel.init()
    /// 项目子类型
    let projectSubType = ReportItemModel.init()
    /// 所属企业
    let enterpriseName = ReportItemModel.init()
    /// 所属建筑
    let buildingName = ReportItemModel.init()
    /// 所属部位
    let keypartName = ReportItemModel.init()
    
    lazy var editText: ReportItemModel = {
        /// 隐患内容描述
        let editText = ReportItemModel.init()
        editText.itemStyle = .TextViewStyle
        editText.titleText = "处理说明"
        editText.inforText = "真实警情"
        editText.uploadInfor = "真实警情"
        editText.isCanEdit = true
        
        return editText
    }()
        
    lazy var addMediaModel: AnnexModel = {
        let add = AnnexModel.init()
        add.annexType = 1
        add.isAddNewItem = true
        add.imageSource = UIImage.init(named: "add_annex")
        return add
    }()
    
    lazy var addVoiceModel: AnnexModel = {
        let add = AnnexModel.init()
        add.annexType = 3
        add.isAddNewItem = true
        return add
    }()
    
    lazy var recHud: AudioRecHud = {
        let hud = AudioRecHud.init(frame: CGRect.zero)
        hud.delegate = self
        return hud
    }()
    
    lazy var containTableView: LZTableView = {
        
        let tableView = LZTableView.init(frame: CGRect.zero, style: UITableView.Style.plain)
        tableView.backgroundColor = UIColor.clear
        tableView.delegate = self
        tableView.dataSource = self
        tableView.bounces = false
        tableView.separatorInset = UIEdgeInsets.init(top: 0, left: 6, bottom: 0, right: 6)
        tableView.register(UINib.init(nibName: "ReportNormalInforTableViewCell", bundle: Bundle.main), forCellReuseIdentifier: ReportNormalInforTableViewCellIdent)
        tableView.register(UINib.init(nibName: "ReportTextInforTableViewCell", bundle: Bundle.main), forCellReuseIdentifier: ReportTextInforTableViewCellIdent)
        tableView.register(UINib.init(nibName: "VoiceTableViewCell", bundle: Bundle.main), forCellReuseIdentifier: VoiceTableViewCellIdent)
        tableView.register(UINib.init(nibName: "LogHandleResultTableViewCell", bundle: Bundle.main), forCellReuseIdentifier: LogHandleResultTableViewCellIdent)
        tableView.register(UINib.init(nibName: "DeviceStateTableViewCell", bundle: Bundle.main), forCellReuseIdentifier: self.DeviceInforCellIdent)
        tableView.register(UINib.init(nibName: "DeviceDataInforTableViewCell", bundle: Bundle.main), forCellReuseIdentifier: self.DataCellIdent)
        tableView.register(UINib.init(nibName: "MoreTableViewCell", bundle: Bundle.main), forCellReuseIdentifier: self.MoreCellIdent)
        tableView.register(UINib.init(nibName: "SepViewTableViewCell", bundle: Bundle.main), forCellReuseIdentifier: self.SepCellIdent)
        
        tableView.register(ReportMediaTableViewCell.self, forCellReuseIdentifier: ReportMediaTableViewCellIdent)
        
        
        return tableView
    }()
    
    override func viewWillDisappear(_ animated: Bool) {
        
        if audioTool.isPlay {
            
            lastVoiceItemCell?.voice_image_view.stopAnimating()
            audioTool.stopPlayRec()
            
        }
        
    }

    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        self.titleName = "警情处理"
        
        let sendItmeModel = NavItemModel.init()
        sendItmeModel.itemText = "上传"
        self.rightItems = [sendItmeModel]
        
        sendNavItem = self.navigationItem.rightBarButtonItems![0]

        self.view.addSubview(self.containTableView)
        self.containTableView.snp.makeConstraints { (make) in
            make.edges.equalTo(self.view)
        }
        
        self.containTableView.isHidden = true
        
        getDeviceStatus(isShowLoading: true)
        
        configerData()
        
    }
    
    /// 上传
    override func navRRClick() {
        self.configerUploadInfor()
    }
    
    override func didSelectAnnex(annexModel: AnnexModel) {
        //判断已有附件数量 如果多余8个则移除添加按钮 少于8个添加添加按钮
        if mAnnexArr.count == 8 {
            mAnnexArr.insert(annexModel, at: 0)
            mAnnexArr.removeLast()
        }else{
            mAnnexArr.insert(annexModel, at: 0)
        }

        self.updateAnnex()
    }
    
    // MARK: - 获取设备运行状态信息
    @objc func getDeviceStatus(isShowLoading:Bool) {
        
        if isShowLoading {
            NeatHud.showLoading(view: self.view, message: "")
        }
        
        NeatRequest.instance.getDeviceStatus(deviceId: self.alarmModel!.deviceId!, eventId: self.alarmModel!.id!, deviceIdType: String(self.alarmModel!.deviceIdType!), systemCategory: self.alarmModel!.systemCategory!, success: { (response) in
            
            
            if isShowLoading {
                NeatHud.dissmissHud(view: self.view)
            }
            
            self.containTableView.isHidden = false
            
            self.sendNavItem?.isEnabled = true
            self.configerDeviceInforModel(response: response)
            
        }) { (errorResponse) in
            
            self.sendNavItem?.isEnabled = false
            
            self.containTableView.isHidden = false
            
            if isShowLoading {
                NeatHud.dissmissHud(view: self.view)
            }
            
            if errorResponse.errCode == RequestResponseCode.dataParsingException {
                //数据异常
                self.containTableView.showRequestFaildView {
                    
                }
            }else if errorResponse.errCode == RequestResponseCode.netCanNotContent || errorResponse.errCode == RequestResponseCode.requestTimeOut || errorResponse.errCode == RequestResponseCode.canNotContentHost{
                //无网络
                self.containTableView.showNetErrorView(errorMessage: errorResponse.errMessage) {
                    self.getDeviceStatus(isShowLoading: true)
                }
            }else{
                self.containTableView.showRequestFaildView(errMessage: errorResponse.errMessage) {
                    self.getDeviceStatus(isShowLoading: true)
                }
            }
            
        }
        
    }
    
    func configerDeviceData(response:InspectionLogDeviceStatusResponse) {
        
        dataArr.removeAll()
        currentDataArr.removeAll()
        
        for model in deviceInfor.dataItems ?? [] {
            dataArr.append(model)
        }
        
        for itemModel in response.deviceStatusModel!.dataItems ?? [] {
            
            let dataInforModel = DeviceDataInforModel.init()
            dataInforModel.titleText = itemModel.itemName ?? ""
            dataInforModel.inforText = (itemModel.itemValue ?? "-") + (itemModel.itemUnit ?? "")
            dataArr.append(dataInforModel)
            
        }
        
        for (index,model) in dataArr.enumerated() {
            if index < 4 {
                currentDataArr.append(model)
            }
        }
        
        self.containTableView.reloadData()
        
    }

    /// 将获取的设备信息 转换为通用 设备信息model
    /// 主要用于展示设备状态信息
    /// - Parameter response: 返回数据
    func configerDeviceInforModel(response:InspectionLogDeviceStatusResponse) {
        
        deviceStatusModel = response.deviceStatusModel!
        
        /// 配置通用设备类型字段 及 数据
        if (deviceStatusModel!.systemCategory == DeciceSystemCategory.Fire ||
            deviceStatusModel!.systemCategory == DeciceSystemCategory.Elecrtical ||
            deviceStatusModel!.systemCategory == DeciceSystemCategory.GasExtinguisher ||
            deviceStatusModel!.systemCategory == DeciceSystemCategory.PowerMonitoringSystem ||
            deviceStatusModel!.systemCategory == DeciceSystemCategory.FireLightingAndIndicatingSystem ||
            deviceStatusModel!.systemCategory == DeciceSystemCategory.GasAlarmSystem ||
        deviceStatusModel!.systemCategory == DeciceSystemCategory.FireDoorSystem){
            
            if deviceStatusModel!.deviceType == 1 {
                ///一体化传输装置
                self.deviceInfor.deviceCategory = DeviceModel.FireSystem.uitd_NT9009
            }else if deviceStatusModel!.deviceType == 2 {
                ///消防主机
                self.deviceInfor.deviceCategory = DeviceModel.FireSystem.IntegratedTransmissionHost
            }else if deviceStatusModel!.deviceType == 3 {
                ///火信号
                self.deviceInfor.deviceCategory = DeviceModel.FireSystem.IntegratedDevice
            }
            
        }else if (deviceStatusModel!.systemCategory == DeciceSystemCategory.Water){
            
            if deviceStatusModel!.deviceType == 1{
                ///水网关
                self.deviceInfor.deviceCategory = DeviceModel.WaterSystem.wireless_NT8327

            }else if deviceStatusModel!.deviceType == 2{
                ///水信号
                self.deviceInfor.deviceCategory = DeviceModel.WaterSystem.NeatWaterSingal

            }
            
        }else if (deviceStatusModel!.systemCategory == DeciceSystemCategory.NBSmokeDetector ||
            deviceStatusModel!.systemCategory == DeciceSystemCategory.NBSensibleTemperature){
            
            self.deviceInfor.deviceCategory = DeviceModel.NBDevice.Probe
            
        }else if (deviceStatusModel!.systemCategory == DeciceSystemCategory.NBCombustibleGas){
            
            self.deviceInfor.deviceCategory = DeviceModel.GasDetector.HM710NVM
            
        }else if deviceStatusModel!.systemCategory == DeciceSystemCategory.SmartPowerSystem{
            
            if deviceStatusModel!.deviceType == 1 {
                self.deviceInfor.deviceCategory = DeviceModel.SmartElectricitySystem.SmartPowerGateway
            }else if deviceStatusModel!.deviceType == 3 {
                self.deviceInfor.deviceCategory = DeviceModel.SmartElectricitySystem.SmartPowerChanel
            }else if deviceStatusModel!.deviceType == 0 {
                self.deviceInfor.deviceCategory = DeviceModel.SmartElectricitySystem.ElectricGateway_NT8127A
            }
            
        }else if deviceStatusModel?.systemCategory == DeciceSystemCategory.HomeFireAlarmSystem{
            //家用系统
            if deviceStatusModel!.deviceType == 1{
                ///网关
                self.deviceInfor.deviceCategory = DeviceModel.FamilySystem.HomeGateway

            }else if deviceStatusModel!.deviceType == 3{
                ///器件
                self.deviceInfor.deviceCategory = DeviceModel.FamilySystem.HomeGatewayDevice

            }
            
        }
        
        deviceInfor = LogHandleDeviceInforModel.formateModelByResponse(response: response, category: self.deviceInfor.deviceCategory)
        
        /// 配置设备基础信息
        deviceBasicCellModel.iconUrl = deviceStatusModel!.pictureUrl ?? ""
        deviceBasicCellModel.deviceName = deviceStatusModel!.name ?? ""
        deviceBasicCellModel.deviceState = deviceStatusModel!.alarmStatus ?? 0
        deviceBasicCellModel.currentValue = String(format:"%@", deviceStatusModel?.curretnValue ?? "")
        deviceBasicCellModel.functionBtnAble = deviceInfor.functionBtnAble
        deviceBasicCellModel.functionBtnAbleImageName = deviceInfor.functionBtnAbleImageName
        deviceBasicCellModel.functionBtnEnAbleImageName = deviceInfor.functionBtnEnAbleImageName
        
        self.configerDeviceData(response: response)
 
    }
    
    /// 配置展示信息
    func configerData() {
        
        let model = DataArchiver.unArchiver(key: ArchireKey.UserInfor) as! LoginResultModel
        
        ///巡检点名称
        pointName.itemStyle = .NormalStyle
        pointName.titleText = "处理人"
        pointName.inforText = model.user_name
        pointName.isCanEdit = false
        pointName.uploadInfor = model.user_name
        
        /// 处理结果
        handleResult.itemStyle = .RadioStyle
        handleResult.titleText = "处理结果"
        handleResult.isCanEdit = true
        handleResult.uploadInfor = "2"
        handleResult.inforText = "2"
    
        /// 上传附件
        mAnnexArr = [addMediaModel]
        anxueModel.itemStyle = .MedialStyle
        anxueModel.titleText = "上传附件"
        anxueModel.annexArr = mAnnexArr
        
        /// 语音附件
        mVoiceArr = [addVoiceModel]
        voiceModel.itemStyle = .VoiceStyle
        voiceModel.titleText = "语音附件"
        voiceModel.annexArr = mVoiceArr
        
        mDataSourceArr.append(pointName)
        mDataSourceArr.append(handleResult)
        mDataSourceArr.append(editText)
        mDataSourceArr.append(anxueModel)
        mDataSourceArr.append(voiceModel)
        
        containTableView.reloadData()
        
    }
    
    func updateAnnex() {
        anxueModel.annexArr = mAnnexArr
        self.containTableView.reloadData()
    }
    
    func updateAudio() {
        voiceModel.annexArr = mVoiceArr
        self.containTableView.reloadData()
    }
    
    func showRecView() {
        
        self.view.addSubview(self.recHud)
        self.recHud.startRecTime()
        self.recHud.snp.makeConstraints { (make) in
            make.center.equalTo(self.view)
            make.width.equalTo(120)
            make.height.equalTo(120)
        }
        
    }
    
    func dismissRecHud() {
        self.recHud.endRecTime()
        self.recHud.removeFromSuperview()
        
    }
    
    func configerUploadInfor() {
        
        sendNavItem!.isEnabled = false
        
        //清空附件数组
        voiceArr.removeAll()
        imageArr.removeAll()
        videoArr.removeAll()
        
        /// 提取音频文件 到音频上传数组
        for model in self.mVoiceArr {
            if !model.isAddNewItem {
                voiceArr.append(model)
            }
        }
        
        /// 提取视频/图片文件 到视频/图片上传数组
        for model in self.mAnnexArr{

            if !model.isAddNewItem {
            
                if model.annexType == 1 {
                    //图片
                    let data = model.imageSource!.jpegData(compressionQuality: 0.1)!
                    model.imageSource = UIImage.init(data: data)
                    imageArr.append(model)
                    
                }else if model.annexType == 2 {
                    //视频
                    videoArr.append(model)
                    
                }
            }
        }
        
        if self.videoArr.count != 0{
            NeatHud.showLoading(view: self.view, message: "正在进行视频转码...")
            sourceDecode(isVideo: true, isAudio: false)
        }else{
            self.fileTransformFinish()
        }

    }
    
    func sourceDecode(isVideo:Bool,isAudio:Bool) {
        
        DispatchQueue.global().async {
            
            if isVideo{
                
                for videoModel in self.videoArr{
                    VideoFormatTransformTool.moveFileTransformToMp4WithSourceUrl(sourceUrl: videoModel.sourceFile!, finish: { (url) in
                        
                        videoModel.sourceFile = url
                        self.mediaSemaphore.signal()
                        
                    }) {
                        ///失败
                    
                    }
                    self.mediaSemaphore.wait()
                }
                
                debugPrint("视频转码完成")
                
            }
            
            self.fileTransformFinish()
            
        }
        
        
    }
    
    func fileTransformFinish() {
        
        let eventId = self.alarmModel?.id
        let eventHandle = self.handleResult.uploadInfor!
        let content = self.editText.uploadInfor!
        
        if eventHandle.isEmpty {
            DispatchQueue.main.async {
                self.sendNavItem?.isEnabled = true
                NeatHud.showMessage(message: "请先选择处理结果", view: self.view)
            }
            return
        }
        
        if videoArr.count == 0 && imageArr.count == 0 && voiceArr.count == 0 && content.isEmpty{
            DispatchQueue.main.async {
                self.sendNavItem!.isEnabled = true
                NeatHud.showMessage(message: "请至少上传语音、图片、视频或隐患内容其中一种信息", view: self.view)
            }
            return
        }
        
        DispatchQueue.main.async {
            NeatHud.showLoading(view: self.view, message: "正在上传处理信息...")
        }
        
        NeatRequest.instance.alarmHandle(eventId: eventId!, eventHandle: eventHandle, content: content, systemCategory: String(deviceStatusModel!.systemCategory!), videoArr: videoArr,imageArr:imageArr, voiceArr: voiceArr, success: { (response) in
            
            DispatchQueue.main.async {
              
                NeatHud.showMessage(message: response.message ?? "警情处理成功", view: self.view) {
                    self.delegate?.didFinishHandle()
                    self.navigationController?.popViewController(animated: true)
                    self.sendNavItem?.isEnabled = true
                }
                
            }
        }) { (error) in
            
            DispatchQueue.main.async {
                self.sendNavItem?.isEnabled = true
                let errorMsg = String(error.errCode) + error.errMessage
                NeatHud.showMessage(message: errorMsg, view: self.view)
            }
            
        }
    
    }
    
    /// 重置设备数据源数组
    func restDeviceInforData() {
        
        currentDataArr.removeAll()
        
        for (index,modelItem) in dataArr.enumerated() {
                
            if index <= 3 {
                currentDataArr.append(modelItem)
            }
            
        }
        
    }
    /// 加载全部设备数据
    func loadMoreDeviceInforData() {
        
        currentDataArr.removeAll()
        
        currentDataArr.append(contentsOf: dataArr)
        
    }

}

extension LogHandleViewController:UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if dataArr.count == 0 {
            
            if indexPath.section == 0 {
                return NormalConstant.DeviceBasicViewHeight
            }else if indexPath.section == 1{
                return NormalConstant.DeviceInforContainSepHeight
            }else{
                
                let model = mDataSourceArr[indexPath.row]
                
                if model.itemStyle == .NormalStyle {
                    return 44
                }else if model.itemStyle == .RadioStyle {
                    return 78
                }else if model.itemStyle == .TextViewStyle {
                    return 98
                }else if model.itemStyle == .MedialStyle{
                    let itemWidth = (NormalConstant.ScreenWidth - 6*(4 - 1 + 2))/4
                    if model.annexArr.count>=1 && model.annexArr.count <= 4 {
                        return itemWidth + 6 + 36
                    }else{
                        return (itemWidth + 6)*2 + 36
                    }
                }else{
                    return CGFloat(48) * CGFloat(model.annexArr.count) + 36
                }
                
            }
        
        }else if (dataArr.count <= 4) {
            
            if indexPath.section == 0 {
                return NormalConstant.DeviceBasicViewHeight
            }else if indexPath.section == 1{
                
                return NormalConstant.DeviceDataViewHeight
              
            }else if indexPath.section == 2 {
                return NormalConstant.DeviceInforContainSepHeight
            }else{
                
                let model = mDataSourceArr[indexPath.row]
                
                if model.itemStyle == .NormalStyle {
                    return 44
                }else if model.itemStyle == .RadioStyle {
                    return 78
                }else if model.itemStyle == .TextViewStyle {
                    return 98
                }else if model.itemStyle == .MedialStyle{
                    let itemWidth = (NormalConstant.ScreenWidth - 6*(4 - 1 + 2))/4
                    if model.annexArr.count>=1 && model.annexArr.count <= 4 {
                        return itemWidth + 6 + 36
                    }else{
                        return (itemWidth + 6)*2 + 36
                    }
                }else{
                    return CGFloat(48) * CGFloat(model.annexArr.count) + 36
                }
                
            }
            
        }else{
            if indexPath.section == 0 {
                return NormalConstant.DeviceBasicViewHeight
            }else if indexPath.section == 1{
                let dataRow = currentDataArr.count % 4
                let row = currentDataArr.count/4
                if dataRow == 0 {
                   return CGFloat(row) * NormalConstant.DeviceDataViewHeight
                }else{
                    if currentDataArr.count > 4 {
                        let row = currentDataArr.count/4 + 1
                        return CGFloat(row) * NormalConstant.DeviceDataViewHeight
                    }else{
                        return NormalConstant.DeviceDataViewHeight
                    }
                }
                
            }else if indexPath.section == 2{
                
                return NormalConstant.DeviceInforContainMoreHeight
                
            }else if indexPath.section == 3{
                
                return NormalConstant.DeviceInforContainSepHeight

            }else{
                
                let model = mDataSourceArr[indexPath.row]
                
                if model.itemStyle == .NormalStyle {
                    return 44
                }else if model.itemStyle == .RadioStyle {
                    return 78
                }else if model.itemStyle == .TextViewStyle {
                    return 98
                }else if model.itemStyle == .MedialStyle{
                    let itemWidth = (NormalConstant.ScreenWidth - 6*(4 - 1 + 2))/4
                    if model.annexArr.count>=1 && model.annexArr.count <= 4 {
                        return itemWidth + 6 + 36
                    }else{
                        return (itemWidth + 6)*2 + 36
                    }
                }else{
                    return CGFloat(48) * CGFloat(model.annexArr.count) + 36
                }
                
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        
        return 0.01
        
    }
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        
        return UIView.init()
    }
    
    
    
}
extension LogHandleViewController:UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        if dataArr.count == 0 {
            return 3
        }else if (dataArr.count <= 4){
            return 4
        }else{
            return 5
        }
    
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let model = mDataSourceArr[indexPath.row]
        
        
        if currentDataArr.count == 0 {
            
            if indexPath.section == 0 {
                let cell = tableView.dequeueReusableCell(withIdentifier: DeviceInforCellIdent, for: indexPath) as! DeviceStateTableViewCell
                            cell.configerCell(model: self.deviceBasicCellModel)
                cell.delegate = self
                return cell
            }else if indexPath.section == 1 {
                
                let cell = tableView.dequeueReusableCell(withIdentifier: self.SepCellIdent, for: indexPath) as! SepViewTableViewCell
                return cell
                
            }else{
                
                if model.itemStyle == .NormalStyle{
                    
                    let cell = tableView.dequeueReusableCell(withIdentifier: ReportNormalInforTableViewCellIdent, for: indexPath) as! ReportNormalInforTableViewCell
                    cell.configerCell(model: model)
                    cell.selectionStyle = .none
                    return cell
                    
                }else if model.itemStyle == .RadioStyle{
                    
                    let cell = tableView.dequeueReusableCell(withIdentifier: LogHandleResultTableViewCellIdent, for: indexPath) as! LogHandleResultTableViewCell
                    cell.delegate = self
                    cell.configerCell(model: model)
                    cell.selectionStyle = .none
                    return cell
                    
                }else if model.itemStyle == .TextViewStyle{
                    
                    let cell = tableView.dequeueReusableCell(withIdentifier: ReportTextInforTableViewCellIdent, for: indexPath) as! ReportTextInforTableViewCell
                    cell.configerCell(model: model)
                    cell.selectionStyle = .none
                    return cell
                    
                }else if model.itemStyle == .MedialStyle {
                    
                    let cell = tableView.dequeueReusableCell(withIdentifier: ReportMediaTableViewCellIdent, for: indexPath) as! ReportMediaTableViewCell
                    cell.delegate = self
                    cell.configerCell(model: model)
                    cell.selectionStyle = .none
                    return cell
                    
                }else{
                    
                    let cell = tableView.dequeueReusableCell(withIdentifier: VoiceTableViewCellIdent, for: indexPath) as! VoiceTableViewCell
                    cell.delegate = self
                    cell.configerCell(mode: model)
                    cell.selectionStyle = .none
                    return cell
                    
                }
                
            }
            
        }else if (dataArr.count <= 4){
            
            if indexPath.section == 0 {
                let cell = tableView.dequeueReusableCell(withIdentifier: DeviceInforCellIdent, for: indexPath) as! DeviceStateTableViewCell
                            cell.configerCell(model: self.deviceBasicCellModel)
                cell.delegate = self
                return cell
            }else if indexPath.section == 1 {
                let cell = tableView.dequeueReusableCell(withIdentifier: self.DataCellIdent, for: indexPath) as! DeviceDataInforTableViewCell
                cell.configerCell(models: currentDataArr)
                return cell
            }else if indexPath.section == 2 {
                let cell = tableView.dequeueReusableCell(withIdentifier: self.SepCellIdent, for: indexPath) as! SepViewTableViewCell
                return cell
            }else{
                
                if model.itemStyle == .NormalStyle{
                    
                    let cell = tableView.dequeueReusableCell(withIdentifier: ReportNormalInforTableViewCellIdent, for: indexPath) as! ReportNormalInforTableViewCell
                    cell.configerCell(model: model)
                    cell.selectionStyle = .none
                    return cell
                    
                }else if model.itemStyle == .RadioStyle{
                    
                    let cell = tableView.dequeueReusableCell(withIdentifier: LogHandleResultTableViewCellIdent, for: indexPath) as! LogHandleResultTableViewCell
                    cell.delegate = self
                    cell.configerCell(model: model)
                    cell.selectionStyle = .none
                    return cell
                    
                }else if model.itemStyle == .TextViewStyle{
                    
                    let cell = tableView.dequeueReusableCell(withIdentifier: ReportTextInforTableViewCellIdent, for: indexPath) as! ReportTextInforTableViewCell
                    cell.configerCell(model: model)
                    cell.selectionStyle = .none
                    return cell
                    
                }else if model.itemStyle == .MedialStyle {
                    
                    let cell = tableView.dequeueReusableCell(withIdentifier: ReportMediaTableViewCellIdent, for: indexPath) as! ReportMediaTableViewCell
                    cell.delegate = self
                    cell.configerCell(model: model)
                    cell.selectionStyle = .none
                    return cell
                    
                }else{
                    
                    let cell = tableView.dequeueReusableCell(withIdentifier: VoiceTableViewCellIdent, for: indexPath) as! VoiceTableViewCell
                    cell.delegate = self
                    cell.configerCell(mode: model)
                    cell.selectionStyle = .none
                    return cell
                    
                }
                
            }
            
        }else{
            
            if indexPath.section == 0 {
                let cell = tableView.dequeueReusableCell(withIdentifier: DeviceInforCellIdent, for: indexPath) as! DeviceStateTableViewCell
                            cell.configerCell(model: self.deviceBasicCellModel)
                cell.delegate = self
                return cell
            }else if indexPath.section == 1 {
                let cell = tableView.dequeueReusableCell(withIdentifier: self.DataCellIdent, for: indexPath) as! DeviceDataInforTableViewCell
                cell.configerCell(models: currentDataArr)
                return cell
                
            }else if indexPath.section == 2 {
                let cell = tableView.dequeueReusableCell(withIdentifier: self.MoreCellIdent, for: indexPath) as! MoreTableViewCell
                cell.delegate = self
                return cell
            }else if indexPath.section == 3 {
                let cell = tableView.dequeueReusableCell(withIdentifier: self.SepCellIdent, for: indexPath) as! SepViewTableViewCell
                return cell
                
            }else{
                
                if model.itemStyle == .NormalStyle{
                    
                    let cell = tableView.dequeueReusableCell(withIdentifier: ReportNormalInforTableViewCellIdent, for: indexPath) as! ReportNormalInforTableViewCell
                    cell.configerCell(model: model)
                    cell.selectionStyle = .none
                    return cell
                    
                }else if model.itemStyle == .RadioStyle{
                    
                    let cell = tableView.dequeueReusableCell(withIdentifier: LogHandleResultTableViewCellIdent, for: indexPath) as! LogHandleResultTableViewCell
                    cell.delegate = self
                    cell.configerCell(model: model)
                    cell.selectionStyle = .none
                    return cell
                    
                }else if model.itemStyle == .TextViewStyle{
                    
                    let cell = tableView.dequeueReusableCell(withIdentifier: ReportTextInforTableViewCellIdent, for: indexPath) as! ReportTextInforTableViewCell
                    cell.configerCell(model: model)
                    cell.selectionStyle = .none
                    return cell
                    
                }else if model.itemStyle == .MedialStyle {
                    
                    let cell = tableView.dequeueReusableCell(withIdentifier: ReportMediaTableViewCellIdent, for: indexPath) as! ReportMediaTableViewCell
                    cell.delegate = self
                    cell.configerCell(model: model)
                    cell.selectionStyle = .none
                    return cell
                    
                }else{
                    
                    let cell = tableView.dequeueReusableCell(withIdentifier: VoiceTableViewCellIdent, for: indexPath) as! VoiceTableViewCell
                    cell.delegate = self
                    cell.configerCell(mode: model)
                    cell.selectionStyle = .none
                    return cell
                    
                }
                
            }
            
        }
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if dataArr.count == 0 {
            if section == 2 {
                return mDataSourceArr.count
            }else{
                return 1
            }
        }else if dataArr.count <= 4 {
            if section == 3 {
                return mDataSourceArr.count
            }else{
                return 1
            }
        }else{
            if section == 4 {
                return mDataSourceArr.count
            }else{
                return 1
            }
        }
        
    }
    
}

extension LogHandleViewController:MoreTableViewCellDelegate{
    
    func didClickMore(isMore: Bool) {
        if isMore {
            
            self.loadMoreDeviceInforData()
            
            let indexSet = IndexSet.init(integer: 1)
            
            self.containTableView.reloadSections(indexSet, with: .automatic)
        }else{
            
            self.restDeviceInforData()
            
            let indexSet = IndexSet.init(integer: 1)
            
            self.containTableView.reloadSections(indexSet, with: .automatic)
        }
    }
    
    
    
    
}

extension LogHandleViewController:ReportMediaTableViewCellDelegate{
    
    func didSelectItem(model: AnnexModel) {
        //点击
        //先判断是否还能添加 如果已满8个则添加按钮消失
        if model.isAddNewItem {
            
            self.imagePickerPresent()
            
        }else{
            //查看图片 或 视频
            let detailVC = MediaPlayViewController.init()
            detailVC.annexModel = model
            self.navigationController?.pushViewController(detailVC, animated: true)
        }
    }
    
    func didLongSelectItem(model:AnnexModel,indexPath: IndexPath) {
        //长按
        //如果不是添加按钮
        if !model.isAddNewItem {
            var isHaveAdd = false
            for item in mAnnexArr {
                if item.isAddNewItem {
                    isHaveAdd = true
                }
            }
            
            if isHaveAdd  {
                //如果少于8个 则删除对应图片即可
                mAnnexArr.remove(at: indexPath.item)
            }else{
                //如果当前是8个附件 则删除后要添加添加按钮
                mAnnexArr.remove(at: indexPath.item)
                mAnnexArr.append(addMediaModel)
            }
            
            self.updateAnnex()
            
        }
        
        
    }
    
}

extension LogHandleViewController:AudioRecHudDelegate{
    
    func recTimeOut() {
        self.endRecVoice()
    }
    
}

extension LogHandleViewController:VoiceTableViewCellDelegate{
    
    func clickRecVoice(model: AnnexModel, index: IndexPath, cell: VoiceItemTableViewCell) {
        
        if lastRadioClickIndex != nil {
            //有记录之前点击过

            if index == lastRadioClickIndex {
                //两次点击相同音频
                if audioTool.isPlay {
                    audioTool.stopPlayRec()
                    cell.voice_image_view.stopAnimating()
                }else{
                    self.playRecVoice(model: model)
                    cell.voice_image_view.startAnimating()
                }
            }else{
                //两次不同
                if audioTool.isPlay {
                    audioTool.stopPlayRec()
                    lastVoiceItemCell?.voice_image_view.stopAnimating()
                    self.playRecVoice(model: model)
                    cell.voice_image_view.startAnimating()
                }else{
                    self.playRecVoice(model: model)
                    cell.voice_image_view.startAnimating()
                }
            }

            lastVoiceItemCell = cell
            lastRadioClickIndex = index

        }else{
            //首次点击
            lastRadioClickIndex = index
            lastVoiceItemCell = cell
            self.playRecVoice(model: model)
            cell.voice_image_view.startAnimating()
        }
        

        
    }
    /// 播放录音
    func playRecVoice(model: AnnexModel) {
        
        audioTool.playRec(model: model, finishPlay: {
            self.lastVoiceItemCell?.voice_image_view.stopAnimating()
        }) {
            self.lastVoiceItemCell?.voice_image_view.stopAnimating()
            NeatHud.showMessage(message: "播放失败", view: self.view)
        }
        
     }

    
    /// 开始录音
    func startRecVoice() {

        if audioTool.isPlay {
            AudioRecorderTool.sharedInstance.stopPlayRec()
        }
        
        audioTool.startRec(url: FileManagerTool.directoryURL(), start: {
            self.showRecView()
        }) { (isPermissionFalied, recFailed) in
            if isPermissionFalied {
                self.showAlert(vc: self, title: "提示", message: "尚未取得麦克风使用权限,无法使用语音功能。是否去设置？", leftTitle: "取消", rightTitle: "去设置", leftAction: { (action) in
                    
                    
                }) { (action) in
                    //去设置
                    let settingsUrl = URL(string:UIApplication.openSettingsURLString)
                    if #available(iOS 10.0, *) {
                        UIApplication.shared.open(settingsUrl!, options: [:], completionHandler: nil)
                    } else {
                        
                    }
                }
            }
            if recFailed {
                NeatHud.showMessage(message: "开启录音失败，稍后再试", view: self.view)
            }
        }
        
    }
    
    func endRecVoice() {
        
        self.dismissRecHud()
        
        audioTool.stopRec(finish: { (model) in

            //判断已有附件数量 如果多余8个则移除添加按钮 少于8个添加添加按钮
            if self.mVoiceArr.count == 3 {
                self.mVoiceArr.insert(model, at: 0)
                self.mVoiceArr.removeLast()
            }else{
                self.mVoiceArr.insert(model, at: 0)
            }
            self.updateAudio()

        }) { (error) in
            NeatHud.showMessage(message: error, view: self.view)
        }
 
    }
    
    func deleteRecVoice(model: AnnexModel, index: IndexPath) {
        
        if audioTool.isPlay {
            audioTool.stopPlayRec()
        }
        
        if !model.isAddNewItem {
            var isHaveAdd = false
            for item in mVoiceArr {
                if item.isAddNewItem {
                    isHaveAdd = true
                }
            }
            
            if isHaveAdd  {
                //如果少于3个 则删除对应图片即可
                mVoiceArr.remove(at: index.item)
            }else{
                //如果当前是3个附件 则删除后要添加添加按钮
                mVoiceArr.remove(at: index.item)
                mVoiceArr.append(addVoiceModel)
            }
            
            self.updateAudio()
            
        }
        
    }
    
}

extension LogHandleViewController:LogHandleResultTableViewCellDelegate{
    
    func confirmRadioBtnChange(currentModel: ReportItemModel) {
        
        self.handleResult.uploadInfor = currentModel.inforText
        
        if currentModel.inforText == "2" {
            //真实警情
            self.editText.inforText = "真实警情"
            self.editText.uploadInfor = "真实警情"
            
        }else if currentModel.inforText == "3"{
            //系统测试
            self.editText.inforText = "系统测试"
            self.editText.uploadInfor = "系统测试"
        
        }else if currentModel.inforText == "4"{
            //设备误报
            self.editText.inforText = "设备误报"
            self.editText.uploadInfor = "设备误报"
        
        }
        
        self.containTableView.reloadData()
    }
    
    
}

extension LogHandleViewController:DeviceStateTableViewCellDelegate{
    
    func refreshClick() {
        self.perform(#selector(getDeviceStatus(isShowLoading:)), with: self, afterDelay: 1)
    }
    
    func functionClick() {
        if deviceInfor.deviceCategory == DeviceModel.NBDevice.Probe {
            
            //消音
            NeatRequest.instance.postNBCmute(gid: (self.deviceStatusModel?.deviceCode)!, cid: (self.deviceStatusModel?.deviceCode)!, success: { (response) in
                
                NeatHud.showMessage(message: response.message ?? "消音指令下发成功!", view: self.view)
                
            }) { (errorResponse) in
                NeatHud.showMessage(message: errorResponse.errMessage, view: self.view)
            }
            
        }
    }
    
    
}

//extension LogHandleViewController:DeviceInforTableViewCellDelegate{
//
//    func startRefresh(inforModel: DeviceInforViewModel) {
//
//    }
//
//    func functionBtnClick(inforModel: DeviceInforViewModel) {
//
//        if inforModel.deviceCategory == DeviceCategory.NBProbe.Smoke || inforModel.deviceCategory == DeviceCategory.NBProbe.Temperature{
//
//            //消音
//            NeatRequest.instance.postNBCmute(gid: (self.deviceStatusModel?.deviceCode)!, cid: (self.deviceStatusModel?.deviceCode)!, success: { (response) in
//
//                NeatHud.showMessage(message: response.message ?? "消音指令下发成功!", view: self.view)
//
//            }) { (errorResponse) in
//                NeatHud.showMessage(message: errorResponse.errMessage, view: self.view)
//            }
//
//        }
//    }
//
//    func chancelClick(inforModel: DeviceInforViewModel) {
//
//    }
//
//}
