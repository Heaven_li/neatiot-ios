//
//  LogHandleResultTableViewCell.swift
//  NeatIoT
//
//  Created by Yang Blus on 2020/2/10.
//  Copyright © 2020 Neat. All rights reserved.
//

import UIKit

protocol LogHandleResultTableViewCellDelegate {
    func confirmRadioBtnChange(currentModel:ReportItemModel)
}

class LogHandleResultTableViewCell: UITableViewCell {
    
    var currentModel:ReportItemModel?
    var delegate:LogHandleResultTableViewCellDelegate?
        
    lazy var titleLable: UILabel = {
        let lable = UILabel.init()
        lable.font = UIFont.systemFont(ofSize: 15)
        lable.textColor = #colorLiteral(red: 0.121294491, green: 0.1292245686, blue: 0.141699791, alpha: 1)
        return lable
    }()

    
    lazy var checkView: CheckRadioGroupView = {
        
        let checkView = CheckRadioGroupView.init()
        checkView.isMuitleCheck = false
        checkView.delegate = self
        
        return checkView
    }()

    override func awakeFromNib() {
        
        super.awakeFromNib()
        
        self.addSubview(self.titleLable)

        self.titleLable.snp.makeConstraints { (make) in
            make.leading.equalTo(self).offset(16)
            make.top.equalTo(self).offset(10)
            make.width.equalTo(120)
        }
        
        self.addSubview(self.checkView)
        
        checkView.snp.makeConstraints { (make) in
        
            make.top.equalTo(self.titleLable.snp.bottom).offset(10)
            make.bottom.equalTo(self).offset(-10)
            make.leading.equalTo(self).offset(16)
            make.width.equalTo(300)
            
        }
        
        let itemOne = CheckRadioItemModel.init()
        itemOne.isRadioCheck = true
        itemOne.radioTitle = "真实警情"
        itemOne.radioKey = "2"
        
        let itemTwo = CheckRadioItemModel.init()
        itemTwo.isRadioCheck = false
        itemTwo.radioTitle = "系统测试"
        itemTwo.radioKey = "3"
        
        let itemThird = CheckRadioItemModel.init()
        itemThird.isRadioCheck = false
        itemThird.radioTitle = "设备误报"
        itemThird.radioKey = "4"
        
        checkView.radioItemArr = [itemOne,itemTwo,itemThird]
        
        
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configerCell(model:ReportItemModel) {
        
        currentModel = model
        
        self.titleLable.text = currentModel!.titleText
        
    }
    
}

extension LogHandleResultTableViewCell:CheckRadioGroupViewDelegate{
    
    func checkRadioGroupValueChanged(checkRadioItems: [CheckRadioItemModel]) {
        
        for item in checkRadioItems {
                   
            debugPrint("当前选中项目：",item.radioKey,item.radioTitle)
            currentModel?.inforText = item.radioKey
            currentModel?.uploadInfor = item.radioKey
            self.delegate?.confirmRadioBtnChange(currentModel: currentModel!)
                   
        }
        
    }
    
}


