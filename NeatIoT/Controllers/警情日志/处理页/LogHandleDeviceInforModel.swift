//
//  LogHandleViewModel.swift
//  NeatIoT
//
//  Created by Yang Blus on 2020/6/2.
//  Copyright © 2020 Neat. All rights reserved.
//

import UIKit

class LogHandleDeviceInforModel: NSObject {
    /// 联网状态 在线
    static let online = 2
    /// 联网状态 离线
    static let offline = 1
    
    /// 设备id
    var deviceID:String = ""
    
    /// ICON地址URL
    var iconURL:String = ""
    
    /// 设备编码
    var deviceCode:String = ""
    
    /// 设备名称
    var deviceName:String = ""
    
    /// 设备状态
    var deviceState:Int = 255
    
    /// 设备状态 文字
    var deviceStateStr:String?
    
    /// 设备类型
    var deviceCategory:String = ""
    
    /// 电量
    var deviceBattery:String = ""
    
    /// 联网状态 在线是2 离线是1
    var deviceOnline:Int = 1
    
    /// 通道或设备数
    var deviceChannle:String = ""
    
    /// 信号
    var deviceSignal:String = ""
    
    /// 实时值
    var realTimeValue:String = ""
    
    /// 添加火主机 或 水信号是用到的企业id
    var enterpriseID:String = ""
    
    /// 父器件id
    var deviceFatherID:String = ""
    
    /// 设备展示数据项数组
    var dataItems:Array<DeviceDataInforModel>? = []
    
    /// 功能按钮是否可点击 true 可用 false 不可用
    var functionBtnAble:Bool = false

    /// 功能按钮不可用图片名称
    var functionBtnEnAbleImageName:String = ""
    
    /// 功能按钮可用图片名称
    var functionBtnAbleImageName:String = ""
    
    /// 配置联网状态model
    /// - Parameter response: 返回设备信息
    /// - Returns: model
    class func getConnectStateModel(response:InspectionLogDeviceStatusResponse) -> DeviceDataInforModel{
        
        let onLineState = getOnlineState(onlineState: response.deviceStatusModel?.onlineStatus ?? 0)
        
        let onLineModel = DeviceDataInforModel.init()
        onLineModel.titleText = "联网状态"
        onLineModel.inforText = onLineState.onLineText
        onLineModel.inforTextColor = onLineState.onLineColor
        
        return onLineModel
        
    }
    
    class func getSignalStateModel(response:InspectionLogDeviceStatusResponse) -> DeviceDataInforModel {
        
        let iconName = getSignalIcon(signalStr:String(response.deviceStatusModel?.signalStatus ?? 0))
        
        let signalModel = DeviceDataInforModel.init()
        signalModel.titleText = "信号强度"
        signalModel.inforImageName = iconName
        
        return signalModel
    }
    
    class func getBatteryStateModel(response:InspectionLogDeviceStatusResponse) -> DeviceDataInforModel {
        
        let batteryModel = DeviceDataInforModel.init()
        batteryModel.titleText = "剩余电量"
        
        if (response.deviceStatusModel?.batteryStatus != nil) && (!(response.deviceStatusModel?.batteryStatus?.isEmpty ?? true)) {
            batteryModel.inforText = (response.deviceStatusModel?.batteryStatus ?? "-") + "%"
        }else{
            batteryModel.inforText = "-%"
        }
    
        return batteryModel
        
    }
    
    class func getSubNumModel(category:String,response:InspectionLogDeviceStatusResponse) -> DeviceDataInforModel {
        
        if category == DeviceModel.FireSystem.uitd_NT9009 {
            
            let systemCount = DeviceDataInforModel.init()
            systemCount.titleText = "消防主机数"
            systemCount.inforText = String(response.deviceStatusModel?.hostCount ?? 0)
            return systemCount
            
        }else if category == DeviceModel.WaterSystem.wireless_NT8327{
            
            let systemCount = DeviceDataInforModel.init()
            systemCount.titleText = "水信号数"
            systemCount.inforText = String(response.deviceStatusModel?.hostCount ?? 0)
            return systemCount
            
        }else if category == DeviceModel.SmartElectricitySystem.SmartPowerGateway{
            
            let systemCount = DeviceDataInforModel.init()
            systemCount.titleText = "通道数量"
            systemCount.inforText = String(response.deviceStatusModel?.hostCount ?? 0)
            return systemCount
            
        }else if category == DeviceModel.FamilySystem.HomeGateway{
            
            let deviceCount = DeviceDataInforModel.init()
            deviceCount.titleText = "器件数"
            deviceCount.isUserClickEnable = true
            deviceCount.inforText = String.init(format: "%d", response.deviceStatusModel!.hostCount ?? 0)
            return deviceCount
            
        }else{
            return DeviceDataInforModel()
        }
        
    }
    
    
    class func formateModelByResponse(response:InspectionLogDeviceStatusResponse,category:String) -> LogHandleDeviceInforModel {
        
        let model = LogHandleDeviceInforModel.init()
        
        model.deviceID = response.deviceStatusModel?.id ?? ""
        model.iconURL = response.deviceStatusModel?.pictureUrl ?? ""
        model.deviceName = response.deviceStatusModel?.name ?? ""
        model.deviceState = response.deviceStatusModel?.alarmStatus ?? 255
        model.deviceCategory = category
        
        
        if category == DeviceModel.FireSystem.uitd_NT9009 {
            //一体化传输装置
            let onLineModel = getConnectStateModel(response: response)
            
            let systemCount = getSubNumModel(category: category, response: response)
            
            model.dataItems?.append(onLineModel)
            model.dataItems?.append(systemCount)
            
        }else if category == DeviceModel.FireSystem.IntegratedTransmissionHost ||
            category == DeviceModel.FireSystem.IntegratedDevice{
            //火主机 火器件
            
        }else if category == DeviceModel.WaterSystem.wireless_NT8327{
            //水网关
            let signalModel = getSignalStateModel(response: response)
            
            let onLineModel = getConnectStateModel(response: response)
            
            let systemCount = getSubNumModel(category: category, response: response)
            
            model.dataItems?.append(signalModel)
            model.dataItems?.append(onLineModel)
            model.dataItems?.append(systemCount)
            
            
        }else if category == DeviceModel.WaterSystem.NeatWaterSingal{
            //水信号
//            model.realTimeValue = response.deviceInfor?.value ?? "1"
            
        }else if category == DeviceModel.WaterSystem.Wgw_NT8161B || category == DeviceModel.WaterSystem.Wgw_NT8164B ||
            category == DeviceModel.WaterSystem.Wgw_NT8161A || category == DeviceModel.WaterSystem.Wgw_NT8164A ||
            category == DeviceModel.WaterSystem.Wgw_NT8167A{
            //一体式水源
            let signalModel = getSignalStateModel(response: response)
            model.dataItems?.append(signalModel)
            
            let onLineModel = getConnectStateModel(response: response)
            model.dataItems?.append(onLineModel)
            
            let batteryModel = getBatteryStateModel(response: response)
            
            model.dataItems?.append(batteryModel)
            
            model.realTimeValue = String(response.deviceStatusModel?.curretnValue ?? 0.0)
            
        }else if category == DeviceModel.SmartElectricitySystem.SmartPowerGateway{
            //智慧用电网关
            let onLineModel = getConnectStateModel(response: response)
            
            let systemCount = getSubNumModel(category: category, response: response)

            model.dataItems?.append(onLineModel)
            model.dataItems?.append(systemCount)
            
            if model.deviceOnline != offline {
                //非离线状态可交互
                model.functionBtnAble = true
                model.functionBtnAbleImageName = "mainBtnAble"
                model.functionBtnEnAbleImageName = "mainBtnEnable"
            }else{
                model.functionBtnAble = false
                model.functionBtnAbleImageName = "mainBtnAble"
                model.functionBtnEnAbleImageName = "mainBtnEnable"
            }
            
            model.realTimeValue = String(response.deviceStatusModel?.curretnValue ?? 0.0)

            
        }else if category == DeviceModel.NBDevice.Probe{
            //NB设备
            let signalModel = getSignalStateModel(response: response)
            
            model.dataItems?.append(signalModel)
            
            let onLineModel = getConnectStateModel(response: response)
            
            model.dataItems?.append(onLineModel)
            
            let batteryModel = getBatteryStateModel(response: response)
            
            model.dataItems?.append(batteryModel)
            
            if model.deviceState != AlarmStateType.Fire {
                //非火警状态不可消音
                model.functionBtnAbleImageName = "selenceBtnAble"
                model.functionBtnEnAbleImageName = "selenceBtnEnable"
                model.functionBtnAble = false
            }else{
                model.functionBtnAbleImageName = "selenceBtnAble"
                model.functionBtnEnAbleImageName = "selenceBtnEnable"
                model.functionBtnAble = true
                
            }
            
            
            
        }else if category == DeviceModel.SmartElectricitySystem.ElectricGateway_NT8127A ||
            category == DeviceModel.SmartElectricitySystem.ElectricGateway_NT8128A{
            //8127A 8128A
            let onLineModel = getConnectStateModel(response: response)
            
            model.dataItems?.append(onLineModel)
            
//            for item in response.deviceInfor?.dataItems ?? [] {
//                
//                let infor = DeviceDataInforModel.init()
//                infor.titleText = item.itemName!
//                infor.inforText = item.itemValue! + item.itemUnit!
//                
//                model.dataItems?.append(infor)
//                
//                
//            }
            
        }else if category == DeviceModel.FamilySystem.HomeGateway{
            //8140
            
            let signalModel = getSignalStateModel(response: response)
            
            model.dataItems?.append(signalModel)
            
            let onLineModel = getConnectStateModel(response: response)
            
            model.dataItems?.append(onLineModel)
            
//            let batteryModel = getBatteryStateModel(response: response)
            
//            model.dataItems?.append(batteryModel)
            
            let deviceCount = getSubNumModel(category: category, response: response)
            
            model.dataItems?.append(deviceCount)
            
        }else if category == DeviceModel.FamilySystem.HomeGatewayDevice{
            
            ///8140下器件
           
            let signalModel = getSignalStateModel(response: response)
        
            model.dataItems?.append(signalModel)
            
            let batteryModel = getBatteryStateModel(response: response)
            
            model.dataItems?.append(batteryModel)
            
            model.functionBtnAbleImageName = "cs_able"
            model.functionBtnEnAbleImageName = "cs_able"
            model.functionBtnAble = true
            
        }else if category == DeviceModel.GasDetector.HM710NVM{
            
            let signalModel = getSignalStateModel(response: response)
        
            model.dataItems?.append(signalModel)
            
            let onLineModel = getConnectStateModel(response: response)

            model.dataItems?.append(onLineModel)
            
        }else{
            
        }
    
        return model
        
    }
    
    class func getSignalIcon(signalStr:String) -> String {
        
            /** 信号强度改为最多4格 */
            let signal = Float(signalStr)
            if signal == 0.0 {
                return "signal_0"
            }else if (signal == 1.0) {
                return "signal_1"
            } else if (signal == 2.0) {
                return "signal_2"
            } else if (signal == 3.0) {
                return "signal_3"
            } else if (signal == 4.0) {
                return "signal_4"
            }else {
                return "signal_0"
            }
        }
    
    
    
    class func getOnlineState(onlineState:Int) -> (onLineText:String,onLineColor:UIColor) {
        
        var inforText = ""
        var inforTextColor = #colorLiteral(red: 0.8, green: 0.8, blue: 0.8, alpha: 1)
        if onlineState == online{
            //在线
            inforText = "在线"
            inforTextColor = #colorLiteral(red: 0.4666666667, green: 0.8745098039, blue: 0.4745098039, alpha: 1)
        }else if onlineState == offline{
            //离线
            inforText = "离线"
            inforTextColor = #colorLiteral(red: 0.8, green: 0.8, blue: 0.8, alpha: 1)
        }
        return (inforText,inforTextColor)
    }
    
    
    
    
    

}
