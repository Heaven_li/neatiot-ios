//
//  InspectionAlarmHandleDetailDeviceTableViewCell.swift
//  NeatIoT
//
//  Created by Yang Blus on 2020/2/10.
//  Copyright © 2020 Neat. All rights reserved.
//

import UIKit
import Kingfisher
class InspectionAlarmHandleDetailDeviceTableViewCell: UITableViewCell {

    @IBOutlet weak var deviceBgView: UIView!
    
    @IBOutlet weak var deviceImageView: UIImageView!
    
    @IBOutlet weak var iconBgImageView: UIView!
    
    @IBOutlet weak var titleNameLable: UILabel!
    
    @IBOutlet weak var deviceStateLable: UILabel!
    
    override func awakeFromNib() {
        
        super.awakeFromNib()
        
        let gradientLayer = CAGradientLayer.init()
        gradientLayer.colors = [#colorLiteral(red: 0.01176470588, green: 0.4, blue: 0.5921568627, alpha: 1).cgColor,#colorLiteral(red: 0.007843137255, green: 0.337254902, blue: 0.5568627451, alpha: 1).cgColor]
        gradientLayer.frame = CGRect.init(x: 0, y: 0, width: NormalConstant.ScreenWidth, height: 106)
        gradientLayer.startPoint = CGPoint.init(x: 0, y: 0.5)
        gradientLayer.endPoint = CGPoint.init(x: 1, y: 0.5)
        
        self.contentView.layer.insertSublayer(gradientLayer, at: 0)
        
        self.deviceBgView.layer.cornerRadius = 28
        self.deviceBgView.layer.borderColor = #colorLiteral(red: 0.007843137255, green: 0.337254902, blue: 0.5568627451, alpha: 1).cgColor
        self.deviceBgView.layer.borderWidth = 1
        self.deviceStateLable.layer.cornerRadius = 5
        self.deviceStateLable.clipsToBounds = true
        self.deviceImageView.layer.cornerRadius = 10
        self.deviceImageView.clipsToBounds = true
        
        
        
        
    }
    
    func configerCell(model:DeviceInforViewModel) {
        
        let userInforModel = DataArchiver.unArchiver(key:ArchireKey.UserInfor) as! LoginResultModel
        
        let url = URL.init(string: model.iconURL+"&token="+userInforModel.user_token)
        self.deviceImageView.kf.setImage(with: url, placeholder: UIImage.init(named: "icon_image_ph"))
        
        self.titleNameLable.text = model.deviceName
        
        var stateText = ""
        var stateColor = UIColor.lightGray
        if model.deviceStateStr == "fire" {
            stateText = "火警"
            stateColor = NormalColor.Fire
        }else if model.deviceStateStr == "alarm"{
            stateText = "报警"
            stateColor = NormalColor.Alarm
        }else if model.deviceStateStr == "fault"{
            stateText = "故障"
            stateColor = NormalColor.Fault
        }
        self.deviceStateLable.text = stateText
        self.deviceStateLable.backgroundColor = stateColor
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
