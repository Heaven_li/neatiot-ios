//
//  InspectionAlarmHandleDetailViewController.swift
//  NeatIoT
//
//  Created by Yang Blus on 2020/2/10.
//  Copyright © 2020 Neat. All rights reserved.
//

import UIKit
import AVFoundation



class InspectionAlarmHandleDetailViewController: SupperViewController {

    var alarmModel: AlarmItemModel?
    let DeviceStateTableViewCellIdent = "DeviceStateTableViewCell"
    let ReportNormalInforTableViewCellIdent = "ReportNormalInforTableViewCell"
    let ReportTextInforTableViewCellIdent:String = "ReportTextInforTableViewCell"
    let ReportMediaTableViewCellIdent:String = "ReportMediaTableViewCell"
    let VoiceTableViewCellIdent:String = "VoiceTableViewCell"
    let SepViewTableViewCellIdent:String = "SepViewTableViewCell"
    
    
    /// 数据源数组
    var mDataSourceArr:Array<ReportItemModel> = []
    
    /// 媒体数组
    var mAnnexArr:Array<AnnexModel> = []
    let anxueModel = ReportItemModel.init()
    
    /// 语音数组
    var mVoiceArr:Array<AnnexModel> = []
    let voiceModel = ReportItemModel.init()
    
    var currentAudioPath:URL?
    var lastRadioClickIndex:IndexPath?
    var audioPlayer:AVPlayer?
    var audioItem:AVPlayerItem?
    var lastVoiceItemCell:VoiceItemTableViewCell?
    
    /// 音频播放状态
    var isPalying:Bool = false
    
    /// 处理说明
    lazy var editText: ReportItemModel = {
        /// 处理说明
        let editText = ReportItemModel.init()
        editText.itemStyle = .TextViewStyle
        editText.titleText = "处理说明"
        editText.inforText = ""
        editText.isCanEdit = false
        
        return editText
    }()
    
    /// 处理人
    let personModel = ReportItemModel.init()
    
    /// 处理结果
    let resultModel = ReportItemModel.init()
    
    /// 设备信息
    let deviceInforModel = DeviceBasicInforModel.init()
    
    lazy var containTableView: LZTableView = {
        
        let tableview = LZTableView.init(frame: CGRect.zero, style: UITableView.Style.plain)
        tableview.delegate = self
        tableview.dataSource = self
        tableview.backgroundColor = .clear
        tableview.bounces = false
        tableview.separatorInset = UIEdgeInsets.init(top: 0, left: 6, bottom: 0, right: 6)
        tableview.register(UINib.init(nibName: "ReportNormalInforTableViewCell", bundle: Bundle.main), forCellReuseIdentifier: ReportNormalInforTableViewCellIdent)
        tableview.register(UINib.init(nibName: "SepViewTableViewCell", bundle: Bundle.main), forCellReuseIdentifier: SepViewTableViewCellIdent)
        tableview.register(UINib.init(nibName: "DeviceStateTableViewCell", bundle: Bundle.main), forCellReuseIdentifier: DeviceStateTableViewCellIdent)
        tableview.register(UINib.init(nibName: "ReportTextInforTableViewCell", bundle: Bundle.main), forCellReuseIdentifier: ReportTextInforTableViewCellIdent)
        tableview.register(UINib.init(nibName: "VoiceTableViewCell", bundle: Bundle.main), forCellReuseIdentifier: VoiceTableViewCellIdent)
        tableview.register(ReportMediaTableViewCell.self, forCellReuseIdentifier: ReportMediaTableViewCellIdent)
        return tableview
    }()
    
    override func viewWillDisappear(_ animated: Bool) {
        
        if isPalying {

            lastVoiceItemCell?.voice_image_view.stopAnimating()
            self.audioPlayer?.pause();
            isPalying = false
            if audioItem != nil {
                audioItem!.removeObserver(self, forKeyPath: "status")
                audioItem = nil
            }
            
        }
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.titleName = "警情处理"
        
        self.view.addSubview(self.containTableView)
        self.containTableView.snp.makeConstraints { (make) in
            make.edges.equalTo(self.view)
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(avPlayerItemDidPlayToEndTime), name: .AVPlayerItemDidPlayToEndTime, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(avPlayerItemPlayFailed), name: .AVPlayerItemFailedToPlayToEndTime, object: nil)
        
        getHandleDetail()
    }
    
    func getHandleDetail(_ isRefresh:Bool = false) {
        
        let deviceId = alarmModel!.deviceId
        let eventId = alarmModel!.id
        let deviceIdType = String.init(format: "%d", alarmModel!.deviceIdType!)
        let systemCategory = alarmModel!.systemCategory
        
        if isRefresh {
            NeatHud.showLoading(view: self.view, message: "")
        }else{
            self.containTableView.showLoadingView(UIImage.init(named: "device_detail_load_bg"))
        }
        
        
        NeatRequest.instance.getAlarmHandleDetail(deviceId: deviceId!, eventId: eventId!, deviceIdType: deviceIdType, systemCategory: systemCategory!, success: { (response) in
            if isRefresh {
                NeatHud.dissmissHud(view: self.view)
            }else{
                self.containTableView.hidenHolderView()
            }
            
            self.configerDataSource(response: response)
            
        }) { (errorResponse) in
            if isRefresh {
                NeatHud.dissmissHud(view: self.view)
            }
            
            if errorResponse.errCode == RequestResponseCode.dataParsingException {
                //数据异常
                self.containTableView.showRequestFaildView {
                    
                }
            }else if errorResponse.errCode == RequestResponseCode.netCanNotContent || errorResponse.errCode == RequestResponseCode.requestTimeOut || errorResponse.errCode == RequestResponseCode.canNotContentHost{
                //无网络
                self.containTableView.showNetErrorView(errorMessage: errorResponse.errMessage) {
                    self.getHandleDetail()
                }
            }else{
                
                self.containTableView.showRequestFaildView(errMessage: errorResponse.errMessage) {
                    self.getHandleDetail()
                }
            }
            
        }
    }
    
    func configerDataSource(response:InspectionAlarmHandleDetailResponse) {
        
        mDataSourceArr.removeAll()
        
        deviceInforModel.deviceName = (response.alarmModel?.deviceName ?? "")
        
        if response.alarmModel?.alarmCategoryStr == "fire" {
            deviceInforModel.deviceState = AlarmStateType.Fire
        }else if response.alarmModel?.alarmCategoryStr == "alarm"{
            deviceInforModel.deviceState = AlarmStateType.Alarm
        }else if response.alarmModel?.alarmCategoryStr == "fault"{
            deviceInforModel.deviceState = AlarmStateType.Fault
        }
        deviceInforModel.currentValue = ""
        deviceInforModel.iconUrl = response.alarmModel!.pictureUrl!
        
        personModel.titleText = "处理人"
        personModel.itemStyle = .NormalStyle
        personModel.inforText = response.alarmModel?.eventHandler?.handleUName
        
        resultModel.titleText = "处理结果"
        resultModel.itemStyle = .NormalStyle
        var inforText = ""
        
        if response.alarmModel?.eventHandler?.handleResult! == 2 {
            inforText = "真实警情"
        }else if response.alarmModel?.eventHandler?.handleResult! == 3 {
            inforText = "系统测试"
        }else if response.alarmModel?.eventHandler?.handleResult! == 4 {
            inforText = "设备误报"
        }
        resultModel.inforText = inforText
        
        editText.inforText = response.alarmModel?.eventHandler?.handleContent
        editText.itemStyle = .TextViewStyle
        
        for model in (response.alarmModel?.eventHandler!.uploadInfo)! {
            
            let annexFileModel = AnnexModel.init()
            
            if model.fileDataType == 1 {
                //png
                annexFileModel.annexType = 1
                annexFileModel.isAddNewItem = false
                annexFileModel.isNetSource = true
                if model.fileUrl != nil {
                    annexFileModel.sourceFile = URL.init(string: model.fileUrl!)
                }
                if model.thumbnails != nil{
                    annexFileModel.thumbnail = URL.init(string: model.thumbnails!)
                }
                mAnnexArr.append(annexFileModel)
                
                
            }else if model.fileDataType == 2 {
                //mp3
                annexFileModel.annexType = 3
                annexFileModel.isAddNewItem = false
                annexFileModel.isCanEdit = false
                annexFileModel.isNetSource = true
                if model.fileUrl != nil {
                    annexFileModel.sourceFile = URL.init(string: model.fileUrl!)
                }
                if model.duration != nil {
                    annexFileModel.voiceSecond = Int(model.duration!)
                }
                
                mVoiceArr.append(annexFileModel)
                
            }else if model.fileDataType == 3 {
                //mp4
                annexFileModel.annexType = 2
                annexFileModel.isAddNewItem = false
                annexFileModel.isNetSource = true
                annexFileModel.isCanEdit = false
                if model.fileUrl != nil {
                    annexFileModel.sourceFile = URL.init(string: model.fileUrl!)
                }
                if model.thumbnails != nil{
                    annexFileModel.thumbnail = URL.init(string: model.thumbnails!)
                }
                mAnnexArr.append(annexFileModel)
            }
        }
        anxueModel.titleText = "附件"
        anxueModel.itemStyle = .MedialStyle
        anxueModel.annexArr = mAnnexArr
        
        voiceModel.titleText = "音频附件"
        voiceModel.itemStyle = .VoiceStyle
        voiceModel.annexArr = mVoiceArr
        
        mDataSourceArr.append(personModel)
        mDataSourceArr.append(resultModel)
        mDataSourceArr.append(editText)
        
        if mAnnexArr.count != 0 {
            mDataSourceArr.append(anxueModel)
        }
        if mVoiceArr.count != 0 {
            mDataSourceArr.append(voiceModel)
        }
        
        containTableView.reloadData()
        
    }
    
    func playRecVoice(model: AnnexModel) {
       
       debugPrint("播放录音")
       
       audioItem = AVPlayerItem.init(url: model.sourceFile!)
       audioItem!.addObserver(self, forKeyPath: "status", options: .new, context: nil)
       
       audioPlayer = AVPlayer.init(playerItem: audioItem)
       audioPlayer?.play()
       
            
    }
    
    @objc func avPlayerItemDidPlayToEndTime(){
        lastVoiceItemCell?.voice_image_view.stopAnimating()
        isPalying = false
        if audioItem != nil {
            audioItem!.removeObserver(self, forKeyPath: "status")
            audioItem = nil
        }
        
    }
    
    @objc func avPlayerItemPlayFailed(){
        
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        
        if keyPath == "status" {
            
            let state = change![NSKeyValueChangeKey.kindKey] as! Int
            
            if state == AVPlayerItem.Status.readyToPlay.rawValue {
                debugPrint("readyToPlay")
                self.isPalying = true
            }else if state == AVPlayerItem.Status.failed.rawValue{
                debugPrint("failed")
                self.isPalying = false
                NeatHud.showMessage(message: "音频播放失败", view: self.view)
            }else if state == AVPlayerItem.Status.unknown.rawValue{
                debugPrint("unknown")
                self.isPalying = false
                NeatHud.showMessage(message: "音频播放失败", view: self.view)
            }
        }
        
    }

}

extension InspectionAlarmHandleDetailViewController:UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if indexPath.section == 0 {
            return NormalConstant.DeviceBasicViewHeight
        }else if indexPath.section == 1{
            return NormalConstant.DeviceInforContainSepHeight
        }else{
            let model = mDataSourceArr[indexPath.row]
            
            if model.itemStyle == .NormalStyle{
                return 44
            }else if model.itemStyle == .TextViewStyle{
                return 98
            }else if model.itemStyle == .MedialStyle{
                let itemWidth = (NormalConstant.ScreenWidth - 6*(4 - 1 + 2))/4
                if model.annexArr.count>=1 && model.annexArr.count <= 4 {
                    return itemWidth + 6 + 36
                }else{
                    return (itemWidth + 6)*2 + 36
                }
            }else{
                return CGFloat(48) * CGFloat(model.annexArr.count) + 36
            }
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        
        return 0.01
        
    }
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        
        return UIView.init()
        
    }
    
}

extension InspectionAlarmHandleDetailViewController:UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if section == 0 || section == 1 {
            
            return 1
            
        }else{
            return mDataSourceArr.count
        }
        
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        
        if indexPath.section == 0 {
           
            let cell = tableView.dequeueReusableCell(withIdentifier: DeviceStateTableViewCellIdent, for: indexPath) as! DeviceStateTableViewCell
            cell.delegate = self
            cell.configerCell(model: deviceInforModel)
            return cell
            
        }else if indexPath.section == 1{
            let cell = tableView.dequeueReusableCell(withIdentifier: SepViewTableViewCellIdent, for: indexPath) as! SepViewTableViewCell
           
            return cell
        }else{
            
            let model = mDataSourceArr[indexPath.row]
        
            if model.itemStyle == .NormalStyle{
                
                let cell = tableView.dequeueReusableCell(withIdentifier: ReportNormalInforTableViewCellIdent, for: indexPath) as! ReportNormalInforTableViewCell
                cell.configerCell(model: model)
                cell.selectionStyle = .none
                return cell
                
            }else if model.itemStyle == .TextViewStyle{
                let cell = tableView.dequeueReusableCell(withIdentifier: ReportTextInforTableViewCellIdent, for: indexPath) as! ReportTextInforTableViewCell
                cell.configerCell(model: model)
                cell.selectionStyle = .none
                       
                return cell
            }else if model.itemStyle == .MedialStyle {
                let cell = tableView.dequeueReusableCell(withIdentifier: ReportMediaTableViewCellIdent, for: indexPath) as! ReportMediaTableViewCell
                cell.delegate = self
                cell.configerCell(model: model)
                cell.selectionStyle = .none
                return cell
                
            }else{
                
                let cell = tableView.dequeueReusableCell(withIdentifier: VoiceTableViewCellIdent, for: indexPath) as! VoiceTableViewCell
                cell.delegate = self
                cell.configerCell(mode: model)
                cell.selectionStyle = .none
                return cell
                
            }
            
        }
        
    }
    
}

extension InspectionAlarmHandleDetailViewController:DeviceStateTableViewCellDelegate{
    
    func refreshClick() {
        self.getHandleDetail(true)
    }
    
}

extension InspectionAlarmHandleDetailViewController:VoiceTableViewCellDelegate{
    
    func clickRecVoice(model: AnnexModel, index: IndexPath, cell: VoiceItemTableViewCell) {
        
        if lastRadioClickIndex != nil {
            
            //有记录之前点击过
            if index == lastRadioClickIndex {
                //两次点击相同音频
                if isPalying {
                    self.audioPlayer?.pause()
                    cell.voice_image_view.stopAnimating()
                    isPalying = false
                }else{
                    self.playRecVoice(model: model)
                    cell.voice_image_view.startAnimating()
                }
            }else{
                //两次不同
                if isPalying {
                    self.audioPlayer?.pause()
                    self.lastVoiceItemCell?.voice_image_view.stopAnimating()
                    self.playRecVoice(model: model)
                    cell.voice_image_view.startAnimating()
                }else{
                    self.playRecVoice(model: model)
                    cell.voice_image_view.startAnimating()
                }
            }
            
            lastVoiceItemCell = cell
            lastRadioClickIndex = index
        }else{
            //首次点击
            lastRadioClickIndex = index
            lastVoiceItemCell = cell
            self.playRecVoice(model: model)
            cell.voice_image_view.startAnimating()
        }
        
    }
    
     
//    func stopRecVoice(model: AnnexModel) {
//         debugPrint("结束播放录音")
//     }

    func startRecVoice() {
        
    }
    
    func endRecVoice() {
        
    }
    
    func deleteRecVoice(model: AnnexModel, index: IndexPath) {
        
            
    }
        
    
    
}

extension InspectionAlarmHandleDetailViewController:AVAudioPlayerDelegate{
    
    func audioPlayerDecodeErrorDidOccur(_ player: AVAudioPlayer, error: Error?) {
        debugPrint("解码失败")
    }
    func audioPlayerDidFinishPlaying(_ player: AVAudioPlayer, successfully flag: Bool) {
        debugPrint("播放完毕")
        lastVoiceItemCell?.voice_image_view.stopAnimating()
        
    }
    
}

extension InspectionAlarmHandleDetailViewController:ReportMediaTableViewCellDelegate{
    
    func didSelectItem(model: AnnexModel) {
        //点击
        //查看图片 或 视频
        let detailVC = MediaPlayViewController.init()
        detailVC.annexModel = model
        self.navigationController?.pushViewController(detailVC, animated: true)
    }
    
    
    
    func didLongSelectItem(model:AnnexModel,indexPath: IndexPath) {
        //长按
        
        
    }
    
}






