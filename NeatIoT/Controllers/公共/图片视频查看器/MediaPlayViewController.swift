//
//  MediaPlayViewController.swift
//  NeatIoT
//
//  Created by Yang Blus on 2020/2/11.
//  Copyright © 2020 Neat. All rights reserved.
//

import UIKit
import Kingfisher
import AVFoundation
class MediaPlayViewController: SupperViewController {
    
    var annexModel:AnnexModel = AnnexModel.init()
    
    lazy var containImageView: UIImageView = {
        let imageView = UIImageView.init()
        imageView.contentMode = .scaleAspectFit
        
        return imageView
    }()
    
    var player:AVPlayer?
    
    var playerLayer:AVPlayerLayer?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.titleName = "查看附件"
        
        
        
        configerUI()

        // Do any additional setup after loading the view.
    }
    func initPlay() {
        
        player = AVPlayer.init(url: annexModel.sourceFile!)
        playerLayer = AVPlayerLayer.init(player: player)
        playerLayer?.backgroundColor = UIColor.black.cgColor
        playerLayer?.frame = CGRect.init(x: 0, y: 0, width: NormalConstant.ScreenWidth, height: NormalConstant.ScreenHeight - NormalConstant.NavBarHeight)
        playerLayer?.videoGravity = .resizeAspect
        self.view.layer.addSublayer(playerLayer!)
        player?.play()
        
    }
    
    func configerUI() {
        
        
        
        
        if annexModel.annexType == 1 {
            
            self.view.addSubview(self.containImageView)
            self.containImageView.snp.makeConstraints { (make) in
                make.edges.equalTo(self.view)
            }
            //图片
            if annexModel.isNetSource {
                
                NeatHud.showLoading(view: self.view, message: "")
                
                containImageView.kf.setImage(with: annexModel.sourceFile, placeholder: UIImage.init(named: "media_image_ph"), options:nil, progressBlock: nil) { (result) in
                    
                    NeatHud.dissmissHud(view: self.view)
                    
                    switch result{
                    case .success(let retrieveImage):
                        
                        break
                    case .failure(let error):
                        
                        NeatHud.showMessage(message: error.errorDescription ?? "图片加载失败", view: self.view)
                        break
                    }
                    
                    
                }
                
            }else{
                
                containImageView.image = annexModel.imageSource
            }
            
        }else if annexModel.annexType == 2 {
            //视频
            self.initPlay()
        }
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
