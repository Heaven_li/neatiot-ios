//
//  AudioRecHud.swift
//  NeatIoT
//
//  Created by Yang Blus on 2020/2/6.
//  Copyright © 2020 Neat. All rights reserved.
//

import UIKit

protocol AudioRecHudDelegate {
    func recTimeOut()
}

class AudioRecHud: UIView {
    
    var recSecont = 30
    
    var timer: Timer?
    
    var delegate:AudioRecHudDelegate?
    
    
    lazy var recAnimation: UIImageView = {
        let imageView = UIImageView.init(image: UIImage.init(named: "rec_icon"))
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    lazy var recSecLable:UILabel = {
        let lable = UILabel.init(frame: CGRect.zero)
        lable.font = UIFont.systemFont(ofSize: 16)
        lable.textAlignment = .center
        lable.textColor = .white
        return lable
    }()
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.backgroundColor = #colorLiteral(red: 0.3333333433, green: 0.3333333433, blue: 0.3333333433, alpha: 1)
        
        let path = UIBezierPath.init(roundedRect: CGRect.init(x: 0, y: 0, width: 120, height: 120), cornerRadius: 3)
        
        let coverLayer = CAShapeLayer.init()
        coverLayer.path = path.cgPath
        
        self.layer.mask = coverLayer
        
        self.addSubview(self.recSecLable)
        self.addSubview(self.recAnimation)
        
        self.recAnimation.snp.makeConstraints { (make) in
            make.top.equalTo(self).offset(10)
            make.width.equalTo(self.snp.width).multipliedBy(0.5)
            make.height.equalTo(self.recAnimation.snp.width)
            make.centerX.equalTo(self)
            
        }
        
        self.recSecLable.snp.makeConstraints { (make) in
            make.leading.trailing.equalTo(self)
            make.top.equalTo(self.recAnimation.snp.bottom).offset(8)
            make.bottom.equalTo(self.snp.bottom).offset(-10)
        }
        
        
        
        
    }
    
    func startRecTime() {
        
        recSecont = 30
        self.recSecLable.text = String.init(format: "%ds", recSecont)
        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(timeTarger), userInfo: nil, repeats: true)
        RunLoop.current.add(timer!, forMode: .common)
        timer?.fire()
    
    }
    
    func endRecTime() {
        
        timer?.invalidate()
        
    }
    
    @objc func timeTarger() {
        
        if recSecont >= 0 {
            self.recSecLable.text = String.init(format: "%ds", recSecont)
            recSecont-=1
        }else{
            recSecont = 0
            self.delegate?.recTimeOut()
            timer?.invalidate()
        }
  
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
    
    

}
