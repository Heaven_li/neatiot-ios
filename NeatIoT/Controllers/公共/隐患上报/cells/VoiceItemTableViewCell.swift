//
//  VoiceItemTableViewCell.swift
//  NeatIoT
//
//  Created by Yang Blus on 2020/2/5.
//  Copyright © 2020 Neat. All rights reserved.
//

import UIKit

protocol VoiceItemTableViewCellDelegate {
    ///点击录音
    func clickRecVoice(model:AnnexModel,index:IndexPath,cell:VoiceItemTableViewCell)
    ///删除录音
    func deleteRecVoice(model:AnnexModel,index:IndexPath)
}

class VoiceItemTableViewCell: UITableViewCell {

    @IBOutlet weak var voice_image_view: UIImageView!
    
    @IBOutlet weak var second_lable: UILabel!
    
    @IBOutlet weak var delete_image_view: UIImageView!
    
    @IBOutlet weak var contain_view: UIView!
    
    var delegate:VoiceItemTableViewCellDelegate?
    
    var currentModel:AnnexModel = AnnexModel()
    var currentIndex:IndexPath = IndexPath()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        
        let imageArr = [UIImage.init(named: "voice_1"),
        UIImage.init(named: "voice_2"),
        UIImage.init(named: "voice")]
        
        self.voice_image_view.animationImages = imageArr as? [UIImage]
        self.voice_image_view.animationDuration = 1
        self.voice_image_view.animationRepeatCount = 0
        
        self.contain_view.layer.cornerRadius = 17
        self.contain_view.clipsToBounds = true
        
        /// 删除按钮添加点击事件
        let tap = UITapGestureRecognizer.init(target: self, action: #selector(deleteVoice(tap:)))
        self.delete_image_view.isUserInteractionEnabled = true
        self.delete_image_view.addGestureRecognizer(tap)
        
        /// 语音框添加点击事件
        let playTap = UITapGestureRecognizer.init(target: self, action: #selector(playVoice(tap:)))
        self.contain_view.isUserInteractionEnabled = true
        self.contain_view.addGestureRecognizer(playTap)
        
        
    }
    
    func configerCell(model:AnnexModel,index:IndexPath) {
        
        currentModel = model
        currentIndex = index
        
        self.second_lable.text = String.init(format: "%ds", model.voiceSecond ?? 0)
        
        let scal = Double(model.voiceSecond!)/30.0
        
        let scal_width = (Double(NormalConstant.ScreenWidth - 32) * 0.7 - 90) * scal
        
        let containViewWidth = scal_width + 90
        
        self.contain_view.snp.remakeConstraints{ (make) in
            make.width.equalTo(containViewWidth)
            make.top.equalTo(self).offset(6)
            make.bottom.equalTo(self).offset(-6)
            make.leading.equalTo(self)
        }
        
        self.delete_image_view.isHidden = !model.isCanEdit
       
        self.layoutIfNeeded()
        
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @objc func deleteVoice(tap:UITapGestureRecognizer) {
        
        self.delegate?.deleteRecVoice(model: currentModel, index: currentIndex)
        
    }
    
    @objc func playVoice(tap:UITapGestureRecognizer) {
        
        self.delegate?.clickRecVoice(model: currentModel,index: currentIndex,cell: self)
        
    }
    
    
}
