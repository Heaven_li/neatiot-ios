//
//  ReportTextInforTableViewCell.swift
//  NeatIoT
//
//  Created by Yang Blus on 2020/2/4.
//  Copyright © 2020 Neat. All rights reserved.
//

import UIKit

protocol ReportTextInforTableViewCellDelegate {
    ///结束输入
    func endInput(text:String)
}

class ReportTextInforTableViewCell: UITableViewCell {

    @IBOutlet weak var titleLable: UILabel!
    
    @IBOutlet weak var inputTextView: UITextView!
    
    @IBOutlet weak var desLable: UILabel!
    
    var delegate:ReportTextInforTableViewCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.inputTextView.backgroundColor = .white
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configerCell(model:ReportItemModel) {
        
        self.titleLable.text = model.titleText
        self.inputTextView.text = model.inforText
        
        self.inputTextView.isEditable = model.isCanEdit
        
        self.inputTextView.layer.borderColor = #colorLiteral(red: 0.00223883125, green: 0.2229455411, blue: 0.5099669695, alpha: 1)
        self.inputTextView.layer.cornerRadius = 3
        self.inputTextView.layer.borderWidth = 1
        self.inputTextView.returnKeyType = .done
        self.inputTextView.delegate = self
        
    }
}

extension ReportTextInforTableViewCell:UITextViewDelegate{
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        
        if text == "\n" {
            //确定键
            self.delegate?.endInput(text: textView.text)
            textView.resignFirstResponder()
        }

        
//        if (textView.text.length + text.length > 500) {
//            NSString *allText = [NSString stringWithFormat:@"%@%@",textView.text,text];
//            textView.text = [allText substringToIndex:500];
//            [PubllicMaskViewHelper showTipViewWith:@"输入不能超过500个字" inSuperView:self withDuration:1];
//            return NO;
//        }
    
        
        return true
    }
    
}
