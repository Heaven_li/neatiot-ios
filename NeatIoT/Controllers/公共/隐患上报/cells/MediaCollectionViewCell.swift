//
//  MediaCollectionViewCell.swift
//  NeatIoT
//
//  Created by Yang Blus on 2020/2/4.
//  Copyright © 2020 Neat. All rights reserved.
//

import UIKit
import Kingfisher

protocol MediaCollectionViewCellDelegate {
    
    func longPressItem(model:AnnexModel,indexPath:IndexPath)
    
}

class MediaCollectionViewCell: UICollectionViewCell {
    
    var currentModel:AnnexModel = AnnexModel()
    var currentIndexPath:IndexPath?
    
    @IBOutlet weak var imageView: UIImageView!
    
    @IBOutlet weak var videoPlayImageView: UIImageView!
    var delegate:MediaCollectionViewCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        let longGestrue = UILongPressGestureRecognizer.init(target: self, action: #selector(longPressGesture))
        self.imageView.isUserInteractionEnabled = true
        self.imageView.addGestureRecognizer(longGestrue)
        
    }

    func configerCell(model:AnnexModel,index:IndexPath) {
        
        currentModel = model
        currentIndexPath = index
        
        if model.isNetSource {
            
            if model.annexType == 1 {
                //图片
                self.videoPlayImageView.isHidden = true
            }else if model.annexType == 2{
                //视频
                self.videoPlayImageView.isHidden = false
            }
            self.imageView.kf.setImage(with: model.thumbnail, placeholder: UIImage.init(named: "icon_image_view"))
            
            
            
            
        }else{
            
            if model.annexType == 1 {
                //图片
                self.videoPlayImageView.isHidden = true
            }else if model.annexType == 2{
                //视频
                self.videoPlayImageView.isHidden = false
            }
            self.imageView.image = model.imageSource
        }
        
        
        
    }
    
    @objc func longPressGesture(longPress:UILongPressGestureRecognizer) {
        
        if longPress.state == .began {
            self.delegate?.longPressItem(model: currentModel,indexPath:currentIndexPath!)
        }
        
    }
    
}
