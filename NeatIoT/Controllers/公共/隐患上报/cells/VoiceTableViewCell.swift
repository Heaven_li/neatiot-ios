//
//  VoiceTableViewCell.swift
//  NeatIoT
//
//  Created by Yang Blus on 2020/2/4.
//  Copyright © 2020 Neat. All rights reserved.
//

import UIKit

protocol VoiceTableViewCellDelegate {
    
    /// 开始录音
    func startRecVoice()
    /// 结束录音
    func endRecVoice()
    ///点击录音
    func clickRecVoice(model:AnnexModel,index:IndexPath,cell: VoiceItemTableViewCell)
    ///删除录音
    func deleteRecVoice(model:AnnexModel,index:IndexPath)
    
}

class VoiceTableViewCell: UITableViewCell {
    
    let VoiceItemTableViewCellIdent = "VoiceItemTableViewCellIdent"
    let VoiceAddTableViewCellIdent = "VoiceAddTableViewCellIdent"
    
    var currentModel:ReportItemModel = ReportItemModel()
    
    var delegate:VoiceTableViewCellDelegate?

    @IBOutlet weak var titleLable: UILabel!
    
    @IBOutlet weak var containTableView: UITableView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        containTableView.bounces = false
        containTableView.delegate = self
        containTableView.dataSource = self
        containTableView.separatorStyle = .none
        containTableView.backgroundColor = .white
        containTableView.register(UINib.init(nibName: "VoiceItemTableViewCell", bundle: Bundle.main), forCellReuseIdentifier: VoiceItemTableViewCellIdent)
        containTableView.register(UINib.init(nibName: "VoiceAddTableViewCell", bundle: Bundle.main), forCellReuseIdentifier: VoiceAddTableViewCellIdent)
        
        currentModel.annexArr = []
        
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configerCell(mode:ReportItemModel) {
        
        currentModel = mode
        self.containTableView.reloadData()
        
        
    }
    
}

extension VoiceTableViewCell:UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 46
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.01
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView.init()
    }
    
}

extension VoiceTableViewCell:UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return currentModel.annexArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let model = currentModel.annexArr[indexPath.row]
        
        if model.isAddNewItem {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: VoiceAddTableViewCellIdent, for: indexPath) as! VoiceAddTableViewCell
            cell.selectionStyle = .none
            cell.delegate = self
            return cell
            
        }else{
            
            let cell = tableView.dequeueReusableCell(withIdentifier: VoiceItemTableViewCellIdent, for: indexPath) as! VoiceItemTableViewCell
            cell.configerCell(model: model, index: indexPath)
            cell.delegate = self
            cell.selectionStyle = .none
            return cell
            
        }
        
    }
    
    
    
}

extension VoiceTableViewCell:VoiceAddTableViewCellDelegate{
    
    func startRecVoice() {
        self.delegate?.startRecVoice()
    }
    
    func endRecVoice() {
        self.delegate?.endRecVoice()
    }
    
}
extension VoiceTableViewCell:VoiceItemTableViewCellDelegate{
    
    func clickRecVoice(model: AnnexModel, index: IndexPath, cell: VoiceItemTableViewCell) {
        self.delegate?.clickRecVoice(model: model, index: index, cell: cell)
    }
    
    func deleteRecVoice(model: AnnexModel, index: IndexPath) {
        self.delegate?.deleteRecVoice(model: model, index: index)
    }
    
    
}
