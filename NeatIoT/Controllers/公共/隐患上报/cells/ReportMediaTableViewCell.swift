//
//  ReportMediaTableViewCell.swift
//  NeatIoT
//
//  Created by Yang Blus on 2020/2/4.
//  Copyright © 2020 Neat. All rights reserved.
//

import UIKit

protocol ReportMediaTableViewCellDelegate {
    
    /// 点击选中
    /// - Parameter model: item model
    func didSelectItem(model:AnnexModel)
    
    /// 长按选中
    /// - Parameter model: item model
    func didLongSelectItem(model:AnnexModel,indexPath: IndexPath)
    
}


class ReportMediaTableViewCell: UITableViewCell {
    
    let itemIdent = "MediaCollectionViewCellIdent"
    let space_num:CGFloat = 6
    let item_h_count:CGFloat = 4
    var currentItmeModel:ReportItemModel = ReportItemModel()
    
    var delegate:ReportMediaTableViewCellDelegate?
    
    lazy var titleLable: UILabel = {
        let titleLable = UILabel.init(frame: CGRect.zero)
        titleLable.font = UIFont.systemFont(ofSize: 15)
        titleLable.textColor = #colorLiteral(red: 0.121294491, green: 0.1292245686, blue: 0.141699791, alpha: 1)
        titleLable.textAlignment = .left
        return titleLable
    }()
    
    lazy var containCollectionView: UICollectionView = {
        
        let layout = UICollectionViewFlowLayout.init()
        
        let collectionView = UICollectionView.init(frame: CGRect.zero, collectionViewLayout: layout)
        collectionView.delegate = self;
        collectionView.dataSource = self;
        collectionView.backgroundColor = UIColor.white
        collectionView.register(UINib.init(nibName: "MediaCollectionViewCell", bundle: Bundle.main), forCellWithReuseIdentifier: itemIdent)
    
        return collectionView
    }()

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.backgroundColor = .white
        
        self.contentView.addSubview(self.titleLable)
        self.contentView.addSubview(self.containCollectionView)
        self.titleLable.snp.makeConstraints { (make) in
            make.top.equalTo(self).offset(8)
            make.left.equalTo(self).offset(10)
            make.right.equalTo(self).offset(-10)
            make.bottom.equalTo(self.containCollectionView.snp.top).offset(-8)
        }
        self.containCollectionView.snp.makeConstraints { (make) in
            make.top.equalTo(self.titleLable.snp.bottom).offset(8)
            make.left.equalTo(self).offset(10)
            make.right.equalTo(self).offset(-10)
            make.bottom.equalTo(self)
        }
        
        
    }
    
    func configerCell(model:ReportItemModel) {
                
        currentItmeModel = model
        self.titleLable.text = model.titleText
        
        self.containCollectionView.reloadData()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}

extension ReportMediaTableViewCell:UICollectionViewDelegate{
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let annexModel = currentItmeModel.annexArr[indexPath.item]
        
        self.delegate?.didSelectItem(model: annexModel)
        
    }
    
    
}

extension ReportMediaTableViewCell:UICollectionViewDataSource{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return currentItmeModel.annexArr.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let item = collectionView.dequeueReusableCell(withReuseIdentifier: itemIdent, for: indexPath) as! MediaCollectionViewCell
        
        let model = currentItmeModel.annexArr[indexPath.item]
        
        item.delegate = self
        
        item.configerCell(model: model, index: indexPath)
        
        return item
    }
    
}

extension ReportMediaTableViewCell:MediaCollectionViewCellDelegate{
    func longPressItem(model: AnnexModel, indexPath: IndexPath) {
        
        self.delegate?.didLongSelectItem(model: model,indexPath: indexPath)
        
    }
    
    
    
    
}

extension ReportMediaTableViewCell:UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let itemWidth = (NormalConstant.ScreenWidth - 32 - space_num*(item_h_count - 1 + 2))/4
        return CGSize.init(width: itemWidth, height: itemWidth)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return space_num
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return space_num
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets.init(top: space_num, left: space_num, bottom: space_num, right: space_num)
    }
    
}
