//
//  VoiceAddTableViewCell.swift
//  NeatIoT
//
//  Created by Yang Blus on 2020/2/5.
//  Copyright © 2020 Neat. All rights reserved.
//

import UIKit

protocol VoiceAddTableViewCellDelegate {
    
    /// 开始录音
    func startRecVoice()
    
    /// 结束录音
    func endRecVoice()
}

class VoiceAddTableViewCell: UITableViewCell {
    
    @IBOutlet weak var startLable: UILabel!
    var delegate:VoiceAddTableViewCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        let longPress = UILongPressGestureRecognizer.init(target: self, action: #selector(longPressGesture(longPress:)))
        longPress.minimumPressDuration = 0.4
        
        self.startLable.isUserInteractionEnabled = true
        self.startLable.addGestureRecognizer(longPress)
        self.startLable.layer.cornerRadius = 5
        self.startLable.clipsToBounds = true

        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    @objc func longPressGesture(longPress:UILongPressGestureRecognizer){
        
        if longPress.state == .began {
            
            self.startLable.text = "松开 结束"
            self.startLable.backgroundColor = #colorLiteral(red: 0.00223883125, green: 0.2229455411, blue: 0.5099669695, alpha: 1)
            self.startLable.textColor = UIColor.white
            
//            DispatchQueue.global().async {
                self.delegate?.startRecVoice()
//            }
            
            
            
        }else if longPress.state == .ended{
            
            self.startLable.text = "按下 开始"
            self.startLable.backgroundColor = #colorLiteral(red: 0.9488552213, green: 0.9487094283, blue: 0.9693080783, alpha: 1)
            self.startLable.textColor = #colorLiteral(red: 0.333293438, green: 0.3333562613, blue: 0.3332894742, alpha: 1)
            
//            DispatchQueue.global().async {
                self.delegate?.endRecVoice()
//            }
            
            
        }
    }
    
}
