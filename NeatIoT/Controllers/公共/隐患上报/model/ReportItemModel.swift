//
//  ReportItemModel.swift
//  NeatIoT
//
//  Created by Yang Blus on 2020/2/4.
//  Copyright © 2020 Neat. All rights reserved.
//

import UIKit

/// item样式枚举
enum ItemStyle {
    /// 正常样式
    case NormalStyle;
    /// 正常样式带箭头
    case NormalArrowStyle;
    /// 巡检 隐患内容描述样式
    case TextViewStyle;
    /// 复选框
    case RadioStyle;
    /// 巡检 隐患内容描述样式 没有底部提示语
    case UnTextViewStyle;
    /// 视频照片样式
    case MedialStyle;
    /// 语音样式
    case VoiceStyle;
    /// 设备信息
    case DeviceInforStyle;
    /// 维保工时输入样式
    case NormalInputStyle;
    /// 内容描述带副标题样式
    case TextViewSubTitleStyle;
    /// 维保重的添加耗材样式
    case FunctionStyle;
    /// 耗材列表样式
    case MateriasStyle;
    
}

class ReportItemModel: NSObject {
    
    /// item样式
    var itemStyle:ItemStyle = .NormalStyle
    /// 标题
    var titleText:String?
    /// 副标题文字
    var subTitleText:String?
    /// function样式 title
    var functionTitleText:String?
    /// 内容
    var inforText:String?
    /// 上传字段信息
    var uploadInfor:String?
    /// 是否可编辑
    var isCanEdit:Bool = true
    /// 附件数组
    var annexArr:Array<AnnexModel> = []
    /// 如果是NormalArrowStyle 样式 数据类型
    var inforType:String?
    /// 设备信息model
    var deviceInfor:DeviceInforViewModel?
    /// 是否显示分割线
    var isShowSepLine:Bool = false
    /// 耗材数组
    var materialsArr:Array<MaterialsItemModel>?
    /// 输入样式的占位符
    var placeHolderStr:String = ""

    
}
/// 附件model
class AnnexModel: NSObject {
    
    /// 附件类型 1:图片 2:视频 3:语音
    var annexType:Int = 1;
    
    /// 缩略图地址
    var thumbnail:URL?
    
    /// 源文件地址
    var sourceFile:URL?
    
    /// 图片
    var imageSource:UIImage?
    
    /// 是否是网络资源
    var isNetSource:Bool = false
    
    /// 是否是添加按钮
    var isAddNewItem:Bool = false
    
    /// 语音内容时长
    var voiceSecond:Int?
    
    /// 是否可编辑
    var isCanEdit:Bool = true
    
    
}

/// 维保任务 工单 耗材展示model
class MaterialsItemModel: NSObject {
    
    /// 耗材ID
    var materialsId:String?
    /// 耗材名称
    var firstLableText:String?
    /// 用量
    var secondLableText:String?
    /// 应付总额
    var thirdLableText:String?
    /// 实付总额
    var fourLableText:String?
    

}
