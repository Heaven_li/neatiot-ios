//
//  ReportUploadProjectItem.swift
//  NeatIoT
//
//  Created by Yang Blus on 2020/2/6.
//  Copyright © 2020 Neat. All rights reserved.
//

import UIKit
import HandyJSON

class ReportUpLoadProjectList: HandyJSON {
    
    var projectArr:Array<ReportUploadProjectItem>?
    
    required init(){
        
    }
}

class ReportUploadProjectItem: NSObject{
    
    //项目ID
    var ItemId:String?
    //结果
    var ItemResult:String?
    
    
    
    
}
