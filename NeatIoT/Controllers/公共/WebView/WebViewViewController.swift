//
//  WebViewViewController.swift
//  NeatIoT
//
//  Created by Yang Blus on 2020/8/10.
//  Copyright © 2020 Neat. All rights reserved.
//

import UIKit
import WebKit

class WebViewViewController: SupperViewController {

    var navTitleStr:String = ""
    
    lazy var wkWebView: WKWebView = {
        
        let webView = WKWebView.init()
        webView.uiDelegate = self
        webView.navigationDelegate = self
        webView.addObserver(self, forKeyPath: "estimatedProgress", options: .new, context: nil)
        webView.addObserver(self, forKeyPath: "title", options: .new, context: nil)
        
        return webView
    }()
    
    lazy var progressView: UIProgressView = {
        
        let progerssView = UIProgressView.init(progressViewStyle: .default)
        progerssView.tintColor = #colorLiteral(red: 0.1843209267, green: 0.6234420538, blue: 0.721621573, alpha: 1)
        return progerssView
    }()

    override func viewDidLoad() {
        super.viewDidLoad()

        
        self.titleName = navTitleStr
        
        self.view.addSubview(self.wkWebView)
        
        self.wkWebView.snp.makeConstraints { (make) in
            make.edges.equalTo(self.view)
        }
        
        var urlRequset:URLRequest?
        if (navTitleStr == "隐私政策"){
            urlRequset = URLRequest.init(url: URL.init(string: "https://aly.neat.com.cn:9021/IOTprivacy.html")!)
        }else if (navTitleStr == "服务协议"){
            urlRequset = URLRequest.init(url: URL.init(string: "https://aly.neat.com.cn:9021/IOTlicense.html")!);
        }
        
        self.wkWebView.load(urlRequset!)
        

    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        //  加载进度条
        if keyPath == "estimatedProgress"{
            self.progressView.alpha = 1.0
          
            progressView.setProgress(Float((self.wkWebView.estimatedProgress) ), animated: true)
            if (self.wkWebView.estimatedProgress )  >= 1.0 {
                UIView.animate(withDuration: 0.3, delay: 0.1, options: .curveEaseOut, animations: {
                    self.progressView.alpha = 0
                }, completion: { (finish) in
                    self.progressView.setProgress(0.0, animated: false)
                })
            }
        }
    }


}

extension WebViewViewController:WKUIDelegate{
    
    
}
extension WebViewViewController:WKNavigationDelegate{
    
    ///开始加载
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        
    }
    
    ///加载完成
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        
    }
    
    ///https
    func webView(_ webView: WKWebView, didReceive challenge: URLAuthenticationChallenge, completionHandler: @escaping (URLSession.AuthChallengeDisposition, URLCredential?) -> Void) {
        
        if challenge.protectionSpace.authenticationMethod.elementsEqual(NSURLAuthenticationMethodServerTrust) {
            let card = URLCredential.init(trust: challenge.protectionSpace.serverTrust!)
            
            completionHandler(.useCredential,card)
            
        }
        
    }
    
    ///加载失败
    func webView(_ webView: WKWebView, didFailProvisionalNavigation navigation: WKNavigation!, withError error: Error) {
        
    }
    
    
    
}
