//
//  QRScanViewController.swift
//  NeatIoT
//
//  Created by neat on 2019/11/26.
//  Copyright © 2019 Neat. All rights reserved.
//

import UIKit
import AVFoundation

protocol QRScanViewControllerDelegate {
    
    func scanResult(qrString:String)
    
}

class QRScanViewController: SupperViewController {

    
    var vcInit:VCInitModel = VCInitModel()
    
    var taskPointModel:InspectionTaskPointModel?
    
    var bindPointModel:InspectionBindPointModel?
    
    var isFastInspection:Bool?
    
    var delegate:QRScanViewControllerDelegate?
    
    lazy var qrScanView: QRScanView = {
        let view = QRScanView.init(frame: self.view.bounds)
        view.delegate = self
        
        return view
    }()
    
    lazy var coverView: ScanMaskView = {
        let maskView = ScanMaskView.init(frame: self.view.bounds)
        maskView.delegate = self
        return maskView
    }()
    
    override func viewWillDisappear(_ animated: Bool) {
        
        self.qrScanView.torchOff()
         
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.titleName = vcInit.navTitleText
        
        self.view.addSubview(self.qrScanView)
        self.view.addSubview(self.coverView)
        
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension QRScanViewController:QRScanViewDelegate{
    func qrScanFailed() {
        NeatHud.showMessage(message: "二维码扫描失败！稍后再试！", view: self.view) {
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    func qrScanSuccess(string:String) {
        
    
        if self.taskPointModel != nil {
            ///巡检任务
            let qrModel = InspectionPointQRModel.deserialize(from: string)
            if nil != qrModel {
                if qrModel?.p == self.taskPointModel?.qrCode {
                    let vc = InspectionProjectCheckViewController.init()
                    vc.taskPointModel = self.taskPointModel
                    vc.vcInit = self.vcInit
                    vc.qrModel = qrModel
                    self.navigationController?.pushViewController(vc, animated: true)
                }else{
                    NeatHud.showMessage(message: "点位不符，请确认是否为该巡检点二维码", view: self.view) {
                        self.navigationController?.popViewController(animated: true)
                    }
                }
            }else{
                NeatHud.showMessage(message: "点位不符，请确认是否为该巡检点二维码", view: self.view) {
                    self.navigationController?.popViewController(animated: true)
                }
            }
            
            
        }else if self.bindPointModel != nil {
            ///巡检点绑定
            let qrModel = InspectionPointQRModel.deserialize(from: string)
            if nil != qrModel{
                let vc = InspectionPointBindDetailViewController.init()
                vc.pointModel = self.bindPointModel
                vc.vcInitModel = self.vcInit
                vc.qrModel = qrModel
                self.navigationController?.pushViewController(vc, animated: true)
            }else{
                NeatHud.showMessage(message: "二维码格式错误", view: self.view) {
                    self.navigationController?.popViewController(animated: true)
                }
            }
            
            
        }else if self.isFastInspection ?? false{
            ///快速巡检
            let qrModel = InspectionPointQRModel.deserialize(from: string)
            if nil != qrModel{
                self.getFastPoint(qrModel: qrModel!)
            }else{
                NeatHud.showMessage(message: "二维码格式错误", view: self.view) {
                    self.navigationController?.popViewController(animated: true)
                }
            }
            
        }else{
            
            self.delegate?.scanResult(qrString: string)
            self.navigationController?.popViewController(animated: true)
            
        }

    }
    
    func getFastPoint(qrModel:InspectionPointQRModel) {
        
        NeatHud.showLoading(view: self.view, message: "")
        NeatRequest.instance.fastInspectionGetProject(tagCode: qrModel.p!, tagType: String.init(format: "%d", qrModel.t!), success: { (response) in
            
            NeatHud.dissmissHud(view: self.view)
            
            let vc = InspectionProjectCheckViewController.init()
            vc.taskPointModel = response.pointModel
            vc.isFromQuiclyInspection = true
            vc.vcInit = self.vcInit
            vc.qrModel = qrModel
            self.navigationController?.pushViewController(vc, animated: true)
            
            
        }) { (error) in
            
            NeatHud.showMessage(message: error.errMessage, view: self.view) {
                self.navigationController?.popViewController(animated: true)
            }
            
        }
        
    }
    
    
}

extension QRScanViewController:ScanMaskViewDelegate{
    
    func torchState(isOn: Bool) {
        if isOn {
            self.qrScanView.torchOn()
        }else{
            self.qrScanView.torchOff()
        }
        
    }
    
    
}

