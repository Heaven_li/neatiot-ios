//
//  ScanMaskView.swift
//  NeatIoT
//
//  Created by neat on 2019/11/27.
//  Copyright © 2019 Neat. All rights reserved.
//

import UIKit

protocol ScanMaskViewDelegate {
    func torchState(isOn:Bool)
}

class ScanMaskView: UIView {
    
    let cornerColor = #colorLiteral(red: 0.0007064800969, green: 0.2957481221, blue: 0.661073423, alpha: 1)
    let maskColor = #colorLiteral(red: 0.2352941176, green: 0.2509803922, blue: 0.262745098, alpha: 0.46)
    
    var width:CGFloat = 0
    //扫描框宽度
    var sc_bordeWidth:CGFloat = 0
    //扫描框高度
    var sc_bordeHeight:CGFloat = 0
    //扫描框左上角X坐标
    var sc_left_top_xPoint:CGFloat = 0
    //扫描框左上角Y坐标
    var sc_left_top_yPoint:CGFloat = 0
    
    var delegate:ScanMaskViewDelegate?
    
    /// 扫描线
    lazy var scanLineImageView: UIImageView = {
        let line = UIImageView.init(image: UIImage.init(named: "scan_line"))
        line.frame = CGRect.init(x: sc_left_top_xPoint, y: sc_left_top_yPoint, width: sc_bordeWidth, height: 6)
        return line
    }()
    
    lazy var torchBtn: UIButton = {
        let btn = UIButton.init(type: .custom)
        btn.setImage(UIImage.init(named: "torch_off"), for: .normal)
        btn.setImage(UIImage.init(named: "torch_on"), for: .selected)
        btn.addTarget(self, action: #selector(torchBtnClick(btn:)), for: .touchUpInside)
        return btn
    }()

    
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
        //扫描框右上角X坐标
        let sc_right_top_xPoint = (NormalConstant.ScreenWidth - sc_bordeWidth)/2.0 + sc_bordeWidth;
        //扫描框右上角Y坐标
        let sc_right_top_yPoint = NormalConstant.SCBORDERTOP;
        
        //扫描框左下角X坐标
        let sc_left_bottom_xPoint = (NormalConstant.ScreenWidth - sc_bordeWidth)/2.0;
        //扫描框左下角Y坐标
        let sc_left_bottom_yPoint = NormalConstant.SCBORDERTOP + sc_bordeHeight;
        
        //扫描框右下角X坐标
        let sc_right_bottom_xPoint = (NormalConstant.ScreenWidth - sc_bordeWidth)/2.0 + sc_bordeWidth;
        //扫描框右下角Y坐标
        let sc_right_bottom_yPoint = NormalConstant.SCBORDERTOP + sc_bordeHeight;
        
        //中间矩形框
        let path = UIBezierPath.init()
        path.move(to: CGPoint.init(x: sc_left_top_xPoint, y: sc_left_top_yPoint))
        path.addLine(to: CGPoint.init(x: sc_right_top_xPoint, y: sc_right_top_yPoint))
        path.addLine(to: CGPoint.init(x: sc_right_bottom_xPoint, y: sc_right_bottom_yPoint))
        path.addLine(to: CGPoint.init(x: sc_left_bottom_xPoint, y: sc_left_bottom_yPoint))
        path.addLine(to: CGPoint.init(x: sc_left_top_xPoint, y: sc_left_top_yPoint))
        UIColor.white.set()
        path.stroke()
        
        //扫描框左上角绿角
        let corner_top_left = UIBezierPath.init()
        corner_top_left.lineWidth = 2
        corner_top_left.move(to: CGPoint.init(x: sc_left_top_xPoint, y: NormalConstant.SCBORDERTOP + NormalConstant.CORNERLINELENGTH))
        corner_top_left.addLine(to: CGPoint.init(x: sc_left_top_xPoint, y: NormalConstant.SCBORDERTOP))
        corner_top_left.addLine(to: CGPoint.init(x: sc_left_top_xPoint + NormalConstant.CORNERLINELENGTH, y: NormalConstant.SCBORDERTOP))
        corner_top_left.lineCapStyle = .round
        cornerColor.set()
        corner_top_left.stroke()

        //扫描框右上角绿角
        let corner_top_right = UIBezierPath.init()
        corner_top_right.lineWidth = 2
        corner_top_right.move(to: CGPoint.init(x: sc_right_top_xPoint - NormalConstant.CORNERLINELENGTH, y: NormalConstant.SCBORDERTOP))
        corner_top_right.addLine(to: CGPoint.init(x: sc_right_top_xPoint, y: NormalConstant.SCBORDERTOP))
        corner_top_right.addLine(to: CGPoint.init(x: sc_right_top_xPoint, y: NormalConstant.SCBORDERTOP  + NormalConstant.CORNERLINELENGTH))
        corner_top_right.lineCapStyle = .round
        cornerColor.set()
        corner_top_right.stroke()
        
        //扫描框右下角绿角
        let corner_bottom_right = UIBezierPath.init()
        corner_bottom_right.lineWidth = 2
        corner_bottom_right.move(to: CGPoint.init(x: sc_right_bottom_xPoint - NormalConstant.CORNERLINELENGTH, y: NormalConstant.SCBORDERTOP + sc_bordeHeight))
        corner_bottom_right.addLine(to: CGPoint.init(x: sc_right_bottom_xPoint, y: NormalConstant.SCBORDERTOP + sc_bordeHeight))
        corner_bottom_right.addLine(to: CGPoint.init(x: sc_right_bottom_xPoint, y: NormalConstant.SCBORDERTOP + sc_bordeHeight - NormalConstant.CORNERLINELENGTH))
        corner_bottom_right.lineCapStyle = .round
        cornerColor.set()
        corner_bottom_right.stroke()
        
        //扫描框左下角绿角
        let corner_bottom_left = UIBezierPath.init()
        corner_bottom_left.lineWidth = 2
        corner_bottom_left.move(to: CGPoint.init(x: sc_left_top_xPoint, y: NormalConstant.SCBORDERTOP + sc_bordeHeight - NormalConstant.CORNERLINELENGTH))
        corner_bottom_left.addLine(to: CGPoint.init(x: sc_left_top_xPoint, y: NormalConstant.SCBORDERTOP + sc_bordeHeight))
        corner_bottom_left.addLine(to: CGPoint.init(x: sc_left_top_xPoint + NormalConstant.CORNERLINELENGTH, y: NormalConstant.SCBORDERTOP + sc_bordeHeight))
        corner_bottom_left.lineCapStyle = .round
        cornerColor.set()
        corner_bottom_left.stroke()
        
        
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        
        width = frame.size.width;
        //扫描框宽度
        sc_bordeWidth = width * NormalConstant.SCBORDERWIDTHSCAL;
        //扫描框高度
        sc_bordeHeight = sc_bordeWidth;
        
        //扫描框左上角X坐标
        sc_left_top_xPoint = (NormalConstant.ScreenWidth - sc_bordeWidth)/2.0;
        //扫描框左上角Y坐标
        sc_left_top_yPoint = NormalConstant.SCBORDERTOP;
        
        self.backgroundColor = UIColor.clear
        
        self.maskView(frame: frame)
        
        self.addSubview(self.scanLineImageView)
        
        self.addSubview(self.torchBtn)
        self.torchBtn.isSelected = false
        
        self.torchBtn.snp.makeConstraints { (make) in
            make.centerX.equalTo(self)
            make.bottom.equalTo(self).offset(-100)
        }
        
        self.startScanAnimation()
        
    }
    
    func startScanAnimation() {
        
        let baseAnimation = CABasicAnimation.init(keyPath: "position")
        baseAnimation.fromValue = NSValue.init(cgPoint: CGPoint.init(x: self.scanLineImageView.center.x, y: sc_left_top_yPoint))
        baseAnimation.toValue = NSValue.init(cgPoint: CGPoint.init(x: self.scanLineImageView.center.x, y: sc_left_top_yPoint + sc_bordeWidth))
        baseAnimation.duration = 2
        baseAnimation.repeatCount = MAXFLOAT
        self.scanLineImageView.layer.add(baseAnimation, forKey: "scan")
    }
    
    func maskView(frame:CGRect) {
        
        // Drawing code
        let width = frame.size.width;
        
        //扫描框宽度
        let sc_bordeWidth = width * NormalConstant.SCBORDERWIDTHSCAL;
        //扫描框高度
        let sc_bordeHeight = sc_bordeWidth;
        
        //扫描框左上角X坐标
        let sc_left_top_xPoint = (NormalConstant.ScreenWidth - sc_bordeWidth)/2.0;
        //扫描框左上角Y坐标
        let sc_left_top_yPoint = NormalConstant.SCBORDERTOP;
        
        let maskPath = UIBezierPath.init(rect: CGRect.init(x: 0, y: 0, width: self.bounds.size.width, height: self.bounds.size.height))
        let path = UIBezierPath.init(roundedRect: CGRect.init(x: sc_left_top_xPoint, y: sc_left_top_yPoint, width: sc_bordeWidth, height: sc_bordeHeight), cornerRadius: 1)
        
        maskPath.append(path.reversing())
        
        let maskLayer = CAShapeLayer.init()
        maskLayer.path = maskPath.cgPath
        maskLayer.fillColor = maskColor.cgColor
        self.layer.addSublayer(maskLayer)
        
    }
    
    @objc func torchBtnClick(btn:UIButton) {
        
        btn.isSelected = !btn.isSelected
        self.delegate?.torchState(isOn: btn.isSelected)
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
