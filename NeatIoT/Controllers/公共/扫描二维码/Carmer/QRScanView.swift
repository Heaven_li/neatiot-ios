//
//  QRScanView.swift
//  NeatIoT
//
//  Created by neat on 2019/11/27.
//  Copyright © 2019 Neat. All rights reserved.
//

import UIKit
import AVFoundation


protocol QRScanViewDelegate {
    
    func qrScanFailed()
    
    func qrScanSuccess(string:String)
    
}

enum QRCodeType {
    case DeviceCodeType //设备编码
    case PointCodeType  //巡检点编码
}

class QRScanView: UIView {
    
    var avCaptureDevice:AVCaptureDevice?
    
    var delegate:QRScanViewDelegate?
    
    var codeType:QRCodeType = .DeviceCodeType
    
    private lazy var metadataOutPut: AVCaptureMetadataOutput = {
        
        let output = AVCaptureMetadataOutput.init()
        
        // Drawing code
        let width = self.frame.size.width;
        
        
        //扫描框宽度
        let sc_bordeWidth = width * NormalConstant.SCBORDERWIDTHSCAL;
        //扫描框高度
        let sc_bordeHeight = sc_bordeWidth;
        
        //扫描框左上角X坐标
        let sc_left_top_xPoint = (NormalConstant.ScreenWidth - sc_bordeWidth)/2.0;
        //扫描框左上角Y坐标
        let sc_left_top_yPoint = NormalConstant.SCBORDERTOP;
        
        //rectOfInterest属性设置设备的扫描范围
        output.rectOfInterest = CGRect.init(x:sc_left_top_yPoint/NormalConstant.ScreenHeight,y:sc_left_top_xPoint/NormalConstant.ScreenWidth,width:sc_bordeHeight/NormalConstant.ScreenHeight,height:  sc_bordeWidth/NormalConstant.ScreenWidth);
        
        return output
    }()
    
    private lazy var sessionManager: AVCaptureSession = {
        let session = AVCaptureSession.init()
        
        return session
    }()
    
    private lazy var previewLayer: AVCaptureVideoPreviewLayer = {
        let previewLayer = AVCaptureVideoPreviewLayer.init(session: self.sessionManager)
        previewLayer.videoGravity = .resizeAspectFill
        return previewLayer
    }()
    
    

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        configerSession()
        
        startScan()
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func startScan() {
        
        let queue = DispatchQueue.global()
        
        queue.async {
            self.sessionManager.startRunning()
        }
    }
    
    func stopScan() {
        self.sessionManager.stopRunning()
    }
    
    override func layoutSubviews() {
        self.previewLayer.frame = self.bounds
    }
    
    private func configerSession() {
        
        avCaptureDevice = AVCaptureDevice.default(.builtInWideAngleCamera, for: .video, position: .back)
        
        do {
            let inputDevice = try AVCaptureDeviceInput.init(device: avCaptureDevice!)
            
            if self.sessionManager.canAddOutput(self.metadataOutPut) {
                self.sessionManager.addOutput(self.metadataOutPut)
            }
            if self.sessionManager.canAddInput(inputDevice) {
                self.sessionManager.addInput(inputDevice)
            }
        } catch {
            self.delegate?.qrScanFailed()
            return
        }
        
        if self.metadataOutPut.availableMetadataObjectTypes.contains(AVMetadataObject.ObjectType.qr) {
            self.metadataOutPut.metadataObjectTypes = [AVMetadataObject.ObjectType.qr]
        }else{
            self.delegate?.qrScanFailed()
        }
        
        self.metadataOutPut.setMetadataObjectsDelegate(self, queue: DispatchQueue.main)
        
        self.layer.addSublayer(self.previewLayer)
        
    }
    
    /// 开启闪光灯
    func torchOn() {
        if avCaptureDevice!.hasFlash {
            do {
                
                if !avCaptureDevice!.isTorchActive {
                    try avCaptureDevice?.lockForConfiguration()
                    avCaptureDevice?.torchMode = .on
                    avCaptureDevice?.unlockForConfiguration()
                }
                
            } catch {
                
            }
            
        }
    }
    
    /// 关闭闪光灯
    func torchOff() {
        if avCaptureDevice!.hasFlash {
            do {
                
                if avCaptureDevice!.isTorchActive {
                    try avCaptureDevice?.lockForConfiguration()
                    avCaptureDevice?.torchMode = .off
                    avCaptureDevice?.unlockForConfiguration()
                }
                
            } catch {
                
            }
            
        }
    }

}

extension QRScanView:AVCaptureMetadataOutputObjectsDelegate{
    func metadataOutput(_ output: AVCaptureMetadataOutput, didOutput metadataObjects: [AVMetadataObject], from connection: AVCaptureConnection) {
        
        if metadataObjects.count > 0 {
            
            self.stopScan()
            
            let metadataObject = metadataObjects[0] as! AVMetadataMachineReadableCodeObject
            
            if self.codeType == .DeviceCodeType {
                //设备编码
                self.delegate?.qrScanSuccess(string: metadataObject.stringValue ?? "")
            }else if self.codeType == .PointCodeType{
                //巡检点编码
                self.delegate?.qrScanSuccess(string: metadataObject.stringValue ?? "")
            }
            
            
        }
        
    }
}
