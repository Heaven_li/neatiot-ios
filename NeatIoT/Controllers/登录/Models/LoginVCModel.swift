//
//  LoginVCModel.swift
//  NeatIoT
//
//  Created by neat on 2019/10/24.
//  Copyright © 2019 Neat. All rights reserved.
//

import UIKit

class LoginVCModel: NSObject {
    
    class func checkParameters(host:String,userName:String,password:String,view:UIView) -> Bool {
        
        if !host.isEmpty && !userName.isEmpty && !password.isEmpty {
            
            let hostCheckResult = DataVerify.checkHost(host: host)
            
            if hostCheckResult.correct ?? false {
                
                let userNameCheckResult = DataVerify.checkUserName(userName: userName)
                
                if userNameCheckResult.correct ?? false {
                    
                    let passwordResult = DataVerify.checkPassword(password: password)
                    if passwordResult.correct ?? false {
                        return true
                    }else{
                        NeatHud.showMessage(message: passwordResult.errMessage!, view: view)
                    }
                    
                }else{
                    NeatHud.showMessage(message: userNameCheckResult.errMessage!, view: view)
                }
                
            }else{
                NeatHud.showMessage(message: hostCheckResult.errMessage!, view: view)
            }
            
        }else{
            NeatHud.showMessage(message: "数据不能为空", view: view)
        }
        
        return false
    }

}
