//
//  LoginViewController.swift
//  NeatIoT
//
//  Created by neat on 2019/9/29.
//  Copyright © 2019 Neat. All rights reserved.
//

import UIKit
import SnapKit
import Alamofire



class LoginViewController: SupperViewController {
    
    var isPopBack:Bool?
    
    final let ProAndPri = "ProAndPri"
    
    var isCheckBox = false
    
    /// 背景图
    lazy var bgView: UIImageView = {
        let imageView = UIImageView.init(image: UIImage.init(named: "login_bg"))
        return imageView
    }()
    
    /// 底部插画
    lazy var btCoverView: UIImageView = {
        let coverView = UIImageView.init(image: UIImage.init(named: "login_bt_cover"))
        return coverView
    }()
    
    
    /// logo
    lazy var logoView: UIImageView = {
        let logoView = UIImageView.init(image: UIImage.init(named: "login_logo"))
        return logoView
    }()
    
    
    /// IP端口号
    lazy var domainTextField: IconTextField = {
        let iconTextField = IconTextField.init()
        iconTextField.iconName = "login_ip"
        iconTextField.placeHolder = "请输入IP地址及端口号"
//        iconTextField.textValue = "192.168.0.100:5400"
        iconTextField.keyboardType = .numbersAndPunctuation
        return iconTextField
    }()
    
    /// 用户名
    lazy var userNameTextField: IconTextField = {
        let textField = IconTextField.init()
        textField.iconName = "login_username"
        textField.placeHolder = "请输入用户名"
//        textField.textValue = "ly007"
        textField.keyboardType = .default
        return textField
    }()
    
    /// 密码
    lazy var passwordTextField: IconTextField = {
        let textField = IconTextField.init()
        textField.iconName = "login_password"
        textField.placeHolder = "请输入密码"
        textField.SecureTextEntry = true
//        textField.textValue = "123456"
        textField.keyboardType = .asciiCapable
        return textField
    }()
    
    /// 协议复选框
    lazy var checkBtn: UIButton = {
        let checkBtn = UIButton.init(type: .custom)
        checkBtn.setImage(UIImage.init(named: "en_check_box"), for: .normal)
        checkBtn.setImage(UIImage.init(named: "check_box"), for: .selected)
        checkBtn.addTarget(self, action: #selector(checkBoxClick(btn:)), for: .touchUpInside)
        return checkBtn
    }()
    
    lazy var checkTextView: UITextView = {
        
        let textView = UITextView.init()
        textView.font = UIFont.systemFont(ofSize: 14)
        textView.isEditable = false
        textView.backgroundColor = .clear
        textView.isScrollEnabled = false
        textView.delegate = self
        
        let str = NSString.init(string: "已阅读并同意《隐私政策》和《服务协议》")
        
        let attributeStr = NSMutableAttributedString.init(string: str as String)
        attributeStr.addAttribute(.link, value: "private://", range: str.range(of:"《隐私政策》"))
        attributeStr.addAttribute(.link, value: "protect://", range: str.range(of:"《服务协议》"))
        
        attributeStr.addAttribute(.foregroundColor, value: #colorLiteral(red: 0.01926351836, green: 0.3614924173, blue: 0.8830865977, alpha: 1), range: str.range(of:"已阅读并同意"))
        attributeStr.addAttribute(.foregroundColor, value: #colorLiteral(red: 0.01926351836, green: 0.3614924173, blue: 0.8830865977, alpha: 1), range: str.range(of:"和"))
        
        textView.linkTextAttributes = [.foregroundColor:#colorLiteral(red: 0.9415976476, green: 0.9415976476, blue: 0.9415976476, alpha: 1)]
        textView.attributedText = attributeStr
        
        
        return textView
    }()
    
    /// 登录按钮
    lazy var loginBtn: UIButton = {
        let btn = UIButton.init()
        btn.setTitle("登 录", for: .normal)
        btn.titleLabel?.font = .systemFont(ofSize: 19)
        btn.backgroundColor = .white
        btn.setTitleColor(#colorLiteral(red: 0.003921568627, green: 0.2078431373, blue: 0.5058823529, alpha: 1), for: .normal)
        btn.setTitleColor(#colorLiteral(red: 0.00204584207, green: 0.2991411643, blue: 0.7276512143, alpha: 1), for: .highlighted)
        btn.layer.cornerRadius = NormalConstant.RadioBtnCorner
        btn.addTarget(self, action: #selector(login), for: .touchUpInside)
        return btn
    }()
    
    lazy var versionLable: UILabel = {
        
        let lable = UILabel.init();
        let infoDic = Bundle.main.infoDictionary
        // 获取App的版本号
        let appVersion = infoDic?["CFBundleShortVersionString"]
        lable.text = "v" + (appVersion as! String)
        lable.textColor = UIColor.white
        lable.textAlignment = .center
        lable.font = UIFont.systemFont(ofSize: 14)
        
        return lable;
    }()
    
    
    override var preferredStatusBarStyle: UIStatusBarStyle{
        return .lightContent
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        
        let host = UserDefaults.standard.value(forKey: "host") as? String ?? ""
        
        let userName = UserDefaults.standard.value(forKey: "userName") as? String ?? ""
        
//        let password = UserDefaults.standard.value(forKey: "password") as? String ?? ""
        
        if !host.isEmpty {
            self.domainTextField.textValue = host
        }
        if !userName.isEmpty {
            self.userNameTextField.textValue = userName
        }
//        if !password.isEmpty {
//            self.passwordTextField.textValue = password
//        }
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        self.isCanSwiperBack = false
        configerUI()
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(notification:)), name:UIResponder.keyboardWillShowNotification , object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillDismiss(notification:)), name: UIResponder.keyboardWillHideNotification, object: nil)
        
        
        
    }
    
    func configerUI() -> Void {
        
        self.view.addSubview(bgView)
        bgView.snp.makeConstraints { (make) in
            make.edges.equalTo(self.view)
        }
        
        self.view.addSubview(btCoverView)
        btCoverView.snp.makeConstraints { (make) in
            make.left.right.equalTo(self.view)
            make.bottom.equalTo(self.view)
        }
        
        self.view.addSubview(logoView)
        logoView.snp.makeConstraints { (make) in
            make.top.equalTo(self.view.snp.top).offset(self.view.frame.size.height*0.15)
            make.centerX.equalTo(self.view)
        }
        
        self.view.addSubview(domainTextField)
        domainTextField.snp.makeConstraints { (make) in
            make.top.equalTo(logoView.snp.bottom).offset(self.view.frame.size.height*0.08)
            make.centerX.equalTo(self.view)
            make.left.equalTo(self.view).offset(30)
            make.right.equalTo(self.view).offset(-30)
            make.height.equalTo(self.view).multipliedBy(0.06)
        }
        
        self.view.addSubview(userNameTextField)
        userNameTextField.snp.makeConstraints { (make) in
            make.top.equalTo(domainTextField.snp.bottom).offset(10)
            make.centerX.equalTo(self.view)
            make.left.equalTo(self.view).offset(30)
            make.right.equalTo(self.view).offset(-30)
            make.height.equalTo(self.view).multipliedBy(0.06)
        }
        
        self.view.addSubview(passwordTextField)
        passwordTextField.snp.makeConstraints { (make) in
            make.top.equalTo(userNameTextField.snp.bottom).offset(10)
            make.centerX.equalTo(self.view)
            make.left.equalTo(self.view).offset(30)
            make.right.equalTo(self.view).offset(-30)
            make.height.equalTo(self.view).multipliedBy(0.06)
        }
        
        let isAccept = UserDefaults.standard.bool(forKey: ProAndPri)
        
        if !isAccept {
            self.view.addSubview(checkBtn)
            checkBtn.snp.makeConstraints { (make) in
                make.top.equalTo(passwordTextField.snp.bottom).offset(10)
                make.leading.equalTo(passwordTextField.snp.leading)
                make.width.equalTo(20)
                make.height.equalTo(20)
            }
            
            self.view.addSubview(checkTextView)
            checkTextView.snp.makeConstraints { (make) in
                make.centerY.equalTo(self.checkBtn)
                make.left.equalTo(self.checkBtn.snp.right)
                make.right.equalTo(self.passwordTextField.snp.right)
            }
        }
        
        self.view.addSubview(loginBtn)
        loginBtn.snp.makeConstraints { (make) in
            make.top.equalTo(passwordTextField.snp.bottom).offset(40)
            make.centerX.equalTo(self.view)
            make.left.equalTo(self.view).offset(30)
            make.right.equalTo(self.view).offset(-30)
            make.height.equalTo(NormalConstant.RadioBtnHeight)
        
        }
        
        self.view.addSubview(versionLable)
        versionLable.snp.makeConstraints { (make) in
            make.bottom.equalTo(self.view).offset(-30);
            make.leading.trailing.equalTo(self.view);
        }
        
    
    }
    
    @objc func checkBoxClick(btn:UIButton) {
        
        self.checkBtn.isSelected = !self.checkBtn.isSelected
        
        isCheckBox = self.checkBtn.isSelected
        
        
    }
    
    @objc func keyboardWillShow(notification:NSNotification) -> Void {

        let rect = notification.userInfo?["UIKeyboardFrameEndUserInfoKey"] as! CGRect

        let aniDur = notification.userInfo?["UIKeyboardAnimationDurationUserInfoKey"] as! TimeInterval
        
        let currentLoginBtnY = loginBtn.frame.origin.y
        
        if rect.origin.y <= currentLoginBtnY{
            //挡住了按钮
            let transformY = rect.origin.y - currentLoginBtnY - 10 - loginBtn.frame.size.height
            
            let proAni = UIViewPropertyAnimator.init(duration: aniDur , curve:.linear ) {
                self.view.transform = CGAffineTransform(translationX: 0, y: transformY)
            }
            
            proAni.startAnimation()
            
        }else if rect.origin.y > currentLoginBtnY && rect.origin.y <= currentLoginBtnY + loginBtn.frame.size.height{
            //挡住了部分按钮
            let transformY = (rect.origin.y - currentLoginBtnY) - loginBtn.frame.size.height - 10
            let proAni = UIViewPropertyAnimator.init(duration: aniDur , curve:.easeInOut) {
                self.view.transform = CGAffineTransform(translationX: 0, y: transformY)
            }
            proAni.startAnimation()
            
        }

    }
    
    @objc func keyboardWillDismiss(notification:NSNotification) -> Void {
        
        let aniDur = notification.userInfo?["UIKeyboardAnimationDurationUserInfoKey"] as! TimeInterval
        
        let proAni = UIViewPropertyAnimator.init(duration: aniDur , curve:.easeInOut) {
            self.view.transform = CGAffineTransform(translationX: 0, y: 0)
        }
        proAni.startAnimation()
    }
    
    @objc func login() -> Void {
        
        let host = domainTextField.textField.text
        
        let userName = userNameTextField.textField.text
        
        let password = passwordTextField.textField.text
        
        let isOk = LoginVCModel.checkParameters(host: host ?? "", userName: userName ?? "", password: password ?? "",view:self.view)
        
        if isOk {
            
            
            if !UserDefaults.standard.bool(forKey: ProAndPri) {
                if !isCheckBox {
                    NeatHud.showMessage(message: "请先勾选隐私政策", view: self.view)
                    return
                }
            }
            
            
            
            UrlHost.urlHost = host!
            
            NeatHud.showLoading(view: self.view, message: "")
            
            NeatRequest.instance.login(userName: userName!, password: password!, success: { (success) in
                
                NeatHud.dissmissHud(view: self.view)
            
                let resultData = success.response_data
                let token = resultData!.user_token
                
                UserDefaults.standard.set(host, forKey: "host")
                UserDefaults.standard.set(userName, forKey: "userName")
                UserDefaults.standard.set(password, forKey: "password")
                UserDefaults.standard.set(true, forKey: self.ProAndPri)
                
                let newToken = token.replacingOccurrences(of: "-", with: "")
                
                resultData?.user_token = newToken
                resultData?.user_login = true
                
                
                
                JPUSHService.setAlias(resultData?.user_id, completion: { (ireCode, alias, seq) in
                    debugPrint("\(alias ?? "")设置成功")
                }, seq: 0)
                
                
                var set:Set<String> = []
                
                for tag in resultData?.user_roles ?? []{
                    
                    set.insert(tag)
                }
                
                JPUSHService.setTags(set, completion: { (ireCode, tags, seq) in
                    debugPrint("\(tags ?? [])设置成功")
                }, seq: 1)
                
                DataArchiver.archiver(value: resultData!, key:ArchireKey.UserInfor)
                
                if self.isPopBack ?? false{
                        self.navigationController?.popViewController(animated: true)
                    }else{
                        self.navigationController?.pushViewController(HomeFunctionViewController.init(), animated: true)
                
                    }
                    
                let rootNav = SupperNavigationViewController.init(rootViewController: HomeFunctionViewController.init())
                UIApplication.shared.keyWindow?.rootViewController = rootNav
                
                    
            }) { (error) in
            
                NeatHud.showMessage(message: String(error.errCode)+error.errMessage, view: self.view)
                
            }
        }
    }

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        self.resignFirstResponder()
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension LoginViewController:UITextViewDelegate{
    
    func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange, interaction: UITextItemInteraction) -> Bool {
        
        if URL.scheme == "protect" {
            
            let vc = WebViewViewController.init();
            vc.navTitleStr = "服务协议"
            self.navigationController?.pushViewController(vc, animated: true)
            return false
        }else if URL.scheme == "private"{
            
            let vc = WebViewViewController.init();
            vc.navTitleStr = "隐私政策"
            self.navigationController?.pushViewController(vc, animated: true)
            return false;
        }
        
        return true
    }
    
}
