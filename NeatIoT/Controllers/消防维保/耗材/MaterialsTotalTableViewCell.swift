//
//  MaterialsTotalTableViewCell.swift
//  NeatIoT
//
//  Created by Yang Blus on 2020/5/12.
//  Copyright © 2020 Neat. All rights reserved.
//

import UIKit

protocol MaterialsTotalTableViewCellDelegate {
    
    func addClick(model:ProtectionMaterials)
    func mesClick(model:ProtectionMaterials)
    func reloadTable()
    
    func hudTip(message:String)
    
}

class MaterialsTotalTableViewCell: UITableViewCell {
    
    var currentModel:ProtectionMaterials?
    
    var delegate:MaterialsTotalTableViewCellDelegate?
    
    @IBOutlet weak var deleteImageView: UIImageView!
    
    @IBOutlet weak var materialsLable: UILabel!
    
    @IBOutlet weak var countTextField: UITextField!
    
    @IBOutlet weak var mesImageView: UIImageView!
    
    @IBOutlet weak var countLable: UILabel!
    
    @IBOutlet weak var addImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.backgroundColor = .white
        
        self.countTextField.layer.cornerRadius = 5
        self.countTextField.clipsToBounds = true
        self.countTextField.delegate = self
        
        let padView = UIView.init()
        padView.frame = CGRect.init(x: 0, y: 0, width: 6, height: 6)
        self.countTextField.leftView = padView
        self.countTextField.rightView = padView
        self.countTextField.leftViewMode = .always
        self.countTextField.rightViewMode = .always
        
        let addGesture = UITapGestureRecognizer.init(target: self, action: #selector(addGesture(_:)))
        self.addImageView.isUserInteractionEnabled = true
        self.addImageView.addGestureRecognizer(addGesture)
        
        let mesGesture = UITapGestureRecognizer.init(target: self, action: #selector(mesGesture(_:)))
        self.mesImageView.isUserInteractionEnabled = true
        self.mesImageView.addGestureRecognizer(mesGesture)
        
        let deleteGesture = UITapGestureRecognizer.init(target: self, action: #selector(deleteTap))
        self.deleteImageView.isUserInteractionEnabled = true
        self.deleteImageView.addGestureRecognizer(deleteGesture)
        
        self.countTextField.delegate = self
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
        
        
    }
    
    func configerCell(model:ProtectionMaterials) {
        
        currentModel = model
        
        self.materialsLable.text = model.materialsName
        
        self.countLable.text = String(model.materialsCount)
        
        self.countTextField.text = String.init(format: "%.1f", model.tureTotalMoney)
        
        
    }
    
    @objc func deleteTap() {
        
        currentModel?.materialsCount = 0
        currentModel?.addScTime = nil
        CoreDataHelper.init().updateModel(model: currentModel!,updataTime: true)
        
        self.delegate?.reloadTable()
        
    }
    
    @objc func addGesture(_ sender: Any) {
           
        currentModel!.materialsCount+=1
        
        
        if currentModel!.materialsCount >= 999 {
            self.delegate?.hudTip(message: "耗材数量不能超过999")
            currentModel!.materialsCount = 999
        
        }
       
        let totalPrice = currentModel!.materialsPrice! * Float(currentModel!.materialsCount)
        
        currentModel!.tureTotalMoney = Double(totalPrice)
       
        CoreDataHelper.init().updateModel(model: currentModel!,updataTime: false)
        
        self.delegate?.addClick(model: currentModel!)
           
    }
       
    @objc func mesGesture(_ sender: Any) {
           
       if currentModel?.materialsCount == 0 {
           
           currentModel?.addScTime = nil
            CoreDataHelper.init().updateModel(model: currentModel!,updataTime: true)
        
       }else{
           currentModel!.materialsCount-=1
        
        if currentModel?.materialsCount == 0 {
            currentModel?.addScTime = nil
            CoreDataHelper.init().updateModel(model: currentModel!,updataTime: true)
        }else{
            
            let totalPrice = currentModel!.materialsPrice! * Float(currentModel!.materialsCount)
            currentModel!.tureTotalMoney = Double(totalPrice)
            CoreDataHelper.init().updateModel(model: currentModel!,updataTime: false)
        }
            
       }
       
       self.delegate?.mesClick(model: currentModel!)
           
    }
    
}

extension MaterialsTotalTableViewCell:UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        self.countTextField.resignFirstResponder()
        
        if nil != textField.text && !(textField.text?.isEmpty ?? true) {
            
            currentModel?.tureTotalMoney = Double(textField.text ?? "0") ?? 0
            CoreDataHelper.init().updateModel(model: currentModel!,updataTime: false)
            
        }
        
        self.delegate?.reloadTable()
        
        return true
        
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        if nil != textField.text && !(textField.text?.isEmpty ?? true) {
            
            currentModel?.tureTotalMoney = Double(textField.text ?? "0") ?? 0
            CoreDataHelper.init().updateModel(model: currentModel!,updataTime: true)
            
        }
    
        textField.resignFirstResponder()
        
        self.delegate?.reloadTable()
        
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if nil != textField.text && !textField.text!.isEmpty {
            let endDoc = textField.endOfDocument
            
            let endPos = textField.position(from: endDoc, offset: 0)
            
            let startPos = textField.position(from: endPos!, offset: -textField.text!.count)
            
            textField.selectedTextRange = textField.textRange(from: startPos!, to: endPos!)
        }
        
        
        
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        
        return true
        
    }
    
    
    
}
