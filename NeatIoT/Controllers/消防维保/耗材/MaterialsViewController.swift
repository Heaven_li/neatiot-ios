//
//  MaterialsViewController.swift
//  NeatIoT
//
//  Created by Yang Blus on 2020/5/12.
//  Copyright © 2020 Neat. All rights reserved.
//

import UIKit

class MaterialsViewController: SupperViewController {
    
    let MaterialsTableViewCellIdent = "MaterialsTableViewCellIdent"
    
    var dataSourceArr:Array<ProtectionMaterials> = []
    
    var isFromTask:Bool = true
    
    lazy var containTableView: LZTableView = {
        
        let tableView = LZTableView.init(frame: CGRect.zero, style: .plain)
        tableView.bounces = false
        tableView.delegate = self
        tableView.dataSource = self
        tableView.separatorStyle = .singleLine
        tableView.backgroundColor = NormalColor.viewControllerDefaultBgColor
        tableView.separatorInset = UIEdgeInsets.init(top: 0, left: 0, bottom: 0, right: 0)
        tableView.register(UINib.init(nibName: "MaterialTableViewCell", bundle: Bundle.main), forCellReuseIdentifier: MaterialsTableViewCellIdent)
        
        return tableView
    }()

    lazy var topSearchView: TopSearchView = {
        let topSearchView = TopSearchView.init()
        topSearchView.delegate = self
        return topSearchView
    }()
    
    lazy var nextBtn: UIButton = {
        let btn = UIButton.init(type: .custom)
        btn.setTitle("去结算", for: .normal)
        btn.backgroundColor = #colorLiteral(red: 0.003921568859, green: 0.3411764801, blue: 0.5568627715, alpha: 1)
        btn.setTitleColor(.white, for: .normal)
        btn.addTarget(self, action: #selector(goNext), for: .touchUpInside)
        return btn
    }()
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        getMaterialsList()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configerNav()
        configerUI()
        
    }
    
    func configerNav() {
        self.titleName = "添加耗材"
    }
    
    func configerUI() {
        
        self.view.addSubview(self.topSearchView)
        
        self.view.addSubview(self.containTableView)
        
        self.view.addSubview(self.nextBtn)
        
        self.topSearchView.snp.makeConstraints { (make) in
            make.top.equalTo(self.view)
            make.leading.trailing.equalTo(self.view)
            make.height.equalTo(NormalConstant.TopSearchViewHeight)
        }
        
        self.containTableView.snp.makeConstraints { (make) in
            make.top.equalTo(self.topSearchView.snp.bottom)
            make.leading.trailing.equalTo(self.view)
            make.bottom.equalTo(self.nextBtn.snp.top)
        }
        
        self.nextBtn.snp.makeConstraints { (make) in
            make.bottom.trailing.leading.equalTo(self.view)
            make.height.equalTo(54)
        }
        
    }
    
    func getMaterialsList() {
    
        NeatRequest.instance.getMaterialsList(pageIndex: 1, success: { (response) in

            self.dataSourceArr.removeAll()
            self.dataSourceArr.append(contentsOf: response.result!.materialArr!)

            for model in self.dataSourceArr {
                
                CoreDataHelper.init().insertMaterialsObjec(model: model)
            }

            self.dataSourceArr.removeAll()
            self.dataSourceArr.append(contentsOf: CoreDataHelper.init().readObjec())
            
            if self.dataSourceArr.count == 0 {
                self.containTableView.showEmptyView()
            }else{
                self.containTableView.hidenHolderView()
            }
            
            self.containTableView.reloadData()
            
        }) { (errorResponse) in

            if errorResponse.errCode == RequestResponseCode.dataParsingException {
                //数据异常
                NeatHud.dissmissHud(view: self.view)
                self.containTableView.showRequestFaildView {
                    
                }
            }else if errorResponse.errCode == RequestResponseCode.netCanNotContent || errorResponse.errCode == RequestResponseCode.requestTimeOut || errorResponse.errCode == RequestResponseCode.canNotContentHost{
                //无网络
                NeatHud.dissmissHud(view: self.view)
                self.containTableView.showNetErrorView(errorMessage: errorResponse.errMessage) {
                    self.getMaterialsList()
                }
            }else{
                NeatHud.showMessage(message: errorResponse.errMessage, view: self.view)
            }

        }
        
    }
    
    @objc func goNext() {
        
        let vc = MaterialsTotalViewController.init()
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}


extension MaterialsViewController:TopSearchViewDelegate{
    
    func doSearchWith(keyword: String) {
        
        if keyword.isEmpty{
            self.getMaterialsList()
        }else{
            dataSourceArr.removeAll()
            dataSourceArr.append(contentsOf: CoreDataHelper.init().searchMaterialsWithKeyWord(keyWord: keyword))
            self.containTableView.reloadData()
        }
    
    }
    
}

extension MaterialsViewController:UITableViewDelegate{
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        self.view.endEditing(false)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 46
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.01
    }
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView.init()
    }
    
}
extension MaterialsViewController:UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.dataSourceArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: MaterialsTableViewCellIdent, for: indexPath) as! MaterialTableViewCell
        cell.delegate = self
        cell.configerCell(model: dataSourceArr[indexPath.row])
        cell.selectionStyle = .none
        
        return cell
    }
    
}

extension MaterialsViewController:MaterialTableViewCellDelegate {
    
    func hudTip(message: String) {
        NeatHud.showMessage(message: message, view: self.view)
    }
    
    func addClick(model: ProtectionMaterials) {
        
        self.getMaterialsList()
        self.containTableView.reloadData()
        
    }
    
    func mesClick(model: ProtectionMaterials) {
        self.getMaterialsList()
        self.containTableView.reloadData()
    }
    
    func inputMaterialCount(model: ProtectionMaterials) {
        self.getMaterialsList()
        
    }
    
    
    
}
