//
//  MaterialsTotalViewController.swift
//  NeatIoT
//
//  Created by Yang Blus on 2020/5/12.
//  Copyright © 2020 Neat. All rights reserved.
//

import UIKit

class MaterialsTotalViewController: SupperViewController {
    
    let MaterialsTotalTableViewCellIdent = "MaterialsTotalTableViewCellIdent"
    
    var isFirstLoad:Bool = true
    
    var dataSourceArr:Array<ProtectionMaterials> = []
    
    lazy var containTableView: UITableView = {
        
        let tableView = UITableView.init(frame: CGRect.zero, style: .plain)
        tableView.bounces = false
        tableView.delegate = self
        tableView.dataSource = self
        tableView.backgroundColor = NormalColor.viewControllerDefaultBgColor
        tableView.separatorInset = UIEdgeInsets.init(top: 0, left: 0, bottom: 0, right: 0)
        tableView.separatorStyle = .singleLine
        tableView.register(UINib.init(nibName: "MaterialsTotalTableViewCell", bundle: Bundle.main), forCellReuseIdentifier: MaterialsTotalTableViewCellIdent)
        
        return tableView
    }()

    lazy var paymentView: MaterialsPaymentView = {
        let view = MaterialsPaymentView.init()
        view.delegate = self
        return view
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configerNav()
        configerUI()

        updateTableView()
        
    }
    func configerNav() {
        
        self.titleName = "结算"
        
    }
    
    func configerUI() {
        
        self.view.addSubview(self.containTableView)
        
        self.view.addSubview(self.paymentView)
        
        self.containTableView.snp.makeConstraints { (make) in
            make.top.leading.trailing.equalTo(self.view)
            make.bottom.equalTo(self.paymentView.snp.top)
        }
        
        self.paymentView.snp.makeConstraints { (make) in
            make.bottom.equalTo(self.view).offset(-10)
            make.leading.equalTo(self.view).offset(10)
            make.trailing.equalTo(self.view).offset(-10)
            make.height.equalTo(56)
        }
        
    }
    
    func updateTableView() {
        
        dataSourceArr.removeAll()
        dataSourceArr.append(contentsOf: CoreDataHelper.init().getMaterialsByAddTime())
        
        for model in dataSourceArr{
            
            if isFirstLoad {
                let totalPrice = model.materialsPrice! * Float(model.materialsCount)
                
                model.tureTotalMoney = Double(totalPrice)
                
                CoreDataHelper.init().updateModel(model: model, updataTime: false)
            }
        }
        
        dataSourceArr.removeAll()
        
        dataSourceArr.append(contentsOf: CoreDataHelper.init().getMaterialsByAddTime())
        
        self.containTableView.reloadData()
        
        self.updataBottomView()
        
        isFirstLoad = false
    }
    
    func updataBottomView() {
        
        let materCount = dataSourceArr.count
        var truePayment:Float = 0
        var nativePayment:Float = 0
        for model in dataSourceArr {
            truePayment += Float(model.tureTotalMoney)
            nativePayment += Float(model.materialsPrice!) * Float(model.materialsCount)
        }
        

        self.paymentView.reloadDataWith(materCount: materCount, turePayMent: truePayment, nativePayMent: nativePayment)
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension MaterialsTotalViewController:UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 48
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.1
    }
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView.init()
    }
    
}

extension MaterialsTotalViewController:UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.dataSourceArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: MaterialsTotalTableViewCellIdent, for: indexPath) as! MaterialsTotalTableViewCell
        cell.selectionStyle = .none
        cell.configerCell(model: self.dataSourceArr[indexPath.row])
        cell.delegate = self
        
        
        return cell
    }
    
    
    
}

extension MaterialsTotalViewController:MaterialsTotalTableViewCellDelegate{
    
    func reloadTable() {
        self.updateTableView()
    }

    func addClick(model: ProtectionMaterials) {
        
        self.updateTableView()
        
    }
    
    func mesClick(model: ProtectionMaterials) {
        self.updateTableView()
    }
    
    func hudTip(message: String) {
        NeatHud.showMessage(message: message, view: self.view)
    }
    
}

extension MaterialsTotalViewController:MaterialsPaymentViewDelegate{
    
    func didClickFinish() {
        
        CoreDataHelper.init().deleteMaterialsObjec()
        
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: CustomNotificationName.ReciveMaterialsData), object: self.dataSourceArr)
        
        self.popTo(vcClass: ProtectionHandleViewController.init())
        
        
    }
 
}
