//
//  MaterialTableViewCell.swift
//  NeatIoT
//
//  Created by Yang Blus on 2020/5/12.
//  Copyright © 2020 Neat. All rights reserved.
//

import UIKit

protocol MaterialTableViewCellDelegate {
    
    func addClick(model:ProtectionMaterials)
    func mesClick(model:ProtectionMaterials)
    func inputMaterialCount(model:ProtectionMaterials)
    func hudTip(message:String)
    
}

class MaterialTableViewCell: UITableViewCell {
    
    var currentModel:ProtectionMaterials?
    
    var delegate:MaterialTableViewCellDelegate?
    
    @IBOutlet weak var materialNameLable: UILabel!
    
    @IBOutlet weak var materialUnitLable: UILabel!
    //减
    @IBOutlet weak var mesImageView: UIImageView!
    //加
    @IBOutlet weak var addImageView: UIImageView!

    
    @IBOutlet weak var countTextField: UITextField!
    
    @IBOutlet weak var materialUnitBg: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.backgroundColor = .white
        self.materialUnitBg.layer.cornerRadius = 5
        
        let addGesture = UITapGestureRecognizer.init(target: self, action: #selector(addGesture(_:)))

        self.addImageView.addGestureRecognizer(addGesture)
        
        let mesGesture = UITapGestureRecognizer.init(target: self, action: #selector(mesGesture(_:)))

        self.mesImageView.addGestureRecognizer(mesGesture)
        
        self.countTextField.delegate = self
        self.countTextField.addTarget(self, action: #selector(countTextFieldValueChange), for: .editingChanged)
        
    }
    
    @objc func countTextFieldValueChange() {
        
        if (self.countTextField.text?.count ?? 0 > 3) {
            
            let count = String.init(self.countTextField.text!)
            let index = count.index(count.startIndex, offsetBy: 3)
            let newCount = count[..<index]
            
            self.countTextField.text = String.init(newCount)
        }
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configerCell(model:ProtectionMaterials) {
        
        currentModel = model
        
        self.materialNameLable.text = model.materialsName
        
        let price = String.init(format: "¥ %.1f", model.materialsPrice!)
        
        self.materialUnitLable.text = price + "/" + model.materialsUnit!
        
        if model.materialsCount != 0 {
            self.countTextField.text = String(model.materialsCount)
            self.countTextField.isHidden = false
            self.mesImageView.isHidden = false
            self.addImageView.isHidden = false
        }else{
            self.countTextField.isHidden = true
            self.mesImageView.isHidden = true
            self.addImageView.isHidden = false
        }
        
    }
    @objc func addGesture(_ sender: Any) {
        
        currentModel!.materialsCount+=1
        
        if currentModel?.materialsCount == 1 {
            currentModel?.addScTime = Date.init(timeIntervalSinceNow: 0)
            CoreDataHelper.init().updateModel(model: currentModel!,updataTime: true)
        }else{
            
            if currentModel!.materialsCount >= 999 {
                currentModel!.materialsCount = 999
                self.delegate?.hudTip(message: "耗材数量不能超过999")
            }
            CoreDataHelper.init().updateModel(model: currentModel!,updataTime: false)
            
            
            
        }
        
        self.delegate?.addClick(model: currentModel!)
        
    }
    
    @objc func mesGesture(_ sender: Any) {
        
        if currentModel?.materialsCount == 0 {
            self.mesImageView.isHidden = true
            self.countTextField.isHidden = true
            CoreDataHelper.init().updateModel(model: currentModel!,updataTime: true)
            
        }else{
            
            currentModel!.materialsCount-=1
            if currentModel?.materialsCount == 0 {
                currentModel?.addScTime = nil
                CoreDataHelper.init().updateModel(model: currentModel!,updataTime: true)
            }else{
                CoreDataHelper.init().updateModel(model: currentModel!,updataTime: false)
            }
                
        }
        
        self.delegate?.mesClick(model: currentModel!)
        
    }
    
}

extension MaterialTableViewCell:UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        self.countTextField.resignFirstResponder()
        
        return true
        
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        if nil != textField.text && !(textField.text?.isEmpty ?? true) {
            
            var count = Int(textField.text ?? "0") ?? 0
            
            if Int(textField.text ?? "0") ?? 0 == 0 {
                currentModel?.materialsCount = count
                currentModel?.addScTime = nil
                CoreDataHelper.init().updateModel(model: currentModel!,updataTime: true)
            }else{
                currentModel?.materialsCount = count
                CoreDataHelper.init().updateModel(model: currentModel!,updataTime: true)
            }
            self.delegate?.mesClick(model: currentModel!)
            
        }else{
            
            currentModel?.materialsCount = 0
            currentModel?.addScTime = nil
            CoreDataHelper.init().updateModel(model: currentModel!,updataTime: true)
            self.delegate?.mesClick(model: currentModel!)
            
        }
        
        
        textField.resignFirstResponder()
        
        
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if nil != textField.text && !textField.text!.isEmpty {
            
            let endDoc = textField.endOfDocument
            
            let endPos = textField.position(from: endDoc, offset: 0)
            
            let startPos = textField.position(from: endPos!, offset: -textField.text!.count)
            
            textField.selectedTextRange = textField.textRange(from: startPos!, to: endPos!)
            
        }
        

    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        
        return true
        
    }
    
    
    
}
