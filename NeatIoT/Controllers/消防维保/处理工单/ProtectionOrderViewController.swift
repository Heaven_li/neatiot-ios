//
//  ProtectionOrderViewController.swift
//  NeatIoT
//
//  Created by Yang Blus on 2020/5/8.
//  Copyright © 2020 Neat. All rights reserved.
//

import UIKit

class ProtectionOrderViewController: SupperTableViewViewController {
    
    let ProtectionOrderCellIdent = "ProtectionOrderCellIdent"
    
    var menuModel:MenuModel?
    
    /// 获取列表参数
    var currentPageIndex:Int = 1
    
    var dataSourceArr:Array<ProtectionOrder> = []

    override func viewDidLoad() {
        super.viewDidLoad()

        configerNav()
        configerUI()
        
        getOrderList()
        
    }
    
    func configerNav() {
        self.titleName = self.menuModel?.name ?? "维保工单"
    }
    
    func configerUI() {
        
        self.listContainView.delegate = self
        self.listContainView.dataSource = self
        self.listContainView.separatorStyle = .none
        self.listContainView.separatorInset = UIEdgeInsets.init(top: 0, left: 5, bottom: 0, right: 5)
        self.listContainView.register(UINib.init(nibName: "ProtectionOrderTableViewCell", bundle: Bundle.main), forCellReuseIdentifier: ProtectionOrderCellIdent)
        self.view.addSubview(self.listContainView)
        
        self.listContainView.snp.makeConstraints { (make) in
            make.top.equalTo(self.view)
            make.left.right.equalTo(self.view)
            make.bottom.equalTo(self.view)
        }
        
    }
    
    override func refreshAction() {
        currentPageIndex = 1
        
        NeatRequest.instance.getProtectionOrderList( pageIndex: 1, success: { (response) in
            
            self.dataSourceArr = response.result?.orderArr ?? []
            self.listContainView.reloadData()
            self.endRefresh(isUpdateFooter: true, dataCount: self.dataSourceArr.count, pageSize: 10)
            
        }) { (error) in
            
            NeatHud.showMessage(message: error.errMessage, view: self.view)
        }
    }
    
    override func loadMoreAction() {
        
        currentPageIndex+=1
        
        NeatRequest.instance.getProtectionOrderList( pageIndex: currentPageIndex, success: { (response) in
            
            let taskArr = response.result?.orderArr ?? []
                    
                if taskArr.count == 0{
                    self.currentPageIndex-=1
                }
            
                self.endLoadMoreData(dataCount:taskArr.count, pageSize: 10)
            
                self.dataSourceArr.append(contentsOf: taskArr)
                self.listContainView.reloadData()
            
        }) { (error) in
            
            self.currentPageIndex-=1
            self.endLoadMoreData(dataCount:0, pageSize: 10)
            NeatHud.showMessage(message: error.errMessage, view: self.view)
        }
    }
    
    func getOrderList() {
        
        self.listContainView.showLoadingView()
        
        NeatRequest.instance.getProtectionOrderList( pageIndex: 1, success: { (response) in
            
            self.dataSourceArr = response.result?.orderArr ?? []
            self.endLoadMoreData(dataCount: self.dataSourceArr.count, pageSize: 10)
            if self.dataSourceArr.count == 0{
                self.listContainView.showEmptyView()
            }else{
                self.listContainView.hidenHolderView()
                self.listContainView.reloadData()
            }
            
        }) { (error) in
            
            if error.errCode == RequestResponseCode.dataParsingException {
                //数据异常
                NeatHud.dissmissHud(view: self.view)
                self.listContainView.showRequestFaildView {
                    
                }
            }else if error.errCode == RequestResponseCode.netCanNotContent || error.errCode == RequestResponseCode.requestTimeOut{
                //无网络
                NeatHud.dissmissHud(view: self.view)
                self.listContainView.showNetErrorView(errorMessage: error.errMessage) {
                    self.getOrderList()
                }
            }else{
                NeatHud.showMessage(message: error.errMessage, view: self.view)
            }
            
        }
        
    }

}

extension ProtectionOrderViewController:UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 114
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.1
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView.init()
    }
}

extension ProtectionOrderViewController:UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: ProtectionOrderCellIdent, for: indexPath) as! ProtectionOrderTableViewCell
        cell.configerCell(model: dataSourceArr[indexPath.row])
        cell.selectionStyle = .none
        cell.delegate = self
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSourceArr.count
    }
    
}

extension ProtectionOrderViewController:ProtectionOrderTableViewCellDelegate{
    
    func didClickFunction(model: ProtectionOrder) {
        
        if model.orderStatusType == 2 {
            let vc = ProtectionHandleViewController.init()
            vc.delegate = self
            vc.orderModel = model
            self.navigationController?.pushViewController(vc, animated: true)
        }else if model.orderStatusType == 4{
            let vc = ProtectionDetailViewController.init()
            vc.orderModel = model
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
    }
  
}

extension ProtectionOrderViewController:ProtectionHandleViewControllerDelgate{
    
    func handleFinish() {
        self.getOrderList()
    }

}
