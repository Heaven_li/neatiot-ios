//
//  ProtectionOrderTableViewCell.swift
//  NeatIoT
//
//  Created by Yang Blus on 2020/5/9.
//  Copyright © 2020 Neat. All rights reserved.
//

import UIKit

protocol ProtectionOrderTableViewCellDelegate {
    func didClickFunction(model:ProtectionOrder)
}

class ProtectionOrderTableViewCell: UITableViewCell {
    
    
    @IBOutlet weak var iconImageView: UIImageView!
    
    @IBOutlet weak var orderNumLable: UILabel!
    
    @IBOutlet weak var entNameLable: UILabel!
    
    @IBOutlet weak var endDateLable: UILabel!
    
    @IBOutlet weak var personLable: UILabel!
    
    @IBOutlet weak var functionImageView: UIImageView!
    
    @IBOutlet weak var bgView: UIView!
    
    @IBOutlet weak var orderUpToTimeLable: UILabel!
    
    var currentModel:ProtectionOrder?
    
    var delegate:ProtectionOrderTableViewCellDelegate?
    
    var tap:UITapGestureRecognizer?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.bgView.layer.cornerRadius = 5
        self.bgView.clipsToBounds = true
        
        tap = UITapGestureRecognizer.init(target: self, action: #selector(tapfunctionImageView))
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configerCell(model:ProtectionOrder) {
        
        currentModel = model
    
        self.orderNumLable.text = model.orderNo
        
        self.entNameLable.text = "单位名称:" + model.entName!
        
        self.endDateLable.text = "截止日期:" + model.acceptOrderUptoTime!
        
        self.personLable.text = "发起人:" + model.initiatorName! + "(" + model.tel! + ")"
        
        if model.orderStatusType == 1 {
            //待受理
            self.functionImageView.image = UIImage.init(named: "")
        }else if model.orderStatusType == 2{
            //待处理
            self.functionImageView.image = UIImage.init(named: "unHandle")
            self.functionImageView.isUserInteractionEnabled = true
            self.functionImageView.addGestureRecognizer(self.tap!)
        }else if model.orderStatusType == 3{
            //待确认
            self.functionImageView.image = UIImage.init(named: "unConfirm")
        }else if model.orderStatusType == 4{
            //已完成
            self.functionImageView.image = UIImage.init(named: "order_finish")
            self.functionImageView.isUserInteractionEnabled = true
            self.functionImageView.addGestureRecognizer(self.tap!)
            self.orderUpToTimeLable.isHidden = false
            if !(model.realUptoTime?.isEmpty ?? true){
                
                self.orderUpToTimeLable.text = model.realUptoTime
                
            }else{
                self.orderUpToTimeLable.text = "----"
            }
            
        }else{
            //未知
            
        }
        
        
            
    }
 
    @objc func tapfunctionImageView() {
        self.delegate?.didClickFunction(model: currentModel!)
    }
    
}
