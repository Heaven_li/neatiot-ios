//
//  ProtectionMessageTableViewCell.swift
//  NeatIoT
//
//  Created by Yang Blus on 2020/5/9.
//  Copyright © 2020 Neat. All rights reserved.
//

import UIKit

class ProtectionMessageTableViewCell: UITableViewCell {
    
    
    @IBOutlet weak var iconImageView: UIImageView!
    
    @IBOutlet weak var titleLable: UILabel!
    
    @IBOutlet weak var timeLable: UILabel!
    
    @IBOutlet weak var inforLable: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configerCell(model:ProtectionMessage) {
        
        var titleStr:String = ""
        
        if model.dataTable == "mta_contract" {
            //合同提醒
            if model.flag == 2 {
                //快到期
                titleStr = "合同过期提醒";
            }else if model.flag == 3 {
                //已超期
                titleStr = "合同超期提醒";
            }
            
            //合同
            let numBeforStr = model.number!+"维保合同距离过期时间还有"
            
            let inforStr = numBeforStr + model.days! + "天，请相关单位做好续签准备"
            
            let attributiStr = NSMutableAttributedString.init(string: inforStr)
            
            attributiStr.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.blue, range: NSRange.init(location: 0, length: model.number!.count))
            attributiStr.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.red, range: NSRange.init(location: numBeforStr.count, length: model.days!.count))

            self.inforLable.attributedText = attributiStr
            
            self.iconImageView.image = UIImage.init(named: "mta_contract")
            
            
        }else if model.dataTable == "mta_plan"{
            //维保计划
            if model.flag == 2 {
                //快到期
                titleStr = "维保计划过期提醒";
            }else if model.flag == 3 {
                //已超期
                titleStr = "维保计划超期提醒";
            }
            
            let numBeforStr = model.number! + "维保计划距离过期时间还有";
            let inforStr = numBeforStr + model.days! + "天，请确保在截止时间前完成维保";
            
            let attributiStr = NSMutableAttributedString.init(string: inforStr)
            
            attributiStr.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.blue, range: NSRange.init(location: 0, length: model.number!.count))
            attributiStr.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.red, range: NSRange.init(location: numBeforStr.count, length: model.days!.count))
            
            self.inforLable.attributedText = attributiStr
            
            self.iconImageView.image = UIImage.init(named: "mta_plan")
            
        }
        
        
        self.titleLable.text = titleStr
        
        self.timeLable.text = model.sendTime
        
        
    }
    
}
