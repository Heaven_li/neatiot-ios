//
//  ProtectionMessageViewController.swift
//  NeatIoT
//
//  Created by Yang Blus on 2020/5/8.
//  Copyright © 2020 Neat. All rights reserved.
//

import UIKit

class ProtectionMessageViewController: SupperTableViewViewController {
    
    
    let ProtectionMessageCellIdent = "ProtectionMessageCellIdent"
    
    var menuModel:MenuModel?
    
    /// 获取列表参数
    var currentPageIndex:Int = 1
    
    var dataSourceArr:Array<ProtectionMessage> = []

    override func viewDidLoad() {
        super.viewDidLoad()

        configerNav()
        configerUI()
        
        getMessageList()
    }
    
    func configerNav() {
        self.titleName = self.menuModel?.name ?? "消息提醒"
    }
    
    func configerUI() {
        
        self.listContainView.delegate = self
        self.listContainView.dataSource = self
        self.listContainView.separatorStyle = .singleLine
        self.listContainView.separatorInset = UIEdgeInsets.init(top: 0, left: 5, bottom: 0, right: 5)
        self.listContainView.register(UINib.init(nibName: "ProtectionMessageTableViewCell", bundle: Bundle.main), forCellReuseIdentifier: ProtectionMessageCellIdent)
        self.view.addSubview(self.listContainView)
        
        self.listContainView.snp.makeConstraints { (make) in
            make.top.equalTo(self.view)
            make.left.right.equalTo(self.view)
            make.bottom.equalTo(self.view)
        }
        
    }
    
    override func refreshAction() {
        currentPageIndex = 1
        
        NeatRequest.instance.getProtectionMessageList(pageIndex: currentPageIndex, success: { (response) in
            
            self.dataSourceArr = response.result?.messageArr ?? []
            self.listContainView.reloadData()
            self.endRefresh(isUpdateFooter: true, dataCount: self.dataSourceArr.count, pageSize: 10)
            
        }) { (error) in
            
            NeatHud.showMessage(message: error.errMessage, view: self.view)
        }
    }
    
    override func loadMoreAction() {
        
        currentPageIndex+=1
        
        NeatRequest.instance.getProtectionMessageList(pageIndex: currentPageIndex, success: { (response) in
            
            let taskArr = response.result?.messageArr ?? []
                    
                if taskArr.count == 0{
                    self.currentPageIndex-=1
                }
            
                self.endLoadMoreData(dataCount:taskArr.count, pageSize: 10)
            
                self.dataSourceArr.append(contentsOf: taskArr)
                self.listContainView.reloadData()
            
        }) { (error) in
            
            self.currentPageIndex-=1
            self.endLoadMoreData(dataCount:0, pageSize: 10)
            NeatHud.showMessage(message: error.errMessage, view: self.view)
        }
        
    }
    
    func getMessageList() {
        
        self.listContainView.showLoadingView()
        
        NeatRequest.instance.getProtectionMessageList(pageIndex: currentPageIndex, success: { (response) in
            
            self.dataSourceArr = response.result?.messageArr ?? []
            self.endLoadMoreData(dataCount: self.dataSourceArr.count, pageSize: 10)
            if self.dataSourceArr.count == 0{
                self.listContainView.showEmptyView()
            }else{
                self.listContainView.hidenHolderView()
                self.listContainView.reloadData()
            }
            
        }) { (error) in
            
            if error.errCode == RequestResponseCode.dataParsingException {
                //数据异常
                NeatHud.dissmissHud(view: self.view)
                self.listContainView.showRequestFaildView {
                    
                }
            }else if error.errCode == RequestResponseCode.netCanNotContent || error.errCode == RequestResponseCode.requestTimeOut{
                //无网络
                NeatHud.dissmissHud(view: self.view)
                self.listContainView.showNetErrorView(errorMessage: error.errMessage) {
                    self.getMessageList()
                }
            }else{
                NeatHud.showMessage(message: error.errMessage, view: self.view)
            }
            
        }
        
    }

}

extension ProtectionMessageViewController:UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSourceArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    
        let cell = tableView.dequeueReusableCell(withIdentifier: ProtectionMessageCellIdent, for: indexPath) as! ProtectionMessageTableViewCell
        
        cell.selectionStyle = .none
        
        cell.configerCell(model: dataSourceArr[indexPath.row])
        
        return cell
    }
    
    
}

extension ProtectionMessageViewController:UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.01
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView.init()
    }
    
}
