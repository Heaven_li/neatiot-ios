//
//  ProtectionDetailViewController.swift
//  NeatIoT
//
//  Created by Yang Blus on 2020/5/18.
//  Copyright © 2020 Neat. All rights reserved.
//

import UIKit
import AVFoundation
class ProtectionDetailViewController: SupperViewController {
    
    
    /// 单行显示 或 输入控件
    var NormalItemHeight:CGFloat = 0
    /// 文本控件
    var FullTextEditHeight:CGFloat = 0
    /// 图片视频控件高度
    var MediaViewHeight:CGFloat = 0
    /// 耗材控件高度
    var MaterialsViewHeight:CGFloat = 0
    /// 音频控件高度
    var AudioViewHeight:CGFloat = 0
    
    var taskModel:ProtectionTaskDetail?
    
    var orderModel:ProtectionOrder?
    /// 展示数组
    var dataSourceArr:Array<ReportItemModel> = []
    /// 耗材数组
    var materialsArr:Array<MaterialsItemModel> = []
    
    /// 媒体数组
    var mAnnexArr:Array<AnnexModel> = []
    let anxueModel = ReportItemModel.init()
    
    /// 语音数组
    var mVoiceArr:Array<AnnexModel> = []
    let voiceModel = ReportItemModel.init()

    var currentAudioPath:URL?
    var lastRadioClickIndex:IndexPath?
    var audioPlayer:AVPlayer?
    var audioItem:AVPlayerItem?
    var lastVoiceItemCell:VoiceItemTableViewCell?
    
    /// 音频播放状态
    var isPalying:Bool = false
    
    
    lazy var containScrollView: UIScrollView = {
        
        let scrollView = UIScrollView.init()
        
        return scrollView
    }()
    
    lazy var containView: UIView = {
        let view = UIView.init()
        
        return view
    }()
    
    lazy var mediaView: MediaView = {
        let mediaView = MediaView.init()
//        mediaView.delegate = self
        return mediaView
    }()
    
    lazy var audioView: AudioView = {
        let audioView = AudioView.init()
//        audioView.delegate = self
        return audioView
    }()
    
    override func viewWillDisappear(_ animated: Bool) {
        
        if isPalying {

            lastVoiceItemCell?.voice_image_view.stopAnimating()
            self.audioPlayer?.pause();
            isPalying = false
            if audioItem != nil {
                audioItem!.removeObserver(self, forKeyPath: "status")
                audioItem = nil
            }
            
        }
        
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        if taskModel != nil {
            self.titleName = "任务详情"
            getTaskDetail()
        }else if orderModel != nil {
            self.titleName = "工单详情"
            getOrderDetail()
        }
        
        self.view.addSubview(self.containScrollView)
        
        self.containScrollView.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }
        
        self.containScrollView.addSubview(self.containView)
        self.containView.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
            make.width.equalTo(NormalConstant.ScreenWidth)
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(avPlayerItemDidPlayToEndTime), name: .AVPlayerItemDidPlayToEndTime, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(avPlayerItemPlayFailed), name: .AVPlayerItemFailedToPlayToEndTime, object: nil)
        
    }
    
    @objc func avPlayerItemDidPlayToEndTime(){
        lastVoiceItemCell?.voice_image_view.stopAnimating()
        isPalying = false
        if audioItem != nil {
            audioItem!.removeObserver(self, forKeyPath: "status")
            audioItem = nil
        }
        
    }
    
    @objc func avPlayerItemPlayFailed(){
        
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        
        if keyPath == "status" {
            
            let state = change![NSKeyValueChangeKey.kindKey] as! Int
            
            if state == AVPlayerItem.Status.readyToPlay.rawValue {
                debugPrint("readyToPlay")
                self.isPalying = true
            }else if state == AVPlayerItem.Status.failed.rawValue{
                debugPrint("failed")
                self.isPalying = false
            }else if state == AVPlayerItem.Status.unknown.rawValue{
                debugPrint("unknown")
                self.isPalying = false
            }
        }
        
    }
    /// 初始化控件高度
    func initCompHeight() {
        
        NormalItemHeight = 46
        
        FullTextEditHeight = 86
        
        //计算上传附件view高度
        /// 单个图片大小
        let itemWidth = (NormalConstant.ScreenWidth - 6*(4 - 1 + 2))/4
        
        if anxueModel.annexArr.count>=1 && anxueModel.annexArr.count <= 4 {
            MediaViewHeight = CGFloat(itemWidth + 6 + 36)
        }else{
            MediaViewHeight = CGFloat((itemWidth + 6)*2 + 36)
        }
        
        //计算耗材view高度
        if materialsArr.count != 0 {
            MaterialsViewHeight = CGFloat(42+26*(materialsArr.count + 1))
        }
        
        //计算音频view高度
        AudioViewHeight = CGFloat(48) * CGFloat(voiceModel.annexArr.count) + 36
        
        
    }
    
    func getTaskDetail() {
        
        NeatRequest.instance.getProtectionDetailById(taskId: taskModel?.taskId, success: { (response) in
            
            self.formateResponseToTaskData(model: response)
            
        }) { (error) in
            
            debugPrint(error)
            
        }
    }
    
    func getOrderDetail() {
        NeatRequest.instance.getProtectionOrderDetailById(acceptId: self.orderModel?.acceptId, success: { (response) in
            
            self.formateResponseToOrderData(model: response)
            
        }) { (error) in
        
            debugPrint(error)
            
        }
    }
    
    func formateResponseToOrderData(model:ProtectionOrderResponse) {
        
        //入场时间
        let enterTimeItem = ReportItemModel.init()
        enterTimeItem.itemStyle = .NormalStyle
        enterTimeItem.titleText = "入场时间"
        enterTimeItem.inforText = model.result?.orderDetail?.enterTime
        enterTimeItem.isShowSepLine = true
        
        //发起人
        let initiatorNameItem = ReportItemModel.init()
        initiatorNameItem.itemStyle = .NormalStyle
        initiatorNameItem.titleText = "发起人"
        initiatorNameItem.inforText = model.result?.orderDetail?.initiatorName
        initiatorNameItem.isShowSepLine = true
        
        //所属单位
        let entNameItem = ReportItemModel.init()
        entNameItem.itemStyle = .NormalStyle
        entNameItem.titleText = "所属单位"
        entNameItem.inforText = model.result?.orderDetail?.entName
        entNameItem.isShowSepLine = true
        
        //联系方式
        let telItem = ReportItemModel.init()
        telItem.itemStyle = .NormalStyle
        telItem.titleText = "联系方式"
        telItem.inforText = model.result?.orderDetail?.tel
        telItem.isShowSepLine = true
        
        //总工时
        let workHoursItem = ReportItemModel.init()
        workHoursItem.itemStyle = .NormalStyle
        workHoursItem.titleText = "总工时"
        workHoursItem.inforText = (model.result?.orderDetail?.workHours) ?? "-" + "小时"
        workHoursItem.isShowSepLine = true
        
        //工单描述
        let orderDesItem = ReportItemModel.init()
        orderDesItem.itemStyle = .UnTextViewStyle
        orderDesItem.isCanEdit = false
        orderDesItem.titleText = "工单描述"
        if model.result?.historyCount != 0 {
            orderDesItem.subTitleText = String(model.result?.historyCount ?? 0)
        }
        orderDesItem.inforText = model.result?.orderDetail?.orderDes
        orderDesItem.isShowSepLine = false
        
        //处理内容
        let handleTextItem = ReportItemModel.init()
        handleTextItem.itemStyle = .UnTextViewStyle
        handleTextItem.titleText = "处理内容"
        handleTextItem.isCanEdit = false
        handleTextItem.inforText = model.result?.orderDetail?.handleText
        handleTextItem.isShowSepLine = false
        
        //耗材
        for model in model.result?.materials ?? [] {
            
            let inforModel = MaterialsItemModel.init()
            inforModel.firstLableText = model.name
            inforModel.secondLableText = model.dosage
            inforModel.thirdLableText = model.actualCost
            inforModel.fourLableText = model.amountPaid
            
            materialsArr.append(inforModel)
            
        }
        
        let materialsItem = ReportItemModel.init()
        materialsItem.itemStyle = .MateriasStyle
        materialsItem.titleText = "耗材"
        materialsItem.materialsArr = materialsArr
        materialsItem.isShowSepLine = false
        
        //图片视频附件
        //语音附件
        self.mediaFormate(uploadFiles: (model.result?.uploadFiles)!)
        
        dataSourceArr.append(enterTimeItem)
        dataSourceArr.append(initiatorNameItem)
        dataSourceArr.append(entNameItem)
        dataSourceArr.append(telItem)
        dataSourceArr.append(workHoursItem)
        
        if materialsArr.count != 0 {
            dataSourceArr.append(materialsItem)
        }
        dataSourceArr.append(orderDesItem)
        dataSourceArr.append(handleTextItem)
        if anxueModel.annexArr.count != 0 {
            anxueModel.itemStyle = .MedialStyle
            dataSourceArr.append(anxueModel)
        }
        if voiceModel.annexArr.count != 0 {
            voiceModel.itemStyle = .VoiceStyle
            dataSourceArr.append(voiceModel)
        }
        
        configerSubUI()
        
    }
    
    func formateResponseToTaskData(model:ProtectionTaskResponse) {
        
        //维保时间
        let timeItem = ReportItemModel.init()
        timeItem.itemStyle = .NormalStyle
        timeItem.titleText = "维保时间"
        timeItem.inforText = model.result?.taskDetail?.maintenanceTime
        timeItem.isShowSepLine = true
        
        //维保人
        let taskUserNameItem = ReportItemModel.init()
        taskUserNameItem.itemStyle = .NormalStyle
        taskUserNameItem.titleText = "维保人"
        taskUserNameItem.inforText = model.result?.taskDetail?.taskUserName
        taskUserNameItem.isShowSepLine = true
        
        //GIS位置
        let addressItem = ReportItemModel.init()
        addressItem.itemStyle = .NormalStyle
        addressItem.titleText = "GIS位置"
        addressItem.inforText = model.result?.taskDetail?.address
        addressItem.isShowSepLine = true
        
        //总工时
        let workHoursItem = ReportItemModel.init()
        workHoursItem.itemStyle = .NormalStyle
        workHoursItem.titleText = "总工时"
        workHoursItem.inforText = String(model.result?.taskDetail?.workHours ?? "-") + "小时"
        workHoursItem.isShowSepLine = true
        
        //耗材
        for model in model.result?.materials ?? [] {
            
            let inforModel = MaterialsItemModel.init()
            inforModel.firstLableText = model.name
            inforModel.secondLableText = model.dosage
            inforModel.thirdLableText = model.actualCost
            inforModel.fourLableText = model.amountPaid
            
            materialsArr.append(inforModel)
            
        }
        
        let materialsItem = ReportItemModel.init()
        materialsItem.itemStyle = .MateriasStyle
        materialsItem.titleText = "耗材"
        materialsItem.materialsArr = materialsArr
        materialsItem.isShowSepLine = false
        
        
    
        
        //内容描述
        let desItem = ReportItemModel.init()
        desItem.itemStyle = .UnTextViewStyle
        desItem.titleText = "内容描述"
        desItem.isCanEdit = false
        desItem.inforText = model.result?.taskDetail?.taskDes
        desItem.isShowSepLine = false
        
        
        self.mediaFormate(uploadFiles: (model.result?.uploadFiles)!)
        
        
        dataSourceArr.append(timeItem)
        dataSourceArr.append(taskUserNameItem)
        dataSourceArr.append(addressItem)
        dataSourceArr.append(workHoursItem)
        if materialsArr.count != 0 {
            dataSourceArr.append(materialsItem)
        }
        dataSourceArr.append(desItem)
        
        if anxueModel.annexArr.count != 0 {
            anxueModel.itemStyle = .MedialStyle
            dataSourceArr.append(anxueModel)
        }
        
        configerSubUI()
        
        
        
    }
    
    func configerSubUI() {
        
        var upView:UIView = self.containView
        
        var currentView:UIView?
        
        /// 取出最新的控件高度
        self.initCompHeight()
        
        for (index, model) in dataSourceArr.enumerated() {
            
            if model.itemStyle == .NormalStyle {
                
                let staticView = StaticInforView.init()
                staticView.configerView(model: model)
                
                currentView = staticView
                
                self.containView.addSubview(currentView!)
                
                currentView?.snp.makeConstraints({ (make) in
                    if index == 0{
                        
                        make.top.equalTo(upView)
                        make.width.equalToSuperview()
                        make.leading.trailing.equalToSuperview()
                        make.height.equalTo(NormalItemHeight)
                        
                    }else if index == dataSourceArr.count - 1{
                        
                        make.top.equalTo(upView.snp.bottom)
                        make.width.equalToSuperview()
                        make.leading.trailing.equalToSuperview()
                        make.height.equalTo(NormalItemHeight)
                        make.bottom.equalTo(self.containView.snp.bottom)
                        
                    }else{
                        
                        make.top.equalTo(upView.snp.bottom)
                        make.width.equalToSuperview()
                        make.leading.trailing.equalToSuperview()
                        make.height.equalTo(NormalItemHeight)
                        
                    }
                    
                    
                })
                
            }else if model.itemStyle == .NormalInputStyle {
                
                let inputInforView = InputInforView.init()
                inputInforView.configerView(model: model)
                
                currentView = inputInforView
                
                self.containView.addSubview(currentView!)
                
                currentView?.snp.makeConstraints({ (make) in
                    if index == 0{
                        
                        make.top.equalTo(upView)
                        make.width.equalToSuperview()
                        make.leading.trailing.equalToSuperview()
                        make.height.equalTo(NormalItemHeight)
                        
                    }else if index == dataSourceArr.count - 1{
                        
                        make.top.equalTo(upView.snp.bottom)
                        make.width.equalToSuperview()
                        make.leading.trailing.equalToSuperview()
                        make.height.equalTo(NormalItemHeight)
                        make.bottom.equalTo(self.containView.snp.bottom)
                        
                    }else{
                        
                        make.top.equalTo(upView.snp.bottom)
                        make.width.equalToSuperview()
                        make.leading.trailing.equalToSuperview()
                        make.height.equalTo(NormalItemHeight)
                        
                    }
                    
                    
                })
                
            }else if model.itemStyle == .UnTextViewStyle || model.itemStyle == .TextViewSubTitleStyle{
                
                let editTextView = FullTextEditView.init()
                editTextView.configerView(model: model)
                editTextView.delegate = self
                currentView = editTextView
                
                self.containView.addSubview(currentView!)
                
                currentView?.snp.makeConstraints({ (make) in
                    if index == 0{
                        
                        make.top.equalTo(upView)
                        make.width.equalToSuperview()
                        make.leading.trailing.equalToSuperview()
                        make.height.equalTo(FullTextEditHeight)
                        
                    }else if index == dataSourceArr.count - 1{
                        
                        make.top.equalTo(upView.snp.bottom)
                        make.width.equalToSuperview()
                        make.leading.trailing.equalToSuperview()
                        make.height.equalTo(FullTextEditHeight)
                        make.bottom.equalTo(self.containView.snp.bottom)
                        
                    }else{
                        
                        make.top.equalTo(upView.snp.bottom)
                        make.width.equalToSuperview()
                        make.leading.trailing.equalToSuperview()
                        make.height.equalTo(FullTextEditHeight)
                        
                    }
                    
                })
                
            }else if model.itemStyle == .FunctionStyle {
                
                /// 添加耗材
                let functionView = FunctionView.init()
                functionView.configerView(model: model)
                
                currentView = functionView
                
                self.containView.addSubview(currentView!)
                
                currentView?.snp.makeConstraints({ (make) in
                    if index == 0{
                        
                        make.top.equalTo(upView)
                        make.width.equalToSuperview()
                        make.leading.trailing.equalToSuperview()
                        make.height.equalTo(NormalItemHeight)
                        
                    }else if index == dataSourceArr.count - 1{
                        
                        make.top.equalTo(upView.snp.bottom)
                        make.width.equalToSuperview()
                        make.leading.trailing.equalToSuperview()
                        make.height.equalTo(NormalItemHeight)
                        make.bottom.equalTo(self.containView.snp.bottom)
                        
                    }else{
                        
                        make.top.equalTo(upView.snp.bottom)
                        make.width.equalToSuperview()
                        make.leading.trailing.equalToSuperview()
                        make.height.equalTo(NormalItemHeight)
                        
                    }
                    
                })
                
            }else if model.itemStyle == .MateriasStyle {
                
                let materialseView = MaterialsView.init()
                materialseView.configerCell(model: model)
                
                currentView = materialseView
                
                self.containView.addSubview(currentView!)
                
                currentView?.snp.makeConstraints({ (make) in
                    if index == 0{
                        
                        make.top.equalTo(upView)
                        make.width.equalToSuperview()
                        make.leading.trailing.equalToSuperview()
                        make.height.equalTo(MaterialsViewHeight)
                        
                    }else if index == dataSourceArr.count - 1{
                        
                        make.top.equalTo(upView.snp.bottom)
                        make.width.equalToSuperview()
                        make.leading.trailing.equalToSuperview()
                        make.height.equalTo(MaterialsViewHeight)
                        make.bottom.equalTo(self.containView.snp.bottom)
                        
                    }else{
                        
                        make.top.equalTo(upView.snp.bottom)
                        make.width.equalToSuperview()
                        make.leading.trailing.equalToSuperview()
                        make.height.equalTo(MaterialsViewHeight)
                        
                    }
                    
                    
                })
                
            }else if model.itemStyle == .MedialStyle {
                
                /// 附件
                mediaView.configerView(model: model)
                mediaView.delegate = self
                currentView = mediaView
                
                self.containView.addSubview(currentView!)
                
                currentView?.snp.makeConstraints({ (make) in
                    if index == 0{
                        
                        make.top.equalTo(upView)
                        make.width.equalToSuperview()
                        make.leading.trailing.equalToSuperview()
                        make.height.equalTo(MediaViewHeight)
                        
                    }else if index == dataSourceArr.count - 1{
                        
                        make.top.equalTo(upView.snp.bottom)
                        make.width.equalToSuperview()
                        make.leading.trailing.equalToSuperview()
                        make.height.equalTo(MediaViewHeight)
                        make.bottom.equalTo(self.containView.snp.bottom)
                        
                    }else{
                        
                        make.top.equalTo(upView.snp.bottom)
                        make.width.equalToSuperview()
                        make.leading.trailing.equalToSuperview()
                        make.height.equalTo(MediaViewHeight)
                        
                    }
                    
                })
                
            }else if model.itemStyle == .VoiceStyle {
                
                audioView.configerView(model: voiceModel)
                audioView.delegate = self
                currentView = audioView
                
                self.containView.addSubview(currentView!)
                
                currentView?.snp.makeConstraints({ (make) in
                    if index == 0{
                        
                        make.top.equalTo(upView)
                        make.width.equalToSuperview()
                        make.leading.trailing.equalToSuperview()
                        make.height.equalTo(AudioViewHeight)
                        
                    }else if index == dataSourceArr.count - 1{
                        
                        make.top.equalTo(upView.snp.bottom)
                        make.width.equalToSuperview()
                        make.leading.trailing.equalToSuperview()
                        make.height.equalTo(AudioViewHeight)
                        make.bottom.equalTo(self.containView.snp.bottom)
                        
                    }else{
                        
                        make.top.equalTo(upView.snp.bottom)
                        make.width.equalToSuperview()
                        make.leading.trailing.equalToSuperview()
                        make.height.equalTo(AudioViewHeight)
                        
                    }
                    
                })
                
            }
            
            upView = currentView!
            
        }
        
    }
    
    func mediaFormate(uploadFiles:Array<UploadFile>) {
        
        for model in uploadFiles {
            
            let annexFileModel = AnnexModel.init()
            
            if model.fileType == 1 {
                //png
                annexFileModel.annexType = 1
                annexFileModel.isAddNewItem = false
                annexFileModel.isNetSource = true
                if model.fileUrl != nil {
                    annexFileModel.sourceFile = URL.init(string: model.fileUrl!)
                }
                if model.fileThumbnailUrl != nil{
                    annexFileModel.thumbnail = URL.init(string: model.fileThumbnailUrl!)
                }
                mAnnexArr.append(annexFileModel)
                
                
            }else if model.fileType == 2 {
                //mp3
                annexFileModel.annexType = 3
                annexFileModel.isAddNewItem = false
                annexFileModel.isCanEdit = false
                annexFileModel.isNetSource = true
                if model.fileUrl != nil {
                    annexFileModel.sourceFile = URL.init(string: model.fileUrl!)
                }
                if model.fileTimeSpan != nil {
                    annexFileModel.voiceSecond = Int(model.fileTimeSpan!)
                }
                
                mVoiceArr.append(annexFileModel)
                
            }else if model.fileType == 3 {
                //mp4
                annexFileModel.annexType = 2
                annexFileModel.isAddNewItem = false
                annexFileModel.isNetSource = true
                annexFileModel.isCanEdit = false
                if model.fileUrl != nil {
                    annexFileModel.sourceFile = URL.init(string: model.fileUrl!)
                }
                if model.fileThumbnailUrl != nil{
                    annexFileModel.thumbnail = URL.init(string: model.fileThumbnailUrl!)
                }
                mAnnexArr.append(annexFileModel)
            }
            
            
        }
        anxueModel.titleText = "附件"
        anxueModel.itemStyle = .MedialStyle
        anxueModel.annexArr = mAnnexArr
        
        voiceModel.titleText = "音频附件"
        voiceModel.itemStyle = .VoiceStyle
        voiceModel.annexArr = mVoiceArr
        
    }
    
    func playRecVoice(model: AnnexModel) {
       
       debugPrint("播放录音")
       audioItem = AVPlayerItem.init(url: model.sourceFile!)
       audioItem!.addObserver(self, forKeyPath: "status", options: .new, context: nil)
       audioPlayer = AVPlayer.init(playerItem: audioItem)
       audioPlayer?.play()
       
    }
    

}

extension ProtectionDetailViewController:MediaViewDelegate{
    
    func didSelectItem(model: AnnexModel) {
        //点击
        //查看图片 或 视频
        let detailVC = MediaPlayViewController.init()
        detailVC.annexModel = model
        self.navigationController?.pushViewController(detailVC, animated: true)
    }
    
    func didLongSelectItem(model: AnnexModel, indexPath: IndexPath) {
        
    }
 
}

extension ProtectionDetailViewController:AudioViewDelegate{
    
    func startRecVoice() {
        
    }
    
    func endRecVoice() {
        
    }
    
    func clickRecVoice(model: AnnexModel, index: IndexPath, cell: VoiceItemTableViewCell) {
        
        if lastRadioClickIndex != nil {
            
            //有记录之前点击过
            if index == lastRadioClickIndex {
                //两次点击相同音频
                if isPalying {
                    self.audioPlayer?.pause()
                    cell.voice_image_view.stopAnimating()
                    isPalying = false
                }else{
                    self.playRecVoice(model: model)
                    cell.voice_image_view.startAnimating()
                }
            }else{
                //两次不同
                if isPalying {
                    self.audioPlayer?.pause()
                    self.lastVoiceItemCell?.voice_image_view.stopAnimating()
                    self.playRecVoice(model: model)
                    cell.voice_image_view.startAnimating()
                    
                }else{
                    self.playRecVoice(model: model)
                    cell.voice_image_view.startAnimating()
                }
            }
            
            lastVoiceItemCell = cell
            lastRadioClickIndex = index
        }else{
            //首次点击
            lastRadioClickIndex = index
            lastVoiceItemCell = cell
            self.playRecVoice(model: model)
            cell.voice_image_view.startAnimating()
        }
        
    }
    
    func deleteRecVoice(model: AnnexModel, index: IndexPath) {
        
    }
    
}

extension ProtectionDetailViewController:FullTextEditViewDelegate{
    
    func didInputInfor(text: String) {
        
    }
    
    func didClickHistoryRec() {
        
        let vc = ProtectionHistoryRecViewController.init()
        if orderModel != nil {
            vc.orderId = orderModel?.orderId
        }
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    
}
