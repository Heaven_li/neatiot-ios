//
//  ProtectionHistoryRecItemTableViewCell.swift
//  NeatIoT
//
//  Created by Yang Blus on 2020/5/19.
//  Copyright © 2020 Neat. All rights reserved.
//

import UIKit

class ProtectionHistoryRecItemTableViewCell: UITableViewCell {
    
    
    @IBOutlet weak var hanlePersonLable: UILabel!
    
    @IBOutlet weak var handleTimeLable: UILabel!
    
    @IBOutlet weak var desLable: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configerCell(model:ProtectionOrderHistoryItem) {
        
        self.hanlePersonLable.text = "处理人:" + model.handlerUserName!
        
        self.handleTimeLable.text = "处理时间:" + model.handlerOrderTime! 
        
        self.desLable.text = model.handlerText ?? ""
        
    }
}
