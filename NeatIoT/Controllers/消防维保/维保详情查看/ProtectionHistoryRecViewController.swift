//
//  ProtectionHistoryRecViewController.swift
//  NeatIoT
//
//  Created by Yang Blus on 2020/5/19.
//  Copyright © 2020 Neat. All rights reserved.
//

import UIKit

class ProtectionHistoryRecViewController: SupperTableViewViewController {
    
    let cellIdent = "ProtectionHistoryRecItemTableViewCell"
    var orderId:String?
    var dataSource:Array<ProtectionOrderHistoryItem> = []

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.titleName = "历史记录"

        self.listContainView.delegate = self
        self.listContainView.dataSource = self
        self.listContainView.separatorStyle = .singleLine
        self.listContainView.separatorInset = UIEdgeInsets.init(top: 0, left: 5, bottom: 0, right: 5)
        self.listContainView.register(UINib.init(nibName: "ProtectionHistoryRecItemTableViewCell", bundle: Bundle.main), forCellReuseIdentifier: self.cellIdent)
        self.listContainView.mj_footer!.isHidden = true
        self.listContainView.mj_header!.isHidden = true
        self.view.addSubview(self.listContainView)
        self.listContainView.snp.makeConstraints { (make) in
            make.top.equalTo(self.view)
            make.left.right.equalTo(self.view)
            make.bottom.equalTo(self.view)
        }
        
        getRec()
        
    }
    
    
    func getRec() {
        
        self.listContainView.showLoadingView()
        NeatRequest.instance.getProtectionOrderHistoryRecById(orderId: self.orderId!, success: { (response) in
            
            self.configerDataSource(model: response)
            
        }) { (errorRes) in
            
            if errorRes.errCode == RequestResponseCode.dataParsingException {
                //数据异常
                NeatHud.dissmissHud(view: self.view)
                self.listContainView.showRequestFaildView {
                    
                }
            }else if errorRes.errCode == RequestResponseCode.netCanNotContent || errorRes.errCode == RequestResponseCode.requestTimeOut{
                //无网络
                NeatHud.dissmissHud(view: self.view)
                self.listContainView.showNetErrorView(errorMessage: errorRes.errMessage) {
                    self.getRec()
                }
            }else{
                NeatHud.showMessage(message: errorRes.errMessage, view: self.view)
            }
            
        }
    }
    
    func configerDataSource(model:ProtectionOrderHistoryRecResponse) {
        
        self.dataSource.removeAll()
        self.dataSource.append(contentsOf: (model.result?.data)!)
        
        if self.dataSource.count == 0{
            self.listContainView.showEmptyView()
        }else{
            self.listContainView.hidenHolderView()
            self.listContainView.reloadData()
        }
        
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension ProtectionHistoryRecViewController:UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.01
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView.init()
    }
    
}
extension ProtectionHistoryRecViewController:UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.dataSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: self.cellIdent, for: indexPath) as! ProtectionHistoryRecItemTableViewCell
        
        cell.configerCell(model: self.dataSource[indexPath.row])
        
        return cell
        
    }
    
    
    
}
