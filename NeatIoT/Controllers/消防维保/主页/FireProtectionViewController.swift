//
//  FireProtectionViewController.swift
//  NeatIoT
//
//  Created by Yang Blus on 2020/5/6.
//  Copyright © 2020 Neat. All rights reserved.
//

import UIKit

class FireProtectionViewController: SupperViewController {
    
    let CollectionViewCellIdent = "FunctionItemCollectionViewCell"
    let DashboardViewCellIdent = "DasboardCollectionViewCell"
    
    ///维保任务
    let maintenance_plan = "maintenance_plan";
    ///维保计划
    let maintenance_history = "maintenance_history";
    ///处理工单
    let maintenance_order = "maintenance_order";
    ///临时维保
    let temporary_maintenance = "temporary_maintenance";
    ///消息提醒
    let patrol_msg_alert = "patrol_msg_alert";
    
    var menuModel:MenuModel?
    
    var dataSourceArr:Array<MenuModel> = []
    
    let dashboardModel = DasboardCollectionModel.init()
    
    lazy var containCollectionView: UICollectionView = {
        
        let layout = UICollectionViewFlowLayout.init()
        layout.scrollDirection = .vertical
        layout.minimumLineSpacing = 1
        layout.minimumInteritemSpacing = 1
        
        let collectionView = UICollectionView.init(frame: self.view.frame, collectionViewLayout: layout)
        collectionView.delegate = self
        collectionView.dataSource = self
        
        collectionView.backgroundColor = NormalColor.viewControllerDefaultBgColor
        collectionView.register(UINib.init(nibName: "FunctionItemCollectionViewCell", bundle: Bundle.main), forCellWithReuseIdentifier: CollectionViewCellIdent)
        collectionView.register(UINib.init(nibName: "DasboardCollectionViewCell", bundle: Bundle.main), forCellWithReuseIdentifier: DashboardViewCellIdent)
        
        return collectionView
        
    }()
    
    override func viewWillAppear(_ animated: Bool) {
         self.navigationController?.navigationBar.setBackgroundImage(UIImage.init(named: "inspection_nav_bg"), for: .default)
         self.navigationController?.navigationBar.shadowImage = UIImage.init()
        
        getMtaStatistics()

    }
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.navigationBar.setBackgroundImage(UIImage.init(named: "nav_bar_bg"), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage.init()
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        initUI()
        
        
    }
    
    
    func initUI() {
        
        self.titleName = "消防维保"
        
        self.view.addSubview(self.containCollectionView)
        
        dataSourceArr.append(contentsOf: menuModel?.children ?? [])
        
        self.containCollectionView.reloadData()
        
        
        
    }
    
    func getMtaStatistics() {
        
        NeatRequest.instance.getMtaStatistics(success: { (response) in
            
            self.formateRate(response: response)
            self.updateUnReadDot(response: response)
            
            
        }) { (error) in
            
            NeatHud.showMessage(message: error.errMessage, view: self.view)
            
        }
        
    }
    
    func formateRate(response:ProtectionHomeStatistics) {
        
        dashboardModel.leftTitel = "已完成计划数"
        dashboardModel.leftInfor = Float(response.result?.taskCompleteCount ?? 0)
        dashboardModel.midTitel = "已完率"
        if response.result!.taskCompleteRate ?? 0 > Float(0) {
            dashboardModel.midInfor = (response.result?.taskCompleteRate)!/Float(100)
        }else{
            dashboardModel.midInfor = Float(0)
        }
        
        dashboardModel.rightTitel = "处理工单数"
        dashboardModel.rightInfor = Float(response.result?.handleOrderCount ?? 0)
        
        
    }
    
    func updateUnReadDot(response:ProtectionHomeStatistics) {
        
        for model in dataSourceArr{
            
            if model.style_id == "maintenance_plan" {
                model.notifyNum = response.result?.taskNotCompleteCount ?? 0
            }else if model.style_id == "maintenance_order"{
                model.notifyNum = response.result?.notHandleOrderCount ?? 0
            }
        }
        
        self.containCollectionView.reloadData()
    }
    
    
    
}

extension FireProtectionViewController:UICollectionViewDelegate{
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 2
    }
    

    
}
extension FireProtectionViewController:UICollectionViewDataSource{
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        
        
        if indexPath.section == 0 {
                
                let item = collectionView.dequeueReusableCell(withReuseIdentifier: DashboardViewCellIdent, for: indexPath) as! DasboardCollectionViewCell
                
                item.configerCell(dashboardModel: self.dashboardModel)
                
                return item
                
            }else{
                
                let item = collectionView.dequeueReusableCell(withReuseIdentifier: CollectionViewCellIdent, for: indexPath) as! FunctionItemCollectionViewCell
            
                item.configerItem(menuModel: dataSourceArr[indexPath.item])
                
                return item
                
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if section == 0 {
            return 1
        }else{
            return dataSourceArr.count
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if indexPath.section != 0 {
            let menuModel = dataSourceArr[indexPath.item]
                    
            if (menuModel.style_id == maintenance_plan){
                //维保任务

                let vc = ProtectionTaskViewController.init()
                vc.menuModel = menuModel
                self.navigationController?.pushViewController(vc, animated: true)


            }else if (menuModel.style_id == maintenance_history){
                //维保计划

                let vc = ProtectionPlanViewController.init()
                vc.menuModel = menuModel
                self.navigationController?.pushViewController(vc, animated: true)

            }else if (menuModel.style_id == maintenance_order){
                //处理工单

                let vc = ProtectionOrderViewController.init()
                vc.menuModel = menuModel
                self.navigationController?.pushViewController(vc, animated: true)

            }else if (menuModel.style_id == temporary_maintenance){
                //临时维保
                NeatHud.showMessage(message: "该功能正在开发中", view: self.view)

            }else if (menuModel.style_id == patrol_msg_alert){
                //消息提醒

                let vc = ProtectionMessageViewController.init()
                vc.menuModel = menuModel
                self.navigationController?.pushViewController(vc, animated: true)

            }
        }
        
    }
    
}
extension FireProtectionViewController:UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        var itemSize = CGSize.zero
        if indexPath.section == 0 {
            itemSize = CGSize.init(width:NormalConstant.ScreenWidth, height: NormalConstant.ScreenWidth*0.46)
        }else{
            itemSize = CGSize.init(width:(NormalConstant.ScreenWidth-2)/3.0, height: (NormalConstant.ScreenWidth-2)/3.0)
        }
        return itemSize
    }
    
}
