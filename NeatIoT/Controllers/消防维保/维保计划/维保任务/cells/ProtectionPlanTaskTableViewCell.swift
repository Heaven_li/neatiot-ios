//
//  ProtectionPlanTaskTableViewCell.swift
//  NeatIoT
//
//  Created by Yang Blus on 2020/5/9.
//  Copyright © 2020 Neat. All rights reserved.
//

import UIKit

protocol ProtectionPlanTaskTableViewCellDelegate {
    func didClickFinishStateLable(model:ProtectionTaskDetail)
}

class ProtectionPlanTaskTableViewCell: UITableViewCell {
    
    
    var delegate:ProtectionPlanTaskTableViewCellDelegate?
    
    @IBOutlet weak var taskStateLable: UILabel!
    
    @IBOutlet weak var taskNameLable: UILabel!
    
    @IBOutlet weak var taskFinishState: UILabel!
    
    @IBOutlet weak var personNameLable: UILabel!
    
    @IBOutlet weak var addressLable: UILabel!
    @IBOutlet weak var bgView: UIView!
    
    var currentModel:ProtectionTaskDetail?
    
    var tap:UITapGestureRecognizer?
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.backgroundColor = .white
        
        self.bgView.layer.cornerRadius = 3
        
        tap = UITapGestureRecognizer.init(target: self, action: #selector(tapFinishState))
        
        self.taskFinishState.isUserInteractionEnabled = true
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configerCell(model:ProtectionTaskDetail) {
        
        currentModel = model
        
        self.taskNameLable.text = model.flagTime
        
        self.taskStateLable.layer.cornerRadius = 3
        self.taskStateLable.clipsToBounds = true
        
        if model.overTimeStatus == 1 {
            //已超时 异常
            self.taskStateLable.text = "异常"
            self.taskStateLable.backgroundColor = #colorLiteral(red: 0.9645915627, green: 0.5095934272, blue: 0.4940578938, alpha: 1)
        }else if model.overTimeStatus == 2 {
            //未超时 正常
            self.taskStateLable.text = "正常"
            self.taskStateLable.backgroundColor = #colorLiteral(red: 0.2779615819, green: 0.8432173729, blue: 0.3846079707, alpha: 1)
        }
        
        if model.taskStatus == 1{
            //已完成
            self.taskFinishState.text = "已完成"
            self.taskFinishState.textColor = #colorLiteral(red: 0.2779615819, green: 0.8432173729, blue: 0.3846079707, alpha: 1)
            self.taskFinishState.addGestureRecognizer(tap!)
        }else if model.taskStatus == 2{
            //未完成
            self.taskFinishState.text = "未完成"
            self.taskFinishState.textColor = #colorLiteral(red: 0.9645915627, green: 0.5095934272, blue: 0.4940578938, alpha: 1)
        }
        
        self.personNameLable.text = "维保人:"+model.maintenancePersonName!
        
        self.addressLable.text = "位置信息:"+model.address!
        
        
    }
    
    @objc func tapFinishState() {
        
        self.delegate?.didClickFinishStateLable(model: currentModel!)
        
    }
    
}
