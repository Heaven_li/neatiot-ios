//
//  ProtectionPlanTaskViewController.swift
//  NeatIoT
//
//  Created by Yang Blus on 2020/5/9.
//  Copyright © 2020 Neat. All rights reserved.
//

import UIKit

class ProtectionPlanTaskViewController: SupperTableViewViewController {
    
    let ProtectionPlanTaskCellIdent = "ProtectionPlanTaskCellIdent"
    
    var planId:String? = ""
    
    /// 获取列表参数
    var currentPageIndex:Int = 1
    
    var dataSourceArr:Array<ProtectionTaskDetail> = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        configerNav()
        configerUI()
        
        getTaskList()
    }
    
    func configerNav() {
        self.titleName = "维保任务"
    }
    
    func configerUI() {
        
        self.listContainView.delegate = self
        self.listContainView.dataSource = self
        self.listContainView.separatorStyle = .none
        self.listContainView.separatorInset = UIEdgeInsets.init(top: 0, left: 5, bottom: 0, right: 5)
        self.listContainView.register(UINib.init(nibName: "ProtectionPlanTaskTableViewCell", bundle: Bundle.main), forCellReuseIdentifier: ProtectionPlanTaskCellIdent)
        self.view.addSubview(self.listContainView)
        
        self.listContainView.snp.makeConstraints { (make) in
            make.top.equalTo(self.view)
            make.left.right.equalTo(self.view)
            make.bottom.equalTo(self.view)
        }
        
        
    }
    
    override func refreshAction() {
        currentPageIndex = 1
        
        NeatRequest.instance.getProtectionTaskListByPlanId(planId: planId!, pageIndex: currentPageIndex, success: { (response) in
            
            self.dataSourceArr = response.result?.taskArr ?? []
            self.listContainView.reloadData()
            self.endRefresh(isUpdateFooter: true, dataCount: self.dataSourceArr.count, pageSize: 10)
            
        }) { (error) in
            
            NeatHud.showMessage(message: error.errMessage, view: self.view)
        }
    }
    
    override func loadMoreAction() {
        currentPageIndex+=1
        
        NeatRequest.instance.getProtectionTaskListByPlanId(planId: planId!, pageIndex: currentPageIndex, success: { (response) in
            
            let taskArr = response.result?.taskArr ?? []
                    
                if taskArr.count == 0{
                    self.currentPageIndex-=1
                }
            
                self.endLoadMoreData(dataCount:taskArr.count, pageSize: 10)
            
                self.dataSourceArr.append(contentsOf: taskArr)
                self.listContainView.reloadData()
            
        }) { (error) in
            
            self.currentPageIndex-=1
            self.endLoadMoreData(dataCount:0, pageSize: 10)
            NeatHud.showMessage(message: error.errMessage, view: self.view)
        }
        
    }
    
    func getTaskList() {
        
        self.listContainView.showLoadingView()
        
        NeatRequest.instance.getProtectionTaskListByPlanId(planId: planId!, pageIndex: currentPageIndex, success: { (response) in
            
            self.dataSourceArr = response.result?.taskArr ?? []
            self.endLoadMoreData(dataCount: self.dataSourceArr.count, pageSize: 10)
            if self.dataSourceArr.count == 0{
                self.listContainView.showEmptyView()
            }else{
                self.listContainView.hidenHolderView()
                self.listContainView.reloadData()
            }
            
        }) { (error) in
            
            if error.errCode == RequestResponseCode.dataParsingException {
                //数据异常
                NeatHud.dissmissHud(view: self.view)
                self.listContainView.showRequestFaildView {
                    
                }
            }else if error.errCode == RequestResponseCode.netCanNotContent || error.errCode == RequestResponseCode.requestTimeOut{
                //无网络
                NeatHud.dissmissHud(view: self.view)
                self.listContainView.showNetErrorView(errorMessage: error.errMessage) {
                    self.getTaskList()
                }
            }else{
                NeatHud.showMessage(message: error.errMessage, view: self.view)
            }
            
            
        }
    }
    

    

}

extension ProtectionPlanTaskViewController:UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 122
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.01
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView.init()
    }
    
    
    
}
extension ProtectionPlanTaskViewController:UITableViewDataSource{
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSourceArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: self.ProtectionPlanTaskCellIdent, for: indexPath) as! ProtectionPlanTaskTableViewCell
        cell.delegate = self
        cell.selectionStyle = .none
        cell.configerCell(model: dataSourceArr[indexPath.row])
        
        
        return cell
    }
    
    
    
}

extension ProtectionPlanTaskViewController:ProtectionPlanTaskTableViewCellDelegate{
    
    func didClickFinishStateLable(model: ProtectionTaskDetail) {
        
        let vc = ProtectionDetailViewController.init()
        vc.taskModel = model
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    
    
    
}
