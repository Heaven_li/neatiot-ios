//
//  ProtectionPlanTableViewCell.swift
//  NeatIoT
//
//  Created by Yang Blus on 2020/5/9.
//  Copyright © 2020 Neat. All rights reserved.
//

import UIKit

class ProtectionPlanTableViewCell: UITableViewCell {
    
    
    @IBOutlet weak var planNameLable: UILabel!
    
    @IBOutlet weak var planFinishTimeLable: UILabel!
    
    @IBOutlet weak var planStateLable: UILabel!
    
    @IBOutlet weak var stateBgView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        stateBgView.layer.cornerRadius = 5
        stateBgView.clipsToBounds = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configerCell(model:ProtectionPlan) {
        
        self.planNameLable.text = model.planName
        
        var planState = "未知"
        var planStateLableColor = #colorLiteral(red: 0.2779615819, green: 0.8432173729, blue: 0.3846079707, alpha: 1)
        if model.planStatus == 1 {
            planState = "正常"
        }else if model.planStatus == 2 {
            planState = "异常"
            planStateLableColor = #colorLiteral(red: 0.9645915627, green: 0.5095934272, blue: 0.4940578938, alpha: 1)
        }
        self.planStateLable.text = planState
        self.stateBgView.backgroundColor = planStateLableColor
        
        self.planStateLable.layer.cornerRadius = 2
        self.planStateLable.clipsToBounds = true
        
        if model.trueTaskEtime?.isEmpty ?? true {
            self.planFinishTimeLable.text = "最近完成时间: -"
        }else{
            self.planFinishTimeLable.text = "最近完成时间:"+model.trueTaskEtime!
        }
        
        
        
        
    }
    
}
