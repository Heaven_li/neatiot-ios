//
//  ProtectionPlanViewController.swift
//  NeatIoT
//
//  Created by Yang Blus on 2020/5/8.
//  Copyright © 2020 Neat. All rights reserved.
//

import UIKit

class ProtectionPlanViewController: SupperTableViewViewController {
    
    let ProtectionPlanCellIdent = "ProtectionPlanCellIdent"
    
    var menuModel:MenuModel?
    
    /// 获取列表参数
    var currentPageIndex:Int = 1
    var currentKeyword:String = ""
    
    var dataSourceArr:Array<ProtectionPlan> = []
    
    lazy var topSearchView: TopSearchView = {
        let topSearchView = TopSearchView.init()
        topSearchView.delegate = self
        return topSearchView
    }()

    override func viewDidLoad() {
        super.viewDidLoad()

        self.configerNavBar()
        
        self.configerUI()
        
        self.getPlanList()
        
    }
    
    func configerNavBar(){
        
        self.titleName = self.menuModel?.name ?? "维保任务"

    }
    
    func configerUI() {
        
        self.view.addSubview(self.topSearchView)
        
        self.listContainView.delegate = self
        self.listContainView.dataSource = self
        self.listContainView.separatorStyle = .singleLine
        self.listContainView.separatorInset = UIEdgeInsets.init(top: 0, left: 5, bottom: 0, right: 5)
        self.listContainView.register(UINib.init(nibName: "ProtectionPlanTableViewCell", bundle: Bundle.main), forCellReuseIdentifier: ProtectionPlanCellIdent)
        self.view.addSubview(self.listContainView)
        
        self.topSearchView.snp.makeConstraints { (make) in
            make.top.left.right.equalTo(self.view)
            make.height.equalTo(NormalConstant.TopSearchViewHeight)
        }
        
        self.listContainView.snp.makeConstraints { (make) in
            make.top.equalTo(self.topSearchView.snp.bottom)
            make.left.right.equalTo(self.view)
            make.bottom.equalTo(self.view)
        }
        
    }
    
    override func refreshAction() {
        
        currentPageIndex = 1
        
        NeatRequest.instance.getProtectionPlanList(planName: currentKeyword, pageIndex: currentPageIndex, success: { (response) in
                
                self.dataSourceArr = response.result?.planArray ?? []
                self.listContainView.reloadData()
                self.endRefresh(isUpdateFooter: true, dataCount: self.dataSourceArr.count, pageSize: 10)
                
        }) { (error) in
            
            NeatHud.showMessage(message: error.errMessage, view: self.view)
    
        }
        
    }
    override func loadMoreAction() {
        
        currentPageIndex+=1
        
        NeatRequest.instance.getProtectionPlanList(planName: currentKeyword, pageIndex: currentPageIndex, success: { (response) in
            
            let taskArr = response.result?.planArray ?? []
                
            if taskArr.count == 0{
                self.currentPageIndex-=1
            }
        
            self.endLoadMoreData(dataCount:taskArr.count, pageSize: 10)
        
            self.dataSourceArr.append(contentsOf: taskArr)
            self.listContainView.reloadData()
            
        }) { (error) in
            
            self.currentPageIndex-=1
            self.endLoadMoreData(dataCount:0, pageSize: 10)
            NeatHud.showMessage(message: error.errMessage, view: self.view)
            
        }
        
    }
    

    func getPlanList() {
        
        self.listContainView.showLoadingView()
        
        NeatRequest.instance.getProtectionPlanList(planName: currentKeyword, pageIndex: currentPageIndex, success: { (response) in
            
            self.dataSourceArr = response.result?.planArray ?? []
            self.endLoadMoreData(dataCount: self.dataSourceArr.count, pageSize: 10)
            if self.dataSourceArr.count == 0{
                self.listContainView.showEmptyView()
            }else{
                self.listContainView.hidenHolderView()
                self.listContainView.reloadData()
            }
            
        }) { (error) in
            if error.errCode == RequestResponseCode.dataParsingException {
                //数据异常
                NeatHud.dissmissHud(view: self.view)
                self.listContainView.showRequestFaildView {
                    
                }
            }else if error.errCode == RequestResponseCode.netCanNotContent || error.errCode == RequestResponseCode.requestTimeOut{
                //无网络
                NeatHud.dissmissHud(view: self.view)
                self.listContainView.showNetErrorView(errorMessage: error.errMessage) {
                    self.getPlanList()
                }
            }else{
                NeatHud.showMessage(message: error.errMessage, view: self.view)
            }
        }
        
    }

}

extension ProtectionPlanViewController:TopSearchViewDelegate{
    func doSearchWith(keyword: String) {
        currentPageIndex = 1
        currentKeyword = keyword
        self.getPlanList()
        
    }
}
extension ProtectionPlanViewController:UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 64
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.01
    }
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView.init()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let model = self.dataSourceArr[indexPath.row]
        
        let vc = ProtectionPlanTaskViewController.init()
        
        vc.planId = model.planId
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
}
extension ProtectionPlanViewController:UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return dataSourceArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: self.ProtectionPlanCellIdent, for: indexPath) as! ProtectionPlanTableViewCell
        cell.selectionStyle = .none
        cell.configerCell(model: dataSourceArr[indexPath.row])
        
        return cell
    }

}
