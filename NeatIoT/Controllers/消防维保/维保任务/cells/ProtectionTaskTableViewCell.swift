//
//  ProtectionTaskTableViewCell.swift
//  NeatIoT
//
//  Created by Yang Blus on 2020/5/9.
//  Copyright © 2020 Neat. All rights reserved.
//

import UIKit

protocol ProtectionTaskTableViewCellDelegate {
    func startTask(model:ProtectionTask)
}

class ProtectionTaskTableViewCell: UITableViewCell {
    
    @IBOutlet weak var taskNameLable: UILabel!
    
    @IBOutlet weak var entNameLable: UILabel!
    
    @IBOutlet weak var frequencyLable: UILabel!
    
    @IBOutlet weak var taskDateLable: UILabel!
    
    @IBOutlet weak var startBtn: UIButton!
    
    @IBOutlet weak var flagLable: UILabel!
    
    var delegate:ProtectionTaskTableViewCellDelegate?
    
    var currentTaskModel:ProtectionTask?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configerCell(model:ProtectionTask) {
        
        currentTaskModel = model
        
        self.taskNameLable.text = model.planName
        
        self.entNameLable.text = "所属单位:"+model.entName!
        
        self.frequencyLable.text = "维保频率:"+model.frequency!
        
        self.taskDateLable.text = "执行时间:"+model.intervalTime!
        
        if !(model.flagTime?.isEmpty ?? true) {
            self.flagLable.text = model.flagTime
        }else{
            self.flagLable.isHidden = true
        }
        
    }
    
    @IBAction func startTaskBtnClick(_ sender: Any) {
        
        self.delegate?.startTask(model: currentTaskModel!)
    }
}
