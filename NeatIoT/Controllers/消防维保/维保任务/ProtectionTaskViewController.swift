//
//  ProtectionTaskViewController.swift
//  NeatIoT
//
//  Created by Yang Blus on 2020/5/8.
//  Copyright © 2020 Neat. All rights reserved.
//

import UIKit

class ProtectionTaskViewController: SupperTableViewViewController {
    
    let ProtectionTaskCellIdent = "ProtectionTaskCellIdent"
    
    /// 获取列表参数
    var currentPageIndex:Int = 1
    var currentKeyword:String = ""
    var currentFrequence:String = ""
    
    var menuModel:MenuModel?
    
    var dataSourceArr:Array<ProtectionTask> = []
    
    lazy var filterView: NormalFilterView = {
        let filterView = NormalFilterView.init()
        filterView.delegate = self
        return filterView
    }()
    
    lazy var topSearchView: TopSearchView = {
        let topSearchView = TopSearchView.init()
        topSearchView.delegate = self
        return topSearchView
    }()
    
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let mounth = FrequenceModel.init()
        mounth.id = "1"
        mounth.rateName = "每月一次"
        
        let quaqrter = FrequenceModel.init()
        quaqrter.id = "2"
        quaqrter.rateName = "每季一次"
        
        let year = FrequenceModel.init()
        year.id = "3"
        year.rateName = "每年一次"
        
        let freqArr:Array<FrequenceModel> = [mounth,quaqrter,year]
        
        configerFrequenceData(frequenceArr: freqArr)
        
        configerNavBar()
        configerUI()

        getTaskList()
        
    }
    
    @objc override func navRRClick() -> Void {
        
        self.view.addSubview(self.filterView)
        
        self.filterView.snp.makeConstraints { (make) in
            make.edges.equalTo(self.view)
        }
        
    }
    
    func configerNavBar(){
        
        self.titleName = self.menuModel?.name ?? "维保任务"
        
        let filterNavItem = NavItemModel.init()
        filterNavItem.itemIcon = "nav_filter"
        self.rightItems = [filterNavItem]
    }
    
    func configerUI() {
        
        self.view.addSubview(self.topSearchView)
        
        self.listContainView.delegate = self
        self.listContainView.dataSource = self
        self.listContainView.separatorStyle = .singleLine
        self.listContainView.separatorInset = UIEdgeInsets.init(top: 0, left: 5, bottom: 0, right: 5)
        self.listContainView.register(UINib.init(nibName: "ProtectionTaskTableViewCell", bundle: Bundle.main), forCellReuseIdentifier: ProtectionTaskCellIdent)
        self.view.addSubview(self.listContainView)
        
        self.topSearchView.snp.makeConstraints { (make) in
            make.top.left.right.equalTo(self.view)
            make.height.equalTo(NormalConstant.TopSearchViewHeight)
        }
        
        self.listContainView.snp.makeConstraints { (make) in
            make.top.equalTo(self.topSearchView.snp.bottom)
            make.left.right.equalTo(self.view)
            make.bottom.equalTo(self.view)
        }
        
    }
    
    func configerFrequenceData(frequenceArr:Array<FrequenceModel>) {
        
        var dataArr:Array<NormalFilterIItemModel> = []
        for frequenceModel in frequenceArr {
            
            let filter = NormalFilterIItemModel.init()
            filter.filterName = frequenceModel.rateName
            filter.filterID = frequenceModel.id
            filter.isSelect = false
            
            dataArr.append(filter)
        }
        
        let filterFive = NormalFilterIItemModel.init()
        filterFive.filterName = "全部"
        filterFive.filterID = ""
        filterFive.isSelect = true
        dataArr.append(filterFive)
        
        self.filterView.dataSource = dataArr
        
    }
    
    override func refreshAction() {
        
        currentPageIndex = 1
        
        NeatRequest.instance.getProtectionTaskList(planName: currentKeyword, frequency: currentFrequence, pageIndex: 1, success: { (response) in
                
                self.dataSourceArr = response.result?.taskList ?? []
                self.listContainView.reloadData()
                self.endRefresh(isUpdateFooter: true, dataCount: self.dataSourceArr.count, pageSize: 10)
                
        }) { (error) in
            
            NeatHud.showMessage(message: error.errMessage, view: self.view)
    
        }
        
    }
    override func loadMoreAction() {
        
        currentPageIndex+=1
        
        NeatRequest.instance.getProtectionTaskList(planName: currentKeyword, frequency: currentFrequence, pageIndex: currentPageIndex, success: { (response) in
            
            let taskArr = response.result?.taskList ?? []
                
            if taskArr.count == 0{
                self.currentPageIndex-=1
            }
        
            self.endLoadMoreData(dataCount:taskArr.count, pageSize: 10)
        
            self.dataSourceArr.append(contentsOf: taskArr)
            self.listContainView.reloadData()
            
        }) { (error) in
            
            self.currentPageIndex-=1
            self.endLoadMoreData(dataCount:0, pageSize: 10)
            NeatHud.showMessage(message: error.errMessage, view: self.view)
            
        }
        
    }
    
    func getTaskList() {
        
        self.listContainView.showLoadingView()
        
        NeatRequest.instance.getProtectionTaskList(planName: currentKeyword, frequency: currentFrequence, pageIndex: currentPageIndex, success: { (response) in
            
            self.dataSourceArr = response.result?.taskList ?? []
            self.endLoadMoreData(dataCount: self.dataSourceArr.count, pageSize: 10)
            if self.dataSourceArr.count == 0{
                self.listContainView.showEmptyView()
            }else{
                self.listContainView.hidenHolderView()
                self.listContainView.reloadData()
            }
            
        }) { (errorRes) in
            
            if errorRes.errCode == RequestResponseCode.dataParsingException {
                //数据异常
                NeatHud.dissmissHud(view: self.view)
                self.listContainView.showRequestFaildView {
                    
                }
            }else if errorRes.errCode == RequestResponseCode.netCanNotContent || errorRes.errCode == RequestResponseCode.requestTimeOut{
                //无网络
                NeatHud.dissmissHud(view: self.view)
                self.listContainView.showNetErrorView(errorMessage: errorRes.errMessage) {
                    self.getTaskList()
                }
            }else{
                NeatHud.showMessage(message: errorRes.errMessage, view: self.view)
            }
            
        }
        
    }

}

extension ProtectionTaskViewController:UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 102
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView.init()
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.01
    }
    
    
}

extension ProtectionTaskViewController:UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSourceArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: ProtectionTaskCellIdent, for: indexPath) as! ProtectionTaskTableViewCell
        cell.selectionStyle = .none
        cell.delegate = self
        cell.configerCell(model: dataSourceArr[indexPath.row])
        
        return cell
    }
    
}

extension ProtectionTaskViewController:NormalFilterViewDelegate{
    
    func didSelectFilterItme(model: NormalFilterIItemModel) {
        currentPageIndex = 1
        currentFrequence = model.filterID!
        self.getTaskList()
        
    }
         
}

extension ProtectionTaskViewController:TopSearchViewDelegate{
    
    func doSearchWith(keyword: String) {
        currentPageIndex = 1
        currentKeyword = keyword
        self.getTaskList()
        
    }
    
}

extension ProtectionTaskViewController:ProtectionTaskTableViewCellDelegate{
    
    func startTask(model: ProtectionTask) {
        
        let vc = ProtectionHandleViewController.init()
        vc.delegate = self
        vc.taskModel = model
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}

extension ProtectionTaskViewController:ProtectionHandleViewControllerDelgate{
    
    func handleFinish() {
        self.getTaskList()
    }

}
