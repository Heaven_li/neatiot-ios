//
//  ProtectionHandleViewController.swift
//  NeatIoT
//
//  Created by Yang Blus on 2020/5/11.
//  Copyright © 2020 Neat. All rights reserved.
//

import UIKit
import AMapLocationKit

protocol ProtectionHandleViewControllerDelgate {
    func handleFinish()
}

class ProtectionHandleViewController: SupperViewController {
    
    var sendNavItem:UIBarButtonItem?
    /// 单行显示 或 输入控件
    var NormalItemHeight:CGFloat = 0
    /// 文本控件
    var FullTextEditHeight:CGFloat = 0
    /// 图片视频控件高度
    var MediaViewHeight:CGFloat = 0
    /// 耗材控件高度
    var MaterialsViewHeight:CGFloat = 0
    /// 音频控件高度
    var AudioViewHeight:CGFloat = 0
    /// 提交按钮点击 标识
    var isUploadClick:Bool = false
    
    let voiceSemaphore = DispatchSemaphore.init(value: 0)
    
    let mediaSemaphore = DispatchSemaphore.init(value: 0)
    
    var delegate:ProtectionHandleViewControllerDelgate?
    
    /// 上传数据分类
    var voiceArr:Array<AnnexModel> = []
    var imageArr:Array<AnnexModel> = []
    var videoArr:Array<AnnexModel> = []
    
    /// 用户信息单列
    let loginRes = DataArchiver.unArchiver(key:ArchireKey.UserInfor) as! LoginResultModel
    
    /// 工单上传数据model
    var uploadModel:ProtectionHandleReportModel?
    /// 任务上传model
    var taskUploadModel:ProtectionTaskUploadModel?
    /// 任务跳转到处理
    var taskModel:ProtectionTask?
    /// 工单跳转到处理
    var orderModel:ProtectionOrder?
    /// 入场时间model
    var enterTimeRec:EnterTiemModel?
    
    /// 展示数组
    var dataSourceArr:Array<ReportItemModel> = []
    var lastRadioClickIndex:IndexPath?
    var lastVoiceItemCell:VoiceItemTableViewCell?
    
    let audioTool = AudioRecorderTool.sharedInstance
    
    /// 耗材model
    var materialsModel = ReportItemModel.init()
    var materialsArr:Array<MaterialsItemModel> = []
    
    /// 媒体数组
    var mAnnexArr:Array<AnnexModel> = []
    let anxueModel = ReportItemModel.init()
    
    /// 语音数组
    var mVoiceArr:Array<AnnexModel> = []
    let voiceModel = ReportItemModel.init()
    
    lazy var locationManager: AMapLocationManager = {
        
        let locationManager = AMapLocationManager.init()
        locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters
        locationManager.locationTimeout = 2
        locationManager.reGeocodeTimeout = 2
        
        return locationManager
    }()
    
    lazy var protectionEntView: StaticInforView = {
        let protectionEntView = StaticInforView.init()
        return protectionEntView
    }()
    
    lazy var initiatorNameView: StaticInforView = {
        let initiatorNameView = StaticInforView.init()
        return initiatorNameView
    }()
    
    lazy var containView: UIView = {
        let containView = UIView.init()
        return containView
    }()
    
    lazy var containScrollView: UIScrollView = {
        
        let scrollView = UIScrollView.init()
        return scrollView
        
    }()
    
    lazy var nextBtn: UIButton = {
        let btn = UIButton.init(type: .custom)
        btn.setTitle("入场打卡", for: .normal)
        btn.backgroundColor = #colorLiteral(red: 0.003921568859, green: 0.3411764801, blue: 0.5568627715, alpha: 1)
        btn.setTitleColor(.white, for: .normal)
        btn.addTarget(self, action: #selector(goNext), for: .touchUpInside)
        return btn
    }()
//    /// 耗材model
//    lazy var materialsViewModel: MaterialsViewModel = {
//        let model = MaterialsViewModel.init()
//        model.materialsArr = []
//        return model
//    }()
    /// 媒体model
    lazy var addMediaModel: AnnexModel = {
        let add = AnnexModel.init()
        add.annexType = 1
        add.isAddNewItem = true
        add.imageSource = UIImage.init(named: "add_annex")
        return add
    }()
    /// 音频model
    lazy var addVoiceModel: AnnexModel = {
        let add = AnnexModel.init()
        add.annexType = 3
        add.isAddNewItem = true
        return add
    }()

    lazy var mediaView: MediaView = {
        let mediaView = MediaView.init()
        mediaView.delegate = self
        return mediaView
    }()
    
    lazy var audioView: AudioView = {
        let audioView = AudioView.init()
        audioView.delegate = self
        return audioView
    }()

    
    lazy var recHud: AudioRecHud = {
        let hud = AudioRecHud.init(frame: CGRect.zero)
        hud.delegate = self
        return hud
    }()
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.view.endEditing(true)
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        configerNav()
        initCompHeight()
        
        configerUI()
        
    }
    
    func getLocation() {
        
        self.locationManager.requestLocation(withReGeocode: true) { (location, reGeocode, error) in
            
            if let error = error {
                let error = error as NSError
                
                if error.code == AMapLocationErrorCode.locateFailed.rawValue {
                    //定位错误：此时location和regeocode没有返回值，不进行annotation的添加
                    debugPrint("定位错误:{\(error.code) - \(error.localizedDescription)};")
                    
                    if self.isUploadClick{
                        self.checkUploadInfor()
                    }
                    return
                }
                else if error.code == AMapLocationErrorCode.reGeocodeFailed.rawValue
                    || error.code == AMapLocationErrorCode.timeOut.rawValue
                    || error.code == AMapLocationErrorCode.cannotFindHost.rawValue
                    || error.code == AMapLocationErrorCode.badURL.rawValue
                    || error.code == AMapLocationErrorCode.notConnectedToInternet.rawValue
                    || error.code == AMapLocationErrorCode.cannotConnectToHost.rawValue {
                    
                    //逆地理错误：在带逆地理的单次定位中，逆地理过程可能发生错误，此时location有返回值，regeocode无返回值，进行annotation的添加
                    debugPrint("逆地理错误:{\(error.code) - \(error.localizedDescription)};")
                }
                else {
                    //没有错误：location有返回值，regeocode是否有返回值取决于是否进行逆地理操作，进行annotation的添加
                    debugPrint("location\(location?.coordinate.latitude) \(location?.coordinate.longitude) 逆地理编码\(reGeocode?.formattedAddress)")
                }
            }
            
            if let location = location {
                debugPrint("location:%@", location)
                
                if self.isUploadClick {
                    
                    if self.taskModel != nil {
                        self.taskUploadModel?.Latitue = String(location.coordinate.latitude)
                        self.taskUploadModel?.Longitude = String(location.coordinate.longitude)
                    }else if self.orderModel != nil {
                        self.uploadModel?.Latitue = String(location.coordinate.latitude)
                        self.uploadModel?.Longitude = String(location.coordinate.longitude)
                    }
                }
            }
            
            if let reGeocode = reGeocode {
                debugPrint("reGeocode:%@", reGeocode)
                
                if self.isUploadClick {
                    
                    if self.taskModel != nil {
                        self.taskUploadModel?.MaintenanceLocationText = reGeocode.formattedAddress ?? ""
                    }else if self.orderModel != nil {
                        self.uploadModel?.MaintenanceLocationText = reGeocode.formattedAddress ?? ""
                    }
                    
                }else{
                    self.enterTimeRec!.enterAddress = reGeocode.formattedAddress ?? ""
                    CoreDataHelper.init().insertEnterTimeObjec(model: self.enterTimeRec!)
                }
                
            }
            
            if self.isUploadClick{
                self.checkUploadInfor()
            }
        
        }
        
    }
    
    /// 初始化控件高度
    func initCompHeight() {
        
        NormalItemHeight = 46
        
        FullTextEditHeight = 86
        
        //计算上传附件view高度
        /// 单个图片大小
        let itemWidth = (NormalConstant.ScreenWidth - 6*(4 - 1 + 2))/4
        
        if anxueModel.annexArr.count>=1 && anxueModel.annexArr.count <= 4 {
            MediaViewHeight = CGFloat(itemWidth + 6 + 36)
        }else{
            MediaViewHeight = CGFloat((itemWidth + 6)*2 + 36)
        }
        
        //计算耗材view高度
        if nil != materialsModel.materialsArr {
            MaterialsViewHeight = CGFloat(42+26*(materialsModel.materialsArr!.count + 1))
        }
        
        //计算音频view高度
        AudioViewHeight = CGFloat(48) * CGFloat(voiceModel.annexArr.count) + 36
        
        
    }
    
    func configerNav() {
        
        if self.taskModel != nil {
            
            self.titleName = "维保任务"
            
            let submit = NavItemModel.init()
            submit.itemText = "提交"
            
            self.taskUploadModel = ProtectionTaskUploadModel .init()
            self.taskUploadModel?.taskId = self.taskModel?.taskId
            self.taskUploadModel?.entId = self.taskModel?.maintenanceEntId
            self.taskUploadModel?.maintenancePersonnelId = loginRes.user_id
            
            self.rightItems = [submit]
            
            
            if CoreDataHelper.init().getEnterInfor(acceptId: (self.taskModel?.taskId)!).ishave {
                enterTimeRec = CoreDataHelper.init().getEnterInfor(acceptId: (self.taskModel?.taskId)!).enterTime
                self.taskUploadModel?.MaintenanceBelocationText = enterTimeRec?.enterAddress
                self.taskUploadModel?.entranceTime = enterTimeRec?.enterTime
            }else{
                enterTimeRec = EnterTiemModel.init()
                enterTimeRec?.acceptId = self.taskModel?.taskId
            }
            
            
        }else if self.orderModel != nil {
            
            self.titleName = "工单处理"
            
            let submit = NavItemModel.init()
            submit.itemText = "提交"
            
            self.uploadModel = ProtectionHandleReportModel.init()
            self.uploadModel?.acceptOrderId = self.orderModel?.acceptId
            
            
            self.rightItems = [submit]
            
            if CoreDataHelper.init().getEnterInfor(acceptId: (self.orderModel?.acceptId)!).ishave {
                enterTimeRec = CoreDataHelper.init().getEnterInfor(acceptId: (self.orderModel?.acceptId)!).enterTime
                self.uploadModel?.MaintenanceBelocationText = enterTimeRec?.enterAddress
                self.uploadModel?.entranceTime = enterTimeRec?.enterTime
            }else{
                enterTimeRec = EnterTiemModel.init()
                enterTimeRec?.acceptId = self.orderModel?.acceptId
            }
        }
        
        sendNavItem = self.navigationItem.rightBarButtonItems![0]
        
    }
    
    override func navRRClick() {
        
        self.isUploadClick = true
        self.view.endEditing(true)
        
        sendNavItem?.isEnabled = false
        configerUploadInfor()

    }
    
    func configerUploadInfor() {
        
        //清空附件数组
        voiceArr.removeAll()
        imageArr.removeAll()
        videoArr.removeAll()
        
        for model in self.mVoiceArr {
            if !model.isAddNewItem {
                voiceArr.append(model)
            }
        }
        
        for model in self.mAnnexArr{

            if !model.isAddNewItem {
            
                if model.annexType == 1 {
                    //图片
                    let data = model.imageSource!.jpegData(compressionQuality: 0.1)!
                    model.imageSource = UIImage.init(data: data)
                    imageArr.append(model)
                    
                }else if model.annexType == 2 {
                    //视频
                    videoArr.append(model)
                    
                }
            }
        }
        
        if self.videoArr.count != 0{
            NeatHud.showLoading(view: self.view, message: "正在进行视频转码...")
            sourceDecode(isVideo: true, isAudio: false)
        }else{
            self.fileTransformFinish()
        }
        
    }
    
    func sourceDecode(isVideo:Bool,isAudio:Bool) {
        
        DispatchQueue.global().async {
            
            if isVideo{
                
                for videoModel in self.videoArr{
                    VideoFormatTransformTool.moveFileTransformToMp4WithSourceUrl(sourceUrl: videoModel.sourceFile!, finish: { (url) in
                        
                        videoModel.sourceFile = url
                        self.mediaSemaphore.signal()
                        
                    }) {
                        ///失败
                    
                    }
                    self.mediaSemaphore.wait()
                }
                
            }
            DispatchQueue.main.async {
                
                NeatHud.dissmissHud(view: self.view)
                self.fileTransformFinish()
            }

        }
        
    }
    
    func fileTransformFinish() {
        
        self.getLocation()
        
    }
    
    func checkUploadInfor() {
        
        if self.taskModel != nil {
            
            if self.nextBtn.isHidden {
                //有入场时间 取出工时 和 内容描述
                
                self.taskUploadModel?.MaintenanceBelocationText = self.enterTimeRec?.enterAddress ?? ""
                self.taskUploadModel?.entranceTime = self.enterTimeRec?.enterTime ?? ""
                
                if dataSourceArr.count > 6 {
                    
                    let workHour = dataSourceArr[3]
                    
                    let contentDes = dataSourceArr[5]
                    
                    self.taskUploadModel?.workHours = workHour.inforText
                    
                    self.taskUploadModel?.maintenanceContent = contentDes.inforText
                    
                    
                }
                
            }else {
                //没有入场时间 取出工时 和 内容描述
                if dataSourceArr.count > 5 {
                    
                    let workHour = dataSourceArr[2]
                    
                    let contentDes = dataSourceArr[4]
                    
                    self.taskUploadModel?.workHours = workHour.inforText
                    
                    self.taskUploadModel?.maintenanceContent = contentDes.inforText
                    
                }
                
            }
            
            ///校验工时
            let verifyResult = DataVerify.checkWorkHourCheck(workHour: self.taskUploadModel?.workHours ?? "-2")
            if !verifyResult.correct! {
                NeatHud.showMessage(message: verifyResult.errMessage!, view: self.view)
                sendNavItem?.isEnabled = true
                return
            }
            ///校验处理内容
            if nil == self.taskUploadModel?.maintenanceContent || (self.taskUploadModel?.maintenanceContent?.isEmpty ?? true) {
                NeatHud.showMessage(message: "请输入处理内容", view: self.view)
                sendNavItem?.isEnabled = true
                return
            }
            
            
            var uploadMaterialsArr:Array<[String:String]> = []
            
            for model in self.materialsArr {
                
                var dic = [String:String]()
                dic["id"] = model.materialsId
                dic["dosage"] = String(model.secondLableText ?? "0")
                dic["amountPaid"] = String(model.fourLableText ?? "0")
                
                uploadMaterialsArr.append(dic)
                
            }
            taskUploadModel?.materialsArr = uploadMaterialsArr
            taskUploadModel?.videoArr.append(contentsOf: videoArr)
            taskUploadModel?.imageArr.append(contentsOf: imageArr)
            
            
            NeatHud.showLoading(view: self.view, message: "")
            NeatRequest.instance.protectionTaskHandle(model: taskUploadModel!, success: { (response) in
                
                NeatHud.showMessage(message: response.message ?? "提交成功", view: self.view) {
                    
                    self.delegate?.handleFinish()
                    self.navigationController?.popViewController(animated: true)
                }
               
            }) { (error) in
                NeatHud.showMessage(message: error.errMessage, view: self.view)
                self.sendNavItem?.isEnabled = true
                self.isUploadClick = false
            }
              
            
        }else if self.orderModel != nil {
            
            if self.nextBtn.isHidden {
                //有入场时间 取出工时 和 处理内容
                
                self.uploadModel?.MaintenanceBelocationText = self.enterTimeRec?.enterAddress ?? ""
                self.uploadModel?.entranceTime = self.enterTimeRec?.enterTime ?? ""
                
                if dataSourceArr.count > 9 {
                    
                    let workHour = dataSourceArr[4]
                    
                    let contentDes = dataSourceArr[7]
                    
                    self.uploadModel?.workHours = workHour.inforText
                    
                    self.uploadModel?.handleText = contentDes.inforText
                    
                }
                
            }else {
                //没有入场时间 取出工时 和 处理内容
                
                if dataSourceArr.count > 8 {
                    
                    let workHour = dataSourceArr[3]
                    
                    let contentDes = dataSourceArr[6]
                    
                    self.uploadModel?.workHours = workHour.inforText
                    
                    self.uploadModel?.handleText = contentDes.inforText
                    
                }
                
            }
            
            ///校验工时
            let verifyResult = DataVerify.checkWorkHourCheck(workHour: self.uploadModel?.workHours ?? "-2")
            if !verifyResult.correct! {
                NeatHud.showMessage(message: verifyResult.errMessage!, view: self.view)
                self.sendNavItem?.isEnabled = true
                return
            }
            ///校验处理内容
            if nil == self.uploadModel?.handleText || (self.uploadModel?.handleText?.isEmpty ?? true) {
                NeatHud.showMessage(message: "请输入处理内容", view: self.view)
                self.sendNavItem?.isEnabled = true
                return
            }
            
            var uploadMaterialsArr:Array<[String:String]> = []
            
            for model in self.materialsArr {
                
                var dic = [String:String]()
                dic["id"] = model.materialsId
                dic["dosage"] = String(model.secondLableText ?? "0")
                dic["amountPaid"] = String(model.fourLableText ?? "0")
                
                uploadMaterialsArr.append(dic)
                
            }
            uploadModel?.materialsArr = uploadMaterialsArr
            uploadModel?.videoArr.append(contentsOf: videoArr)
            uploadModel?.imageArr.append(contentsOf: imageArr)
            uploadModel?.audioArr.append(contentsOf: voiceArr)
            
            NeatHud.showLoading(view: self.view, message: "")
            
            NeatRequest.instance.protectionOrderHandle(model: uploadModel!, success: { (response) in
                NeatHud.showMessage(message: response.message ?? "提交成功", view: self.view) {
                    
                    self.navigationController?.popViewController(animated: true)
                }
            }) { (error) in
                NeatHud.showMessage(message: error.errMessage, view: self.view)
                self.sendNavItem?.isEnabled = true
                self.isUploadClick = false
            }
            
        }
        
        
    }
    
    func configerUI() {
        
        self.view.addSubview(self.containScrollView)

        self.view.addSubview(self.nextBtn)
        
        self.nextBtn.snp.makeConstraints { (make) in
            make.bottom.trailing.leading.equalTo(self.view)
            make.height.equalTo(54)
        }
        
        self.containScrollView.snp.makeConstraints { (make) in
            
            make.top.equalTo(self.view)
            make.leading.trailing.equalTo(self.view)
            make.bottom.equalTo(self.nextBtn.snp.top)
        
        }
        
        self.containScrollView.addSubview(self.containView)
        
        self.containView.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
            make.width.equalTo(NormalConstant.ScreenWidth)
        }
        
        initDataSource()
        
        configerSubUI()
        
    }
    /// 配置初始数据
    func initDataSource() {
        
        /// 入场时间
        let enterTime = ReportItemModel.init()
        enterTime.titleText = "入场时间"
        enterTime.inforText = ""
        enterTime.itemStyle = .NormalStyle
        enterTime.isShowSepLine = true
            
        /// 维保单位
        let protectionEntModel = ReportItemModel.init()
        protectionEntModel.titleText = "维保单位"
        protectionEntModel.inforText = taskModel?.entName
        protectionEntModel.itemStyle = .NormalStyle
        protectionEntModel.isShowSepLine = true

        /// 维保人
        let protectionPersonModel = ReportItemModel.init()
        protectionPersonModel.titleText = "维保人"
        protectionPersonModel.inforText = loginRes.user_name
        protectionPersonModel.itemStyle = .NormalStyle
        protectionPersonModel.isShowSepLine = true
        
        /// 发起人
        let initiatorNameModel = ReportItemModel.init()
        initiatorNameModel.titleText = "发起人"
        initiatorNameModel.inforText = orderModel?.initiatorName
        initiatorNameModel.itemStyle = .NormalStyle
        initiatorNameModel.isShowSepLine = true
        
        /// 所属单位
        let ownEntModel = ReportItemModel.init()
        ownEntModel.titleText = "所属单位"
        ownEntModel.inforText = orderModel?.entName
        ownEntModel.itemStyle = .NormalStyle
        ownEntModel.isShowSepLine = true
        
        /// 联系方式
        let telModel = ReportItemModel.init()
        telModel.titleText = "联系方式"
        telModel.inforText = orderModel?.tel
        telModel.itemStyle = .NormalStyle
        telModel.isShowSepLine = true
        
        /// 总工时
        let inputInforModel = ReportItemModel.init()
        inputInforModel.titleText = "总工时(小时)"
        inputInforModel.inforText = ""
        inputInforModel.placeHolderStr = "请输入总工时"
        inputInforModel.itemStyle = .NormalInputStyle
        inputInforModel.isShowSepLine = true
        
        /// 耗材
        let functionModel = ReportItemModel.init()
        functionModel.titleText = "耗材"
        functionModel.itemStyle = .FunctionStyle
        functionModel.isShowSepLine = true
        functionModel.functionTitleText = "点击添加耗材"
        
        /// 耗材详情
        let materialsModel = ReportItemModel.init()
        materialsModel.titleText = "耗材列表"
        materialsModel.itemStyle = .MateriasStyle
        materialsModel.materialsArr = self.materialsArr
        
        /// 内容描述
        let editTextViewModel = ReportItemModel.init()
        editTextViewModel.inforText = ""
        editTextViewModel.titleText = "内容描述"
        editTextViewModel.isCanEdit = true
        editTextViewModel.itemStyle = .UnTextViewStyle
        editTextViewModel.placeHolderStr = "请输入内容描述"
        
        /// 工单描述
        let orderDesViewModel = ReportItemModel.init()
        orderDesViewModel.inforText = orderModel?.orderDes
        orderDesViewModel.isCanEdit = false
        if orderModel?.HistoryCount != 0 {
            orderDesViewModel.subTitleText = String(orderModel?.HistoryCount ?? 0)
        }
        orderDesViewModel.titleText = "工单描述"
        orderDesViewModel.itemStyle = .TextViewSubTitleStyle
        
        /// 处理内容
        let handleInforViewModel = ReportItemModel.init()
        handleInforViewModel.inforText = ""
        handleInforViewModel.titleText = "处理内容"
        handleInforViewModel.isCanEdit = true
        handleInforViewModel.placeHolderStr = "请输入处理内容"
        handleInforViewModel.itemStyle = .UnTextViewStyle
        
        /// 上传附件
        mAnnexArr = [addMediaModel]
        anxueModel.itemStyle = .MedialStyle
        anxueModel.titleText = "上传附件"
        anxueModel.annexArr = mAnnexArr
        
        /// 上传语音
        mVoiceArr = [addVoiceModel]
        voiceModel.itemStyle = .VoiceStyle
        voiceModel.titleText = "语音附件"
        voiceModel.annexArr = mVoiceArr
        
        if enterTimeRec?.enterTime != nil {
            enterTime.inforText = enterTimeRec?.enterTime
            dataSourceArr.append(enterTime)
            
            self.nextBtn.isHidden = true
            self.containScrollView.snp.remakeConstraints { (make) in
                make.edges.equalTo(self.view)
            }
        }
            
        if self.taskModel != nil {
            
            dataSourceArr.append(protectionEntModel)
            dataSourceArr.append(protectionPersonModel)
            dataSourceArr.append(inputInforModel)
            if self.materialsArr.count == 0 {
                dataSourceArr.append(functionModel)
            }else{
                dataSourceArr.append(materialsModel)
            }
            
            dataSourceArr.append(editTextViewModel)
            dataSourceArr.append(anxueModel)
            
        }else{
            
            dataSourceArr.append(initiatorNameModel)
            dataSourceArr.append(ownEntModel)
            dataSourceArr.append(telModel)
            dataSourceArr.append(inputInforModel)
            
            if self.materialsArr.count == 0 {
                dataSourceArr.append(functionModel)
            }else{
                dataSourceArr.append(materialsModel)
            }
            
            dataSourceArr.append(orderDesViewModel)
            dataSourceArr.append(handleInforViewModel)
            dataSourceArr.append(anxueModel)
            dataSourceArr.append(voiceModel)
            
        }
        
    }
    
    func configerSubUI() {
        
        var upView:UIView = self.containView
        
        var currentView:UIView?
        
        /// 取出最新的控件高度
        self.initCompHeight()
        
        for (index, model) in dataSourceArr.enumerated() {
            
            if model.itemStyle == .NormalStyle {
                
                let staticView = StaticInforView.init()
                staticView.configerView(model: model)
                
                currentView = staticView
                
                self.containView.addSubview(currentView!)
                
                currentView?.snp.makeConstraints({ (make) in
                    if index == 0{
                        
                        make.top.equalTo(upView)
                        make.width.equalToSuperview()
                        make.leading.trailing.equalToSuperview()
                        make.height.equalTo(NormalItemHeight)
                        
                    }else if index == dataSourceArr.count - 1{
                        
                        make.top.equalTo(upView.snp.bottom)
                        make.width.equalToSuperview()
                        make.leading.trailing.equalToSuperview()
                        make.height.equalTo(NormalItemHeight)
                        make.bottom.equalTo(self.containView.snp.bottom)
                        
                    }else{
                        
                        make.top.equalTo(upView.snp.bottom)
                        make.width.equalToSuperview()
                        make.leading.trailing.equalToSuperview()
                        make.height.equalTo(NormalItemHeight)
                        
                    }
                    
                    
                })
                
            }else if model.itemStyle == .NormalInputStyle {
                
                let inputInforView = InputInforView.init()
                inputInforView.configerView(model: model)
                inputInforView.delegate = self
                currentView = inputInforView
                
                self.containView.addSubview(currentView!)
                
                currentView?.snp.makeConstraints({ (make) in
                    if index == 0{
                        
                        make.top.equalTo(upView)
                        make.width.equalToSuperview()
                        make.leading.trailing.equalToSuperview()
                        make.height.equalTo(NormalItemHeight)
                        
                    }else if index == dataSourceArr.count - 1{
                        
                        make.top.equalTo(upView.snp.bottom)
                        make.width.equalToSuperview()
                        make.leading.trailing.equalToSuperview()
                        make.height.equalTo(NormalItemHeight)
                        make.bottom.equalTo(self.containView.snp.bottom)
                        
                    }else{
                        
                        make.top.equalTo(upView.snp.bottom)
                        make.width.equalToSuperview()
                        make.leading.trailing.equalToSuperview()
                        make.height.equalTo(NormalItemHeight)
                        
                    }
                    
                    
                })
                
            }else if model.itemStyle == .UnTextViewStyle || model.itemStyle == .TextViewSubTitleStyle{
                
                let editTextView = FullTextEditView.init()
                editTextView.configerView(model: model)
                editTextView.delegate = self
                currentView = editTextView
                
                self.containView.addSubview(currentView!)
                
                currentView?.snp.makeConstraints({ (make) in
                    if index == 0{
                        
                        make.top.equalTo(upView)
                        make.width.equalToSuperview()
                        make.leading.trailing.equalToSuperview()
                        make.height.equalTo(FullTextEditHeight)
                        
                    }else if index == dataSourceArr.count - 1{
                        
                        make.top.equalTo(upView.snp.bottom)
                        make.width.equalToSuperview()
                        make.leading.trailing.equalToSuperview()
                        make.height.equalTo(FullTextEditHeight)
                        make.bottom.equalTo(self.containView.snp.bottom)
                        
                    }else{
                        
                        make.top.equalTo(upView.snp.bottom)
                        make.width.equalToSuperview()
                        make.leading.trailing.equalToSuperview()
                        make.height.equalTo(FullTextEditHeight)
                        
                    }
                    
                })
                
            }else if model.itemStyle == .FunctionStyle {
                
                /// 添加耗材
                let functionView = FunctionView.init()
                functionView.configerView(model: model)
                functionView.delegate = self
                currentView = functionView
                
                self.containView.addSubview(currentView!)
                
                currentView?.snp.makeConstraints({ (make) in
                    if index == 0{
                        
                        make.top.equalTo(upView)
                        make.width.equalToSuperview()
                        make.leading.trailing.equalToSuperview()
                        make.height.equalTo(NormalItemHeight)
                        
                    }else if index == dataSourceArr.count - 1{
                        
                        make.top.equalTo(upView.snp.bottom)
                        make.width.equalToSuperview()
                        make.leading.trailing.equalToSuperview()
                        make.height.equalTo(NormalItemHeight)
                        make.bottom.equalTo(self.containView.snp.bottom)
                        
                    }else{
                        
                        make.top.equalTo(upView.snp.bottom)
                        make.width.equalToSuperview()
                        make.leading.trailing.equalToSuperview()
                        make.height.equalTo(NormalItemHeight)
                        
                    }
                    
                })
                
            }else if model.itemStyle == .MateriasStyle {
                
                let materialseView = MaterialsView.init()
                materialseView.configerCell(model: model)
                
                currentView = materialseView
                
                self.containView.addSubview(currentView!)
                
                currentView?.snp.makeConstraints({ (make) in
                    if index == 0{
                        
                        make.top.equalTo(upView)
                        make.width.equalToSuperview()
                        make.leading.trailing.equalToSuperview()
                        make.height.equalTo(MaterialsViewHeight)
                        
                    }else if index == dataSourceArr.count - 1{
                        
                        make.top.equalTo(upView.snp.bottom)
                        make.width.equalToSuperview()
                        make.leading.trailing.equalToSuperview()
                        make.height.equalTo(MaterialsViewHeight)
                        make.bottom.equalTo(self.containView.snp.bottom)
                        
                    }else{
                        
                        make.top.equalTo(upView.snp.bottom)
                        make.width.equalToSuperview()
                        make.leading.trailing.equalToSuperview()
                        make.height.equalTo(MaterialsViewHeight)
                        
                    }
                    
                    
                })
                
            }else if model.itemStyle == .MedialStyle {
                
                /// 附件
                mediaView.configerView(model: model)
                mediaView.delegate = self
                currentView = mediaView
                
                self.containView.addSubview(currentView!)
                
                currentView?.snp.makeConstraints({ (make) in
                    if index == 0{
                        
                        make.top.equalTo(upView)
                        make.width.equalToSuperview()
                        make.leading.trailing.equalToSuperview()
                        make.height.equalTo(MediaViewHeight)
                        
                    }else if index == dataSourceArr.count - 1{
                        
                        make.top.equalTo(upView.snp.bottom)
                        make.width.equalToSuperview()
                        make.leading.trailing.equalToSuperview()
                        make.height.equalTo(MediaViewHeight)
                        make.bottom.equalTo(self.containView.snp.bottom)
                        
                    }else{
                        
                        make.top.equalTo(upView.snp.bottom)
                        make.width.equalToSuperview()
                        make.leading.trailing.equalToSuperview()
                        make.height.equalTo(MediaViewHeight)
                        
                    }
                    
                })
                
            }else if model.itemStyle == .VoiceStyle {
                
                audioView.configerView(model: voiceModel)
                audioView.delegate = self
                currentView = audioView
                
                self.containView.addSubview(currentView!)
                
                currentView?.snp.makeConstraints({ (make) in
                    if index == 0{
                        
                        make.top.equalTo(upView)
                        make.width.equalToSuperview()
                        make.leading.trailing.equalToSuperview()
                        make.height.equalTo(AudioViewHeight)
                        
                    }else if index == dataSourceArr.count - 1{
                        
                        make.top.equalTo(upView.snp.bottom)
                        make.width.equalToSuperview()
                        make.leading.trailing.equalToSuperview()
                        make.height.equalTo(AudioViewHeight)
                        make.bottom.equalTo(self.containView.snp.bottom)
                        
                    }else{
                        
                        make.top.equalTo(upView.snp.bottom)
                        make.width.equalToSuperview()
                        make.leading.trailing.equalToSuperview()
                        make.height.equalTo(AudioViewHeight)
                        
                    }
                    
                })
                
            }
            
            upView = currentView!
            
        }
        
    }
    
    func updateMaterialsView() {
        
        self.initCompHeight()
        
        let materialseView = MaterialsView.init()
        materialseView.configerCell(model: materialsModel)
        
        for (index, value) in dataSourceArr.enumerated() {
            
            if value.itemStyle == .FunctionStyle {
                
                let upView = self.containView.subviews[index-1]
                
                let functionView = self.containView.subviews[index]
                
                let bottomView = self.containView.subviews[index+1]
                
                functionView.removeFromSuperview()
                
                self.containView.addSubview(materialseView)
                
                materialseView.snp.makeConstraints { (make) in
                    make.top.equalTo(upView.snp.bottom)
                    make.leading.trailing.equalToSuperview()
                    make.width.equalToSuperview()
                    make.height.equalTo(MaterialsViewHeight)
                }
        
                bottomView.snp.remakeConstraints { (make) in
                    make.top.equalTo(materialseView.snp.bottom)
                    make.leading.trailing.equalToSuperview()
                    make.width.equalToSuperview()
                    make.height.equalTo(bottomView.frame.size.height)
                    
                }
                
                dataSourceArr.remove(at: index)
                dataSourceArr.insert(materialsModel, at: index)
                
            }
            
        }
        
    }
    
    
    
    func updateMedia(){
        
        anxueModel.annexArr = mAnnexArr

        mediaView.updataUI(model: anxueModel)
        
        let itemWidth = (NormalConstant.ScreenWidth - 6*(4 - 1 + 2))/4
        
        if anxueModel.annexArr.count>=1 && anxueModel.annexArr.count <= 4 {
            let mediaViewHeight = itemWidth + 6 + 36
            mediaView.snp.updateConstraints { (make) in
                make.height.equalTo(mediaViewHeight)
            }
        }else{
            let mediaViewHeight = (itemWidth + 6)*2 + 36
            mediaView.snp.updateConstraints { (make) in
                make.height.equalTo(mediaViewHeight)
            }
        }
        
        
    }
    
    func updateAudio() {
        
        self.voiceModel.annexArr = self.mVoiceArr
        
        self.audioView.updateView(model: voiceModel)
        
        let audioViewHeight = CGFloat(48) * CGFloat(voiceModel.annexArr.count) + 36
        
        audioView.snp.updateConstraints { (make) in
            make.height.equalTo(audioViewHeight)
        }
        
    }
    
    func showRecView() {
        
        self.view.addSubview(self.recHud)
        self.recHud.startRecTime()
        self.recHud.snp.makeConstraints { (make) in
            make.center.equalTo(self.view)
            make.width.equalTo(120)
            make.height.equalTo(120)
        }
        
    }
    
    func dismissRecHud() {
        self.recHud.endRecTime()
        self.recHud.removeFromSuperview()
    }
    
    override func didSelectAnnex(annexModel: AnnexModel) {
        //判断已有附件数量 如果多余8个则移除添加按钮 少于8个添加添加按钮
        if mAnnexArr.count == 8 {
            mAnnexArr.insert(annexModel, at: 0)
            mAnnexArr.removeLast()
        }else{
            mAnnexArr.insert(annexModel, at: 0)
        }
        
        self.updateMedia()
        
    }
    
    @objc func goNext() {
        
        getEnterancTime()
        
    }
    
    func getEnterancTime() {
        
        NeatRequest.instance.getEntranceTime(success: { (response) in
            
            self.reloadScrollView(model: response.result!)
            
            self.getLocation()
            
        }) { (error) in
            
            NeatHud.showMessage(message: error.errMessage, view: self.view)
            
        }
    }
    
    func reloadScrollView(model:HandleEnteranceResult) {
        
        self.enterTimeRec!.enterTime = model.enterTime
        CoreDataHelper.init().insertEnterTimeObjec(model: self.enterTimeRec!)
        
        if taskModel != nil {
        
            /// 入场时间
            let entranceModel = ReportItemModel.init()
            entranceModel.titleText = "入场时间"
            entranceModel.inforText = model.enterTime
            entranceModel.itemStyle = .NormalStyle
            entranceModel.isShowSepLine = true
            
            dataSourceArr.insert(entranceModel, at: 0)
            
            self.nextBtn.isHidden = true
            self.containScrollView.snp.remakeConstraints{ (make) in
                make.edges.equalToSuperview()
            }
            
//            updateContainSize()
            
            let entranceView = StaticInforView.init()
            entranceView.configerView(model: entranceModel)
            
            self.containView.addSubview(entranceView)
            self.containView.bringSubviewToFront(entranceView)
            
            let view = self.containView.subviews[0]
            
            entranceView.snp.makeConstraints { (make) in
                make.top.equalTo(self.containScrollView)
                make.width.equalToSuperview()
                make.height.equalTo(self.NormalItemHeight)
            }
            
            view.snp.remakeConstraints { (make) in
                make.top.equalTo(entranceView.snp.bottom)
                make.width.equalToSuperview()
                make.height.equalTo(self.NormalItemHeight)
            }
            
        }else if orderModel != nil {
            
            
            /// 入场时间
            let entranceModel = ReportItemModel.init()
            entranceModel.titleText = "入场时间"
            entranceModel.inforText = model.enterTime
            entranceModel.itemStyle = .NormalStyle
            entranceModel.isShowSepLine = true
            
            dataSourceArr.insert(entranceModel, at: 0)
            
            self.nextBtn.isHidden = true
            self.containScrollView.snp.remakeConstraints{ (make) in
                make.edges.equalToSuperview()
            }
            
//            updateContainSize()
            
            let entranceView = StaticInforView.init()
            entranceView.configerView(model: entranceModel)
            
            self.containView.addSubview(entranceView)
            self.containView.bringSubviewToFront(entranceView)
            
            let view = self.containView.subviews[0]

            entranceView.snp.makeConstraints { (make) in
                make.top.equalTo(self.containScrollView)
                make.width.equalToSuperview()
                make.height.equalTo(self.NormalItemHeight)
            }
            
            view.snp.remakeConstraints { (make) in
                make.top.equalTo(entranceView.snp.bottom)
                make.width.equalToSuperview()
                make.height.equalTo(self.NormalItemHeight)
            }
        }
        
    }
    
    @objc func reciveMaterials(notify:NSNotification) {
        
        let dataSourceArr:Array<ProtectionMaterials> = notify.object as! Array<ProtectionMaterials>
        
        
        for model in dataSourceArr {
            
            let materialsModel = MaterialsItemModel.init()
            materialsModel.materialsId = model.materialsId
            materialsModel.firstLableText = model.materialsName
            materialsModel.secondLableText = String(model.materialsCount)
            materialsModel.thirdLableText = String(model.materialsPrice! * Float(model.materialsCount))
            materialsModel.fourLableText = String(model.tureTotalMoney)
            
            self.materialsArr.append(materialsModel)
            
        }
        
        self.materialsModel.materialsArr = self.materialsArr
        self.materialsModel.itemStyle = .MateriasStyle

        self.updateMaterialsView()
        
    }
    

}
extension ProtectionHandleViewController:AudioRecHudDelegate{
    
    func recTimeOut() {
        self.endRecVoice()
    }
    
}

extension ProtectionHandleViewController:MediaViewDelegate{
    
    func didSelectItem(model: AnnexModel) {
        //点击
        //先判断是否还能添加 如果已满8个则添加按钮消失
        if model.isAddNewItem {
            self.imagePickerPresent()
        }else{
            //查看图片 或 视频
            let detailVC = MediaPlayViewController.init()
            detailVC.annexModel = model
            self.navigationController?.pushViewController(detailVC, animated: true)
        }
    }
    
    func didLongSelectItem(model: AnnexModel, indexPath: IndexPath) {
        //长按
        //如果不是添加按钮
        if !model.isAddNewItem {
            var isHaveAdd = false
            for item in mAnnexArr {
                if item.isAddNewItem {
                    isHaveAdd = true
                }
            }

            if isHaveAdd  {
                //如果少于8个 则删除对应图片即可
                mAnnexArr.remove(at: indexPath.item)
            }else{
                //如果当前是8个附件 则删除后要添加添加按钮
                mAnnexArr.remove(at: indexPath.item)
                mAnnexArr.append(addMediaModel)
            }

            self.updateMedia()

        }
    }
    
}

extension ProtectionHandleViewController:AudioViewDelegate{
    
    func clickRecVoice(model: AnnexModel, index: IndexPath, cell: VoiceItemTableViewCell) {
            
            if lastRadioClickIndex != nil {
                //有记录之前点击过

                if index == lastRadioClickIndex {
                    //两次点击相同音频
                    if audioTool.isPlay {
                        audioTool.stopPlayRec()
                        cell.voice_image_view.stopAnimating()
                    }else{
                        self.playRecVoice(model: model)
                        cell.voice_image_view.startAnimating()
                    }
                }else{
                    //两次不同
                    if audioTool.isPlay {
                        audioTool.stopPlayRec()
                        lastVoiceItemCell?.voice_image_view.stopAnimating()
                        self.playRecVoice(model: model)
                        cell.voice_image_view.startAnimating()
                    }else{
                        self.playRecVoice(model: model)
                        cell.voice_image_view.startAnimating()
                    }
                }

                lastVoiceItemCell = cell
                lastRadioClickIndex = index

            }else{
                //首次点击
                lastRadioClickIndex = index
                lastVoiceItemCell = cell
                self.playRecVoice(model: model)
                cell.voice_image_view.startAnimating()
            }
            

            
        }
        /// 播放录音
        func playRecVoice(model: AnnexModel) {
            
            audioTool.playRec(model: model, finishPlay: {
                self.lastVoiceItemCell?.voice_image_view.stopAnimating()
            }) {
                self.lastVoiceItemCell?.voice_image_view.stopAnimating()
                NeatHud.showMessage(message: "播放失败", view: self.view)
            }
            
         }

        
        /// 开始录音
        func startRecVoice() {

            if audioTool.isPlay {
                AudioRecorderTool.sharedInstance.stopPlayRec()
            }
            
            audioTool.startRec(url: FileManagerTool.directoryURL(), start: {
                
                self.showRecView()
                
            }) { (isPermissionFalied, recFailed) in
                if isPermissionFalied {
                    self.showAlert(vc: self, title: "提示", message: "尚未取得麦克风使用权限,无法使用语音功能。是否去设置？", leftTitle: "取消", rightTitle: "去设置", leftAction: { (action) in
                        
                        
                    }) { (action) in
                        //去设置
                        let settingsUrl = URL(string:UIApplication.openSettingsURLString)
                        if #available(iOS 10.0, *) {
                            UIApplication.shared.open(settingsUrl!, options: [:], completionHandler: nil)
                        } else {
                            
                        }
                    }
                }
                if recFailed {
                    NeatHud.showMessage(message: "开启录音失败，稍后再试", view: self.view)
                }
            }
            
        }
        
        func endRecVoice() {
            
            self.dismissRecHud()
            
            audioTool.stopRec(finish: { (model) in

    //            model.voiceSecond = self.dismissRecHud()

                //判断已有附件数量 如果多余8个则移除添加按钮 少于8个添加添加按钮
                if self.mVoiceArr.count == 3 {
                    self.mVoiceArr.insert(model, at: 0)
                    self.mVoiceArr.removeLast()
                }else{
                    self.mVoiceArr.insert(model, at: 0)
                }
                
                self.updateAudio()

            }) { (error) in
                NeatHud.showMessage(message: error, view: self.view)
            }
     
        }
        
        func deleteRecVoice(model: AnnexModel, index: IndexPath) {
            
            if audioTool.isPlay {
                audioTool.stopPlayRec()
            }
            
            if !model.isAddNewItem {
                var isHaveAdd = false
                for item in mVoiceArr {
                    if item.isAddNewItem {
                        isHaveAdd = true
                    }
                }
                
                if isHaveAdd  {
                    //如果少于3个 则删除对应图片即可
                    mVoiceArr.remove(at: index.item)
                }else{
                    //如果当前是3个附件 则删除后要添加添加按钮
                    mVoiceArr.remove(at: index.item)
                    mVoiceArr.append(addVoiceModel)
                }
                
                self.updateAudio()
                
            }
            
        }

}

extension ProtectionHandleViewController:FunctionViewDelegate{
    
    
    func didClickAddMaterBtn() {
        
        let vc = MaterialsViewController.init()
        
        if self.taskModel != nil {
            vc.isFromTask = true
        }else if self.orderModel != nil {
            vc.isFromTask = false
        }
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
}
extension ProtectionHandleViewController:InputInforViewDelegate{
    
    func didInputText(text:String) {
        
        if self.taskModel != nil {
            
            for (index,model) in dataSourceArr.enumerated() {
            
                if index == 3 {
                    /// 内容描述
                    model.inforText = text
                }
                
            }
            
        }else if self.orderModel != nil {
            
            for (index,model) in dataSourceArr.enumerated() {
            
                if index == 4 {
                    /// 处理描述
                    model.inforText = text
                }
                
            }
            
        }
    }
    
}

extension ProtectionHandleViewController:FullTextEditViewDelegate{
    
    
    func didClickHistoryRec() {
        
        let vc = ProtectionHistoryRecViewController.init()
        if orderModel != nil {
            vc.orderId = orderModel?.orderId
        }
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    func didInputInfor(text: String) {
        
        if self.taskModel != nil {
            
            for (index,model) in dataSourceArr.enumerated() {
            
                if index == 5 {
                    /// 内容描述
                    model.inforText = text
                }
                
            }
            
        }else if self.orderModel != nil {
            
            for (index,model) in dataSourceArr.enumerated() {
            
                if index == 6 {
                    /// 处理描述
                    model.inforText = text
                }
                
            }
            
        }
        
    }
    
}
