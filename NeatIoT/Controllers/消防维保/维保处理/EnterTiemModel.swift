//
//  EnterTiemRec.swift
//  NeatIoT
//
//  Created by Yang Blus on 2020/5/14.
//  Copyright © 2020 Neat. All rights reserved.
//

import UIKit
import CoreData

class EnterTiemModel: NSManagedObject {
    
    var acceptId:String?
    
    var enterTime:String?
    
    var enterAddress:String?

}
