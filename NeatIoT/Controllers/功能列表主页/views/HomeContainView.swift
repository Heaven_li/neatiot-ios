//
//  HomeContainView.swift
//  NeatIoT
//
//  Created by neat on 2019/10/25.
//  Copyright © 2019 Neat. All rights reserved.
//

import UIKit

protocol HomeContainViewDelegate {
    
    /// 退出登录
    func logout() -> Void
    
    /// 选中功能按钮
    /// - Parameter model: 按钮model
    func selectItem(model:MenuModel) -> Void
    
    
    func privateBtnClick() -> Void
    
    func userProtectBtnClick() -> Void
    
}

class HomeContainView: UIView {
    
    let BANNERCELLIDENTIFIER = "BANNERCELLIDENTIFIER"
    
    let FUNCTIONCELLIDENTIFIER = "FUNCTIONCELLIDENTIFIER"
    
    var dataSourcArr = Array<MenuModel>()
    
    var delegate:HomeContainViewDelegate?
    
    var dataSource: Array<MenuModel> {
        get {
            return Array()
        }
        set {
            dataSourcArr.removeAll()
            dataSourcArr.append(contentsOf: newValue)
            self.containCollectionView.reloadData()
        }
    }
    
    lazy var privateBtn: UIButton = {
        let btn = UIButton.init(type: .custom)
        btn.setTitle("隐私政策", for: .normal)
        btn.titleLabel?.font = UIFont.systemFont(ofSize: 14)
        btn.setTitleColor(#colorLiteral(red: 0.5440994404, green: 0.543546174, blue: 0.5532103613, alpha: 1), for: .normal)
        btn.addTarget(self, action: #selector(privateBtnClick), for: .touchUpInside)
        return btn
    }()
    
    lazy var sepLine: UIView = {
        let sepLine = UIView.init()
        sepLine.backgroundColor = #colorLiteral(red: 0.5440994404, green: 0.543546174, blue: 0.5532103613, alpha: 1)
        return sepLine
    }()
    
    lazy var userProtectBtn: UIButton = {
        let btn = UIButton.init(type: .custom)
        btn.setTitle("服务协议", for: .normal)
        btn.titleLabel?.font = UIFont.systemFont(ofSize: 14)
        btn.setTitleColor(#colorLiteral(red: 0.5440994404, green: 0.543546174, blue: 0.5532103613, alpha: 1), for: .normal)
        btn.addTarget(self, action: #selector(userProtectBtnClick), for: .touchUpInside)
        return btn
    }()
    
    
    lazy var containCollectionView: LZCollectionView = {
        
        let flowLayout = UICollectionViewFlowLayout.init()
        flowLayout.scrollDirection = .vertical
        flowLayout.minimumLineSpacing = 1
        flowLayout.minimumInteritemSpacing = 1
        
        let collectionView = LZCollectionView.init(frame: CGRect.zero, collectionViewLayout: flowLayout)
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.backgroundColor = NormalColor.viewControllerDefaultBgColor
        
        collectionView.register(UINib.init(nibName: "BannerCollectionViewCell", bundle: Bundle.main), forCellWithReuseIdentifier: self.BANNERCELLIDENTIFIER)
        collectionView.register(UINib.init(nibName: "HomeItemCollectionViewCell", bundle: Bundle.main), forCellWithReuseIdentifier: self.FUNCTIONCELLIDENTIFIER)
        
        return collectionView
        
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.addSubview(self.containCollectionView)
        
        
        self.addSubview(self.privateBtn)
        self.addSubview(self.userProtectBtn)
        self.addSubview(self.sepLine)
        
        self.privateBtn.snp.makeConstraints { (make) in
            
            make.bottom.equalTo(self).offset(-10)
            make.centerX.equalTo(self).offset(40)
            
        }
        
        self.sepLine.snp.makeConstraints { (make) in
            make.centerX.equalTo(self)
            make.centerY.equalTo(self.privateBtn.snp.centerY)
            make.width.equalTo(1)
            make.height.equalTo(self.privateBtn.snp.height).multipliedBy(0.5)
        }
        
        self.userProtectBtn.snp.makeConstraints { (make) in
            make.bottom.equalTo(self).offset(-10)
            make.centerX.equalTo(self).offset(-40)
        }
        
        self.containCollectionView.snp.makeConstraints { (make) in
            make.top.equalTo(self)
            make.left.right.equalTo(self)
            make.bottom.equalTo(self.privateBtn.snp.top)
        }
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    @objc func privateBtnClick() {
        self.delegate?.privateBtnClick()
    }
    
    @objc func userProtectBtnClick() {
        self.delegate?.userProtectBtnClick()
    }
    
    
    

}

extension HomeContainView: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
       
        if indexPath.section == 1 {
            self.delegate?.selectItem(model: dataSourcArr[indexPath.item])
        }
        
    }
}

extension HomeContainView: UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 2
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return section == 0 ? 1:dataSourcArr.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if indexPath.section == 0 {
            
            let item = collectionView.dequeueReusableCell(withReuseIdentifier: BANNERCELLIDENTIFIER, for: indexPath) as! BannerCollectionViewCell
            item.delegate = self
            
            return item
            
        }else{
            
            let item = collectionView.dequeueReusableCell(withReuseIdentifier: FUNCTIONCELLIDENTIFIER, for: indexPath) as! HomeItemCollectionViewCell
    
            item.configerItem(menuModel: dataSourcArr[indexPath.item])
            
            return item
            
        }
        
    }
    
}

extension HomeContainView: UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        var itemSize = CGSize.zero
        if indexPath.section == 0 {
            itemSize = CGSize.init(width:NormalConstant.ScreenWidth, height: NormalConstant.ScreenWidth*9/16.0)
        }else{
            itemSize = CGSize.init(width:(NormalConstant.ScreenWidth-2)/3.0, height: (NormalConstant.ScreenWidth-2)/3.0)
        }
        return itemSize
    }
    

    
}

extension HomeContainView: BannerCollectionViewCellDelegate{
    func logout() {
        self.delegate?.logout()
    }
}
