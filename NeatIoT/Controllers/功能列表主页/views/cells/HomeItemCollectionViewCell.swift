//
//  HomeItemCollectionViewCell.swift
//  NeatIoT
//
//  Created by neat on 2019/10/25.
//  Copyright © 2019 Neat. All rights reserved.
//

import UIKit

class HomeItemCollectionViewCell: UICollectionViewCell {
    
    
    @IBOutlet weak var iconImageView: UIImageView!
    
    @IBOutlet weak var mainTitleLable: UILabel!
    
    @IBOutlet weak var desLable: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        
    }
    
    func configerItem(menuModel:MenuModel) -> Void {
        
        iconImageView.image = UIImage.init(named: menuModel.style_id)
        mainTitleLable.text = menuModel.name
        desLable.text = menuModel.description
        
    }

}
