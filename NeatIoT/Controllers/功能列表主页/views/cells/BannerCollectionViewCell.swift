//
//  BannerCollectionViewCell.swift
//  NeatIoT
//
//  Created by neat on 2019/10/25.
//  Copyright © 2019 Neat. All rights reserved.
//

import UIKit

protocol BannerCollectionViewCellDelegate {
    func logout() -> Void
}

class BannerCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var logoutBtn: UIButton!

    var delegate:BannerCollectionViewCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    @IBAction func logoutClick(_ sender: Any) {
        
        debugPrint("退出登录 cell")
        self.delegate?.logout()
        
    }
    

}
