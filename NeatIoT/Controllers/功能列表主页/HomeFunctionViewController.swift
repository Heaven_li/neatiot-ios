//
//  HomeFunctionViewController.swift
//  NeatIoT
//
//  Created by neat on 2019/9/29.
//  Copyright © 2019 Neat. All rights reserved.
//

import UIKit


class HomeFunctionViewController: SupperViewController {
    
    var num = 0
    let semaphore = DispatchSemaphore.init(value: 1)
    
    lazy var containView: HomeContainView = {
        let containView = HomeContainView.init(frame: CGRect.init(x: 0, y: 0, width: NormalConstant.ScreenWidth, height: NormalConstant.ScreenHeight))
        containView.delegate = self
        return containView
    }()
    
    lazy var stateBarCover: UIView = {
        
        let view = UIView.init()
        
        if NormalConstant.ISLHP {
            view.frame = CGRect.init(x: 0, y: 0, width: NormalConstant.ScreenWidth, height: UIApplication.shared.statusBarFrame.size.height)
        }else{
            view.frame = CGRect.init(x: 0, y: 0, width: NormalConstant.ScreenWidth, height: 20)
        }
        
        let glayer = CAGradientLayer.init()
        glayer.frame = view.bounds
        glayer.startPoint = CGPoint.init(x: 0, y: 0.5)
        glayer.endPoint = CGPoint.init(x: 1, y: 0.5)
        glayer.colors = [#colorLiteral(red: 0, green: 0.09019607843, blue: 0.2941176471, alpha: 1).cgColor,#colorLiteral(red: 0.01049359236, green: 0.1155966893, blue: 0.3619515896, alpha: 1).cgColor]
        view.layer.addSublayer(glayer)
        
        return view
    }()
    
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        self.isCanSwiperBack = false
        self.view.addSubview(self.containView)
        
        self.view.addSubview(stateBarCover)
        
        getMenu()
        
        
    
    }
    
    func getMenu() {
        
        NeatHud.showLoading(view: self.view, message: "")
        
        NeatRequest.instance.getMenuList(success: { (response) in

            NeatHud.dissmissHud(view: self.view)
            self.containView.containCollectionView.hidenHolderView()
            self.containView.dataSource = response.response_result

        }) { (errorResponse) in

            if errorResponse.errCode == RequestResponseCode.dataParsingException {
                //数据异常
                NeatHud.dissmissHud(view: self.view)
                self.containView.containCollectionView.showRequestFaildView {

                }
            }else if errorResponse.errCode == RequestResponseCode.netCanNotContent || errorResponse.errCode == RequestResponseCode.requestTimeOut || errorResponse.errCode == RequestResponseCode.canNotContentHost{
                //无网络
                NeatHud.dissmissHud(view: self.view)
                self.containView.containCollectionView.showNetErrorView(errorMessage: errorResponse.errMessage) {
                    self.getMenu()
                }
            }else{
                NeatHud.showMessage(message: errorResponse.errMessage, view: self.view)
            }

        }
        
    }
    
    override func backClick() {

        self.navigationController?.popViewController(animated: true)
    }
    
    
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension HomeFunctionViewController:HomeContainViewDelegate{
    
    
    func privateBtnClick() {
        
        let vc = WebViewViewController.init();
        vc.navTitleStr = "隐私政策"
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    func userProtectBtnClick() {
        
        let vc = WebViewViewController.init();
        vc.navTitleStr = "服务协议"
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    //退出
    func logout() {
        
        debugPrint("退出")
        NeatHud.showLoading(view: self.view, message:"")
        NeatRequest.instance.logout(success: { (response) in
            NeatHud.dissmissHud(view: self.view)
            
            DataArchiver.clearArchiver(keyArr:[ArchireKey.UserInfor])
            UIApplication.shared.keyWindow?.rootViewController = SupperNavigationViewController.init(rootViewController: LoginViewController.init())
            self.navigationController?.popToRootViewController(animated: true)
            
        }) { (error) in
            NeatHud.dissmissHud(view: self.view)
            NeatHud.showMessage(message: error.errMessage, view: self.view)
        }
        
    }
    //选中item
    func selectItem(model: MenuModel) {
        
        if model.style_id == "warn_log" {
            //警情日志
            let jumpController = LogViewController.init()
            jumpController.menuModel = model
            self.navigationController?.pushViewController(jumpController, animated: true)
            return
            
        }else if model.style_id == "realtime_monitor"{
            //实时监控
            
        }else if model.style_id == "electric_safty"{
            //用电安全
            
        }else if model.style_id == "patrol"{
            //消防巡检
            let jumpController = InspectionViewController.init()
            jumpController.menuModel = model
            self.navigationController?.pushViewController(jumpController, animated: true)
            return
            
        }else if model.style_id == "fire_protection"{
            //消防维保
            let jumpController = FireProtectionViewController.init()
            jumpController.menuModel = model
            self.navigationController?.pushViewController(jumpController, animated: true)
            return
        }else if model.style_id == "device_manager"{
            //设备管理
            let jumpController = DeviceManagerViewController.init()
            jumpController.menuModel = model
            self.navigationController?.pushViewController(jumpController, animated: true)
            return
        }else if model.style_id == "query_statistics"{
            //查询统计
            
        }else if model.style_id == "param_config"{
            //参数设置
            
        }else if model.style_id == "about_us"{
            //关于我们
            
        }else if model.style_id == "load_car"{
            //生产检测
            
        }else if model.style_id == "device_Query"{
            //设备查询
            let jumpController = QureyStatsMainViewController.init()
            jumpController.menuModel = model
            self.navigationController?.pushViewController(jumpController, animated: true)
            return
        }
        
        NeatHud.showMessage(message: "该功能尚未开放！", view: self.view)
        
    }
    
}


