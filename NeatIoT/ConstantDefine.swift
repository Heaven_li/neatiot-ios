//
//  ConstantDefine.swift
//  NeatIoT
//
//  Created by neat on 2019/10/9.
//  Copyright © 2019 Neat. All rights reserved.
//

import UIKit

///// 网络请求接口地址类
//class UrlHost: NSObject {
//    
//    
//    
//}


/// 警情日志 系统类别
class DeciceSystemCategory: NSObject {
    
    /** 火系统 deviceIdType=1->uitd; deviceIdType=2->消防主机; deviceIdType=3->火信号*/
   static let Fire = 0;
   /** 水系统 deviceIdType=0->一体式水; deviceIdType=1->水网关; deviceIdType=2->水信号*/
   static let Water = 1;
   /** 电气火灾系统 deviceIdType=1->uitd; deviceIdType=2->消防主机; deviceIdType=3->火信号*/
   static let Elecrtical = 2;
   /** 气体灭火系统 deviceIdType=1->uitd; deviceIdType=2->消防主机; deviceIdType=3->火信号*/
   static let GasExtinguisher = 3;
   /** 电源监控系统 deviceIdType=1->uitd; deviceIdType=2->消防主机; deviceIdType=3->火信号*/
   static let PowerMonitoringSystem = 5;
   /** 消防应急照明与指示疏散系统 deviceIdType=1->uitd; deviceIdType=2->消防主机; deviceIdType=3->火信号*/
   static let FireLightingAndIndicatingSystem = 6;
   /** 可燃气体报警系统 deviceIdType=1->uitd; deviceIdType=2->消防主机; deviceIdType=3->火信号*/
   static let GasAlarmSystem = 7;
   /** 防火门系统  deviceIdType=1->uitd; deviceIdType=2->消防主机; deviceIdType=3->火信号*/
   static let FireDoorSystem = 8;
   /** 家用火灾安全报警系统 家用火灾安全报警系统 现未接入*/
   static let HomeFireAlarmSystem = 9;
   /** 独立式光电感烟火灾探测报警器 deviceIdType 忽略 为NB设备*/
   static let NBSmokeDetector = 10;
   /** 独立式感温火灾探测报警系统 deviceIdType 忽略 为NB设备*/
   static let NBSensibleTemperature = 11;
   /** 独立式可燃气体探测器系统 deviceIdType 忽略 为NB设备 */
   static let NBCombustibleGas = 12;
   /** 智慧用电系统  deviceIdType=1 ->智慧用电网关;  deviceIDType=3->通道*/
   static let SmartPowerSystem = 13;
    
}

/// 设备类型
class DeviceModel: NSObject {
    
    /// 火系统
    class FireSystem: NSObject {
        
        /// 一体化传输装置
        static var uitd_NT9009: String {
            return "NT9009"
        }
        
        /// 一体化主机
        static var IntegratedTransmissionHost: String {
            return "IntegratedTransmissionHost"
        }
        
        /// 一体化火器件
        static var IntegratedDevice: String {
            return "IntegratedDevice"
        }
        
        
    }
    
    /// 水系统
    class WaterSystem: NSObject {
        
        ///Neat水信号
        static var NeatWaterSingal: String {
            return "NeatWaterSingal"
        }
        
        ///neat水网关
        static var wireless_NT8327: String {
            return "NT8327"
        }
        ///消防管网压力探测器 拓扑索尔
        static var Wgw_NT8161A: String {
            return "NT8161A"
        }
        
        ///消防管网压力探测器 拓扑索尔
        static var Wgw_NT8164A: String {
            return "NT8164A"
        }
        
        ///消火检测终端 拓扑索尔
        static var Wgw_NT8167A: String {
            return "NT8167A"
        }
        
        ///消防栓终端检测 铭控
        static var Wgw_NT8167C: String {
            return "NT8167C"
        }
        
        ///红色水压水位探测器 万讯
        static var Wgw_NT8161B: String {
            return "NT8161B"
        }
        
        ///消防水位探测器 万讯
        static var Wgw_NT8164B: String {
            return "NT8164B"
        }
        
        ///有人泵房检测网关
        static var NT8160A: String {
            return "NT8160A"
        }
        
        ///有人泵房检测网关 下属器件
        static var NT8160A_device: String {
            return "NT8160A_device"
        }
        
        
    }
    
    /// 智慧用电
    class SmartElectricitySystem: NSObject {
        
        ///智慧用电网关
        static var SmartPowerGateway: String {
            return "NT8126"
        }
        
        ///智慧用电通道
        static var SmartPowerChanel: String {
            return "SmartPowerChanel"
        }
        
        ///一体式智慧用电8127A
        static var ElectricGateway_NT8127A: String {
            return "NT8127A"
        }
        
        ///一体式智慧用电8127C
        static var ElectricGateway_NT8127C: String {
            return "NT8127C"
        }
        
        ///一体式智慧用电8128A
        static var ElectricGateway_NT8128A: String {
            return "NT8128A"
        }
        
        
        
    }
    
    
    /// 家用系统
    class FamilySystem: NSObject {
        
        ///家用网关
        static var HomeGateway: String {
            return "NT8140"
        }
        
        ///家用网关器件
        static var HomeGatewayDevice: String {
            return "HomeGatewayDevice"
        }
        
        ///家用火灾报警控制器
        static var HomeFire9008: String {
            return "HomeFire9008"
        }
    }
    
    ///NB设备
    class NBDevice: NSObject {
        
        ///感烟 感温
        static var Probe: String {
            return "8131/8132"
        }
        
//        ///感温
//        static var Temperature: String {
//            return "Temperature"
//        }
        
    }
    
    ///可燃气体探测器
    class GasDetector: NSObject {
        
        ///HM710NVM-NB
        static var HM710NVM: String {
            return "HM710NVMNB"
        }
        
        ///WS2CG-NB
        static var WS2CG: String {
            return "WS2CGNB"
        }
        
    }
    
}

/// 网络请求返回码
class RequestResponseCode: NSObject {
    /// 服务端返回码
    static let responseOk = 200                //成功
    static let validateError = 401             //验证错误
    static let tokenValidateError = 402        //token验证错误
    static let forbidden = 403                 //拒绝访问
    static let notFound = 404                  //没找到
    static let tokenTimeOut = 405              //token过期
    static let unDefineError = 500             //服务器错误
    static let notImplemented = 501            //未实现错误

    static let dataParsingException = 7001     //数据解析异常
    
    static let urlIndival = 10000              //无效的URL
    static let netCanNotContent = -1009        //无网络服务
    static let canNotContentHost = -1004       //无法连接服务器
    static let requestTimeOut = -1001          //请求超时
    
}

/// 规范颜色
class NormalColor: NSObject {
    /// 所有视图控制器的默认颜色
    static let viewControllerDefaultBgColor = #colorLiteral(red: 0.9215686275, green: 0.9215686275, blue: 0.9215686275, alpha: 1)
    /// 弹出框半透明背景色
    static let opaBgColor = #colorLiteral(red: 0.2274509804, green: 0.2, blue: 0.1882352941, alpha: 0.45)
    /// tableview 分割线颜色
    static let tableviewSepLineColor = #colorLiteral(red: 0.8829022091, green: 0.8829022091, blue: 0.8829022091, alpha: 1)
    /// 火警
    static let Fire = #colorLiteral(red: 1, green: 0.2235294118, blue: 0.337254902, alpha: 1)
    /// 正常
    static let Normal = #colorLiteral(red: 0.2352941176, green: 0.6352941176, blue: 0.4509803922, alpha: 1)
    /// 故障
    static let Fault = #colorLiteral(red: 0.9843137255, green: 0.6666666667, blue: 0.007843137255, alpha: 1)
    /// 报警
    static let Alarm = #colorLiteral(red: 0.7098039216, green: 0.568627451, blue: 0.9411764706, alpha: 1)
    /// 未找到
    static let NotFound = #colorLiteral(red: 0.6, green: 0.6, blue: 0.6, alpha: 1)
    /// 离线
    static let Offline = #colorLiteral(red: 0.6, green: 0.6, blue: 0.6, alpha: 1)
    /// 标准文字颜色
    static let NormalFontColor = #colorLiteral(red: 0.2152313292, green: 0.2236304879, blue: 0.2284422219, alpha: 1)
    /// 副标题文字颜色
    static let SubFontColor = #colorLiteral(red: 0.2869201801, green: 0.2980533466, blue: 0.3050713869, alpha: 1)
    /// 分割线颜色
    static let SepLineColor = #colorLiteral(red: 0.8806095517, green: 0.8806095517, blue: 0.8806095517, alpha: 1)
}

/// 通知中心名字
class CustomNotificationName: NSObject {
    
    static let ReloadDeviceListData = "ReloadDeviceListData"
    
    static let ReciveMaterialsData = "ReciveMaterialsData"
    
    static let ReloadHostDeviceListData = "ReloadHostDeviceListData"
}

class ArchireKey: NSObject{
    
    static let UserInfor = "UserInfor"
    static let InspectionFrequence = "InspectionFrequence"
    
}

/// 常量
class NormalConstant: NSObject {
    
    /// 取状态栏高度 在iOS13之后该方法被废弃 根据状态栏高度判断是不是刘海屏
    static let ISLHP: Bool = UIApplication.shared.statusBarFrame.size.height > 20 ? true:false
    static let LHPBottom_Space: CGFloat = 34.0
    static let NavBarHeight: CGFloat = UIApplication.shared.statusBarFrame.size.height == 44 ? 88:64
    static let BottomSafeSpace: CGFloat = UIApplication.shared.statusBarFrame.size.height == 44 ? 34:0


    /// 系统化组件尺寸常量
    /// 屏幕高度
    static let ScreenHeight: CGFloat = UIScreen.main.bounds.height
    /// 屏幕宽度
    static let ScreenWidth: CGFloat = UIScreen.main.bounds.width
    /// 设备基础信息view高度
    static let DeviceBasicViewHeight:CGFloat = 96
    /// 设备信息分割cell高度
    static let DeviceInforContainSepHeight:CGFloat = 10
    /// 设备信息更多cell高度
    static let DeviceInforContainMoreHeight:CGFloat = 26
    /// 设备数据信息view高度
    static let DeviceDataViewHeight:CGFloat = 54
    /// 圆角按钮圆角
    static let RadioBtnCorner: CGFloat = UIScreen.main.bounds.height * 0.06/2.0
    /// 圆角按钮高度
    static let RadioBtnHeight: CGFloat = UIScreen.main.bounds.height * 0.06
    
    /// 扫码框 占屏幕宽度比例
    static let SCBORDERWIDTHSCAL:CGFloat = 0.6
    
    /// 扫码框 距顶部距离
    static let SCBORDERTOP:CGFloat = 100
    
    /// 扫码边角长度
    static let CORNERLINELENGTH:CGFloat = 15
    
    /// 搜索栏高度
    static let TopSearchViewHeight = 56
    
}

/// 设备通讯方式
class DeviceConnectType: NSObject {
    
    /// 直连
    static let DirectConnectionType = "1"
    /// 移动
    static let MobileConnectionType = "2"
    /// 电信
    static let TelecommunConnectionType = "3"
    /// 潍坊
    static let WFConnectionType = "4"
}

/// 设备级别：1:网关 2:主机 3:器件
class DeviceLevel: NSObject {
    
    /// 网关
    static let Gateway = 1
    /// 主机
    static let Host = 2
    /// 器件
    static let Device = 3
    
}

class WaterManufacturer: NSObject {
    /// 拓普索尔
    static let TPSE = "4001"
    /// 万讯
    static let WX = "4003"
}

/// 报警类型
class AlarmStateType: NSObject {
    
    /**正常*/
    static let Normal = 0
    /**火警*/
    static let Fire = 1
    /**故障*/
    static let Fault = 2
    /**正常*/
    static let Status = 3
    /**操作*/
    static let Operation = 4
    /**报警*/
    static let Alarm = 5
    /**未找到*/
    static let NotFound = 254
    /**离线*/
    static let Offline = 255
}

/// 设备信息项
class DeviceInforType: NSObject {
    
    /// 设备编码
    static let code = "code"
    /// 选择设备编码
    static let select_code = "select_code"
    /// 安装位置
    static let address = "address"
    /// 设备名称
    static let name = "name"
    /// 设备类型
    static let type = "type"
    /// 有人泵房附属器件类型
    static let usrDeviceType = "UsrDeviceType"
    /// 型号
    static let model = "model"
    /// 报文协议
    static let message_protocol = "message_protocol"
    /// 厂商
    static let manufacturer = "manufacturer"
    /// 中心
    static let domain = "domain"
    /// 企业
    static let enterprise = "enterprise"
    /// 建筑
    static let building = "building"
    /// 部位
    static let keypart = "keypart"
    /// 事件类型
    static let eventTyep = "eventTyep"
    /// 发生时间
    static let appendTime = "appendTime"
    
    /// GIS定位
    static let gis = "gis"
    /// 测量单位
    static let unit = "unit"
    /// 信号类型
    static let signal_type = "signal_type"
    /// 通讯方式
    static let connection_type = "connection_type"
    /// 真(1)值含义
    static let true_mean_type = "true_mean_type"
    /// 假(0)值含义
    static let false_mean_type = "false_mean_type"
    /// 阈值下限
    static let threshold_lower_limit = "threshold_lower_limit"
    /// 阈值上限
    static let threshold_upper_limit = "threshold_upper_limit"
    /// 下限报警值
    static let lower_limit_warning = "lower_limit_warning"
    /// 下下限报警值
    static let lower_lower_limit_warning = "lower_lower_limit_warning"
    /// 上限报警值
    static let upper_limit_warning = "upper_limit_warning"
    /// 上上限报警值
    static let upper_upper_limit_warning = "upper_upper_limit_warning"
    /// 负责人
    static let principal = "principal"
    /// 联系电话
    static let contact_number = "contact_number"
    /// 变比系数
    static let ratio_coefficient = "ratio_coefficient"
    /// 万讯IMEI号
    static let imei = "imei"
    /// 万讯IMSI号
    static let imsi = "imsi"
    
    /// 智慧用电
    /// SIM卡号
    static let sim = "sim"
    /// 上传周期
    static let timeout = "timeout"
    /// 软件版本
    static let soft_version = "version"
    
    /// 巡检频率
    static let frequency = "frequency"
    /// 完成情况
    static let finishRate = "finishRate"
    
    ///趋势图筛选频率
    static let frequency_query = "frequency_query"
    
    ///可燃气体上报频率
    static let gas_detector_fre = "gas_detector_fre"
    
}

/// 巡检信息项
class InspectionInforType: NSObject{
    
    /// 巡检点名称
    static let pointName = "pointName"
    /// 项目类型
    static let projectType = "projectType"
    /// 项目子类型
    static let projectSubType = "projectSubType"
    /// 企业
    static let enterprise = "enterprise"
    /// 建筑
    static let building = "building"
    /// 部位
    static let keypart = "keypart"
    
    
}



